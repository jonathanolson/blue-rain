package edu.colorado.phet.util;

import java.io.*;
import java.util.*;
import java.util.zip.GZIPInputStream;

import com.jonathanolson.util.FileUtils;

public class MoleculeReader {
    public static void debug( int cid, String mess ) {
//        System.out.println( mess + ": " + cid );
    }

    public static void main( String[] args ) {
        String dir = args[0];
        System.out.println( "dir: " + dir );
        assert ( new File( dir ).exists() );

        File dirFile = new File( dir );
        List<File> files = new LinkedList<File>( Arrays.asList( dirFile.listFiles( new FilenameFilter() {
            @Override
            public boolean accept( File dir, String name ) {
                return name.endsWith( ".sdf.gz" );
            }
        } ) ) );
        Collections.sort( files, new Comparator<File>() {
            @Override
            public int compare( File a, File b ) {
                return a.getName().compareTo( b.getName() );
            }
        } );
        int count = 1;
        try {
            for ( File file : files ) {
                System.out.print( "processing " + file.getName() + ", " + count + " of " + files.size() + ": " );
                count++;

                int countInFile = 0;

                BufferedReader reader = new BufferedReader( new InputStreamReader( new GZIPInputStream( new FileInputStream( file ) ) ) );

                boolean marker = true;
                int cid = 0;

                // flags
                boolean ok = true;
                boolean isotopeMarker = false;
                boolean heavyMarker = false;
                boolean componentMarker = false;
                boolean chargeMarker = false;

                StringBuilder builder = new StringBuilder();

                while ( reader.ready() ) {
                    String line = reader.readLine();
                    if ( marker ) {
                        marker = false;
                        cid = Integer.parseInt( line );
//                    System.out.println( "Reading: #" + cid );
                    }

                    if ( line.equals( "$$$$" ) ) {
                        // separator

                        marker = true;

                        if ( ok ) {
                            debug( cid, "OK" );
                            countInFile++;
                            FileUtils.writeString( new File( "/home/jon/phet/molecules/full3d/" + cid + ".sdf" ), builder.toString() );
                        }

                        builder = new StringBuilder();
                        ok = true;
                        isotopeMarker = false;
                        heavyMarker = false;
                        componentMarker = false;
                        chargeMarker = false;
                    }
                    else {
                        if ( ok ) {
                            builder.append( line ).append( "\n" );
                            if ( isotopeMarker ) {
                                isotopeMarker = false;
                                int isotopic = Integer.parseInt( line );
                                if ( isotopic > 0 ) {
                                    ok = false;
                                    debug( cid, "ISO" );
                                }
                            }
                            if ( heavyMarker ) {
                                heavyMarker = false;
                                int heavy = Integer.parseInt( line );
                                if ( heavy > 4 ) {
                                    ok = false;
                                    debug( cid, "HEAVY" );
                                }
                            }
                            if ( componentMarker ) {
                                componentMarker = false;
                                int components = Integer.parseInt( line );
                                if ( components > 1 ) {
                                    ok = false;
                                    debug( cid, "COMPONENTS" );
                                }
                            }
                            if ( chargeMarker ) {
                                chargeMarker = false;
                                if ( !line.trim().equals( "0" ) ) {
                                    ok = false;
                                    debug( cid, "TOTAL CHARGE" );
                                }
                            }
                            if ( line.equals( "> <PUBCHEM_ISOTOPIC_ATOM_COUNT>" ) ) {
                                isotopeMarker = true;
                            }
                            if ( line.equals( "> <PUBCHEM_HEAVY_ATOM_COUNT>" ) ) {
                                heavyMarker = true;
                            }
                            if ( line.equals( "> <PUBCHEM_COMPONENT_COUNT>" ) ) {
                                componentMarker = true;
                            }
                            if ( line.equals( "> <PUBCHEM_TOTAL_CHARGE>" ) ) {
                                chargeMarker = true;
                            }
                        }
                    }
                }

                System.out.println( countInFile );

                reader.close();

            }
        }
        catch( IOException e ) {
            e.printStackTrace();
        }

    }

}
