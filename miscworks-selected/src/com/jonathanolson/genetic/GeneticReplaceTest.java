package com.jonathanolson.genetic;

import java.util.Arrays;

import com.jonathanolson.genetic.blockmutations.RandomExpressionMutation;
import com.jonathanolson.util.java.ast.Function;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.expressions.FunctionCall;
import com.jonathanolson.util.java.ast.expressions.IntegerConstant;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.ExpressionStatement;
import com.jonathanolson.util.java.ast.statements.SimpleAssignment;

public class GeneticReplaceTest {

    public static void main( String[] args ) {
        Block block = new Block();

        Variable x = new Variable( Type.INTEGER, "x" );
        Variable y = new Variable( Type.INTEGER, "y" );

        block.addStatement( new SimpleAssignment( new VariableReference( x ), new IntegerConstant( 2 ) ) );
        block.addStatement( new SimpleAssignment( new VariableReference( y ), new VariableReference( x ) ) );
        block.addStatement( new ExpressionStatement( new FunctionCall( new Function( "fun", Arrays.asList( Type.INTEGER, Type.INTEGER ), Type.VOID ), Arrays.asList( (Expression) new VariableReference( x ), (Expression) new VariableReference( y ) ) ) ) );

        block.cooldown();

        System.out.println( "AAAAAAAAAAAAAAAAA\n" + block.toString() );

        block = (Block) block.copy();

        block.cooldown();

        System.out.println( "BBBBBBBBBBBBBBBBB\n" + block.toString() );

        block.replaceVariable( x, new Variable( Type.INTEGER, "x" ) );

        block.cooldown();

        System.out.println( "CCCCCCCCCCCCCCCCC\n" + block.toString() );

        block = (Block) block.copy();

        block.cooldown();

        System.out.println( "DDDDDDDDDDDDDDDDD\n" + block.toString() );

        new RandomExpressionMutation().mutateBlock( block );
        block.cooldown();

        System.out.println( "***\n" + block.toString() );
    }
}
