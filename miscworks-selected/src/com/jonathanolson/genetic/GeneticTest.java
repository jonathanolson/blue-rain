package com.jonathanolson.genetic;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import com.jonathanolson.util.java.CompilerUtils;
import com.jonathanolson.util.java.JavaStringSource;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.Equals;
import com.jonathanolson.util.java.ast.expressions.IntegerAdd;
import com.jonathanolson.util.java.ast.expressions.IntegerConstant;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.*;

public class GeneticTest {
    public static void main( String[] args ) {

        Block block = new Block();

        block.addStatement( new PrintStatement( new IntegerConstant( 5 ) ) );
        block.addStatement( new PrintStatement( new IntegerAdd( new IntegerConstant( 7 ), new IntegerConstant( 1 ) ) ) );

        Variable x = new Variable( Type.INTEGER );

        block.addVariable( x );

        block.addStatement( new SimpleAssignment( new VariableReference( x ), new IntegerConstant( 2 ) ) );
        block.addStatement( new PrintStatement( new VariableReference( x ) ) );


        Variable y = new Variable( Type.INTEGER );
        block.addVariable( y );

        Block ifBlock = new Block();
        ifBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( 0 ) ) );
        Block elseBlock = new Block();
        elseBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( -1 ) ) );

        block.addStatement( new If( new Equals( new VariableReference( x ), new IntegerConstant( 2 ) ), ifBlock, elseBlock ) );

        block.addStatement( new PrintStatement( new VariableReference( y ) ) );

        Block forBlock = new Block();
        SimpleFor forStatement = new SimpleFor( new IntegerConstant( 0 ), new IntegerConstant( 10 ), forBlock );
        block.addStatement( forStatement );
        forBlock.addStatement( new PrintStatement( new VariableReference( forStatement.getIterator() ) ) );


        // after fully constructed
        block.cooldown();

        String fullClassName = "com.jonathanolson.test.GeneticTestA";
        JavaStringSource source = createTestVoid( fullClassName, block );

        System.out.println( source + "\n\n" );
        System.out.println( "Valid: " + block.isValid() );

        CompilerUtils utils = new CompilerUtils();
        try {
            utils.compileSources( new JavaStringSource[]{source} );
            runTestVoid( utils.getCompiledClass( fullClassName ) );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
        catch( ClassNotFoundException e ) {
            e.printStackTrace();
        }
        catch( InvocationTargetException e ) {
            e.printStackTrace();
        }
        catch( NoSuchMethodException e ) {
            e.printStackTrace();
        }
        catch( InstantiationException e ) {
            e.printStackTrace();
        }
        catch( IllegalAccessException e ) {
            e.printStackTrace();
        }
    }

    public static JavaStringSource createTestVoid( String fullClassName, Block block ) {
        String source = "";
        String packageName = JavaStringSource.getPackageName( fullClassName );
        String className = JavaStringSource.getClassName( fullClassName );
        if ( packageName != null ) {
            source += "package " + packageName + ";\n";
        }
        source += "public class " + className + " {\n";
        source += "public void test() {\n";
        source += block.toString();
        source += "}\n";
        source += "}\n";
        return new JavaStringSource( fullClassName, source );
    }

    public static void runTestVoid( Class clazz ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor cons = clazz.getConstructor( new Class[]{} );
        Object ob = cons.newInstance();
        Method meth = clazz.getMethod( "test", new Class[]{} );
        meth.invoke( ob );
    }
}
