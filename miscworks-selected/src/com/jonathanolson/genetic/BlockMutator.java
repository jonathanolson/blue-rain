package com.jonathanolson.genetic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.jonathanolson.genetic.blockmutations.BlockMutation;
import com.jonathanolson.util.java.ast.statements.Block;

public class BlockMutator implements BlockMutation {

    private double totalWeight;

    private Set<BlockMutation> mutations;
    private Map<BlockMutation, Double> weights;

    public BlockMutator() {
        totalWeight = 0;
        mutations = new HashSet<BlockMutation>();
        weights = new HashMap<BlockMutation, Double>();
    }


    public void addMutation( BlockMutation mutation, double weight ) {
        mutations.add( mutation );
        weights.put( mutation, weight );
        calculateTotalWeight();
    }

    public void removeMutation( BlockMutation mutation ) {
        mutations.remove( mutation );
        weights.remove( mutation );
        calculateTotalWeight();
    }

    private void calculateTotalWeight() {
        totalWeight = 0;
        for ( BlockMutation mutation : mutations ) {
            totalWeight += weights.get( mutation );
        }
    }

    public void mutateBlock( Block block ) {
        if ( mutations.isEmpty() ) {
            return;
        }

        double val = Math.random() * totalWeight;

        for ( BlockMutation mutation : mutations ) {
            double weight = weights.get( mutation );

            val -= weight;
            if ( val <= 0 ) {
                mutation.mutateBlock( block );
                return;
            }
        }
    }
}
