package com.jonathanolson.genetic;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.util.java.CompilerUtils;
import com.jonathanolson.util.java.JavaStringSource;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.Equals;
import com.jonathanolson.util.java.ast.expressions.IntegerAdd;
import com.jonathanolson.util.java.ast.expressions.IntegerConstant;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.*;


/**
 * For reference:
 * <p/>
 * Created dir: /tmp/javatmp-25603
 * 36302
 * Created dir: /tmp/javatmp-50375
 * 1258
 */
public class GeneticTimingTest {
    public static void main( String[] args ) {
        /*
        long start = new Date().getTime();
        compileSeparateBlocks();
        long end = new Date().getTime();
        System.out.println( end - start );

        start = new Date().getTime();
        compileBlocksTogether();
        end = new Date().getTime();
        System.out.println( end - start );
        */

        compileMutatedBlocksTogether();
    }

    private static void compileMutatedBlocksTogether() {
        CompilerUtils utils = new CompilerUtils();

        BlockMutator mutator = new MatrixBlockMutator();

        Block block = createBlock();

        List<JavaStringSource> sources = new LinkedList<JavaStringSource>();

        for ( int i = 0; i < 750; i++ ) {
            if ( i % 20 == 0 ) {
                System.out.println( "Generating: " + i );
            }
            String fullClassName = "com.jonathanolson.test.GeneticTimingTest" + String.valueOf( i );
            JavaStringSource source = createTestVoid( fullClassName, block );
            sources.add( source );
            Block tmp = (Block) block.copy();
            tmp.cooldown();
            mutator.mutateBlock( tmp );
            tmp.cooldown();
            block = tmp;
        }

        try {
            utils.compileSources( sources );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    private static void compileSeparateBlocks() {
        CompilerUtils utils = new CompilerUtils();

        Block block = createBlock();

        for ( int i = 0; i < 200; i++ ) {
            String fullClassName = "com.jonathanolson.test.GeneticTimingTest" + String.valueOf( i );
            JavaStringSource source = createTestVoid( fullClassName, block );

            try {
                utils.compileSources( new JavaStringSource[]{source} );
            }
            catch( IOException e ) {
                e.printStackTrace();
            }
        }
    }

    private static void compileBlocksTogether() {
        CompilerUtils utils = new CompilerUtils();

        Block block = createBlock();

        List<JavaStringSource> sources = new LinkedList<JavaStringSource>();

        for ( int i = 0; i < 200; i++ ) {
            String fullClassName = "com.jonathanolson.test.GeneticTimingTest" + String.valueOf( i );
            JavaStringSource source = createTestVoid( fullClassName, block );
            sources.add( source );
        }

        try {
            utils.compileSources( sources );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    private static Block createBlock() {
        Block block = new Block();

        block.addStatement( new PrintStatement( new IntegerConstant( 5 ) ) );
        block.addStatement( new PrintStatement( new IntegerAdd( new IntegerConstant( 7 ), new IntegerConstant( 1 ) ) ) );

        Variable x = new Variable( Type.INTEGER );

        block.addVariable( x );

        block.addStatement( new SimpleAssignment( new VariableReference( x ), new IntegerConstant( 2 ) ) );
        block.addStatement( new PrintStatement( new VariableReference( x ) ) );


        Variable y = new Variable( Type.INTEGER );
        block.addVariable( y );

        Block ifBlock = new Block();
        ifBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( 0 ) ) );
        Block elseBlock = new Block();
        elseBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( -1 ) ) );

        block.addStatement( new If( new Equals( new VariableReference( x ), new IntegerConstant( 2 ) ), ifBlock, elseBlock ) );

        block.addStatement( new PrintStatement( new VariableReference( y ) ) );

        Block forBlock = new Block();
        SimpleFor forStatement = new SimpleFor( new IntegerConstant( 0 ), new IntegerConstant( 10 ), forBlock );
        block.addStatement( forStatement );
        forBlock.addStatement( new PrintStatement( new VariableReference( forStatement.getIterator() ) ) );


        // after fully constructed
        block.cooldown();
        return block;
    }

    public static JavaStringSource createTestVoid( String fullClassName, Block block ) {
        String source = "";
        String packageName = JavaStringSource.getPackageName( fullClassName );
        String className = JavaStringSource.getClassName( fullClassName );
        if ( packageName != null ) {
            source += "package " + packageName + ";\n";
        }
        // add imports!
        source += "import com.jonathanolson.genetic.library.MatrixTestBase;\n";

        source += "public class " + className + " extends MatrixTestBase {\n";

        source += "public void test() throws InterruptedException {\n";
        source += "startTime();\n";
        source += block.toString();
        source += "endTime();\n";
        source += "}\n";
        source += "}\n";
        return new JavaStringSource( fullClassName, source );
    }

    public static void runTestVoid( Class clazz ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor cons = clazz.getConstructor( new Class[]{} );
        Object ob = cons.newInstance();
        Method meth = clazz.getMethod( "test", new Class[]{} );
        meth.invoke( ob );
    }
}