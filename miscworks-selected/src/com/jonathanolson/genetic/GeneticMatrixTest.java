package com.jonathanolson.genetic;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.util.*;

import com.jonathanolson.genetic.library.MatrixTestBase;
import com.jonathanolson.util.FileUtils;
import com.jonathanolson.util.ListUtils;
import com.jonathanolson.util.java.CompilerUtils;
import com.jonathanolson.util.java.JavaStringSource;
import com.jonathanolson.util.java.ast.ASTUtils;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.Equals;
import com.jonathanolson.util.java.ast.expressions.IntegerConstant;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.If;
import com.jonathanolson.util.java.ast.statements.SimpleAssignment;
import com.jonathanolson.util.java.ast.statements.SimpleFor;

public class GeneticMatrixTest {

    private MatrixBlockMutator mutator;

    private List<Info> infos;

    private Info bestInfo = null;

    private CompilerUtils compiler;
    private MatrixTestBase bestOb;

    private static final int MAX_INFOS = 500;

    private File imageDir;
    private int imageCount = 0;

    public GeneticMatrixTest() {
        mutator = new MatrixBlockMutator();
        infos = new LinkedList<Info>();
        compiler = new CompilerUtils();
        imageDir = new File( System.getProperty( "java.io.tmpdir" ), "jimages-" + String.valueOf( new Date().getTime() ) );
        imageDir.mkdirs();
        for ( File file : imageDir.listFiles() ) {
            file.delete();
        }
    }

    public static void main( String[] args ) {
        GeneticMatrixTest test = new GeneticMatrixTest();

        for ( int i = 0; i < 100000; i++ ) {
            test.cycle();
        }
    }

    public String getFilenamePrefix() {
        String ret = "image-";
        if ( imageCount < 10 ) {
            ret += "0";
        }
        if ( imageCount < 100 ) {
            ret += "0";
        }
        if ( imageCount < 1000 ) {
            ret += "0";
        }
        ret += String.valueOf( imageCount );
        return ret;
    }

    public void cycle() {
        double oldBestVal = -10000000;
        if ( bestInfo != null ) {
            oldBestVal = bestInfo.getScore();
        }
        thinBlocks();
        List<Block> newBlocks = generateNewBlocks();
        handleNewBlocks( newBlocks );

        System.out.println( "\n\nCompleted cycle" );
        //System.out.println( "Best code:\n" + bestInfo.getSource().getSource() );
        System.out.println( "Best score: " + bestInfo.getScore() );
        if ( bestInfo.getScore() > oldBestVal ) {
            System.out.println( "Outputting best PPM" );
            try {
                FileUtils.overwriteStringToFile( bestInfo.getSource().getSource(), new File( imageDir, getFilenamePrefix() + ".java" ) );
                FileUtils.overwriteStringToFile( bestOb.getPPMString(), new File( imageDir, getFilenamePrefix() + ".ppm" ) );
                imageCount++;
            }
            catch( IOException e ) {
                e.printStackTrace();
            }
        }
        if ( !GeneticConstants.DEBUG ) {
            compiler.clearDir();
        }
    }

    private void thinBlocks() {
        double totalScore = 0;
        for ( Info info : infos ) {
            totalScore += info.getScore();
        }
        double averageScore = totalScore / (double) infos.size();

        ListIterator iter = infos.listIterator();

        while ( iter.hasNext() ) {
            Info info = (Info) iter.next();

            if ( info.getScore() < averageScore && infos.size() > MAX_INFOS && Math.random() < 0.1 ) {
                iter.remove();
            }
        }
    }

    private List<Block> generateNewBlocks() {
        List<Block> ret = new LinkedList<Block>();

        for ( int i = 0; i < 30; i++ ) {
            if ( Math.random() < 0.5 || infos.isEmpty() ) {
                Block block = createRootBlock();
                for ( int j = 0; j < 100 && Math.random() < 0.98; j++ ) {
                    mutateBlock( block );
                    if ( GeneticConstants.DEBUG ) {
                        boolean passed = false;
                        try {
                            ASTUtils.checkVariables( block );
                            passed = block.isValid();
                        }
                        catch( RuntimeException e ) {
                            e.printStackTrace();
                        }
                        finally {
                            if ( !passed ) {
                                warnInvalidBlock( block );
                            }
                            else {
                                System.out.println( "PASSED:\n" + block.toString() + "/PASSED\n" );
                            }
                        }
                    }
                }
                if ( block.isValid() ) {
                    ret.add( block );
                }
                else {
                    warnInvalidBlock( block );
                }
            }
            else {
                Info info = ListUtils.pickFromList( infos );
                if ( GeneticConstants.DEBUG ) {
                    System.out.println( "Mutating from " + info.getSource().getFullClassName() );
                }
                Block block = info.getBlock();
                Block newBlock = (Block) block.copy();
                newBlock.cooldown();
                for ( int j = 0; j < 20; j++ ) {
                    mutateBlock( newBlock );
                    if ( GeneticConstants.DEBUG ) {
                        boolean passed = false;
                        try {
                            ASTUtils.checkVariables( newBlock );
                            passed = newBlock.isValid();
                        }
                        catch( RuntimeException e ) {
                            e.printStackTrace();
                        }
                        finally {
                            if ( !passed ) {
                                warnInvalidBlock( newBlock );
                            }
                            else {
                                System.out.println( "PASSED:\n" + newBlock.toString() + "/PASSED\n" );
                            }
                        }
                    }
                }
                if ( newBlock.isValid() ) {
                    ret.add( newBlock );
                }
                else {
                    warnInvalidBlock( newBlock );
                }
            }
        }

        return ret;
    }

    private synchronized void warnInvalidBlock( Block block ) {
        JavaStringSource tempSource = createSource( "ErroredClass", block );
        System.out.println( tempSource.toString() );
        System.out.println( "INVALID BLOCK???" );
        System.exit( 1 );
    }

    private Block createRootBlock() {
        Block block = new Block();

        /*
        Variable v1 = new Variable( Type.INTEGER );
        block.addVariable( v1 );
        Variable v2 = new Variable( Type.INTEGER );
        block.addVariable( v2 );

        block.addStatement( new SimpleAssignment( new VariableReference( v1 ), new IntegerConstant( 0 ) ) );
        block.addStatement( new SimpleAssignment( new VariableReference( v2 ), new IntegerConstant( 0 ) ) );

        */


        //block.addStatement( new PrintStatement( new IntegerConstant( 5 ) ) );
        //block.addStatement( new PrintStatement( new IntegerAdd( new IntegerConstant( 7 ), new IntegerConstant( 1 ) ) ) );

        Variable x = new Variable( Type.INTEGER );

        block.addVariable( x );

        block.addStatement( new SimpleAssignment( new VariableReference( x ), new IntegerConstant( 2 ) ) );
        //block.addStatement( new PrintStatement( new VariableReference( x ) ) );


        Variable y = new Variable( Type.INTEGER );
        block.addVariable( y );

        Block ifBlock = new Block();
        ifBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( 0 ) ) );
        Block elseBlock = new Block();
        elseBlock.addStatement( new SimpleAssignment( new VariableReference( y ), new IntegerConstant( -1 ) ) );

        block.addStatement( new If( new Equals( new VariableReference( x ), new IntegerConstant( 2 ) ), ifBlock, elseBlock ) );

        //block.addStatement( new PrintStatement( new VariableReference( y ) ) );

        Block forBlock = new Block();
        SimpleFor forStatement = new SimpleFor( new IntegerConstant( 0 ), new IntegerConstant( 10 ), forBlock );
        block.addStatement( forStatement );
        //forBlock.addStatement( new PrintStatement( new VariableReference( forStatement.getIterator() ) ) );


        block.cooldown();

        return block;
    }

    private void mutateBlock( Block block ) {
        mutator.mutateBlock( block );
        block.cooldown();
    }

    private static class Info implements Comparable {

        private Block block;

        private double score;

        private JavaStringSource source;

        public Info( Block block, JavaStringSource source, double score ) {
            this.block = block;
            this.source = source;
            this.score = score;
        }

        public Block getBlock() {
            return block;
        }


        public void setBlock( Block block ) {
            this.block = block;
        }

        public double getScore() {
            return score;
        }

        public void setScore( double score ) {
            this.score = score;
        }

        public JavaStringSource getSource() {
            return source;
        }

        /**
         * this is the opposite of regular comparisons
         */
        public int compareTo( Object obj ) {
            Info other = (Info) obj;
            if ( score == other.score ) {
                return 0;
            }
            if ( score < other.score ) {
                return 1;
            }
            else {
                return -1;
            }
        }
    }

    private void submitScore( Block block, JavaStringSource source, double score, MatrixTestBase ob ) {
        Info info = new Info( block, source, score );
        infos.add( info );
        if ( bestInfo == null || bestInfo.getScore() < info.getScore() ) {
            bestInfo = info;
            bestOb = ob;
        }
    }

    private static int count = 0;

    private void handleNewBlocks( List<Block> blocks ) {
        List<JavaStringSource> sources = new LinkedList<JavaStringSource>();

        Map<JavaStringSource, Block> map = new HashMap<JavaStringSource, Block>();

        for ( Block block : blocks ) {
            String fullClassName = "com.jonathanolson.test.MatrixTest" + String.valueOf( count++ );
            if ( GeneticConstants.DEBUG ) {
                System.out.println( "Creating source for " + fullClassName );
            }
            JavaStringSource source = createSource( fullClassName, block );
            sources.add( source );
            map.put( source, block );
        }

        try {
            compiler.compileSources( sources );
        }
        catch( IOException e ) {
            e.printStackTrace();
            //return;
            System.exit( 1 );
        }

        for ( JavaStringSource source : sources ) {

            boolean finished = false;
            try {
                Class clazz = compiler.getCompiledClass( source.getFullClassName() );

                Constructor cons = clazz.getConstructor( new Class[]{} );
                Object rawOb = cons.newInstance();
                final MatrixTestBase ob = (MatrixTestBase) rawOb;

                Thread thread = new Thread() {
                    public void run() {
                        try {
                            ob.test();
                        }
                        catch( InterruptedException e ) {
                        }
                    }
                };

                thread.start();

                thread.join( 1000 );

                synchronized( this ) {

                    if ( !thread.isAlive() ) {
                        double score = ob.score();

                        submitScore( map.get( source ), source, score, ob );

                        finished = true;
                    }
                    else {
                        thread.interrupt();
                    }
                }
            }
            catch( MalformedURLException e ) {
                e.printStackTrace();
            }
            catch( ClassNotFoundException e ) {
                e.printStackTrace();
            }
            catch( InvocationTargetException e ) {
                e.printStackTrace();
            }
            catch( NoSuchMethodException e ) {
                e.printStackTrace();
            }
            catch( IllegalAccessException e ) {
                e.printStackTrace();
            }
            catch( InstantiationException e ) {
                e.printStackTrace();
            }
            catch( InterruptedException e ) {
                System.out.println( "***** Interrupted!!!" );
                //e.printStackTrace();
            }
            finally {
                if ( !finished ) {
                    System.out.println( "* block did not finish execution" );
                    //System.out.println( "************Following code errored:" );
                    //System.out.println( source.getSource() );
                    //System.out.println( "************End errored source" );
                }
            }
        }

    }

    private JavaStringSource createSource( String fullClassName, Block block ) {
        String source = "";
        String packageName = JavaStringSource.getPackageName( fullClassName );
        String className = JavaStringSource.getClassName( fullClassName );
        if ( packageName != null ) {
            source += "package " + packageName + ";\n";
        }

        // add imports!
        source += "import com.jonathanolson.genetic.library.MatrixTestBase;\n";

        source += "public class " + className + " extends MatrixTestBase {\n";

        source += "public void test() throws InterruptedException {\n";
        source += "startTime();\n";
        source += block.toString();
        source += "endTime();\n";
        source += "}\n";
        source += "}\n";
        return new JavaStringSource( fullClassName, source );
    }

}
