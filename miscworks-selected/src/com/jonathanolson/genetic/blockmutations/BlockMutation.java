package com.jonathanolson.genetic.blockmutations;

import com.jonathanolson.util.java.ast.statements.Block;

public interface BlockMutation {

    public void mutateBlock( Block block );

}
