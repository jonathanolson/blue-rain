package com.jonathanolson.genetic.blockmutations;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.statements.Block;

public class AddVariableMutation implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding new integer variable" );
        }

        MutationUtils.selectRandomBlock( block ).addVariable( new Variable( Type.INTEGER ) );
    }

}
