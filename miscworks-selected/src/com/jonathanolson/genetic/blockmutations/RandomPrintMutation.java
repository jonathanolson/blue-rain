package com.jonathanolson.genetic.blockmutations;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.java.ast.StatementContext;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.PrintStatement;
import com.jonathanolson.util.math.RandomUtils;

public class RandomPrintMutation implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding random print" );
        }

        Block subBlock = MutationUtils.selectRandomBlock( block );

        int size = subBlock.getStatements().size();
        int index = RandomUtils.nextInt( size + 1 );

        StatementContext context = subBlock.getContextAtIndex( index );

        subBlock.insertStatement( new PrintStatement( MutationUtils.createIntegerExpression( context ) ), index );
    }

}
