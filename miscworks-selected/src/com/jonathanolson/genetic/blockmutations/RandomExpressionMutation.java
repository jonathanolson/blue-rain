package com.jonathanolson.genetic.blockmutations;

import java.util.List;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.ListUtils;
import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.statements.Block;

public class RandomExpressionMutation implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### mutating random expression" );
        }

        List<Node> options = MutationUtils.getNodesWithSubexpressions( block );

        if ( options.isEmpty() ) {
            //System.out.println( "### no expressions to mutate" );
            return;
        }

        Node baseNode = ListUtils.pickFromList( options );

        List<Expression> subexpressions = MutationUtils.getNodeSubexpressions( baseNode );

        if ( subexpressions.isEmpty() ) {
            throw new RuntimeException( "subexpressions should never be empty if we are here" );
        }

        Expression subexpression = ListUtils.pickFromList( subexpressions );

        Expression newExpression = MutationUtils.createExpression( subexpression.getType(), MutationUtils.getParentStatementOfNode( subexpression ).getContext() );

        //System.out.println( "###    mutating [" + subexpression + "] to [" + newExpression + "]" );

        try {
            baseNode.replaceChild( subexpression, newExpression );
        }
        catch( InvalidChildReplacement e ) {
            e.printStackTrace();
        }
        catch( ChildDoesNotExist e ) {
            e.printStackTrace();
        }

    }

}
