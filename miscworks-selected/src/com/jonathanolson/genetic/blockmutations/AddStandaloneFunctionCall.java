package com.jonathanolson.genetic.blockmutations;

import java.util.List;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.java.ast.Function;
import com.jonathanolson.util.java.ast.StatementContext;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.expressions.FunctionCall;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.ExpressionStatement;
import com.jonathanolson.util.math.RandomUtils;

public class AddStandaloneFunctionCall implements BlockMutation {

    private Function function;

    public AddStandaloneFunctionCall( Function function ) {
        this.function = function;
    }

    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding new function call for " + function.getName() );
        }

        block = MutationUtils.selectRandomBlock( block );

        int size = block.getStatements().size();
        int index = RandomUtils.nextInt( size + 1 );

        StatementContext context = block.getContextAtIndex( index );

        List<Expression> args = MutationUtils.createFunctionArguments( function, context );

        block.insertStatement( new ExpressionStatement( new FunctionCall( function, args ) ), index );
    }
}
