package com.jonathanolson.genetic.blockmutations;

import java.util.HashSet;
import java.util.Set;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.SetUtils;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.SimpleAssignment;
import com.jonathanolson.util.math.RandomUtils;

public class RandomIntegerAssignment implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding random integer assign" );
        }

        block = MutationUtils.selectRandomBlock( block );

        Set<Variable> options = new HashSet<Variable>();
        Set<Variable> integerOptions = new HashSet<Variable>();

        options.addAll( block.getContext().getUninitializedVariables() );
        options.addAll( block.getContext().getInitializedVariables() );
        options.addAll( block.getVariables() );

        for ( Variable option : options ) {
            if ( option.getType() == Type.INTEGER && option.isMutationSettable() ) {
                integerOptions.add( option );
            }
        }

        if ( options.size() == 0 ) {
            Variable newVar = new Variable( Type.INTEGER );
            block.addVariable( newVar );
            integerOptions.add( newVar );
        }

        Variable var = SetUtils.pickFromSet( integerOptions );

        if ( var == null ) {
            return;
        }

        int index = RandomUtils.nextInt( block.getStatements().size() + 1 );
        block.insertStatement( new SimpleAssignment( new VariableReference( var ), MutationUtils.createIntegerExpression( block.getContextAtIndex( index ) ) ), index );
    }

}
