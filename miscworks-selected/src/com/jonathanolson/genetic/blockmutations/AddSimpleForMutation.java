package com.jonathanolson.genetic.blockmutations;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.java.ast.StatementContext;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.SimpleFor;
import com.jonathanolson.util.math.RandomUtils;

public class AddSimpleForMutation implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding for statement" );
        }

        Block subBlock = MutationUtils.selectRandomBlock( block );

        int size = subBlock.getStatements().size();
        int index = RandomUtils.nextInt( size + 1 );

        StatementContext context = subBlock.getContextAtIndex( index );

        Expression startVal = MutationUtils.createIntegerExpression( context );
        Expression endVal = MutationUtils.createIntegerExpression( context );

        Block forBlock = new Block();

        subBlock.insertStatement( new SimpleFor( startVal, endVal, forBlock ), index );
    }

}