package com.jonathanolson.genetic.blockmutations;

import com.jonathanolson.genetic.GeneticConstants;
import com.jonathanolson.genetic.MutationUtils;
import com.jonathanolson.util.java.ast.StatementContext;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.If;
import com.jonathanolson.util.math.RandomUtils;

public class AddIfMutation implements BlockMutation {
    public void mutateBlock( Block block ) {
        if ( GeneticConstants.DEBUG ) {
            System.out.println( "### adding if statement" );
        }

        Block subBlock = MutationUtils.selectRandomBlock( block );

        int size = subBlock.getStatements().size();
        int index = RandomUtils.nextInt( size + 1 );

        StatementContext context = subBlock.getContextAtIndex( index );

        Expression testExpression = MutationUtils.createBooleanExpression( context );
        Block ifBlock = new Block();
        Block elseBlock = null;

        if ( Math.random() < 0.5 ) {
            elseBlock = new Block();
        }

        subBlock.insertStatement( new If( testExpression, ifBlock, elseBlock ), index );
    }

}
