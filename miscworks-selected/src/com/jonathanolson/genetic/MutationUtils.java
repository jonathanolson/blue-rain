package com.jonathanolson.genetic;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jonathanolson.util.ListUtils;
import com.jonathanolson.util.SetUtils;
import com.jonathanolson.util.java.ast.*;
import com.jonathanolson.util.java.ast.expressions.*;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.CompoundStatement;
import com.jonathanolson.util.java.ast.statements.Statement;

public class MutationUtils {

    public static List<Expression> createFunctionArguments( Function function, StatementContext context ) {
        List<Expression> ret = new LinkedList<Expression>();

        for ( Type type : function.getSpecification() ) {
            ret.add( createExpression( type, context ) );
        }

        return ret;
    }

    public static Expression createExpression( Type type, StatementContext context ) {
        if ( type == Type.INTEGER ) {
            return createIntegerExpression( context );
        }
        else if ( type == Type.BOOLEAN ) {
            return createBooleanExpression( context );
        }

        throw new RuntimeException( "Unhandled type" );
    }

    public static Expression createBooleanExpression( StatementContext context ) {
        return new Equals( createIntegerExpression( context ), createIntegerExpression( context ) );
    }

    public static Expression createIntegerExpression( StatementContext context ) {
        double val = Math.random();
        Variable var = null;

        if ( context != null ) {
            Set<Variable> options = new HashSet<Variable>();

            for ( Variable variable : context.getInitializedVariables() ) {
                if ( variable.getType() == Type.INTEGER ) {
                    options.add( variable );
                }
            }

            var = SetUtils.pickFromSet( options );
        }

        if ( val < 0.7 && var != null ) {
            return new VariableReference( var );
        }
        else if ( val < 0.8 ) {
            return createRandomIntegerConstant();
        }
        else if ( val < 0.9 ) {
            return new IntegerMultiply( createIntegerExpression( context ), createIntegerExpression( context ) );
        }
        else {
            return new IntegerAdd( createIntegerExpression( context ), createIntegerExpression( context ) );
        }
    }

    public static Expression createRandomIntegerConstant() {
        return new IntegerConstant( (int) ( Math.random() * 1000 - 10 ) );
    }

    public static Block selectRandomBlock( Block block ) {
        List<Block> subBlocks = new LinkedList<Block>();
        for ( Node node : block.getChildren() ) {
            if ( node instanceof Block ) {
                subBlocks.add( (Block) node );
            }
            else if ( node instanceof CompoundStatement ) {
                for ( Node no : node.getChildren() ) {
                    if ( no instanceof Block ) {
                        subBlocks.add( (Block) no );
                    }
                }
            }
        }
        if ( subBlocks.isEmpty() ) {
            return block;
        }

        int numSubBlocks = subBlocks.size();

        double val = Math.random();

        if ( val < Math.sqrt( 1.0 / (double) ( numSubBlocks + 1 ) ) ) {
            return block;
        }
        else {
            return selectRandomBlock( ListUtils.pickFromList( subBlocks ) );
        }
    }

    public static List<Node> getNodesWithSubexpressions( Node node ) {
        List<Node> ret = new LinkedList<Node>();

        if ( node.isMutatable() ) {
            List<Expression> exprs = getNodeSubexpressions( node );
            if ( !exprs.isEmpty() ) {
                ret.add( node );
            }
        }

        for ( Node subNode : node.getChildren() ) {
            if ( subNode.isMutatable() ) {
                ret.addAll( getNodesWithSubexpressions( subNode ) );
            }
        }

        return ret;
    }

    // only return mutatable ones!
    public static List<Expression> getNodeSubexpressions( Node node ) {
        List<Expression> ret = new LinkedList<Expression>();

        for ( Node subNode : node.getChildren() ) {
            if ( subNode.isMutatable() && subNode instanceof Expression && ( (Expression) ( subNode ) ).getType() != Type.VOID ) {
                ret.add( (Expression) subNode );
            }
        }

        return ret;
    }

    public static Statement getParentStatementOfNode( Node node ) {
        Node parent = node.getParent();

        if ( parent == null ) {
            return null;
        }

        if ( parent instanceof Statement ) {
            return (Statement) parent;
        }
        else {
            return getParentStatementOfNode( parent );
        }
    }

}
