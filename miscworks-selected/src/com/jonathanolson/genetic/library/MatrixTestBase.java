package com.jonathanolson.genetic.library;

import java.util.Date;

import com.jonathanolson.util.math.MathUtils;

public abstract class MatrixTestBase {

    private int[][][] matrix;

    private static final int ROWS = 480;
    private static final int COLS = 640;

    private static final int DEFAULT = 128;

    private long start;
    private long end;

    public MatrixTestBase() {

        matrix = new int[ROWS][COLS][3];

        for ( int row = 0; row < ROWS; row++ ) {
            for ( int col = 0; col < COLS; col++ ) {
                for ( int channel = 0; channel < 3; channel++ ) {
                    matrix[row][col][channel] = DEFAULT;
                }
            }
        }

    }

    public abstract void test() throws InterruptedException;

    public void startTime() {
        start = new Date().getTime();
    }

    public void endTime() {
        end = new Date().getTime();
    }

    public long getRuntime() {
        return end - start;
    }

    public double score() {
        double ret = 0;
        ret += (double) countNonDefault();

        ret -= getRuntime() / 10;

        ret += scoreChanges() / 5000;

        ret += scoreContrast() / 500;

        return ret;
    }

    public void setValue( int row, int col, int channel, int val ) {
        row = MathUtils.integerModulus( row, ROWS );
        col = MathUtils.integerModulus( col, COLS );
        channel = MathUtils.integerModulus( channel, 3 );

        val = MathUtils.integerModulus( val, 256 );

        matrix[row][col][channel] = val;
    }

    public String toString() {
        String ret = "";
        for ( int i = 0; i < matrix.length; i++ ) {
            int[][] row = matrix[i];
            for ( int j = 0; j < row.length; j++ ) {
                int[] val = row[j];
                if ( j > 0 ) {
                    ret += " ";
                }
                ret += "(" + val[0] + "," + val[1] + "," + val[2] + ")";
            }
            ret += "\n";
        }
        return ret;
    }

    public String getPPMString() {
        StringBuffer ret = new StringBuffer();
        ret.append( "P3\n" + COLS + " " + ROWS + " 256\n" );
        for ( int i = 0; i < matrix.length; i++ ) {
            int[][] row = matrix[i];
            for ( int j = 0; j < row.length; j++ ) {
                int[] val = row[j];
                if ( j > 0 ) {
                    ret.append( " " );
                }
                ret.append( val[0] + " " + val[1] + " " + val[2] );
            }
            ret.append( "\n" );
        }
        return ret.toString();
    }

    public int countNonDefault() {
        int ret = 0;
        for ( int row = 0; row < ROWS; row++ ) {
            for ( int col = 0; col < COLS; col++ ) {
                for ( int channel = 0; channel < 3; channel++ ) {
                    if ( matrix[row][col][channel] != DEFAULT ) {
                        ret++;
                    }
                }
            }
        }
        return ret;
    }

    public double scoreChanges() {
        int ideal = 4;
        int colorideal = 16;
        double hits = 0;
        for ( int channel = 0; channel < 3; channel++ ) {
            for ( int row = 0; row < ROWS; row++ ) {
                int cur = matrix[row][0][channel];
                for ( int col = 0; col < COLS; col++ ) {
                    int next = matrix[row][col][channel];
                    hits -= Math.abs( ideal - Math.abs( cur - next ) );
                    cur = next;
                }
            }
            for ( int col = 0; col < COLS; col++ ) {
                int cur = matrix[0][col][channel];
                for ( int row = 0; row < ROWS; row++ ) {
                    int next = matrix[row][col][channel];
                    hits -= Math.abs( ideal - Math.abs( cur - next ) );
                    cur = next;
                }
            }
        }

        // color v color contrast
        for ( int row = 0; row < ROWS; row++ ) {
            for ( int col = 0; col < COLS; col++ ) {
                for ( int channel = 0; channel < 3; channel++ ) {
                    int[] dat = matrix[row][col];
                    hits -= Math.abs( colorideal - Math.abs( dat[0] - dat[1] ) ) / 10;
                    hits -= Math.abs( colorideal - Math.abs( dat[1] - dat[2] ) ) / 10;
                    hits -= Math.abs( colorideal - Math.abs( dat[0] - dat[2] ) ) / 10;
                }
            }
        }

        return hits;
    }

    public double scoreContrast() {
        double lightCount = 1;
        double darkCount = 1;
        for ( int row = 0; row < ROWS; row++ ) {
            for ( int col = 0; col < COLS; col++ ) {
                int[] dat = matrix[row][col];

                double val = ( dat[0] + dat[1] + dat[2] ) / ( 3 * 255 );

                lightCount += val * val;
                darkCount += ( 1 - val ) * ( 1 - val );
            }
        }

        return 2 * darkCount + lightCount - darkCount / lightCount - lightCount / darkCount;
    }

}
