package com.jonathanolson.genetic;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;

import com.jonathanolson.genetic.blockmutations.*;
import com.jonathanolson.genetic.library.MatrixTestBase;
import com.jonathanolson.util.java.CompilerUtils;
import com.jonathanolson.util.java.JavaStringSource;
import com.jonathanolson.util.java.ast.Function;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.expressions.Equals;
import com.jonathanolson.util.java.ast.expressions.IntegerAdd;
import com.jonathanolson.util.java.ast.expressions.IntegerConstant;
import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.If;
import com.jonathanolson.util.java.ast.statements.PrintStatement;
import com.jonathanolson.util.java.ast.statements.SimpleAssignment;


public class GeneticMatrixMutationTest {
    public static void main( String[] args ) {

        Function function = new Function( "setValue", Arrays.asList( Type.INTEGER, Type.INTEGER, Type.INTEGER, Type.INTEGER ), Type.VOID );

        BlockMutator mutator = new BlockMutator();

        mutator.addMutation( new AddVariableMutation(), 2 );
        mutator.addMutation( new RandomExpressionMutation(), 20 );
        mutator.addMutation( new AddIfMutation(), 7 );
        mutator.addMutation( new AddSimpleForMutation(), 7 );
        mutator.addMutation( new RandomIntegerAssignment(), 30 );
        //mutator.addMutation( new RandomPrintMutation(), 5 );
        mutator.addMutation( new AddStandaloneFunctionCall( function ), 7 );

        Block block = new Block();

        block.addStatement( new PrintStatement( new IntegerConstant( 5 ) ) );
        block.addStatement( new PrintStatement( new IntegerAdd( new IntegerConstant( 7 ), new IntegerConstant( 1 ) ) ) );

        Variable x = new Variable( Type.INTEGER, "x" );
        Variable y = new Variable( Type.INTEGER, "y" );

        block.addVariable( x );
        block.addVariable( y );

        block.addStatement( new SimpleAssignment( new VariableReference( x ), new IntegerConstant( 2 ) ) );
        block.addStatement( new PrintStatement( new VariableReference( x ) ) );

        block.addStatement( new SimpleAssignment( new VariableReference( y ), new VariableReference( x ) ) );

        block.addStatement( new If( new Equals( new VariableReference( x ), new VariableReference( y ) ), new Block(), new Block() ) );

        // after fully constructed
        block.cooldown();

        Block testBlock = (Block) block.copy();
        for ( int i = 0; i < 500; i++ ) {
            testBlock.cooldown();
            boolean success = attemptBlock( testBlock );
            if ( success ) {
                block = testBlock;
                System.out.println( "SUCCESS!!!" );
            }
            testBlock = (Block) block.copy();
            testBlock.cooldown();

            mutator.mutateBlock( testBlock );
            //MutationUtils.mutateRandomBlock( testBlock );
        }
    }

    public static boolean attemptBlock( Block block ) {
        boolean ret = false;

        String fullClassName = "com.jonathanolson.test.GeneticTestB";

        JavaStringSource source = createTestVoid( fullClassName, block );

        //System.out.println( source + "\n\n" );
        boolean valid = block.isValid();
        System.out.println( "*** Valid: " + valid );

        if ( !valid ) {
            return false;
        }

        CompilerUtils utils = new CompilerUtils();
        try {
            System.out.println( "*** Compiling" );
            utils.compileSources( new JavaStringSource[]{source} );
            System.out.println( "*** Running" );
            runTest( utils.getCompiledClass( fullClassName ) );
            System.out.println( "*** Finished running" );
            ret = true;
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
        catch( ClassNotFoundException e ) {
            e.printStackTrace();
        }
        catch( InvocationTargetException e ) {
            e.printStackTrace();
        }
        catch( NoSuchMethodException e ) {
            e.printStackTrace();
        }
        catch( InstantiationException e ) {
            e.printStackTrace();
        }
        catch( IllegalAccessException e ) {
            e.printStackTrace();
        }
        catch( Exception e ) {
            e.printStackTrace();
        }

        return ret;
    }

    public static JavaStringSource createTestVoid( String fullClassName, Block block ) {
        String source = "";
        String packageName = JavaStringSource.getPackageName( fullClassName );
        String className = JavaStringSource.getClassName( fullClassName );
        if ( packageName != null ) {
            source += "package " + packageName + ";\n";
        }

        // add imports!
        source += "import com.jonathanolson.genetic.library.MatrixTestBase;\n";

        source += "public class " + className + " extends MatrixTestBase {\n";
        source += "public void test() {\n";
        source += block.toString();

        source += "}\n";
        source += "}\n";
        return new JavaStringSource( fullClassName, source );
    }

    public static void runTest( Class clazz ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor cons = clazz.getConstructor( new Class[]{} );
        Object rawOb = cons.newInstance();
        /*
        Method meth = clazz.getMethod( "test", new Class[]{} );
        meth.invoke( ob );
        */
        MatrixTestBase ob = (MatrixTestBase) rawOb;

        try {
            ob.test();
        }
        catch( InterruptedException e ) {
            e.printStackTrace();
        }

        System.out.println( "Data:" );

        System.out.println( ob );

    }
}