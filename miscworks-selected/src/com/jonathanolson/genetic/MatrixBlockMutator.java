package com.jonathanolson.genetic;

import java.util.Arrays;

import com.jonathanolson.genetic.blockmutations.*;
import com.jonathanolson.util.java.ast.Function;
import com.jonathanolson.util.java.ast.Type;

public class MatrixBlockMutator extends BlockMutator {
    private Function function;

    public MatrixBlockMutator() {
        super();

        function = new Function( "setValue", Arrays.asList( Type.INTEGER, Type.INTEGER, Type.INTEGER, Type.INTEGER ), Type.VOID );

        addMutation( new AddVariableMutation(), 2 );
        addMutation( new RandomExpressionMutation(), 35 );
        addMutation( new AddIfMutation(), 7 );
        addMutation( new AddSimpleForMutation(), 7 );
        addMutation( new RandomIntegerAssignment(), 25 );
        addMutation( new AddStandaloneFunctionCall( function ), 20 );
        //addMutation( new RandomPrintMutation(), 5 );
    }

    public Function getFunction() {
        return function;
    }
}
