package com.jonathanolson.words;

import java.util.*;

public class Letter {
    public static final String BLANK = "";
    public static final Map<String, Integer> values = new HashMap<String, Integer>();
    public static final Map<String, Integer> quantities = new HashMap<String, Integer>();
    public static final Map<Location, TileScore> tileScores = new HashMap<Location, TileScore>();

    private static final Random random = new Random( System.currentTimeMillis() );

    public static final String[] letters = new String[]{
            "", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
    };
    public static final int[] letterQuantArray = new int[]{
            2, 9, 2, 2, 5, 13, 2, 3, 4, 8, 1, 1, 4, 2, 5, 8, 2, 1, 6, 6, 7, 4, 2, 2, 1, 2, 1
    };

    public static String drawEstimated() {
        int r = random.nextInt( 104 );
        for ( int i = 0; i < letterQuantArray.length; i++ ) {
            r -= letterQuantArray[i];
            if ( r < 0 ) {
                return letters[i];
            }
        }
        throw new RuntimeException( "aiiii!" );
    }

    public static TileOptions fullDrawEstimated() {
        List<String> tiles = new LinkedList<String>();
        int blanks = 0;
        for ( int i = 0; i < 7; i++ ) {
            String letter = drawEstimated();
            if ( letter.equals( "" ) ) {
                blanks++;
            }
            else {
                tiles.add( letter );
            }
        }
        return new TileOptions( tiles.toArray( new String[]{} ), blanks );
    }

    public static void addLetter( String letter, int value, int quantity ) {
        values.put( letter, value );
        quantities.put( letter, quantity );
    }

    static {
        // todo: (refactor) replace with letters and quant array
        addLetter( "", 0, 2 );
        addLetter( "a", 1, 9 );
        addLetter( "b", 4, 2 );
        addLetter( "c", 4, 2 );
        addLetter( "d", 2, 5 );
        addLetter( "e", 1, 13 );
        addLetter( "f", 4, 2 );
        addLetter( "g", 3, 3 );
        addLetter( "h", 3, 4 );
        addLetter( "i", 1, 8 );
        addLetter( "j", 10, 1 );
        addLetter( "k", 5, 1 );
        addLetter( "l", 2, 4 );
        addLetter( "m", 4, 2 );
        addLetter( "n", 2, 5 );
        addLetter( "o", 1, 8 );
        addLetter( "p", 4, 2 );
        addLetter( "q", 10, 1 );
        addLetter( "r", 1, 6 );
        addLetter( "s", 1, 6 );
        addLetter( "t", 1, 7 );
        addLetter( "u", 2, 4 );
        addLetter( "v", 5, 2 );
        addLetter( "w", 4, 2 );
        addLetter( "x", 8, 1 );
        addLetter( "y", 3, 2 );
        addLetter( "z", 10, 1 );

        for ( int i = 0; i < WordsConstants.BOARD_SIZE; i++ ) {
            for ( int j = 0; j < WordsConstants.BOARD_SIZE; j++ ) {
                tileScores.put( new Location( i, j ), TileScore.REGULAR );
            }
        }

        tileScores.put( new Location( 0, 3 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 0, 11 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 14, 3 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 14, 11 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 3, 0 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 11, 0 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 3, 14 ), TileScore.TRIPLE_WORD );
        tileScores.put( new Location( 11, 14 ), TileScore.TRIPLE_WORD );

        tileScores.put( new Location( 0, 6 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 0, 8 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 14, 6 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 14, 8 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 6, 0 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 8, 0 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 6, 14 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 8, 14 ), TileScore.TRIPLE_LETTER );

        tileScores.put( new Location( 3, 3 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 11, 3 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 3, 11 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 11, 11 ), TileScore.TRIPLE_LETTER );

        tileScores.put( new Location( 5, 5 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 9, 5 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 5, 9 ), TileScore.TRIPLE_LETTER );
        tileScores.put( new Location( 9, 9 ), TileScore.TRIPLE_LETTER );

        tileScores.put( new Location( 1, 5 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 1, 9 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 13, 5 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 13, 9 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 5, 1 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 9, 1 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 5, 13 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 9, 13 ), TileScore.DOUBLE_WORD );

        tileScores.put( new Location( 3, 7 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 7, 3 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 7, 11 ), TileScore.DOUBLE_WORD );
        tileScores.put( new Location( 11, 7 ), TileScore.DOUBLE_WORD );

        tileScores.put( new Location( 1, 2 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 1, 12 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 13, 2 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 13, 12 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 2, 1 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 12, 1 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 2, 13 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 12, 13 ), TileScore.DOUBLE_LETTER );

        tileScores.put( new Location( 2, 4 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 2, 10 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 12, 4 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 12, 10 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 4, 2 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 10, 2 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 4, 12 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 10, 12 ), TileScore.DOUBLE_LETTER );

        tileScores.put( new Location( 4, 6 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 4, 8 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 10, 6 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 10, 8 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 6, 4 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 8, 4 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 6, 10 ), TileScore.DOUBLE_LETTER );
        tileScores.put( new Location( 8, 10 ), TileScore.DOUBLE_LETTER );
    }

    public static void main( String[] args ) {
        String str = "";
        for ( int i = 0; i < WordsConstants.BOARD_SIZE; i++ ) {
            for ( int j = 0; j < WordsConstants.BOARD_SIZE; j++ ) {
                str += tileScores.get( new Location( i, j ) ).ordinal() + " ";
            }
            str += "\n";
        }
        System.out.println( str );
    }

}
