package com.jonathanolson.words;

import java.util.HashMap;
import java.util.List;

public class StringTrieNode {
    public String string;
    public HashMap<Character, StringTrieNode> children;
    public int depth;
    public StringTrie trie;

    StringTrieNode( int depth, StringTrie trie ) {
        this.depth = depth;
        this.trie = trie;
    }

    public boolean isEmpty() {
        return string == null && ( children == null );
    }

    public boolean isLeaf() {
        return children == null;
    }

    public StringTrieNode getChild( char character ) {
        if ( children == null ) {
            return null;
        }
        return children.get( character );
    }

    public void addChildStringsToList( List<String> strings ) {
        if ( string != null ) {
            strings.add( string );
        }
        if ( children != null ) {
            for ( StringTrieNode node : children.values() ) {
                node.addChildStringsToList( strings );
            }
        }
    }

    public boolean hasString() {
        return string != null;
    }

    public String getString() {
        return string;
    }

    private void convertToBranch() {
        string = null;
        children = new HashMap<Character, StringTrieNode>();
    }

    public void addString( String str ) {
        if ( isEmpty() ) {
            // we must be the root
            string = str;
            return;
        }

        // if the string to add is a duplicate of our existing string, bail
        if ( string != null && string.length() == str.length() && str.equals( string ) ) {
            return;
        }

        if ( isLeaf() ) {
            // we are a leaf. convert to a branch
            String other = string;
            convertToBranch();

            // add the two strings in
            addString( other );
            addString( str );
        }
        else {
            // not a leaf node
            if ( str.length() == depth ) {
                // we are adding a shorter version of existing strings
                string = str;
            }
            else {
                char character = str.charAt( depth );
                StringTrieNode possibleChild = getChild( character );
                if ( possibleChild != null ) {
                    // follow the path
                    possibleChild.addString( str );
                }
                else {
                    // create a new node
                    StringTrieNode child = new StringTrieNode( depth + 1, trie );
                    child.string = str;
                    children.put( character, child );
                }
            }
        }
    }

    @Override
    public String toString() {
        // for verification purposes

        if ( isEmpty() ) {
            return "empty";
        }
        String padding = "";
        for ( int i = 0; i < depth; i++ ) {
            padding += ".   ";
        }
        if ( isLeaf() ) {
            return padding + string + "\n";
        }
        else {
            String ret = "";
            if ( string != null ) {
                ret += padding + "* (" + string + ")\n";
            }
            for ( Character character : children.keySet() ) {
                ret += padding + character + "\n";
                ret += children.get( character ).toString();
            }
            return ret;
        }
    }
}
