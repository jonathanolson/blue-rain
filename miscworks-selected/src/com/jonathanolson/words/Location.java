package com.jonathanolson.words;

public class Location {
    public int row;
    public int col;

    public Location( int row, int col ) {
        this.row = row;
        this.col = col;
    }

    @Override
    public String toString() {
        return row + "," + col;
    }

    @Override
    public int hashCode() {
        return row + 100 * col;
    }

    @Override
    public boolean equals( Object obj ) {
        if ( obj instanceof Location ) {
            Location other = (Location) obj;
            return other.row == this.row && other.col == this.col;
        }
        else {
            return false;
        }
    }
}
