package com.jonathanolson.words;

import java.util.LinkedList;
import java.util.List;

/**
 * Unsorted String Trie. This means that given a string 'str', it should be able to efficiently add it to the trie, or
 * find all contained strings that start with 'str'
 */
public class StringTrie {
    private StringTrieNode root;

    public StringTrie() {
        this.root = new StringTrieNode( 0, this );
    }

    /**
     * Returns all strings that start with the substring start
     *
     * @param start Starting substring
     * @return All strings that start with the substring start
     */
    public List<String> getStartingWith( String start ) {
        List<String> ret = new LinkedList<String>();
        StringTrieNode node = root;
        for ( char character : start.toCharArray() ) {
            StringTrieNode next = node.getChild( character );
            if ( next == null ) {
                if ( node.isLeaf() && startsWith( start, node.getString() ) ) {
                    ret.add( node.getString() );
                }
                node = next;
                break;
            }
            node = next;
        }
        if ( node == null ) {
            return ret;
        }
        node.addChildStringsToList( ret );
        return ret;
    }

    public boolean contains( String str ) {
        StringTrieNode node = root;
        for ( char character : str.toCharArray() ) {
            if ( node.hasString() && node.getString().equals( str ) ) {
                return true;
            }
            node = node.getChild( character );
            if ( node == null ) {
                break;
            }
        }
        if ( node != null && node.hasString() && node.getString().equals( str ) ) {
            return true;
        }
        return false;
    }

    /**
     * Adds a string to the trie
     *
     * @param str String to add
     */
    public void add( String str ) {
        root.addString( str );
    }

    @Override
    public String toString() {
        return root.toString();
    }

    /**
     * @return Whether str starts with a i18nized case-insensitive prefix
     */
    protected boolean startsWith( String prefix, String str ) {
        if ( str.length() < prefix.length() ) {
            return false;
        }
        return prefix.equals( str.substring( 0, prefix.length() ) );
    }

    public StringTrieNode getRoot() {
        return root;
    }
}

