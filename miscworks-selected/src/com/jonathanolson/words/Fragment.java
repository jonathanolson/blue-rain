package com.jonathanolson.words;

public class Fragment {
    public final String text;
    public final int offset;

    public Fragment( String text, int offset ) {
        this.text = text;
        this.offset = offset;
    }
}
