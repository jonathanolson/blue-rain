package com.jonathanolson.words;

import java.util.Arrays;

public class ParserState {
    // left available to use
    public int blanks = 0;
    public boolean[] fragments;
    public boolean[] tiles;

    // used so far
    public int usedLength = 0;
    public String usedString = "";
    public int[] scores;
    public int offset = -1; // ie we don't know where this would be placed

    // our line context
    public LineContext lineContext;

    public ParserState( LineContext context, int blanks ) {
        this.blanks = blanks;

        fragments = new boolean[context.fragments.length];
        for ( int i = 0; i < fragments.length; i++ ) {
            fragments[i] = true;
        }

        tiles = new boolean[context.tiles.length];
        for ( int i = 0; i < tiles.length; i++ ) {
            tiles[i] = true;
        }

        scores = new int[0];
        lineContext = context;
    }

    protected ParserState() {

    }

    public boolean canAddFragment( int fragmentIndex ) {
        return fragments[fragmentIndex] && ( offset == -1 || offset + usedLength == lineContext.fragments[fragmentIndex].offset );
    }

    public ParserState withTile( int tileIndex ) {
        ParserState ret = new ParserState();
        ret.blanks = blanks;
        ret.fragments = Arrays.copyOf( fragments, fragments.length );
        ret.tiles = Arrays.copyOf( tiles, tiles.length );
        ret.tiles[tileIndex] = false;

        ret.usedLength = usedLength + 1;
        String letter = lineContext.tiles[tileIndex];
        ret.usedString = usedString + letter;
        ret.scores = Arrays.copyOf( scores, scores.length + 1 );
        ret.scores[scores.length] = Letter.values.get( letter );
        ret.offset = offset;
        ret.lineContext = lineContext;
        return ret;
    }

    public ParserState withFragment( int fragmentIndex ) {
        ParserState ret = new ParserState();
        ret.blanks = blanks;
        ret.fragments = Arrays.copyOf( fragments, fragments.length );
        ret.fragments[fragmentIndex] = false;
        ret.tiles = Arrays.copyOf( tiles, tiles.length );

        String str = lineContext.fragments[fragmentIndex].text;
        ret.usedLength = usedLength + str.length();
        ret.usedString = usedString + str;
        ret.scores = Arrays.copyOf( scores, scores.length + str.length() );
        for ( int offsetOfSpot = scores.length; offsetOfSpot < scores.length + str.length(); offsetOfSpot++ ) {
            // read scores from the board
            ret.scores[offsetOfSpot] = lineContext.getValue( offsetOfSpot );
        }

        // find our previous (possibly new) offset
        ret.offset = ( offset == -1 ? ( lineContext.fragments[fragmentIndex].offset - usedLength ) : offset );
        ret.lineContext = lineContext;
        return ret;
    }

    public ParserState withBlank( String letter ) {
        ParserState ret = new ParserState();
        ret.blanks = blanks - 1;
        ret.fragments = Arrays.copyOf( fragments, fragments.length );
        ret.tiles = Arrays.copyOf( tiles, tiles.length );

        ret.usedLength = usedLength + 1;
        ret.usedString = usedString + letter;
        ret.scores = Arrays.copyOf( scores, scores.length + 1 );
        ret.scores[scores.length] = 0; // no score for a blank
        ret.offset = offset;
        ret.lineContext = lineContext;
        return ret;
    }


}
