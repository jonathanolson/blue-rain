package com.jonathanolson.words;

public class TrieWrapper {
    private StringTrieNode node;

    private String soFar = "";
    private boolean classic = true; // everything nice!

    private TrieWrapper() {

    }

    public TrieWrapper( StringTrie trie ) {
        this.node = trie.getRoot();
    }

    public boolean hasString() {
        return node.hasString() && node.getString().equals( soFar );
    }

    public String getNodeString() {
        return node.getString();
    }

    public String getSoFar() {
        return soFar;
    }

    // return null on failure
    public TrieWrapper add( char character ) {
        if ( classic ) {
            if ( node.isLeaf() ) {
                if ( node.getString().startsWith( soFar + character ) ) {
                    TrieWrapper ret = new TrieWrapper();
                    ret.node = this.node;
                    ret.soFar = this.soFar + character;
                    ret.classic = false; // we are delving into uncharted territory
                    return ret;
                }
                else {
                    // can't extend
                    return null;
                }
            }
            else {
                StringTrieNode child = node.getChild( character );
                if ( child == null ) {
                    return null;
                }
                TrieWrapper ret = new TrieWrapper();
                ret.node = child;
                ret.soFar = this.soFar + character;
                return ret;
            }
        }
        else {
            if ( node.getString().startsWith( soFar + character ) ) {
                TrieWrapper ret = new TrieWrapper();
                ret.node = this.node;
                ret.soFar = this.soFar + character;
                ret.classic = false; // we are delving into uncharted territory
                return ret;
            }
            else {
                return null;
            }
        }
    }

    // return null on failure
    public TrieWrapper add( String str ) {
        TrieWrapper ret = this;
        for ( char c : str.toCharArray() ) {
            ret = ret.add( c );
            if ( ret == null ) {
                // didn't work
                return null;
            }
        }
        return ret;
    }
}
