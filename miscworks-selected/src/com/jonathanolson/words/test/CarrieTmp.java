package com.jonathanolson.words.test;

import com.jonathanolson.words.Board;
import com.jonathanolson.words.RoughSearcher;

public class CarrieTmp {
    public static void main( String[] args ) {
        Board board = new Board( new String[] { "e","l","y","s","a","s" } );

        board.setSpot( 3,5,"v" );
        board.setSpot( 3,6,"o" );
        board.setSpot( 3,7,"w" );
        board.setSpot( 4,7,"i" );
        board.setSpot( 5,5,"p" );
        board.setSpot( 5,6,"e" );
        board.setSpot( 5,7,"t" );
        board.setSpot( 6,7,"c" );
        board.setSpot( 7,7,"h" );
        board.setSpot( 7,8,"i" );
        board.setSpot( 7,9,"m" );
        board.setSpot( 6,9,"o" );
        board.setSpot( 5,9,"c" );
        board.setSpot( 8,9,"e" );
        board.setSpot( 9,6,"b" );
        board.setSpot( 9,7,"i" );
        board.setSpot( 9,8,"r" );
        board.setSpot( 9,9,"t" );
        board.setSpot( 9,10,"h" );
        board.setSpot( 9,11,"e" );
        board.setSpot( 9,12,"d" );

        System.out.println( board );

        new RoughSearcher( board, 1 ).process();
    }
}
