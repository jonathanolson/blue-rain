package com.jonathanolson.words.test;

import com.jonathanolson.words.Board;
import com.jonathanolson.words.Letter;
import com.jonathanolson.words.RoughSearcher;
import com.jonathanolson.words.TileOptions;

public class CVC {
    public static void main( String[] args ) {
        TileOptions options = Letter.fullDrawEstimated();
        Board board = new Board( options.tiles );

        System.out.println( board );

        while ( true ) {
            RoughSearcher searcher = new RoughSearcher( board, options.blanks );
            searcher.process();
            options = Letter.fullDrawEstimated();
            board = searcher.getBestSolution().getBoard();
            board.tiles = options.tiles;
        }
    }

}
