package com.jonathanolson.words.test;

import com.jonathanolson.words.Board;
import com.jonathanolson.words.RoughSearcher;

public class CarrieMary1 {
    public static void main( String[] args ) {
        Board board = new Board( new String[]{"f", "i", "u", "t", "k", "u", "t"} );

        board.setSpot( 3, 0, "g" );
        board.setSpot( 3, 3, "b" );
        board.setSpot( 4, 0, "o" );
        board.setSpot( 4, 3, "e" );
        board.setSpot( 5, 0, "o" );
        board.setSpot( 5, 1, "o" );
        board.setSpot( 5, 2, "z" );
        board.setSpot( 5, 3, "e" );
        board.setSpot( 5, 5, "n" );
        board.setSpot( 6, 0, "i" );
        board.setSpot( 6, 4, "b" );
        board.setSpot( 6, 5, "o" );
        board.setSpot( 7, 0, "e" );
        board.setSpot( 7, 3, "h" );
        board.setSpot( 7, 4, "a" );
        board.setSpot( 7, 5, "r" );
        board.setSpot( 7, 6, "p" );
        board.setSpot( 7, 7, "y" );
        board.setSpot( 8, 0, "r" );
        board.setSpot( 8, 1, "a" );
        board.setSpot( 8, 2, "t" );
        board.setSpot( 8, 4, "n" );
        board.setSpot( 8, 6, "e" );
        board.setSpot( 9, 1, "g" );
        board.setSpot( 9, 2, "a" );
        board.setSpot( 9, 3, "u" );
        board.setSpot( 9, 4, "d" );
        board.setSpot( 9, 6, "e" );
        board.setSpot( 9, 7, "n" );
        board.setSpot( 10, 2, "w" );
        board.setSpot( 10, 6, "r" );
        board.setSpot( 10, 7, "e" );
        board.setSpot( 11, 7, "e" );
        board.setSpot( 11, 8, "r" );
        board.setSpot( 12, 7, "d" );
        board.setSpot( 12, 8, "o" );
        board.setSpot( 13, 8, "v" );
        board.setSpot( 14, 8, "e" );

        System.out.println( board );

        new RoughSearcher( board, 0 ).process();
    }
}
