package com.jonathanolson.words.test;

import com.jonathanolson.words.Board;
import com.jonathanolson.words.RoughSearcher;

public class CarrieMary2 {
    public static void main( String[] args ) {
        Board board = new Board( new String[] { "t","r","l","a","e","n","s" } );

        System.out.println( board );

        new RoughSearcher( board, 0 ).process();
    }
}
