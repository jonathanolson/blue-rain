package com.jonathanolson.words.test;

import com.jonathanolson.words.Board;
import com.jonathanolson.words.RoughSearcher;

public class Test1 {
    public static void main( String[] args ) {
        Board board = new Board( new String[]{"c", "a", "e", "o", "i", "l", "r"} );

        board.setSpot( 0, 11, "t" );
        board.setSpot( 0, 12, "o" );
        board.setSpot( 0, 13, "n" );
        board.setSpot( 0, 14, "g", 0 ); // was a blank tile
        board.setSpot( 1, 14, "l" );
        board.setSpot( 2, 14, "o" );
        board.setSpot( 3, 14, "v" );
        board.setSpot( 4, 13, "f" );
        board.setSpot( 4, 14, "e" );
        board.setSpot( 5, 13, "i" );
        board.setSpot( 5, 14, "d" );
        board.setSpot( 6, 10, "h" );
        board.setSpot( 6, 13, "n" );
        board.setSpot( 7, 5, "t" );
        board.setSpot( 7, 6, "a" );
        board.setSpot( 7, 7, "b" );
        board.setSpot( 7, 9, "f" );
        board.setSpot( 7, 10, "a" );
        board.setSpot( 7, 11, "d" );
        board.setSpot( 7, 12, "e" );
        board.setSpot( 7, 13, "s" );
        board.setSpot( 8, 7, "i" );
        board.setSpot( 8, 10, "r" );
        board.setSpot( 9, 7, "b" );
        board.setSpot( 9, 8, "i" );
        board.setSpot( 9, 9, "l" );
        board.setSpot( 9, 10, "e" );
        board.setSpot( 10, 7, "l" );
        board.setSpot( 10, 9, "o" );
        board.setSpot( 10, 10, "m" );
        board.setSpot( 11, 7, "e" );
        board.setSpot( 11, 9, "v" );
        board.setSpot( 12, 3, "d" );
        board.setSpot( 12, 9, "e" );
        board.setSpot( 13, 3, "i" );
        board.setSpot( 13, 5, "m" );
        board.setSpot( 13, 6, "i" );
        board.setSpot( 13, 7, "r" );
        board.setSpot( 13, 8, "e" );
        board.setSpot( 13, 9, "d" );
        board.setSpot( 14, 3, "g" );
        board.setSpot( 14, 4, "a" );
        board.setSpot( 14, 5, "y" );

        System.out.println( board );

        new RoughSearcher( board, 0 ).process();
    }
}
