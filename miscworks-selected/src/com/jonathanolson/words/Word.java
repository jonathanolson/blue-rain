package com.jonathanolson.words;

import java.util.LinkedList;
import java.util.List;

public class Word {
    public boolean horizontal;
    public int rowcol; // rows for horizontal, cols for vertical
    public int offset; // the opposite
    public int length;

    public Word( boolean horizontal, int rowcol, int offset, int length ) {
        this.horizontal = horizontal;
        this.rowcol = rowcol;
        this.offset = offset;
        this.length = length;
    }

    public Location getWithOffset( int offset ) {
        if ( horizontal ) {
            return new Location( rowcol, this.offset + offset );
        }
        else {
            return new Location( this.offset + offset, rowcol );
        }
    }

    @Override
    public boolean equals( Object ob ) {
        if ( ob instanceof Word ) {
            Word other = (Word) ob;
            return this.horizontal == other.horizontal && this.rowcol == other.rowcol && this.offset == other.offset && this.length == other.length;
        }
        else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return rowcol * ( 17 + offset * ( 3754 + length * 13 ) ) * 2 + ( horizontal ? 10101010 : 1010101 );
    }

    public static List<Word> getWordsFromLocation( Board board, Location location ) {
        List<Word> ret = new LinkedList<Word>();
        // horizontal
        int hStartCol = location.col;
        int hEndCol = location.col;
        while ( board.inBounds( location.row, hStartCol - 1 ) && board.isSet( location.row, hStartCol - 1 ) ) {
            hStartCol -= 1;
        }
        while ( board.inBounds( location.row, hEndCol + 1 ) && board.isSet( location.row, hEndCol + 1 ) ) {
            hEndCol += 1;
        }
        if ( hStartCol != hEndCol ) { // don't allow 1-long varieties
            ret.add( new Word( true, location.row, hStartCol, hEndCol - hStartCol + 1 ) );
        }

        // vertical
        int vStartRow = location.row;
        int vEndRow = location.row;
        while ( board.inBounds( vStartRow - 1, location.col ) && board.isSet( vStartRow - 1, location.col ) ) {
            vStartRow -= 1;
        }
        while ( board.inBounds( vEndRow + 1, location.col ) && board.isSet( vEndRow + 1, location.col ) ) {
            vEndRow += 1;
        }
        if ( vStartRow != vEndRow ) { // don't allow 1-long varieties
            ret.add( new Word( false, location.col, vStartRow, vEndRow - vStartRow + 1 ) );
        }

        return ret;
    }
}
