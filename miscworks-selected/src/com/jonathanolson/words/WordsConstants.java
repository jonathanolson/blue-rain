package com.jonathanolson.words;

public class WordsConstants {
    public static final int BOARD_SIZE = 15;

    public static final int LONGEST_WORD = 15;
    public static final int MAX_TILES = 7;

    public static final int NUM_SPOTS = BOARD_SIZE * BOARD_SIZE;

    public static final int START_ROW = 7;
    public static final int START_COL = 7;
}
