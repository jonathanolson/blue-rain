package com.jonathanolson.words;

import java.io.File;
import java.io.IOException;

import com.jonathanolson.util.FileUtils;

public class WordDictionary {
    public static final StringTrie trie = new StringTrie();

    public static boolean contains( String str ) {
        return trie.contains( str );
    }

    static {
        try {
            String file = FileUtils.loadFileAsString( new File( "/home/jon/miscworks/svn/trunk/wordfriends2.txt" ) );
            String[] lines = file.split( "\n" );
            for ( String line : lines ) {
                trie.add( line.trim() );
            }
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    public static void main( String[] args ) {
        for ( String str : trie.getStartingWith( "pan" ) ) {
            System.out.println( str );
        }
    }
}
