package com.jonathanolson.words;

import java.util.LinkedList;
import java.util.List;

public abstract class LineContext {
    public Fragment[] fragments = new Fragment[]{};
    public Board board;

    public String[] tiles;

    public abstract boolean isHotspot( int offset );

    public abstract boolean isSet( int offset );

    public abstract String getSpot( int offset );

    // TODO: allowed letters for each spot. scan beforehand for speed, and possible use during parsing?

    public void initializeFragments() {
        List<Fragment> frags = new LinkedList<Fragment>();

        boolean inFragment = false;
        int fragStart = 0;
        String fragString = "";
        for ( int offset = 0; offset < WordsConstants.BOARD_SIZE; offset++ ) {
            if ( isSet( offset ) ) {
                if ( !inFragment ) {
                    inFragment = true;
                    fragStart = offset;
                }
                fragString += getSpot( offset );
            }
            else {
                if ( inFragment ) {
                    inFragment = false;
                    frags.add( new Fragment( fragString, fragStart ) );
                    fragString = "";
                }
            }
        }
        if ( inFragment ) {
            frags.add( new Fragment( fragString, fragStart ) );
        }
        fragments = frags.toArray( fragments );

//        for ( Fragment fragment : fragments ) {
//            System.out.println( "fragment: " + fragment.text );
//        }
    }

    public abstract int getValue( int offset );

    public static class HorizontalLineContext extends LineContext {
        public int row;

        public HorizontalLineContext( Board board, int row ) {
            this.board = board;
            this.row = row;
            this.tiles = board.tiles;

            initializeFragments();
        }

        @Override
        public boolean isHotspot( int offset ) {
            return board.isHot( row, offset );
        }

        @Override
        public boolean isSet( int offset ) {
            return board.isSet( row, offset );
        }

        @Override
        public String getSpot( int offset ) {
            return board.getSpot( row, offset );
        }

        @Override
        public int getValue( int offset ) {
            return board.getValue( row, offset );
        }
    }

    public static class VerticalLineContext extends LineContext {
        public int col;

        public VerticalLineContext( Board board, int col ) {
            this.board = board;
            this.col = col;
            this.tiles = board.tiles;

            initializeFragments();
        }

        @Override
        public boolean isHotspot( int offset ) {
            return board.isHot( offset, col );
        }

        @Override
        public boolean isSet( int offset ) {
            return board.isSet( offset, col );
        }

        @Override
        public String getSpot( int offset ) {
            return board.getSpot( offset, col );
        }

        @Override
        public int getValue( int offset ) {
            return board.getValue( offset, col );
        }
    }
}
