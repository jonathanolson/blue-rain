package com.jonathanolson.words;

public enum TileScore {
    REGULAR,
    DOUBLE_LETTER,
    TRIPLE_LETTER,
    DOUBLE_WORD,
    TRIPLE_WORD
}
