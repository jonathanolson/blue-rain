package com.jonathanolson.words;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

// TODO: (HIGH) filter out duplicate matches

public class RoughSearcher {
    private int startingBlanks;
    private Board board;
    private List<Solution> solutions = new LinkedList<Solution>();

    private List<ParserState> ret;
    private Solution bestSolution;

    public RoughSearcher( Board board, int blanks ) {
        this.board = board;
        this.startingBlanks = blanks;
    }

    public void process() {
        // ret already initialized

        for ( int row = 0; row < WordsConstants.BOARD_SIZE; row++ ) {
            // wipe ret array
            ret = new LinkedList<ParserState>();

            LineContext context = new LineContext.HorizontalLineContext( board, row );
            step( new ParserState( context, startingBlanks ), new TrieWrapper( WordDictionary.trie ) );

            for ( ParserState state : ret ) {
//                System.out.println( state.usedString + "     " + state.offset );
                if ( state.offset == -1 ) {
                    for ( int offset = 0; offset <= WordsConstants.BOARD_SIZE - state.usedLength; offset++ ) {
                        checkHorizontalStateLocation( state, row, offset );
                    }
                }
                else {
                    checkHorizontalStateLocation( state, row, state.offset );
                }
            }
        }

        for ( int col = 0; col < WordsConstants.BOARD_SIZE; col++ ) {
            // wipe ret array
            ret = new LinkedList<ParserState>();

            LineContext context = new LineContext.VerticalLineContext( board, col );
            step( new ParserState( context, startingBlanks ), new TrieWrapper( WordDictionary.trie ) );

            for ( ParserState state : ret ) {
//                System.out.println( state.usedString );
                if ( state.offset == -1 ) {
                    for ( int offset = 0; offset <= WordsConstants.BOARD_SIZE - state.usedLength; offset++ ) {
                        checkVerticalStateLocation( state, col, offset );
                    }
                }
                else {
                    checkVerticalStateLocation( state, col, state.offset );
                }
            }
        }

        Collections.sort( solutions, new Comparator<Solution>() {
            @Override
            public int compare( Solution a, Solution b ) {
                return new Integer( b.getScore() ).compareTo( a.getScore() );
            }
        } );

        printSolution( solutions.get( 0 ) );
        bestSolution = solutions.get( 0 );
//        for ( Solution solution : solutions.subList( 0,30 ) ) {
//            printSolution( solution );
//        }
    }

    public Solution getBestSolution() {
        return bestSolution;
    }

    public void checkHorizontalStateLocation( ParserState state, int row, int offset ) {
        boolean foundHotspot = false;
        if ( offset < 0 || offset + state.usedLength >= WordsConstants.BOARD_SIZE ) {
            // word goes out of bounds!
            return;
        }
        for ( int col = offset; col < offset + state.usedLength; col++ ) {
            if ( board.isHot( row, col ) ) {
                foundHotspot = true;
                break;
            }
        }
        if ( !foundHotspot ) {
            // doesn't hit a hotspot
            return;
        }
        Board newBoard = board.copy();
        List<Location> changedLocations = new LinkedList<Location>();
        for ( int col = offset; col < offset + state.usedLength; col++ ) {
            if ( !newBoard.isSet( row, col ) ) {
                newBoard.setSpot( row, col, Character.toString( state.usedString.charAt( col - offset ) ), state.scores[col - offset] );
                changedLocations.add( new Location( row, col ) );
            }
        }
        if ( newBoard.isValid() ) {
            registerSolution( new Solution( state, newBoard, changedLocations ) );
        }
    }

    public void checkVerticalStateLocation( ParserState state, int col, int offset ) {
        boolean foundHotspot = false;
        if ( offset < 0 || offset + state.usedLength >= WordsConstants.BOARD_SIZE ) {
            // word goes out of bounds!
            return;
        }
        for ( int row = offset; row < offset + state.usedLength; row++ ) {
            if ( board.isHot( row, col ) ) {
                foundHotspot = true;
                break;
            }
        }
        if ( !foundHotspot ) {
            // doesn't hit a hotspot
            return;
        }
        Board newBoard = board.copy();
        List<Location> changedLocations = new LinkedList<Location>();
        for ( int row = offset; row < offset + state.usedLength; row++ ) {
            if ( !newBoard.isSet( row, col ) ) {
                newBoard.setSpot( row, col, Character.toString( state.usedString.charAt( row - offset ) ), state.scores[row - offset] );
                changedLocations.add( new Location( row, col ) );
            }
        }
        if ( newBoard.isValid() ) {
            registerSolution( new Solution( state, newBoard, changedLocations ) );
        }
    }

    public void registerSolution( Solution solution ) {
        solutions.add( solution );
    }

    private void printSolution( Solution solution ) {
        System.out.println( "####" );
        System.out.println( "option: " + solution.getState().usedString );
        System.out.println( "score: " + solution.getScore() );
        System.out.println( solution.getBoard() );
    }

    public void step( ParserState state, TrieWrapper wrapper ) {
//        System.out.println( "step: " + state.usedString );
        // TODO: blocking of state with more constraints!
        for ( int i = 0; i < state.tiles.length; i++ ) {
//            if ( state.usedLength == 0 ) {
//                System.out.println( "tile: #" + i + ": " + state.lineContext.tiles[i] + " is available: " + state.tiles[i] );
//            }
            if ( state.tiles[i] ) { // it is available
                // TODO: speed up?
                TrieWrapper child = wrapper.add( state.lineContext.tiles[i].toCharArray()[0] );
                if ( child != null ) { // has this character!
                    ParserState newState = state.withTile( i );
                    if ( child.hasString() ) {
                        ret.add( newState );
                    }
                    step( newState, child );
                }
            }
        }
        if ( state.blanks > 0 ) {
            for ( char letter : new char[]{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'} ) {
                TrieWrapper child = wrapper.add( letter );
                if ( child != null ) {
                    ParserState newState = state.withBlank( Character.toString( letter ) );
                    if ( child.hasString() ) {
                        ret.add( newState );
                    }
                    step( newState, child );
                }
            }
        }
        for ( int i = 0; i < state.fragments.length; i++ ) {
            if ( state.canAddFragment( i ) ) {
                TrieWrapper child = wrapper.add( state.lineContext.fragments[i].text );
                if ( child != null ) {
                    ParserState newState = state.withFragment( i );
                    if ( newState.offset >= 0 ) { // only use fragments if they allow us in-bounds
                        if ( child.hasString() ) {
                            ret.add( newState );
                        }
                        step( newState, child );
                    }
                }
            }
        }
    }

    public static class Solution {
        private ParserState state;
        private Board board;
        private List<Location> changedLocations;
        private int score;

        public Solution( ParserState state, Board board, List<Location> changedLocations ) {
            this.state = state;
            this.board = board;
            this.changedLocations = changedLocations;
            this.score = board.getScore( changedLocations );
        }

        public ParserState getState() {
            return state;
        }

        public Board getBoard() {
            return board;
        }

        public List<Location> getChangedLocations() {
            return changedLocations;
        }

        public int getScore() {
            return score;
        }
    }
}
