package com.jonathanolson.words;

public class TileOptions {
    public String[] tiles;
    public int blanks;

    public TileOptions( String[] tiles, int blanks ) {
        this.tiles = tiles;
        this.blanks = blanks;
    }
}
