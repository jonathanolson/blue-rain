package com.jonathanolson.words;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Board {
    private String[] spots = new String[WordsConstants.NUM_SPOTS];
    private boolean[] hotSpots = new boolean[WordsConstants.NUM_SPOTS];
    private int[] values = new int[WordsConstants.NUM_SPOTS];
    public String[] tiles;

    private Board() {

    }

    public Board( String[] tiles ) {
        this.tiles = tiles;
        for ( int i = 0; i < WordsConstants.NUM_SPOTS; i++ ) {
            spots[i] = null; // just to be sure
            hotSpots[i] = false;
            values[i] = 0;
        }
        hotSpots[index( WordsConstants.START_ROW, WordsConstants.START_COL )] = true;
    }

    public Board copy() {
        Board board = new Board();
        // state copy
        board.spots = Arrays.copyOf( spots, spots.length );
        board.hotSpots = Arrays.copyOf( hotSpots, hotSpots.length );
        board.values = Arrays.copyOf( values, values.length );
        board.tiles = Arrays.copyOf( tiles, tiles.length );
        return board;
    }

    public boolean isValid() {
        // TODO: don't call this for each step, use a better-performance option
        for ( int row = 0; row < WordsConstants.BOARD_SIZE; row++ ) {
            LineContext context = new LineContext.HorizontalLineContext( this, row );
            for ( Fragment fragment : context.fragments ) {
                if ( !isFragmentValid( fragment ) ) {
                    return false;
                }
            }
        }

        for ( int col = 0; col < WordsConstants.BOARD_SIZE; col++ ) {
            LineContext context = new LineContext.VerticalLineContext( this, col );
            for ( Fragment fragment : context.fragments ) {
                if ( !isFragmentValid( fragment ) ) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isFragmentValid( Fragment fragment ) {
        return fragment.text.length() <= 1 || WordDictionary.contains( fragment.text );
    }

    private int index( int row, int col ) {
        return row * WordsConstants.BOARD_SIZE + col;
    }

    public boolean isSet( int row, int col ) {
        return getSpot( row, col ) != null;
    }

    public boolean isHot( int row, int col ) {
        return hotSpots[index( row, col )];
    }

    public int getValue( int row, int col ) {
        return values[index( row, col )];
    }

    public boolean inBounds( int row, int col ) {
        return row >= 0 && row < WordsConstants.BOARD_SIZE && col >= 0 && col < WordsConstants.BOARD_SIZE;
    }

    public String getSpot( int row, int col ) {
        return spots[index( row, col )];
    }

    public void setSpot( int row, int col, String letter ) {
        setSpot( row, col, letter, Letter.values.get( letter ) );
    }

    public void setSpot( int row, int col, String letter, int value ) {
        spots[index( row, col )] = letter;
        hotSpots[index( row, col )] = false;
        values[index( row, col )] = value;

        // hotspot all of the other ones!
        if ( inBounds( row, col - 1 ) && !isSet( row, col - 1 ) ) {
            hotSpots[index( row, col - 1 )] = true;
        }
        if ( inBounds( row, col + 1 ) && !isSet( row, col + 1 ) ) {
            hotSpots[index( row, col + 1 )] = true;
        }
        if ( inBounds( row - 1, col ) && !isSet( row - 1, col ) ) {
            hotSpots[index( row - 1, col )] = true;
        }
        if ( inBounds( row + 1, col ) && !isSet( row + 1, col ) ) {
            hotSpots[index( row + 1, col )] = true;
        }
    }

    @Override
    public String toString() {
        // not efficient
        String ret = "";
        ret += "-----------------------------\n";
        for ( int row = 0; row < WordsConstants.BOARD_SIZE; row++ ) {
            for ( int col = 0; col < WordsConstants.BOARD_SIZE; col++ ) {
                ret += isSet( row, col ) ? getSpot( row, col ) + " " : ( isHot( row, col ) ? " " : "." ) + " ";
            }
            ret += "\n";
        }
        return ret;
    }

    public int getScore( List<Location> changedLocations ) {
//        boolean debug = "k".equals( getSpot( 0, 4 ) ) && "u".equals( getSpot( 1, 4 ) ) && "f".equals( getSpot( 2, 4 ) )
//                        && "i".equals( getSpot( 3, 4 ) );
        boolean debug = false;
        if ( debug ) {System.out.println( "Debugging score" );}
        int score = 0;
        if ( changedLocations.size() == 7 ) {
            if ( debug ) { System.out.println( "+35 for 7" );}
            score += 35; // yay!
        }
        Set<Word> words = new HashSet<Word>();
        for ( Location location : changedLocations ) {
            List<Word> www = Word.getWordsFromLocation( this, location );
            if ( debug ) {
                for ( Word word : www ) {
                    System.out.println( "word for location " + location.row + "," + location.col + ": " + word.horizontal + "," + word.rowcol + "," + word.offset + "," + word.length );
                }
            }
            words.addAll( www );
        }

        HashSet<Location> locs = new HashSet<Location>();
        locs.addAll( changedLocations );

        for ( Word word : words ) {
            if ( debug ) {
                System.out.println( "word: " + word.horizontal + "," + word.rowcol + "," + word.offset + "," + word.length );
            }
            int mulBonus = 1;
            int subscore = 0;
            for ( int offset = 0; offset < word.length; offset++ ) {
                Location location = word.getWithOffset( offset );
                int letterValue = getValue( location.row, location.col );
                if ( debug ) {System.out.println( "value at location " + location + ": " + letterValue );}
                if ( locs.contains( location ) ) {
                    if ( debug ) {
                        System.out.println( "placed location: " + location + ", with: " + Letter.tileScores.get( location ) );
                    }
                    // we placed here
                    switch( Letter.tileScores.get( location ) ) {
                        case REGULAR:
                            break;
                        case DOUBLE_LETTER:
                            letterValue *= 2;
                            break;
                        case TRIPLE_LETTER:
                            letterValue *= 3;
                            break;
                        case DOUBLE_WORD:
                            mulBonus *= 2;
                            break;
                        case TRIPLE_WORD:
                            mulBonus *= 3;
                            break;
                    }
                }
                subscore += letterValue;
            }
            if ( debug ) {
                System.out.println( "word score: " + ( mulBonus * subscore ) + " with subscore: " + subscore );
            }
            score += mulBonus * subscore;
        }
        return score;
    }
}
