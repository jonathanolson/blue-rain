package com.jonathanolson.games.cards.michiganrummy;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.games.cards.*;

public abstract class MichiganRummyPlayer {

    private Hand hand;
    private String name;
    private int points = 0;

    public abstract Card chooseCard( List<Card> options );

    protected MichiganRummyPlayer() {
        this( null );
    }

    protected MichiganRummyPlayer( String name ) {
        this.name = name;
    }

    public Hand getHand() {
        return hand;
    }

    public void setHand( Hand hand ) {
        this.hand = hand;
    }

    // returns whether we were able to play anything
    public boolean attemptPlay( MichiganRummyGame game, boolean starting ) {
        List<Card> options = getPlayableCards( game.getPile(), starting );

        if ( options.isEmpty() ) {
            return false;
        }

        Card playCard;

        if ( options.size() == 1 ) {
            playCard = options.get( 0 );
        }
        else {
            // multiple cards, must be starting. need to choose intelligently!
            playCard = chooseCard( options );
        }

        if ( name != null ) {
            if ( starting ) {
                System.out.println( name + " starts with " + playCard );
            }
            else {
                System.out.println( name + " plays " + playCard );
            }
        }

        game.playCard( this, playCard );
        return true;
    }

    private List<Card> getPlayableCards( MichiganRummyPile pile, boolean starting ) {
        List<Card> cards = hand.getCards();
        List<Card> ret = new LinkedList<Card>();

        if ( cards.isEmpty() ) {
            return ret;
        }

        Card lastCard = pile.getLastCard();

        StandardCardOrder order = new StandardCardOrder();


        // if we are NOT starting
        if ( !starting ) {
            Rank pileRank = lastCard.getRank();
            Suit pileSuit = lastCard.getSuit();

            if ( pileRank == order.getHighRank() ) {
                return ret;
            }

            Rank desiredRank = order.getNextRank( pileRank );

            for ( Card card : cards ) {
                if ( card.getRank() == desiredRank && card.getSuit() == pileSuit ) {
                    ret.add( card );
                    return ret;
                }
            }

            return ret;
        }

        // we must be starting

        order.orderRanksLowToHigh( cards );

        while ( !cards.isEmpty() ) {
            Rank rank = cards.get( 0 ).getRank();

            List<Card> sameRankCards = new LinkedList<Card>();

            while ( !cards.isEmpty() && cards.get( 0 ).getRank() == rank ) {
                Card card = cards.get( 0 );
                cards.remove( 0 );

                if ( lastCard != null && lastCard.getSuit() == card.getSuit() ) {
                    continue;
                }

                sameRankCards.add( card );
            }

            if ( !sameRankCards.isEmpty() ) {
                return sameRankCards;
            }

        }

        // return emptyness!
        return ret;

    }

    public String getName() {
        return name;
    }

    public int getPoints() {
        return points;
    }

    public void addPoints( int points ) {
        this.points += points;
    }

    public void subtractPoints( int points ) {
        this.points -= points;
    }

    public void setPoints( int points ) {
        this.points = points;
    }

    public String toString() {
        return "[" + name + "] " + "(" + ( points > 0 ? "+" : "" ) + points + ") " + hand;
    }
}
