package com.jonathanolson.games.cards.michiganrummy.players;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.jonathanolson.games.cards.Card;
import com.jonathanolson.games.cards.Rank;
import com.jonathanolson.games.cards.StandardCardOrder;
import com.jonathanolson.games.cards.Suit;
import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;

public class MichiganRummyPossibleRunPlayer extends MichiganRummyPlayer {

    public MichiganRummyPossibleRunPlayer() {
    }

    public MichiganRummyPossibleRunPlayer( String name ) {
        super( name );
    }

    public Card chooseCard( List<Card> options ) {
        List<Card> cards = getHand().getCards();

        StandardCardOrder order = new StandardCardOrder();

        order.orderRanksLowToHigh( cards );

        Map<Suit, List<Card>> suitCards = new HashMap<Suit, List<Card>>();

        for ( Suit suit : Suit.values() ) {
            suitCards.put( suit, new LinkedList<Card>() );
        }

        for ( Card card : cards ) {
            suitCards.get( card.getSuit() ).add( card );
        }

        int mostruns = 0;
        Card mostRunsCard = null;

        for ( int i = 0; i < options.size(); i++ ) {
            Card option = options.get( i );
            Suit suit = option.getSuit();
            List<Card> cardsSameSuit = suitCards.get( suit );

            Rank curRank = null;
            int runs = 0;

            for ( Card card : cardsSameSuit ) {
                if ( curRank == null || order.getNextRank( curRank ) != card.getRank() ) {
                    runs++;
                    curRank = card.getRank();
                }
                else {
                    continue;
                }

            }

            if ( runs > mostruns || mostRunsCard == null ) {
                mostruns = runs;
                mostRunsCard = option;
            }

        }

        return mostRunsCard;
    }
}