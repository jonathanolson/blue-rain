package com.jonathanolson.games.cards.michiganrummy.players;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.jonathanolson.games.cards.Card;
import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;

public class MichiganRummyRandomPlayer extends MichiganRummyPlayer {

    private static Random random = new Random( System.currentTimeMillis() );

    public MichiganRummyRandomPlayer() {
    }

    public MichiganRummyRandomPlayer( String name ) {
        super( name );
    }

    public Card chooseCard( List<Card> options ) {
        Collections.shuffle( options, random );
        return options.get( 0 );
    }
}
