package com.jonathanolson.games.cards.michiganrummy.tests;

import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyRandomPlayer;

public class MichiganRummyPlayerControlTest {
    public static void main( String[] args ) {
        MichiganRummyPlayerTest test = new MichiganRummyPlayerTest();
        test.addPlayer( new MichiganRummyRandomPlayer() );
        test.addPlayer( new MichiganRummyRandomPlayer() );
        test.addPlayer( new MichiganRummyRandomPlayer() );
        test.run();
    }
}

/*

1M runs for both:

        [1]:
            points / iterations: 0.109487
            wins / iterations: 0.335996
        [2]:
            points / iterations: -0.015389
            wins / iterations: 0.332241
        [3]:
            points / iterations: -0.094572
            wins / iterations: 0.329188
        draws / iterations: 0.002575

        [1]:
            points / iterations: 0.077597
            wins / iterations: 0.336079
        [2]:
            points / iterations: 0.015349
            wins / iterations: 0.330706
        [3]:
            points / iterations: -0.093414
            wins / iterations: 0.330397
        draws / iterations: 0.002818
 */
