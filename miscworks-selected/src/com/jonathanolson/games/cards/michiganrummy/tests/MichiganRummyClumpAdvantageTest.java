package com.jonathanolson.games.cards.michiganrummy.tests;

import com.jonathanolson.games.cards.michiganrummy.MichiganRummyGame;
import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;
import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyClumpPlayer;
import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyRandomPlayer;

/**
 * Clump advantage test
 * a: 0.338317
 * b: 0.33091
 * c: 0.328185
 * draw: 0.002588
 * <p/>
 * shows only a slight advantage? is there a better way to plan?
 */
public class MichiganRummyClumpAdvantageTest {
    public static void main( String[] args ) {
        System.out.println( "Clump advantage test" );
        int iterations = 1000000;

        MichiganRummyPlayer a = new MichiganRummyClumpPlayer();
        MichiganRummyPlayer b = new MichiganRummyRandomPlayer();
        MichiganRummyPlayer c = new MichiganRummyRandomPlayer();

        int aWins = 0;
        int bWins = 0;
        int cWins = 0;
        int draws = 0;

        for ( int i = 0; i < iterations; i++ ) {
            MichiganRummyGame game = new MichiganRummyGame();

            game.addPlayer( a );
            game.addPlayer( b );
            game.addPlayer( c );

            switch( i % 3 ) {
                case 0:
                    game.setLead( a );
                    break;
                case 1:
                    game.setLead( b );
                    break;
                case 2:
                    game.setLead( c );
                    break;
            }

            game.deal();
            game.playGame();

            if ( !game.isDraw() ) {
                MichiganRummyPlayer winner = game.getWinner();

                if ( winner == a ) {
                    aWins++;
                }
                else if ( winner == b ) {
                    bWins++;
                }
                else if ( winner == c ) {
                    cWins++;
                }
            }
            else {
                draws++;
            }
        }

        double aFraction = (double) aWins / (double) iterations;
        double bFraction = (double) bWins / (double) iterations;
        double cFraction = (double) cWins / (double) iterations;
        double drawFraction = (double) draws / (double) iterations;

        System.out.println( "a: " + aFraction );
        System.out.println( "b: " + bFraction );
        System.out.println( "c: " + cFraction );
        System.out.println( "draw: " + drawFraction );
    }
}