package com.jonathanolson.games.cards.michiganrummy.tests;

import java.util.HashMap;
import java.util.Map;

import com.jonathanolson.games.cards.michiganrummy.MichiganRummyGame;
import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;

public class MichiganRummyPlayerTest {

    private MichiganRummyGame game;
    private int iterations = 1000000;

    private Map<MichiganRummyPlayer, Integer> wins;
    private int draws;

    public MichiganRummyPlayerTest() {
        game = new MichiganRummyGame();
        wins = new HashMap<MichiganRummyPlayer, Integer>();
    }

    public void addPlayer( MichiganRummyPlayer player ) {
        game.addPlayer( player );
        wins.put( player, 0 );
    }

    public void run() {
        for ( int i = 0; i < iterations; i++ ) {
            game.deal();
            game.playGame();

            if ( game.isDraw() ) {
                draws++;
            }
            else {
                MichiganRummyPlayer winner = game.getWinner();
                wins.put( winner, wins.get( winner ) + 1 );
            }
        }

        double diters = (double) iterations;

        int playerNum = 0;

        for ( MichiganRummyPlayer player : game.getPlayers() ) {
            playerNum++;

            if ( player.getName() != null ) {
                System.out.println( "[" + player.getName() + "]:" );
            }
            else {
                System.out.println( "[" + playerNum + "]:" );
            }

            System.out.println( "\tpoints / iterations: " + (double) player.getPoints() / diters );
            System.out.println( "\twins / iterations: " + (double) wins.get( player ) / diters );
        }

        System.out.println( "draws / iterations: " + (double) draws / diters );
    }

}
