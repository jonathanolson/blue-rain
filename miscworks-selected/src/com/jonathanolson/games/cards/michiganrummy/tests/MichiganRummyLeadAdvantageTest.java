package com.jonathanolson.games.cards.michiganrummy.tests;

import com.jonathanolson.games.cards.michiganrummy.MichiganRummyGame;
import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;
import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyRandomPlayer;

/**
 * Results (1M iterations):
 * <p/>
 * a: 0.345516
 * b: 0.328305
 * c: 0.323383
 * <p/>
 * and
 * <p/>
 * a: 0.345896
 * b: 0.327786
 * c: 0.323629
 * draw: 0.002689 (was neglected in above computation)
 * <p/>
 * Further testing for points (and random players!!):
 * Lead advantage test
 * a: 0.346615
 * b: 0.326473
 * c: 0.324309
 * draw: 0.002603
 * a points / iteration: 0.577123
 * b points / iteration: -0.23459
 * c points / iteration: -0.342809
 */
public class MichiganRummyLeadAdvantageTest {
    public static void main( String[] args ) {
        System.out.println( "Lead advantage test" );
        int iterations = 1000000;

        MichiganRummyGame game = new MichiganRummyGame();

        MichiganRummyPlayer a = new MichiganRummyRandomPlayer();
        MichiganRummyPlayer b = new MichiganRummyRandomPlayer();
        MichiganRummyPlayer c = new MichiganRummyRandomPlayer();

        game.addPlayer( a );
        game.addPlayer( b );
        game.addPlayer( c );

        int aWins = 0;
        int bWins = 0;
        int cWins = 0;
        int draws = 0;

        for ( int i = 0; i < iterations; i++ ) {

            game.deal();

            game.setLead( a );

            game.playGame();

            if ( !game.isDraw() ) {
                MichiganRummyPlayer winner = game.getWinner();

                if ( winner == a ) {
                    aWins++;
                }
                else if ( winner == b ) {
                    bWins++;
                }
                else if ( winner == c ) {
                    cWins++;
                }
            }
            else {
                draws++;
            }
        }

        double aFraction = (double) aWins / (double) iterations;
        double bFraction = (double) bWins / (double) iterations;
        double cFraction = (double) cWins / (double) iterations;
        double drawFraction = (double) draws / (double) iterations;

        System.out.println( "a: " + aFraction );
        System.out.println( "b: " + bFraction );
        System.out.println( "c: " + cFraction );
        System.out.println( "draw: " + drawFraction );

        System.out.println( "a points / iteration: " + (double) a.getPoints() / (double) iterations );
        System.out.println( "b points / iteration: " + (double) b.getPoints() / (double) iterations );
        System.out.println( "c points / iteration: " + (double) c.getPoints() / (double) iterations );
    }
}
