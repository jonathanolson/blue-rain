package com.jonathanolson.games.cards.michiganrummy.tests;

import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyPossibleRunPlayer;
import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyRandomPlayer;

/**
 * Possible run advantage test
 * a: 0.338878
 * b: 0.330472
 * c: 0.328112
 * draw: 0.002538
 */
public class MichiganRummyPossibleRunAdvantageTest {
    public static void main( String[] args ) {
        MichiganRummyPlayerTest test = new MichiganRummyPlayerTest();

        test.addPlayer( new MichiganRummyPossibleRunPlayer() );
        test.addPlayer( new MichiganRummyRandomPlayer() );
        test.addPlayer( new MichiganRummyRandomPlayer() );

        test.run();
    }
}