package com.jonathanolson.games.cards.michiganrummy.tests;

import com.jonathanolson.games.cards.michiganrummy.MichiganRummyGame;
import com.jonathanolson.games.cards.michiganrummy.players.MichiganRummyRandomPlayer;

public class MichiganRummyGameTest {
    public static void main( String[] args ) {
        MichiganRummyGame game = new MichiganRummyGame();

        game.addPlayer( new MichiganRummyRandomPlayer( "Alice" ) );
        game.addPlayer( new MichiganRummyRandomPlayer( "Bob" ) );
        game.addPlayer( new MichiganRummyRandomPlayer( "Charles" ) );

        game.setDebug( true );

        int iterations = 10;

        for ( int i = 0; i < iterations; i++ ) {

            System.out.println( "\n\n********* DEALING\n" );

            game.deal();

            System.out.println( game );

            game.playGame();
        }
    }
}
