package com.jonathanolson.games.cards.michiganrummy;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.games.cards.*;

public class MichiganRummyGame {

    private List<MichiganRummyPlayer> players;
    private MichiganRummyPile pile;
    private MichiganRummyPlayer lead;
    private MichiganRummyPlayer lastPlayed;
    private int lastDealtIndex = 0;

    private boolean finished = false;
    private boolean draw = false;
    private MichiganRummyPlayer winner;

    private Hand dumpedHand;

    private boolean debug = false;
    private Deck deck;

    private int tenPot = 0;
    private int jackPot = 0;
    private int queenPot = 0;
    private int kingPot = 0;
    private int acePot = 0;
    private int eightNineTenPot = 0;
    private int queenKingPot = 0;
    private int kittyPot = 0;

    private boolean justPlayedQueenHearts = false;
    private boolean justPlayedEight = false;
    private boolean justPlayedEightNine = false;
    private Suit eightNineTenSuit = null;
    private MichiganRummyPlayer bonusPlayer = null;

    public MichiganRummyGame() {
        players = new LinkedList<MichiganRummyPlayer>();
    }

    public void addPlayer( MichiganRummyPlayer player ) {
        if ( players.isEmpty() ) {
            lead = player;
        }
        players.add( player );
    }

    public void deal() {
        finished = false;
        draw = false;
        winner = null;
        pile = new MichiganRummyPile();
        deck = new StandardDeck();

        deck.shuffle();
        List<Hand> hands = deck.deal( players.size() + 1 );
        for ( int i = 0; i < players.size(); i++ ) {
            MichiganRummyPlayer player = players.get( i );
            player.setHand( hands.get( i ) );
        }
        dumpedHand = hands.get( players.size() );

        // change the deal to the next player

        lastDealtIndex++;
        if ( lastDealtIndex >= players.size() ) {
            lastDealtIndex = 0;
        }
        lead = players.get( lastDealtIndex );

        ante();

    }

    private void ante() {
        for ( MichiganRummyPlayer player : players ) {
            player.subtractPoints( 8 );

            tenPot++;
            jackPot++;
            queenPot++;
            kingPot++;
            acePot++;

            eightNineTenPot++;
            queenKingPot++;

            kittyPot++;
        }
    }

    public void playGame() {
        while ( !finished ) {
            playerStart( lead );

            if ( finished ) {
                break;
            }

            continuePlay();

            if ( debug ) {
                System.out.println( "\n" + getPlayersString() );
            }

            if ( finished ) {
                break;
            }

            lead = lastPlayed;
        }

        if ( debug ) {
            if ( draw ) {
                System.out.println( "DRAW!!!" );
            }
            else {
                if ( winner.getName() != null ) {
                    System.out.println( winner.getName() + " WINS!!!" );
                }
            }
        }

        if ( !draw ) {
            winner.addPoints( kittyPot );
            if ( debug ) {
                System.out.println( "Player " + winner.getName() + " wins kitty of " + kittyPot );
            }
            kittyPot = 0;

            for ( MichiganRummyPlayer player : players ) {
                if ( player == winner ) {
                    continue;
                }

                int amount = player.getHand().getCards().size();
                if ( debug ) {
                    System.out.println( "Player " + player.getName() + " had " + amount + " cards left" );
                }
                winner.addPoints( amount );
                player.subtractPoints( amount );
            }
        }
    }

    private void playerStart( MichiganRummyPlayer player ) {
        boolean played = player.attemptPlay( this, true );
        if ( played ) {
            lastPlayed = player;
            if ( checkWin( player ) ) {
                return;
            }
        }

        MichiganRummyPlayer curPlayer = getNextPlayer( player );
        while ( !played && curPlayer != player ) {
            played = curPlayer.attemptPlay( this, true );
            if ( played ) {
                lastPlayed = curPlayer;
                if ( checkWin( curPlayer ) ) {
                    return;
                }
            }
            curPlayer = getNextPlayer( curPlayer );
        }

        if ( !played ) {
            finished = true;
            draw = true;
        }
    }

    private MichiganRummyPlayer getNextPlayer( MichiganRummyPlayer player ) {
        int idx = players.indexOf( player );
        idx++;
        if ( idx >= players.size() ) {
            idx = 0;
        }
        return players.get( idx );
    }

    private void continuePlay() {
        boolean rolling = true;

        while ( rolling ) {
            rolling = false;

            for ( MichiganRummyPlayer player : players ) {
                boolean played = player.attemptPlay( this, false );
                if ( played ) {
                    lastPlayed = player;
                }

                if ( checkWin( player ) ) {
                    return;
                }

                if ( played ) {
                    rolling = true;
                    break;
                }
            }
        }

    }

    public void playCard( MichiganRummyPlayer player, Card card ) {

        pile.setLastCard( card );
        player.getHand().removeCard( card );

        if ( justPlayedQueenHearts && bonusPlayer == player && card.getSuit() == Suit.HEART && card.getRank() == Rank.KING ) {
            player.addPoints( queenKingPot );
            if ( player.getName() != null ) {
                System.out.println( "Player " + player.getName() + " wins the queen-king of hearts for " + queenKingPot + " points" );
            }
            queenKingPot = 0;
        }

        if ( justPlayedEightNine && bonusPlayer == player && card.getRank() == Rank.TEN && card.getSuit() == eightNineTenSuit ) {
            player.addPoints( eightNineTenPot );
            if ( player.getName() != null ) {
                System.out.println( "Player " + player.getName() + " wins the eight-nine-ten for " + eightNineTenPot + " points" );
            }
            eightNineTenPot = 0;
        }

        if ( card.equals( Rank.QUEEN, Suit.HEART ) ) {
            justPlayedQueenHearts = true;
            bonusPlayer = player;
        }
        else {
            justPlayedQueenHearts = false;
        }

        if ( justPlayedEight && bonusPlayer == player && card.getRank() == Rank.NINE && card.getSuit() == eightNineTenSuit ) {
            justPlayedEightNine = true;
        }
        else {
            justPlayedEightNine = false;
        }

        if ( card.getRank() == Rank.EIGHT ) {
            justPlayedEight = true;
            bonusPlayer = player;
            eightNineTenSuit = card.getSuit();
        }
        else {
            justPlayedEight = false;
        }

        if ( card.getSuit() == Suit.HEART ) {
            switch( card.getRank() ) {
                case TEN:
                    player.addPoints( tenPot );
                    if ( player.getName() != null ) {
                        System.out.println( "Player " + player.getName() + " wins the ten of hearts for " + tenPot + " points" );
                    }
                    tenPot = 0;
                    break;
                case JACK:
                    player.addPoints( jackPot );
                    if ( player.getName() != null ) {
                        System.out.println( "Player " + player.getName() + " wins the jack of hearts for " + jackPot + " points" );
                    }
                    jackPot = 0;
                    break;
                case QUEEN:
                    player.addPoints( queenPot );
                    if ( player.getName() != null ) {
                        System.out.println( "Player " + player.getName() + " wins the queen of hearts for " + queenPot + " points" );
                    }
                    queenPot = 0;
                    break;
                case KING:
                    player.addPoints( kingPot );
                    if ( player.getName() != null ) {
                        System.out.println( "Player " + player.getName() + " wins the king of hearts for " + kingPot + " points" );
                    }
                    kingPot = 0;
                    break;
                case ACE:
                    player.addPoints( acePot );
                    if ( player.getName() != null ) {
                        System.out.println( "Player " + player.getName() + " wins the ace of hearts for " + acePot + " points" );
                    }
                    acePot = 0;
                    break;
            }
        }
    }

    private boolean checkWin( MichiganRummyPlayer player ) {
        if ( player.getHand().getCards().isEmpty() ) {
            finished = true;
            winner = player;
        }

        return finished;
    }

    public String getPlayersString() {
        String ret = "";
        for ( MichiganRummyPlayer player : players ) {
            ret += player + "\n";
        }
        return ret;
    }

    public String toString() {
        String ret = "";
        ret += "Kitty:      " + kittyPot + "\n";
        ret += "Ten:        " + tenPot + "\n";
        ret += "Jack:       " + jackPot + "\n";
        ret += "Queen:      " + queenPot + "\n";
        ret += "King:       " + kingPot + "\n";
        ret += "Ace:        " + acePot + "\n";
        ret += "8-9-10:     " + eightNineTenPot + "\n";
        ret += "Queen-King: " + queenKingPot + "\n";
        ret += getPlayersString();
        return ret;
    }

    /////
    // Getters and setters
    /////

    public MichiganRummyPlayer getLead() {
        return lead;
    }

    public void setLead( MichiganRummyPlayer lead ) {
        this.lead = lead;
    }

    public List<MichiganRummyPlayer> getPlayers() {
        return players;
    }

    public MichiganRummyPile getPile() {
        return pile;
    }

    public MichiganRummyPlayer getLastPlayed() {
        return lastPlayed;
    }

    public MichiganRummyPlayer getWinner() {
        return winner;
    }

    public boolean isDraw() {
        return draw;
    }

    public Hand getDumpedHand() {
        return dumpedHand;
    }

    public boolean isFinished() {
        return finished;
    }

    public boolean isDebug() {
        return debug;
    }

    public void setDebug( boolean debug ) {
        this.debug = debug;
    }

    public Deck getDeck() {
        return deck;
    }

    public int getTenPot() {
        return tenPot;
    }

    public int getJackPot() {
        return jackPot;
    }

    public int getQueenPot() {
        return queenPot;
    }

    public int getKingPot() {
        return kingPot;
    }

    public int getAcePot() {
        return acePot;
    }

    public int getEightNineTenPot() {
        return eightNineTenPot;
    }

    public int getQueenKingPot() {
        return queenKingPot;
    }
}
