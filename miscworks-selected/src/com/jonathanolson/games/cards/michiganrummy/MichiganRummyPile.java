package com.jonathanolson.games.cards.michiganrummy;

import com.jonathanolson.games.cards.Card;

public class MichiganRummyPile {

    private Card lastCard;

    public MichiganRummyPile() {
        lastCard = null;
    }

    public Card getLastCard() {
        return lastCard;
    }

    public void setLastCard( Card lastCard ) {
        this.lastCard = lastCard;
    }
}
