package com.jonathanolson.games.cards;

public class StandardCardOrder extends CardOrder {

    private static Rank[] RANKS = {Rank.TWO, Rank.THREE, Rank.FOUR, Rank.FIVE, Rank.SIX, Rank.SEVEN, Rank.EIGHT, Rank.NINE,
            Rank.TEN, Rank.JACK, Rank.QUEEN, Rank.KING, Rank.ACE};
    private static Suit[] SUITS = {Suit.CLUB, Suit.DIAMOND, Suit.HEART, Suit.SPADE};

    public Rank getLowRank() {
        return Rank.TWO;
    }

    public Rank getHighRank() {
        return Rank.ACE;
    }

    public Suit getLowSuit() {
        return Suit.CLUB;
    }

    public Suit getHighSuit() {
        return Suit.SPADE;
    }

    public Rank getNextRank( Rank rank ) {
        switch( rank ) {
            case TWO:
                return Rank.THREE;
            case THREE:
                return Rank.FOUR;
            case FOUR:
                return Rank.FIVE;
            case FIVE:
                return Rank.SIX;
            case SIX:
                return Rank.SEVEN;
            case SEVEN:
                return Rank.EIGHT;
            case EIGHT:
                return Rank.NINE;
            case NINE:
                return Rank.TEN;
            case TEN:
                return Rank.JACK;
            case JACK:
                return Rank.QUEEN;
            case QUEEN:
                return Rank.KING;
            case KING:
                return Rank.ACE;
            case ACE:
                return Rank.TWO;
            default:
                throw new RuntimeException( "Rank not handled: " + rank );
        }
    }

    public Rank getPreviousRank( Rank rank ) {
        switch( rank ) {
            case TWO:
                return Rank.ACE;
            case THREE:
                return Rank.TWO;
            case FOUR:
                return Rank.THREE;
            case FIVE:
                return Rank.FOUR;
            case SIX:
                return Rank.FIVE;
            case SEVEN:
                return Rank.SIX;
            case EIGHT:
                return Rank.SEVEN;
            case NINE:
                return Rank.EIGHT;
            case TEN:
                return Rank.NINE;
            case JACK:
                return Rank.TEN;
            case QUEEN:
                return Rank.JACK;
            case KING:
                return Rank.QUEEN;
            case ACE:
                return Rank.KING;
            default:
                throw new RuntimeException( "Rank not handled: " + rank );
        }

    }

    public Suit getNextSuit( Suit suit ) {
        switch( suit ) {
            case CLUB:
                return Suit.DIAMOND;
            case DIAMOND:
                return Suit.HEART;
            case HEART:
                return Suit.SPADE;
            case SPADE:
                return Suit.CLUB;
            default:
                throw new RuntimeException( "Suit not handled: " + suit );
        }
    }

    public Suit getPreviousSuit( Suit suit ) {
        switch( suit ) {
            case CLUB:
                return Suit.SPADE;
            case DIAMOND:
                return Suit.CLUB;
            case HEART:
                return Suit.DIAMOND;
            case SPADE:
                return Suit.HEART;
            default:
                throw new RuntimeException( "Suit not handled: " + suit );
        }
    }

    public int compareRank( Rank a, Rank b ) {
        if ( a == b ) {
            return 0;
        }

        for ( int i = 0; i < RANKS.length; i++ ) {
            Rank rank = RANKS[i];

            if ( rank == a ) {
                return -1;
            }
            else if ( rank == b ) {
                return 1;
            }
        }

        throw new RuntimeException( "Rank not handled!" );
    }

    public int compareSuit( Suit a, Suit b ) {
        if ( a == b ) {
            return 0;
        }

        for ( int i = 0; i < SUITS.length; i++ ) {
            Suit suit = SUITS[i];

            if ( suit == a ) {
                return -1;
            }
            else if ( suit == b ) {
                return 1;
            }
        }

        throw new RuntimeException( "Suit not handled!" );
    }
}
