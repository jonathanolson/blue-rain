package com.jonathanolson.games.cards;

public class Card {

    private Rank rank;
    private Suit suit;

    public Card( Rank rank, Suit suit ) {
        this.rank = rank;
        this.suit = suit;
    }

    public boolean equals( Rank rank, Suit suit ) {
        return this.rank == rank && this.suit == suit;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank( Rank rank ) {
        this.rank = rank;
    }

    public Suit getSuit() {
        return suit;
    }

    public void setSuit( Suit suit ) {
        this.suit = suit;
    }

    public String toString() {
        String ret = "";
        switch( rank ) {
            case TWO:
                ret += "2 ";
                break;
            case THREE:
                ret += "3 ";
                break;
            case FOUR:
                ret += "4 ";
                break;
            case FIVE:
                ret += "5 ";
                break;
            case SIX:
                ret += "6 ";
                break;
            case SEVEN:
                ret += "7 ";
                break;
            case EIGHT:
                ret += "8 ";
                break;
            case NINE:
                ret += "9 ";
                break;
            case TEN:
                ret += "10";
                break;
            case JACK:
                ret += "J ";
                break;
            case QUEEN:
                ret += "Q ";
                break;
            case KING:
                ret += "K ";
                break;
            case ACE:
                ret += "A ";
                break;
        }
        switch( suit ) {
            case CLUB:
                ret += "C";
                break;
            case DIAMOND:
                ret += "D";
                break;
            case HEART:
                ret += "H";
                break;
            case SPADE:
                ret += "S";
                break;
        }
        return ret;
    }

    public String getRankString() {
        switch( rank ) {
            case TWO:
                return "2";
            case THREE:
                return "3";
            case FOUR:
                return "4";
            case FIVE:
                return "5";
            case SIX:
                return "6";
            case SEVEN:
                return "7";
            case EIGHT:
                return "8";
            case NINE:
                return "9";
            case TEN:
                return "10";
            case JACK:
                return "J";
            case QUEEN:
                return "Q";
            case KING:
                return "K";
            case ACE:
                return "A";
        }

        throw new RuntimeException( "Unhandled rank: " + rank );
    }
}
