package com.jonathanolson.games.cards;

import java.util.LinkedList;
import java.util.List;

public class StandardDeck extends Deck {

    public StandardDeck() {
        List<Card> cards = new LinkedList<Card>();
        CardOrder order = new StandardCardOrder();

        for ( Suit suit : order.getOrderedSuitList() ) {
            for ( Rank rank : order.getOrderedRankList() ) {
                cards.add( new Card( rank, suit ) );
            }
        }

        initialize( cards );

    }

    private static class DeckTest {
        public static void main( String[] args ) {
            Deck deck = new StandardDeck();
            deck.shuffle();
            System.out.println( "Shuffled deck:" );
            System.out.println( deck );

            List<Hand> hands = deck.deal( 4 );
            for ( Hand hand : hands ) {
                System.out.println( "Hand:" );
                hand.order();
                System.out.println( hand );
            }
        }
    }
}
