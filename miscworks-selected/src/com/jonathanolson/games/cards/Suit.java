package com.jonathanolson.games.cards;

public enum Suit {
    SPADE, HEART, DIAMOND, CLUB
}
