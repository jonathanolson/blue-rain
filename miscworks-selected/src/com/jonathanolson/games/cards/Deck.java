package com.jonathanolson.games.cards;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import com.jonathanolson.games.cards.michiganrummy.MichiganRummyPlayer;

public abstract class Deck {

    private List<Card> cards;

    private static Random random;

    public void initialize( List<Card> cards ) {
        random = new Random( System.currentTimeMillis() );
        this.cards = cards;
    }

    public void shuffle() {
        Collections.shuffle( cards, random );
    }

    public void dealPlayers( List<MichiganRummyPlayer> players ) {
        List<Hand> hands = deal( players.size() );

        for ( int i = 0; i < hands.size(); i++ ) {
            Hand hand = hands.get( i );
            MichiganRummyPlayer player = players.get( i );

            player.setHand( hand );
        }
    }

    public List<Hand> deal( int numHands ) {
        List<Hand> hands = new LinkedList<Hand>();

        for ( int i = 0; i < numHands; i++ ) {
            hands.add( new Hand() );
        }

        while ( cards.size() >= numHands ) {
            for ( Hand hand : hands ) {
                hand.addCard( pop() );
            }
        }

        return hands;
    }

    public Card pop() {
        if ( cards.size() == 0 ) {
            throw new RuntimeException( "Card pop on empty deck" );
        }
        Card ret = cards.get( 0 );
        cards.remove( 0 );
        return ret;
    }

    public List<Card> getCards() {
        return cards;
    }

    public String toString() {
        String ret = "";
        int count = 0;
        int totalCount = 0;
        for ( Card card : cards ) {
            if ( count > 0 ) {
                ret += "    ";
            }
            else if ( count == 0 && totalCount != 0 ) {
                ret += "\n";
            }

            ret += card;

            count++;
            if ( count > 6 ) {
                count = 0;
            }

            totalCount++;
        }
        return ret;
    }

}
