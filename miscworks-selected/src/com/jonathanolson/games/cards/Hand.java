package com.jonathanolson.games.cards;

import java.util.LinkedList;
import java.util.List;

public class Hand {

    private List<Card> cards;

    public Hand() {
        cards = new LinkedList<Card>();
    }

    public void addCard( Card card ) {
        cards.add( card );
    }

    public void removeCard( Card card ) {
        cards.remove( card );
    }

    public void order() {
        order( new StandardCardOrder() );
    }

    public void order( CardOrder order ) {
        order.orderCardsHighToLow( cards );
    }

    public List<Card> getCards() {
        List<Card> ret = new LinkedList<Card>();
        ret.addAll( cards );
        return ret;
    }

    public String toString() {
//        String ret = "";
//        int count = 0;
//
//        for ( Card card : cards ) {
//            if( count > 0 ) {
//                ret += "   ";
//            }
//
//            ret += card;
//
//            count++;
//        }
//
//        return ret;
        String ret = "";

        List<Card> sortedCards = new LinkedList<Card>();
        sortedCards.addAll( cards );
        new StandardCardOrder().orderCardsHighToLow( sortedCards );

        Suit suit = null;

        for ( Card card : sortedCards ) {
            if ( card.getSuit() != suit ) {
                if ( suit != null ) {
                    ret += "  ";
                }
                suit = card.getSuit();

                switch( suit ) {
                    case CLUB:
                        ret += "C:";
                        break;
                    case DIAMOND:
                        ret += "D:";
                        break;
                    case HEART:
                        ret += "H:";
                        break;
                    case SPADE:
                        ret += "S:";
                        break;
                }
            }

            ret += " ";
            ret += card.getRankString();
        }

        return ret;
    }
}
