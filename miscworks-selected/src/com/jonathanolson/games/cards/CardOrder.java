package com.jonathanolson.games.cards;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public abstract class CardOrder {

    public abstract Rank getLowRank();

    public abstract Rank getHighRank();

    public abstract Suit getLowSuit();

    public abstract Suit getHighSuit();

    public abstract Rank getNextRank( Rank rank );

    public abstract Rank getPreviousRank( Rank rank );

    public abstract Suit getNextSuit( Suit suit );

    public abstract Suit getPreviousSuit( Suit suit );

    public abstract int compareRank( Rank a, Rank b );

    public abstract int compareSuit( Suit a, Suit b );

    /**
     * Full visual card order
     *
     * @param a A card
     * @param b Another card
     * @return Standard comparison value
     */
    public int compareCards( Card a, Card b ) {
        int ret = compareSuit( a.getSuit(), b.getSuit() );
        if ( ret != 0 ) {
            return ret;
        }
        return compareRank( a.getRank(), b.getRank() );
    }

    public List<Rank> getOrderedRankList() {
        List<Rank> ret = new LinkedList<Rank>();

        Rank highRank = getHighRank();
        Rank rank = getLowRank();

        ret.add( rank );

        while ( rank != highRank ) {
            rank = getNextRank( rank );
            ret.add( rank );
        }

        return ret;
    }

    public List<Suit> getOrderedSuitList() {
        List<Suit> ret = new LinkedList<Suit>();

        Suit highSuit = getHighSuit();
        Suit suit = getLowSuit();

        ret.add( suit );

        while ( suit != highSuit ) {
            suit = getNextSuit( suit );
            ret.add( suit );
        }

        return ret;
    }

    public void orderCardsHighToLow( List<Card> cards ) {
        Collections.sort( cards, new Comparator<Card>() {
            public int compare( Card a, Card b ) {
                return -compareCards( a, b );
            }
        } );
    }

    public void orderCardsLowToHigh( List<Card> cards ) {
        Collections.sort( cards, new Comparator<Card>() {
            public int compare( Card a, Card b ) {
                return compareCards( a, b );
            }
        } );
    }

    public void orderRanksHighToLow( List<Card> cards ) {
        Collections.sort( cards, new Comparator<Card>() {
            public int compare( Card a, Card b ) {
                return -compareRank( a.getRank(), b.getRank() );
            }
        } );
    }

    public void orderRanksLowToHigh( List<Card> cards ) {
        Collections.sort( cards, new Comparator<Card>() {
            public int compare( Card a, Card b ) {
                return compareRank( a.getRank(), b.getRank() );
            }
        } );
    }
}
