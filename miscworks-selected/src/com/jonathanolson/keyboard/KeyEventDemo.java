package com.jonathanolson.keyboard;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

public class KeyEventDemo extends JFrame implements KeyListener, ActionListener {
    JTextArea displayArea;
    JTextField typingArea;
    static final String newline = System.getProperty( "line.separator" );

    public static void main( String[] args ) {
        try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
            UIManager.setLookAndFeel( "javax.swing.plaf.metal.MetalLookAndFeel" );
        }
        catch( UnsupportedLookAndFeelException ex ) {
            ex.printStackTrace();
        }
        catch( IllegalAccessException ex ) {
            ex.printStackTrace();
        }
        catch( InstantiationException ex ) {
            ex.printStackTrace();
        }
        catch( ClassNotFoundException ex ) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put( "swing.boldMetal", Boolean.FALSE );

        //Schedule a job for event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        } );
    }

    private static void createAndShowGUI() {
        //Create and set up the window.
        KeyEventDemo frame = new KeyEventDemo( "KeyEventDemo" );
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        JButton button = new JButton( "Clear" );
        button.addActionListener( frame );

        frame.typingArea = new JTextField( 20 );
        frame.typingArea.addKeyListener( frame );

        frame.typingArea.setFocusTraversalKeysEnabled( false );

        frame.displayArea = new JTextArea();
        frame.displayArea.setEditable( false );
        JScrollPane scrollPane = new JScrollPane( frame.displayArea );
        scrollPane.setPreferredSize( new Dimension( 375, 125 ) );

        frame.getContentPane().add( frame.typingArea, BorderLayout.PAGE_START );
        frame.getContentPane().add( scrollPane, BorderLayout.CENTER );
        frame.getContentPane().add( button, BorderLayout.PAGE_END );

        //Display the window.
        frame.pack();
        frame.setVisible( true );
    }

    public KeyEventDemo( String name ) {
        super( name );
    }


    public void keyTyped( KeyEvent e ) {
        displayInfo( e, "KEY TYPED: " );
    }

    public void keyPressed( KeyEvent e ) {
        displayInfo( e, "KEY PRESSED: " );
    }

    public void keyReleased( KeyEvent e ) {
        displayInfo( e, "KEY RELEASED: " );
    }

    public void actionPerformed( ActionEvent e ) {
        //Clear the text components.
        displayArea.setText( "" );
        typingArea.setText( "" );

        //Return the focus to the typing area.
        typingArea.requestFocusInWindow();
    }

    private void displayInfo( KeyEvent e, String keyStatus ) {
        String str = "";

        int id = e.getID();

        str += keyStatus + "\n";

        if ( id == KeyEvent.KEY_TYPED ) {
            str += "    char: " + e.getKeyChar() + "\n";
        }
        else {
            str += "    code: " + e.getKeyCode() + "\n";
            str += "    " + KeyEvent.getKeyText( e.getKeyCode() ) + "\n";
        }

        int modifiersEx = e.getModifiersEx();
        String modString = "    " + modifiersEx;
        String tmpString = KeyEvent.getModifiersExText( modifiersEx );
        if ( tmpString.length() > 0 ) {
            modString += " (" + tmpString + ")";
        }
        else {
            modString += " (no extended modifiers)";
        }
        str += modString;

        /*
        String actionString = "action key? ";
        if ( e.isActionKey() ) {
            actionString += "YES";
        }
        else {
            actionString += "NO";
        }
        */

        str += "    ";
        int location = e.getKeyLocation();
        if ( location == KeyEvent.KEY_LOCATION_STANDARD ) {
            str += "standard";
        }
        else if ( location == KeyEvent.KEY_LOCATION_LEFT ) {
            str += "left";
        }
        else if ( location == KeyEvent.KEY_LOCATION_RIGHT ) {
            str += "right";
        }
        else if ( location == KeyEvent.KEY_LOCATION_NUMPAD ) {
            str += "numpad";
        }
        else { // (location == KeyEvent.KEY_LOCATION_UNKNOWN)
            str += "unknown";
        }
        str += "\n";


        displayArea.append( str + "\n" );
        displayArea.setCaretPosition( displayArea.getDocument().getLength() );

    }
}
