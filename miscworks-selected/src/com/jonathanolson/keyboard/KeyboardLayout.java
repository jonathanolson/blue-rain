package com.jonathanolson.keyboard;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.jonathanolson.keyboard.model.Key;
import com.jonathanolson.util.FileUtils;

public class KeyboardLayout {
    private KeyBehavior[] behaviors;
    private Map<Character, List<KeyLocation>> charmap = new HashMap<Character, List<KeyLocation>>();

    // TODO: abstract out key behavior better? (for repeat-key press and other custom key codes?)

    public KeyboardLayout() {
        behaviors = new KeyBehavior[Key.numKeys()];
    }

    public void addKey( Key key, KeyBehavior behavior ) {
        behaviors[key.ordinal()] = behavior;
        if ( !behavior.isActionKey() ) {
            addCharKeys( key, behavior );
        }
    }

    public KeyBehavior getKeyBehavior( Key key ) {
        return behaviors[key.ordinal()];
    }

    public List<KeyLocation> getCharKeys( Character chr ) {
        return charmap.get( chr );
    }

    @Override
    public String toString() {
        StringBuffer buf = new StringBuffer();
        for ( Key key : Key.values() ) {
            buf.append( key.name() + " " + getKeyBehavior( key ) + "\n" );
        }
        return buf.toString();
    }

    public static KeyboardLayout fromString( String str ) {
        KeyboardLayout layout = new KeyboardLayout();
        String[] lines = str.split( "\n" );
        for ( String line : lines ) {
            if ( line == null || line.length() == 0 || line.indexOf( " " ) <= 0 ) {
                break;
            }
            int sidx = line.indexOf( " " );
            String prefix = line.substring( 0, sidx );
            String postfix = line.substring( sidx + 1 );
            Key key = Key.valueOf( prefix );
            KeyBehavior behavior = KeyBehavior.fromString( postfix );
            if ( behavior != null ) {
                layout.addKey( key, behavior );
            }
        }
        return layout;
    }

    @Override
    public int hashCode() {
        int x = 0;
        for ( Key key : Key.values() ) {
            if ( getKeyBehavior( key ) != null ) {
                x += ( key.ordinal() * 47 + 7 ) * getKeyBehavior( key ).hashCode();
            }
        }
        return x;
    }

    @Override
    public boolean equals( Object o ) {
        if ( o instanceof KeyboardLayout ) {
            KeyboardLayout layout = (KeyboardLayout) o;
            for ( Key key : Key.values() ) {
                if ( layout.getKeyBehavior( key ) != getKeyBehavior( key ) ) {
                    return false;
                }
            }
            return true;
        }
        else {
            return false;
        }
    }

    private void addCharKeys( Key key, KeyBehavior behavior ) {
        if ( behavior.getMain() != null ) {
            addToCharMap( behavior.getMain(), new KeyLocation( key, false, false ) );
        }
        if ( behavior.getCap() != null ) {
            addToCharMap( behavior.getCap(), new KeyLocation( key, true, false ) );
        }
        if ( behavior.getAgmain() != null ) {
            addToCharMap( behavior.getAgmain(), new KeyLocation( key, false, true ) );
        }
        if ( behavior.getAgcap() != null ) {
            addToCharMap( behavior.getAgcap(), new KeyLocation( key, true, true ) );
        }
    }

    private void addToCharMap( char chr, KeyLocation location ) {
        List<KeyLocation> locations = charmap.get( chr );
        if ( locations == null ) {
            locations = new LinkedList<KeyLocation>();
            charmap.put( chr, locations );
        }
        locations.add( location );
    }

    public static class Standard extends KeyboardLayout {
        public Standard() {
            addKey( Key.TLDE, new KeyBehavior( '`', '~', null, null ) );
            addKey( Key.AE01, new KeyBehavior( '1', '!', null, null ) );
            addKey( Key.AE02, new KeyBehavior( '2', '@', null, null ) );
            addKey( Key.AE03, new KeyBehavior( '3', '#', null, null ) );
            addKey( Key.AE04, new KeyBehavior( '4', '$', null, null ) );
            addKey( Key.AE05, new KeyBehavior( '5', '%', null, null ) );
            addKey( Key.AE06, new KeyBehavior( '6', '^', null, null ) );
            addKey( Key.AE07, new KeyBehavior( '7', '&', null, null ) );
            addKey( Key.AE08, new KeyBehavior( '8', '*', null, null ) );
            addKey( Key.AE09, new KeyBehavior( '9', '(', null, null ) );
            addKey( Key.AE10, new KeyBehavior( '0', ')', null, null ) );
            addKey( Key.AE11, new KeyBehavior( '-', '_', null, null ) );
            addKey( Key.AE12, new KeyBehavior( '=', '+', null, null ) );
            addKey( Key.BKSP, new KeyBehavior( KeyAction.BACKSPACE ) );

            addKey( Key.TAB, new KeyBehavior( KeyAction.TAB ) );
            addKey( Key.AD01, new KeyBehavior( 'q', 'Q', null, null ) );
            addKey( Key.AD02, new KeyBehavior( 'w', 'W', null, null ) );
            addKey( Key.AD03, new KeyBehavior( 'e', 'E', null, null ) );
            addKey( Key.AD04, new KeyBehavior( 'r', 'R', null, null ) );
            addKey( Key.AD05, new KeyBehavior( 't', 'T', null, null ) );
            addKey( Key.AD06, new KeyBehavior( 'y', 'Y', null, null ) );
            addKey( Key.AD07, new KeyBehavior( 'u', 'U', null, null ) );
            addKey( Key.AD08, new KeyBehavior( 'i', 'I', null, null ) );
            addKey( Key.AD09, new KeyBehavior( 'o', 'O', null, null ) );
            addKey( Key.AD10, new KeyBehavior( 'p', 'P', null, null ) );
            addKey( Key.AD11, new KeyBehavior( '[', '{', null, null ) );
            addKey( Key.AD12, new KeyBehavior( ']', '}', null, null ) );
            addKey( Key.BKSL, new KeyBehavior( '\\', '|', null, null ) );

            addKey( Key.CAPS, new KeyBehavior( KeyAction.CAPS_LOCK ) );
            addKey( Key.AC01, new KeyBehavior( 'a', 'A', null, null ) );
            addKey( Key.AC02, new KeyBehavior( 's', 'S', null, null ) );
            addKey( Key.AC03, new KeyBehavior( 'd', 'D', null, null ) );
            addKey( Key.AC04, new KeyBehavior( 'f', 'F', null, null ) );
            addKey( Key.AC05, new KeyBehavior( 'g', 'G', null, null ) );
            addKey( Key.AC06, new KeyBehavior( 'h', 'H', null, null ) );
            addKey( Key.AC07, new KeyBehavior( 'j', 'J', null, null ) );
            addKey( Key.AC08, new KeyBehavior( 'k', 'K', null, null ) );
            addKey( Key.AC09, new KeyBehavior( 'l', 'L', null, null ) );
            addKey( Key.AC10, new KeyBehavior( ';', ':', null, null ) );
            addKey( Key.AC11, new KeyBehavior( '\'', '"', null, null ) );
            addKey( Key.RTRN, new KeyBehavior( KeyAction.ENTER ) );

            addKey( Key.LFSH, new KeyBehavior( KeyAction.SHIFT ) );
            addKey( Key.AB01, new KeyBehavior( 'z', 'Z', null, null ) );
            addKey( Key.AB02, new KeyBehavior( 'x', 'X', null, null ) );
            addKey( Key.AB03, new KeyBehavior( 'c', 'C', null, null ) );
            addKey( Key.AB04, new KeyBehavior( 'v', 'V', null, null ) );
            addKey( Key.AB05, new KeyBehavior( 'b', 'B', null, null ) );
            addKey( Key.AB06, new KeyBehavior( 'n', 'N', null, null ) );
            addKey( Key.AB07, new KeyBehavior( 'm', 'M', null, null ) );
            addKey( Key.AB08, new KeyBehavior( ',', '<', null, null ) );
            addKey( Key.AB09, new KeyBehavior( '.', '>', null, null ) );
            addKey( Key.AB10, new KeyBehavior( '/', '?', null, null ) );
            addKey( Key.RTSH, new KeyBehavior( KeyAction.SHIFT ) );
            addKey( Key.LCTL, new KeyBehavior( KeyAction.CONTROL ) );
            addKey( Key.LWIN, new KeyBehavior( KeyAction.SUPER ) );
            addKey( Key.LALT, new KeyBehavior( KeyAction.ALT ) );
            addKey( Key.SPCE, new KeyBehavior( KeyAction.SPACE ) );
            addKey( Key.RALT, new KeyBehavior( KeyAction.ALT_GRAPH ) );
            addKey( Key.MENU, new KeyBehavior( KeyAction.MENU ) );
        }
    }

    public static class Colemak extends KeyboardLayout {
        public Colemak() {
            addKey( Key.TLDE, new KeyBehavior( '`', '~', null, null ) );
            addKey( Key.AE01, new KeyBehavior( '1', '!', null, null ) );
            addKey( Key.AE02, new KeyBehavior( '2', '@', null, null ) );
            addKey( Key.AE03, new KeyBehavior( '3', '#', null, null ) );
            addKey( Key.AE04, new KeyBehavior( '4', '$', null, null ) );
            addKey( Key.AE05, new KeyBehavior( '5', '%', null, null ) );
            addKey( Key.AE06, new KeyBehavior( '6', '^', null, null ) );
            addKey( Key.AE07, new KeyBehavior( '7', '&', null, null ) );
            addKey( Key.AE08, new KeyBehavior( '8', '*', null, null ) );
            addKey( Key.AE09, new KeyBehavior( '9', '(', null, null ) );
            addKey( Key.AE10, new KeyBehavior( '0', ')', null, null ) );
            addKey( Key.AE11, new KeyBehavior( '-', '_', null, null ) );
            addKey( Key.AE12, new KeyBehavior( '=', '+', null, null ) );
            addKey( Key.BKSP, new KeyBehavior( KeyAction.BACKSPACE ) );

            addKey( Key.TAB, new KeyBehavior( KeyAction.TAB ) );
            addKey( Key.AD01, new KeyBehavior( 'q', 'Q', null, null ) );
            addKey( Key.AD02, new KeyBehavior( 'w', 'W', null, null ) );
            addKey( Key.AD03, new KeyBehavior( 'f', 'F', null, null ) );
            addKey( Key.AD04, new KeyBehavior( 'p', 'P', null, null ) );
            addKey( Key.AD05, new KeyBehavior( 'g', 'G', null, null ) );
            addKey( Key.AD06, new KeyBehavior( 'j', 'J', null, null ) );
            addKey( Key.AD07, new KeyBehavior( 'l', 'L', null, null ) );
            addKey( Key.AD08, new KeyBehavior( 'u', 'U', null, null ) );
            addKey( Key.AD09, new KeyBehavior( 'y', 'Y', null, null ) );
            addKey( Key.AD10, new KeyBehavior( ';', ':', null, null ) );
            addKey( Key.AD11, new KeyBehavior( '[', '{', null, null ) );
            addKey( Key.AD12, new KeyBehavior( ']', '}', null, null ) );
            addKey( Key.BKSL, new KeyBehavior( '\\', '|', null, null ) );

            addKey( Key.CAPS, new KeyBehavior( KeyAction.BACKSPACE ) );
            addKey( Key.AC01, new KeyBehavior( 'a', 'A', null, null ) );
            addKey( Key.AC02, new KeyBehavior( 'r', 'R', null, null ) );
            addKey( Key.AC03, new KeyBehavior( 's', 'S', null, null ) );
            addKey( Key.AC04, new KeyBehavior( 't', 'T', null, null ) );
            addKey( Key.AC05, new KeyBehavior( 'd', 'D', null, null ) );
            addKey( Key.AC06, new KeyBehavior( 'h', 'H', null, null ) );
            addKey( Key.AC07, new KeyBehavior( 'n', 'N', null, null ) );
            addKey( Key.AC08, new KeyBehavior( 'e', 'E', null, null ) );
            addKey( Key.AC09, new KeyBehavior( 'i', 'I', null, null ) );
            addKey( Key.AC10, new KeyBehavior( 'o', 'O', null, null ) );
            addKey( Key.AC11, new KeyBehavior( '\'', '"', null, null ) );
            addKey( Key.RTRN, new KeyBehavior( KeyAction.ENTER ) );

            addKey( Key.LFSH, new KeyBehavior( KeyAction.SHIFT ) );
            addKey( Key.AB01, new KeyBehavior( 'z', 'Z', null, null ) );
            addKey( Key.AB02, new KeyBehavior( 'x', 'X', null, null ) );
            addKey( Key.AB03, new KeyBehavior( 'c', 'C', null, null ) );
            addKey( Key.AB04, new KeyBehavior( 'v', 'V', null, null ) );
            addKey( Key.AB05, new KeyBehavior( 'b', 'B', null, null ) );
            addKey( Key.AB06, new KeyBehavior( 'k', 'k', null, null ) );
            addKey( Key.AB07, new KeyBehavior( 'm', 'M', null, null ) );
            addKey( Key.AB08, new KeyBehavior( ',', '<', null, null ) );
            addKey( Key.AB09, new KeyBehavior( '.', '>', null, null ) );
            addKey( Key.AB10, new KeyBehavior( '/', '?', null, null ) );
            addKey( Key.RTSH, new KeyBehavior( KeyAction.SHIFT ) );
            addKey( Key.LCTL, new KeyBehavior( KeyAction.CONTROL ) );
            addKey( Key.LWIN, new KeyBehavior( KeyAction.SUPER ) );
            addKey( Key.LALT, new KeyBehavior( KeyAction.ALT ) );
            addKey( Key.SPCE, new KeyBehavior( KeyAction.SPACE ) );
            addKey( Key.RALT, new KeyBehavior( KeyAction.ALT_GRAPH ) );
            addKey( Key.MENU, new KeyBehavior( KeyAction.MENU ) );

        }
    }

    public static void main( String[] args ) {
        System.out.println( "Standard QWERTY layout:" );
        File keyboardDir = new File( "svn/trunk/data/keyboard/standard" );
        try {
            FileUtils.writeString( new File( keyboardDir, "qwerty-standard.kbd" ), ( new Standard() ).toString() );
            FileUtils.writeString( new File( keyboardDir, "colemak-standard.kbd" ), ( new Colemak() ).toString() );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    private static class TestInputOutput {
        public static void main( String[] args ) {
            for ( int count = 0; count < 500; count++ ) {
                KeyboardLayout layout = new KeyboardLayout();
                for ( Key key : Key.values() ) {
                    KeyBehavior behavior = KeyBehavior.semiRandom();
                    if ( behavior != null ) {
                        layout.addKey( key, behavior );
                    }
                }
                String str = layout.toString();
                KeyboardLayout out = KeyboardLayout.fromString( str );
                assert ( layout == out );
            }
        }
    }

}
