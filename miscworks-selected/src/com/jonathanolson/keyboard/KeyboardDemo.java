package com.jonathanolson.keyboard;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import com.jonathanolson.keyboard.model.Key;

public class KeyboardDemo extends JPanel implements KeyListener {
    private KeyboardComponent keyboard;
    private JTextArea text;
    private KeyboardLayout layout;

    public KeyboardDemo( KeyboardLayout layout ) {
        super( new GridBagLayout() );

        this.layout = layout;

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets( 5, 5, 5, 5 );

        keyboard = new KeyboardComponent( layout );
        add( keyboard, c );

        c.gridy = 1;
        c.fill = GridBagConstraints.BOTH;

        text = new JTextArea();
        JScrollPane pane = new JScrollPane( text );
        pane.setPreferredSize( KeyboardComponent.KEYBOARD_SIZE );
        add( pane, c );

        text.addKeyListener( this );
        text.setEditable( false );

    }

    public static void main( String[] args ) {
        //UIManager.put( "swing.boldMetal", Boolean.FALSE );
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JFrame frame = new JFrame( "KeyboardDemo Test" );
                frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

                JPanel panel = new JPanel( new FlowLayout() );
                frame.setContentPane( panel );

                panel.add( new KeyboardDemo( new KeyboardLayout.Standard() ) );
                panel.add( new KeyboardDemo( new KeyboardLayout.Colemak() ) );

                frame.setFocusTraversalKeysEnabled( false );

                frame.pack();
                frame.setVisible( true );
            }
        } );
    }

    @Override
    public void keyTyped( KeyEvent e ) {
    }

    @Override
    public void keyPressed( KeyEvent e ) {
        KeyNode node = keyboard.getKeyboardNode().getNodeFromKey( Key.fromEvent( e ) );
        if ( node == null ) {
            System.out.println( "Did not find key code: " + e.getKeyCode() );
        }
        else {
            node.setPressed( true );
        }

        KeyBehavior symbols = layout.getKeyBehavior( Key.fromEvent( e ) );
        if ( symbols.isActionKey() ) {
            switch( symbols.getAction() ) {
                case ENTER:
                    text.setText( text.getText() + "\n" );
                    break;
                case SPACE:
                    text.setText( text.getText() + " " );
                    break;
                case BACKSPACE:
                    if ( text.getText().length() > 0 ) {
                        text.setText( text.getText().substring( 0, text.getText().length() - 1 ) );
                    }
                    break;
            }
        }
        else {
            Character chr;
            if ( ( e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK ) == InputEvent.SHIFT_DOWN_MASK ) {
                if ( ( e.getModifiersEx() & InputEvent.ALT_GRAPH_DOWN_MASK ) == InputEvent.ALT_GRAPH_DOWN_MASK ) {
                    chr = symbols.getAgcap();
                }
                else {
                    chr = symbols.getCap();
                }
            }
            else {
                if ( ( e.getModifiersEx() & InputEvent.ALT_GRAPH_DOWN_MASK ) == InputEvent.ALT_GRAPH_DOWN_MASK ) {
                    chr = symbols.getAgmain();
                }
                else {
                    chr = symbols.getMain();
                }
            }
            text.setText( text.getText() + chr );
        }
    }

    @Override
    public void keyReleased( KeyEvent e ) {
        KeyNode node = keyboard.getKeyboardNode().getNodeFromKey( Key.fromEvent( e ) );
        if ( node == null ) {
            System.out.println( "Did not find key code: " + e.getKeyCode() );
        }
        else {
            node.setPressed( false );
        }

    }

}
