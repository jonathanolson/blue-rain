package com.jonathanolson.keyboard;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import edu.umd.cs.piccolo.PCanvas;

import com.jonathanolson.keyboard.model.Key;

public class KeyboardComponent extends PCanvas {

    public static final Dimension KEYBOARD_SIZE = new Dimension( 521, 171 );

    private KeyboardNode keyboardNode;

    public KeyboardComponent( KeyboardLayout layout ) {
        keyboardNode = new KeyboardNode( layout );

        getLayer().addChild( keyboardNode );


        setPreferredSize( KEYBOARD_SIZE );

        setOpaque( false );

        setBackground( new Color( 0, 0, 0, 0 ) );
    }

    public KeyboardNode getKeyboardNode() {
        return keyboardNode;
    }

    public static void main( String[] args ) {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JFrame frame = new JFrame( "KeyboardComponent Test" );
                frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

                JPanel panel = new JPanel( new FlowLayout() );
                frame.setContentPane( panel );

                KeyboardComponent standardKeyboard = new KeyboardComponent( new KeyboardLayout.Standard() );
                panel.add( standardKeyboard );

                KeyboardComponent colemakKeyboard = new KeyboardComponent( new KeyboardLayout.Colemak() );
                panel.add( colemakKeyboard );

                frame.setFocusTraversalKeysEnabled( false );
                frame.addKeyListener( new KeySelectionListener( standardKeyboard ) );
                frame.addKeyListener( new KeySelectionListener( colemakKeyboard ) );

                frame.pack();
                frame.setVisible( true );
            }
        } );
    }

    public static class KeySelectionListener implements KeyListener {
        private final KeyboardComponent keyboardComponent;

        public KeySelectionListener( KeyboardComponent keyboardComponent ) {
            this.keyboardComponent = keyboardComponent;
        }

        @Override
        public void keyTyped( KeyEvent e ) {

        }

        @Override
        public void keyPressed( KeyEvent e ) {
            KeyNode node = keyboardComponent.getKeyboardNode().getNodeFromKey( Key.fromEvent( e ) );
            if ( node == null ) {
                //System.out.println( "Did not find key code: " + e.getKeyCode() );
            }
            else {
                node.setPressed( true );
            }
        }

        @Override
        public void keyReleased( KeyEvent e ) {
            KeyNode node = keyboardComponent.getKeyboardNode().getNodeFromKey( Key.fromEvent( e ) );
            if ( node == null ) {
                //System.out.println( "Did not find key code: " + e.getKeyCode() );
            }
            else {
                node.setPressed( false );
            }
        }
    }
}
