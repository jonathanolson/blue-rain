package com.jonathanolson.keyboard;

public class KeyBehavior {
    private boolean isActionKey;

    private Character main;
    private Character cap;
    private Character agmain;
    private Character agcap;

    private KeyAction action;

    public KeyBehavior( Character main, Character cap ) {
        isActionKey = false;
        this.main = main;
        this.cap = cap;
    }

    public KeyBehavior( Character main, Character cap, Character agmain, Character agcap ) {
        isActionKey = false;
        this.main = main;
        this.cap = cap;
        this.agmain = agmain;
        this.agcap = agcap;
    }

    public KeyBehavior( KeyAction action ) {
        isActionKey = true;
        this.action = action;
    }

    @Override
    public int hashCode() {
        int x = 7;
        if ( isActionKey() ) {
            x += 17 * action.ordinal();
            x *= 524;
        }
        else {
            x *= 3;
            if ( main != null ) {
                x += main.hashCode();
            }
            x *= 17;
            if ( cap != null ) {
                x += cap.hashCode();
            }
            x *= 7;
            if ( agmain != null ) {
                x += agmain.hashCode();
            }
            x *= 57;
            if ( agcap != null ) {
                x += agcap.hashCode();
            }
        }
        return x;
    }

    @Override
    public boolean equals( Object o ) {
        if ( o instanceof KeyBehavior ) {
            KeyBehavior kb = (KeyBehavior) o;
            if ( kb.isActionKey() != isActionKey() ) {
                return false;
            }
            if ( isActionKey() ) {
                return kb.getAction() == getAction();
            }
            else {
                return kb.getMain() == getMain() && kb.getCap() == getCap() && kb.getAgmain() == getAgmain() && kb.getAgcap() == getAgcap();
            }
        }
        else {
            return false;
        }
    }

    public static KeyBehavior fromString( String str ) {
        if ( str.equals( "null" ) ) {
            return null;
        }
        int cidx = str.indexOf( ":" );
        String prefix = str.substring( 0, cidx );
        String postfix = str.substring( cidx + 1 );
        if ( prefix.equals( "A" ) ) {
            return new KeyBehavior( KeyAction.valueOf( postfix ) );
        }
        else {
            Character a = null;
            Character b = null;
            Character c = null;
            Character d = null;
            char[] chars = postfix.toCharArray();
            int mask = Integer.parseInt( prefix );
            int idx = 0;
            if ( mask % 2 > 0 ) {
                a = chars[idx++];
            }
            mask /= 2;
            if ( mask % 2 > 0 ) {
                b = chars[idx++];
            }
            mask /= 2;
            if ( mask % 2 > 0 ) {
                c = chars[idx++];
            }
            mask /= 2;
            if ( mask % 2 > 0 ) {
                d = chars[idx];
            }
            return new KeyBehavior( a, b, c, d );
        }
    }

    @Override
    public String toString() {
        String ret = "";
        if ( isActionKey ) {
            ret += "A:" + action.name();
        }
        else {
            String k = "";
            int q = 0;
            if ( main != null ) {
                k += main.toString();
                q += 1;
            }
            if ( cap != null ) {
                k += cap.toString();
                q += 2;
            }
            if ( agmain != null ) {
                k += agmain.toString();
                q += 4;
            }
            if ( agcap != null ) {
                k += agcap.toString();
                q += 8;
            }
            ret += q + ":" + k;
        }
        return ret;
    }

    public boolean isActionKey() {
        return isActionKey;
    }

    public void setActionKey( boolean actionKey ) {
        this.isActionKey = actionKey;
    }

    public Character getMain() {
        return main;
    }

    public void setMain( Character main ) {
        this.main = main;
    }

    public Character getCap() {
        return cap;
    }

    public void setCap( Character cap ) {
        this.cap = cap;
    }

    public Character getAgmain() {
        return agmain;
    }

    public void setAgmain( Character agmain ) {
        this.agmain = agmain;
    }

    public Character getAgcap() {
        return agcap;
    }

    public void setAgcap( Character agcap ) {
        this.agcap = agcap;
    }

    public KeyAction getAction() {
        return action;
    }

    public void setAction( KeyAction action ) {
        this.action = action;
    }

    public static KeyBehavior semiRandom() {
        if ( Math.random() > 0.8 ) {
            return null;
        }
        else if ( Math.random() > 0.5 ) {
            return new KeyBehavior( KeyAction.values()[(int) ( KeyAction.values().length * Math.random() )] );
        }
        else {
            char[] chars = new char[]{'a', 'A', 'r', 'H', ':', '!', '(', '~', ' ', '\u00F1', '\u00E1', '\u00C7'};
            Character a = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
            Character b = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
            Character c = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
            Character d = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
            return new KeyBehavior( a, b, c, d );
        }
    }

    private static class TestInputOutput {
        public static void main( String[] args ) {
            System.out.println( "Action Key tests:" );
            for ( KeyAction actionKey : KeyAction.values() ) {
                KeyBehavior start = new KeyBehavior( actionKey );
                String str = start.toString();
                KeyBehavior end = KeyBehavior.fromString( str );
                System.out.println( start + " => " + end );
                assert ( start == end );
            }
            System.out.println( "Character tests:" );
            char[] chars = new char[]{'a', 'A', 'r', 'H', ':', '!', '(', '~', ' ', '\u00F1', '\u00E1', '\u00C7'};
            for ( int count = 0; count < 100; count++ ) {
                Character a = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
                Character b = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
                Character c = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
                Character d = ( Math.random() > 0.5 ) ? null : chars[(int) ( Math.random() * chars.length )];
                KeyBehavior start = new KeyBehavior( a, b, c, d );
                System.out.println( start );
                String str = start.toString();
                KeyBehavior end = KeyBehavior.fromString( str );
                System.out.println( start + " => " + end );
                assert ( start == end );
            }
        }
    }
}
