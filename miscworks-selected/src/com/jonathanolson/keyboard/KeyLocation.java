package com.jonathanolson.keyboard;

import com.jonathanolson.keyboard.model.Key;

public class KeyLocation {
    private Key key;
    private boolean cap;
    private boolean ag;

    public KeyLocation( Key key, boolean cap, boolean ag ) {
        this.key = key;
        this.cap = cap;
        this.ag = ag;
    }

    public Key getKey() {
        return key;
    }

    public void setKey( Key key ) {
        this.key = key;
    }

    public boolean isCap() {
        return cap;
    }

    public void setCap( boolean cap ) {
        this.cap = cap;
    }

    public boolean isAg() {
        return ag;
    }

    public void setAg( boolean ag ) {
        this.ag = ag;
    }
}
