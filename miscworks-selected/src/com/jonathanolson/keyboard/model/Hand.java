package com.jonathanolson.keyboard.model;

import java.util.Arrays;
import java.util.List;

public enum Hand {
    LEFT( Arrays.asList( Finger.LEFT_INDEX, Finger.LEFT_MIDDLE, Finger.LEFT_RING, Finger.LEFT_LITTLE, Finger.LEFT_THUMB ) ),
    RIGHT( Arrays.asList( Finger.RIGHT_INDEX, Finger.RIGHT_MIDDLE, Finger.RIGHT_RING, Finger.RIGHT_LITTLE, Finger.RIGHT_THUMB ) );

    private final List<Finger> fingers;

    Hand( List<Finger> fingers ) {
        this.fingers = fingers;
    }

    public List<Finger> getFingers() {
        return fingers;
    }

    public Finger getRandomFinger() {
        return fingers.get( (int) ( Math.random() * fingers.size() ) );
    }
}
