package com.jonathanolson.keyboard.model;

public class KeyStrike {
    private Key key;
    private Finger finger;
    private long time;

    public KeyStrike( Key key, Finger finger, long time ) {
        this.key = key;
        this.finger = finger;
        this.time = time;
    }

    @Override
    public String toString() {
        return key.ordinal() + "," + finger.ordinal() + "," + time;
    }

    public Key getKey() {
        return key;
    }

    public void setKey( Key key ) {
        this.key = key;
    }

    public Finger getFinger() {
        return finger;
    }

    public void setFinger( Finger finger ) {
        this.finger = finger;
    }

    public long getTime() {
        return time;
    }

    public void setTime( long time ) {
        this.time = time;
    }
}
