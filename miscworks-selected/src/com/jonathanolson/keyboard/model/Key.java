package com.jonathanolson.keyboard.model;

import java.awt.event.KeyEvent;

public enum Key {
    TLDE( "~" ),
    AE01( "1" ),
    AE02( "2" ),
    AE03( "3" ),
    AE04( "4" ),
    AE05( "5" ),
    AE06( "6" ),
    AE07( "7" ),
    AE08( "8" ),
    AE09( "9" ),
    AE10( "0" ),
    AE11( "-" ),
    AE12( "=" ),
    BKSP( "Bksp" ),
    TAB( "Tab" ),
    AD01( "Q" ),
    AD02( "W" ),
    AD03( "E" ),
    AD04( "R" ),
    AD05( "T" ),
    AD06( "Y" ),
    AD07( "U" ),
    AD08( "I" ),
    AD09( "O" ),
    AD10( "P" ),
    AD11( "[" ),
    AD12( "]" ),
    BKSL( "\"" ),
    CAPS( "Caps" ),
    AC01( "A" ),
    AC02( "S" ),
    AC03( "D" ),
    AC04( "F" ),
    AC05( "G" ),
    AC06( "H" ),
    AC07( "J" ),
    AC08( "K" ),
    AC09( "L" ),
    AC10( ";" ),
    AC11( "'" ),
    RTRN( "Enter" ),
    LFSH( "LShift" ),
    AB01( "Z" ),
    AB02( "X" ),
    AB03( "C" ),
    AB04( "V" ),
    AB05( "B" ),
    AB06( "N" ),
    AB07( "M" ),
    AB08( "," ),
    AB09( "." ),
    AB10( "/" ),
    RTSH( "RShift" ),
    LCTL( "LCtl" ),
    LWIN( "LWin" ),
    LALT( "LAlt" ),
    SPCE( "Space" ),
    RALT( "RAlt" ),
    MENU( "Menu" );


    private String desc;

    Key( String desc ) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }

    public static int numKeys() {
        return Key.values().length;
    }

    public static Key fromEvent( KeyEvent event ) {
        return fromEventCodes( event.getKeyCode(), event.getKeyLocation() );
    }

    public static Key fromEventCodes( int keycode, int location ) {
        switch( keycode ) {
            // row E
            case KeyEvent.VK_BACK_QUOTE:
                return TLDE;
            case KeyEvent.VK_1:
                return AE01;
            case KeyEvent.VK_2:
                return AE02;
            case KeyEvent.VK_3:
                return AE03;
            case KeyEvent.VK_4:
                return AE04;
            case KeyEvent.VK_5:
                return AE05;
            case KeyEvent.VK_6:
                return AE06;
            case KeyEvent.VK_7:
                return AE07;
            case KeyEvent.VK_8:
                return AE08;
            case KeyEvent.VK_9:
                return AE09;
            case KeyEvent.VK_0:
                return AE10;
            case KeyEvent.VK_MINUS:
                return AE11;
            case KeyEvent.VK_EQUALS:
                return AE12;
            case KeyEvent.VK_BACK_SPACE:
                return BKSP;

            // row D
            case KeyEvent.VK_TAB:
                return TAB;
            case KeyEvent.VK_Q:
                return AD01;
            case KeyEvent.VK_W:
                return AD02;
            case KeyEvent.VK_E:
                return AD03;
            case KeyEvent.VK_R:
                return AD04;
            case KeyEvent.VK_T:
                return AD05;
            case KeyEvent.VK_Y:
                return AD06;
            case KeyEvent.VK_U:
                return AD07;
            case KeyEvent.VK_I:
                return AD08;
            case KeyEvent.VK_O:
                return AD09;
            case KeyEvent.VK_P:
                return AD10;
            case KeyEvent.VK_OPEN_BRACKET:
                return AD11;
            case KeyEvent.VK_CLOSE_BRACKET:
                return AD12;
            case KeyEvent.VK_BACK_SLASH:
                return BKSL;

            // row C
            case KeyEvent.VK_CAPS_LOCK:
                return CAPS;
            case KeyEvent.VK_A:
                return AC01;
            case KeyEvent.VK_S:
                return AC02;
            case KeyEvent.VK_D:
                return AC03;
            case KeyEvent.VK_F:
                return AC04;
            case KeyEvent.VK_G:
                return AC05;
            case KeyEvent.VK_H:
                return AC06;
            case KeyEvent.VK_J:
                return AC07;
            case KeyEvent.VK_K:
                return AC08;
            case KeyEvent.VK_L:
                return AC09;
            case KeyEvent.VK_SEMICOLON:
                return AC10;
            case KeyEvent.VK_QUOTE:
                return AC11;
            case KeyEvent.VK_ENTER:
                return RTRN;

            // row B
            case KeyEvent.VK_SHIFT:
                if ( location == KeyEvent.KEY_LOCATION_LEFT ) {
                    return LFSH;
                }
                else {
                    return RTSH;
                }
            case KeyEvent.VK_Z:
                return AB01;
            case KeyEvent.VK_X:
                return AB02;
            case KeyEvent.VK_C:
                return AB03;
            case KeyEvent.VK_V:
                return AB04;
            case KeyEvent.VK_B:
                return AB05;
            case KeyEvent.VK_N:
                return AB06;
            case KeyEvent.VK_M:
                return AB07;
            case KeyEvent.VK_COMMA:
                return AB08;
            case KeyEvent.VK_PERIOD:
                return AB09;
            case KeyEvent.VK_SLASH:
                return AB10;

            // row A
            case KeyEvent.VK_CONTROL:
                // for now, only represent L ctrl
                return LCTL;
            case KeyEvent.VK_WINDOWS:
                // for now, only represent L Win
                return LWIN;
            case KeyEvent.VK_ALT:
                return LALT;
            case KeyEvent.VK_SPACE:
                return SPCE;
            case KeyEvent.VK_ALT_GRAPH:
                return RALT;
            case KeyEvent.VK_CONTEXT_MENU:
                return MENU;
        }
        return null;
    }
}
