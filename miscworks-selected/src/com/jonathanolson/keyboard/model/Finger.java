package com.jonathanolson.keyboard.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.jonathanolson.keyboard.model.Key.*;

public enum Finger {
    LEFT_LITTLE(
            Hand.LEFT,
            new Key[]{
                    TAB, AD01, AD02, AD03,
                    CAPS, AC01, AC02, AC03,
                    LFSH, AB01, AB02,
                    LCTL, LWIN, LALT
            }
    ),
    LEFT_RING(
            Hand.LEFT,
            new Key[]{
                    TLDE, AE01, AE02, AE03,
                    TAB, AD01, AD02, AD03, AD04,
                    AC01, AC02, AC03, AC04,
                    LFSH, AB01, AB02, AB03,
                    LWIN, LALT
            }
    ),
    LEFT_MIDDLE(
            Hand.LEFT,
            new Key[]{
                    AE02, AE03, AE04, AE05, AE06,
                    AD01, AD02, AD03, AD04, AD05,
                    AC02, AC03, AC04, AC05,
                    AB02, AB03, AB04,
                    LWIN, LALT
            }
    ),
    LEFT_INDEX(
            Hand.LEFT,
            new Key[]{
                    AE02, AE03, AE04, AE05, AE06,
                    AD02, AD03, AD04, AD05, AD06,
                    AC02, AC03, AC04, AC05, AC06,
                    AB02, AB03, AB04, AB05,
                    LALT
            }
    ),
    LEFT_THUMB(
            Hand.LEFT,
            new Key[]{
                    LALT, SPCE
            }
    ),
    RIGHT_THUMB(
            Hand.RIGHT,
            new Key[]{
                    SPCE
            }
    ),
    RIGHT_INDEX(
            Hand.RIGHT,
            new Key[]{
                    AE07, AE08, AE09, AE10,
                    AD06, AD07, AD08, AD09, AD10, AD11,
                    AC06, AC07, AC08, AC09, AC10,
                    AB05, AB06, AB07, AB08, AB09,
                    RALT
            }
    ),
    RIGHT_MIDDLE(
            Hand.RIGHT,
            new Key[]{
                    AE08, AE09, AE10, AE11,
                    AD07, AD08, AD09, AD10, AD11, AD12,
                    AC07, AC08, AC09, AC10, AC11,
                    AB07, AB08, AB09, AB10,
                    RALT
            }
    ),
    RIGHT_RING(
            Hand.RIGHT,
            new Key[]{
                    AE09, AE10, AE11, AE12, BKSP,
                    AD08, AD09, AD10, AD11, AD12, BKSL,
                    AC08, AC09, AC10, AC11, RTRN,
                    AB08, AB09, AB10, RTSH,
                    RALT
            }
    ),
    RIGHT_LITTLE(
            Hand.RIGHT,
            new Key[]{
                    AD10, AD11, AD12, BKSL,
                    AC09, AC10, AC11, RTRN,
                    AB09, AB10, RTSH
            }
    );

    private Hand hand;
    private List<Key> domain = new ArrayList<Key>();

    Finger( Hand hand, Key[] domain ) {
        this.hand = hand;
        this.domain.addAll( Arrays.asList( domain ) );
    }

    public Hand getHand() {
        return hand;
    }

    public List<Key> getDomain() {
        return domain;
    }

    public static Finger getRandomFinger() {
        return Finger.values()[(int) ( Math.random() * 10 )];
    }

    public Key getRandomKey() {
        return domain.get( (int) ( Math.random() * domain.size() ) );
    }
}
