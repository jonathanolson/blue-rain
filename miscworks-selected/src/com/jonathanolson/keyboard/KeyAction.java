package com.jonathanolson.keyboard;

public enum KeyAction {
    BACKSPACE( "Bksp" ),
    SPACE( "Space" ),
    SHIFT( "Shift" ),
    CONTROL( "Ctrl" ),
    ALT( "Alt" ),
    ALT_GRAPH( "AltG" ),
    SUPER( "Win" ),
    CAPS_LOCK( "Caps" ),
    TAB( "Tab" ),
    ENTER( "Enter" ),
    MENU( "Menu" );

    private String desc;

    KeyAction( String desc ) {
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}
