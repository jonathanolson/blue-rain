package com.jonathanolson.keyboard.test;

import com.jonathanolson.keyboard.model.Finger;
import com.jonathanolson.keyboard.model.Hand;
import com.jonathanolson.keyboard.model.Key;

public class RandomDomainTest {
    public static void main( String[] args ) {
        for ( int i = 2; i < 5; i++ ) {
            for ( int j = 0; j < 10; j++ ) {
                Hand hand = Math.random() > 0.5 ? Hand.LEFT : Hand.RIGHT;
                for ( int k = 0; k < i; k++ ) {
                    // k is index into string
                    Finger finger = hand.getRandomFinger();
                    Key key = finger.getRandomKey();
                    System.out.print( finger + " " + key.getDesc() + " " );
                }
                System.out.println( "" );
            }
        }
    }
}
