package com.jonathanolson.keyboard.test;

import java.awt.*;
import java.awt.event.InputEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.*;

import com.jonathanolson.keyboard.KeyBehavior;
import com.jonathanolson.keyboard.KeyNode;
import com.jonathanolson.keyboard.KeyboardComponent;
import com.jonathanolson.keyboard.KeyboardLayout;
import com.jonathanolson.keyboard.model.Key;

public class TextTimingTest extends JFrame implements KeyListener {
    private JTextArea text;
    private KeyboardLayout layout = new KeyboardLayout.Standard();
    private KeyboardComponent keyboardComponent = new KeyboardComponent( layout );

    private String stringToType;

    public TextTimingTest( String stringToType ) {
        super( "Text Timing Test" );

        this.stringToType = stringToType;

        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        JPanel panel = new JPanel( new GridBagLayout() );
        setContentPane( panel );

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.insets = new Insets( 5, 5, 5, 5 );

        panel.add( keyboardComponent, c );

        c.gridy = 2;
        c.fill = GridBagConstraints.BOTH;

        text = new JTextArea();
        JScrollPane pane = new JScrollPane( text );
        pane.setPreferredSize( KeyboardComponent.KEYBOARD_SIZE );
        add( pane, c );

        c.gridy = 1;
        JTextField field = new JTextField( stringToType );
        field.setEditable( false );
        field.setFocusable( false );
        add( field, c );

        text.addKeyListener( this );
        text.setEditable( false );

        setFocusTraversalKeysEnabled( false );
        addKeyListener( new KeyboardComponent.KeySelectionListener( keyboardComponent ) );
        addKeyListener( new KeyAdapter() {
            @Override
            public void keyPressed( KeyEvent keyEvent ) {
                super.keyPressed( keyEvent );
                Key key = Key.fromEvent( keyEvent );
                System.out.println( key );
                System.out.println( System.currentTimeMillis() );
            }
        } );

        pack();
        setVisible( true );
    }

    @Override
    public void keyTyped( KeyEvent e ) {
    }

    @Override
    public void keyPressed( KeyEvent e ) {
        Key key = Key.fromEvent( e );
        long time = System.currentTimeMillis();

        System.out.println( "P " + time + " " + key );

        KeyNode node = keyboardComponent.getKeyboardNode().getNodeFromKey( key );
        if ( node == null ) {
            System.out.println( "Did not find key code: " + e.getKeyCode() );
        }
        else {
            node.setPressed( true );
        }

        KeyBehavior symbols = layout.getKeyBehavior( key );
        if ( symbols.isActionKey() ) {
            switch( symbols.getAction() ) {
                case ENTER:
                    text.setText( text.getText() + "\n" );
                    break;
                case SPACE:
                    text.setText( text.getText() + " " );
                    break;
                case BACKSPACE:
                    if ( text.getText().length() > 0 ) {
                        text.setText( text.getText().substring( 0, text.getText().length() - 1 ) );
                    }
                    break;
            }
        }
        else {
            Character chr;
            if ( ( e.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK ) == InputEvent.SHIFT_DOWN_MASK ) {
                if ( ( e.getModifiersEx() & InputEvent.ALT_GRAPH_DOWN_MASK ) == InputEvent.ALT_GRAPH_DOWN_MASK ) {
                    chr = symbols.getAgcap();
                }
                else {
                    chr = symbols.getCap();
                }
            }
            else {
                if ( ( e.getModifiersEx() & InputEvent.ALT_GRAPH_DOWN_MASK ) == InputEvent.ALT_GRAPH_DOWN_MASK ) {
                    chr = symbols.getAgmain();
                }
                else {
                    chr = symbols.getMain();
                }
            }
            text.setText( text.getText() + chr );
        }
        if ( stringToType.equals( text.getText() ) ) {
            System.out.println( "DONE" );
        }
    }

    @Override
    public void keyReleased( KeyEvent e ) {
        Key key = Key.fromEvent( e );
        long time = System.currentTimeMillis();

        System.out.println( "  " + time + " " + key );

        KeyNode node = keyboardComponent.getKeyboardNode().getNodeFromKey( key );
        if ( node == null ) {
            System.out.println( "Did not find key code: " + e.getKeyCode() );
        }
        else {
            node.setPressed( false );
        }

    }

    public static void main( String[] args ) {
        SwingUtilities.invokeLater( new Runnable() {
            @Override
            public void run() {
                new TextTimingTest( "This is a test of the emergency text typing system.\n" );
            }
        } );
    }
}
