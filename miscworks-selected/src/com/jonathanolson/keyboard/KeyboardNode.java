package com.jonathanolson.keyboard;

import java.util.HashMap;
import java.util.Map;

import edu.umd.cs.piccolo.PNode;

import com.jonathanolson.keyboard.model.Key;

public class KeyboardNode extends PNode {

    private Map<Key, KeyNode> nodeMap = new HashMap<Key, KeyNode>();

    private static final double KD = 30.0;
    private static final double SPACER = 5.0;

    private double x, y;

    public KeyboardNode( KeyboardLayout layout ) {
        double maxX;
        x = 0;
        y = 0;
        addKey( layout, Key.TLDE, x, y, KD, KD );
        addKey( layout, Key.AE01, x, y, KD, KD );
        addKey( layout, Key.AE02, x, y, KD, KD );
        addKey( layout, Key.AE03, x, y, KD, KD );
        addKey( layout, Key.AE04, x, y, KD, KD );
        addKey( layout, Key.AE05, x, y, KD, KD );
        addKey( layout, Key.AE06, x, y, KD, KD );
        addKey( layout, Key.AE07, x, y, KD, KD );
        addKey( layout, Key.AE08, x, y, KD, KD );
        addKey( layout, Key.AE09, x, y, KD, KD );
        addKey( layout, Key.AE10, x, y, KD, KD );
        addKey( layout, Key.AE11, x, y, KD, KD );
        addKey( layout, Key.AE12, x, y, KD, KD );
        addKey( layout, Key.BKSP, x, y, 65, KD );
        maxX = x - SPACER;

        x = 0;
        nextRow();
        addKey( layout, Key.TAB, x, y, 47, KD );
        addKey( layout, Key.AD01, x, y, KD, KD );
        addKey( layout, Key.AD02, x, y, KD, KD );
        addKey( layout, Key.AD03, x, y, KD, KD );
        addKey( layout, Key.AD04, x, y, KD, KD );
        addKey( layout, Key.AD05, x, y, KD, KD );
        addKey( layout, Key.AD06, x, y, KD, KD );
        addKey( layout, Key.AD07, x, y, KD, KD );
        addKey( layout, Key.AD08, x, y, KD, KD );
        addKey( layout, Key.AD09, x, y, KD, KD );
        addKey( layout, Key.AD10, x, y, KD, KD );
        addKey( layout, Key.AD11, x, y, KD, KD );
        addKey( layout, Key.AD12, x, y, KD, KD );
        addKey( layout, Key.BKSL, x, y, maxX - x, KD );

        x = 0;
        nextRow();
        addKey( layout, Key.CAPS, x, y, 57, KD );
        addKey( layout, Key.AC01, x, y, KD, KD );
        addKey( layout, Key.AC02, x, y, KD, KD );
        addKey( layout, Key.AC03, x, y, KD, KD );
        addKey( layout, Key.AC04, x, y, KD, KD );
        addKey( layout, Key.AC05, x, y, KD, KD );
        addKey( layout, Key.AC06, x, y, KD, KD );
        addKey( layout, Key.AC07, x, y, KD, KD );
        addKey( layout, Key.AC08, x, y, KD, KD );
        addKey( layout, Key.AC09, x, y, KD, KD );
        addKey( layout, Key.AC10, x, y, KD, KD );
        addKey( layout, Key.AC11, x, y, KD, KD );
        addKey( layout, Key.RTRN, x, y, maxX - x, KD );

        x = 0;
        nextRow();
        addKey( layout, Key.LFSH, x, y, 74, KD );
        addKey( layout, Key.AB01, x, y, KD, KD );
        addKey( layout, Key.AB02, x, y, KD, KD );
        addKey( layout, Key.AB03, x, y, KD, KD );
        addKey( layout, Key.AB04, x, y, KD, KD );
        addKey( layout, Key.AB05, x, y, KD, KD );
        addKey( layout, Key.AB06, x, y, KD, KD );
        addKey( layout, Key.AB07, x, y, KD, KD );
        addKey( layout, Key.AB08, x, y, KD, KD );
        addKey( layout, Key.AB09, x, y, KD, KD );
        addKey( layout, Key.AB10, x, y, KD, KD );
        addKey( layout, Key.RTSH, x, y, maxX - x, KD );

        x = 0;
        nextRow();
        addKey( layout, Key.LCTL, x, y, 45, KD );
        x += KD;
        //addKey( new Key( KeyEvent.VK_DEAD_CEDILLA, KeyEvent.KEY_LOCATION_UNKNOWN ), x, y, KD, KD, "Fn", null, null, null );
        addKey( layout, Key.LWIN, x, y, KD, KD );
        addKey( layout, Key.LALT, x, y, 40, KD );
        addKey( layout, Key.SPCE, x, y, KD * 6, KD );
        addKey( layout, Key.RALT, x, y, KD, KD );
    }


    public KeyNode getNodeFromKey( Key key ) {
        return nodeMap.get( key );
    }

    private void nextRow() {
        y += KD + SPACER;
    }

    private String charString( Character chr ) {
        if ( chr == null ) {
            return null;
        }
        else {
            return chr.toString();
        }
    }

    private void addKey( KeyboardLayout layout, Key key, double x, double y, double width, double height ) {
        KeyBehavior behavior = layout.getKeyBehavior( key );
        if ( behavior == null ) {
            addKey( key, x, y, width, height, "<" + key.name() + ">", null, null, null );
        }
        else if ( behavior.isActionKey() ) {
            addKey( key, x, y, width, height, behavior.getAction().getDesc(), null, null, null );
        }
        else {
            addKey( key, x, y, width, height,
                    charString( behavior.getMain() ),
                    charString( behavior.getCap() ),
                    charString( behavior.getAgmain() ),
                    charString( behavior.getAgcap() ) );
        }
    }

    private void addKey( Key key, double x, double y, double width, double height, String main, String cap, String agmain, String agcap ) {
        KeyNode node = new KeyNode( width, height, main, cap, agmain, agcap );
        node.setOffset( x, y );
        nodeMap.put( key, node );
        addChild( node );
        this.x += width + SPACER;
    }

}
