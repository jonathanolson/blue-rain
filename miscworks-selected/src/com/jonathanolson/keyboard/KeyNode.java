package com.jonathanolson.keyboard;

import java.awt.*;

import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.nodes.PPath;
import edu.umd.cs.piccolo.nodes.PText;
import edu.umd.cs.piccolo.util.PBounds;

public class KeyNode extends PNode {

    private double width;
    private double height;

    private static double padding = 1.0;

    private PPath border;

    private String mainString;
    private String capString;
    private String agmainString;
    private String agcapString;
    private PText mainText;
    private PText capText;
    private PText agmainText;
    private PText agcapText;
    private PNode borderHolder;

    public KeyNode( double width, double height, String main, String cap, String agmain, String agcap ) {
        this.width = width;
        this.height = height;

        mainString = main;
        capString = cap;
        agmainString = agmain;
        agcapString = agcap;

        createBorder();
        borderHolder = new PNode();
        addChild( borderHolder );
        borderHolder.addChild( border );

        mainText = new PText( non( mainString ) );
        capText = new PText( non( capString ) );
        agmainText = new PText( non( agmainString ) );
        agcapText = new PText( non( agcapString ) );

        addChild( mainText );
        addChild( capText );
        addChild( agmainText );
        addChild( agcapText );

        layout();
    }

    public void setMain( String value ) {
        mainString = value;
        mainText.setText( value );
        layout();
    }

    public void setCap( String value ) {
        capString = value;
        capText.setText( value );
        layout();
    }

    public void setAgMain( String value ) {
        agmainString = value;
        agmainText.setText( value );
        layout();
    }

    public void setAgCap( String value ) {
        agcapString = value;
        agcapText.setText( value );
        layout();
    }

    public void setKeyWidth( double value ) {
        width = value;
        borderHolder.removeChild( border );
        createBorder();
        borderHolder.addChild( border );
        layout();
    }

    public void setKeyHeight( double value ) {
        height = value;
        borderHolder.removeChild( border );
        createBorder();
        borderHolder.addChild( border );
        layout();
    }

    private void createBorder() {
        border = PPath.createRectangle( 0f, 0f, (float) width, (float) height );
        border.setStrokePaint( Color.GRAY );
        border.setPaint( Color.WHITE );
    }

    public void setPressed( boolean pressed ) {
        if ( pressed ) {
            border.setStrokePaint( Color.RED );
            border.setPaint( Color.LIGHT_GRAY );
        }
        else {
            border.setStrokePaint( Color.GRAY );
            border.setPaint( Color.WHITE );
        }
    }

    private void layout() {
        PBounds mainBounds = mainText.getBounds();
        if ( capString == null ) {
            mainText.setOffset( middleWidth( mainBounds ), middleHeight( mainBounds ) );
        }
        else if ( agmainString == null && agcapString == null ) {
            mainText.setOffset( middleWidth( mainBounds ), bottomHeight( mainBounds ) );
        }
        else {
            mainText.setOffset( leftWidth( mainBounds ), bottomHeight( mainBounds ) );
        }

        PBounds capBounds = capText.getBounds();
        if ( agmainString == null && agcapString == null ) {
            capText.setOffset( middleWidth( capBounds ), topHeight( capBounds ) );
        }
        else {
            capText.setOffset( leftWidth( capBounds ), topHeight( capBounds ) );
        }

        PBounds agmainBounds = agmainText.getBounds();
        agmainText.setOffset( rightWidth( agmainBounds ), bottomHeight( agmainBounds ) );

        PBounds agcapBounds = agcapText.getBounds();
        agcapText.setOffset( rightWidth( agcapBounds ), topHeight( agcapBounds ) );

    }

    private String non( String a ) {
        if ( a == null ) {
            return "";
        }
        else {
            return a;
        }
    }

    private double leftWidth( PBounds bounds ) {
        return ( width - bounds.getWidth() ) / 2 - width / 4 + padding;
    }

    private double middleWidth( PBounds bounds ) {
        return ( width - bounds.getWidth() ) / 2;
    }

    private double rightWidth( PBounds bounds ) {
        return ( width - bounds.getWidth() ) / 2 + width / 4 - padding;
    }

    private double topHeight( PBounds bounds ) {
        return ( height - bounds.getHeight() ) / 2 - height / 4 + padding;
    }

    private double middleHeight( PBounds bounds ) {
        return ( height - bounds.getHeight() ) / 2;
    }

    private double bottomHeight( PBounds bounds ) {
        return ( height - bounds.getHeight() ) / 2 + height / 4 - padding;
    }

}
