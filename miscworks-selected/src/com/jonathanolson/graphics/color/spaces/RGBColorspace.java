package com.jonathanolson.graphics.color.spaces;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.util.math.vec.Matrix3d;
import com.jonathanolson.util.math.vec.Point3d;

public class RGBColorspace extends Colorspace {

    private double gamma;

    /**
     * Reference white in XYZ
     */
    private Point3d referenceWhiteXYZ;

    private Point3d redxyY;
    private Point3d greenxyY;
    private Point3d bluexyY;

    private Matrix3d RGBToXYZ = null;
    private Matrix3d XYZToRGB = null;

    public RGBColorspace( double gamma, Point3d referenceWhiteXYZ, Point3d redxyY, Point3d greenxyY, Point3d bluexyY ) {
        this.gamma = gamma;
        this.referenceWhiteXYZ = referenceWhiteXYZ;
        this.redxyY = redxyY;
        this.greenxyY = greenxyY;
        this.bluexyY = bluexyY;
    }

    @Override
    public void toXYZ( Point3d color ) {
        if ( gamma != 1 ) {
            color.set( Math.pow( color.getX(), gamma ), Math.pow( color.getY(), gamma ), Math.pow( color.getZ(), gamma ) );
        }
        if ( RGBToXYZ == null ) {
            calculateMatrices();
        }
        RGBToXYZ.transform( color );
    }

    @Override
    public void fromXYZ( Point3d color ) {
        if ( XYZToRGB == null ) {
            calculateMatrices();
        }
        XYZToRGB.transform( color );
        if ( gamma != 1 ) {
            color.set( Math.pow( color.getX(), 1 / gamma ), Math.pow( color.getY(), 1 / gamma ), Math.pow( color.getZ(), 1 / gamma ) );
        }
    }

    public static final RGBColorspace Adobe = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.297361 ), new Point3d( 0.2100, 0.7100, 0.627355 ), new Point3d( 0.1500, 0.0600, 0.075285 ) );
    public static final RGBColorspace Apple = new RGBColorspace( 1.8, StandardIlluminant.D65_XYZ, new Point3d( 0.6250, 0.3400, 0.244634 ), new Point3d( 0.2800, 0.5950, 0.672034 ), new Point3d( 0.1550, 0.0700, 0.083332 ) );
    public static final RGBColorspace Best = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.7347, 0.2653, 0.228457 ), new Point3d( 0.2150, 0.7750, 0.737352 ), new Point3d( 0.1300, 0.0350, 0.034191 ) );
    public static final RGBColorspace Beta = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.6888, 0.3112, 0.303273 ), new Point3d( 0.1986, 0.7551, 0.663786 ), new Point3d( 0.1265, 0.0352, 0.032941 ) );
    public static final RGBColorspace Bruce = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.240995 ), new Point3d( 0.2800, 0.6500, 0.683554 ), new Point3d( 0.1500, 0.0600, 0.075452 ) );
    public static final RGBColorspace CIE = new RGBColorspace( 2.2, StandardIlluminant.E_XYZ, new Point3d( 0.7350, 0.2650, 0.176204 ), new Point3d( 0.2740, 0.7170, 0.812985 ), new Point3d( 0.1670, 0.0090, 0.010811 ) );
    public static final RGBColorspace ColorMatch = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.6300, 0.3400, 0.274884 ), new Point3d( 0.2950, 0.6050, 0.658132 ), new Point3d( 0.1500, 0.0750, 0.066985 ) );
    public static final RGBColorspace ECI = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.6700, 0.3300, 0.320250 ), new Point3d( 0.2100, 0.7100, 0.602071 ), new Point3d( 0.1400, 0.0800, 0.077679 ) );
    public static final RGBColorspace Ekta_Space_PS5 = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.6950, 0.3050, 0.260629 ), new Point3d( 0.2600, 0.7000, 0.734946 ), new Point3d( 0.1100, 0.0050, 0.004425 ) );
    public static final RGBColorspace NTSC = new RGBColorspace( 2.2, StandardIlluminant.C_XYZ, new Point3d( 0.6700, 0.3300, 0.298839 ), new Point3d( 0.2100, 0.7100, 0.586811 ), new Point3d( 0.1400, 0.0800, 0.114350 ) );
    public static final RGBColorspace PAL_SECAM = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.222021 ), new Point3d( 0.2900, 0.6000, 0.706645 ), new Point3d( 0.1500, 0.0600, 0.071334 ) );
    public static final RGBColorspace ProPhoto = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.7347, 0.2653, 0.288040 ), new Point3d( 0.1596, 0.8404, 0.711874 ), new Point3d( 0.0366, 0.0001, 0.000086 ) );
    public static final RGBColorspace SMPTE_C = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6300, 0.3400, 0.212395 ), new Point3d( 0.3100, 0.5950, 0.701049 ), new Point3d( 0.1550, 0.0700, 0.086556 ) );
    public static final RGBColorspace Wide_Gamut = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.7350, 0.2650, 0.258187 ), new Point3d( 0.1150, 0.8260, 0.724938 ), new Point3d( 0.1570, 0.0180, 0.016875 ) );

    public static final RGBColorspace sRGB = new RGBColorspace( 1, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.212656 ), new Point3d( 0.3000, 0.6000, 0.715158 ), new Point3d( 0.1500, 0.0600, 0.072186 ) ) {
        @Override
        public void toXYZ( Point3d color ) {
            color.set(
                    color.getX() <= 0.04045 ? color.getX() / 12.92 : Math.pow( ( color.getX() + 0.055 ) / 1.055, 2.4 ),
                    color.getY() <= 0.04045 ? color.getY() / 12.92 : Math.pow( ( color.getY() + 0.055 ) / 1.055, 2.4 ),
                    color.getZ() <= 0.04045 ? color.getZ() / 12.92 : Math.pow( ( color.getZ() + 0.055 ) / 1.055, 2.4 )
            );
            super.toXYZ( color );
        }

        @Override
        public void fromXYZ( Point3d color ) {
            super.fromXYZ( color );
            color.set(
                    color.getX() <= 0.0031308 ? color.getX() * 12.92 : 1.055 * Math.pow( color.getX(), 1.0 / 2.4 ) - 0.055,
                    color.getY() <= 0.0031308 ? color.getY() * 12.92 : 1.055 * Math.pow( color.getY(), 1.0 / 2.4 ) - 0.055,
                    color.getZ() <= 0.0031308 ? color.getZ() * 12.92 : 1.055 * Math.pow( color.getZ(), 1.0 / 2.4 ) - 0.055
            );
        }

        @Override
        public double getGamma() {
            return 2.2;
        }
    };

    public double getGamma() {
        return gamma;
    }

    public Point3d getReferenceWhiteXYZ() {
        return referenceWhiteXYZ;
    }

    public Point3d getRedxyY() {
        return redxyY;
    }

    public Point3d getGreenxyY() {
        return greenxyY;
    }

    public Point3d getBluexyY() {
        return bluexyY;
    }

    public Matrix3d getRGBToXYZ() {
        return RGBToXYZ;
    }

    public Matrix3d getXYZToRGB() {
        return XYZToRGB;
    }

    private synchronized void calculateMatrices() {
        // reference: http://www.brucelindbloom.com/index.html?Eqn_RGB_XYZ_Matrix.html
        double Xr = redxyY.getX() / redxyY.getY();
        double Yr = 1;
        double Zr = ( 1 - redxyY.getX() - redxyY.getY() ) / redxyY.getY();

        double Xg = greenxyY.getX() / greenxyY.getY();
        double Yg = 1;
        double Zg = ( 1 - greenxyY.getX() - greenxyY.getY() ) / greenxyY.getY();

        double Xb = bluexyY.getX() / bluexyY.getY();
        double Yb = 1;
        double Zb = ( 1 - bluexyY.getX() - bluexyY.getY() ) / bluexyY.getY();

        Matrix3d tmp = new Matrix3d(
                Xr, Xg, Xb,
                Yr, Yg, Yb,
                Zr, Zg, Zb
        );
        tmp.invert();

        Point3d s = new Point3d( referenceWhiteXYZ );
        tmp.transform( s );

        RGBToXYZ = new Matrix3d(
                s.getX() * Xr, s.getY() * Xg, s.getZ() * Xb,
                s.getX() * Yr, s.getY() * Yg, s.getZ() * Yb,
                s.getX() * Zr, s.getY() * Zg, s.getZ() * Zb
        );

        XYZToRGB = new Matrix3d( RGBToXYZ );
        XYZToRGB.invert();

    }
}
