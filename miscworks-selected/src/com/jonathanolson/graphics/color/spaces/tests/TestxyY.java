package com.jonathanolson.graphics.color.spaces.tests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.jonathanolson.graphics.color.spaces.xyYColorspace;
import com.jonathanolson.util.math.vec.Point3d;

public class TestxyY {
    @Test
    public void checkInverse() {
        System.out.println( "Checking XYZ <==> xyY invertability" );
        xyYColorspace space = new xyYColorspace();
        Point3d a = new Point3d( 0.1657, 0.32546, 0.25567 );
        Point3d b = new Point3d( a );
        space.fromXYZ( b );
        Point3d c = new Point3d( b );
        space.toXYZ( c );
        assertTrue( a.epsilonEquals( c, 0.0001 ) );
    }
}
