package com.jonathanolson.graphics.color.spaces.tests;

import com.jonathanolson.util.math.vec.Matrix3d;

public class MIPrecision {
    public static void main( String[] args ) {
        Matrix3d a = new Matrix3d(
                0.8951000, 0.2664000, -0.1614000,
                -0.7502000, 1.7135000, 0.0367000,
                0.0389000, -0.0685000, 1.0296000
        );
        Matrix3d b = new Matrix3d(
                0.9869929, -0.1470543, 0.1599627,
                0.4323053, 0.5183603, 0.0492912,
                -0.0085287, 0.0400428, 0.9684867
        );
        Matrix3d c = new Matrix3d( a );
        Matrix3d d = new Matrix3d( c );
        d.invert();

        System.out.println( "a:\n" + a );
        System.out.println( "b:\n" + b );
        System.out.println( "c:\n" + c );
        System.out.println( "d:\n" + d );

        a.mul( b );
        c.mul( d );

        System.out.println( "a*b:\n" + a );
        System.out.println( "c*d:\n" + c );
    }
}
