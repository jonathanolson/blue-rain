package com.jonathanolson.graphics.color.spaces.tests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.jonathanolson.graphics.color.ChromaticAdaptation;
import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.util.math.vec.Matrix3d;

public class TestChromaticAdaptation {


    @Test
    public void AtoD65XYZScalingTest() {
        System.out.println( "A to D65 XYZ scaling chromatic adaptation test" );
        Matrix3d expected = new Matrix3d(
                0.8652435, 0.0000000, 0.0000000,
                0.0000000, 1.0000000, 0.0000000,
                0.0000000, 0.0000000, 3.0598005
        );
        Matrix3d result = ChromaticAdaptation.construct( ChromaticAdaptation.XYZ_SCALING, StandardIlluminant.A_XYZ, StandardIlluminant.D65_XYZ );

        System.out.println( "expected:\n" + expected );
        System.out.println( "result:\n" + result );

        System.out.println( "using A:" + StandardIlluminant.A_XYZ );
        System.out.println( "using D65: " + StandardIlluminant.D65_XYZ );
        System.out.println( "using XYZ-scaling:\n" + ChromaticAdaptation.XYZ_SCALING );

        assertTrue( expected.epsilonEquals( result, 0.0001 ) );
    }

    @Test
    public void AtoD65BradfordTest() {
        System.out.println( "A to D65 Bradford chromatic adaptation test" );
        Matrix3d expected = new Matrix3d(
                0.8446965, -0.1179225, 0.3948108,
                -0.1366303, 1.1041226, 0.1291718,
                0.0798489, -0.1348999, 3.1924009
        );
        Matrix3d result = ChromaticAdaptation.construct( ChromaticAdaptation.BRADFORD, StandardIlluminant.A_XYZ, StandardIlluminant.D65_XYZ );

        System.out.println( "expected:\n" + expected );
        System.out.println( "result:\n" + result );

        System.out.println( "using A:" + StandardIlluminant.A_XYZ );
        System.out.println( "using D65: " + StandardIlluminant.D65_XYZ );
        System.out.println( "using bradford:\n" + ChromaticAdaptation.BRADFORD );

        assertTrue( expected.epsilonEquals( result, 0.0001 ) );
    }
}
