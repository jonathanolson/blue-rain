package com.jonathanolson.graphics.color.spaces.tests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.util.math.vec.Matrix3d;
import com.jonathanolson.util.math.vec.Point3d;

public class TestRGB {
    @Test
    public void AdobeInvert() {
        System.out.println( "Testing adobe RGB color inversion" );
        Colorspace space = RGBColorspace.Adobe;
        Point3d a = new Point3d( 0.1657, 0.32546, 0.25567 );
        Point3d b = new Point3d( a );
        space.toXYZ( b );
        Point3d c = new Point3d( b );
        space.fromXYZ( c );
        assertTrue( a.epsilonEquals( c, 0.0001 ) );
    }

    @Test
    public void CheckAdobeMatrices() {
        Matrix3d rgbtoxyz = new Matrix3d(
                0.5767309, 0.1855540, 0.1881852,
                0.2973769, 0.6273491, 0.0752741,
                0.0270343, 0.0706872, 0.9911085
        );
        Matrix3d xyztorgb = new Matrix3d(
                2.0413690, -0.5649464, -0.3446944,
                -0.9692660, 1.8760108, 0.0415560,
                0.0134474, -0.1183897, 1.0154096
        );
        System.out.println( "Reference Adobe RGB=>XYZ:\n" + rgbtoxyz );
        System.out.println( "Computed Adobe RGB=>XYZ:\n" + RGBColorspace.Adobe.getRGBToXYZ() );
        assertTrue( rgbtoxyz.epsilonEquals( RGBColorspace.Adobe.getRGBToXYZ(), 0.0001 ) );
        assertTrue( xyztorgb.epsilonEquals( RGBColorspace.Adobe.getXYZToRGB(), 0.0001 ) );
    }

    @Test
    public void CheckAdobeConversion() {
        System.out.println( "Checking adobe conversion:" );
        Point3d referenceXYZ = new Point3d( 0.4, 0.45, 0.5 );
        Point3d referenceRGB = new Point3d( 0.651790, 0.714474, 0.702467 );
        Point3d rgb = RGBColorspace.Adobe.getFromXYZ( referenceXYZ );
        Point3d xyz = RGBColorspace.Adobe.getToXYZ( referenceRGB );

        System.out.println( "reference XYZ: " + referenceXYZ );
        System.out.println( "reference RGB: " + referenceRGB );
        System.out.println( "calculated XYZ: " + xyz );
        System.out.println( "calculated RGB: " + rgb );
        assertTrue( rgb.epsilonEquals( referenceRGB, 0.0001 ) );
        assertTrue( xyz.epsilonEquals( referenceXYZ, 0.0001 ) );
    }

    @Test
    public void sRGBInvert() {
        System.out.println( "Testing sRGB color inversion" );
        Colorspace space = RGBColorspace.sRGB;
        Point3d a = new Point3d( 0.1657, 0.32546, 0.25567 );
        Point3d b = new Point3d( a );
        space.toXYZ( b );
        Point3d c = new Point3d( b );
        space.fromXYZ( c );
        assertTrue( a.epsilonEquals( c, 0.0001 ) );
    }

    @Test
    public void ChecksRGBMatrices() {
        Matrix3d rgbtoxyz = new Matrix3d(
                0.4124564, 0.3575761, 0.1804375,
                0.2126729, 0.7151522, 0.0721750,
                0.0193339, 0.1191920, 0.9503041
        );
        Matrix3d xyztorgb = new Matrix3d(
                3.2404542, -1.5371385, -0.4985314,
                -0.9692660, 1.8760108, 0.0415560,
                0.0556434, -0.2040259, 1.0572252
        );
        System.out.println( "Reference sRGB=>XYZ:\n" + rgbtoxyz );
        System.out.println( "Computed sRGB=>XYZ:\n" + RGBColorspace.sRGB.getRGBToXYZ() );
        assertTrue( rgbtoxyz.epsilonEquals( RGBColorspace.sRGB.getRGBToXYZ(), 0.0001 ) );
        assertTrue( xyztorgb.epsilonEquals( RGBColorspace.sRGB.getXYZToRGB(), 0.0001 ) );
    }

    @Test
    public void ChecksRGBConversionMainRange() {
        System.out.println( "Checking sRGB (MAIN) conversion:" );
        Point3d referenceXYZ = new Point3d( 0.4, 0.45, 0.5 );
        Point3d referenceRGB = new Point3d( 0.630411, 0.720187, 0.707718 );
        Point3d rgb = RGBColorspace.sRGB.getFromXYZ( referenceXYZ );
        Point3d xyz = RGBColorspace.sRGB.getToXYZ( referenceRGB );

        System.out.println( "reference XYZ: " + referenceXYZ );
        System.out.println( "reference RGB: " + referenceRGB );
        System.out.println( "calculated XYZ: " + xyz );
        System.out.println( "calculated RGB: " + rgb );
        assertTrue( rgb.epsilonEquals( referenceRGB, 0.0001 ) );
        assertTrue( xyz.epsilonEquals( referenceXYZ, 0.0001 ) );
    }

    @Test
    public void ChecksRGBConversionLowRange() {
        System.out.println( "Checking sRGB (LOW) conversion:" );
        Point3d referenceXYZ = new Point3d( 0.004000, 0.004500, 0.005000 );
        Point3d referenceRGB = new Point3d( 0.045605, 0.058782, 0.056952 );
        Point3d rgb = RGBColorspace.sRGB.getFromXYZ( referenceXYZ );
        Point3d xyz = RGBColorspace.sRGB.getToXYZ( referenceRGB );

        System.out.println( "reference XYZ: " + referenceXYZ );
        System.out.println( "reference RGB: " + referenceRGB );
        System.out.println( "calculated XYZ: " + xyz );
        System.out.println( "calculated RGB: " + rgb );
        assertTrue( rgb.epsilonEquals( referenceRGB, 0.0001 ) );
        assertTrue( xyz.epsilonEquals( referenceXYZ, 0.0001 ) );
    }
}
