package com.jonathanolson.graphics.color.spaces.tests;

import static org.junit.Assert.assertTrue;
import org.junit.Test;

import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.LabColorspace;
import com.jonathanolson.util.math.vec.Point3d;

public class TestLab {
    @Test
    public void checkInverse() {
        System.out.println( "Checking XYZ <==> Lab invertability" );
        Colorspace space = new LabColorspace();
        Point3d a = new Point3d( 0.1657, 0.32546, 0.25567 );
        Point3d b = new Point3d( a );
        space.toXYZ( b );
        Point3d c = new Point3d( b );
        space.fromXYZ( c );
        assertTrue( a.epsilonEquals( c, 0.0001 ) );
    }

    public void testLabConversion( Point3d referenceXYZ, Point3d referenceLab ) {
        System.out.println( "testing lab conversion with xyz" );
        Point3d a = new Point3d( referenceXYZ );
        Point3d b = new Point3d( referenceLab );
        Colorspace space = new LabColorspace();
        Point3d lab = space.getFromXYZ( referenceXYZ );
        Point3d xyz = space.getToXYZ( referenceLab );

        System.out.println( "reference XYZ: " + referenceXYZ );
        System.out.println( "reference Lab: " + referenceLab );
        System.out.println( "calculated XYZ: " + xyz );
        System.out.println( "calculated Lab: " + lab );
        assertTrue( lab.epsilonEquals( referenceLab, 0.0001 ) );
        assertTrue( xyz.epsilonEquals( referenceXYZ, 0.0001 ) );
    }

    @Test
    public void LabConversionChecksLow() {
        Point3d referenceXYZ = new Point3d( 0.004000, 0.004500, 0.005000 );
        Point3d referenceLab = new Point3d( 4.064833, -1.368841, -2.428097 );
        testLabConversion( referenceXYZ, referenceLab );
    }

    @Test
    public void LabConversionChecksExtraLow() {
        Point3d referenceXYZ = new Point3d( 0.0004000, 0.0004500, 0.0005000 );
        Point3d referenceLab = new Point3d( 0.406483, -0.136884, -0.242810 );
        testLabConversion( referenceXYZ, referenceLab );
    }

    @Test
    public void LabConversionChecksExtraHigh() {
        Point3d referenceXYZ = new Point3d( 0.305950, 0.388892, 0.360251 );
        Point3d referenceLab = new Point3d( 68.670929, -23.929179, -5.735895 );
        testLabConversion( referenceXYZ, referenceLab );
    }
}