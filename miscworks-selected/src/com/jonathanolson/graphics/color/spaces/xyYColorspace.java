package com.jonathanolson.graphics.color.spaces;

import com.jonathanolson.util.math.vec.Point3d;

public class xyYColorspace extends Colorspace {
    @Override
    public void toXYZ( Point3d color ) {
        if ( color.getY() == 0 ) {
            color.set( 0, 0, 0 );
        }
        else {
            color.set(
                    color.getX() * color.getZ() / color.getY(),
                    color.getZ(),
                    ( 1.0 - color.getX() - color.getY() ) * color.getZ() / color.getY()
            );
        }
    }

    @Override
    public void fromXYZ( Point3d color ) {
        double total = color.getX() + color.getY() + color.getZ();
        if ( total == 0 ) {
            color.set( 1.0 / 3.0, 1.0 / 3.0, color.getY() );
        }
        else {
            color.set(
                    color.getX() / total,
                    color.getY() / total,
                    color.getY()
            );
        }
    }
}
