package com.jonathanolson.graphics.color.spaces;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.util.math.vec.Point3d;

public class LabColorspace extends Colorspace {

    private static final double epsilon = 216.0 / 24389.0;
    private static final double kappa = 24389.0 / 27.0;

    private Point3d referenceWhiteXYZ;

    public static final LabColorspace LAB = new LabColorspace();

    public LabColorspace() {
        this( StandardIlluminant.D50_XYZ );
    }

    public LabColorspace( Point3d referenceWhiteXYZ ) {
        this.referenceWhiteXYZ = referenceWhiteXYZ;
    }

    public static double deltaE1976( Point3d lab1, Point3d lab2 ) {
        double l = lab1.getX() - lab2.getX();
        double a = lab1.getY() - lab2.getY();
        double b = lab1.getZ() - lab2.getZ();
        return Math.sqrt( l * l + a * a + b * b );
    }

    // TODO: add other delta E formulas

    @Override
    public void toXYZ( Point3d color ) {
        double L = color.getX();
        double a = color.getY();
        double b = color.getZ();

        double fy = ( L + 16 ) / 116;
        double fx = a / 500 + fy;
        double fz = fy - b / 200;

        double xr = fx * fx * fx <= epsilon ? ( 116 * fx - 16 ) / kappa : fx * fx * fx;
        double yr = fy * fy * fy <= epsilon ? ( 116 * fy - 16 ) / kappa : fy * fy * fy;
        double zr = fz * fz * fz <= epsilon ? ( 116 * fz - 16 ) / kappa : fz * fz * fz;

        double x = xr * referenceWhiteXYZ.getX();
        double y = yr * referenceWhiteXYZ.getY();
        double z = zr * referenceWhiteXYZ.getZ();

        color.set( x, y, z );
    }

    @Override
    public void fromXYZ( Point3d color ) {
        color.set(
                color.getX() / referenceWhiteXYZ.getX(),
                color.getY() / referenceWhiteXYZ.getY(),
                color.getZ() / referenceWhiteXYZ.getZ()
        );
        color.set(
                color.getX() <= epsilon ? ( kappa * color.getX() + 16 ) / 116 : Math.pow( color.getX(), 1.0 / 3.0 ),
                color.getY() <= epsilon ? ( kappa * color.getY() + 16 ) / 116 : Math.pow( color.getY(), 1.0 / 3.0 ),
                color.getZ() <= epsilon ? ( kappa * color.getZ() + 16 ) / 116 : Math.pow( color.getZ(), 1.0 / 3.0 )
        );
        color.set(
                116 * color.getY() - 16,
                500 * ( color.getX() - color.getY() ),
                200 * ( color.getY() - color.getZ() )
        );
    }

    public Point3d getReferenceWhiteXYZ() {
        return referenceWhiteXYZ;
    }
}
