package com.jonathanolson.graphics.color.spaces;

import java.awt.*;

import com.jonathanolson.util.math.vec.Point3d;

public abstract class Colorspace {

    public abstract void toXYZ( Point3d color );

    public abstract void fromXYZ( Point3d color );

    public Point3d getToXYZ( Point3d color ) {
        Point3d ret = new Point3d( color );
        toXYZ( ret );
        return ret;
    }

    public Point3d getFromXYZ( Point3d color ) {
        Point3d ret = new Point3d( color );
        fromXYZ( ret );
        return ret;
    }

    public static Color toColor( Point3d color ) {
        int r = (int) ( color.getX() * 256 );
        int g = (int) ( color.getY() * 256 );
        int b = (int) ( color.getZ() * 256 );
        if ( r > 255 ) {
            r = 255;
        }
        if ( r < 0 ) {
            r = 0;
        }
        if ( g > 255 ) {
            g = 255;
        }
        if ( g < 0 ) {
            g = 0;
        }
        if ( b > 255 ) {
            b = 255;
        }
        if ( b < 0 ) {
            b = 0;
        }
        return new Color( r, g, b );
    }

    public static int convertChannel( double value ) {
        int r = (int) ( value * 256 );
        if ( r > 255 ) {
            r = 255;
        }
        if ( r < 0 ) {
            r = 0;
        }
        return r;
    }
}
