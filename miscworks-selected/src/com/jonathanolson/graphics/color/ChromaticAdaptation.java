package com.jonathanolson.graphics.color;

import com.jonathanolson.util.math.vec.Matrix3d;
import com.jonathanolson.util.math.vec.Point3d;

public class ChromaticAdaptation {
    public static final Matrix3d XYZ_SCALING = new Matrix3d(
            1, 0, 0,
            0, 1, 0,
            0, 0, 1
    );
    public static final Matrix3d BRADFORD = new Matrix3d(
            0.8951000, 0.2664000, -0.1614000,
            -0.7502000, 1.7135000, 0.0367000,
            0.0389000, -0.0685000, 1.0296000
    );
    public static final Matrix3d VON_KRIES = new Matrix3d(
            0.4002400, 0.7076000, -0.0808100,
            -0.2263000, 1.1653200, 0.0457000,
            0.0000000, 0.0000000, 0.9182200
    );

    public static Matrix3d construct( Matrix3d coneResponseDomain, Point3d sourceWhite, Point3d destinationWhite ) {
        Point3d coneSource = new Point3d( sourceWhite );
        coneResponseDomain.transform( coneSource );
        Point3d coneDestination = new Point3d( destinationWhite );
        coneResponseDomain.transform( coneDestination );
        Matrix3d inverse = new Matrix3d( coneResponseDomain );
        inverse.invert();
        Matrix3d middle = new Matrix3d(
                coneDestination.getX() / coneSource.getX(), 0, 0,
                0, coneDestination.getY() / coneSource.getY(), 0,
                0, 0, coneDestination.getZ() / coneSource.getZ()
        );
        inverse.mul( middle );
        inverse.mul( coneResponseDomain );
        return inverse;
    }
}
