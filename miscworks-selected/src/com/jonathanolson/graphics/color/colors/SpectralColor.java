package com.jonathanolson.graphics.color.colors;

public interface SpectralColor {
    public double reflection( double wavelength );
}
