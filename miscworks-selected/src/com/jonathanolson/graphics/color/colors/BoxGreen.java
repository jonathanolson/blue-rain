package com.jonathanolson.graphics.color.colors;

import com.jonathanolson.util.math.Sampling;

public class BoxGreen implements SpectralColor {
    public static final double[] data = {
            0.092, 0.096, 0.098, 0.097, 0.098, 0.095, 0.095, 0.097,
            0.095, 0.094, 0.097, 0.098, 0.096, 0.101, 0.103, 0.104,
            0.107, 0.109, 0.112, 0.115, 0.125, 0.140, 0.160, 0.187,
            0.229, 0.285, 0.343, 0.390, 0.435, 0.464, 0.472, 0.476,
            0.481, 0.462, 0.447, 0.441, 0.426, 0.406, 0.373, 0.347,
            0.337, 0.314, 0.285, 0.277, 0.266, 0.250, 0.230, 0.207,
            0.186, 0.171, 0.160, 0.148, 0.141, 0.136, 0.130, 0.126,
            0.123, 0.121, 0.122, 0.119, 0.114, 0.115, 0.117, 0.117,
            0.118, 0.120, 0.122, 0.128, 0.132, 0.139, 0.144, 0.146,
            0.150, 0.152, 0.157, 0.159
    };

    @Override
    public double reflection( double wavelength ) {
        return Sampling.fromSamples( data, 400, 700, wavelength );
    }
}