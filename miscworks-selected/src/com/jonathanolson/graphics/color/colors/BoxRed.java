package com.jonathanolson.graphics.color.colors;

import com.jonathanolson.util.math.Sampling;

public class BoxRed implements SpectralColor {
    public static final double[] data = {
            0.040, 0.046, 0.048, 0.053, 0.049, 0.050, 0.053, 0.055,
            0.057, 0.056, 0.059, 0.057, 0.061, 0.061, 0.060, 0.062,
            0.062, 0.062, 0.061, 0.062, 0.060, 0.059, 0.057, 0.058,
            0.058, 0.058, 0.056, 0.055, 0.056, 0.059, 0.057, 0.055,
            0.059, 0.059, 0.058, 0.059, 0.061, 0.061, 0.063, 0.063,
            0.067, 0.068, 0.072, 0.080, 0.090, 0.099, 0.124, 0.154,
            0.192, 0.255, 0.287, 0.349, 0.402, 0.443, 0.487, 0.513,
            0.558, 0.584, 0.620, 0.606, 0.609, 0.651, 0.612, 0.610,
            0.650, 0.638, 0.627, 0.620, 0.630, 0.628, 0.642, 0.639,
            0.657, 0.639, 0.635, 0.642
    };

    @Override
    public double reflection( double wavelength ) {
        return Sampling.fromSamples( data, 400, 700, wavelength );
    }
}
