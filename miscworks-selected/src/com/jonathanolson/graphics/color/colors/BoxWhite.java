package com.jonathanolson.graphics.color.colors;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.util.math.Sampling;
import com.jonathanolson.util.math.vec.Point3d;

public class BoxWhite implements SpectralColor {
    public static final double[] data = {
            0.343, 0.445, 0.551, 0.624, 0.665, 0.687, 0.708, 0.723,
            0.715, 0.710, 0.745, 0.758, 0.739, 0.767, 0.777, 0.765,
            0.751, 0.745, 0.748, 0.729, 0.745, 0.757, 0.753, 0.750,
            0.746, 0.747, 0.735, 0.732, 0.739, 0.734, 0.725, 0.721,
            0.733, 0.725, 0.732, 0.743, 0.744, 0.748, 0.728, 0.716,
            0.733, 0.726, 0.713, 0.740, 0.754, 0.764, 0.752, 0.736,
            0.734, 0.741, 0.740, 0.732, 0.745, 0.755, 0.751, 0.744,
            0.731, 0.733, 0.744, 0.731, 0.712, 0.708, 0.729, 0.730,
            0.727, 0.707, 0.703, 0.729, 0.750, 0.760, 0.751, 0.739,
            0.724, 0.730, 0.740, 0.737
    };

    @Override
    public double reflection( double wavelength ) {
        return Sampling.fromSamples( data, 400, 700, wavelength );
    }

    public Point3d getXYZ() {
        double x = 0;
        double y = 0;
        double z = 0;
        for ( double wavelength = 400; wavelength <= 700; wavelength += 1 ) {
            double reflec = reflection( wavelength );
            double[] response = XYZResponse.sample2deg( wavelength );
            x += reflec * response[0];
            y += reflec * response[1];
            z += reflec * response[2];
        }
        x /= y;
        z /= y;
        y = 1;
        return new Point3d( x, y, z );
    }

}