package com.jonathanolson.graphics.color.tests;

import org.junit.Assert;
import org.junit.Test;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.spaces.xyYColorspace;
import com.jonathanolson.graphics.trace.util.Numbers;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;

public class StandardIlluminantTests {
    @Test
    public void testDIlluminantXY() {
        System.out.println( "Testing D illuminant xy" );
        Point3d d65spec = new Point3d( StandardIlluminant.D65_XYZ );
        ( new xyYColorspace() ).fromXYZ( d65spec );
        Point2d d65comp = StandardIlluminant.cieDIlluminantChroma( 6500 );
        System.out.println( "specified xyY: " + d65spec );
        System.out.println( "computed xy: " + d65comp );
        Assert.assertTrue( Numbers.equalsEpsilon( d65spec.x, d65comp.x, 0.001 ) );
        Assert.assertTrue( Numbers.equalsEpsilon( d65spec.y, d65comp.y, 0.001 ) );
    }
}
