package com.jonathanolson.graphics.test;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.jonathanolson.graphics.ImagePanel;

public class ColorTest {
    public static Color getColorAt( BufferedImage image, int x, int y ) {
        int[] pixel = new int[3];
        pixel = image.getRaster().getPixel( 0, 0, pixel );
        float[] scaledPixel = image.getColorModel().getNormalizedComponents( pixel, 0, null, 0 );
        return new Color( image.getColorModel().getColorSpace(), scaledPixel, 1.0f );
    }

    public static void debugColor( Color color ) {
        System.out.println( color );
        float[] cmps = color.getColorComponents( null );
        System.out.println( "Raw: " + colorComponents( cmps ) );
        System.out.println( "RGB: " + colorComponents( color.getColorSpace().toRGB( cmps ) ) );
        System.out.println( "XYZ: " + colorComponents( color.getColorSpace().toCIEXYZ( cmps ) ) );
        System.out.println( "LinearRGB: " + colorComponents( color.getColorComponents( ColorSpace.getInstance( ColorSpace.CS_LINEAR_RGB ), null ) ) );
        System.out.println( "L*,a*,b*: " + colorComponents( color.getColorComponents( getLabColorSpace(), null ) ) );
    }

    public static String colorComponents( float[] cmps ) {
        String ret = "[";
        for ( float cmp : cmps ) {
            ret += " " + cmp;
        }
        ret += " ]";
        return ret;
    }

    private static String svnBase;
    private static ColorSpace labColorSpace = null;

    public static ColorSpace getLabColorSpace() {
        if ( labColorSpace == null ) {
            ICC_Profile anICCLabProfile = null;
            try {
                anICCLabProfile = ICC_Profile.getInstance( svnBase + "/trunk/data/lab8.icm" );
            }
            catch( IOException e ) {
                e.printStackTrace();
            }
            labColorSpace = new ICC_ColorSpace( anICCLabProfile );
        }
        return labColorSpace;
    }

    public static void main( String[] args ) {
        svnBase = args[0];

        try {
            File imageFile = new File( svnBase + "/trunk/data/DSC_2079_nef2-small.png" );
            System.out.println( "File: " + imageFile );
            BufferedImage image = ImageIO.read( imageFile );

            JFrame frame = new JFrame( "ColorTest" );
            JPanel container = new JPanel();
            frame.setContentPane( container );
            container.add( new ImagePanel( image ) );


            Color color = getColorAt( image, 0, 0 );
            debugColor( color );


            final ColorModel dstColorModel = new ComponentColorModel( getLabColorSpace(), new int[]{8, 8, 8}, false, false, ColorModel.OPAQUE, DataBuffer.TYPE_BYTE );
            final BufferedImage aDstImg = new BufferedImage( dstColorModel, image.getRaster().createCompatibleWritableRaster(), false, null );

            // Create a ColorConvertOp with the ColorSpace from the source and the lab colorspace
            final ColorSpace srcSpace = image.getColorModel().getColorSpace();
            final ColorConvertOp cco = new ColorConvertOp( srcSpace, getLabColorSpace(), new RenderingHints( RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY ) );
            cco.filter( image, aDstImg );

            final int numPixels = aDstImg.getWidth() * aDstImg.getHeight();

            // L channel as grayscale image
            final BufferedImage luminanceImage = new BufferedImage( aDstImg.getWidth(), aDstImg.getHeight(), BufferedImage.TYPE_BYTE_GRAY );
            luminanceImage.getRaster().setSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 0, aDstImg.getRaster().getSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 0, new int[numPixels] ) );

            // a channel as grayscale image
            final BufferedImage aChannelImage = new BufferedImage( aDstImg.getWidth(), aDstImg.getHeight(), BufferedImage.TYPE_BYTE_GRAY );
            aChannelImage.getRaster().setSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 0, aDstImg.getRaster().getSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 1, new int[numPixels] ) );

            // b channel as grayscale image
            final BufferedImage bChannelImage = new BufferedImage( aDstImg.getWidth(), aDstImg.getHeight(), BufferedImage.TYPE_BYTE_GRAY );
            bChannelImage.getRaster().setSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 0, aDstImg.getRaster().getSamples( 0, 0, aDstImg.getWidth(), aDstImg.getHeight(), 2, new int[numPixels] ) );

            container.add( new ImagePanel( luminanceImage ) );
            container.add( new ImagePanel( aChannelImage ) );
            container.add( new ImagePanel( bChannelImage ) );

            frame.pack();
            frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
            frame.setVisible( true );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }
}
