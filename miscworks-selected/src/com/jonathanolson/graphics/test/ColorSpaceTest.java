package com.jonathanolson.graphics.test;

import java.awt.color.ColorSpace;

import com.jonathanolson.graphics.trace.util.ColorUtils;

public class ColorSpaceTest {
    public static void main( String[] args ) {
        ColorSpace xyz = ColorUtils.getXYZ();
        for ( float x = 0; x < 3; x += 0.1 ) {
            float[] r = xyz.toRGB( new float[]{0.3f, x, 0.3f} );
            System.out.println( x + ": " + r[0] + ", " + r[1] + ", " + r[2] );
        }
    }
}
