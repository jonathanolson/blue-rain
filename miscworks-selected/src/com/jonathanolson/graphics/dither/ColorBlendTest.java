package com.jonathanolson.graphics.dither;

import java.awt.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import com.jonathanolson.graphics.color.spaces.LabColorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.util.math.vec.Point3d;

public class ColorBlendTest {

    private static final int steps = 254;

    public static void main( String[] args ) {
        Point3d a = new Point3d( 255, 0, 0 );
        Point3d b = new Point3d( 128, 255, 128 );

        Point3d xyzA = new Point3d( a );
        Point3d xyzB = new Point3d( b );
        xyzA.scale( 1.0 / 256.0 );
        xyzB.scale( 1.0 / 256.0 );
        RGBColorspace.sRGB.toXYZ( xyzA );
        RGBColorspace.sRGB.toXYZ( xyzB );

        Point3d labA = new Point3d( xyzA );
        Point3d labB = new Point3d( xyzB );
        LabColorspace.LAB.fromXYZ( labA );
        LabColorspace.LAB.fromXYZ( labB );

        Point3d[] bads = new Point3d[steps];
        Point3d[] goods = new Point3d[steps];
        Point3d[] bests = new Point3d[steps];

        for ( int i = 1; i <= steps; i++ ) {
            double ar = ( (double) ( ( steps + 1 ) - i ) ) / ( (double) ( steps + 1 ) );
            double br = 1 - ar;

            bads[i - 1] = new Point3d( a.x * ar + b.x * br, a.y * ar + b.y * br, a.z * ar + b.z * br );

            goods[i - 1] = new Point3d( xyzA.x * ar + xyzB.x * br, xyzA.y * ar + xyzB.y * br, xyzA.z * ar + xyzB.z * br );
            RGBColorspace.sRGB.fromXYZ( goods[i - 1] );
            goods[i - 1].scale( 256.0 );
            goods[i - 1].clamp( 0, 255 );

            bests[i - 1] = new Point3d( labA.x * ar + labB.x * br, labA.y * ar + labB.y * br, labA.z * ar + labB.z * br );
            LabColorspace.LAB.toXYZ( bests[i - 1] );
            RGBColorspace.sRGB.fromXYZ( bests[i - 1] );
            bests[i - 1].scale( 256.0 );
            bests[i - 1].clamp( 0, 255 );
        }

        JFrame frame = new JFrame( "Color Blend Test" );
        JPanel container = new JPanel();
        frame.setContentPane( container );
        container.setLayout( new GridLayout( 3, steps + 2, 0, 5 ) );
        container.setBackground( Color.BLACK );

        container.add( coloredBox( a ) );
        for ( Point3d bad : bads ) {
            container.add( coloredBox( bad ) );
        }
        container.add( coloredBox( b ) );

        container.add( coloredBox( a ) );
        for ( Point3d good : goods ) {
            container.add( coloredBox( good ) );
        }
        container.add( coloredBox( b ) );

        container.add( coloredBox( a ) );
        for ( Point3d best : bests ) {
            container.add( coloredBox( best ) );
        }
        container.add( coloredBox( b ) );

        frame.pack();
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setVisible( true );
    }

    private static Component coloredBox( Point3d color ) {
        JPanel panel = new JPanel( new GridLayout( 2, steps + 2, 0, 0 ) );
        panel.setBorder( new EmptyBorder( 0, 0, 0, 0 ) );
        Component box = Box.createRigidArea( new Dimension( 2, 50 ) );
        panel.add( box );
        panel.setBackground( new Color( (int) color.x, (int) color.y, (int) color.z ) );
        return panel;
    }
}
