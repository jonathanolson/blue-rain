package com.jonathanolson.graphics.dither;

import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.util.math.vec.Point3d;

/**
 * NOTE: not thread safe
 */
public class LegoColorMap implements ColorMap {
    private Point3d best;
    private double bestDelta;

    public LegoColorMap() {
    }

    public static Point3d white() {
        return new Point3d( 255, 255, 255 );
//        return new Point3d( 242, 243, 242 );
    }

    public static Point3d lightGrey() {
        return new Point3d( 156, 156, 156 );
//        return new Point3d( 199, 193, 183 );
    }

    public static Point3d black() {
        return new Point3d( 0, 0, 0 );
//        return new Point3d( 27, 42, 52 );
    }

    public static Point3d red() {
        return new Point3d( 179, 0, 6 );
//        return new Point3d( 196, 40, 27 );
    }

    public static Point3d brown() {
        return new Point3d( 83, 33, 21 );
    }

    public static Point3d yellow() {
        return new Point3d( 247, 209, 23 );
    }

    public static Point3d green() {
        return new Point3d( 0, 100, 46 );
    }

    public static Point3d blue() {
        return new Point3d( 0, 87, 166 );
    }

    @Override
    public Point3d getClosestColor( Point3d color ) {
        best = new Point3d( 0, 0, 0 );
        bestDelta = Double.POSITIVE_INFINITY;

        testCandidate( color, white() );
        testCandidate( color, lightGrey() );
        testCandidate( color, black() );
        testCandidate( color, red() );
//        testCandidate( color, brown() );
        testCandidate( color, yellow() );
//        testCandidate( color, green() );
        testCandidate( color, blue() );

        return best;
    }

    private void testCandidate( Point3d color, Point3d candidate ) {
        RGBColorspace.sRGB.toXYZ( candidate );
        double delta = candidate.distance( color );
        if ( delta < bestDelta ) {
            bestDelta = delta;
            best = candidate;
        }
    }

}
