package com.jonathanolson.graphics.dither;

import java.util.*;

/**
 * Test to see whether adding "images" to what at full-resolution looks grey is feasible
 */
public class GammaBackTest {

    /*
Desired output:
count: 1919300
63 0,0,1,254
64 1,2,2,254
65 3,3,3,254
66 4,4,5,254
67 5,6,6,254
68 0,0,22,253
69 2,2,22,253
70 4,5,21,253
71 4,11,19,253
72 10,11,17,253
73 14,14,14,253
74 15,15,16,253
75 4,14,33,252
76 7,16,32,252
77 11,18,30,252
78 16,21,26,252
79 22,22,23,252
80 16,16,40,251
81 17,22,37,251
82 22,27,31,251
83 28,28,28,251
84 18,30,41,250
85 27,29,37,250
86 22,27,49,249
87 23,37,42,249
88 35,35,36,249
89 26,36,49,248
90 35,40,40,248
91 28,40,52,247
92 40,40,44,247
93 31,45,53,246
94 44,44,45,246
95 36,51,51,245
96 33,53,57,244
97 48,49,50,244
98 41,54,57,243
99 41,52,64,242
100 43,50,69,241
101 55,55,56,241
102 51,57,63,240
103 50,62,64,239
104 55,56,70,238
105 57,57,72,237
106 54,68,69,236
107 58,65,73,235
108 61,68,72,234
109 68,69,69,233
110 57,76,79,231
111 67,67,83,230
112 69,75,78,229
113 65,76,87,227
114 71,81,81,226
115 68,84,87,224
116 81,81,82,223
117 79,81,90,221
118 81,81,94,219
119 79,88,95,217
120 83,88,97,215
121 88,89,97,213
122 82,97,102,210
123 91,93,103,208
124 88,101,105,205
125 95,95,111,202
126 96,100,112,199
127 100,105,110,196
128 100,111,112,192
129 107,108,116,188
130 106,117,117,183
131 115,116,118,178
132 115,121,124,171
133 121,122,130,162
134 125,133,135,146
135 135,135,135,135
     */

    public static final int R0 = 135;
    public static final double GAMMA = 2.2;
    public static final double INV_GAMMA = 1 / 2.2;

    public static final double[] intmap = new double[256];

    public static final double[] cur = new double[4];
    public static final int[] curint = new int[4];

    public static final double ERR;
    public static final double y;

    public static final Map<Integer, Best> bestmap = new HashMap<Integer, Best>();

    public static int count = 0;

    static {
        for ( int i = 0; i < 256; i++ ) {
            intmap[i] = Math.pow( ( (double) i ) / 255.0, GAMMA );
        }
        ERR = Math.abs( intmap[R0] - intmap[R0 - 1] );
        // -1: 1919300
        // -2: 1919300
        y = 4 * intmap[R0];
    }

    public static void main( String[] args ) {

        iter( 4, y );

        System.out.println( "count: " + count );

        List<Integer> rls = new LinkedList<Integer>( bestmap.keySet() );
        Collections.sort( rls );

        for ( Integer rl : rls ) {
            Best best = bestmap.get( rl );
            int[] x = best.getEntries();
            System.out.println( rl + " " + x[0] + "," + x[1] + "," + x[2] + "," + x[3] );
        }

    }

    private static double currentError() {
        return Math.sqrt( ( R0 - curint[0] ) * ( R0 - curint[0] ) + ( R0 - curint[1] ) * ( R0 - curint[1] ) + ( R0 - curint[2] ) * ( R0 - curint[2] ) + ( R0 - curint[3] ) * ( R0 - curint[3] ) );
    }

    private static void check() {
        if ( (int) ( Math.pow( ( cur[0] + cur[1] + cur[2] + cur[3] ) / 4, INV_GAMMA ) * 255 ) == R0 ) {
            //System.out.println( cur[0] + "," + cur[1] + "," + cur[2] + "," + cur[3] );
            //System.out.println( ( curint[0] + curint[1] + curint[2] + curint[3] ) / 4 );
            int rl = ( curint[0] + curint[1] + curint[2] + curint[3] ) / 4;
            count++;
            Best best = bestmap.get( rl );
            if ( best == null ) {
                bestmap.put( rl, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
            }
            else {
                if ( currentError() < best.getError() ) {
                    bestmap.put( rl, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
                }
            }
        }
    }

    private static void iter( int n, double yleft ) {
        if ( n == 0 ) {
            check();
            return;
        }
        if ( n == 3 ) {
            System.out.println( curint[0] );
        }
        if ( yleft < -ERR || yleft > ( (double) n ) + ERR ) {
            return;
        }
        int low = 0;
        if ( n < 4 ) {
            low = curint[3 - n];
        }
        for ( int i = low; i < 256; i++ ) {
            double entry = intmap[i];
            cur[4 - n] = entry;
            curint[4 - n] = i;
            iter( n - 1, yleft - entry );
        }
    }

    public static class Best {
        double error;
        int[] entries;

        public Best( double error, int[] entries ) {
            this.error = error;
            this.entries = entries;
        }

        public double getError() {
            return error;
        }

        public int[] getEntries() {
            return entries;
        }
    }
}
