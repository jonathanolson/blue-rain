package com.jonathanolson.graphics.dither;

import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.LabColorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.util.math.vec.Point3d;

/**
 * NOTE: not thread safe
 *
 * BROKEN
 */
public class SatLabColorMap implements ColorMap {
    private Point3d best;
    private double bestDelta;

    private Colorspace space = new LabColorspace( );

    public SatLabColorMap() {
    }

    @Override
    public Point3d getClosestColor( Point3d color ) {
        best = new Point3d( 0, 0, 0 );
        bestDelta = Double.POSITIVE_INFINITY;

        color.clampMin( 0 );
        space.fromXYZ( color );
        if( color.x < 0 ) {
            color.x = 0;
        }

        for ( int i = 0; i < 256; i++ ) {
            testCandidate( color, new Point3d( i, 0, 0 ) );
            testCandidate( color, new Point3d( 0, i, 0 ) );
            testCandidate( color, new Point3d( 0, 0, i ) );

            testCandidate( color, new Point3d( i, 255, 0 ) );
            testCandidate( color, new Point3d( 255, i, 0 ) );
            testCandidate( color, new Point3d( 255, 0, i ) );

            testCandidate( color, new Point3d( i, 0, 255 ) );
            testCandidate( color, new Point3d( 0, i, 255 ) );
            testCandidate( color, new Point3d( 0, 255, i ) );

            testCandidate( color, new Point3d( i, 255, 255 ) );
            testCandidate( color, new Point3d( 255, i, 255 ) );
            testCandidate( color, new Point3d( 255, 255, i ) );
        }

        return best;
    }

    private void testCandidate( Point3d color, Point3d candidate ) {
        RGBColorspace.sRGB.toXYZ( candidate );
        Point3d labCandidate = new Point3d( candidate );
        labCandidate.clampMin( 0 );
        space.fromXYZ( labCandidate );
        if( labCandidate.x < 0 ) {
            labCandidate.x = 0;
        }
        double delta = labCandidate.distance( color );
        if ( delta < bestDelta ) {
            bestDelta = delta;
            best = candidate;
        }
    }

}