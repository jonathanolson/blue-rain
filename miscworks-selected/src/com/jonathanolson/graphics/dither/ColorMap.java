package com.jonathanolson.graphics.dither;

import com.jonathanolson.util.math.vec.Point3d;

public interface ColorMap {
    public Point3d getClosestColor( Point3d color );
}
