package com.jonathanolson.graphics.dither;

import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.imageio.ImageIO;

import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;

public class GammaGrayDoubleFake {

    private static final GammaColorTable table = new GammaColorTable( 135, 2.2 );

    public static Map<Integer, int[]> grayMap = new HashMap<Integer, int[]>();// TODO remove

    private static final Random random = new Random();

    public static Raster3 fakeGray( WritableRaster inRaster ) {
        Raster3 outRaster = new Raster3( inRaster.getWidth() * 2, inRaster.getHeight() * 2, new Point2d( 0, 0 ), 1 );
        for ( int y = 0; y < inRaster.getHeight(); y++ ) {
            for ( int x = 0; x < inRaster.getWidth(); x++ ) {
                int[] in = new int[4];
                inRaster.getPixel( x, y, in );
                Point3d[] ret = new Point3d[4];
                ret[0] = new Point3d();
                ret[1] = new Point3d();
                ret[2] = new Point3d();
                ret[3] = new Point3d();

                int[] gray0 = grayMap.get( mapToFit( in[0] ) );
                int[] gray1 = grayMap.get( mapToFit( in[1] ) );
                int[] gray2 = grayMap.get( mapToFit( in[2] ) );

                permute( ret );
                ret[0].setX( gray0[0] );
                ret[1].setX( gray0[1] );
                ret[2].setX( gray0[2] );
                ret[3].setX( gray0[3] );
                permute( ret );
                ret[0].setY( gray1[0] );
                ret[1].setY( gray1[1] );
                ret[2].setY( gray1[2] );
                ret[3].setY( gray1[3] );
                permute( ret );
                ret[0].setZ( gray2[0] );
                ret[1].setZ( gray2[1] );
                ret[2].setZ( gray2[2] );
                ret[3].setZ( gray2[3] );

                outRaster.setPixelPoint( x * 2, y * 2, ret[0] );
                outRaster.setPixelPoint( x * 2 + 1, y * 2, ret[1] );
                outRaster.setPixelPoint( x * 2, y * 2 + 1, ret[2] );
                outRaster.setPixelPoint( x * 2 + 1, y * 2 + 1, ret[3] );
            }
        }
        return outRaster;
    }

    private static void permute( Point3d[] points ) {
        // low quality, and only for 4 points
        for ( int i = 0; i < points.length; i++ ) {
            int r = random.nextInt( points.length - 1 );
            if ( r == i ) { r++; }
            Point3d tmp = points[i];
            points[i] = points[r];
            points[r] = tmp;
        }
    }

    public static int mapToFit( int in ) {
        return ( in * ( 135 - 63 ) ) / 255 + 63;
    }

    public static void fillMap() {
        grayMap.put( 63, new int[]{0, 0, 1, 254} );
        grayMap.put( 64, new int[]{1, 2, 2, 254} );
        grayMap.put( 65, new int[]{3, 3, 3, 254} );
        grayMap.put( 66, new int[]{4, 4, 5, 254} );
        grayMap.put( 67, new int[]{5, 6, 6, 254} );
        grayMap.put( 68, new int[]{0, 0, 22, 253} );
        grayMap.put( 69, new int[]{2, 2, 22, 253} );
        grayMap.put( 70, new int[]{4, 5, 21, 253} );
        grayMap.put( 71, new int[]{4, 11, 19, 253} );
        grayMap.put( 72, new int[]{10, 11, 17, 253} );
        grayMap.put( 73, new int[]{14, 14, 14, 253} );
        grayMap.put( 74, new int[]{15, 15, 16, 253} );
        grayMap.put( 75, new int[]{4, 14, 33, 252} );
        grayMap.put( 76, new int[]{7, 16, 32, 252} );
        grayMap.put( 77, new int[]{11, 18, 30, 252} );
        grayMap.put( 78, new int[]{16, 21, 26, 252} );
        grayMap.put( 79, new int[]{22, 22, 23, 252} );
        grayMap.put( 80, new int[]{16, 16, 40, 251} );
        grayMap.put( 81, new int[]{17, 22, 37, 251} );
        grayMap.put( 82, new int[]{22, 27, 31, 251} );
        grayMap.put( 83, new int[]{28, 28, 28, 251} );
        grayMap.put( 84, new int[]{18, 30, 41, 250} );
        grayMap.put( 85, new int[]{27, 29, 37, 250} );
        grayMap.put( 86, new int[]{22, 27, 49, 249} );
        grayMap.put( 87, new int[]{23, 37, 42, 249} );
        grayMap.put( 88, new int[]{35, 35, 36, 249} );
        grayMap.put( 89, new int[]{26, 36, 49, 248} );
        grayMap.put( 90, new int[]{35, 40, 40, 248} );
        grayMap.put( 91, new int[]{28, 40, 52, 247} );
        grayMap.put( 92, new int[]{40, 40, 44, 247} );
        grayMap.put( 93, new int[]{31, 45, 53, 246} );
        grayMap.put( 94, new int[]{44, 44, 45, 246} );
        grayMap.put( 95, new int[]{36, 51, 51, 245} );
        grayMap.put( 96, new int[]{33, 53, 57, 244} );
        grayMap.put( 97, new int[]{48, 49, 50, 244} );
        grayMap.put( 98, new int[]{41, 54, 57, 243} );
        grayMap.put( 99, new int[]{41, 52, 64, 242} );
        grayMap.put( 100, new int[]{43, 50, 69, 241} );
        grayMap.put( 101, new int[]{55, 55, 56, 241} );
        grayMap.put( 102, new int[]{51, 57, 63, 240} );
        grayMap.put( 103, new int[]{50, 62, 64, 239} );
        grayMap.put( 104, new int[]{55, 56, 70, 238} );
        grayMap.put( 105, new int[]{57, 57, 72, 237} );
        grayMap.put( 106, new int[]{54, 68, 69, 236} );
        grayMap.put( 107, new int[]{58, 65, 73, 235} );
        grayMap.put( 108, new int[]{61, 68, 72, 234} );
        grayMap.put( 109, new int[]{68, 69, 69, 233} );
        grayMap.put( 110, new int[]{57, 76, 79, 231} );
        grayMap.put( 111, new int[]{67, 67, 83, 230} );
        grayMap.put( 112, new int[]{69, 75, 78, 229} );
        grayMap.put( 113, new int[]{65, 76, 87, 227} );
        grayMap.put( 114, new int[]{71, 81, 81, 226} );
        grayMap.put( 115, new int[]{68, 84, 87, 224} );
        grayMap.put( 116, new int[]{81, 81, 82, 223} );
        grayMap.put( 117, new int[]{79, 81, 90, 221} );
        grayMap.put( 118, new int[]{81, 81, 94, 219} );
        grayMap.put( 119, new int[]{79, 88, 95, 217} );
        grayMap.put( 120, new int[]{83, 88, 97, 215} );
        grayMap.put( 121, new int[]{88, 89, 97, 213} );
        grayMap.put( 122, new int[]{82, 97, 102, 210} );
        grayMap.put( 123, new int[]{91, 93, 103, 208} );
        grayMap.put( 124, new int[]{88, 101, 105, 205} );
        grayMap.put( 125, new int[]{95, 95, 111, 202} );
        grayMap.put( 126, new int[]{96, 100, 112, 199} );
        grayMap.put( 127, new int[]{100, 105, 110, 196} );
        grayMap.put( 128, new int[]{100, 111, 112, 192} );
        grayMap.put( 129, new int[]{107, 108, 116, 188} );
        grayMap.put( 130, new int[]{106, 117, 117, 183} );
        grayMap.put( 131, new int[]{115, 116, 118, 178} );
        grayMap.put( 132, new int[]{115, 121, 124, 171} );
        grayMap.put( 133, new int[]{121, 122, 130, 162} );
        grayMap.put( 134, new int[]{125, 133, 135, 146} );
        grayMap.put( 135, new int[]{135, 135, 135, 135} );

    }

    public static void main( String[] args ) {
        File imageFile = new File( args[0] );
        File outFile = new File( args[1] );

        fillMap();

        try {
            BufferedImage inImage = ImageIO.read( imageFile );

            Raster3 raster = fakeGray( inImage.getRaster() );
            BufferedImage outImage = new BufferedImage( raster.getWidth(), raster.getHeight(), BufferedImage.TYPE_INT_RGB );

            int[] cacheRow = new int[raster.getWidth() * 3];

            for ( int i = 0; i < raster.getHeight(); i++ ) {
                for ( int j = 0; j < raster.getWidth(); j++ ) {
                    cacheRow[j * 3] = (int) raster.getPixelPoint( j, i ).getX();
                    cacheRow[j * 3 + 1] = (int) raster.getPixelPoint( j, i ).getY();
                    cacheRow[j * 3 + 2] = (int) raster.getPixelPoint( j, i ).getZ();
                }
                outImage.getRaster().setPixels( 0, i, raster.getWidth(), 1, cacheRow );
            }

            ImageIO.write( outImage, "png", outFile );

        }
        catch( IOException e ) {
            e.printStackTrace();
        }


    }


}