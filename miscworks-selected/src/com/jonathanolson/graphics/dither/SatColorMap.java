package com.jonathanolson.graphics.dither;

import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.util.math.vec.Point3d;

/**
 * NOTE: not thread safe
 */
public class SatColorMap implements ColorMap {
    private Point3d best;
    private double bestDelta;

    public SatColorMap() {
    }

    @Override
    public Point3d getClosestColor( Point3d color ) {
        best = new Point3d( 0, 0, 0 );
        bestDelta = Double.POSITIVE_INFINITY;

        for ( int i = 0; i < 256; i++ ) {
            testCandidate( color, new Point3d( i, 0, 0 ) );
            testCandidate( color, new Point3d( 0, i, 0 ) );
            testCandidate( color, new Point3d( 0, 0, i ) );

            testCandidate( color, new Point3d( i, 255, 0 ) );
            testCandidate( color, new Point3d( 255, i, 0 ) );
            testCandidate( color, new Point3d( 255, 0, i ) );

            testCandidate( color, new Point3d( i, 0, 255 ) );
            testCandidate( color, new Point3d( 0, i, 255 ) );
            testCandidate( color, new Point3d( 0, 255, i ) );

            testCandidate( color, new Point3d( i, 255, 255 ) );
            testCandidate( color, new Point3d( 255, i, 255 ) );
            testCandidate( color, new Point3d( 255, 255, i ) );
        }

        return best;
    }

    private void testCandidate( Point3d color, Point3d candidate ) {
        RGBColorspace.sRGB.toXYZ( candidate );
        double delta = candidate.distance( color );
        if ( delta < bestDelta ) {
            bestDelta = delta;
            best = candidate;
        }
    }

}
