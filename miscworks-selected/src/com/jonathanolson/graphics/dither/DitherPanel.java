package com.jonathanolson.graphics.dither;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.*;

public class DitherPanel extends JComponent {
    private File imageFile;
    private BufferedImage image;

    private static Random random = new Random( System.currentTimeMillis() );

    public DitherPanel( final File inFile, final File outFile ) {
        this.imageFile = inFile;
        try {
            image = ImageIO.read( inFile );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }

        addMouseListener( new MouseListener() {
            public void mouseClicked( MouseEvent mouseEvent ) {
                try {
                    image = ImageIO.read( inFile );
                    AwtDither.standardDither( image.getRaster(), new LegoColorMap() );
//                    AwtDither.standardDither( image.getRaster(), new SatColorMap() );
                    DitherPanel.this.repaint();
                    ImageIO.write( image, "png", outFile );
                }
                catch( IOException e ) {
                    e.printStackTrace();
                }
            }

            public void mousePressed( MouseEvent mouseEvent ) {

            }

            public void mouseReleased( MouseEvent mouseEvent ) {

            }

            public void mouseEntered( MouseEvent mouseEvent ) {

            }

            public void mouseExited( MouseEvent mouseEvent ) {

            }
        } );
    }

    @Override
    public void paint( Graphics g ) {
        g.drawImage( image, 0, 0, null );
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension( image.getWidth(), image.getHeight() );
    }

    public static void main( String[] args ) {
        File imageFile = new File( args[0] );
        File outFile = new File( args[1] );
        JFrame frame = new JFrame( "DitherPanel" );
        JPanel container = new JPanel();
        frame.setContentPane( container );
        container.add( new DitherPanel( imageFile, outFile ) );
        container.add( new DitherPanel( imageFile, outFile ) );
        frame.pack();
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.setVisible( true );
    }
}
