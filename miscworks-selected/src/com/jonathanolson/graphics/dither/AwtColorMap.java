package com.jonathanolson.graphics.dither;

import java.util.List;

public interface AwtColorMap {
    public int[] getClosestColor( final int[] color );

    public static class Monochrome implements AwtColorMap {
        public int[] getClosestColor( final int[] color ) {
            if ( color[0] + color[1] + color[2] > 128 * 3 ) {
                return new int[]{255, 255, 255};
            }
            else {
                return new int[]{0, 0, 0};
            }
        }
    }

    public static class EightBit implements AwtColorMap {
        // lazy implementation
        public int[] getClosestColor( final int[] color ) {
            int[] c = new int[4];
            c[0] = ( color[0] >> 5 ) * 36;
            c[1] = ( color[1] >> 5 ) * 36;
            c[2] = ( color[2] >> 5 ) * 36;
            return c;
        }
    }

    public static class ModSat implements AwtColorMap {
        public int[] getClosestColor( int[] color ) {
            int sum = color[0] + color[1] + color[2];
            int lowidx;
            if ( color[0] > color[1] ) {
                lowidx = ( color[1] > color[2] ? 2 : 1 );
            }
            else {
                lowidx = ( color[0] > color[2] ? 2 : 0 );
            }
            int highidx;
            if ( color[0] > color[1] ) {
                highidx = ( color[0] > color[2] ? 0 : 2 );
            }
            else {
                highidx = ( color[1] > color[2] ? 1 : 2 );
            }
            int[] ret = new int[4];
            ret[3] = 255;
            for ( int i = 0; i < 3; i++ ) {
                if ( sum > 255 * 2 ) {
                    if ( i == lowidx ) {
                        ret[i] = sum - 255 * 2;
                    }
                    else {
                        ret[i] = 255;
                    }
                }
                else if ( sum > 255 ) {
                    if ( i == lowidx ) {
                        ret[i] = 0;
                    }
                    else if ( i == highidx ) {
                        ret[i] = 255;
                    }
                    else {
                        ret[i] = sum - 255;
                    }
                }
                else {
                    if ( i == highidx ) {
                        ret[i] = sum;
                    }
                    else {
                        ret[i] = 0;
                    }
                }
            }
            return ret;
        }
    }

    public static class HalfSat implements AwtColorMap {
        public int[] getClosestColor( int[] color ) {
            int sum = color[0] + color[1] + color[2];
            int maxidx;
            if ( color[0] > color[1] ) {
                maxidx = ( color[0] > color[2] ? 0 : 2 );
            }
            else {
                maxidx = ( color[1] > color[2] ? 1 : 2 );
            }
            int[] ret = new int[4];
            for ( int i = 0; i < 3; i++ ) {
                if ( sum > 255 ) {
                    if ( i == maxidx ) {
                        ret[i] = 255;
                    }
                    else {
                        ret[i] = ( sum - 255 ) / 2;
                    }
                }
                else {
                    if ( i == maxidx ) {
                        ret[i] = sum;
                    }
                    else {
                        ret[i] = 0;
                    }
                }
            }
            return ret;
        }
    }

    public static class ColorListMap implements AwtColorMap {
        private List<int[]> colors;

        public ColorListMap( List<int[]> colors ) {
            this.colors = colors;
        }

        public int[] getClosestColor( int[] color ) {
            int bestScore = 0;
            boolean scored = false;
            int[] bestColor = null;
            for ( int[] possibleColor : colors ) {
                int score = 0;
                score += abs( color[0] - possibleColor[0] );
                score += abs( color[1] - possibleColor[1] );
                score += abs( color[2] - possibleColor[2] );
                if ( !scored || score < bestScore ) {
                    scored = true;
                    bestColor = possibleColor;
                    bestScore = score;
                }
            }

            return bestColor;
        }

        private static int abs( int a ) {
            if ( a > 0 ) {
                return a;
            }
            else {
                return -a;
            }
        }
    }

}
