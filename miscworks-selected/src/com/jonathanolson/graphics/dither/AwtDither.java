package com.jonathanolson.graphics.dither;

import java.awt.image.WritableRaster;

import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;

public class AwtDither {
    public static void standardDither( WritableRaster raster, ColorMap map ) {
        Colorspace rgb = RGBColorspace.sRGB;

        Raster3 rast = internal( raster );

        System.out.println( "Boo");

        for ( int y = 0; y < raster.getHeight(); y++ ) {
            for ( int x = 0; x < raster.getWidth(); x++ ) {
                Point3d point = rast.getPixelPoint( x, y );
                rgb.toXYZ( point );
                rast.setPixelPoint( x, y, point );
            }
        }

        System.out.println( "Who");

        for ( int y = 0; y < rast.getHeight(); y++ ) {
            System.out.println("y: " + y);
            for ( int x = 0; x < rast.getWidth(); x++ ) {
                Point3d old = rast.getPixelPoint( x, y );

                // TODO: fix with getClosestColor
                Point3d replacement = map.getClosestColor( old );

                Point3d diff = new Point3d();
                diff.sub( old, replacement );

                rast.setPixelPoint( x, y, replacement );

                if ( x + 1 < rast.getWidth() ) {
                    addColor( rast, x + 1, y, new Point3d( diff.x * 7 / 16, diff.y * 7 / 16, diff.z * 7 / 16 ) );
                }
                if ( y + 1 < rast.getHeight() ) {
                    addColor( rast, x, y + 1, new Point3d( diff.x * 5 / 16, diff.y * 5 / 16, diff.z * 5 / 16 ) );
                    if ( x - 1 >= 0 ) {
                        addColor( rast, x - 1, y + 1, new Point3d( diff.x * 3 / 16, diff.y * 3 / 16, diff.z * 3 / 16 ) );
                    }
                    if ( x + 1 < rast.getWidth() ) {
                        addColor( rast, x + 1, y + 1, new Point3d( diff.x * 1 / 16, diff.y * 1 / 16, diff.z * 1 / 16 ) );
                    }
                }
            }
        }

        System.out.println("YAY");

        for ( int y = 0; y < raster.getHeight(); y++ ) {
            for ( int x = 0; x < raster.getWidth(); x++ ) {
                Point3d point = rast.getPixelPoint( x, y );
                rgb.fromXYZ( point );
                rast.setPixelPoint( x, y, point );
            }
        }

        System.out.println("OOH");

        writeToRaster( rast, raster );
    }

    private static int[] clampColor( int[] color ) {
        return clampColor( color, 3, 0, 255 );
    }

    private static void addColor( Raster3 raster, int x, int y, Point3d color ) {
        Point3d old = raster.getPixelPoint( x, y );
        old.add( color );
        raster.setPixelPoint( x, y, old );
    }

    private static int[] clampColor( int[] color, int channels, int min, int max ) {
        //System.out.println( "Clamping color: " + colorString( color ) );
        for ( int i = 0; i < channels; i++ ) {
            if ( color[i] < min ) {
                color[i] = min;
            }
            if ( color[i] > max ) {
                color[i] = max;
            }
        }
        return color;
    }

    private static String colorString( int[] color ) {
        return color[0] + ", " + color[1] + ", " + color[2];
    }

    /**
     * Create a new Raster3 and transfer the raster's data into it
     */
    private static Raster3 internal( WritableRaster raster ) {
        Raster3 ret = new Raster3( raster.getWidth(), raster.getHeight(), new Point2d( 0, 0 ), 1 );
        for ( int x = 0; x < raster.getWidth(); x++ ) {
            for ( int y = 0; y < raster.getHeight(); y++ ) {
                double[] tmp = new double[4];
                raster.getPixel( x, y, tmp );
                ret.setPixelPoint( x, y, new Point3d( tmp ) );
            }
        }
        return ret;
    }

    private static void writeToRaster( Raster3 a, WritableRaster b ) {
        for ( int y = 0; y < a.getHeight(); y++ ) {
            for ( int x = 0; x < a.getWidth(); x++ ) {
                double[] tmp = a.getPixel( x, y );
                if ( b.getNumBands() == 4 ) {
                    int[] tmp4 = new int[4];
                    tmp4[0] = (int) tmp[0];
                    tmp4[1] = (int) tmp[1];
                    tmp4[2] = (int) tmp[2];
                    tmp4[3] = 255;
                    clampColor( tmp4, 4, 0, 255 );
                    b.setPixel( x, y, tmp4 );
                }
                else {
                    int[] tmp3 = new int[3];
                    tmp3[0] = (int) tmp[0];
                    tmp3[1] = (int) tmp[1];
                    tmp3[2] = (int) tmp[2];
                    clampColor( tmp3, 3, 0, 255 );
                    b.setPixel( x, y, tmp3 );
                }
            }
        }
    }
}
