package com.jonathanolson.graphics.dither;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

import javax.imageio.ImageIO;

/**
 * Test to see whether adding "images" to what at full-resolution looks grey is feasible
 */
public class GammaColorTable implements Serializable {

    private final int R0;
    private final double gamma;
    private final double invGamma;

    private final double[] intmap = new double[256];

    private final double[] cur = new double[4];
    private final int[] curint = new int[4];

    private final double ERR;
    private final double y;

    private final Map<Integer, Best> bestmap = new HashMap<Integer, Best>();
    private final Map<Pair, Best> valuemap = new HashMap<Pair, Best>();
    private final Map<Integer, Set<Integer>> possibleValuemap = new HashMap<Integer, Set<Integer>>();

    private int count = 0;

    public static GammaColorTable getTable( int R0, double gamma ) {
        return new GammaColorTable( R0, gamma );
//        String name = "gamma-color-table_" + R0 + "_" + gamma;
//        File tmpFile = new File( System.getProperty( "java.io.tmpdir" ), name );
//        GammaColorTable table = null;
//        if ( tmpFile.exists() ) {
//            FileInputStream fis = null;
//            ObjectInputStream in = null;
//            try {
//                fis = new FileInputStream( tmpFile );
//                in = new ObjectInputStream( fis );
//                table = (GammaColorTable) in.readObject();
//                in.close();
//            }
//            catch( IOException ex ) {
//                ex.printStackTrace();
//            }
//            catch( ClassNotFoundException ex ) {
//                ex.printStackTrace();
//            }
//        }
//        else {
//            table = new GammaColorTable( R0, gamma );
//
//            FileOutputStream fos = null;
//            ObjectOutputStream out = null;
//            try {
//                fos = new FileOutputStream( tmpFile );
//                out = new ObjectOutputStream( fos );
//                out.writeObject( table );
//                out.close();
//            }
//            catch( IOException ex ) {
//                ex.printStackTrace();
//            }
//        }
//        return table;
    }

    public GammaColorTable( int R0, double gamma ) {
        this.R0 = R0;
        this.gamma = gamma;
        this.invGamma = 1 / gamma;
        for ( int i = 0; i < 256; i++ ) {
            intmap[i] = Math.pow( ( (double) i ) / 255.0, this.gamma );
        }
        ERR = Math.abs( intmap[R0] - intmap[R0 - 1] );
        // -1: 1919300
        // -2: 1919300
        y = 4 * intmap[R0];

        iter( 4, y );
    }

    private double currentError() {
        return Math.sqrt( ( R0 - curint[0] ) * ( R0 - curint[0] ) + ( R0 - curint[1] ) * ( R0 - curint[1] ) + ( R0 - curint[2] ) * ( R0 - curint[2] ) + ( R0 - curint[3] ) * ( R0 - curint[3] ) );
    }

    private void check() {
        if ( (int) ( Math.pow( ( cur[0] + cur[1] + cur[2] + cur[3] ) / 4, invGamma ) * 255 ) == R0 ) {
            //System.out.println( cur[0] + "," + cur[1] + "," + cur[2] + "," + cur[3] );
            //System.out.println( ( curint[0] + curint[1] + curint[2] + curint[3] ) / 4 );
            int rl = ( curint[0] + curint[1] + curint[2] + curint[3] ) / 4;
            count++;
            Best best = bestmap.get( rl );
            if ( best == null ) {
                bestmap.put( rl, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
            }
            else {
                if ( currentError() < best.getError() ) {
                    bestmap.put( rl, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
                }
            }
            for ( int v : curint ) {
                Pair pair = new Pair( rl, v );
                best = valuemap.get( pair );
                if ( best == null ) {
                    valuemap.put( pair, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
                }
                else {
                    if ( currentError() < best.getError() ) {
                        valuemap.put( pair, new Best( currentError(), new int[]{curint[0], curint[1], curint[2], curint[3]} ) );
                    }

                }
                if ( possibleValuemap.get( rl ) == null ) {
                    HashSet<Integer> set = new HashSet<Integer>();
                    set.add( v );
                    possibleValuemap.put( rl, set );
                }
                else {
                    possibleValuemap.get( rl ).add( v );
                }
            }
        }
    }

    private void iter( int n, double yleft ) {
        if ( n == 0 ) {
            check();
            return;
        }
        if ( n == 3 ) {
            System.out.println( curint[0] );
        }
        if ( yleft < -ERR || yleft > ( (double) n ) + ERR ) {
            return;
        }
        int low = 0;
        if ( n < 4 ) {
            low = curint[3 - n];
        }
        for ( int i = low; i < 256; i++ ) {
            double entry = intmap[i];
            cur[4 - n] = entry;
            curint[4 - n] = i;
            iter( n - 1, yleft - entry );
        }
    }

    public Map<Integer, Best> getBestmap() {
        return bestmap;
    }

    public Map<Pair, Best> getValuemap() {
        return valuemap;
    }

    public Map<Integer, Set<Integer>> getPossibleValuemap() {
        return possibleValuemap;
    }

    public int getCount() {
        return count;
    }

    public static void main( String[] args ) {

        //GammaColorTable table = new GammaColorTable( 135, 2.2 );
        GammaColorTable table = getTable( 135, 2.2 );

        System.out.println( "count: " + table.getCount() );

        List<Integer> rls = new LinkedList<Integer>( table.getBestmap().keySet() );
        Collections.sort( rls );

        boolean self = true;

        for ( Integer rl : rls ) {
            Best best = table.getBestmap().get( rl );
            int[] x = best.getEntries();
            System.out.println( rl + " " + x[0] + "," + x[1] + "," + x[2] + "," + x[3] );

            Set<Integer> possibles = table.getPossibleValuemap().get( rl );

            List<Integer> options = new LinkedList<Integer>( possibles );
            Collections.sort( options );
            System.out.println( options.get( 0 ) + " to " + options.get( options.size() - 1 ) );
            System.out.println( "size: " + possibles.size() );

            if ( !options.contains( rl ) ) {
                self = false;
            }
        }

        // print low bounds
        for ( Integer rl : rls ) {
            List<Integer> options = new LinkedList<Integer>( table.getPossibleValuemap().get( rl ) );
            Collections.sort( options );

            System.out.print( "{" + rl + "," + options.get( 0 ) + "}" );
        }
        System.out.println();

        // print high bounds
        for ( Integer rl : rls ) {
            List<Integer> options = new LinkedList<Integer>( table.getPossibleValuemap().get( rl ) );
            Collections.sort( options );

            System.out.print( "{" + rl + "," + options.get( options.size() - 1 ) + "}," );
        }
        System.out.println();

        BufferedImage outImage = new BufferedImage( rls.size(), 256, BufferedImage.TYPE_INT_RGB );
        for ( Integer rl : rls ) {
            int x = rl - rls.get( 0 );
            for ( Integer v : table.getPossibleValuemap().get( rl ) ) {
                outImage.getRaster().setPixel( x, v, new int[]{255, 255, 255, 255} );
            }
        }
        try {
            ImageIO.write( outImage, "png", new File( "/tmp/out-gamut.png" ) );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }

        System.out.println( "self: " + self );

    }

    public static class Pair implements Serializable {
        private int rl;
        private int pixel;

        public Pair( int rl, int pixel ) {
            this.rl = rl;
            this.pixel = pixel;
        }

        public int getRl() {
            return rl;
        }

        public int getPixel() {
            return pixel;
        }
    }

    public static class Best implements Serializable {
        double error;
        int[] entries;

        public Best( double error, int[] entries ) {
            this.error = error;
            this.entries = entries;
        }

        public double getError() {
            return error;
        }

        public int[] getEntries() {
            return entries;
        }
    }
}