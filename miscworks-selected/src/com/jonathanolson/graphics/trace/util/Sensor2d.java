package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.util.math.vec.Point2d;

public interface Sensor2d {
    public void hit( Point2d location, double wavelength, double intensity );
}
