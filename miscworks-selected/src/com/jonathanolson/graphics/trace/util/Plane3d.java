package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Plane3d {

    private Vector3d normal;
    private double distance;

    public Plane3d( Vector3d normal, double distance ) {
        this.normal = normal;
        this.distance = distance;
    }

    public static class Hit implements RayHit {
        public double distance;
        public Point3d hitPoint;
        public Vector3d normal;

        public Hit( double distance, Point3d hitPoint, Vector3d normal ) {
            this.distance = distance;
            this.hitPoint = hitPoint;
            this.normal = normal;
        }

        public double getDistance() {
            return distance;
        }

        public Point3d getHitPoint() {
            return hitPoint;
        }

        public Vector3d getNormal() {
            return normal;
        }
    }

    // TODO: add tests
    public Hit intersect( Ray3d ray ) {
        double vx = ray.getDir().dot( normal );
        if ( Math.abs( vx ) < Numbers.EPSILON * 2 ) {
            return null;
        }
        double t = ( distance - ray.getPos().dot( normal ) ) / vx;
        if ( t < Numbers.EPSILON * 2 ) {
            return null;
        }
        if ( vx > 0 ) {
            Vector3d n = new Vector3d( normal );
            n.negate();
            return new Hit( t, ray.withDistance( t ), n );
        }
        else {
            return new Hit( t, ray.withDistance( t ), new Vector3d( normal ) );
        }
    }

    public Vector3d getNormal() {
        return normal;
    }

    public double getDistance() {
        return distance;
    }
}
