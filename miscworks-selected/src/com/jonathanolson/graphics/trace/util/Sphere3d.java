package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.graphics.trace.lens.LensMaterial;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Sphere3d {

    /**
     * Center point of the logical sphere
     */
    private Point3d center;

    /**
     * The radius of the logical sphere
     */
    private double radius;

    public Sphere3d( Point3d center, double radius ) {
        this.center = center;
        this.radius = radius;
    }

    public Hit intersect( Ray3d ray ) {
        Vector3d raydir = ray.getDir();
        Point3d pos = ray.getPos();
        Vector3d centerToRay = new Vector3d();
        centerToRay.sub( pos, center );
        double tmp = raydir.dot( centerToRay );
        double centerToRayDistSq = centerToRay.lengthSquared();
        double det = 4 * tmp * tmp - 4 * ( centerToRayDistSq - radius * radius );
        if ( det < Numbers.EPSILON ) {
            // ray misses sphere entirely
            return null;
        }
        double base = raydir.dot( center ) - raydir.dot( pos );
        double sqt = Math.sqrt( det ) / 2;
        double ta = base - sqt;
        double tb = base + sqt;

        if ( tb < Numbers.EPSILON ) {
            // sphere is behind ray
            return null;
        }

        Point3d pb = ray.withDistance( tb );
        Vector3d nb = new Vector3d();
        nb.sub( pb, center );
        nb.normalize();

        if ( ta < Numbers.EPSILON ) {
            // we are inside the sphere
            // in => out
            nb.negate();
            return new Hit( tb, pb, nb, false );
        }
        else {
            // two possible hits
            Point3d pa = ray.withDistance( ta );
            Vector3d na = new Vector3d();
            na.sub( pa, center );
            na.normalize();

            // close hit, we have out => in
            return new Hit( ta, pa, na, true );
        }
    }

    public double getIndexOfRefraction( LensMaterial material, double wavelength ) {
        if ( material == null ) {
            return 1;
        }
        else {
            return material.getDispersion().compute( wavelength );
        }
    }

    public Ray3d getTransmittedRay( Ray3d incomingRay, double nOut, double nIn ) {
        Hit hit = intersect( incomingRay );
        if ( hit == null ) {
            return null;
        }
        Ray3d ret = new Ray3d();
        ret.setPos( hit.hitPoint );
        Vector3d normal = new Vector3d( hit.normal );
        double na;
        double nb;
        if ( hit.fromOutside ) {
            na = nOut;
            nb = nIn;
        }
        else {
            na = nIn;
            nb = nOut;
        }
        if ( FresnelReflection.isTotalInternalReflection( incomingRay.getDir(), normal, na, nb ) ) {
            return null;
        }
        Vector3d outgoing = FresnelReflection.transmit( incomingRay.getDir(), normal, na, nb );

        ret.setDir( outgoing );
        return ret;
    }

    public Ray3d getFresnelRay( Ray3d incomingRay, double nOut, double nIn ) {
        Hit hit = intersect( incomingRay );
        if ( hit == null ) {
            return null;
        }
        double na;
        double nb;
        if ( hit.fromOutside ) {
            na = nOut;
            nb = nIn;
        }
        else {
            na = nIn;
            nb = nOut;
        }
        Vector3d dir = FresnelReflection.getFresnelDir( incomingRay.getDir(), hit.normal, na, nb );
        return new Ray3d( hit.hitPoint, dir );
    }

    public Point3d getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public static class Hit implements RayHit {
        public double distance;
        public Point3d hitPoint;
        public Vector3d normal;
        public boolean fromOutside;

        public Hit( double distance, Point3d hitPoint, Vector3d normal, boolean fromOutside ) {
            this.distance = distance;
            this.hitPoint = hitPoint;
            this.normal = normal;
            this.fromOutside = fromOutside;
        }

        public double getDistance() {
            return distance;
        }

        public Point3d getHitPoint() {
            return hitPoint;
        }

        public Vector3d getNormal() {
            return normal;
        }

        public boolean isFromOutside() {
            return fromOutside;
        }
    }
}