package com.jonathanolson.graphics.trace.util;

public abstract class RasterThread extends Thread {

    public abstract void render();

    @Override
    public void run() {
        while ( true ) {
            if ( Thread.interrupted() ) {
                return;
            }
            render();
        }
    }
}
