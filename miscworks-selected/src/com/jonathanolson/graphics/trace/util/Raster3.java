package com.jonathanolson.graphics.trace.util;

import java.io.Serializable;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;

public class Raster3 implements Serializable, Sensor2d {
    private double[][][] data;

    private int width;
    private int height;

    private Point2d center;
    private double scale;

    public Raster3( int width, int height, Point2d center, double scale ) {
        this.width = width;
        this.height = height;
        this.center = center;
        this.scale = scale;
        data = new double[height][width][3];
    }

    public synchronized void addXYZAt( Point2d location, double wavelength, double intensity ) {
        if ( isLocationInRaster( location ) ) {
            double[] xyz = XYZResponse.sample2deg( wavelength );
            int x = mapX( location.x );
            int y = mapY( location.y );
            data[y][x][0] += intensity * xyz[0];
            data[y][x][1] += intensity * xyz[1];
            data[y][x][2] += intensity * xyz[2];
        }
    }

    public void addAt( Point2d location, Point3d contribution ) {
        if ( isLocationInRaster( location ) ) {
            int x = mapX( location.x );
            int y = mapY( location.y );
            data[y][x][0] += contribution.x;
            data[y][x][1] += contribution.y;
            data[y][x][2] += contribution.z;
        }
    }

    public double[] getPixel( int x, int y ) {
        return data[y][x];
    }

    public Point3d getPixelPoint( int x, int y ) {
        return new Point3d( getPixel( x, y ) );
    }

    public void setPixelPoint( int x, int y, Point3d color ) {
        double[] part = data[y][x];
        part[0] = color.x;
        part[1] = color.y;
        part[2] = color.z;
    }

    // TODO: MASSIVE: fix for correct ratio and bounds

    public boolean isLocationInRaster( Point2d location ) {
        Point2d p = new Point2d( location );
        p.sub( center );
        return Math.abs( p.x ) < scale && Math.abs( p.y ) < scale;
    }

    public int mapX( double x ) {
        double dx = ( center.x - x ) / scale;
        double cx = ( dx + 1 ) / 2; // 0 to 1
        cx *= ( (double) width );
        int ret = (int) Math.floor( cx );
        if ( ret == width ) { ret = width - 1; }
        return ret;
    }

    public int mapY( double y ) {
        double dy = ( center.y - y ) / scale;
        double cy = ( dy + 1 ) / 2; // 0 to 1
        cy *= ( (double) height );
        int ret = (int) Math.floor( cy );
        if ( ret == height ) { ret = height - 1; }
        return ret;
    }

    public double reverseMapX( int x ) {
        double ret = x + 0.5;
        ret /= ( (double) width );
        ret = 2 * ret - 1;
        ret = center.x - ret * scale;
        return ret;
    }

    public double reverseMapY( int y ) {
        double ret = y + 0.5;
        ret /= ( (double) height );
        ret = 2 * ret - 1;
        ret = center.y - ret * scale;
        return ret;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public Point2d getCenter() {
        return center;
    }

    public double getScale() {
        return scale;
    }

    public double[][][] getData() {
        return data;
    }

    @Override
    public void hit( Point2d location, double wavelength, double intensity ) {
        addXYZAt( location, wavelength, intensity );
    }
}
