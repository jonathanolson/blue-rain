package com.jonathanolson.graphics.trace.util;

import java.io.Externalizable;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public interface RayTrail extends Externalizable {

    // TODO: add objects with IDs

    public void bounce( double distance, Point3d point, Vector3d dir, Vector3d normal, boolean reflected );

    public void absorb( double distance, Point3d point );

    public void image( double distance, Point3d point, Vector3d dir, Vector3d normal );

    public void miss();


    // TODO: subclass for a lens-aware ray? maybe have a component for handling that
    //public abstract void enterLens();
    //public abstract void exitLens();
    //public void aperture( double distance, Point3d point );

    // TODO: functions to modify the energy (added to bounce)

    public Ray3d getCurrentRay();

    public double getWavelength();

    public boolean isAlive();

//    protected void rayCheck( double distance, Point3d point ) {
//        assert ( currentRay.withDistance( distance ).epsilonEquals( point, Numbers.EPSILON ) );
//    }

}
