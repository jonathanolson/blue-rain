package com.jonathanolson.graphics.trace.util;

import java.awt.*;
import java.awt.color.ColorSpace;

public class ColorUtils {
    public static Color fromXYZ( double[] v ) {
        double[] r = new double[3];

        r[0] = v[0];
        r[1] = v[1];
        r[2] = v[2];

        // keep the color the same... we need to do more adaptation to handle other things...
        if ( r[0] > 1 ) {
            //r[1] /= r[0];
            //r[2] /= r[0];
            r[0] = 1;
        }
        if ( r[2] > 1 ) {
            //r[0] /= r[2];
            //r[1] /= r[2];
            r[2] = 1;
        }
        if ( r[1] > 1 ) {
            //r[0] /= r[1];
            //r[2] /= r[1];
            r[1] = 1;
        }
        if ( r[0] < 0 ) {
            r[0] = 0;
        }
        if ( r[1] < 0 ) {
            r[1] = 0;
        }
        if ( r[2] < 0 ) {
            r[2] = 0;
        }
        return fromXYZ( r[0], r[1], r[2] );
    }

    public static Color fromXYZ( double x, double y, double z ) {
        return fromXYZ( (float) x, (float) y, (float) z );
    }

    public static Color fromXYZ( float x, float y, float z ) {
        return new Color( ColorSpace.getInstance( ColorSpace.CS_CIEXYZ ), new float[]{x, y, z}, 1f );
    }

    public static Color fromXYZ( float[] v ) {
        return fromXYZ( new double[]{(double) v[0], (double) v[1], (double) v[2]} );
    }

    public static float[] toXYZ( Color c ) {
        return c.getColorSpace().toCIEXYZ( c.getColorComponents( null ) );
    }

    public static float[] chromaticity( Color c ) {
        float[] xyz = toXYZ( c );
        float total = xyz[0] + xyz[1] + xyz[2];
        return new float[]{xyz[0] / total, xyz[1] / total};
    }

    public static ColorSpace getsRGB() {
        return ColorSpace.getInstance( ColorSpace.CS_sRGB );
    }

    public static ColorSpace getXYZ() {
        return ColorSpace.getInstance( ColorSpace.CS_CIEXYZ );
    }
}
