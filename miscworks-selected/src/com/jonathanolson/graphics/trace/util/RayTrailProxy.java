package com.jonathanolson.graphics.trace.util;

import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class RayTrailProxy implements RayTrail {

    private RayTrail trail;
    private boolean activated = false;

    private double distance;
    private Point3d point;
    private Vector3d dir;
    private Vector3d normal;
    private boolean reflected;

    // TODO (mostly unimplemented)
    private boolean missed;

    public RayTrailProxy( RayTrail trail ) {
        this.trail = trail;
    }

    @Override
    public void bounce( double distance, Point3d point, Vector3d dir, Vector3d normal, boolean reflected ) {
    }

    @Override
    public void absorb( double distance, Point3d point ) {
    }

    @Override
    public void image( double distance, Point3d point, Vector3d dir, Vector3d normal ) {
    }

    @Override
    public void miss() {
    }

    @Override
    public Ray3d getCurrentRay() {
        return null;
    }

    @Override
    public double getWavelength() {
        return 0;
    }

    @Override
    public boolean isAlive() {
        return false;
    }

    @Override
    public void writeExternal( ObjectOutput objectOutput ) throws IOException {
        trail.writeExternal( objectOutput );
    }

    @Override
    public void readExternal( ObjectInput objectInput ) throws IOException, ClassNotFoundException {
        trail.readExternal( objectInput );
    }
}
