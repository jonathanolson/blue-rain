package com.jonathanolson.graphics.trace.util;

public class Numbers {
    public static final double EPSILON = 0.00000001;

    public static boolean zeroEpsilon( double x ) {
        return Math.abs( x ) < EPSILON;
    }

    public static boolean equalsEpsilon( double x, double y ) {
        return Math.abs( x - y ) < EPSILON;
    }

    public static boolean equalsEpsilon( double x, double y, double epsilon ) {
        return Math.abs( x - y ) < epsilon;
    }
}
