package com.jonathanolson.graphics.trace.util;

import java.util.ArrayList;
import java.util.List;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class HitList implements Hittable {
    private static List<Hittable> hittables = new ArrayList<Hittable>();

    public void addHittable( Hittable hittable ) {
        hittables.add( hittable );
    }

    @Override
    public RayHit intersect( Ray3d ray ) {
        double bestDistance = Double.POSITIVE_INFINITY;
        RayHit bestHit = null;
        int bestIndex = -1;
        for ( int idx = 0; idx < hittables.size(); idx++ ) {
            RayHit hit = hittables.get( idx ).intersect( ray );
            if ( hit != null && hit.getDistance() < bestDistance ) {
                bestDistance = hit.getDistance();
                bestHit = hit;
                bestIndex = idx;
            }
        }
        if ( bestHit == null ) {
            return null;
        }
        else {
            return new HitListHit( bestHit, bestIndex );
        }
    }

    @Override
    public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
        HitListHit hhit = (HitListHit) hit;
        return hittables.get( hhit.getIndex() ).process( oldRay, hhit.getChild(), wavelength );
    }

    public static class HitListHit implements RayHit {
        private RayHit child;
        private int index;

        public HitListHit( RayHit child, int index ) {
            this.child = child;
            this.index = index;
        }

        @Override
        public double getDistance() {
            return child.getDistance();
        }

        @Override
        public Point3d getHitPoint() {
            return child.getHitPoint();
        }

        @Override
        public Vector3d getNormal() {
            return child.getNormal();
        }

        public RayHit getChild() {
            return child;
        }

        public int getIndex() {
            return index;
        }
    }
}
