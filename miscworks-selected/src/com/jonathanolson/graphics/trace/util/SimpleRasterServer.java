package com.jonathanolson.graphics.trace.util;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.zip.GZIPOutputStream;

import com.jonathanolson.graphics.trace.test.Box5a;
import com.jonathanolson.util.math.vec.Point2d;

public class SimpleRasterServer {
    private Raster3 raster;

    public SimpleRasterServer( Raster3 raster ) throws IOException {
        this.raster = raster;

        ServerSocket srv = new ServerSocket( 7192 );
        while ( true ) {
            Socket s = srv.accept();
            ObjectOutputStream o = new ObjectOutputStream( new GZIPOutputStream( s.getOutputStream() ) );
            o.writeObject( raster );
            o.close();
            s.close();
        }
    }

    public static void main( String[] args ) {
        try {
            System.out.println( "Box5a (fixed)" );
            final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), 0.10 );

            int threads = 15;

            for ( int i = 0; i < threads; i++ ) {
                Thread t = new Box5a( raster );
                t.start();
            }

            SimpleRasterServer server = new SimpleRasterServer( raster );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }
}
