package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Vector3d;

public class FresnelReflection {

    public static Vector3d reflect( Vector3d incident, Vector3d normal ) {
        assert ( Math.abs( incident.length() - 1 ) < Numbers.EPSILON );
        assert ( Math.abs( normal.length() - 1 ) < Numbers.EPSILON );

        double dot = incident.dot( normal );
        if ( dot > -Numbers.EPSILON ) {
            // TODO: change to assert?
            throw new RuntimeException( "dot > -Numbers.EPSILON" );
        }
        Vector3d ret = new Vector3d( normal );
        ret.scale( 2 * dot );
        ret.sub( incident );
        ret.negate();

        assert ( Math.abs( ret.length() - 1 ) < Numbers.EPSILON );
        return ret;
    }

    public static Vector3d transmit( Vector3d incident, Vector3d normal, double na, double nb ) {
        assert ( Math.abs( incident.length() - 1 ) < Numbers.EPSILON );
        assert ( Math.abs( normal.length() - 1 ) < Numbers.EPSILON );

        // check for TIR
        assert ( !isTotalInternalReflection( incident, normal, na, nb ) );

        assert ( na >= 1 );
        assert ( nb >= 1 );

        double q = na / nb;
        double dot = incident.dot( normal );
        if ( dot > -Numbers.EPSILON ) {
            // TODO: change to assert?
            throw new RuntimeException( "dot > -Numbers.EPSILON" );
        }
        Vector3d t = new Vector3d( normal );
        Vector3d ret = new Vector3d( incident );
        t.scale( q * dot + ( Math.sqrt( 1 - q * q * ( 1 - dot * dot ) ) ) );
        ret.scale( q );
        ret.sub( t );

        assert ( Math.abs( ret.length() - 1 ) < Numbers.EPSILON );
        return ret;
    }

    public static boolean isTotalInternalReflection( Vector3d incident, Vector3d normal, double na, double nb ) {
        if ( na <= nb ) {
            return false;
        }
        double dot = -normal.dot( incident );

        assert ( dot > Numbers.EPSILON );

        double cosineTIRAngle = Math.sqrt( 1 - ( nb / na ) * ( nb / na ) );

        // be conservative about TIR, so that if within epsilon we report TIR is true
        if ( dot >= cosineTIRAngle + Numbers.EPSILON ) {
            return false;
        }
        else {
            return true;
        }
    }

    // TODO: implement transfer-matrix method for handling optical coatings!!

    public static double simpleFresnelDielectric( Vector3d incident, Vector3d normal, double na, double nb ) {
        double[] rr = fresnelDielectric( incident, normal, na, nb );
        return ( rr[0] + rr[1] ) / 2;
    }

    public static double[] fresnelDielectric( Vector3d incident, Vector3d normal, double na, double nb ) {
        return fresnelDielectric( incident, normal, transmit( incident, normal, na, nb ), na, nb );
    }

    /**
     * Reflectance (and thus transmission) for dielectric surfaces (IE glass)
     *
     * @param incident    Incident unit vector
     * @param normal      Normal unit vector
     * @param transmitted Transmitted unit vector
     * @param na          IOR from
     * @param nb          IOR to
     * @return An array consisting of S and P reflectance (in that order)
     */
    public static double[] fresnelDielectric( Vector3d incident, Vector3d normal, Vector3d transmitted, double na, double nb ) {
        double[] ret = new double[2];
        double doti = Math.abs( incident.dot( normal ) );
        double dott = Math.abs( transmitted.dot( normal ) );
        ret[0] = ( na * doti - nb * dott ) / ( na * doti + nb * dott );
        ret[0] *= ret[0];
        ret[1] = ( na * dott - nb * doti ) / ( na * dott + nb * doti );
        ret[1] *= ret[1];
        return ret;
    }

    /**
     * Reflectance (and thus transmission / absorption) for metalic surfaces
     *
     * @param incident Incident unit vector
     * @param normal   Normal unit vector
     * @param na       IOR from
     * @param nb       IOR to
     * @param k        I forget...
     * @return An array consisting of S and P reflectance (in that order)
     */
    public static double[] fresnel( Vector3d incident, Vector3d normal, double na, double nb, double k ) {
        double[] ret = new double[2];
        double doti = Math.abs( incident.dot( normal ) );
        double comm = na * na * ( doti * doti - 1 ) / ( ( nb * nb + k * k ) * ( nb * nb + k * k ) );
        double resq = 1.0 + comm * ( nb * nb - k * k );
        double imsq = 2.0 * comm * nb * k;
        double temdott = Math.sqrt( resq * resq + imsq * imsq );
        double redott = ( Math.sqrt( 2.0 ) / 2.0 ) * Math.sqrt( temdott + resq );
        double imdott = ( imsq >= 0.0 ? 1.0 : -1.0 ) * ( Math.sqrt( 2.0 ) / 2.0 ) * Math.sqrt( temdott - resq );
        double renpdott = nb * redott + k * imdott;
        double imnpdott = nb * imdott - k * redott;
        double retop = na * doti - renpdott;
        double rebot = na * doti + renpdott;
        double retdet = rebot * rebot + imnpdott * imnpdott;
        double reret = ( retop * rebot + -imnpdott * imnpdott ) / retdet;
        double imret = ( -imnpdott * rebot - retop * imnpdott ) / retdet;
        ret[0] = reret * reret + imret * imret;
        retop = ( nb * nb - k * k ) * doti - na * renpdott;
        rebot = ( nb * nb - k * k ) * doti + na * renpdott;
        double imtop = -2.0 * nb * k * doti - na * imnpdott;
        double imbot = -2.0 * nb * k * doti + na * imnpdott;
        retdet = rebot * rebot + imbot * imbot;
        reret = ( retop * rebot + imtop * imbot ) / retdet;
        imret = ( imtop * rebot - retop * imbot ) / retdet;
        ret[1] = reret * reret + imret * imret;
        return ret;
    }

    public static void main( String[] args ) {
        Vector3d normal = new Vector3d( 0, 0, 1 );
        double na = 2;
        double nb = 1;
        double q = nb / na;
        // vector at the critical angle. should be normalized
        Vector3d a = new Vector3d( q - 0.01, 0, -Math.sqrt( 1 - q * q ) );
        a.normalize();
        double[] ar = fresnelDielectric( a, normal, na, nb );
        System.out.println( ar[0] + " " + ar[1] );
        ar = fresnel( a, normal, na, nb, 0 );
        System.out.println( ar[0] + " " + ar[1] );
    }

    public static Vector3d getFresnelDir( Vector3d incident, Vector3d normal, double na, double nb ) {
        double reflect = 0;
        Vector3d transmitDir = null;
        if ( FresnelReflection.isTotalInternalReflection( incident, normal, na, nb ) ) {
            reflect = 1;
        }
        else {
            transmitDir = FresnelReflection.transmit( incident, normal, na, nb );
            double[] rr = FresnelReflection.fresnelDielectric( incident, normal, transmitDir, na, nb );
            reflect = ( rr[0] + rr[1] ) * 0.5;
        }
        if ( RandomUtils.random() > reflect ) {
            // refract
            return transmitDir;
        }
        else {
            // reflect
            return FresnelReflection.reflect( incident, normal );
        }
    }

}
