package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Ray3d {
    private Point3d pos;
    private Vector3d dir;

    public Ray3d() {
    }

    public Ray3d( Point3d pos, Vector3d dir ) {
        this.pos = pos;
        this.dir = dir;
        assert ( Numbers.equalsEpsilon( 1, dir.lengthSquared() ) );
    }

    public Ray3d( Point3d pos, Vector3d dir, boolean normalize ) {
        this.pos = pos;
        this.dir = dir;
        if ( normalize ) {
            dir.normalize();
        }
        assert ( normalize || Numbers.equalsEpsilon( 1, dir.lengthSquared() ) );
    }

    public Point3d getPos() {
        return pos;
    }

    public void setPos( Point3d pos ) {
        this.pos = pos;
    }

    public Vector3d getDir() {
        return dir;
    }

    public void setDir( Vector3d dir ) {
        this.dir = dir;
    }

    public Point3d withDistance( double distance ) {
        Point3d ret = new Point3d();
        ret.scale( distance, dir );
        ret.add( pos );
        return ret;
    }

    @Override
    public String toString() {
        return pos + " => " + dir;
    }
}
