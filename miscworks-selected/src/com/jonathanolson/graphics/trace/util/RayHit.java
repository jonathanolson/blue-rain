package com.jonathanolson.graphics.trace.util;

import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public interface RayHit {
    public double getDistance();

    public Point3d getHitPoint();

    public Vector3d getNormal();
}
