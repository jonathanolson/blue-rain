package com.jonathanolson.graphics.trace.util;

public class SellmeierDispersion {

    private double b1, b2, b3, c1, c2, c3;

    public SellmeierDispersion( double b1, double b2, double b3, double c1, double c2, double c3 ) {
        this.b1 = b1;
        this.b2 = b2;
        this.b3 = b3;
        this.c1 = c1;
        this.c2 = c2;
        this.c3 = c3;
    }

    public double compute( double wavelength ) {
        return compute( b1, b2, b3, c1, c2, c3, wavelength );
    }

    public static double compute( double bx, double by, double bz, double cx, double cy, double cz, double wavelength ) {
        double lams = wavelength * wavelength / 1000000.0;
        return Math.sqrt( 1.0 + ( bx * lams ) / ( lams - cx ) + ( by * lams ) / ( lams - cy ) + ( bz * lams ) / ( lams - cz ) );
    }

    // TODO: get lens designs from http://books.google.com/books?id=A7c4DoEAbkwC&pg=PA13&lpg=PA13&dq=double+gauss+bk7&source=bl&ots=NzkjGoZ2Qt&sig=NKJBFGDPNDORL8au7cvbtTrqNN8&hl=en&ei=TFHxSqazN4uEswOP-bn4AQ&sa=X&oi=book_result&ct=result&resnum=1&ved=0CA4Q6AEwAA#v=onepage&q=double%20gauss%20bk7&f=false

    // more dispersions at http://www.schott.com/advanced_optics/german/download/inquiryglassdatasheetsv090609.xls

    // more glass parameter links: http://refractiveindex.info/dispersion.php?group=SCHOTT&material=N-LAF2&wavelength=0.5876

    // equivalences at http://www.hebo-glass.com/en/o_assortment

    // double gauss lens http://www.sinopt.com/software1/usrguide54/examples/dblgauss.htm

    // could pull the design from patent 6122111

    // another double gauss:
    /*
radius of curvature (mm), diameter (mm), separation or thickness (mm), material
85.500,76.0,11.60,LaF2
408.330,76.0,1.50,Air
40.350,66.0,17.00,SK55
156.050,66.0,3.50,FN11
25.050,44.0,13.70,Air
Stop,42.6,8.30,Air
-36.800,44.0,3.50,SF8
55.000,52.0,23.00,LaF2
-51.500,52.0,1.00,Air
123.500,51.0,17.00,LaF2
-204.960,51.0,55.07,Air

for-sure lenses:
100fov
170fov (need to down glass spec N_SK5, N_PSK3)
hologon
petzval1
(petzval2 n-lak33(a) discrepancy?)
telephoto2 (oooh, all glasses, plus focusing movement?)

     */


    // Schott glasses (misc)
    public static final SellmeierDispersion BK7 = new SellmeierDispersion( 1.03961212, 0.231792344, 1.01046945, 0.00600069867, 0.0200179144, 103.560653 );
    public static final SellmeierDispersion CaF_2 = new SellmeierDispersion( 0.5675888, 0.4710914, 3.8484723, 0.00252642999, 0.0100783328, 1200.55597 );
    public static final SellmeierDispersion FusedSilica = new SellmeierDispersion( 0.6961663, 0.4079426, 0.8974794, 0.00467914826, 0.0135120631, 97.9340025 );
    public static final SellmeierDispersion SF15 = new SellmeierDispersion( 1.53925927, 0.247620926, 1.03816409, 0.0119307961, 0.0556077536, 116.416747 );


    // schott catalog
    public static final SellmeierDispersion F2 = new SellmeierDispersion( 1.34533359, 0.209073176, 0.937357162, 0.00997743871, 0.0470450767, 111.886764 );
    public static final SellmeierDispersion F2HT = new SellmeierDispersion( 1.34533359, 0.209073176, 0.937357162, 0.00997743871, 0.0470450767, 111.886764 );
    public static final SellmeierDispersion F5 = new SellmeierDispersion( 1.3104463, 0.19603426, 0.96612977, 0.00958633048, 0.0457627627, 115.011883 );
    public static final SellmeierDispersion K10 = new SellmeierDispersion( 1.15687082, 0.0642625444, 0.872376139, 0.00809424251, 0.0386051284, 104.74773 );
    public static final SellmeierDispersion K7 = new SellmeierDispersion( 1.1273555, 0.124412303, 0.827100531, 0.00720341707, 0.0269835916, 100.384588 );
    public static final SellmeierDispersion KZFS12 = new SellmeierDispersion( 1.55624873, 0.239769276, 0.947887658, 0.0102012744, 0.0469277969, 69.8370722 );
    public static final SellmeierDispersion KZFSN5 = new SellmeierDispersion( 1.47727858, 0.191686941, 0.897333608, 0.00975488335, 0.0450495404, 67.8786495 );
    public static final SellmeierDispersion LAFN7 = new SellmeierDispersion( 1.66842615, 0.298512803, 1.0774376, 0.0103159999, 0.0469216348, 82.5078509 );
    public static final SellmeierDispersion LF5 = new SellmeierDispersion( 1.28035628, 0.163505973, 0.893930112, 0.00929854416, 0.0449135769, 110.493685 );
    public static final SellmeierDispersion LLF1 = new SellmeierDispersion( 1.21640125, 0.13366454, 0.883399468, 0.00857807248, 0.0420143003, 107.59306 );
    public static final SellmeierDispersion N_BAF10 = new SellmeierDispersion( 1.5851495, 0.143559385, 1.08521269, 0.00926681282, 0.0424489805, 105.613573 );
    public static final SellmeierDispersion N_BAF4 = new SellmeierDispersion( 1.42056328, 0.102721269, 1.14380976, 0.00942015382, 0.0531087291, 110.278856 );
    public static final SellmeierDispersion N_BAF51 = new SellmeierDispersion( 1.51503623, 0.153621958, 1.15427909, 0.00942734715, 0.04308265, 124.889868 );
    public static final SellmeierDispersion N_BAF52 = new SellmeierDispersion( 1.43903433, 0.0967046052, 1.09875818, 0.00907800128, 0.050821208, 105.691856 );
    public static final SellmeierDispersion N_BAK1 = new SellmeierDispersion( 1.12365662, 0.309276848, 0.881511957, 0.00644742752, 0.0222284402, 107.297751 );
    public static final SellmeierDispersion N_BAK2 = new SellmeierDispersion( 1.01662154, 0.319903051, 0.937232995, 0.00592383763, 0.0203828415, 113.118417 );
    public static final SellmeierDispersion N_BAK4 = new SellmeierDispersion( 1.28834642, 0.132817724, 0.945395373, 0.00779980626, 0.0315631177, 105.965875 );
    public static final SellmeierDispersion N_BALF4 = new SellmeierDispersion( 1.31004128, 0.142038259, 0.964929351, 0.0079659645, 0.0330672072, 109.19732 );
    public static final SellmeierDispersion N_BALF5 = new SellmeierDispersion( 1.28385965, 0.0719300942, 1.05048927, 0.00825815975, 0.0441920027, 107.097324 );
    public static final SellmeierDispersion N_BASF2 = new SellmeierDispersion( 1.53652081, 0.156971102, 1.30196815, 0.0108435729, 0.0562278762, 131.3397 );
    public static final SellmeierDispersion N_BASF64 = new SellmeierDispersion( 1.65554268, 0.17131977, 1.33664448, 0.0104485644, 0.0499394756, 118.961472 );
    public static final SellmeierDispersion N_BK10 = new SellmeierDispersion( 0.888308131, 0.328964475, 0.984610769, 0.00516900822, 0.0161190045, 99.7575331 );
    public static final SellmeierDispersion N_BK7 = new SellmeierDispersion( 1.03961212, 0.231792344, 1.01046945, 0.00600069867, 0.0200179144, 103.560653 );
    public static final SellmeierDispersion N_F2 = new SellmeierDispersion( 1.39757037, 0.159201403, 1.2686543, 0.00995906143, 0.0546931752, 119.248346 );
    public static final SellmeierDispersion N_FK5 = new SellmeierDispersion( 0.844309338, 0.344147824, 0.910790213, 0.00475111955, 0.0149814849, 97.8600293 );
    public static final SellmeierDispersion N_FK51A = new SellmeierDispersion( 0.971247817, 0.216901417, 0.904651666, 0.00472301995, 0.0153575612, 168.68133 );
    public static final SellmeierDispersion N_K5 = new SellmeierDispersion( 1.08511833, 0.199562005, 0.930511663, 0.00661099503, 0.024110866, 111.982777 );
    public static final SellmeierDispersion N_KF9 = new SellmeierDispersion( 1.19286778, 0.0893346571, 0.920819805, 0.00839154696, 0.0404010786, 112.572446 );
    public static final SellmeierDispersion N_KZFS11 = new SellmeierDispersion( 1.3322245, 0.28924161, 1.15161734, 0.0084029848, 0.034423972, 88.4310532 );
    public static final SellmeierDispersion N_KZFS2 = new SellmeierDispersion( 1.23697554, 0.153569376, 0.903976272, 0.00747170505, 0.0308053556, 70.1731084 );
    public static final SellmeierDispersion N_KZFS4 = new SellmeierDispersion( 1.35055424, 0.197575506, 1.09962992, 0.0087628207, 0.0371767201, 90.3866994 );
    public static final SellmeierDispersion N_KZFS5 = new SellmeierDispersion( 1.47460789, 0.193584488, 1.26589974, 0.00986143816, 0.0445477583, 106.436258 );
    public static final SellmeierDispersion N_KZFS8 = new SellmeierDispersion( 1.62693651, 0.24369876, 1.62007141, 0.010880863, 0.0494207753, 131.009163 );
    public static final SellmeierDispersion N_LAF2 = new SellmeierDispersion( 1.80984227, 0.15729555, 1.0930037, 0.0101711622, 0.0442431765, 100.687748 );
    public static final SellmeierDispersion N_LAF21 = new SellmeierDispersion( 1.87134529, 0.25078301, 1.22048639, 0.0093332228, 0.0345637762, 83.2404866 );
    public static final SellmeierDispersion N_LAF33 = new SellmeierDispersion( 1.79653417, 0.311577903, 1.15981863, 0.00927313493, 0.0358201181, 87.3448712 );
    public static final SellmeierDispersion N_LAF34 = new SellmeierDispersion( 1.75836958, 0.313537785, 1.18925231, 0.00872810026, 0.0293020832, 85.1780644 );
    public static final SellmeierDispersion N_LAF35 = new SellmeierDispersion( 1.51697436, 0.455875464, 1.07469242, 0.00750943203, 0.0260046715, 80.5945159 );
    public static final SellmeierDispersion N_LAF36 = new SellmeierDispersion( 1.85744228, 0.294098729, 1.16615417, 0.00982397191, 0.0384309138, 89.3984634 );
    public static final SellmeierDispersion N_LAF7 = new SellmeierDispersion( 1.74028764, 0.226710554, 1.32525548, 0.010792558, 0.0538626639, 106.268665 );
    public static final SellmeierDispersion N_LAK10 = new SellmeierDispersion( 1.72878017, 0.169257825, 1.19386956, 0.00886014635, 0.0363416509, 82.9009069 );
    public static final SellmeierDispersion N_LAK12 = new SellmeierDispersion( 1.17365704, 0.588992398, 0.978014394, 0.00577031797, 0.0200401678, 95.4873482 );
    public static final SellmeierDispersion N_LAK14 = new SellmeierDispersion( 1.50781212, 0.318866829, 1.14287213, 0.00746098727, 0.0242024834, 80.9565165 );
    public static final SellmeierDispersion N_LAK21 = new SellmeierDispersion( 1.22718116, 0.420783743, 1.01284843, 0.00602075682, 0.0196862889, 88.4370099 );
    public static final SellmeierDispersion N_LAK22 = new SellmeierDispersion( 1.14229781, 0.535138441, 1.04088385, 0.00585778594, 0.0198546147, 100.834017 );
    public static final SellmeierDispersion N_LAK33A = new SellmeierDispersion( 1.44116999, 0.571749501, 1.16605226, 0.00680933877, 0.0222291824, 80.9379555 );
    public static final SellmeierDispersion N_LAK34 = new SellmeierDispersion( 1.26661442, 0.665919318, 1.1249612, 0.00589278062, 0.0197509041, 78.8894174 );
    public static final SellmeierDispersion N_LAK7 = new SellmeierDispersion( 1.23679889, 0.445051837, 1.01745888, 0.00610105538, 0.0201388334, 90.638038 );
    public static final SellmeierDispersion N_LAK8 = new SellmeierDispersion( 1.33183167, 0.546623206, 1.19084015, 0.00620023871, 0.0216465439, 82.5827736 );
    public static final SellmeierDispersion N_LAK9 = new SellmeierDispersion( 1.46231905, 0.344399589, 1.15508372, 0.00724270156, 0.0243353131, 85.4686868 );
    public static final SellmeierDispersion N_LASF31A = new SellmeierDispersion( 1.96485075, 0.475231259, 1.48360109, 0.00982060155, 0.0344713438, 110.739863 );
    public static final SellmeierDispersion N_LASF40 = new SellmeierDispersion( 1.98550331, 0.274057042, 1.28945661, 0.010958331, 0.0474551603, 96.9085286 );
    public static final SellmeierDispersion N_LASF41 = new SellmeierDispersion( 1.86348331, 0.413307255, 1.35784815, 0.00910368219, 0.0339247268, 93.3580595 );
    public static final SellmeierDispersion N_LASF43 = new SellmeierDispersion( 1.93502827, 0.23662935, 1.26291344, 0.0104001413, 0.0447505292, 87.437569 );
    public static final SellmeierDispersion N_LASF44 = new SellmeierDispersion( 1.78897105, 0.38675867, 1.30506243, 0.00872506277, 0.0308085023, 92.7743824 );
    public static final SellmeierDispersion N_LASF45 = new SellmeierDispersion( 1.87140198, 0.267777879, 1.73030008, 0.011217192, 0.0505134972, 147.106505 );
    public static final SellmeierDispersion N_LASF46A = new SellmeierDispersion( 2.16701566, 0.319812761, 1.66004486, 0.0123595524, 0.0560610282, 107.047718 );
    public static final SellmeierDispersion N_LASF9 = new SellmeierDispersion( 2.00029547, 0.298926886, 1.80691843, 0.0121426017, 0.0538736236, 156.530829 );
    public static final SellmeierDispersion N_PK51 = new SellmeierDispersion( 1.15610775, 0.153229344, 0.785618966, 0.00585597402, 0.0194072416, 140.537046 );
    public static final SellmeierDispersion N_PK52A = new SellmeierDispersion( 1.029607, 0.1880506, 0.736488165, 0.00516800155, 0.0166658798, 138.964129 );
    public static final SellmeierDispersion N_PSK3 = new SellmeierDispersion( 0.88727211, 0.489592425, 1.04865296, 0.00469824067, 0.0161818463, 104.374975 );
    public static final SellmeierDispersion N_PSK53A = new SellmeierDispersion( 1.38121836, 0.196745645, 0.886089205, 0.00706416337, 0.0233251345, 97.4847345 );
    public static final SellmeierDispersion N_SF1 = new SellmeierDispersion( 1.60865158, 0.237725916, 1.51530653, 0.0119654879, 0.0590589722, 135.521676 );
    public static final SellmeierDispersion N_SF10 = new SellmeierDispersion( 1.62153902, 0.256287842, 1.64447552, 0.0122241457, 0.0595736775, 147.468793 );
    public static final SellmeierDispersion N_SF11 = new SellmeierDispersion( 1.73759695, 0.313747346, 1.89878101, 0.013188707, 0.0623068142, 155.23629 );
    public static final SellmeierDispersion N_SF14 = new SellmeierDispersion( 1.69022361, 0.288870052, 1.7045187, 0.0130512113, 0.061369188, 149.517689 );
    public static final SellmeierDispersion N_SF15 = new SellmeierDispersion( 1.57055634, 0.218987094, 1.50824017, 0.0116507014, 0.0597856897, 132.709339 );
    public static final SellmeierDispersion N_SF2 = new SellmeierDispersion( 1.47343127, 0.163681849, 1.36920899, 0.0109019098, 0.0585683687, 127.404933 );
    public static final SellmeierDispersion N_SF4 = new SellmeierDispersion( 1.67780282, 0.282849893, 1.63539276, 0.012679345, 0.0602038419, 145.760496 );
    public static final SellmeierDispersion N_SF5 = new SellmeierDispersion( 1.52481889, 0.187085527, 1.42729015, 0.011254756, 0.0588995392, 129.141675 );
    public static final SellmeierDispersion N_SF57 = new SellmeierDispersion( 1.87543831, 0.37375749, 2.30001797, 0.0141749518, 0.0640509927, 177.389795 );
    public static final SellmeierDispersion N_SF57HT = new SellmeierDispersion( 1.87543831, 0.37375749, 2.30001797, 0.0141749518, 0.0640509927, 177.389795 );
    public static final SellmeierDispersion N_SF6 = new SellmeierDispersion( 1.77931763, 0.338149866, 2.08734474, 0.0133714182, 0.0617533621, 174.01759 );
    public static final SellmeierDispersion N_SF66 = new SellmeierDispersion( 2.0245976, 0.470187196, 2.59970433, 0.0147053225, 0.0692998276, 161.817601 );
    public static final SellmeierDispersion N_SF6HT = new SellmeierDispersion( 1.77931763, 0.338149866, 2.08734474, 0.0133714182, 0.0617533621, 174.01759 );
    public static final SellmeierDispersion N_SF8 = new SellmeierDispersion( 1.55075812, 0.209816918, 1.46205491, 0.0114338344, 0.0582725652, 133.24165 );
    public static final SellmeierDispersion N_SK11 = new SellmeierDispersion( 1.17963631, 0.229817295, 0.935789652, 0.00680282081, 0.0219737205, 101.513232 );
    public static final SellmeierDispersion N_SK14 = new SellmeierDispersion( 0.936155374, 0.594052018, 1.04374583, 0.00461716525, 0.016885927, 103.736265 );
    public static final SellmeierDispersion N_SK16 = new SellmeierDispersion( 1.34317774, 0.241144399, 0.994317969, 0.00704687339, 0.0229005, 92.7508526 );
    public static final SellmeierDispersion N_SK2 = new SellmeierDispersion( 1.28189012, 0.257738258, 0.96818604, 0.0072719164, 0.0242823527, 110.377773 );
    public static final SellmeierDispersion N_SK4 = new SellmeierDispersion( 1.32993741, 0.228542996, 0.988465211, 0.00716874107, 0.0246455892, 100.886364 );
    public static final SellmeierDispersion N_SK5 = new SellmeierDispersion( 0.991463823, 0.495982121, 0.987393925, 0.00522730467, 0.0172733646, 98.3594579 );
    public static final SellmeierDispersion N_SSK2 = new SellmeierDispersion( 1.4306027, 0.153150554, 1.01390904, 0.00823982975, 0.0333736841, 106.870822 );
    public static final SellmeierDispersion N_SSK5 = new SellmeierDispersion( 1.59222659, 0.103520774, 1.05174016, 0.00920284626, 0.0423530072, 106.927374 );
    public static final SellmeierDispersion N_SSK8 = new SellmeierDispersion( 1.44857867, 0.117965926, 1.06937528, 0.00869310149, 0.0421566593, 111.300666 );
    public static final SellmeierDispersion N_ZK7 = new SellmeierDispersion( 1.07715032, 0.168079109, 0.851889892, 0.00676601657, 0.0230642817, 89.0498778 );
    public static final SellmeierDispersion P_LASF47 = new SellmeierDispersion( 1.85543101, 0.315854649, 1.28561839, 0.0100328203, 0.0387095168, 94.5421507 );
    public static final SellmeierDispersion P_PK53 = new SellmeierDispersion( 0.960316767, 0.340437227, 0.777865595, 0.00531032986, 0.0175073434, 106.87533 );
    public static final SellmeierDispersion P_SF67 = new SellmeierDispersion( 1.97464225, 0.467095921, 2.43154209, 0.0145772324, 0.0669790359, 157.444895 );
    public static final SellmeierDispersion P_SF8 = new SellmeierDispersion( 1.55370411, 0.206332561, 1.39708831, 0.011658267, 0.0582087757, 130.748028 );
    public static final SellmeierDispersion P_SK57 = new SellmeierDispersion( 1.31053414, 0.169376189, 1.10987714, 0.00740877235, 0.0254563489, 107.751087 );
    public static final SellmeierDispersion SF1 = new SellmeierDispersion( 1.55912923, 0.284246288, 0.968842926, 0.0121481001, 0.0534549042, 112.174809 );
    public static final SellmeierDispersion SF10 = new SellmeierDispersion( 1.61625977, 0.259229334, 1.07762317, 0.0127534559, 0.0581983954, 116.60768 );
    public static final SellmeierDispersion SF2 = new SellmeierDispersion( 1.40301821, 0.231767504, 0.939056586, 0.0105795466, 0.0493226978, 112.405955 );
    public static final SellmeierDispersion SF4 = new SellmeierDispersion( 1.61957826, 0.339493189, 1.02566931, 0.0125502104, 0.0544559822, 117.652222 );
    public static final SellmeierDispersion SF5 = new SellmeierDispersion( 1.46141885, 0.247713019, 0.949995832, 0.0111826126, 0.0508594669, 112.041888 );
    public static final SellmeierDispersion SF56A = new SellmeierDispersion( 1.70579259, 0.344223052, 1.09601828, 0.0133874699, 0.0579561608, 121.616024 );
    public static final SellmeierDispersion SF57 = new SellmeierDispersion( 1.81651371, 0.428893641, 1.07186278, 0.0143704198, 0.0592801172, 121.419942 );
    public static final SellmeierDispersion SF57HHT = new SellmeierDispersion( 1.81651371, 0.428893641, 1.07186278, 0.0143704198, 0.0592801172, 121.419942 );
    public static final SellmeierDispersion SF6 = new SellmeierDispersion( 1.72448482, 0.390104889, 1.04572858, 0.0134871947, 0.0569318095, 118.557185 );
    public static final SellmeierDispersion SF6HT = new SellmeierDispersion( 1.72448482, 0.390104889, 1.04572858, 0.0134871947, 0.0569318095, 118.557185 );
    public static final SellmeierDispersion LITHOTEC_CAF2 = new SellmeierDispersion( 0.617617011, 0.421117656, 3.79711183, 0.00275381936, 0.0105900875, 1182.67444 );
    public static final SellmeierDispersion LITHOSIL_Q = new SellmeierDispersion( 0.67071081, 0.433322857, 0.877379057, 0.00449192312, 0.0132812976, 95.8899878 );


    // ohara
    public static final SellmeierDispersion S_BAH10 = new SellmeierDispersion( 1.59034337E0, 1.38464579E-1, 1.21988043E0, 9.3273434E-3, 4.27498654E-2, 1.19251777E2 );
    public static final SellmeierDispersion S_BAH11 = new SellmeierDispersion( 1.5713886E0, 1.47869313E-1, 1.28092846E0, 9.10807936E-3, 4.02401684E-2, 1.30399367E2 );
    public static final SellmeierDispersion S_BAH27 = new SellmeierDispersion( 1.68939052E0, 1.33081013E-1, 1.41165515E0, 1.03598193E-2, 5.33982239E-2, 1.26515503E2 );
    public static final SellmeierDispersion S_BAH28 = new SellmeierDispersion( 1.69493484E0, 1.92890298E-1, 1.56385948E0, 1.0272319E-2, 5.2118764E-2, 1.37818035E2 );
    public static final SellmeierDispersion S_BAH32 = new SellmeierDispersion( 1.5802363E0, 1.37504632E-1, 1.60603298E0, 1.03578062E-2, 5.48393088E-2, 1.47982885E2 );
    public static final SellmeierDispersion S_BAL2 = new SellmeierDispersion( 1.30923813E0, 1.14137353E-1, 1.17882259E0, 8.38873953E-3, 3.99436485E-2, 1.40257892E2 );
    public static final SellmeierDispersion S_BAL3 = new SellmeierDispersion( 1.2936689E0, 1.32440252E-1, 1.10197293E0, 8.00367962E-3, 3.54711196E-2, 1.34517431E2 );
    public static final SellmeierDispersion S_BAL11 = new SellmeierDispersion( 8.21314256E-1, 6.12586478E-1, 1.24859637E0, 3.51436131E-3, 1.79762375E-2, 1.3345667E2 );
    public static final SellmeierDispersion S_BAL12 = new SellmeierDispersion( 7.14605258E-1, 6.21993289E-1, 1.22537681E0, 3.01763913E-3, 1.6650545E-2, 1.43506314E2 );
    public static final SellmeierDispersion S_BAL14 = new SellmeierDispersion( 1.27553696E0, 1.46083393E-1, 1.16754699E0, 7.49692359E-3, 3.1042153E-2, 1.28947092E2 );
    public static final SellmeierDispersion S_BAL35 = new SellmeierDispersion( 9.41357273E-1, 5.46174895E-1, 1.16168917E0, 1.40333996E-2, 9.06635683E-4, 1.14163758E2 );
    public static final SellmeierDispersion S_BAL41 = new SellmeierDispersion( 1.243442E0, 1.66301104E-1, 1.10586114E0, 1.16396708E-2, -8.90464938E-3, 1.1411122E2 );
    public static final SellmeierDispersion S_BAL42 = new SellmeierDispersion( 1.39570615E0, 7.1850507E-2, 1.27129267E0, 1.12218843E-2, -2.52117422E-2, 1.3449786E2 );
    public static final SellmeierDispersion S_BAM3 = new SellmeierDispersion( 1.36955358E0, 8.53825867E-2, 1.16159771E0, 9.41331434E-3, 5.04359027E-2, 1.30548899E2 );
    public static final SellmeierDispersion S_BAM4 = new SellmeierDispersion( 1.41059317E0, 1.11201306E-1, 1.34148939E0, 9.63312192E-3, 4.9877821E-2, 1.52237696E2 );
    public static final SellmeierDispersion S_BAM12 = new SellmeierDispersion( 1.50161605E0, 1.26987445E-1, 1.43544052E0, 9.40761826E-3, 4.72602195E-2, 1.41666499E2 );
    public static final SellmeierDispersion S_BSL7 = new SellmeierDispersion( 1.1515019E0, 1.18583612E-1, 1.26301359E0, 1.0598413E-2, -1.1822519E-2, 1.29617662E2 );
    public static final SellmeierDispersion S_BSM2 = new SellmeierDispersion( 8.67168676E-1, 6.72848343E-1, 1.18456107E0, 3.69311003E-3, 1.81652804E-2, 1.32376147E2 );
    public static final SellmeierDispersion S_BSM4 = new SellmeierDispersion( 9.6244308E-1, 5.95939234E-1, 1.10558352E0, 4.68062141E-3, 1.78772082E-2, 1.15896432E2 );
    public static final SellmeierDispersion S_BSM9 = new SellmeierDispersion( 1.37020077E0, 1.89397267E-1, 1.24202324E0, 7.57631457E-3, 3.00787515E-2, 1.31350111E2 );
    public static final SellmeierDispersion S_BSM10 = new SellmeierDispersion( 9.45443081E-1, 6.43237376E-1, 1.17752968E0, 1.57263798E-2, 1.61924066E-3, 1.21361748E2 );
    public static final SellmeierDispersion S_BSM14 = new SellmeierDispersion( 1.2828627E0, 2.47647429E-1, 1.10383999E0, 1.22902399E-2, -6.13142361E-3, 1.06883378E2 );
    public static final SellmeierDispersion S_BSM15 = new SellmeierDispersion( 9.53128328E-1, 6.37613977E-1, 1.65245647E0, 3.87638985E-3, 1.85094632E-2, 1.59442367E2 );
    public static final SellmeierDispersion S_BSM16 = new SellmeierDispersion( 1.14490383E0, 4.39563911E-1, 1.27688079E0, 1.37034916E-2, -1.86514205E-3, 1.19535585E2 );
    public static final SellmeierDispersion S_BSM18 = new SellmeierDispersion( 9.27886025E-1, 7.08858526E-1, 1.18610897E0, 4.17549199E-3, 1.84691838E-2, 1.22210416E2 );
    public static final SellmeierDispersion S_BSM22 = new SellmeierDispersion( 1.44305741E0, 1.40786358E-1, 1.26093951E0, 8.1920891E-3, 3.56911455E-2, 1.31959337E2 );
    public static final SellmeierDispersion S_BSM25 = new SellmeierDispersion( 1.34814257E0, 3.47530319E-1, 1.38798368E0, 6.95364366E-3, 2.77863478E-2, 1.42138122E2 );
    public static final SellmeierDispersion S_BSM28 = new SellmeierDispersion( 1.43822841E0, 1.28100017E-1, 1.3435553E0, 8.5977975E-3, 4.08617854E-2, 1.4370989E2 );
    public static final SellmeierDispersion S_BSM71 = new SellmeierDispersion( 1.50847885E0, 1.58099826E-1, 1.36815368E0, 8.12769076E-3, 3.54200898E-2, 1.36110038E2 );
    public static final SellmeierDispersion S_BSM81 = new SellmeierDispersion( 9.96356844E-1, 6.51392837E-1, 1.22432622E0, 1.44821587E-2, 1.54826389E-3, 8.99818604E1 );
    public static final SellmeierDispersion S_FPL51 = new SellmeierDispersion( 1.17010505E0, 4.75710783E-2, 7.63832445E-1, 6.16203924E-3, 2.63372876E-2, 1.41882642E2 );
    public static final SellmeierDispersion S_FPL51Y = new SellmeierDispersion( 1.14031443E0, 7.71496272E-2, 1.43721957E0, 5.95466872E-3, 2.23953953E-2, 2.74290057E2 );
    public static final SellmeierDispersion S_FPL53 = new SellmeierDispersion( 9.83532327E-1, 6.9568814E-2, 1.11409238E0, 4.92234955E-3, 1.93581091E-2, 2.64275294E2 );
    public static final SellmeierDispersion S_FSL5 = new SellmeierDispersion( 1.17447043E0, 1.40056154E-2, 1.19272435E0, 8.41855181E-3, -5.81790767E-2, 1.29599726E2 );
    public static final SellmeierDispersion S_FSL5Y = new SellmeierDispersion( 9.77409944E-1, 2.10950834E-1, 1.37142848E0, 5.57649364E-3, 1.77000313E-2, 1.49211443E2 );
    public static final SellmeierDispersion S_FTM16 = new SellmeierDispersion( 1.32940907E0, 1.41512125E-1, 1.44299068E0, 1.02377287E-2, 5.78081956E-2, 1.50597139E2 );
    public static final SellmeierDispersion S_LAH51 = new SellmeierDispersion( 1.82586991E0, 2.83023349E-1, 1.35964319E0, 9.35297152E-3, 3.73803057E-2, 1.00655798E2 );
    public static final SellmeierDispersion S_LAH52 = new SellmeierDispersion( 1.85390925E0, 2.97925555E-1, 1.39382086E0, 9.55320687E-3, 3.9381685E-2, 1.02706848E2 );
    public static final SellmeierDispersion S_LAH53 = new SellmeierDispersion( 1.91811619E0, 2.53724399E-1, 1.39473885E0, 1.02147684E-2, 4.33176011E-2, 1.01938021E2 );
    public static final SellmeierDispersion S_LAH55 = new SellmeierDispersion( 1.95615766E0, 3.19216215E-1, 1.39173189E0, 9.79338965E-3, 3.76836296E-2, 9.48775271E1 );
    public static final SellmeierDispersion S_LAH58 = new SellmeierDispersion( 1.78764964E0, 6.526356E-1, 1.79914564E0, 8.47378536E-3, 3.13126408E-2, 1.32788001E2 );
    public static final SellmeierDispersion S_LAH59 = new SellmeierDispersion( 1.51372967E0, 7.02462343E-1, 1.33600982E0, 7.05246901E-3, 2.49488689E-2, 1.00085908E2 );
    public static final SellmeierDispersion S_LAH60 = new SellmeierDispersion( 1.95243469E0, 3.0710021E-1, 1.56578094E0, 1.06442437E-2, 4.56735302E-2, 1.1028141E2 );
    public static final SellmeierDispersion S_LAH63 = new SellmeierDispersion( 1.89458276E0, 2.68702978E-1, 1.45705526E0, 1.02277048E-2, 4.42801243E-2, 1.04874927E2 );
    public static final SellmeierDispersion S_LAH64 = new SellmeierDispersion( 1.83021453E0, 2.9156359E-1, 1.28544024E0, 9.0482329E-3, 3.30756689E-2, 8.93675501E1 );
    public static final SellmeierDispersion S_LAH65 = new SellmeierDispersion( 1.68191258E0, 4.93779818E-1, 1.45682822E0, 7.7668425E-3, 2.88916181E-2, 9.92574356E1 );
    public static final SellmeierDispersion S_LAH66 = new SellmeierDispersion( 1.39280586E0, 6.79577094E-1, 1.38702069E0, 6.08475118E-3, 2.33925351E-2, 9.58354094E1 );
    public static final SellmeierDispersion S_LAH71 = new SellmeierDispersion( 1.98280031E0, 3.1675845E-1, 2.44472646E0, 1.18987459E-2, 5.27156001E-2, 2.13220697E2 );
    public static final SellmeierDispersion S_LAH79 = new SellmeierDispersion( 2.32557148E0, 5.07967133E-1, 2.43087198E0, 1.32895208E-2, 5.28335449E-2, 1.61122408E2 );
    public static final SellmeierDispersion S_LAL7 = new SellmeierDispersion( 9.16121247E-1, 7.65948319E-1, 1.27745023E0, 3.95889743E-3, 1.67547425E-2, 1.10762706E2 );
    public static final SellmeierDispersion S_LAL8 = new SellmeierDispersion( 1.30663291E0, 5.71377253E-1, 1.24303605E0, 6.11862448E-3, 2.1272147E-2, 9.06285686E1 );
    public static final SellmeierDispersion S_LAL9 = new SellmeierDispersion( 1.16195687E0, 6.44860099E-1, 1.25062221E0, 1.59659509E-2, 5.05502467E-4, 9.38284169E1 );
    public static final SellmeierDispersion S_LAL10 = new SellmeierDispersion( 1.52812575E0, 3.67965267E-1, 1.11751784E0, 7.76817644E-3, 2.72026548E-2, 8.886974E1 );
    public static final SellmeierDispersion S_LAL12 = new SellmeierDispersion( 9.92053895E-1, 7.71377731E-1, 1.18296264E0, 1.67095063E-2, 2.36750156E-3, 1.0590108E2 );
    public static final SellmeierDispersion S_LAL13 = new SellmeierDispersion( 9.80071267E-1, 8.32904776E-1, 1.28111995E0, 3.89123698E-3, 1.89164592E-2, 9.89052676E1 );
    public static final SellmeierDispersion S_LAL14 = new SellmeierDispersion( 1.2372097E0, 5.89722623E-1, 1.3192188E0, 1.5355132E-2, -3.0789625E-4, 9.37202947E1 );
    public static final SellmeierDispersion S_LAL18 = new SellmeierDispersion( 1.50276318E0, 4.30224497E-1, 1.3472606E0, 1.45462356E-2, -3.32784153E-3, 9.33508342E1 );
    public static final SellmeierDispersion S_LAL54 = new SellmeierDispersion( 1.41910189E0, 2.58416881E-1, 1.07385537E0, 7.26647428E-3, 2.63842499E-2, 1.02555463E2 );
    public static final SellmeierDispersion S_LAL56 = new SellmeierDispersion( 1.54052E0, 2.17748704E-1, 1.30456122E0, 8.26765101E-3, 3.28533726E-2, 1.24527479E2 );
    public static final SellmeierDispersion S_LAL58 = new SellmeierDispersion( 1.06368789E0, 7.44939067E-1, 1.59178942E0, 1.8519964E-2, 1.16295862E-3, 1.56636025E2 );
    public static final SellmeierDispersion S_LAL59 = new SellmeierDispersion( 1.13962742E0, 8.05227838E-1, 1.29488061E0, 4.93294862E-3, 2.0247996E-2, 9.34746507E1 );
    public static final SellmeierDispersion S_LAL61 = new SellmeierDispersion( 1.11073292E0, 8.59347773E-1, 1.26707433E0, 4.64181248E-3, 1.92989261E-2, 8.73917698E1 );
    public static final SellmeierDispersion S_LAM2 = new SellmeierDispersion( 1.7713E0, 1.9581423E-1, 1.19487834E0, 9.76652444E-3, 4.12718628E-2, 1.10458122E2 );
    public static final SellmeierDispersion S_LAM3 = new SellmeierDispersion( 1.64258713E0, 2.3963461E-1, 1.22483026E0, 8.6824602E-3, 3.51226242E-2, 1.16604369E2 );
    public static final SellmeierDispersion S_LAM7 = new SellmeierDispersion( 1.71014712E0, 2.56943292E-1, 1.63986271E0, 1.0516108E-2, 5.02809636E-2, 1.46181217E2 );
    public static final SellmeierDispersion S_LAM51 = new SellmeierDispersion( 1.638472E0, 1.88330533E-1, 1.47502357E0, 9.04853452E-3, 3.72740173E-2, 1.3777005E2 );
    public static final SellmeierDispersion S_LAM52 = new SellmeierDispersion( 1.73442942E0, 1.5155391E-1, 1.46225433E0, 1.00690928E-2, 4.70634701E-2, 1.40084396E2 );
    public static final SellmeierDispersion S_LAM54 = new SellmeierDispersion( 1.84213306E0, 1.75468631E-1, 1.25750878E0, 9.4399322E-3, 3.95281122E-2, 8.65463013E1 );
    public static final SellmeierDispersion S_LAM55 = new SellmeierDispersion( 1.85412979E0, 1.65450323E-1, 1.27255422E0, 1.08438152E-2, 5.1405098E-2, 1.09986837E2 );
    public static final SellmeierDispersion S_LAM58 = new SellmeierDispersion( 1.70984856E0, 1.73342897E-1, 1.64833565E0, 1.00852127E-2, 4.70890831E-2, 1.5746852E2 );
    public static final SellmeierDispersion S_LAM59 = new SellmeierDispersion( 1.63056133E0, 1.86994897E-1, 1.30014289E0, 8.99690705E-3, 3.68011993E-2, 1.22239544E2 );
    public static final SellmeierDispersion S_LAM60 = new SellmeierDispersion( 1.60673056E0, 3.6641564E-1, 1.31761804E0, 7.7504614E-3, 2.89967611E-2, 9.30720709E1 );
    public static final SellmeierDispersion S_LAM61 = new SellmeierDispersion( 1.7388333E0, 1.5093743E-1, 1.12118445E0, 9.80244105E-3, 4.33179685E-2, 1.01214625E2 );
    public static final SellmeierDispersion S_LAM66 = new SellmeierDispersion( 1.92094221E0, 2.19901208E-1, 1.72705231E0, 1.15075241E-2, 5.47993543E-2, 1.20133674E2 );
    public static final SellmeierDispersion S_NBH5 = new SellmeierDispersion( 1.47544521E0, 1.93060095E-1, 1.5093901E0, 9.5583674E-3, 4.60430483E-2, 1.26422746E2 );
    public static final SellmeierDispersion S_NBH8 = new SellmeierDispersion( 1.61344136E0, 2.57295888E-1, 1.98364455E0, 1.06386752E-2, 4.87071624E-2, 1.59784404E2 );
    public static final SellmeierDispersion S_NBH51 = new SellmeierDispersion( 1.71203689E0, 2.55989588E-1, 1.81456998E0, 1.07724134E-2, 4.88593504E-2, 1.36359013E2 );
    public static final SellmeierDispersion S_NBH52 = new SellmeierDispersion( 1.50305799E0, 2.21715926E-1, 1.84496391E0, 9.99021738E-3, 4.50327698E-2, 1.63722302E2 );
    public static final SellmeierDispersion S_NBH53 = new SellmeierDispersion( 1.6582834E0, 2.63275666E-1, 2.10142759E0, 1.13872516E-2, 5.22108137E-2, 1.65523649E2 );
    public static final SellmeierDispersion S_NBH55 = new SellmeierDispersion( 1.83145156E0, 2.87818024E-1, 2.152083E0, 1.22443139E-2, 5.7387731E-2, 1.86099124E2 );
    public static final SellmeierDispersion S_NBM51 = new SellmeierDispersion( 1.37023101E0, 1.77665568E-1, 1.30515471E0, 8.71920342E-3, 4.05725552E-2, 1.12703058E2 );
    public static final SellmeierDispersion S_NPH1 = new SellmeierDispersion( 1.75156623E0, 3.64006304E-1, 2.47874141E0, 1.35004681E-2, 6.68245147E-2, 1.70756006E2 );
    public static final SellmeierDispersion S_NPH2 = new SellmeierDispersion( 2.0386951E0, 4.37269641E-1, 2.96711461E0, 1.70796224E-2, 7.49254813E-2, 1.74155354E2 );
    public static final SellmeierDispersion S_NPH53 = new SellmeierDispersion( 1.85484904E0, 3.96194484E-1, 2.43512461E0, 1.34621486E-2, 6.31945361E-2, 1.70864886E2 );
    public static final SellmeierDispersion S_NSL3 = new SellmeierDispersion( 8.82514764E-1, 3.89271907E-1, 1.10693448E0, 4.64504582E-3, 2.00551397E-2, 1.36234339E2 );
    public static final SellmeierDispersion S_NSL5 = new SellmeierDispersion( 1.04574577E0, 2.39613026E-1, 1.1590685E0, 5.8523228E-3, 2.36858752E-2, 1.31329061E2 );
    public static final SellmeierDispersion S_NSL36 = new SellmeierDispersion( 1.09666153E0, 1.68990073E-1, 1.20580827E0, 6.67491123E-3, 3.3609545E-2, 1.41668738E2 );
    public static final SellmeierDispersion S_PHM52 = new SellmeierDispersion( 1.0996655E0, 4.78125422E-1, 1.13214074E0, 1.32718559E-2, -6.01649685E-4, 1.30595472E2 );
    public static final SellmeierDispersion S_PHM53 = new SellmeierDispersion( 1.09775423E0, 4.34816432E-1, 1.13894976E0, 1.233694E-2, -3.72522903E-4, 1.24276984E2 );
    public static final SellmeierDispersion S_TIH1 = new SellmeierDispersion( 1.60326759E0, 2.42980935E-1, 1.81313592E0, 1.18019139E-2, 5.91363658E-2, 1.61218747E2 );
    public static final SellmeierDispersion S_TIH3 = new SellmeierDispersion( 1.64797648E0, 2.67261917E-1, 2.19772845E0, 1.21917693E-2, 5.97893039E-2, 1.9215834E2 );
    public static final SellmeierDispersion S_TIH4 = new SellmeierDispersion( 1.66755531E0, 2.94411865E-1, 2.49422119E0, 1.22052137E-2, 5.97775329E-2, 2.14869618E2 );
    public static final SellmeierDispersion S_TIH6 = new SellmeierDispersion( 1.77227611E0, 3.4569125E-1, 2.40788501E0, 1.31182633E-2, 6.14479619E-2, 2.00753254E2 );
    public static final SellmeierDispersion S_TIH10 = new SellmeierDispersion( 1.61549392E0, 2.62433239E-1, 2.09426189E0, 1.19830897E-2, 5.9651024E-2, 1.81657554E2 );
    public static final SellmeierDispersion S_TIH11 = new SellmeierDispersion( 1.72677471E0, 3.24568628E-1, 2.65816809E0, 1.29369958E-2, 6.18255245E-2, 2.21904637E2 );
    public static final SellmeierDispersion S_TIH13 = new SellmeierDispersion( 1.62224674E0, 2.93844589E-1, 1.99225164E0, 1.18368386E-2, 5.90208025E-2, 1.71959976E2 );
    public static final SellmeierDispersion S_TIH14 = new SellmeierDispersion( 1.68915108E0, 2.90462024E-1, 2.37971516E0, 1.28202514E-2, 6.18090841E-2, 2.01094352E2 );
    public static final SellmeierDispersion S_TIH18 = new SellmeierDispersion( 1.59921608E0, 2.59532164E-1, 2.12454543E0, 1.16469304E-2, 5.84824883E-2, 1.86927779E2 );
    public static final SellmeierDispersion S_TIH23 = new SellmeierDispersion( 1.73986485E0, 3.13894918E-1, 2.31093206E0, 1.294413E-2, 6.12116868E-2, 1.97420482E2 );
    public static final SellmeierDispersion S_TIH53 = new SellmeierDispersion( 1.87904886E0, 3.69719775E-1, 2.33730863E0, 1.4412177E-2, 6.3881799E-2, 1.8266818E2 );
    public static final SellmeierDispersion S_TIL1 = new SellmeierDispersion( 1.25088944E0, 9.97973327E-2, 1.20583504E0, 8.83921279E-3, 4.82685052E-2, 1.37414953E2 );
    public static final SellmeierDispersion S_TIL2 = new SellmeierDispersion( 1.23401499E0, 9.59796833E-2, 1.20503991E0, 8.69507801E-3, 4.65611429E-2, 1.37953301E2 );
    public static final SellmeierDispersion S_TIL6 = new SellmeierDispersion( 1.17701777E0, 1.2795803E-1, 1.34740124E0, 7.71087686E-3, 4.11325328E-2, 1.54531692E2 );
    public static final SellmeierDispersion S_TIL25 = new SellmeierDispersion( 1.32122534E0, 1.23824976E-1, 1.43685254E0, 9.52091436E-3, 5.16062665E-2, 1.49064883E2 );
    public static final SellmeierDispersion S_TIL26 = new SellmeierDispersion( 1.31066488E0, 9.41903094E-2, 1.23292644E0, 9.68897812E-3, 5.27763106E-2, 1.33296422E2 );
    public static final SellmeierDispersion S_TIL27 = new SellmeierDispersion( 1.31433154E0, 1.12300168E-1, 1.413901E0, 9.50404477E-3, 5.24112772E-2, 1.48429972E2 );
    public static final SellmeierDispersion S_TIM1 = new SellmeierDispersion( 1.4496383E0, 1.22986408E-1, 1.38066723E0, 1.12094282E-2, 5.9626577E-2, 1.38178326E2 );
    public static final SellmeierDispersion S_TIM2 = new SellmeierDispersion( 1.42193846E0, 1.33827968E-1, 1.45060574E0, 1.07291511E-2, 5.72587546E-2, 1.45381805E2 );
    public static final SellmeierDispersion S_TIM3 = new SellmeierDispersion( 1.40691144E0, 1.28369745E-1, 1.51826191E0, 1.05633641E-2, 5.68483105E-2, 1.52107924E2 );
    public static final SellmeierDispersion S_TIM5 = new SellmeierDispersion( 1.38531342E0, 1.22372945E-1, 1.40508326E0, 1.04074567E-2, 5.57440088E-2, 1.44878733E2 );
    public static final SellmeierDispersion S_TIM8 = new SellmeierDispersion( 1.37262713E0, 1.12636276E-1, 1.39786421E0, 1.03220068E-2, 5.50195044E-2, 1.47735609E2 );
    public static final SellmeierDispersion S_TIM22 = new SellmeierDispersion( 1.44222294E0, 1.94432265E-1, 1.74092482E0, 1.04249404E-2, 5.50235257E-2, 1.69710769E2 );
    public static final SellmeierDispersion S_TIM25 = new SellmeierDispersion( 1.50659233E0, 2.04786135E-1, 1.92036668E0, 1.09501562E-2, 5.74980285E-2, 1.78128535E2 );
    public static final SellmeierDispersion S_TIM27 = new SellmeierDispersion( 1.4168047E0, 1.96785057E-1, 1.68001322E0, 1.00732158E-2, 5.37616908E-2, 1.64672436E2 );
    public static final SellmeierDispersion S_TIM28 = new SellmeierDispersion( 1.5427081E0, 2.17113891E-1, 1.81904459E0, 1.13925005E-2, 5.79224572E-2, 1.67697189E2 );
    public static final SellmeierDispersion S_TIM35 = new SellmeierDispersion( 1.55849775E0, 2.30767007E-1, 1.84436099E0, 1.15367235E-2, 5.86095947E-2, 1.62981888E2 );
    public static final SellmeierDispersion S_TIM39 = new SellmeierDispersion( 1.47008105E0, 2.24752746E-1, 2.44968592E0, 1.02900432E-2, 5.41276904E-2, 2.3743494E2 );
    public static final SellmeierDispersion S_YGH51 = new SellmeierDispersion( 1.0828017E0, 9.33988681E-1, 1.32367286E0, 1.8115636E-2, 3.04157575E-3, 9.10353195E1 );
    public static final SellmeierDispersion BAL15Y = new SellmeierDispersion( 1.28348331E0, 1.02800765E-1, 4.04609885E-1, 7.90900515E-3, 3.05971274E-2, 4.65268356E1 );
    public static final SellmeierDispersion BAL35Y = new SellmeierDispersion( 1.26231429E0, 2.2515421E-1, 6.39119345E-1, 6.95586355E-3, 2.21310699E-2, 6.31662736E1 );
    public static final SellmeierDispersion BSL7Y = new SellmeierDispersion( 1.13329383E0, 1.36897201E-1, 7.03456004E-1, 6.69407868E-3, 2.3739176E-2, 7.07030316E1 );
    public static final SellmeierDispersion BSM51Y = new SellmeierDispersion( 1.22393171E0, 3.06482383E-1, 8.23950901E-1, 6.49521083E-3, 2.08194161E-2, 7.95168951E1 );
    public static final SellmeierDispersion L_BAL35 = new SellmeierDispersion( 1.1626263E0, 3.25661051E-1, 1.35132486E0, 1.25957437E-2, -3.2691105E-3, 1.19214596E2 );
    public static final SellmeierDispersion L_BAL42 = new SellmeierDispersion( 1.39528097E0, 7.2551952E-2, 1.66335848E0, 1.1186203E-2, -2.46748575E-2, 1.67717958E2 );
    public static final SellmeierDispersion L_BBH1 = new SellmeierDispersion( 2.56949414E0, 5.24386192E-1, 1.40160566E0, 2.05613974E-2, 8.3348454E-2, 1.19733343E2 );
    public static final SellmeierDispersion L_BSL7 = new SellmeierDispersion( 9.17473918E-1, 3.52687665E-1, 1.05579788E0, 5.27701411E-3, 1.70809497E-2, 1.04302583E2 );
    public static final SellmeierDispersion L_LAH53 = new SellmeierDispersion( 1.90781372E0, 2.6350013E-1, 1.28144614E0, 1.03413285E-2, 4.19041155E-2, 9.57068567E1 );
    public static final SellmeierDispersion L_LAH81 = new SellmeierDispersion( 1.89927344E0, 2.70978866E-1, 1.33163819E0, 1.02901828E-2, 4.24227173E-2, 1.00967566E2 );
    public static final SellmeierDispersion L_LAH83 = new SellmeierDispersion( 1.82331579E0, 5.48625885E-1, 1.63182855E0, 9.11468875E-3, 3.2841967E-2, 1.23611174E2 );
    public static final SellmeierDispersion L_LAH84 = new SellmeierDispersion( 1.86267109E0, 3.15564131E-1, 1.30716934E0, 1.01627115E-2, 3.94096655E-2, 1.03774464E2 );
    public static final SellmeierDispersion L_LAH85 = new SellmeierDispersion( 1.93889988E0, 3.9787919E-1, 1.49937713E0, 9.96381644E-3, 3.6975114E-2, 1.07951467E2 );
    public static final SellmeierDispersion L_LAH86 = new SellmeierDispersion( 2.11102709E0, 3.70368094E-1, 1.62687484E0, 1.20714135E-2, 5.32464416E-2, 1.09166396E2 );
    public static final SellmeierDispersion L_LAH87 = new SellmeierDispersion( 1.4888027E0, 5.70635082E-1, 1.16467761E0, 7.66106412E-3, 2.50852548E-2, 1.01411992E2 );
    public static final SellmeierDispersion L_LAL12 = new SellmeierDispersion( 1.28516283E0, 4.78333797E-1, 1.21605301E0, 6.41062082E-3, 2.14815099E-2, 1.00243378E2 );
    public static final SellmeierDispersion L_LAL13 = new SellmeierDispersion( 1.17776146E0, 6.34591345E-1, 1.20435649E0, 5.57618243E-3, 2.06821469E-2, 9.96322776E1 );
    public static final SellmeierDispersion L_LAL67 = new SellmeierDispersion( 1.10974374E0, 6.26230534E-1, 1.20552031E0, 5.31546965E-3, 1.92438085E-2, 9.49177322E1 );
    public static final SellmeierDispersion L_LAM60 = new SellmeierDispersion( 1.47574184E0, 4.96132743E-1, 1.23796236E0, 7.3695E-3, 2.51891746E-2, 9.80306651E1 );
    public static final SellmeierDispersion L_LAM69 = new SellmeierDispersion( 1.7403896E0, 1.76996917E-1, 1.76775413E0, 1.0339887E-2, 4.84822765E-2, 1.36671996E2 );
    public static final SellmeierDispersion L_LAM72 = new SellmeierDispersion( 1.50483297E0, 4.33346414E-1, 1.2714921E0, 7.5034233E-3, 2.6900952E-2, 9.57631272E1 );
    public static final SellmeierDispersion L_NBH54 = new SellmeierDispersion( 2.00722652E0, 4.42086773E-1, 3.37287426E0, 1.35705124E-2, 5.91808873E-2, 2.19832665E2 );
    public static final SellmeierDispersion L_PHL1 = new SellmeierDispersion( 1.07570798E0, 3.35020347E-1, 8.10997558E-1, 5.91654042E-3, 2.03432769E-2, 1.06182158E2 );
    public static final SellmeierDispersion L_PHL2 = new SellmeierDispersion( 1.08137176E0, 3.1325766E-1, 8.79192863E-1, 5.94210177E-3, 1.98011567E-2, 1.09893817E2 );
    public static final SellmeierDispersion L_TIH53 = new SellmeierDispersion( 1.83464643E0, 4.15079602E-1, 2.82563492E0, 1.34992001E-2, 6.18608854E-2, 2.0162917E2 );
    public static final SellmeierDispersion L_TIM28 = new SellmeierDispersion( 1.58039099E0, 1.78294323E-1, 1.14876204E0, 1.25270147E-2, 6.02807505E-2, 1.16215055E2 );
    public static final SellmeierDispersion PBL1Y = new SellmeierDispersion( 1.24772961E0, 1.01954909E-1, 3.50479619E-1, 9.26606623E-3, 4.51754311E-2, 4.50186705E1 );
    public static final SellmeierDispersion PBL6Y = new SellmeierDispersion( 1.22310794E0, 8.11217929E-2, 3.21400939E-1, 8.97805333E-3, 4.45756957E-2, 4.05962247E1 );
    public static final SellmeierDispersion PBL25Y = new SellmeierDispersion( 1.31960626E0, 1.23752633E-1, 2.10055351E-1, 1.01863415E-2, 4.83593508E-2, 2.73272029E1 );
    public static final SellmeierDispersion PBL26Y = new SellmeierDispersion( 1.29471773E0, 1.08880981E-1, 2.20322964E-1, 9.86579479E-3, 4.77568828E-2, 2.88509863E1 );
    public static final SellmeierDispersion PBM2Y = new SellmeierDispersion( 1.39446503E0, 1.59230985E-1, 2.45470216E-1, 1.10571872E-2, 5.07194882E-2, 3.14440142E1 );
    public static final SellmeierDispersion PBM8Y = new SellmeierDispersion( 1.35351322E0, 1.30212912E-1, 1.58337266E-1, 1.05624626E-2, 4.96606652E-2, 2.07965806E1 );
    public static final SellmeierDispersion PBM18Y = new SellmeierDispersion( 1.34660215E0, 1.36322343E-1, 1.83371587E-1, 1.06313733E-2, 4.91403013E-2, 2.39154655E1 );

    // schott inquiry glass
    public static final SellmeierDispersion FK3 = new SellmeierDispersion( 0.973346627, 0.146642231, 0.679304225, 0.00640795469, 0.020565293, 80.4965389 );
    public static final SellmeierDispersion N_SK10 = new SellmeierDispersion( 1.34972093, 0.238587973, 0.9667336, 0.00736272269, 0.0253765327, 103.502909 );
    public static final SellmeierDispersion N_SK15 = new SellmeierDispersion( 1.30417786, 0.28584116, 0.974781572, 0.00695051276, 0.0232023703, 99.016884 );
    public static final SellmeierDispersion N_BAF3 = new SellmeierDispersion( 1.34859634, 0.10764424, 1.13207084, 0.00871492932, 0.0478406436, 112.936116 );
    public static final SellmeierDispersion BAFN6 = new SellmeierDispersion( 1.36719201, 0.10907994, 1.02108011, 0.00882820704, 0.0438731646, 113.58602 );
    public static final SellmeierDispersion N_LAF3 = new SellmeierDispersion( 1.73155854, 0.150874455, 1.06586596, 0.00953833914, 0.0407887211, 98.0758545 );
    public static final SellmeierDispersion SFL57 = new SellmeierDispersion( 1.88742326, 0.360534025, 2.26189313, 0.0145939341, 0.0648198946, 176.062211 );
    public static final SellmeierDispersion SFL6 = new SellmeierDispersion( 1.78922056, 0.328427448, 2.01639441, 0.0135163537, 0.0622729599, 168.014713 );
    public static final SellmeierDispersion SF11 = new SellmeierDispersion( 1.73848403, 0.311168974, 1.17490871, 0.0136068604, 0.0615960463, 121.922711 );
    public static final SellmeierDispersion N_SF19 = new SellmeierDispersion( 1.52005444, 0.17573947, 1.43623424, 0.01096144, 0.0593248486, 126.795151 );
    public static final SellmeierDispersion N_SF56 = new SellmeierDispersion( 1.73562085, 0.317487012, 1.95398203, 0.0129624742, 0.0612884288, 161.559441 );
    public static final SellmeierDispersion LASF35 = new SellmeierDispersion( 2.45505861, 0.453006077, 2.3851308, 0.0135670404, 0.054580302, 167.904715 );
    public static final SellmeierDispersion N_SF64 = new SellmeierDispersion( 1.59163762, 0.219908428, 1.46929315, 0.0118623434, 0.0594585499, 133.310762 );
    public static final SellmeierDispersion N_PSK53 = new SellmeierDispersion( 1.3434087, 0.241417935, 0.952896897, 0.00675074317, 0.0219910513, 103.551457 );
    public static final SellmeierDispersion BK7G18 = new SellmeierDispersion( 1.26538542, 0.0144191073, 1.00323028, 0.00813104078, 0.0543303226, 102.821166 );
    public static final SellmeierDispersion LF5G19 = new SellmeierDispersion( 1.34611327, 0.142428018, 0.900477176, 0.0097174385, 0.0501911619, 111.959703 );
    public static final SellmeierDispersion LF5G15 = new SellmeierDispersion( 1.28887331, 0.162818811, 10.5579792, 0.0092001566, 0.0456954308, 1275.44015 );
    public static final SellmeierDispersion K5G20 = new SellmeierDispersion( 1.14094396, 0.14500119, 37.4705786, 0.00694945478, 0.0310574444, 4536.25624 );
    public static final SellmeierDispersion LAK9G15 = new SellmeierDispersion( 1.28773667, 0.518244853, 26.1756109, 0.0055754192, 0.0223679524, 1892.2533 );
    public static final SellmeierDispersion F2G12 = new SellmeierDispersion( 1.34702224, 0.210037763, 19.5350768, 0.00980850553, 0.0471788018, 2279.1547 );
    public static final SellmeierDispersion SF6G05 = new SellmeierDispersion( 1.62113942, 0.506586092, 10.4032298, 0.0113478992, 0.0535840223, 1118.83658 );

    /* Common glass dispersions

   from http://cvilaser.com/Common/PDFs/Dispersion_Equations.pdf


    Also Laurent series equation is:
       n^2 = A0 + A1 lambda^2 + A2 / lambda^2 + A3 / lambda^4 + A4 / lambda^6 + A5 / lambda^8
       A0-A5:
       Crystal Quartz he 2.3849 -0.01259 0.01079 0.00016518 -0.00000194741 0.0000000936476
       Crystal Quartz ho 2.35728 -0.0117 0.01054 0.000134143 -0.000000445368 0.0000000592362
       S-LAM60M 2.958448 -0.01542036 0.02030973 0.001302646 -0.0001238708 0.000007401598
       S-BSL7M 2.262505 -0.01037921 0.009341724 0.0006453071 -0.00007463858 0.000004096215
    */

    // ohara equivalencies
    public static final SellmeierDispersion KF6 = S_NSL36;
    public static final SellmeierDispersion K3 = S_NSL3;
    public static final SellmeierDispersion N_LLF6 = S_TIL6;
    public static final SellmeierDispersion N_LLF1 = S_TIL1;
    public static final SellmeierDispersion LF7 = S_TIL27;
    public static final SellmeierDispersion N_LF5 = S_TIL25;
    public static final SellmeierDispersion SK_12 = S_BAL42;
    public static final SellmeierDispersion TIFN5 = S_FTM16;
    public static final SellmeierDispersion PSK52 = S_PHM53;
    public static final SellmeierDispersion N_SK18 = S_BSM18;
    //public static final SellmeierDispersion LAKN6 = S_BSM36; // can't find either?
    //public static final SellmeierDispersion LAK11 = S_LAL11;
    public static final SellmeierDispersion BAFN11 = S_BAH11;
    public static final SellmeierDispersion BASF12 = S_BAH32;
    public static final SellmeierDispersion LAKN13 = S_LAL13;
    public static final SellmeierDispersion N_BASF52 = S_BAH27;
    public static final SellmeierDispersion BASF51 = S_BAH28;
    public static final SellmeierDispersion LAK16A = S_LAL59;
    public static final SellmeierDispersion LAFN24 = S_LAM54;
    public static final SellmeierDispersion SF14 = S_TIH14;
    //public static final SellmeierDispersion N_LAF33 = S_LAH51;
    public static final SellmeierDispersion LASFN9 = S_LAH71;
    public static final SellmeierDispersion N_LASF31 = S_LAH58;
    //public static final SellmeierDispersion SF66 = PBH71;


    // equivalencies stored at http://www.glassfab.com/content/info.asp
    public static final SellmeierDispersion FK5 = S_FSL5;
    //public static final SellmeierDispersion BK3 = BSL3;
    public static final SellmeierDispersion FTL10 = K10;
    //public static final SellmeierDispersion PK1 = BSL21;
    //public static final SellmeierDispersion BK1 = BSL1;
    public static final SellmeierDispersion NSL7 = K7;
    //public static final SellmeierDispersion KF3 = NSL33;
    //public static final  SellmeierDispersion PK2 = BSL22;
    public static final SellmeierDispersion K5 = S_NSL5;
    //public static final SellmeierDispersion KZFN2 = SSL2;
    //public static final SellmeierDispersion LLF8 = PBL6;
    public static final SellmeierDispersion BAK2 = S_BAL12;
    public static final SellmeierDispersion SK11 = S_BAL41;
    public static final SellmeierDispersion BAK4 = S_BAL14;
    public static final SellmeierDispersion BAK1 = S_BAL11;
    //public static final SellmeierDispersion BAF3 = BAM3;
    //public static final SellmeierDispersion BAF3 = BAM3;
    public static final SellmeierDispersion SK12 = S_BAL42;
    public static final SellmeierDispersion SK5 = S_BAL35;
    //public static final SellmeierDispersion F8 = PBM8;
    //public static final SellmeierDispersion BK3 = BSL3;
    //public static final SellmeierDispersion KF3 = NSL33;
    public static final SellmeierDispersion SK14 = S_BSM14;
    public static final SellmeierDispersion BAF4 = S_BAM4;
    public static final SellmeierDispersion SK2 = S_BSM2;
    //public static final SellmeierDispersion SK7 = BSM7;
    public static final SellmeierDispersion SK4 = S_BSM4;
    public static final SellmeierDispersion SSKN8 = S_BSM28;
    public static final SellmeierDispersion PSK53A = S_PHM52;
    public static final SellmeierDispersion SK16 = S_BSM16;
    public static final SellmeierDispersion SK10 = S_BSM10;
    public static final SellmeierDispersion SK15 = S_BSM15;
    public static final SellmeierDispersion SK18A = S_BSM18;
    public static final SellmeierDispersion LAKL21 = S_BSM81;
    //public static final SellmeierDispersion LAKN6 = S_BSM36;
    //public static final SellmeierDispersion BASF10 = BAH30;
    public static final SellmeierDispersion LAKN22 = S_LAL54;
    public static final SellmeierDispersion LAKN7 = S_LAL7;
    public static final SellmeierDispersion SSKN5 = S_BSM25;
    //public static final SellmeierDispersion LAK11 = S_LAL11;
    public static final SellmeierDispersion BAFN10 = S_BAH10;
    //public static final SellmeierDispersion LAK23 = S_LAL52;
    public static final SellmeierDispersion LAKN12 = S_LAL12;
    //public static final SellmeierDispersion SF8 = PBM28;
    public static final SellmeierDispersion LAK9 = S_LAL9;
    public static final SellmeierDispersion LAKN14 = S_LAL14;
    public static final SellmeierDispersion PBM35 = SF15;
    public static final SellmeierDispersion BASF52 = S_BAH27;
    public static final SellmeierDispersion LAK8 = S_LAL8;
    public static final SellmeierDispersion PBH1 = SF1;
    public static final SellmeierDispersion LAF3 = S_LAM3;
    //public static final SellmeierDispersion KZFS8 = BPH8;
    public static final SellmeierDispersion LAK10 = S_LAL10;
    public static final SellmeierDispersion PBH10 = SF10;
    //public static final SellmeierDispersion SF3 = PBH3;
    //public static final SellmeierDispersion SF13 = PBH13;
    public static final SellmeierDispersion LAF2 = S_LAM2;
    //public static final SellmeierDispersion SF19 = PBM39;
    //public static final SellmeierDispersion LAK23 = S_LAL52;
    public static final SellmeierDispersion PBM25 = SF5;
    //public static final SellmeierDispersion SF13 = PBH13;
    public static final SellmeierDispersion SFL4A = S_TIH4;
    public static final SellmeierDispersion LAK33 = S_YGH51;
    //public static final SellmeierDispersion SF55 = PBH25;
    public static final SellmeierDispersion LAFN28 = S_LAH66;
    public static final SellmeierDispersion SFL56 = S_TIH23;
    public static final SellmeierDispersion LAFN10 = S_LAH51;
    public static final SellmeierDispersion LAFN21 = S_LAH64;
    public static final SellmeierDispersion LASF36A = S_LAM66;
    public static final SellmeierDispersion LASFN30 = S_LAH65;
    public static final SellmeierDispersion LASF3 = S_LAH53;
    public static final SellmeierDispersion LASFN31 = S_LAH58;
    //public static final SellmeierDispersion SF58 = PBH71;
    //public static final SellmeierDispersion F6 = PBM6;


    // equivalencies from http://www.nakedoptics.com/nhg_glass_crossref.php
    public static final SellmeierDispersion N_LAK33 = S_YGH51;

    // equivalency from patent 6122111 (http://www.freepatentsonline.com/6122111.pdf) and http://www.glassfab.com/content/info.asp
    public static final SellmeierDispersion PBH6W = S_TIH6;
    public static final SellmeierDispersion PHMS2 = S_PHM52;


    // equivaliencies from http://www.hebo-glass.com/en/o_assortment
    //public static final SellmeierDispersion BASF64A = N_BASF64A;
    public static final SellmeierDispersion BK10 = N_BK10;
    //public static final SellmeierDispersion FK51 = N_FK51;
    //public static final SellmeierDispersion LAF3 = N_LAF3;
    public static final SellmeierDispersion LAK21 = N_LAK21;
    public static final SellmeierDispersion PK51A = N_PK51;
    public static final SellmeierDispersion PSK3 = N_PSK3;
    public static final SellmeierDispersion ZKN7 = N_ZK7;


    // yet another equivalence table: http://www.sinolens.com/products.php?id=3&prdid=31&type=1

    public static void main( String[] args ) {

        printGlassDifference( S_NSL3, K3 );
    }

    private static void printGlassDifference( SellmeierDispersion a, SellmeierDispersion b ) {
        double ax = a.compute( 450 );
        double bx = b.compute( 450 );
        double ay = a.compute( 550 );
        double by = b.compute( 550 );
        double az = a.compute( 650 );
        double bz = b.compute( 650 );
        System.out.println( ax + " : " + bx );
        System.out.println( ay + " : " + by );
        System.out.println( az + " : " + bz );
    }
}
