package com.jonathanolson.graphics.trace.util;

public interface Hittable {
    public RayHit intersect( Ray3d ray );

    public Ray3d process( Ray3d ray, RayHit hit, double wavelength );
}
