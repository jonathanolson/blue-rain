package com.jonathanolson.graphics.trace.formats;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;

public class XYZFormat {
    public static float[][][] readXYZ( String filename ) throws IOException {
        DataInputStream in = new DataInputStream( new FileInputStream( filename ) );
        byte[] widthBytes = new byte[4];
        byte[] heightBytes = new byte[4];
        in.readFully( widthBytes );
        in.readFully( heightBytes );
        int width = OldFormatUtils.reverseIntBytes( widthBytes, 0 );
        int height = OldFormatUtils.reverseIntBytes( heightBytes, 0 );
        float[][][] ret = new float[height][width][3];
        byte[] bytes = new byte[width * height * 3 * 4];
        int buflen = 0;
        int pos = 0;
        for ( int y = 0; y < height; y++ ) {
            while ( pos + width * 3 * 4 > buflen ) {
                int numtoread = width * 3 * 4;
                if ( numtoread + buflen > bytes.length ) {
                    numtoread = bytes.length - buflen;
                }
                int readcount = in.read( bytes, buflen, numtoread );
                if ( readcount == -1 ) {
                    throw new RuntimeException( "OI!" );
                }
                buflen += readcount;
            }
            for ( int x = 0; x < width; x++ ) {
                for ( int c = 0; c < 3; c++ ) {
                    if ( pos + 4 > buflen ) {
                        throw new RuntimeException( "Ran past" );
                    }
                    float v = Float.intBitsToFloat( OldFormatUtils.reverseIntBytes( bytes, pos ) );
                    pos += 4;
                    ret[y][x][c] = v;
                }
            }
        }
        in.close();
        return ret;
    }
}
