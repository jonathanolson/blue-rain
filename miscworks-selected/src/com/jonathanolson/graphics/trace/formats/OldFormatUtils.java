package com.jonathanolson.graphics.trace.formats;

public class OldFormatUtils {
    public static int reverseIntBytes( byte[] bytes, int offset ) {
        int ret = bytes[offset] & 0xFF;
        ret += ( bytes[offset + 1] & 0xFF ) << 8;
        ret += ( bytes[offset + 2] & 0xFF ) << 16;
        ret += ( bytes[offset + 3] & 0xFF ) << 24;
        return ret;
    }
}
