package com.jonathanolson.graphics.trace.test;

import java.awt.*;
import java.util.LinkedList;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.PNode;
import edu.umd.cs.piccolo.nodes.PPath;

import com.jonathanolson.graphics.trace.lens.*;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.util.FresnelReflection;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.graphics.trace.util.RayHit;
import com.jonathanolson.util.SwingUtils;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class DisplayLens extends PCanvas {
    private double multiplier;
    private LensDesign design;
    private int padding = 30;
    private PNode holder;
    private double yoffset;

    public DisplayLens( LensDesign design ) {
        this( design, 256, 0 );
    }

    public DisplayLens( LensDesign design, double multiplier, double leftpad ) {
        this.design = design;
        this.multiplier = multiplier;

        holder = new PNode();
        getLayer().addChild( holder );
        holder.setOffset( padding + multiplier * leftpad, padding );

        setPreferredSize( new Dimension(
                (int) ( multiplier * ( design.getWidth() + leftpad ) ) + 2 * padding,
                (int) ( multiplier * design.getHeight() ) + 2 * padding
        ) );

        yoffset = multiplier * design.getHeight() / 2;
        for ( LensElement element : design.getElements() ) {
            displayElement( element );
        }

    }

    public void drawRandomForwardRays( Point3d point, int count, double wavelength, Color color ) {
        for ( int i = 0; i < count; i++ ) {
            Vector3d dir = Vector3d.randomUnit();
            dir.setZ( Math.abs( dir.z ) );
            Ray3d ray = new Ray3d( point, dir );
            drawForwardRays( ray, wavelength, color );
        }
    }

    public void drawForwardRays( Ray3d init, double wavelength, Color color ) {
        Ray3d r0 = init;
        boolean success = true;
        for ( LensElement element : design.getElements() ) {
            Ray3d r1 = element.getTransmittedRay( r0, wavelength );
            if ( r1 == null ) {
                success = false;
                break;
            }
            PPath line = ( cline( r0.getPos().z, r0.getPos().y, r1.getPos().z, r1.getPos().y ) );
            addTraceLine( color, line );
            r0 = r1;
        }
        if ( success ) {
            PPath line = ( cline( r0.getPos().z, r0.getPos().y, r0.getPos().z + r0.getDir().z * 10, r0.getPos().y + r0.getDir().y * 10 ) );
            addTraceLine( color, line );
        }
    }

    public void drawRandomBackwardRays( Point3d point, int count, double wavelength, Color color ) {
        for ( int i = 0; i < count; i++ ) {
            Vector3d dir = Vector3d.randomUnit();
            dir.setZ( -Math.abs( dir.z ) );
            Ray3d ray = new Ray3d( point, dir );
            drawBackwardRays( ray, wavelength, color );
        }
    }

    public void drawBackwardRays( Ray3d init, double wavelength, Color color ) {
        Ray3d r0 = init;
        boolean success = true;
        LinkedList<LensElement> revs = new LinkedList<LensElement>();
        for ( LensElement element : design.getElements() ) {
            if ( !( element instanceof SimpleLensImage ) ) {
                revs.add( 0, element );
            }
        }
        for ( LensElement element : revs ) {
            Ray3d r1 = element.getTransmittedRay( r0, wavelength );
            if ( r1 == null ) {
                success = false;
                break;
            }
            PPath line = ( cline( r0.getPos().z, r0.getPos().y, r1.getPos().z, r1.getPos().y ) );
            addTraceLine( color, line );
            r0 = r1;
        }
        if ( success ) {
            PPath line = ( cline( r0.getPos().z, r0.getPos().y, r0.getPos().z + r0.getDir().z * 100, r0.getPos().y + r0.getDir().y * 100 ) );
            addTraceLine( color, line );
        }
    }

    public void drawRandomFullRays( Point3d point, int count, double wavelength, Color color ) {
        for ( int i = 0; i < count; i++ ) {
            Vector3d dir = Vector3d.randomUnit();
            dir.setZ( -Math.abs( dir.z ) );
            Ray3d ray = new Ray3d( point, dir );
            drawFullRays( ray, wavelength, color );
        }
    }

    public void drawFullRays( Ray3d init, double wavelength, Color color ) {
        Ray3d ray = init;
        boolean hitOnce = false;
        int lastHitIndex = -1;
        java.util.List<LensElement> elements = new LinkedList<LensElement>();
        for ( LensElement element : design.getElements() ) {
            if ( element instanceof SimpleLensImage ) {

            }
            else {
                elements.add( element );
            }
        }
        while ( true ) {
            double bestDistance = Double.POSITIVE_INFINITY;
            int bestIndex = -1;
            Ray3d bestRay = null;
            for ( int idx = 0; idx < elements.size(); idx++ ) {
                if ( lastHitIndex != -1 && ( idx < lastHitIndex - 1 || idx > lastHitIndex + 1 ) ) {
                    continue;
                }
                LensElement element = elements.get( idx );

                RayHit hit = element.intersect( ray );

                if ( hit == null || hit.getDistance() > bestDistance ) {
                    continue;
                }

                bestDistance = hit.getDistance();
                bestIndex = idx;
                if ( hit instanceof LensSurfaceHit ) {
                    LensSurfaceHit lensHit = (LensSurfaceHit) hit;
                    double na = ( lensHit.fromMaterial == null ) ? 1 : ( lensHit.fromMaterial.getDispersion().compute( wavelength ) );
                    double nb = ( lensHit.toMaterial == null ) ? 1 : ( lensHit.toMaterial.getDispersion().compute( wavelength ) );
                    bestRay = new Ray3d( hit.getHitPoint(), FresnelReflection.getFresnelDir( ray.getDir(), hit.getNormal(), na, nb ) );
                }
                else {
                    bestRay = new Ray3d( hit.getHitPoint(), ray.getDir() );
                }
            }
            if ( bestRay == null ) {
                break;
            }
            else {
                hitOnce = true;
            }
            PPath line = ( cline( ray.getPos().z, ray.getPos().y, bestRay.getPos().z, bestRay.getPos().y ) );
            addTraceLine( color, line );
            ray = bestRay;
            lastHitIndex = bestIndex;
        }
        if ( hitOnce ) {
            PPath line = ( cline( ray.getPos().z, ray.getPos().y, ray.getPos().z + ray.getDir().z * 0.1, ray.getPos().y + ray.getDir().y * 0.1 ) );
            addTraceLine( color, line );
        }
    }

    private void addTraceLine( Color color, PPath line ) {
        line.setStrokePaint( color );
        line.setPaint( new Color( 0, 0, 0, 0 ) );
        line.setStroke( new BasicStroke( 1f ) );
        holder.addChild( line );
    }

    private PPath cline( double x1, double y1, double x2, double y2 ) {
        return PPath.createLine( (float) ( x1 * multiplier ), (float) ( y1 * multiplier + yoffset ), (float) ( x2 * multiplier ), (float) ( y2 * multiplier + yoffset ) );
    }

    private void addThinBlack( PPath imageLine ) {
        imageLine.setStrokePaint( Color.BLACK );
        imageLine.setPaint( new Color( 0, 0, 0, 0 ) );
        imageLine.setStroke( new BasicStroke( 1f ) );
        holder.addChild( imageLine );
    }

    private void addMarker( double x, double y, Color color ) {
        float radius = 2.0f;
        PPath path = PPath.createEllipse( (float) ( x - radius ), (float) ( y - radius ), radius * 2, radius * 2 );
        path.setPaint( new Color( 0, 0, 0, 0 ) );
        path.setStrokePaint( color );
        holder.addChild( path );
    }

    private boolean lastWasMaterial = false;
    private double lastX = 0;
    private double lastY = 0;

    public void displayElement( LensElement element ) {
        if ( element instanceof SphericalLensSurface ) {
            SphericalLensSurface surface = (SphericalLensSurface) element;
            int pointcount = 50;
            float[] xs = new float[pointcount];
            float[] ys = new float[pointcount];
            double radius = surface.getRadius();
            double absRadius = Math.abs( radius );
            double angle = Math.asin( surface.getDiameter() / ( 2 * absRadius ) );
            double center = surface.getCenter().z * multiplier;
            for ( int i = 0; i < pointcount; i++ ) {
                double curangle = ( 2 * angle * ( (double) i ) / ( (double) ( pointcount - 1 ) ) ) - angle;
                if ( surface.getRadius() > 0 ) {
                    xs[i] = (float) ( center - absRadius * multiplier * Math.cos( curangle ) );
                }
                else {
                    xs[i] = (float) ( center + absRadius * multiplier * Math.cos( curangle ) );
                }
                ys[i] = (float) ( yoffset + absRadius * multiplier * Math.sin( curangle ) );
            }
            PPath curve = PPath.createPolyline( xs, ys );
            addGray( curve );

            double curX, curY;
            if ( surface.getRadius() > 0 ) {
                curX = center - absRadius * multiplier * Math.cos( angle );
            }
            else {
                curX = center + absRadius * multiplier * Math.cos( angle );
            }
            curY = absRadius * multiplier * Math.sin( angle );

            if ( lastWasMaterial ) {
                if ( lastY < curY ) {
                    addGray( PPath.createLine( (float) lastX, (float) ( lastY + yoffset ), (float) lastX, (float) ( curY + yoffset ) ) );
                    addGray( PPath.createLine( (float) lastX, (float) ( -lastY + yoffset ), (float) lastX, (float) ( -curY + yoffset ) ) );
                    lastY = curY;
                }
                addGray( PPath.createLine( (float) lastX, (float) ( lastY + yoffset ), (float) curX, (float) ( lastY + yoffset ) ) );
                addGray( PPath.createLine( (float) lastX, (float) ( -lastY + yoffset ), (float) curX, (float) ( -lastY + yoffset ) ) );
                if ( curY < lastY ) {
                    addGray( PPath.createLine( (float) curX, (float) ( lastY + yoffset ), (float) curX, (float) ( curY + yoffset ) ) );
                    addGray( PPath.createLine( (float) curX, (float) ( -lastY + yoffset ), (float) curX, (float) ( -curY + yoffset ) ) );
                }
            }

            if ( surface.getRadius() > 0 ) {
                // TODO: implement front / back handling in SphericalLensSurface
                lastWasMaterial = surface.getInMaterial() != null;
            }
            else {
                lastWasMaterial = surface.getOutMaterial() != null;
            }
            lastX = curX;
            lastY = curY;
        }
        else if ( element instanceof FlatLensSurface ) {
            FlatLensSurface surface = (FlatLensSurface) element;
            double center = surface.getCenter().z * multiplier;
            addGray( cline( surface.getCenter().z, surface.getDiameter() / 2, surface.getCenter().z, -surface.getDiameter() / 2 ) );

            double curX, curY;
            curX = center;
            curY = multiplier * surface.getDiameter() / 2;

            if ( lastWasMaterial ) {
                if ( lastY < curY ) {
                    addGray( PPath.createLine( (float) lastX, (float) ( lastY + yoffset ), (float) lastX, (float) ( curY + yoffset ) ) );
                    addGray( PPath.createLine( (float) lastX, (float) ( -lastY + yoffset ), (float) lastX, (float) ( -curY + yoffset ) ) );
                    lastY = curY;
                }
                addGray( PPath.createLine( (float) lastX, (float) ( lastY + yoffset ), (float) curX, (float) ( lastY + yoffset ) ) );
                addGray( PPath.createLine( (float) lastX, (float) ( -lastY + yoffset ), (float) curX, (float) ( -lastY + yoffset ) ) );
                if ( curY < lastY ) {
                    addGray( PPath.createLine( (float) curX, (float) ( lastY + yoffset ), (float) curX, (float) ( curY + yoffset ) ) );
                    addGray( PPath.createLine( (float) curX, (float) ( -lastY + yoffset ), (float) curX, (float) ( -curY + yoffset ) ) );
                }
            }

            // TODO: proper lens direction support in elements
            lastWasMaterial = surface.getInMaterial() != null;
            lastX = curX;
            lastY = curY;
        }
        else if ( element instanceof SimpleLensStop ) {
            SimpleLensStop stop = (SimpleLensStop) element;
            lastWasMaterial = false;
            float x = (float) ( stop.getCenter().z * multiplier );
            float r = (float) stop.getRadius();
            PPath top = PPath.createLine( x, (float) ( 0 ), x, (float) ( yoffset - r * multiplier ) );
            addThickBlack( top );
            PPath bottom = PPath.createLine( x, (float) ( yoffset * 2 ), x, (float) ( yoffset + r * multiplier ) );
            addThickBlack( bottom );
        }
        else if ( element instanceof SimpleLensImage ) {
            // TODO: abstract out lens image element as superclass?
            SimpleLensImage image = (SimpleLensImage) element;
            PPath imageLine = PPath.createLine( (float) ( image.getCenter().z * multiplier ), (float) ( 0 ), (float) ( image.getCenter().z * multiplier ), (float) ( yoffset * 2 ) );
            addThinBlack( imageLine );
        }
    }

    private void addThickBlack( PPath bottom ) {
        bottom.setStrokePaint( Color.BLACK );
        bottom.setPaint( new Color( 0, 0, 0, 0 ) );
        bottom.setStroke( new BasicStroke( 3f ) );
        holder.addChild( bottom );
    }

    private void addGray( PPath curve ) {
        curve.setStrokePaint( Color.GRAY );
        curve.setPaint( new Color( 0, 0, 0, 0 ) );
        holder.addChild( curve );
    }

    public LensDesign getDesign() {
        return design;
    }

    public static void main( String[] args ) {
        //double z = -20;

        DisplayLens test = new DisplayLens( new DoubleGaussD(), 256, 1 );

        double w = test.getDesign().getWidth();

        float alpha = 0.03f;

//        test.drawBackwardRays( new Ray3d( new Point3d( 0, 0.2, w ), new Vector3d( 0, -0.15, -1, true ) ), 700, new Color( 1, 0, 0, 1f ) );
//        test.drawBackwardRays( new Ray3d( new Point3d( 0, 0.2, w ), new Vector3d( 0, -0.15, -1, true ) ), 546, new Color( 0, 1, 0, 1f ) );
//        test.drawBackwardRays( new Ray3d( new Point3d( 0, 0.2, w ), new Vector3d( 0, -0.15, -1, true ) ), 435, new Color( 0, 0, 1, 1f ) );
//
//        test.drawForwardRays( new Ray3d( new Point3d( 0, 1.0, -1 ), new Vector3d( 0, -0.15, 1, true ) ), 700, new Color( 1, 0, 0, 1f ) );
//        test.drawForwardRays( new Ray3d( new Point3d( 0, 1.0, -1 ), new Vector3d( 0, -0.15, 1, true ) ), 546, new Color( 0, 1, 0, 1f ) );
//        test.drawForwardRays( new Ray3d( new Point3d( 0, 1.0, -1 ), new Vector3d( 0, -0.15, 1, true ) ), 435, new Color( 0, 0, 1, 1f ) );

        test.drawRandomFullRays( new Point3d( 0, 0, w ), 20000, 534, new Color( 0, 0, 1, alpha ) );

        //test.drawRandomBackwardRays( new Point3d( 0, -0.25, w ), 2000, 435, new Color( 0, 0, 1, alpha ) );
        //test.drawRandomBackwardRays( new Point3d( 0, 0.25, w ), 2000, 700, new Color( 1, 0, 0, alpha ) );
        //test.drawRandomBackwardRays( new Point3d( 0, 0, w ), 2000, 546, new Color( 0, 1, 0, alpha ) );

//        test.drawRandomForwardRays( new Point3d( 0, 0, z ), 200000, 435, new Color( 0, 0, 1, alpha ) );
//        test.drawRandomForwardRays( new Point3d( 0, 2, z ), 200000, 700, new Color( 1, 0, 0, alpha ) );
//        test.drawRandomForwardRays( new Point3d( 0, 4, z ), 200000, 546, new Color( 0, 1, 0, alpha ) );
//
//        test.drawRandomForwardRays( new Point3d( 0, -40, -200 ), 20000000, 546, new Color( 0, 0, 0, alpha ) );

        SwingUtils.popupFrame( "Lens test", test );
    }
}