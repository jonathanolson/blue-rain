package com.jonathanolson.graphics.trace.test;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.SimpleLensImage;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class PointArrayTest {
    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), 0.5 );
        RasterComponent.showRaster( raster );

        Thread t = new Thread() {
            @Override
            public void run() {
                LensDesign design = new DoubleGaussD();
                for ( LensElement element : design.getElements() ) {
                    if ( element instanceof SimpleLensImage ) {
                        SimpleLensImage image = (SimpleLensImage) element;
                        image.setCenter( new Point3d( 0, 0, image.getCenter().z + 0.08490345743192626 ) );
                    }
                }
                while ( true ) {
                    double wavelength = RandomUtils.random() * 300 + 400;
                    //Point3d pos = new Point3d( RandomUtils.random(), RandomUtils.random(), -10 );
                    double x = Math.floor( RandomUtils.random() * 20 ) / 2 - 4.5;
                    double y = Math.floor( RandomUtils.random() * 20 ) / 2 - 4.5;
                    Point3d pos = new Point3d( x, y, -10 );
                    Vector3d dir = Vector3d.randomUnit();
                    dir.z = Math.abs( dir.z );
                    Ray3d ray = new Ray3d( pos, dir );
                    for ( LensElement element : design.getElements() ) {
                        ray = element.getTransmittedRay( ray, wavelength );
                        if ( ray == null ) {
                            break;
                        }
                    }
                    if ( ray != null ) {
                        raster.addXYZAt( new Point2d( ray.getPos().x, -ray.getPos().y ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );
                    }
                }
            }
        };

        t.start();
    }
}
