package com.jonathanolson.graphics.trace.test;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.colors.BoxGreen;
import com.jonathanolson.graphics.color.colors.BoxRed;
import com.jonathanolson.graphics.color.colors.BoxWhite;
import com.jonathanolson.graphics.color.colors.SpectralColor;
import com.jonathanolson.graphics.trace.lens.BackwardLensTracer;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.*;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Box5cProj extends RasterThread {

    private final Raster3 raster;

    private LensDesign design;
    private double imageZ;
    private double bounds;

    private static final double ratioRadius = 0.5;

    private static final double focusPoint = -600;

    private static final double LEFT_X = -90.0;
    private static final double RIGHT_X = 90.0;
    private static final double TOP_Y = 90.0;
    private static final double BOTTOM_Y = -90.0;
    private static final double FRONT_Z = -50.0;
    private static final double BACK_Z = -1200.0;

    private static final double RADIUS_CHROME = 36.0;
    private static final double RADIUS_GLASS = 50.0;
    private static final double RADIUS_COPPER = 36.0;
    private static final double RADIUS_DIFFUSE = 36.0;
    private static final Point3d CENTER_CHROME = new Point3d( -45.0, -54.0, -200.0 );
    private static final Point3d CENTER_GLASS = new Point3d( 0.0, -0.0, -500.0 );
    private static final Point3d CENTER_COPPER = new Point3d( 45.0, 54.0, -200.0 );
    private static final Point3d CENTER_DIFFUSE = new Point3d( -45.0, 54.0, -600.0 );

    private static final int maxBounceCount = 20;

    private HitList root = new HitList();

    private BackwardLensTracer lensTracer;

    public Box5cProj( Raster3 raster ) {
        this.raster = raster;

        design = new DoubleGaussD();
        design.focusAt( focusPoint, 534 );
        design.scaleStopRadius( ratioRadius );
        imageZ = design.getImagePlaneZ();

        lensTracer = new BackwardLensTracer( design );

        bounds = raster.getScale();

        root.addHittable( new LeftWall() );
        root.addHittable( new RightWall() );
        root.addHittable( new Floor() );
        root.addHittable( new GlassSphere() );
        root.addHittable( new BlackSphere( CENTER_CHROME, RADIUS_CHROME ) );
        root.addHittable( new CopperSphere() );
        //root.addHittable( new DiffuseSphere() );
    }

    private Plane3d topPlane = new Plane3d( new Vector3d( 0, -1, 0 ), -TOP_Y );

    private static class GlassSphere implements Hittable {
        private Sphere3d glassSphere = new Sphere3d( CENTER_GLASS, RADIUS_GLASS );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = glassSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            double na;
            double nb;
            if ( sphereHit.fromOutside ) {
                na = 1;
                nb = SellmeierDispersion.BK7.compute( wavelength );
            }
            else {
                na = SellmeierDispersion.BK7.compute( wavelength );
                nb = 1;
            }
            return new Ray3d( sphereHit.hitPoint, FresnelReflection.getFresnelDir( oldRay.getDir(), sphereHit.normal, na, nb ) );
        }
    }

    private static class BlackSphere implements Hittable {

        private Point3d center;
        private double radius;

        private Sphere3d chromeSphere;

        private BlackSphere( Point3d center, double radius ) {
            this.center = center;
            this.radius = radius;
            this.chromeSphere = new Sphere3d( center, radius );
        }

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = chromeSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside || RandomUtils.random() > FresnelReflection.simpleFresnelDielectric( oldRay.getDir(), sphereHit.normal, 1, SellmeierDispersion.BK7.compute( wavelength ) ) ) {
                return null;
            }
            return new Ray3d( sphereHit.hitPoint, FresnelReflection.reflect( oldRay.getDir(), sphereHit.normal ) );
        }
    }

    private static class CopperSphere implements Hittable {
        private Sphere3d copperSphere = new Sphere3d( CENTER_COPPER, RADIUS_COPPER );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = copperSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside ) {
                return null;
            }
            double n = Copper.getN( wavelength );
            double k = Copper.getK( wavelength );
            double[] rr = FresnelReflection.fresnel( oldRay.getDir(), sphereHit.normal, 1, n, k );
            if ( RandomUtils.random() > ( rr[0] + rr[1] ) / 2 ) {
                return null;
            }
            else {
                return new Ray3d( sphereHit.hitPoint, FresnelReflection.reflect( oldRay.getDir(), sphereHit.normal ) );
            }
        }
    }

    private static class DiffuseSphere implements Hittable {
        private Sphere3d diffuseSphere = new Sphere3d( CENTER_DIFFUSE, RADIUS_DIFFUSE );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = diffuseSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside ) {
                return null;
            }
            return new Ray3d( sphereHit.hitPoint, Vector3d.randomProjectedHemisphere( sphereHit.normal ) );
        }
    }

    private static class LeftWall implements Hittable {

        private Plane3d leftPlane = new Plane3d( new Vector3d( 1, 0, 0 ), LEFT_X );
        private SpectralColor leftColor = new BoxRed();

        @Override
        public RayHit intersect( Ray3d ray ) {
            Plane3d.Hit hit = leftPlane.intersect( ray );
            if ( hit != null && hit.hitPoint.z < FRONT_Z && hit.hitPoint.z > BACK_Z && hit.hitPoint.y > BOTTOM_Y && hit.hitPoint.y < TOP_Y ) {
                return hit;
            }
            else {
                return null;
            }
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            double yy = Math.abs( hit.getHitPoint().getY() ) / 10;
            double zz = Math.abs( hit.getHitPoint().getZ() ) / 50;
            if ( Math.abs( zz - Math.floor( zz ) ) < 0.1 || Math.abs( yy - Math.floor( yy ) ) < 0.1 ) {
                if ( RandomUtils.random() < 0.01 ) {
                    return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
                }
                else {
                    return null;
                }
            }
            if ( RandomUtils.random() < FresnelReflection.simpleFresnelDielectric( oldRay.getDir(), hit.getNormal(), 1, 1.5 ) ) {
                return new Ray3d( hit.getHitPoint(), FresnelReflection.reflect( oldRay.getDir(), hit.getNormal() ) );
            }
            if ( RandomUtils.random() > leftColor.reflection( wavelength ) ) {
                return null;
            }
            else {
                return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
            }
        }
    }

    private static class RightWall implements Hittable {

        private Plane3d rightPlane = new Plane3d( new Vector3d( -1, 0, 0 ), -RIGHT_X );
        private SpectralColor rightColor = new BoxGreen();

        @Override
        public RayHit intersect( Ray3d ray ) {
            Plane3d.Hit hit = rightPlane.intersect( ray );
            if ( hit != null && hit.hitPoint.z < FRONT_Z && hit.hitPoint.z > BACK_Z && hit.hitPoint.y > BOTTOM_Y && hit.hitPoint.y < TOP_Y ) {
                return hit;
            }
            else {
                return null;
            }
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            if ( hit.getHitPoint().getY() < 0 ) {
                if ( RandomUtils.random() > rightColor.reflection( wavelength ) ) {
                    return null;
                }
                else {
                    return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
                }
            }
            else {
                double yy = Math.abs( hit.getHitPoint().getY() ) / 10;
                double zz = Math.abs( hit.getHitPoint().getZ() ) / 50;
                if ( Math.abs( zz - Math.floor( zz ) ) < 0.1 || Math.abs( yy - Math.floor( yy ) ) < 0.1 ) {
                    if ( RandomUtils.random() < 0.01 ) {
                        return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
                    }
                    else {
                        return null;
                    }
                }
                if ( RandomUtils.random() < FresnelReflection.simpleFresnelDielectric( oldRay.getDir(), hit.getNormal(), 1, 1.5 ) ) {
                    return new Ray3d( hit.getHitPoint(), FresnelReflection.reflect( oldRay.getDir(), hit.getNormal() ) );
                }
                if ( RandomUtils.random() > rightColor.reflection( wavelength ) ) {
                    return null;
                }
                else {
                    return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
                }
            }
        }
    }

    private static class Floor implements Hittable {

        private Plane3d bottomPlane = new Plane3d( new Vector3d( 0, 1, 0 ), BOTTOM_Y );
        private SpectralColor bottomColor = new BoxWhite();

        @Override
        public RayHit intersect( Ray3d ray ) {
            Plane3d.Hit hit = bottomPlane.intersect( ray );
            if ( hit != null && hit.hitPoint.z < FRONT_Z && hit.hitPoint.z > BACK_Z && hit.hitPoint.x < RIGHT_X && hit.hitPoint.x > LEFT_X ) {
                return hit;
            }
            else {
                return null;
            }
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            if ( RandomUtils.random() > bottomColor.reflection( wavelength ) ) {
                return null;
            }
            else {
                return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
            }
        }
    }

    @Override
    public void render() {
        int subcount = 10000000;
        int hits = 0;
        int exitLens = 0;
        int c = 0;
        long startTime = System.currentTimeMillis();
        Vector3d pointdir = new Vector3d( 0, 0, -1 );
        Point3d pos = new Point3d( 0, 0, 0 );
        DiffuseSphere lightSphere = new DiffuseSphere();
        StandardIlluminant.DIlluminantSpectralPower ballLight = new StandardIlluminant.DIlluminantSpectralPower( StandardIlluminant.cieDIlluminantChroma( 4000 ) );
        StandardIlluminant.DIlluminantSpectralPower night = new StandardIlluminant.DIlluminantSpectralPower( StandardIlluminant.cieDIlluminantChroma( 24000 ) );
        StandardIlluminant.DIlluminantSpectralPower moon = new StandardIlluminant.DIlluminantSpectralPower( StandardIlluminant.cieDIlluminantChroma( 4000 ) );
        Vector3d moonDir = new Vector3d( 1, 2, 0.3, true );
        while ( c++ < subcount ) {
            double wavelength = RandomUtils.random() * 300 + 400;


            Vector3d dir = Vector3d.randomProjectedHemisphere( pointdir );
            Ray3d ray = new Ray3d( pos, dir );
            double imageX = -dir.getX();
            double imageY = -dir.getY();

            exitLens++;
            boolean absorb = false;
            int bounceCount = 0;

            RayHit rootHit = root.intersect( ray );
            if ( rootHit != null ) {
                Vector3d dd = Vector3d.randomProjectedHemisphere( rootHit.getNormal() );
                RayHit secondHit = root.intersect( new Ray3d( rootHit.getHitPoint(), dd ) );
                if ( secondHit == null ) {
                    //raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, 1 );
                    Point3d contrib = new Point3d(
                            Math.abs( dd.getX() + 1 ) / 2,
                            Math.abs( dd.getY() + 1 ) / 2,
                            Math.abs( dd.getZ() + 1 ) / 2
                    );
                    raster.addAt( new Point2d( imageX, -imageY ), contrib );
                }
            }

//            while ( bounceCount++ < maxBounceCount ) {
//
//                RayHit rootHit = root.intersect( ray );
//                RayHit lightHit = lightSphere.intersect( ray );
//
//                if ( rootHit == null && lightHit == null ) {
//                    break;
//                }
//
//                boolean didHitLight;
//
//                if ( rootHit != null && lightHit != null ) {
//                    didHitLight = lightHit.getDistance() < rootHit.getDistance();
//                }
//                else {
//                    didHitLight = lightHit != null;
//                }
//
//                if ( didHitLight ) {
//                    raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, ballLight.getPower( wavelength ) );
//                    absorb = true;
//                    break;
//                }
//                else {
//                    ray = root.process( ray, rootHit, wavelength );
//
//                    if ( ray == null ) {
//                        absorb = true;
//                        break;
//                    }
//
//                    if ( bounceCount == maxBounceCount ) {
//                        absorb = true;
//                    }
//                }
//
//            }
//
//            if ( !absorb ) {
//                if ( moonDir.dot( ray.getDir() ) > 0.98 ) {
//                    raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, moon.getPower( wavelength ) * 0.1 );
//                }
//                else {
//                    raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, night.getPower( wavelength ) * 0.01 * ( 400 / wavelength ) );
//                }
//            }

        }

        long endTime = System.currentTimeMillis();

        System.out.println( "block: " + ( endTime - startTime ) + "ms" );
        System.out.println( "each: " + ( ( (double) ( endTime - startTime ) ) / ( (double) ( subcount ) ) ) + "ms" );
        System.out.println( "hits / second: " + ( 1000.0 * (double) hits ) / ( (double) ( endTime - startTime ) ) );
        System.out.println( "hits / attempt: " + ( (double) hits ) / ( (double) ( subcount ) ) );
        System.out.println( "lens exit / attempt: " + ( (double) exitLens ) / ( (double) ( subcount ) ) );
        System.out.println();
        System.out.println();

    }

    public static void main( String[] args ) {
        double imageBounds = 1.0;
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), imageBounds );
//        double imageBounds = 0.05;
//        final Raster3 raster = new Raster3( 512, 512, new Point2d( -0.1, -0.13 ), imageBounds );
        RasterComponent rasterComponent = RasterComponent.showRaster( raster );

        int threadCount = 2;
        for ( int i = 0; i < threadCount; i++ ) {
            Thread t = new Box5cProj( raster );
            t.start();
        }

        rasterComponent.startUpdates();
    }
}