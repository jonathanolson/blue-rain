package com.jonathanolson.graphics.trace.test;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.colors.BoxGreen;
import com.jonathanolson.graphics.color.colors.BoxRed;
import com.jonathanolson.graphics.color.colors.BoxWhite;
import com.jonathanolson.graphics.color.colors.SpectralColor;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.SimpleLensImage;
import com.jonathanolson.graphics.trace.lens.SimpleLensStop;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.*;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Box5a extends RasterThread {

    private final Raster3 raster;

    private LensDesign design;
    private List<LensElement> elements;
    private double imageZ;
    private double checkerZ;
    private double imageZOffset;
    private Plane3d checkerPlane;
    private double bounds;

    private static final double ratioRadius = 0.5;

    private static final double LEFT_X = -30.0;
    private static final double RIGHT_X = 30.0;
    private static final double TOP_Y = 30.0;
    private static final double BOTTOM_Y = -30.0;
    private static final double FRONT_Z = -100.0;
    private static final double BACK_Z = -2400.0;

    private static final double RADIUS_CHROME = 12.0;
    private static final double RADIUS_GLASS = 12.0;
    private static final double RADIUS_COPPER = 12.0;
    private static final double RADIUS_DIFFUSE = 12.0;
    private static final Point3d CENTER_CHROME = new Point3d( -15.0, -18.0, -605.0 );
    private static final Point3d CENTER_GLASS = new Point3d( 15.0, -18.0, -595.0 );
    private static final Point3d CENTER_COPPER = new Point3d( 15.0, 18.0, -555.0 );
    private static final Point3d CENTER_DIFFUSE = new Point3d( -15.0, 18.0, -545.0 );

    private static final int maxBounceCount = 20;

    public Box5a( Raster3 raster ) {
        this.raster = raster;
        design = new DoubleGaussD();
        elements = new LinkedList<LensElement>();
        imageZ = 0;
        checkerZ = -600;
        imageZOffset = FocusTest.focusTest( design, checkerZ, 534 );
        System.out.println( "detected image plane offset: " + imageZOffset );

        for ( LensElement element : design.getElements() ) {
            if ( element instanceof SimpleLensImage ) {
                SimpleLensImage image = (SimpleLensImage) element;
                imageZ = image.getCenter().z + imageZOffset;
                image.setCenter( new Point3d( 0, 0, image.getCenter().z + imageZOffset ) );
            }
            else if ( element instanceof SimpleLensStop ) {
                // TODO: stop modifying actual elemens, we need to modify a COPY of the element!
                SimpleLensStop stop = (SimpleLensStop) element;
                stop.setRadius( stop.getRadius() * ratioRadius );
                elements.add( 0, stop );
            }
            else {
                elements.add( 0, element );
            }
        }

        checkerPlane = new Plane3d( new Vector3d( 0, 0, 1 ), checkerZ );
        bounds = raster.getScale();
    }

    private Plane3d leftPlane = new Plane3d( new Vector3d( 1, 0, 0 ), LEFT_X );
    private Plane3d rightPlane = new Plane3d( new Vector3d( -1, 0, 0 ), -RIGHT_X );
    private Plane3d bottomPlane = new Plane3d( new Vector3d( 0, 1, 0 ), BOTTOM_Y );
    private Sphere3d glassSphere = new Sphere3d( CENTER_GLASS, RADIUS_GLASS );
    private Sphere3d chromeSphere = new Sphere3d( CENTER_CHROME, RADIUS_CHROME );
    private Sphere3d copperSphere = new Sphere3d( CENTER_COPPER, RADIUS_COPPER );
    private Sphere3d diffuseSphere = new Sphere3d( CENTER_DIFFUSE, RADIUS_DIFFUSE );
    private SpectralColor leftColor = new BoxRed();
    private SpectralColor rightColor = new BoxGreen();
    private SpectralColor bottomColor = new BoxWhite();
    private SpectralColor diffuseColor = new BoxWhite();

    private Plane3d topPlane = new Plane3d( new Vector3d( 0, -1, 0 ), -TOP_Y );

    @Override
    public void render() {
        int subcount = 1000000;
        int hits = 0;
        int c = 0;
        long startTime = System.currentTimeMillis();
        while ( c++ < subcount ) {
            double wavelength = RandomUtils.random() * 300 + 400;

            double imageX = RandomUtils.random() * 2 * bounds - bounds;
            double imageY = RandomUtils.random() * 2 * bounds - bounds;

            Point3d pos = new Point3d( imageX, imageY, imageZ );
            Vector3d dir = Vector3d.randomUnit();
            Ray3d ray = new Ray3d( pos, dir );
            dir.z = -Math.abs( dir.z );
            for ( LensElement element : elements ) {
                ray = element.getTransmittedRay( ray, wavelength );
                if ( ray == null ) {
                    break;
                }
            }
            if ( ray != null ) {
                boolean absorb = false;
                int bounceCount = 0;
                while ( bounceCount++ < maxBounceCount ) {
                    Plane3d.Hit leftHit = leftPlane.intersect( ray );
                    Plane3d.Hit rightHit = rightPlane.intersect( ray );
                    Plane3d.Hit bottomHit = bottomPlane.intersect( ray );
                    Sphere3d.Hit glassHit = glassSphere.intersect( ray );
                    Sphere3d.Hit chromeHit = chromeSphere.intersect( ray );
                    Sphere3d.Hit copperHit = copperSphere.intersect( ray );
                    Sphere3d.Hit diffuseHit = diffuseSphere.intersect( ray );
                    boolean left = leftHit != null && leftHit.hitPoint.z < FRONT_Z && leftHit.hitPoint.z > BACK_Z && leftHit.hitPoint.y > BOTTOM_Y && leftHit.hitPoint.y < TOP_Y;
                    boolean right = rightHit != null && rightHit.hitPoint.z < FRONT_Z && rightHit.hitPoint.z > BACK_Z && rightHit.hitPoint.y > BOTTOM_Y && rightHit.hitPoint.y < TOP_Y;
                    boolean bottom = bottomHit != null && bottomHit.hitPoint.z < FRONT_Z && bottomHit.hitPoint.z > BACK_Z && bottomHit.hitPoint.x < RIGHT_X && bottomHit.hitPoint.x > LEFT_X;
                    double distance = Double.POSITIVE_INFINITY;
                    Object hit = null;

                    if ( glassHit != null && glassHit.distance < distance ) {
                        distance = glassHit.distance;
                        hit = glassHit;
                    }
                    if ( chromeHit != null && chromeHit.distance < distance ) {
                        distance = chromeHit.distance;
                        hit = chromeHit;
                    }
                    if ( copperHit != null && copperHit.distance < distance ) {
                        distance = copperHit.distance;
                        hit = copperHit;
                    }
                    if ( diffuseHit != null && diffuseHit.distance < distance ) {
                        distance = diffuseHit.distance;
                        hit = diffuseHit;
                    }
                    if ( left && leftHit.distance < distance ) {
                        distance = leftHit.distance;
                        hit = leftHit;
                    }
                    if ( right && rightHit.distance < distance ) {
                        distance = rightHit.distance;
                        hit = rightHit;
                    }
                    if ( bottom && bottomHit.distance < distance ) {
                        distance = bottomHit.distance;
                        hit = bottomHit;
                    }

                    if ( distance == Double.POSITIVE_INFINITY ) {
                        // didn't hit
                        break;
                    }

                    if ( hit == leftHit ) {
                        if ( RandomUtils.random() > leftColor.reflection( wavelength ) ) {
                            absorb = true;
                            break;
                        }
                        else {
                            ray.setPos( leftHit.hitPoint );
                            ray.setDir( Vector3d.randomHemisphere( leftPlane.getNormal() ) );
                        }
                    }
                    else if ( hit == rightHit ) {
                        if ( RandomUtils.random() > rightColor.reflection( wavelength ) ) {
                            absorb = true;
                            break;
                        }
                        else {
                            ray.setPos( rightHit.hitPoint );
                            ray.setDir( Vector3d.randomHemisphere( rightPlane.getNormal() ) );
                        }
                    }
                    else if ( hit == bottomHit ) {
                        if ( RandomUtils.random() > bottomColor.reflection( wavelength ) ) {
                            absorb = true;
                            break;
                        }
                        else {
                            ray.setPos( bottomHit.hitPoint );
                            ray.setDir( Vector3d.randomHemisphere( bottomPlane.getNormal() ) );
                        }
                    }
                    else if ( hit == glassHit ) {
                        double na;
                        double nb;
                        if ( glassHit.fromOutside ) {
                            na = 1;
                            nb = SellmeierDispersion.BK7.compute( wavelength );
                        }
                        else {
                            na = SellmeierDispersion.BK7.compute( wavelength );
                            nb = 1;
                        }
                        ray.setPos( glassHit.hitPoint );
                        ray.setDir( FresnelReflection.getFresnelDir( ray.getDir(), glassHit.normal, na, nb ) );
                    }
                    else if ( hit == chromeHit ) {
                        if ( !chromeHit.fromOutside ) {
                            absorb = true;
                            break;
                        }
                        ray.setPos( chromeHit.hitPoint );
                        ray.setDir( FresnelReflection.reflect( ray.getDir(), chromeHit.normal ) );
                    }
                    else if ( hit == copperHit ) {
                        if ( !copperHit.fromOutside ) {
                            absorb = true;
                            break;
                        }
                        double n = Copper.getN( wavelength );
                        double k = Copper.getK( wavelength );
                        double[] rr = FresnelReflection.fresnel( ray.getDir(), copperHit.normal, 1, n, k );
                        if ( RandomUtils.random() > ( rr[0] + rr[1] ) / 2 ) {
                            absorb = true;
                            break;
                        }
                        else {
                            ray.setPos( copperHit.hitPoint );
                            ray.setDir( FresnelReflection.reflect( ray.getDir(), copperHit.normal ) );
                        }
                    }
                    else if ( hit == diffuseHit ) {
                        if ( !diffuseHit.fromOutside ) {
                            absorb = true;
                            break;
                        }
                        ray.setPos( diffuseHit.hitPoint );
                        ray.setDir( Vector3d.randomHemisphere( diffuseHit.normal ) );
                    }

                    if ( bounceCount == maxBounceCount ) {
                        absorb = true;
                        break;
                    }
                }

                if ( !absorb ) {
                    Plane3d.Hit topHit = topPlane.intersect( ray );
                    if ( topHit != null ) {
                        boolean light = topHit.hitPoint.z > BACK_Z && topHit.hitPoint.z < FRONT_Z && Math.abs( Math.floor( topHit.hitPoint.z / 50 ) - topHit.hitPoint.z / 50 ) < 0.3;
                        if ( light ) {
                            hits++;
                            raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );
                        }
                    }
                }

            }
        }

        long endTime = System.currentTimeMillis();

        System.out.println( "block: " + ( endTime - startTime ) + "ms" );
        System.out.println( "each: " + ( ( (double) ( endTime - startTime ) ) / ( (double) ( subcount ) ) ) + "ms" );
        System.out.println( "hits / second: " + ( 1000.0 * (double) hits ) / ( (double) ( endTime - startTime ) ) );
        System.out.println( "hits / attempt: " + ( (double) hits ) / ( (double) ( subcount ) ) );
        System.out.println();

    }

    public static void main( String[] args ) {
        double imageBounds = 0.10;
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), imageBounds );
        RasterComponent.showRaster( raster );

        int threadCount = 1;
        for ( int i = 0; i < threadCount; i++ ) {
            Thread t = new Box5a( raster );
            t.start();
        }
    }
}
