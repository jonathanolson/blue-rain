package com.jonathanolson.graphics.trace.test;

import java.util.ArrayList;
import java.util.List;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.trace.lens.*;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.*;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class PureWhite extends RasterThread {

    private final Raster3 raster;

    private LensDesign design;
    private List<LensElement> elements;
    private double imageZ;
    private double imageZOffset;
    private double bounds;

    private static final double ratioRadius = 1;

    private static final double focusPoint = -600;

    public PureWhite( Raster3 raster ) {
        this.raster = raster;
        design = new DoubleGaussD();
        elements = new ArrayList<LensElement>();
        imageZ = 0;
        imageZOffset = FocusTest.focusTest( design, focusPoint, 534 );
        System.out.println( "detected image plane offset: " + imageZOffset );

        for ( LensElement element : design.getElements() ) {
            if ( element instanceof SimpleLensImage ) {
                SimpleLensImage image = (SimpleLensImage) element;
                imageZ = image.getCenter().z + imageZOffset;
                image.setCenter( new Point3d( 0, 0, image.getCenter().z + imageZOffset ) );
            }
            else if ( element instanceof SimpleLensStop ) {
                SimpleLensStop stop = (SimpleLensStop) element;
                stop.setRadius( stop.getRadius() * ratioRadius );
                elements.add( 0, stop );
            }
            else {
                elements.add( 0, element );
            }
        }

        bounds = raster.getScale();

    }

    private Ray3d traceLensRay( Ray3d ray, double wavelength ) {
        int elpos = -1;
        int elsize = elements.size();
        int lensBounces = 0;
        while ( ray != null && lensBounces++ < 32 ) {
            double bestDistance = Double.POSITIVE_INFINITY;
            int bestIndex = -1;
            Ray3d bestRay = null;
            for ( int i = elpos - 1; i <= elpos + 1; i++ ) {
                if ( i < 0 || i >= elsize ) {
                    continue;
                }
                LensElement element = elements.get( i );

                RayHit hit = element.intersect( ray );

                if ( hit == null || hit.getDistance() > bestDistance ) {
                    continue;
                }

                bestDistance = hit.getDistance();
                bestIndex = i;
                if ( hit instanceof LensSurfaceHit ) {
                    LensSurfaceHit lensHit = (LensSurfaceHit) hit;
                    double na = ( lensHit.fromMaterial == null ) ? 1 : ( lensHit.fromMaterial.getDispersion().compute( wavelength ) );
                    double nb = ( lensHit.toMaterial == null ) ? 1 : ( lensHit.toMaterial.getDispersion().compute( wavelength ) );
                    bestRay = new Ray3d( hit.getHitPoint(), FresnelReflection.getFresnelDir( ray.getDir(), hit.getNormal(), na, nb ) );
                }
                else {
                    bestRay = new Ray3d( hit.getHitPoint(), ray.getDir() );
                }
            }
            if ( bestRay == null ) {
                if ( elpos < elsize - 1 ) {
                    // only set ray to null if we are not at the last element
                    ray = null;
                }
                break;
            }
            elpos = bestIndex;
            ray = bestRay;
        }
        return ray;
    }

    @Override
    public void render() {
        int subcount = 1000000;
        int hits = 0;
        int exitLens = 0;
        int c = 0;
        long startTime = System.currentTimeMillis();
        while ( c++ < subcount ) {
            double wavelength = RandomUtils.random() * 300 + 400;

            double imageX = RandomUtils.random() * 2 * bounds - bounds;
            double imageY = RandomUtils.random() * 2 * bounds - bounds;

            Point3d pos = new Point3d( imageX, imageY, imageZ );
            Vector3d dir = Vector3d.randomUnit();
            Ray3d ray = new Ray3d( pos, dir );
            dir.z = -Math.abs( dir.z );
            ray = traceLensRay( ray, wavelength );
            if ( ray != null ) {
                raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );
                exitLens++;
                hits++;
            }
        }

        long endTime = System.currentTimeMillis();

        System.out.println( "block: " + ( endTime - startTime ) + "ms" );
        System.out.println( "each: " + ( ( (double) ( endTime - startTime ) ) / ( (double) ( subcount ) ) ) + "ms" );
        System.out.println( "hits / second: " + ( 1000.0 * (double) hits ) / ( (double) ( endTime - startTime ) ) );
        System.out.println( "hits / attempt: " + ( (double) hits ) / ( (double) ( subcount ) ) );
        System.out.println( "lens exit / attempt: " + ( (double) exitLens ) / ( (double) ( subcount ) ) );
        System.out.println();

    }

    public static void main( String[] args ) {
        double imageBounds = 0.25;
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), imageBounds );
        RasterComponent.showRaster( raster );

        int threadCount = 1;
        for ( int i = 0; i < threadCount; i++ ) {
            Thread t = new PureWhite( raster );
            t.start();
        }
    }
}