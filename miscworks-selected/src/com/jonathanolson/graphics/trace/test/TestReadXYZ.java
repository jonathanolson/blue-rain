package com.jonathanolson.graphics.trace.test;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.IOException;

import javax.swing.*;

import com.jonathanolson.graphics.ImagePanel;
import com.jonathanolson.graphics.color.ChromaticAdaptation;
import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.graphics.trace.formats.XYZFormat;
import com.jonathanolson.util.math.vec.Matrix3d;
import com.jonathanolson.util.math.vec.Point3d;

public class TestReadXYZ {
    private int width;
    private int height;

    public float[][][] xdata;
    private float scale = 1000;

    private BufferedImage image;

    public void transfer() {
        int[] cacheRow = new int[width * 3];
        Colorspace srgb = RGBColorspace.sRGB;
        WritableRaster raster = image.getRaster();

        Matrix3d whitewallmatrix = ChromaticAdaptation.construct( ChromaticAdaptation.BRADFORD, StandardIlluminant.E_XYZ, StandardIlluminant.D65_XYZ );
        for ( int i = 0; i < height; i++ ) {
            for ( int j = 0; j < width; j++ ) {
                float[] pixel = xdata[i][j];

                Point3d color = new Point3d( new double[]{pixel[0] / scale, pixel[1] / scale, pixel[2] / scale} );
                whitewallmatrix.transform( color );
                srgb.fromXYZ( color );
                int r = (int) ( color.getX() * 256 );
                int g = (int) ( color.getY() * 256 );
                int b = (int) ( color.getZ() * 256 );
                if ( r > 255 ) {
                    r = 255;
                }
                if ( r < 0 ) {
                    r = 0;
                }
                if ( g > 255 ) {
                    g = 255;
                }
                if ( g < 0 ) {
                    g = 0;
                }
                if ( b > 255 ) {
                    b = 255;
                }
                if ( b < 0 ) {
                    b = 0;
                }
                cacheRow[j * 3] = r;
                cacheRow[j * 3 + 1] = g;
                cacheRow[j * 3 + 2] = b;
                //raster.setPixel( j, i, new int[]{r, g, b} );
            }
            raster.setPixels( 0, i, width, 1, cacheRow );
        }
    }

    public TestReadXYZ( String filename ) {
        try {
            xdata = XYZFormat.readXYZ( filename );
            width = xdata[0].length;
            height = xdata.length;
            System.out.println( width + "x" + height );

            image = new BufferedImage( width, height, BufferedImage.TYPE_INT_RGB );
            Colorspace srgb = RGBColorspace.sRGB;


            transfer();


            JFrame frame = new JFrame( "Test" );
            frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

            JPanel panel = new JPanel( new FlowLayout() );
            frame.setContentPane( panel );

            final ImagePanel imagePanel = new ImagePanel( image );
            panel.add( imagePanel );

            JButton darker = new JButton( "Darker" );
            darker.addActionListener( new ActionListener() {
                @Override
                public void actionPerformed( ActionEvent e ) {
                    scale *= 1.5;
                    transfer();
                    imagePanel.repaint();
                }
            } );
            panel.add( darker );

            JButton lighter = new JButton( "Lighter" );
            lighter.addActionListener( new ActionListener() {
                @Override
                public void actionPerformed( ActionEvent e ) {
                    scale /= 1.5;
                    transfer();
                    imagePanel.repaint();
                }
            } );
            panel.add( lighter );

            frame.pack();
            frame.setVisible( true );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }


    public static void main( String[] args ) {
        //final String filename = "/home/jon/research/ctrace/v/box3a-test-1024.xyz";
        final String filename = "/home/olson/jon/dev/projects/miscworks/svn/trunk/old/box3a-test.xyz";
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                new TestReadXYZ( filename );
            }
        } );

    }
}
