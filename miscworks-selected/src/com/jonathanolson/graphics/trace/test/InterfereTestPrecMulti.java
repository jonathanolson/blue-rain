package com.jonathanolson.graphics.trace.test;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.graphics.trace.util.RasterThread;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Vector3d;

public class InterfereTestPrecMulti extends RasterThread {

    private Raster3 raster;

    private double[][][][] datas;

    private long hits = 0;

    private static final int NUM_SAMPLES = 1;
    private static final double[] samples = new double[NUM_SAMPLES];

    private static final int NUM_FREQS = 100;
    private static final double[] wavelengths = new double[NUM_FREQS];

    private static final double[][] xyzs = new double[NUM_FREQS][3];

    private static final double angfreqs[] = new double[NUM_FREQS];
    private static final double kmags[] = new double[NUM_FREQS];

    static {
        for ( int i = 0; i < NUM_SAMPLES; i++ ) {
            samples[i] = RandomUtils.random() * 365;

            // override
            samples[0] = 55.4348951467877;
            System.out.println( "sample: " + samples[i] );
        }

//        wavelengths[0] = 700;
//        wavelengths[1] = 546.1;
//        wavelengths[2] = 435.8;
//        wavelengths[3] = 789.2;
//        wavelengths[4] = 657.211;
//        wavelengths[5] = 592.93;

//        wavelengths[0] = 789.2;
//        wavelengths[1] = 700;
//        wavelengths[2] = 657.211;
//        wavelengths[3] = 592.93;
        //wavelengths[4] = 546.1;
        //wavelengths[5] = 435.8;

        for ( int i = 0; i < NUM_FREQS; i++ ) {
            wavelengths[i] = 380 + ( (double) ( 830 - 380 ) ) * ( (double) ( i ) / ( (double) ( NUM_FREQS - 1 ) ) );
        }

        for ( int i = 0; i < NUM_FREQS; i++ ) {
            xyzs[i] = XYZResponse.sample2deg( wavelengths[i] );
            double wavelengthMeters = wavelengths[i] * 0.000000001;
            angfreqs[i] = 2.0 * Math.PI * 299792456.2 / wavelengthMeters;
            kmags[i] = 2.0 * Math.PI / wavelengthMeters;
        }
    }

    public InterfereTestPrecMulti( Raster3 raster ) {
        this.raster = raster;
        datas = new double[raster.getWidth()][raster.getHeight()][NUM_SAMPLES][NUM_FREQS];

    }

    @Override
    public void render() {
        //double wavelength = 534;
        //double wavelengthMeters = wavelength * 0.000000001;
        //double angfreq = 2.0 * Math.PI * 299792456.2 / wavelengthMeters;

        //double kmag = 2.0 * Math.PI / wavelengthMeters;

        for ( int i = 0; i < 1000000; i++ ) {
            Vector3d dir = Vector3d.randomCone( 0.99999 );
            double pathLen = 0;

            for ( int x = 0; x < raster.getWidth(); x++ ) {
                for ( int y = 0; y < raster.getHeight(); y++ ) {
                    double xd = raster.reverseMapX( x );
                    double yd = raster.reverseMapY( y );
                    // TODO: handle meters, centimeters, etc. maybe all distances should be meters?

                    Vector3d r = new Vector3d( xd, yd, 0 );

                    double phaseA = ( dir.dot( r ) - pathLen );

                    double[] p = raster.getPixel( x, y );

                    for ( int wi = 0; wi < NUM_FREQS; wi++ ) {
                        double v = 0;

                        for ( int idx = 0; idx < NUM_SAMPLES; idx++ ) {
                            double sample = samples[idx];

                            datas[x][y][idx][wi] += Math.sin( kmags[wi] * phaseA - angfreqs[wi] * sample / 10 );
                            v += datas[x][y][idx][wi] * datas[x][y][idx][wi];

                        }

                        p[0] += v * xyzs[wi][0];
                        p[1] += v * xyzs[wi][1];
                        p[2] += v * xyzs[wi][2];
                    }

                }
            }

            hits += 1;

            System.out.println( "hits: " + hits );

        }
    }

    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), 0.0005 );
        RasterComponent.showRaster( raster );

        Thread t = new InterfereTestPrecMulti( raster );
        t.start();


    }
}