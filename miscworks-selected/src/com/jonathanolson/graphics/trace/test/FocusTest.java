package com.jonathanolson.graphics.trace.test;

import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class FocusTest {
    private LensDesign design;
    private double wavelength;

    public FocusTest( LensDesign design ) {
        this.design = design;
        wavelength = 534;
        double z = -10000;
        System.out.println( "z: " + z );
        Point3d pos = new Point3d( 0, 0, z );
        for ( double dx = 0; dx < 1; dx += Math.abs( 0.001 / z ) ) {
            Ray3d ray = new Ray3d( pos, new Vector3d( dx, 0, 1, true ) );
            ray = result( ray );
            if ( ray == null ) {
                System.out.println( "NULL" );
                break;
            }
            double t = -ray.getPos().x / ray.getDir().x;
            double extra = t * ray.getDir().z;
            System.out.println( "t: " + t + ", extra: " + extra );
        }
    }

    public static double focusTest( LensDesign design, double z, double wavelength ) {
        Point3d pos = new Point3d( 0, 0, z );
        for ( double dx = 0; dx < 1; dx += Math.abs( 0.001 / z ) ) {
            Ray3d ray = new Ray3d( pos, new Vector3d( dx, 0, 1, true ) );
            for ( LensElement element : design.getElements() ) {
                ray = element.getTransmittedRay( ray, wavelength );
                if ( ray == null ) {
                    break;
                }
            }
            if ( ray == null ) {
                //System.out.println( "NULL" );
                break;
            }
            double t = -ray.getPos().x / ray.getDir().x;
            double extra = t * ray.getDir().z;
            //System.out.println( "t: " + t + ", extra: " + extra );
            if ( !Double.isNaN( extra ) ) {
                return extra;
            }
        }
        return Double.POSITIVE_INFINITY;
    }

    private Ray3d result( Ray3d ray ) {
        Ray3d ret = ray;
        for ( LensElement element : design.getElements() ) {
            ret = element.getTransmittedRay( ret, wavelength );
            if ( ret == null ) {
                break;
            }
        }
        return ret;
    }

    public static void main( String[] args ) {
        //new FocusTest( new DoubleGaussD() );
        LensDesign design = new DoubleGaussD();
        for ( double z = -1; z > -10000; z -= 1 ) {
            System.out.println( "z: " + z + ", extra: " + focusTest( design, z, 534 ) );
        }
    }
}
