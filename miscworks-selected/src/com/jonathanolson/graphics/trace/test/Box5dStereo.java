package com.jonathanolson.graphics.trace.test;

import javax.swing.*;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.colors.BoxGreen;
import com.jonathanolson.graphics.color.colors.BoxRed;
import com.jonathanolson.graphics.color.colors.BoxWhite;
import com.jonathanolson.graphics.color.colors.SpectralColor;
import com.jonathanolson.graphics.trace.lens.BackwardLensTracer;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.*;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class Box5dStereo extends RasterThread {

    private final Raster3 raster;

    private LensDesign design;
    private double imageZ;
    private double bounds;

    private static final double ratioRadius = 0.5;

    private static final double focusPoint = -600;

    private static final double LEFT_X = -90.0;
    private static final double RIGHT_X = 90.0;
    private static final double TOP_Y = 90.0;
    private static final double BOTTOM_Y = -90.0;
    private static final double FRONT_Z = -100.0;
    private static final double BACK_Z = -1200.0;

    private static final double RADIUS_CHROME = 36.0;
    private static final double RADIUS_GLASS = 36.0;
    private static final double RADIUS_COPPER = 36.0;
    private static final double RADIUS_DIFFUSE = 36.0;
    private static final Point3d CENTER_CHROME = new Point3d( -45.0, -54.0, -615.0 );
    private static final Point3d CENTER_GLASS = new Point3d( 45.0, -54.0, -585.0 );
    private static final Point3d CENTER_COPPER = new Point3d( 45.0, 54.0, -465.0 );
    private static final Point3d CENTER_DIFFUSE = new Point3d( -45.0, 54.0, -435.0 );

    private static final int maxBounceCount = 20;

    private HitList root = new HitList();

    private double offset;

    private BackwardLensTracer lensTracer;

    public Box5dStereo( Raster3 raster, double offset ) {
        this.raster = raster;
        this.offset = offset;

        design = new DoubleGaussD();
        design.focusAt( focusPoint, 534 );
        design.scaleStopRadius( ratioRadius );
        imageZ = design.getImagePlaneZ();

        lensTracer = new BackwardLensTracer( design );

        bounds = raster.getScale();

        root.addHittable( new Floor() );
        root.addHittable( new GlassSphere() );
        root.addHittable( new ChromeSphere() );
        root.addHittable( new CopperSphere() );
        root.addHittable( new DiffuseSphere() );
    }

    private Plane3d topPlane = new Plane3d( new Vector3d( 0, -1, 0 ), -TOP_Y );

    private static class GlassSphere implements Hittable {
        private Sphere3d glassSphere = new Sphere3d( CENTER_GLASS, RADIUS_GLASS );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = glassSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            double na;
            double nb;
            if ( sphereHit.fromOutside ) {
                na = 1;
                nb = SellmeierDispersion.BK7.compute( wavelength );
            }
            else {
                na = SellmeierDispersion.BK7.compute( wavelength );
                nb = 1;
            }
            return new Ray3d( sphereHit.hitPoint, FresnelReflection.getFresnelDir( oldRay.getDir(), sphereHit.normal, na, nb ) );
        }
    }

    private static class ChromeSphere implements Hittable {
        private Sphere3d chromeSphere = new Sphere3d( CENTER_CHROME, RADIUS_CHROME );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = chromeSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside || RandomUtils.random() > FresnelReflection.simpleFresnelDielectric( oldRay.getDir(), sphereHit.normal, 1, SellmeierDispersion.BK7.compute( wavelength ) ) ) {
                return null;
            }
            return new Ray3d( sphereHit.hitPoint, FresnelReflection.reflect( oldRay.getDir(), sphereHit.normal ) );
        }
    }

    private static class CopperSphere implements Hittable {
        private Sphere3d copperSphere = new Sphere3d( CENTER_COPPER, RADIUS_COPPER );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = copperSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside ) {
                return null;
            }
            double n = Copper.getN( wavelength );
            double k = Copper.getK( wavelength );
            double[] rr = FresnelReflection.fresnel( oldRay.getDir(), sphereHit.normal, 1, n, k );
            if ( RandomUtils.random() > ( rr[0] + rr[1] ) / 2 ) {
                return null;
            }
            else {
                return new Ray3d( sphereHit.hitPoint, FresnelReflection.reflect( oldRay.getDir(), sphereHit.normal ) );
            }
        }
    }

    private static class DiffuseSphere implements Hittable {
        private Sphere3d diffuseSphere = new Sphere3d( CENTER_DIFFUSE, RADIUS_DIFFUSE );

        @Override
        public RayHit intersect( Ray3d ray ) {
            Sphere3d.Hit hit = diffuseSphere.intersect( ray );
            return hit;
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            Sphere3d.Hit sphereHit = (Sphere3d.Hit) hit;
            if ( !sphereHit.fromOutside ) {
                return null;
            }
            return new Ray3d( sphereHit.hitPoint, Vector3d.randomProjectedHemisphere( sphereHit.normal ) );
        }
    }

    private static class Floor implements Hittable {

        private Plane3d bottomPlane = new Plane3d( new Vector3d( 0, 1, 0 ), BOTTOM_Y );
        private SpectralColor white = new BoxWhite();
        private SpectralColor green = new BoxGreen();
        private SpectralColor red = new BoxRed();

        @Override
        public RayHit intersect( Ray3d ray ) {
            Plane3d.Hit hit = bottomPlane.intersect( ray );
            if ( hit != null ) {
                return hit;
            }
            else {
                return null;
            }
        }

        @Override
        public Ray3d process( Ray3d oldRay, RayHit hit, double wavelength ) {
            SpectralColor color;
            double sc = 15.0;
            boolean xx = hit.getHitPoint().x / sc - Math.floor( hit.getHitPoint().x / sc ) < 0.5;
            boolean yy = hit.getHitPoint().z / sc - Math.floor( hit.getHitPoint().z / sc ) < 0.5;
            if ( xx == yy ) {
                color = red;
            }
            else if ( xx ) {
                color = green;
            }
            else {
                color = white;
            }
            if ( RandomUtils.random() > color.reflection( wavelength ) ) {
                return null;
            }
            else {
                return new Ray3d( hit.getHitPoint(), Vector3d.randomProjectedHemisphere( hit.getNormal() ) );
            }
        }
    }

    @Override
    public void render() {
        int subcount = 10000000;
        int hits = 0;
        int exitLens = 0;
        int c = 0;
        long startTime = System.currentTimeMillis();
        while ( c++ < subcount ) {
            double wavelength = RandomUtils.random() * 300 + 400;

            double imageX = RandomUtils.random() * 2 * bounds - bounds;
            double imageY = RandomUtils.random() * 2 * bounds - bounds;

            Point3d pos = new Point3d( imageX, imageY, imageZ );
            Vector3d dir = Vector3d.randomUnit();
            Ray3d ray = new Ray3d( pos, dir );
            dir.z = -Math.abs( dir.z );
            ray = lensTracer.transmitRay( ray, wavelength );
            if ( ray != null ) {
                ray.getPos().x += offset;
                exitLens++;
                boolean absorb = false;
                int bounceCount = 0;
                while ( bounceCount++ < maxBounceCount ) {

                    RayHit hit = root.intersect( ray );

                    if ( hit == null ) {
                        break;
                    }

                    ray = root.process( ray, hit, wavelength );

                    if ( ray == null ) {
                        absorb = true;
                        break;
                    }

                    if ( bounceCount == maxBounceCount ) {
                        absorb = true;
                        break;
                    }
                }

                if ( !absorb ) {
                    hits++;
                    raster.addXYZAt( new Point2d( imageX, -imageY ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) * dir.getZ() );
                }

            }
        }

        long endTime = System.currentTimeMillis();

        System.out.println( "block: " + ( endTime - startTime ) + "ms" );
        System.out.println( "each: " + ( ( (double) ( endTime - startTime ) ) / ( (double) ( subcount ) ) ) + "ms" );
        System.out.println( "hits / second: " + ( 1000.0 * (double) hits ) / ( (double) ( endTime - startTime ) ) );
        System.out.println( "hits / attempt: " + ( (double) hits ) / ( (double) ( subcount ) ) );
        System.out.println( "lens exit / attempt: " + ( (double) exitLens ) / ( (double) ( subcount ) ) );
        System.out.println();
        System.out.println();

    }

    public static void main( String[] args ) {
        double imageBounds = 0.25;
        final Raster3 leftRaster = new Raster3( 512, 512, new Point2d( 0, 0 ), imageBounds );
        final Raster3 rightRaster = new Raster3( 512, 512, new Point2d( 0, 0 ), imageBounds );
        JFrame frame = new JFrame( "Rasters" );

        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        JPanel panel = new JPanel();
        frame.setContentPane( panel );

        RasterComponent leftRasterComponent = new RasterComponent( leftRaster );
        RasterComponent rightRasterComponent = new RasterComponent( rightRaster );
        frame.add( leftRasterComponent );
        frame.add( rightRasterComponent );

        leftRasterComponent.transfer();
        rightRasterComponent.transfer();

        frame.pack();
        frame.setVisible( true );

        Thread leftThread = new Box5dStereo( leftRaster, 2 );
        Thread rightThread = new Box5dStereo( rightRaster, -2 );

        leftThread.start();
        rightThread.start();

        leftRasterComponent.startUpdates();
        rightRasterComponent.startUpdates();
    }
}