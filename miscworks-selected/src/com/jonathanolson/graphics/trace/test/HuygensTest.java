package com.jonathanolson.graphics.trace.test;

import java.util.ArrayList;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.graphics.trace.lens.*;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.*;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class HuygensTest extends RasterThread {

    private Raster3 raster;

    private LensDesign design;
    private static final double focusPoint = -600;
    private static final double ratioRadius = 0.5;
    private double imageZ;

    private long hits = 0;

    private ArrayList<Vector3d> unitDirs = new ArrayList<Vector3d>();
    private ArrayList<Double> opls = new ArrayList<Double>();

    public HuygensTest( Raster3 raster ) {
        this.raster = raster;

        design = new DoubleGaussD();
        design.focusAt( focusPoint, 534 );
        design.scaleStopRadius( ratioRadius );
        imageZ = design.getImagePlaneZ();

    }

    @Override
    public void render() {
        double wavelength = 534;
        double wavelengthMeters = 534 * 0.000000001;
        double[] xyz = XYZResponse.sample2deg( wavelength );

        double kmag = 2.0 * Math.PI / wavelengthMeters;

        for ( int i = 0; i < 1000000; i++ ) {
            double opticalPathLength = 0;

            Point3d pos = new Point3d( 0, 0, -600 );
            Vector3d dir = Vector3d.randomCone( 0.99999 );
            dir.z = Math.abs( dir.z );
            Ray3d ray = new Ray3d( pos, dir );
            Ray3d startRay = ray;
            for ( LensElement element : design.getElements() ) {
                Ray3d newRay;
                double n = 1;
                double distance = 0;
                if ( element instanceof SphericalLensSurface ) {
                    SphericalLensSurface e = (SphericalLensSurface) element;

                    LensSurfaceHit hit = e.intersect( ray );
                    if ( hit == null ) {
                        newRay = null;
                    }
                    else {
                        Vector3d normal = new Vector3d( hit.normal );
                        double na = SphericalLensSurface.getIndexOfRefraction( hit.fromMaterial, wavelength );
                        double nb = SphericalLensSurface.getIndexOfRefraction( hit.toMaterial, wavelength );
                        if ( FresnelReflection.isTotalInternalReflection( ray.getDir(), normal, na, nb ) ) {
                            newRay = null;
                        }
                        else {
                            Vector3d outgoing = FresnelReflection.transmit( ray.getDir(), normal, na, nb );
                            newRay = new Ray3d( hit.hitPoint, outgoing );
                        }

                        n = na;
                        distance = hit.getDistance();
                    }
                }
                else if ( element instanceof FlatLensSurface ) {
                    FlatLensSurface e = (FlatLensSurface) element;

                    Plane3d.Hit hit = e.getPlane().intersect( ray );
                    if ( hit == null ) {
                        newRay = null;
                    }
                    else {
                        double na;
                        double nb;
                        Vector3d normal;
                        if ( ray.getDir().z > 0 ) {
                            na = e.getNOut( wavelength );
                            nb = e.getNIn( wavelength );
                            normal = new Vector3d( 0, 0, -1 );
                        }
                        else {
                            na = e.getNIn( wavelength );
                            nb = e.getNOut( wavelength );
                            normal = new Vector3d( 0, 0, 1 );
                        }
                        n = na;
                        if ( FresnelReflection.isTotalInternalReflection( ray.getDir(), normal, na, nb ) ) {
                            newRay = null;
                        }
                        else {
                            Vector3d outgoing = FresnelReflection.transmit( ray.getDir(), normal, na, nb );
                            newRay = new Ray3d( hit.hitPoint, outgoing );
                        }
                        distance = hit.getDistance();
                    }

                    // TODO: get lens elements to include report of distance, IORs, etc.
                }
                else {
                    newRay = element.getTransmittedRay( ray, wavelength );
                    n = 1.00;
                    if ( newRay != null ) {
                        distance = ray.getPos().distance( newRay.getPos() );
                    }
                }
                if ( newRay == null ) {
                    ray = newRay;
                    break;
                }

                opticalPathLength += n * distance;

                ray = newRay;
            }
            if ( ray != null ) {
                Vector3d kvec = ray.getDir();
                double pathLen = opticalPathLength;

                hits++;
                //raster.addXYZAt( new Point2d( ray.getPos().x, -ray.getPos().y ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );

                if ( hits > 200 ) {
                    continue;
                }
                else {
                    System.out.println( "hits: " + hits );
                }

                int siz = unitDirs.size();
                double zd = imageZ;

                for ( int x = 0; x < raster.getWidth(); x++ ) {
                    for ( int y = 0; y < raster.getHeight(); y++ ) {
                        double xd = raster.reverseMapX( x );
                        double yd = raster.reverseMapY( y );
                        // TODO: handle meters, centimeters, etc. maybe all distances should be meters?

                        Vector3d r = new Vector3d( xd, yd, zd );

                        double v = 0.5;

                        double phaseA = ( kvec.dot( r ) - pathLen );

                        for ( int idx = 0; idx < siz; idx++ ) {
                            v += Math.cos( kmag * ( phaseA - ( unitDirs.get( idx ).dot( r ) - opls.get( idx ) ) ) );
                        }

                        double[] p = raster.getPixel( x, y );

                        p[0] += v * xyz[0];
                        p[1] += v * xyz[1];
                        p[2] += v * xyz[2];

                    }
                }

                //System.out.println( "." );


                unitDirs.add( kvec );
                opls.add( pathLen );
            }
        }
        //System.out.println( "---" );
        //System.out.println( "sent: " + sent );
        //System.out.println( "hit 1: " + hitFirstSurface );
    }

    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 512, 512, new Point2d( 0, 0 ), 0.0001 );
        RasterComponent.showRaster( raster );

        Thread t = new HuygensTest( raster );
        t.start();


    }
}
