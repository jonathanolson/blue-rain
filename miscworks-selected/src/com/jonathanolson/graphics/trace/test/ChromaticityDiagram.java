package com.jonathanolson.graphics.trace.test;

import java.awt.*;
import java.awt.color.ColorSpace;

import edu.umd.cs.piccolo.PCanvas;
import edu.umd.cs.piccolo.nodes.PPath;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.graphics.trace.util.ColorUtils;
import com.jonathanolson.util.SwingUtils;

public class ChromaticityDiagram extends PCanvas {
    private double multiplier;

    public ChromaticityDiagram() {
        this( 256 );
    }

    public ChromaticityDiagram( double multiplier ) {
        this.multiplier = multiplier;

        setPreferredSize( new Dimension( (int) multiplier, (int) multiplier ) );

        for ( double wavelength = 400; wavelength <= 700; wavelength += 1 ) {
            Color c = ColorUtils.fromXYZ( XYZResponse.sample2deg( wavelength ) );
            displayColor( c, 1, false );
        }

        ColorSpace sRGB = ColorUtils.getsRGB();
        for ( float x = 1; x >= 0; x -= 0.01 ) {
            displayColor( new Color( sRGB, new float[]{x, 1, 0}, 1f ), 1, false );
            displayColor( new Color( sRGB, new float[]{1, x, 0}, 1f ), 1, false );
            displayColor( new Color( sRGB, new float[]{x, 0, 1}, 1f ), 1, false );
            displayColor( new Color( sRGB, new float[]{1, 0, x}, 1f ), 1, false );
            displayColor( new Color( sRGB, new float[]{0, x, 1}, 1f ), 1, false );
            displayColor( new Color( sRGB, new float[]{0, 1, x}, 1f ), 1, false );
        }
    }

    public void displayColor( Color c ) {
        displayColor( c, 10 );
    }

    public void displayColor( Color c, float rad ) {
        displayColor( c, rad, true );
    }

    public void displayColor( Color c, float rad, boolean border ) {
        float[] chroma = ColorUtils.chromaticity( c );
        PPath circle = PPath.createEllipse( (float) ( chroma[0] * multiplier ), (float) ( ( 1 - chroma[1] ) * multiplier ), rad, rad );
        if ( !border ) {
            circle.setStrokePaint( c );
        }
        circle.setPaint( c );
        circle.setOffset( -rad / 2, -rad / 2 );
        getLayer().addChild( circle );
    }

    private static void debugColor( Color c ) {
        float[] xyz = ColorUtils.toXYZ( c );
        System.out.println( "RGB: " + c );
        System.out.println( "XYZ: " + xyz[0] + ", " + xyz[1] + ", " + xyz[2] );
    }

    public static void main( String[] args ) {
        ChromaticityDiagram test = new ChromaticityDiagram( 256 );
        test.displayColor( Color.BLUE );
        test.displayColor( Color.CYAN );
        test.displayColor( Color.MAGENTA );
        test.displayColor( Color.GREEN );
        test.displayColor( Color.RED );
        test.displayColor( Color.GRAY );
        test.displayColor( Color.ORANGE );
        test.displayColor( Color.PINK );
        test.displayColor( Color.YELLOW );
        SwingUtils.popupFrame( "Chromaticity test", test );
    }
}
