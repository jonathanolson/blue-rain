package com.jonathanolson.graphics.trace.test;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.SimpleLensImage;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.Plane3d;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class NaiveReverseSquareArrayTest {

    private static final double imageBounds = 0.5;

    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), imageBounds );
        RasterComponent.showRaster( raster );

        Thread t = new Thread() {
            @Override
            public void run() {
                LensDesign design = new DoubleGaussD();
                List<LensElement> elements = new LinkedList<LensElement>();
                double imageZ = 0;
                for ( LensElement element : design.getElements() ) {
                    if ( element instanceof SimpleLensImage ) {
                        SimpleLensImage image = (SimpleLensImage) element;
                        imageZ = image.getCenter().z + 0.08490345743192626;
                        image.setCenter( new Point3d( 0, 0, image.getCenter().z + 0.08490345743192626 ) );
                    }
                    else {
                        elements.add( 0, element );
                    }
                }
                Plane3d checkerPlane = new Plane3d( new Vector3d( 0, 0, 1 ), -10 );
                while ( true ) {
                    double wavelength = RandomUtils.random() * 300 + 400;

                    double imageX = RandomUtils.random() * 2 * imageBounds - imageBounds;
                    double imageY = RandomUtils.random() * 2 * imageBounds - imageBounds;

                    Point3d pos = new Point3d( imageX, imageY, imageZ );
                    Vector3d dir = Vector3d.randomUnit();
                    Ray3d ray = new Ray3d( pos, dir );
                    dir.z = -Math.abs( dir.z );
                    for ( LensElement element : elements ) {
                        ray = element.getTransmittedRay( ray, wavelength );
                        if ( ray == null ) {
                            break;
                        }
                    }
                    if ( ray != null ) {
                        Point3d checkerpoint = checkerPlane.intersect( ray ).hitPoint;
                        double x = checkerpoint.x;
                        double y = checkerpoint.y;
                        boolean xx = ( (int) ( x * 2 + 100 ) ) % 2 == 0;
                        boolean yy = ( (int) ( y * 2 + 100 ) ) % 2 == 0;

                        // test to see where it is
                        if ( xx == yy ) {
                            raster.addXYZAt( new Point2d( -imageX, -imageY ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );
                        }
                    }
                }
            }
        };

        t.start();
    }
}