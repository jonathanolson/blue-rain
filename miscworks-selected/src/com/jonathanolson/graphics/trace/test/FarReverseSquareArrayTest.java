package com.jonathanolson.graphics.trace.test;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.SimpleLensImage;
import com.jonathanolson.graphics.trace.lens.SimpleLensStop;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.Plane3d;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.graphics.trace.util.RasterThread;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class FarReverseSquareArrayTest {

    public static final double imageBounds = 0.025;
    public static final double ratioRadius = 0.5;

    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 1024, 1024, new Point2d( 0, 0 ), imageBounds );
        RasterComponent.showRaster( raster );

        int threadCount = 2;
        for ( int i = 0; i < threadCount; i++ ) {
            Thread t = new RunThread( raster );
            t.start();
        }
    }

    public static class RunThread extends RasterThread {
        private final Raster3 raster;
        private LensDesign design;
        private List<LensElement> elements;
        private double imageZ;
        private double checkerZ;
        private double imageZOffset;
        private Plane3d checkerPlane;

        public RunThread( Raster3 raster ) {
            this.raster = raster;
            design = new DoubleGaussD();
            elements = new LinkedList<LensElement>();
            imageZ = 0;
            checkerZ = -600;
            imageZOffset = FocusTest.focusTest( design, checkerZ, 534 );
            System.out.println( "detected image plane offset: " + imageZOffset );

            for ( LensElement element : design.getElements() ) {
                if ( element instanceof SimpleLensImage ) {
                    SimpleLensImage image = (SimpleLensImage) element;
                    imageZ = image.getCenter().z + imageZOffset;
                    image.setCenter( new Point3d( 0, 0, image.getCenter().z + imageZOffset ) );
                }
                else if ( element instanceof SimpleLensStop ) {
                    // TODO: stop modifying actual elemens, we need to modify a COPY of the element!
                    SimpleLensStop stop = (SimpleLensStop) element;
                    stop.setRadius( stop.getRadius() * ratioRadius );
                    elements.add( 0, stop );
                }
                else {
                    elements.add( 0, element );
                }
            }

            checkerPlane = new Plane3d( new Vector3d( 0, 0, 1 ), checkerZ );
        }

        public void render() {
            int subcount = 1000;
            int c = 0;
            while ( c++ < subcount ) {
                double wavelength = RandomUtils.random() * 300 + 400;

                double imageX = RandomUtils.random() * 2 * imageBounds - imageBounds;
                double imageY = RandomUtils.random() * 2 * imageBounds - imageBounds;

                Point3d pos = new Point3d( imageX, imageY, imageZ );
                Vector3d dir = Vector3d.randomUnit();
                Ray3d ray = new Ray3d( pos, dir );
                dir.z = -Math.abs( dir.z );
                for ( LensElement element : elements ) {
                    ray = element.getTransmittedRay( ray, wavelength );
                    if ( ray == null ) {
                        break;
                    }
                }
                if ( ray != null ) {
                    Point3d checkerpoint = checkerPlane.intersect( ray ).hitPoint;
                    double x = checkerpoint.x;
                    double y = checkerpoint.y;
                    boolean xx = ( (int) ( x * 2 + 100 ) ) % 2 == 0;
                    boolean yy = ( (int) ( y * 2 + 100 ) ) % 2 == 0;

                    // test to see where it is
                    if ( xx == yy ) {
                        raster.addXYZAt( new Point2d( -imageX, -imageY ), wavelength, StandardIlluminant.D65_SPECTRAL( wavelength ) );
                    }
                }
            }
        }
    }
}