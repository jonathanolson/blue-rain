package com.jonathanolson.graphics.trace.test;

import java.util.ArrayList;

import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.graphics.trace.ui.RasterComponent;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.graphics.trace.util.RasterThread;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Vector3d;

public class InterfereTest extends RasterThread {

    private Raster3 raster;

    private ArrayList<Vector3d> unitDirs = new ArrayList<Vector3d>();
    private ArrayList<Double> opls = new ArrayList<Double>();

    private long hits = 0;

    public InterfereTest( Raster3 raster ) {
        this.raster = raster;
    }

    @Override
    public void render() {
        double wavelength = 534;
        double wavelengthMeters = wavelength * 0.000000001;
        double[] xyz = XYZResponse.sample2deg( wavelength );

        double kmag = 2.0 * Math.PI / wavelengthMeters;

        for ( int i = 0; i < 1000000; i++ ) {
            Vector3d dir = Vector3d.randomCone( 0.99999 );
            double pathLen = 0;

            int siz = unitDirs.size();

            for ( int x = 0; x < raster.getWidth(); x++ ) {
                for ( int y = 0; y < raster.getHeight(); y++ ) {
                    double xd = raster.reverseMapX( x );
                    double yd = raster.reverseMapY( y );
                    // TODO: handle meters, centimeters, etc. maybe all distances should be meters?

                    Vector3d r = new Vector3d( xd, yd, 0 );

                    double v = 0.5;

                    double phaseA = ( dir.dot( r ) - pathLen );

                    for ( int idx = 0; idx < siz; idx++ ) {
                        v += Math.cos( kmag * ( phaseA - ( unitDirs.get( idx ).dot( r ) - opls.get( idx ) ) ) );
                    }

                    double[] p = raster.getPixel( x, y );

                    p[0] += v * xyz[0];
                    p[1] += v * xyz[1];
                    p[2] += v * xyz[2];

                }
            }


            unitDirs.add( dir );
            opls.add( pathLen );

            hits += 1;

            System.out.println( "hits: " + hits );

        }
    }

    public static void main( String[] args ) {
        final Raster3 raster = new Raster3( 256, 256, new Point2d( 0, 0 ), 0.0005 );
        RasterComponent.showRaster( raster );

        Thread t = new InterfereTest( raster );
        t.start();


    }
}