package com.jonathanolson.graphics.trace.lens.designs;

import com.jonathanolson.graphics.trace.lens.ImageEntry;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensEntry;
import com.jonathanolson.graphics.trace.lens.LensMaterial;
import com.jonathanolson.graphics.trace.util.SellmeierDispersion;

public class DoubleGaussA extends LensDesign {
    // from doublegauss1
    public DoubleGaussA() {
        addEntry( new LensEntry( 0.1258, 0.960, 0.9377, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.0150, 0.960, 2.3033, null ) );
        addEntry( new LensEntry( 0.1185, 0.760, 0.5241, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.0783, 0.760, 0.9235, new LensMaterial( SellmeierDispersion.SF1 ) ) );
        addEntry( new LensEntry( 0.1466, 0.520, 0.3714, null ) );
        addEntry( new LensEntry( 0.1109, 0.368 ) );
        addEntry( new LensEntry( 0.0600, 0.500, -0.6206, new LensMaterial( SellmeierDispersion.F5 ) ) );
        addEntry( new LensEntry( 0.1311, 0.680, 1.5225, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.0175, 0.680, -0.9137, null ) );
        addEntry( new LensEntry( 0.2956, 0.860, 3.6353, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.7500, 0.860, -2.0103, null ) );
        addEntry( new ImageEntry() );
    }
}
