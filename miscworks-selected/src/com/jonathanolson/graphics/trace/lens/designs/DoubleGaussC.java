package com.jonathanolson.graphics.trace.lens.designs;

import java.awt.*;

import com.jonathanolson.graphics.trace.lens.ImageEntry;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensEntry;
import com.jonathanolson.graphics.trace.lens.LensMaterial;
import com.jonathanolson.graphics.trace.test.DisplayLens;
import com.jonathanolson.graphics.trace.util.SellmeierDispersion;
import com.jonathanolson.util.SwingUtils;
import com.jonathanolson.util.math.vec.Point3d;

public class DoubleGaussC extends LensDesign {
    // TODO: test?
    // from Laikin Lens Design: 6.3 (4.0 EFL f/1 lens)
    public DoubleGaussC() {
        addEntry( new LensEntry( 1.399, 4.43, 3.5519, new LensMaterial( SellmeierDispersion.LAF2 ) ) );
        addEntry( new LensEntry( 0.009, 4.43, 0, null ) );
        addEntry( new LensEntry( 0.939, 2.94, 1.7043, new LensMaterial( SellmeierDispersion.PSK3 ) ) );
        addEntry( new LensEntry( 0.252, 2.94, -16.2381, new LensMaterial( SellmeierDispersion.SF1 ) ) );
        addEntry( new LensEntry( 0.434, 1.62, 0.9860, null ) );
        addEntry( new LensEntry( 0.278, 1.60 ) );
        addEntry( new LensEntry( 0.270, 1.62, -1.6424, new LensMaterial( SellmeierDispersion.SF1 ) ) );
        addEntry( new LensEntry( 1.159, 2.05, 1.8028, new LensMaterial( SellmeierDispersion.LAF2 ) ) );
        addEntry( new LensEntry( 0.447, 2.05, -2.3336, null ) );
        addEntry( new LensEntry( 1.044, 1.86, 1.6593, new LensMaterial( SellmeierDispersion.LAF2 ) ) );
        addEntry( new LensEntry( 0.813, 1.86, 14.4112, null ) );
        addEntry( new ImageEntry() );
    }

    public static void main( String[] args ) {
        double z = -20;

        DisplayLens test = new DisplayLens( new DoubleGaussC(), 100, 1 );

        float alpha = 0.05f;


        test.drawRandomForwardRays( new Point3d( 0, 0, z ), 200000, 435, new Color( 0, 0, 1, alpha ) );
        test.drawRandomForwardRays( new Point3d( 0, 2, z ), 200000, 700, new Color( 1, 0, 0, alpha ) );
        test.drawRandomForwardRays( new Point3d( 0, 4, z ), 200000, 546, new Color( 0, 1, 0, alpha ) );

        test.drawRandomForwardRays( new Point3d( 0, -40, -200 ), 20000000, 546, new Color( 0, 0, 0, alpha ) );

        SwingUtils.popupFrame( "Lens test", test );
    }
}