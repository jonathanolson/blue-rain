package com.jonathanolson.graphics.trace.lens.designs;

import java.awt.*;

import com.jonathanolson.graphics.trace.lens.ImageEntry;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensEntry;
import com.jonathanolson.graphics.trace.test.DisplayLens;
import com.jonathanolson.util.SwingUtils;
import com.jonathanolson.util.math.vec.Point3d;

import static com.jonathanolson.graphics.trace.util.SellmeierDispersion.*;

public class DoubleGaussD extends LensDesign {
    // from Laikin Lens Design: 6.4 (25mm F/0.85 double gauss lens)
    // focuses at z = -600.0 (approx)
    public DoubleGaussD() {
        addEntry( new LensEntry( 0.5001, 2.280, -2.83155, glass( LASF36A ) ) );
        addEntry( new LensEntry( 0.0700, 2.420, -2.19063, null ) );
        addEntry( new LensEntry( 0.3494, 1.880, 1.97886, glass( LASF3 ) ) );
        addEntry( new LensEntry( 0.1067, 1.880, -10.65614, null ) );
        addEntry( new LensEntry( 0.0805, 1.660, -2.75248, glass( SF6 ) ) );
        addEntry( new LensEntry( 0.0100, 1.880, -21.99313, null ) );
        addEntry( new LensEntry( 0.2000, 1.220, 0.69612, glass( LASF3 ) ) );
        addEntry( new LensEntry( 0.0806, 0.980, 0.67653, glass( SF6 ) ) );
        addEntry( new LensEntry( 0.3260, 0.810, 0.46644, null ) );
        addEntry( new LensEntry( 0.1256, 0.679 ) );
        addEntry( new LensEntry( 0.0507, 0.700, -0.60866, glass( SF57 ) ) );
        addEntry( new LensEntry( 0.4156, 1.060, 0.81321, glass( LASFN31 ) ) );
        addEntry( new LensEntry( 0.0105, 1.060, -0.91272, null ) );
        addEntry( new LensEntry( 0.2367, 1.240, 1.55910, glass( LASFN31 ) ) );
        addEntry( new LensEntry( 0.0104, 1.240, -6.18794, null ) );
        addEntry( new LensEntry( 0.3699, 1.120, 0.90347, glass( LASF3 ) ) );
        addEntry( new LensEntry( 0.3431, 0.840, 1.99074, null ) );
        addEntry( new ImageEntry() );
    }

    public static void main( String[] args ) {
        double z = -20;

        DisplayLens test = new DisplayLens( new DoubleGaussD(), 256, 1 );

        float alpha = 0.2f;


        test.drawRandomForwardRays( new Point3d( 0, 0, z ), 200000, 435, new Color( 0, 0, 1, alpha ) );
        test.drawRandomForwardRays( new Point3d( 0, 2, z ), 200000, 700, new Color( 1, 0, 0, alpha ) );
        test.drawRandomForwardRays( new Point3d( 0, 4, z ), 200000, 546, new Color( 0, 1, 0, alpha ) );

        test.drawRandomForwardRays( new Point3d( 0, -40, -200 ), 20000000, 546, new Color( 0, 0, 0, alpha ) );

        SwingUtils.popupFrame( "Lens test", test );
    }
}