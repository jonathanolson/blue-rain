package com.jonathanolson.graphics.trace.lens.designs;

import com.jonathanolson.graphics.trace.lens.ImageEntry;
import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensEntry;
import com.jonathanolson.graphics.trace.lens.LensMaterial;
import com.jonathanolson.graphics.trace.util.SellmeierDispersion;

public class DoubleGaussB extends LensDesign {
    // from doublegauss2
    public DoubleGaussB() {
        addEntry( new LensEntry( 0.1746, 1.480, 1.3517, new LensMaterial( SellmeierDispersion.N_BASF2 ) ) );
        addEntry( new LensEntry( 0.0150, 1.420, 3.1073, null ) );
        addEntry( new LensEntry( 0.2934, 1.260, 1.0848, new LensMaterial( SellmeierDispersion.N_LAK8 ) ) );
        addEntry( new LensEntry( 0.0800, 1.260, 23.3464, new LensMaterial( SellmeierDispersion.SF2 ) ) );
        addEntry( new LensEntry( 0.2770, 0.900, 0.6617, null ) );
        addEntry( new LensEntry( 0.3652, 0.772 ) );
        addEntry( new LensEntry( 0.0800, 0.940, -0.6679, new LensMaterial( SellmeierDispersion.SF2 ) ) );
        addEntry( new LensEntry( 0.2911, 1.260, 2.7336, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.0150, 1.260, -1.0096, null ) );
        addEntry( new LensEntry( 0.2358, 1.520, 0, new LensMaterial( SellmeierDispersion.N_LAK33 ) ) );
        addEntry( new LensEntry( 0.0150, 1.520, -2.3087, null ) );
        addEntry( new LensEntry( 0.1318, 1.620, 3.1206, new LensMaterial( SellmeierDispersion.N_LAK8 ) ) );
        addEntry( new LensEntry( 1.3006, 1.620, 27.5356, null ) );
        addEntry( new ImageEntry() );
    }
}