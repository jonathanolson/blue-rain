package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.FresnelReflection;
import com.jonathanolson.graphics.trace.util.Numbers;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class SphericalLensSurface implements LensSurface {
    /**
     * Center point of the logical sphere
     */
    private Point3d center;

    /**
     * Unit vector representing the direction from the center of the logical sphere to the center of the actual surface
     */
    private Vector3d dir;

    /**
     * Mindot is the dot-product of the dir vector and the center => top/bottom edge of lens vector. This represents the
     * smallest dot-product of a vector from the center to a point on the logical sphere that is ON the lens (and not
     * clipped by the diameter, or behind)
     */
    private double minDot;

    /**
     * The top-to-bottom length of the lens
     */
    private double diameter;

    /**
     * The radius of the logical sphere
     */
    private double radius;

    private LensMaterial outMaterial;

    private LensMaterial inMaterial;

    // TODO: add material properties?

    // TODO: add extras like thin films, etc

    public SphericalLensSurface( Point3d center, Vector3d dir, double diameter, double radius ) {
        assert ( diameter <= Math.abs( radius ) * 2 );
        this.center = center;
        this.dir = dir;
        this.diameter = diameter;
        this.radius = radius;
        minDot = Math.sqrt( 1 - ( diameter / ( 2 * radius ) ) * ( diameter / ( 2 * radius ) ) );
    }

    public SphericalLensSurface( Point3d center, Vector3d dir, double diameter, double radius, LensMaterial outMaterial, LensMaterial inMaterial ) {
        this( center, dir, diameter, radius );
        this.outMaterial = outMaterial;
        this.inMaterial = inMaterial;
    }

    @Override
    public String toString() {
        return "surface | center: " + center + ", dir: " + dir + ", d: " + diameter + ", r: " + radius + ", out: " + outMaterial + ", in: " + inMaterial;
    }

    // TODO: in general, convert .getX()s to .x ?

    public LensSurfaceHit intersect( Ray3d ray ) {
        Vector3d raydir = ray.getDir();
        Point3d pos = ray.getPos();
        Vector3d centerToRay = new Vector3d();
        centerToRay.sub( pos, center );
        double tmp = raydir.dot( centerToRay );
        double detty = tmp * tmp - centerToRay.lengthSquared() + radius * radius;
        if ( detty < Numbers.EPSILON ) {
            // TODO: no bad intersections?
            // ray misses sphere entirely
            return null;
        }
        double base = raydir.dot( center ) - raydir.dot( pos );
        double sqt = Math.sqrt( detty );
        double tb = base + sqt;

        if ( tb < Numbers.EPSILON ) {
            // sphere is behind ray
            return null;
        }

        double ta = base - sqt;

        Point3d pb = ray.withDistance( tb );
        Vector3d nb = new Vector3d();
        nb.sub( pb, center );
        nb.normalize();

        boolean gb = nb.dot( dir ) >= minDot;

        if ( ta < Numbers.EPSILON ) {
            // we are inside the sphere
            if ( gb ) {
                // in => out
                nb.negate();
                return new LensSurfaceHit( tb, pb, nb, inMaterial, outMaterial );
            }
            else {
                return null;
            }
        }
        else {
            // two possible hits
            Point3d pa = ray.withDistance( ta );
            Vector3d na = new Vector3d();
            na.sub( pa, center );
            na.normalize();
            boolean ga = na.dot( dir ) >= minDot;

            if ( ga ) {
                // close hit, we have out => in
                return new LensSurfaceHit( ta, pa, na, outMaterial, inMaterial );
            }
            else if ( gb ) {
                // far hit, we have in => out
                nb.negate();
                return new LensSurfaceHit( tb, pb, nb, inMaterial, outMaterial );
            }
            else {
                return null;
            }
        }
    }

    public double getNOut( double wavelength ) {
        if ( outMaterial != null ) {
            return outMaterial.getDispersion().compute( wavelength );
        }
        else {
            return 1;
        }
    }

    public double getNIn( double wavelength ) {
        if ( inMaterial != null ) {
            return inMaterial.getDispersion().compute( wavelength );
        }
        else {
            return 1;
        }
    }

    public static double getIndexOfRefraction( LensMaterial material, double wavelength ) {
        if ( material == null ) {
            return 1;
        }
        else {
            return material.getDispersion().compute( wavelength );
        }
    }

    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength ) {
        LensSurfaceHit hit = intersect( incomingRay );
        if ( hit == null ) {
            return null;
        }
        Vector3d normal = new Vector3d( hit.normal );
        double na = getIndexOfRefraction( hit.fromMaterial, wavelength );
        double nb = getIndexOfRefraction( hit.toMaterial, wavelength );
        if ( FresnelReflection.isTotalInternalReflection( incomingRay.getDir(), normal, na, nb ) ) {
            return null;
        }
        Vector3d outgoing = FresnelReflection.transmit( incomingRay.getDir(), normal, na, nb );
        return new Ray3d( hit.hitPoint, outgoing );
    }

    public LensMaterial getFrontMaterial() {
        if ( radius > 0 ) {
            return outMaterial;
        }
        else {
            return inMaterial;
        }
    }

    public LensMaterial getBackMaterial() {
        if ( radius > 0 ) {
            return inMaterial;
        }
        else {
            return outMaterial;
        }
    }

    public Point3d getCenter() {
        return center;
    }

    public Vector3d getDir() {
        return dir;
    }

    public double getMinDot() {
        return minDot;
    }

    public double getDiameter() {
        return diameter;
    }

    public double getRadius() {
        return radius;
    }

    public LensMaterial getOutMaterial() {
        return outMaterial;
    }

    public LensMaterial getInMaterial() {
        return inMaterial;
    }
}
