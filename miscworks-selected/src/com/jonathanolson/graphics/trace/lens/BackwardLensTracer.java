package com.jonathanolson.graphics.trace.lens;

import java.util.ArrayList;
import java.util.List;

import com.jonathanolson.graphics.trace.util.FresnelReflection;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.graphics.trace.util.RayHit;

public class BackwardLensTracer {
    private LensDesign design;
    private List<LensElement> elements;

    public BackwardLensTracer( LensDesign design ) {
        this.design = design;

        elements = new ArrayList<LensElement>();

        for ( LensElement element : design.getElements() ) {
            if ( element instanceof SimpleLensImage ) {
            }
            else if ( element instanceof SimpleLensStop ) {
                elements.add( 0, element );
            }
            else {
                elements.add( 0, element );
            }
        }
    }

    public Ray3d transmitRay( Ray3d ray, double wavelength ) {
        for ( LensElement element : elements ) {
            ray = element.getTransmittedRay( ray, wavelength );
            if ( ray == null ) {
                break;
            }
        }
        return ray;
    }

    public Ray3d traceRay( Ray3d ray, double wavelength ) {
        int elpos = -1;
        int elsize = elements.size();
        int lensBounces = 0;
        while ( ray != null && lensBounces++ < 32 ) {
            double bestDistance = Double.POSITIVE_INFINITY;
            int bestIndex = -1;
            Ray3d bestRay = null;
            for ( int i = elpos - 1; i <= elpos + 1; i++ ) {
                if ( i < 0 || i >= elsize ) {
                    continue;
                }
                LensElement element = elements.get( i );

                RayHit hit = element.intersect( ray );

                if ( hit == null || hit.getDistance() > bestDistance ) {
                    continue;
                }

                bestDistance = hit.getDistance();
                bestIndex = i;
                if ( hit instanceof LensSurfaceHit ) {
                    LensSurfaceHit lensHit = (LensSurfaceHit) hit;
                    double na = ( lensHit.fromMaterial == null ) ? 1 : ( lensHit.fromMaterial.getDispersion().compute( wavelength ) );
                    double nb = ( lensHit.toMaterial == null ) ? 1 : ( lensHit.toMaterial.getDispersion().compute( wavelength ) );
                    bestRay = new Ray3d( hit.getHitPoint(), FresnelReflection.getFresnelDir( ray.getDir(), hit.getNormal(), na, nb ) );
                }
                else {
                    bestRay = new Ray3d( hit.getHitPoint(), ray.getDir() );
                }
            }
            if ( bestRay == null ) {
                if ( elpos < elsize - 1 ) {
                    // only set ray to null if we are not at the last element
                    ray = null;
                }
                break;
            }
            elpos = bestIndex;
            ray = bestRay;
        }
        return ray;
    }

}
