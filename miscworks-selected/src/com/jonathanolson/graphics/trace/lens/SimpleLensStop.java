package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.Plane3d;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.graphics.trace.util.RayHit;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class SimpleLensStop implements LensElement {
    private Point3d center;
    private double radius;

    private Plane3d plane;

    public SimpleLensStop( Point3d center, double radius ) {
        this.center = center;
        this.radius = radius;
        plane = new Plane3d( new Vector3d( 0, 0, -1 ), -center.z );
    }

    @Override
    public String toString() {
        return "stop | center: " + center + ", radius: " + radius;
    }

    @Override
    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength ) {
//        // TODO: implement plane intersection, do stuff here
//        return incomingRay;
        Plane3d.Hit hit = plane.intersect( incomingRay );
        if ( hit == null || hit.hitPoint.x * hit.hitPoint.x + hit.hitPoint.y * hit.hitPoint.y > radius * radius ) {
            return null;
        }
        return new Ray3d( hit.hitPoint, new Vector3d( incomingRay.getDir() ) );
    }

    @Override
    public RayHit intersect( Ray3d ray ) {
        Plane3d.Hit hit = plane.intersect( ray );
        if ( hit == null || hit.hitPoint.x * hit.hitPoint.x + hit.hitPoint.y * hit.hitPoint.y > radius * radius ) {
            return null;
        }
        else {
            return hit;
        }
    }

    public Point3d getCenter() {
        return center;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius( double radius ) {
        this.radius = radius;
    }

}
