package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.graphics.trace.util.RayHit;

public interface LensElement {
    /**
     * Simple transmit-only interface
     *
     * @param incomingRay
     * @param wavelength
     * @return
     */
    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength );

    public RayHit intersect( Ray3d ray );
}
