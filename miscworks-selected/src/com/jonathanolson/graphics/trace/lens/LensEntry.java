package com.jonathanolson.graphics.trace.lens;

public class LensEntry {
    private double thickness;
    private double diameter;
    private double radius;

    private boolean stop;
    private boolean image;

    // TODO: refactor type to int or enum

    // use null for air?                              
    private LensMaterial material;

    public LensEntry( double thickness, double diameter, double radius, LensMaterial material ) {
        this.thickness = thickness;
        this.diameter = diameter;
        this.radius = radius;
        this.material = material;
    }

    public LensEntry( double thickness, double diameter ) {
        this.thickness = thickness;
        this.diameter = diameter;
        this.stop = true;
    }

    public LensEntry() {
        image = true;
    }

    public double getThickness() {
        return thickness;
    }

    public double getDiameter() {
        return diameter;
    }

    public double getRadius() {
        return radius;
    }

    public boolean isStop() {
        return stop;
    }

    public boolean isImage() {
        return image;
    }

    public LensMaterial getMaterial() {
        return material;
    }
}
