package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.SellmeierDispersion;

public class LensMaterial {
    private SellmeierDispersion dispersion;

    public LensMaterial( SellmeierDispersion dispersion ) {
        this.dispersion = dispersion;
    }

    public SellmeierDispersion getDispersion() {
        return dispersion;
    }
}
