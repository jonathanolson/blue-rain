package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.RayHit;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class LensSurfaceHit implements RayHit {
    /**
     * Distance along the ray to the hit point
     */
    public double distance;

    /**
     * The hit point
     */
    public Point3d hitPoint;

    /**
     * The normal of the sphere (so always facing outwards, even if intersect was inside!!
     */
    public Vector3d normal;

    public LensMaterial fromMaterial;
    public LensMaterial toMaterial;

    public LensSurfaceHit( double distance, Point3d hitPoint, Vector3d normal, LensMaterial fromMaterial, LensMaterial toMaterial ) {
        this.distance = distance;
        this.hitPoint = hitPoint;
        this.normal = normal;
        this.fromMaterial = fromMaterial;
        this.toMaterial = toMaterial;
    }

    @Override
    public String toString() {
        return "dist: " + distance + ", hit: " + hitPoint + ", normal: " + normal;
    }

    public double getDistance() {
        return distance;
    }

    public Point3d getHitPoint() {
        return hitPoint;
    }

    public Vector3d getNormal() {
        return normal;
    }
}
