package com.jonathanolson.graphics.trace.lens;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.graphics.trace.test.FocusTest;
import com.jonathanolson.graphics.trace.util.SellmeierDispersion;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class LensDesign {
    private List<LensEntry> entries = new LinkedList<LensEntry>();
    private List<LensElement> elements = new ArrayList<LensElement>();

    private LensMaterial lastMaterial = null;
    private double lastZ = 0;

    public LensDesign() {
    }

    public void addEntry( LensEntry entry ) {
        entries.add( entry );
        if ( entry.isStop() ) {
            // TODO: fix radius for lens stop
            elements.add( new SimpleLensStop( new Point3d( 0, 0, lastZ ), entry.getDiameter() / 2 ) );
        }
        else if ( entry.isImage() ) {
            elements.add( new SimpleLensImage( new Point3d( 0, 0, lastZ ) ) );
        }
        else {
            if ( entry.getRadius() == 0 ) {
                Point3d center = new Point3d( 0, 0, lastZ );
                Vector3d dir = new Vector3d( 0, 0, -1 );
                elements.add( new FlatLensSurface( center, dir, entry.getDiameter(), lastMaterial, entry.getMaterial() ) );
                lastMaterial = entry.getMaterial();
            }
            else {
                Point3d center = new Point3d( 0, 0, lastZ + entry.getRadius() );
                Vector3d dir = entry.getRadius() > 0 ? new Vector3d( 0, 0, -1 ) : new Vector3d( 0, 0, 1 );
                LensMaterial outMaterial;
                LensMaterial inMaterial;
                if ( entry.getRadius() > 0 ) {
                    outMaterial = lastMaterial;
                    inMaterial = entry.getMaterial();
                }
                else {
                    outMaterial = entry.getMaterial();
                    inMaterial = lastMaterial;
                }
                lastMaterial = entry.getMaterial();
                elements.add( new SphericalLensSurface( center, dir, entry.getDiameter(), entry.getRadius(), outMaterial, inMaterial ) );
            }
        }

        lastZ += entry.getThickness();
    }

    public List<LensEntry> getEntries() {
        return entries;
    }

    public List<LensElement> getElements() {
        return elements;
    }

    public double getWidth() {
        double ret = 0;
        for ( LensEntry entry : entries ) {
            ret += entry.getThickness();
        }
        return ret;
    }

    public double getHeight() {
        double ret = 0;
        for ( LensEntry entry : entries ) {
            if ( entry.getDiameter() > ret ) {
                ret = entry.getDiameter();
            }
        }
        return ret;
    }

    public LensMaterial glass( SellmeierDispersion dispersion ) {
        return new LensMaterial( dispersion );
    }

    public void focusAt( double focusZ, double wavelength ) {

        double imageZOffset = FocusTest.focusTest( this, focusZ, wavelength );

        for ( LensElement element : elements ) {
            if ( element instanceof SimpleLensImage ) {
                SimpleLensImage image = (SimpleLensImage) element;
                image.setCenter( new Point3d( 0, 0, image.getCenter().z + imageZOffset ) );
            }
        }
    }

    public double getImagePlaneZ() {
        for ( LensElement element : elements ) {
            if ( element instanceof SimpleLensImage ) {
                SimpleLensImage image = (SimpleLensImage) element;
                return image.getCenter().z;
            }
        }
        return getWidth();
    }

    public void scaleStopRadius( double ratioRadius ) {
        for ( LensElement element : elements ) {
            if ( element instanceof SimpleLensStop ) {
                SimpleLensStop stop = (SimpleLensStop) element;
                stop.setRadius( stop.getRadius() * ratioRadius );
            }
        }
    }
}
