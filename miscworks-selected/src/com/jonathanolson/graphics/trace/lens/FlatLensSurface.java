package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.FresnelReflection;
import com.jonathanolson.graphics.trace.util.Plane3d;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class FlatLensSurface implements LensSurface {
    private Point3d center;
    private Vector3d dir;
    private double diameter;

    private LensMaterial frontMaterial;

    private LensMaterial backMaterial;

    private Plane3d plane;

    // TODO: flesh out so we can support better tracing


    // assumes that centered on optical axis

    public FlatLensSurface( Point3d center, Vector3d dir, double diameter, LensMaterial frontMaterial, LensMaterial backMaterial ) {
        this.center = center;
        this.dir = dir;
        this.diameter = diameter;
        this.frontMaterial = frontMaterial;
        this.backMaterial = backMaterial;

        plane = new Plane3d( new Vector3d( 0, 0, -1 ), -center.z );
    }

    public LensSurfaceHit intersect( Ray3d ray ) {
        Plane3d.Hit hit = plane.intersect( ray );
        if ( ray.getDir().z > 0 ) {
            return new LensSurfaceHit( hit.distance, hit.hitPoint, hit.normal, frontMaterial, backMaterial );
        }
        else {
            return new LensSurfaceHit( hit.distance, hit.hitPoint, hit.normal, backMaterial, frontMaterial );
        }
    }

    @Override
    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength ) {
        Plane3d.Hit hit = plane.intersect( incomingRay );
        if ( hit == null ) {
            return null;
        }
        double na;
        double nb;
        Vector3d normal;
        if ( incomingRay.getDir().z > 0 ) {
            na = getNOut( wavelength );
            nb = getNIn( wavelength );
            normal = new Vector3d( 0, 0, -1 );
        }
        else {
            na = getNIn( wavelength );
            nb = getNOut( wavelength );
            normal = new Vector3d( 0, 0, 1 );
        }
        if ( FresnelReflection.isTotalInternalReflection( incomingRay.getDir(), normal, na, nb ) ) {
            return null;
        }
        Vector3d outgoing = FresnelReflection.transmit( incomingRay.getDir(), normal, na, nb );
        return new Ray3d( hit.hitPoint, outgoing );
    }

    public double getNOut( double wavelength ) {
        if ( frontMaterial != null ) {
            return frontMaterial.getDispersion().compute( wavelength );
        }
        else {
            return 1;
        }
    }

    public double getNIn( double wavelength ) {
        if ( backMaterial != null ) {
            return backMaterial.getDispersion().compute( wavelength );
        }
        else {
            return 1;
        }
    }

    public Point3d getCenter() {
        return center;
    }

    public Vector3d getDir() {
        return dir;
    }

    public double getDiameter() {
        return diameter;
    }

    public LensMaterial getOutMaterial() {
        return frontMaterial;
    }

    public LensMaterial getInMaterial() {
        return backMaterial;
    }

    public Plane3d getPlane() {
        return plane;
    }

    public LensMaterial getFrontMaterial() {
        return frontMaterial;
    }

    public LensMaterial getBackMaterial() {
        return backMaterial;
    }
}
