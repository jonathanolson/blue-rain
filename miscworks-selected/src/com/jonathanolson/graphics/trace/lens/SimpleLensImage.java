package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.Plane3d;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.graphics.trace.util.RayHit;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

public class SimpleLensImage implements LensElement {
    private Point3d center;
    private Plane3d plane;

    public SimpleLensImage( Point3d center ) {
        this.center = center;

        // TODO: fix, and check directionality
        plane = new Plane3d( new Vector3d( 0, 0, -1 ), -center.z );
    }

    @Override
    public String toString() {
        return "image | center: " + center;
    }

    public Point3d getCenter() {
        return center;
    }

    public void setCenter( Point3d center ) {
        this.center = center;
        plane = new Plane3d( new Vector3d( 0, 0, -1 ), -center.z );
    }

    @Override
    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength ) {
        Plane3d.Hit hit = plane.intersect( incomingRay );
        if ( hit == null ) {
            return null;
        }
        return new Ray3d( hit.hitPoint, new Vector3d( incomingRay.getDir() ) );
    }

    @Override
    public RayHit intersect( Ray3d ray ) {
        return plane.intersect( ray );
    }
}
