package com.jonathanolson.graphics.trace.lens;

import com.jonathanolson.graphics.trace.util.Ray3d;

public interface LensSurface extends LensElement {
    public Ray3d getTransmittedRay( Ray3d incomingRay, double wavelength );

    public LensMaterial getFrontMaterial();

    public LensMaterial getBackMaterial();

    public double getDiameter();

    public LensSurfaceHit intersect( Ray3d ray );
}
