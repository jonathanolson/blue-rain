package com.jonathanolson.graphics.trace.tests;

import org.junit.Test;

import com.jonathanolson.graphics.trace.util.FresnelReflection;
import com.jonathanolson.graphics.trace.util.Numbers;
import com.jonathanolson.util.math.vec.Vector3d;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ReflectionTest {
    @Test
    public void testReflect45deg() {
        System.out.println( "Testing reflection" );
        Vector3d normal = new Vector3d( 0, 0, 1 );
        Vector3d incident = new Vector3d( 1, 0, -1 );
        Vector3d expectedResult = new Vector3d( 1, 0, 1 );
        incident.normalize();
        expectedResult.normalize();
        Vector3d result = FresnelReflection.reflect( incident, normal );
        System.out.println( expectedResult );
        System.out.println( result );
        assertTrue( result.epsilonEquals( expectedResult, Numbers.EPSILON ) );
    }

    @Test
    public void testReflect0deg() {
        System.out.println( "Testing reflection" );
        Vector3d normal = new Vector3d( 0, 0, 1 );
        Vector3d incident = new Vector3d( 0, 0, -1 );
        Vector3d expectedResult = new Vector3d( 0, 0, 1 );
        Vector3d result = FresnelReflection.reflect( incident, normal );
        System.out.println( expectedResult );
        System.out.println( result );
        assertTrue( result.epsilonEquals( expectedResult, Numbers.EPSILON ) );
    }

    @Test
    public void testUnnormalizedVectors() {
        Vector3d unitVector = new Vector3d( 0, 0, 1 );
        Vector3d nonUnitVector = new Vector3d( 1, 0, -1 );
        try {
            FresnelReflection.reflect( unitVector, nonUnitVector );
            fail();
        }
        catch( AssertionError e ) {

        }
        try {
            FresnelReflection.reflect( nonUnitVector, unitVector );
            fail();
        }
        catch( AssertionError e ) {

        }
        try {
            FresnelReflection.transmit( nonUnitVector, unitVector, 1, 1 );
            fail();
        }
        catch( AssertionError e ) {

        }
        try {
            FresnelReflection.transmit( unitVector, nonUnitVector, 1, 1 );
            fail();
        }
        catch( AssertionError e ) {

        }
    }

    @Test
    public void testWrongDirectionReflections() {
        try {
            System.out.println( "Orthogonal to normal reflection. should error" );
            FresnelReflection.reflect( new Vector3d( 1, 0, 0 ), new Vector3d( 0, 0, 1 ) );
            fail();
        }
        catch( RuntimeException e ) {

        }
        try {
            System.out.println( "Same direction as normal reflection. should error" );
            Vector3d i = new Vector3d( 1, 0, 1 );
            i.normalize();
            FresnelReflection.reflect( i, new Vector3d( 0, 0, 1 ) );
            fail();
        }
        catch( RuntimeException e ) {

        }
    }

    @Test
    public void testTransmitAirAir() {
        System.out.println( "Testing air-air transmission" );
        Vector3d normal = new Vector3d( 0, 0, 1 );
        Vector3d incident = new Vector3d( 1, 0, -1 );
        incident.normalize();
        Vector3d result = FresnelReflection.transmit( incident, normal, 1, 1 );
        System.out.println( result );
        assertTrue( result.epsilonEquals( incident, Numbers.EPSILON ) );
    }

    @Test
    public void testTransmitIOR() {
        try {
            FresnelReflection.transmit( new Vector3d( 0, 0, -1 ), new Vector3d( 0, 0, 1 ), 1, 0.5 );
            fail();
        }
        catch( AssertionError e ) {

        }
        try {
            FresnelReflection.transmit( new Vector3d( 0, 0, -1 ), new Vector3d( 0, 0, 1 ), 0.5, 1 );
            fail();
        }
        catch( AssertionError e ) {

        }
    }

    @Test
    public void testTransmit0deg() {
        System.out.println( "Testing 0deg transmission" );
        Vector3d normal = new Vector3d( 0, 0, 1 );
        Vector3d incident = new Vector3d( 0, 0, -1 );
        Vector3d result = FresnelReflection.transmit( incident, normal, 1, 2 );
        System.out.println( result );
        assertTrue( result.epsilonEquals( incident, Numbers.EPSILON ) );
    }

    @Test
    public void testFresnelDielectricBrewsterAngle() {
        System.out.println( "Testing to make sure there is no p-reflectance at the brewster angle" );
        Vector3d normal = new Vector3d( 0, 0, 1 );
        double na = 1;
        double nb = 2;
        Vector3d b = new Vector3d( nb, 0, -na );
        b.normalize();
        double[] br = FresnelReflection.fresnelDielectric( b, normal, na, nb );
        System.out.println( br[0] + " " + br[1] );
        assertTrue( Math.abs( br[1] ) < Numbers.EPSILON );
    }
}
