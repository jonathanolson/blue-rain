package com.jonathanolson.graphics.trace.tests;

import org.junit.Test;

import com.jonathanolson.graphics.trace.lens.LensSurfaceHit;
import com.jonathanolson.graphics.trace.lens.SphericalLensSurface;
import com.jonathanolson.graphics.trace.util.Numbers;
import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

import static org.junit.Assert.assertTrue;

public class LensSurfaceTest {
    @Test
    public void TestMiss() {
        System.out.println( "TestMiss()" );
        SphericalLensSurface surface = new SphericalLensSurface(
                new Point3d( 0, 0, 0 ),
                new Vector3d( 0, 0, 1, true ),
                1,
                1
        );
        // passes it to the left (but goes through sphere)
        Ray3d ray = new Ray3d(
                new Point3d( 0.75, 0, 4 ),
                new Vector3d( 0, 0, -1 ),
                true
        );
        LensSurfaceHit hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
        // goes off at diagonal, missing
        ray = new Ray3d(
                new Point3d( 0, 0, 4 ),
                new Vector3d( 1, 0, -1 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
        // goes other direction
        ray = new Ray3d(
                new Point3d( 0, 0, 4 ),
                new Vector3d( 0, 0, 1 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
        // skims the front. shouldn't hit
        ray = new Ray3d(
                new Point3d( 5, 0, 0 ),
                new Vector3d( -1, 0, 0 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
        // at the front. shouldn't hit
        ray = new Ray3d(
                new Point3d( 0, 0, 1 ),
                new Vector3d( 0, 0, -1 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
    }

    @Test
    public void TestMiss2() {
        System.out.println( "TestMiss2()" );
        SphericalLensSurface surface = new SphericalLensSurface(
                new Point3d( 0, 3, 0 ),
                new Vector3d( 0, 0, 1, true ),
                4,
                10
        );
        // goes under
        Ray3d ray = new Ray3d(
                new Point3d( 0, 0, 4 ),
                new Vector3d( 0, 0, -1 ),
                true
        );
        LensSurfaceHit hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit == null );
    }

    @Test
    public void TestHit1() {
        System.out.println( "Testing hits" );
        // unit sphere at origin, [-0.75,0.75] vertical
        SphericalLensSurface surface = new SphericalLensSurface(
                new Point3d( 0, 0, 0 ),
                new Vector3d( 0, 0, 1, true ),
                1.5,
                1
        );
        // direct hit
        Ray3d ray = new Ray3d(
                new Point3d( 0, 0, 4 ),
                new Vector3d( 0, 0, -1 ),
                true
        );
        LensSurfaceHit hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit != null );
        assertTrue( Numbers.equalsEpsilon( 3, hit.distance ) );
        assertTrue( hit.normal.epsilonEquals( new Vector3d( 0, 0, 1 ), Numbers.EPSILON ) );
        assertTrue( hit.hitPoint.epsilonEquals( new Point3d( 0, 0, 1 ), Numbers.EPSILON ) );
        // should have a normal at 45 deg
        ray = new Ray3d(
                new Point3d( Math.sqrt( 2 ) / 2, 0, 4 ),
                new Vector3d( 0, 0, -1 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit != null );
        assertTrue( Numbers.equalsEpsilon( 3 + 1 - Math.sqrt( 2 ) / 2, hit.distance ) );
        assertTrue( hit.normal.epsilonEquals( new Vector3d( 1, 0, 1, true ), Numbers.EPSILON ) );
        assertTrue( hit.hitPoint.epsilonEquals( new Point3d( Math.sqrt( 2 ) / 2, 0, Math.sqrt( 2 ) / 2 ), Numbers.EPSILON ) );
        // from inside, pointing back
        ray = new Ray3d(
                new Point3d( 0, 0, 0 ),
                new Vector3d( 1, 0, 1 ),
                true
        );
        hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit != null );
        assertTrue( Numbers.equalsEpsilon( 1, hit.distance ) );
        assertTrue( hit.normal.epsilonEquals( new Vector3d( -1, 0, -1, true ), Numbers.EPSILON ) );
        assertTrue( hit.hitPoint.epsilonEquals( new Point3d( Math.sqrt( 2 ) / 2, 0, Math.sqrt( 2 ) / 2 ), Numbers.EPSILON ) );
    }

    @Test
    public void TestHit2() {
        System.out.println( "Testing hits" );
        // unit sphere at 5,5,0, [-0.5, 0.5] vertical
        SphericalLensSurface surface = new SphericalLensSurface(
                new Point3d( 5, 5, -1 ),
                new Vector3d( 0, 0, 1, true ),
                1,
                1
        );
        // direct hit, angled, but at the front
        Ray3d ray = new Ray3d(
                new Point3d( 0, 0, 5 ),
                new Vector3d( 1, 1, -1 ),
                true
        );
        LensSurfaceHit hit = surface.intersect( ray );
        System.out.println( hit );
        assertTrue( hit != null );
        assertTrue( hit.normal.epsilonEquals( new Vector3d( 0, 0, 1 ), Numbers.EPSILON ) );
        assertTrue( hit.hitPoint.epsilonEquals( new Point3d( 5, 5, 0 ), Numbers.EPSILON ) );
    }
}
