package com.jonathanolson.graphics.trace.tests;

import org.junit.Test;

import com.jonathanolson.graphics.trace.lens.LensDesign;
import com.jonathanolson.graphics.trace.lens.LensElement;
import com.jonathanolson.graphics.trace.lens.LensMaterial;
import com.jonathanolson.graphics.trace.lens.LensSurface;
import com.jonathanolson.graphics.trace.lens.designs.DoubleGaussD;

import static org.junit.Assert.assertTrue;

public class LensDesignTest {
    @Test
    public void TestDoubleGaussD() {
        System.out.println( "Testing Double Gauss D behavior (as an example)" );
        LensDesign design = new DoubleGaussD();

        // material checking tests
        LensMaterial lastMaterial = null;// should start in glass
        for ( LensElement element : design.getElements() ) {
            if ( element instanceof LensSurface ) {
                LensSurface surface = (LensSurface) element;
                assertTrue( lastMaterial == surface.getFrontMaterial() );
                lastMaterial = surface.getBackMaterial();
            }
        }
        assertTrue( lastMaterial == null );// should end in glass
    }
}
