package com.jonathanolson.graphics.trace.tests;

import org.junit.Test;

import com.jonathanolson.graphics.trace.util.Ray3d;
import com.jonathanolson.util.math.vec.Point3d;
import com.jonathanolson.util.math.vec.Vector3d;

import static org.junit.Assert.assertTrue;

public class Ray3dTest {
    @Test
    public void TestDistance() {
        Ray3d ray = new Ray3d( new Point3d( 0, -2, 1 ), new Vector3d( 0, 0, 1 ), true );
        Point3d p = ray.withDistance( 4 );
        assertTrue( p.epsilonEquals( new Point3d( 0, -2, 5 ), 0.0001 ) );
    }
}
