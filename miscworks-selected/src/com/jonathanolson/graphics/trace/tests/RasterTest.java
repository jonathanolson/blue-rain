package com.jonathanolson.graphics.trace.tests;

import org.junit.Test;

import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.util.math.vec.Point2d;

import static org.junit.Assert.assertTrue;

public class RasterTest {
    @Test
    public void TestReversibility() {
        Raster3 raster = new Raster3( 512, 512, new Point2d( 2, 4 ), 0.25 );
        int x = 214;
        double xd = raster.reverseMapX( x );
        int xi = raster.mapX( xd );
        assertTrue( x == xi );
    }
}
