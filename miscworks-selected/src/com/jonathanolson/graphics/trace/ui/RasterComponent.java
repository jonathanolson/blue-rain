package com.jonathanolson.graphics.trace.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.*;

import com.jonathanolson.graphics.ImagePanel;
import com.jonathanolson.graphics.color.StandardIlluminant;
import com.jonathanolson.graphics.color.XYZResponse;
import com.jonathanolson.graphics.color.colors.BoxGreen;
import com.jonathanolson.graphics.color.colors.BoxRed;
import com.jonathanolson.graphics.color.colors.BoxWhite;
import com.jonathanolson.graphics.color.colors.SpectralColor;
import com.jonathanolson.graphics.color.spaces.Colorspace;
import com.jonathanolson.graphics.color.spaces.RGBColorspace;
import com.jonathanolson.graphics.trace.util.Raster3;
import com.jonathanolson.util.math.RandomUtils;
import com.jonathanolson.util.math.vec.Point2d;
import com.jonathanolson.util.math.vec.Point3d;

public class RasterComponent extends JPanel {
    private BufferedImage image;
    private int rasterWidth;
    private int rasterHeight;

    private double scale = 1.0;

    private static final double EQUAL_VALUE = 0.2;

    private Raster3 raster;
    private ImagePanel imagePanel;

    private Thread updateThread;

    public RasterComponent( final Raster3 raster ) {
        super( new GridBagLayout() );
        this.raster = raster;
        rasterWidth = raster.getWidth();
        rasterHeight = raster.getHeight();
        image = new BufferedImage( rasterWidth, rasterHeight, BufferedImage.TYPE_INT_RGB );
        imagePanel = new ImagePanel( image );

        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.gridheight = 5;
        add( imagePanel, c );

        c.gridx = 1;
        c.gridy = 0;
        c.gridheight = 1;
        JButton lightButton = new JButton( "Lighter" );
        add( lightButton, c );

        c.gridx = 1;
        c.gridy = 1;
        c.gridheight = 1;
        JButton darkButton = new JButton( "Darker" );
        add( darkButton, c );

        c.gridx = 1;
        c.gridy = 2;
        c.gridheight = 1;
        JButton equalizeButton = new JButton( "Equal" );
        add( equalizeButton, c );

        c.gridx = 1;
        c.gridy = 3;
        c.gridheight = 1;
        JButton updateButton = new JButton( "Update" );
        add( updateButton, c );

        c.gridx = 1;
        c.gridy = 4;
        c.gridheight = 1;
        c.weighty = 1.0;
        add( Box.createRigidArea( new Dimension( 1, 1 ) ), c );


        lightButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                lighten();
            }
        } );

        darkButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                darken();
            }
        } );

        equalizeButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                equalize();
            }
        } );

        updateButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent e ) {
                transfer();
                triggerRepaint();
            }
        } );

        updateThread = new Thread() {
            @Override
            public void run() {
                int sleeptime = 1000;
                int idx = 0;
                while ( true ) {
                    try {
                        sleep( sleeptime );
                        idx++;
                    }
                    catch( InterruptedException e ) {
                        e.printStackTrace();
                        return;
                    }
                    equalize();
                    sleeptime *= 1.1;
//                    NumberFormat format = NumberFormat.getInstance();
//                    if ( format instanceof DecimalFormat ) {
//                        DecimalFormat f = (DecimalFormat) format;
//                        f.setMinimumIntegerDigits( 6 );
//                        f.setDecimalSeparatorAlwaysShown( false );
//                    }
//                    save( new File( "out-" + format.format( idx ) + ".png" ) );
                }
            }
        };
    }

    public void lighten() {
        scale *= 1.5;
        transfer();
        triggerRepaint();
    }

    public void darken() {
        scale /= 1.5;
        transfer();
        triggerRepaint();
    }

    public void save( File file ) {
        try {
            ImageIO.write( image, "png", file );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    public void triggerRepaint() {
        imagePanel.repaint( 0, 0, 0, rasterWidth, rasterHeight );
    }

    public void equalize() {
        double count = 0;
        double[][][] data = raster.getData();
        for ( int i = 0; i < rasterHeight; i++ ) {
            for ( int j = 0; j < rasterWidth; j++ ) {
                count += data[i][j][0];
                count += data[i][j][1];
                count += data[i][j][2];
            }
        }
        double avg = count / ( 3 * (double) ( rasterHeight * rasterWidth ) );
        scale = EQUAL_VALUE / avg;
        transfer();
        triggerRepaint();
    }

    public void startUpdates() {
        updateThread.start();
    }

    public void transfer() {
        int[] cacheRow = new int[rasterWidth * 3];
        Colorspace srgb = RGBColorspace.sRGB;
        WritableRaster writeRaster = image.getRaster();

        double[][][] data = raster.getData();

        //Matrix3d whitewallmatrix = ChromaticAdaptation.construct( ChromaticAdaptation.BRADFORD, StandardIlluminant.E_XYZ, StandardIlluminant.D65_XYZ );
        for ( int i = 0; i < rasterHeight; i++ ) {
            for ( int j = 0; j < rasterWidth; j++ ) {
                double[] pixel = data[i][j];

                Point3d color = new Point3d( new double[]{pixel[0] * scale, pixel[1] * scale, pixel[2] * scale} );
                //whitewallmatrix.transform( color );
                srgb.fromXYZ( color );

                cacheRow[j * 3] = Colorspace.convertChannel( color.getX() );
                cacheRow[j * 3 + 1] = Colorspace.convertChannel( color.getY() );
                cacheRow[j * 3 + 2] = Colorspace.convertChannel( color.getZ() );
//                writeRaster.setPixel( j, i, new int[]{
//                        Colorspace.convertChannel( color.getX() ),
//                        Colorspace.convertChannel( color.getY() ),
//                        Colorspace.convertChannel( color.getZ() )
//                } );
            }
            writeRaster.setPixels( 0, i, rasterWidth, 1, cacheRow );
        }
    }

    public static RasterComponent showRaster( Raster3 raster ) {
        JFrame frame = new JFrame( "Raster" );

        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        JPanel panel = new JPanel();
        frame.setContentPane( panel );

        RasterComponent rasterComponent = new RasterComponent( raster );
        frame.add( rasterComponent );

        rasterComponent.transfer();

        frame.pack();
        frame.setVisible( true );
        return rasterComponent;
    }

    public static void main( String[] args ) {

        final Raster3 raster = new Raster3( 640, 480, new Point2d( 0, 0 ), 1.0 );

        JFrame frame = new JFrame( "Test" );

        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        JPanel panel = new JPanel();
        frame.setContentPane( panel );

        RasterComponent rasterComponent = new RasterComponent( raster );
        frame.add( rasterComponent );

        rasterComponent.transfer();

        frame.pack();
        frame.setVisible( true );

        Thread tossThread = new Thread() {
            @Override
            public void run() {
                while ( true ) {
                    if ( Thread.interrupted() ) {
                        return;
                    }

                    SpectralColor red = new BoxRed();
                    SpectralColor green = new BoxGreen();
                    SpectralColor white = new BoxWhite();

                    for ( int i = 0; i < 100000; i++ ) {
                        double x = RandomUtils.random();
                        double y = RandomUtils.random();
                        double wavelength = RandomUtils.random() * 500 + 300; // 300 - 800
                        double[] xyz = XYZResponse.sample2deg( wavelength );
                        double power = red.reflection( wavelength ) * StandardIlluminant.D65_SPECTRAL( wavelength );

                        raster.addAt( new Point2d( x, y ), new Point3d( xyz[0] * power, xyz[1] * power, xyz[1] * power ) );
                    }

                    for ( int i = 0; i < 100000; i++ ) {
                        double x = -RandomUtils.random();
                        double y = RandomUtils.random();
                        double wavelength = RandomUtils.random() * 500 + 300; // 300 - 800
                        double[] xyz = XYZResponse.sample2deg( wavelength );
                        double power = StandardIlluminant.D65_SPECTRAL( wavelength );

                        raster.addAt( new Point2d( x, y ), new Point3d( xyz[0] * power, xyz[1] * power, xyz[1] * power ) );
                    }

                    for ( int i = 0; i < 100000; i++ ) {
                        double x = RandomUtils.random();
                        double y = -RandomUtils.random();
                        double wavelength = RandomUtils.random() * 500 + 300; // 300 - 800
                        double[] xyz = XYZResponse.sample2deg( wavelength );
                        double power = white.reflection( wavelength ) * StandardIlluminant.D65_SPECTRAL( wavelength );

                        raster.addAt( new Point2d( x, y ), new Point3d( xyz[0] * power, xyz[1] * power, xyz[1] * power ) );
                    }

                    for ( int i = 0; i < 100000; i++ ) {
                        double x = -RandomUtils.random();
                        double y = -RandomUtils.random();
                        double wavelength = RandomUtils.random() * 500 + 300; // 300 - 800
                        double[] xyz = XYZResponse.sample2deg( wavelength );
                        double power = green.reflection( wavelength ) * StandardIlluminant.D65_SPECTRAL( wavelength );

                        raster.addAt( new Point2d( x, y ), new Point3d( xyz[0] * power, xyz[1] * power, xyz[1] * power ) );
                    }
                }
            }
        };

        tossThread.start();
    }
}
