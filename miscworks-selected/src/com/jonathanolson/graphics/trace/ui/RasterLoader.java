package com.jonathanolson.graphics.trace.ui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

import javax.swing.*;

import com.jonathanolson.graphics.trace.util.Raster3;

public class RasterLoader extends JPanel {

    private JPanel rasterHolder;
    private Raster3 raster;
    private RasterComponent rcomp = null;

    public RasterLoader() {
        super( new BorderLayout() );

        JPanel buttonPanel = new JPanel();
        add( buttonPanel, BorderLayout.PAGE_START );

        JButton fileButton = new JButton( "From File" );
        buttonPanel.add( fileButton );

        JButton networkButton = new JButton( "From Network" );
        buttonPanel.add( networkButton );

        rasterHolder = new JPanel();
        add( rasterHolder, BorderLayout.CENTER );


        fileButton.addActionListener( new ActionListener() {
            @Override
            public void actionPerformed( ActionEvent actionEvent ) {
                JFileChooser fc = new JFileChooser();
                int ret = fc.showOpenDialog( RasterLoader.this );
                if ( ret == JFileChooser.APPROVE_OPTION ) {
                    File file = fc.getSelectedFile();
                    try {
                        ObjectInputStream in = new ObjectInputStream( new FileInputStream( file ) );
                        raster = (Raster3) in.readObject();
                        System.out.println( "raster: " + raster.getWidth() + "x" + raster.getHeight() );
                        if ( rcomp != null ) {
                            rasterHolder.remove( rcomp );
                        }
                        rcomp = new RasterComponent( raster );
                        rasterHolder.add( rcomp );
                    }
                    catch( IOException e ) {
                        e.printStackTrace();
                    }
                    catch( ClassNotFoundException e ) {
                        e.printStackTrace();
                    }
                }
                else {
                    System.out.println( "Not approved" );
                }
            }
        } );
    }

    public static void main( String[] args ) {
        JFrame frame = new JFrame( "Raster" );

        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

        frame.setContentPane( new RasterLoader() );

        frame.pack();
        frame.setVisible( true );
    }
}
