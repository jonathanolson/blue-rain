package com.jonathanolson.graphics;

import java.awt.*;
import java.awt.image.BufferedImage;

import javax.swing.*;

public class ImagePanel extends JPanel {

    private BufferedImage image;

    public ImagePanel( BufferedImage image ) {
        this.image = image;
        setPreferredSize( new Dimension( image.getWidth(), image.getHeight() ) );
    }

    @Override
    public void paint( Graphics g ) {
        g.drawImage( image, 0, 0, null );
    }

}
