package com.jonathanolson.euler;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class P314 {

	private List<Post> posts = new LinkedList<Post>();
	private Post startPost;
	private Path bestFinishedPath;

	public static void main(String[] args) {
		P314 p = new P314();
		p.createPosts();
		p.walk();
		p.printBest();
	}

	private void printBest() {
		System.out.printf("%.10f", bestFinishedPath.getRatio());
	}

	public void walk() {
		walk(new Path(startPost, null), 0);
	}

	public void walk(Path path, double lastSlope) {
		if (path.isFinished()) {
			if (bestFinishedPath == null
					|| bestFinishedPath.getRatio() < path.getRatio()) {
				bestFinishedPath = path;
			}
		} else {
			List<Post> hoppables = postsHoppableFrom(path.post, lastSlope);

			for (Post hoppablePost : hoppables) {
				Path newPath = new Path(hoppablePost, path);
				if (newPath.isBetterThan(hoppablePost.bestPath)) {
					walk(newPath, hoppablePost.slopeFrom(path.post));
				}
			}
		}
	}

	public List<Post> postsHoppableFrom(Post post, double lastSlope) {
		List<Post> ret = new LinkedList<Post>();
		for (Post other : posts) {
			double nextSlope = other.slopeFrom(post);
			if (nextSlope >= lastSlope) { // does implicit check against -1,
											// since lastSlope >= 0
				ret.add(other);
			}
		}
		return ret;
	}

	public void createPosts() {
		for (int y = 0; y <= 250; y++) {
			for (int x = 250; x >= 0; x++) {
				if (y > x || y < 250 - x) {
					continue;
				}
				posts.add(new Post(x, y));
			}
		}
		startPost = posts.get(0);

		// sort all of the posts so ones "closer" to start are first
		Collections.sort(posts, new Comparator<Post>() {
			@Override
			public int compare(Post a, Post b) {
				double va = a.y / a.getDistanceFromOrigin();
				double vb = b.y / b.getDistanceFromOrigin();
				if (va == vb) {
					return 0;
				}
				return (va < vb) ? -1 : 1;
			}
		});
	}
}

class Post {
	public final int x;
	public final int y;

	public Path bestPath;

	public Post(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public double getDistanceFromOrigin() {
		return Math.sqrt(x * x + y * y);
	}

	public double slopeFrom(Post previous) {
		if (previous.y > this.y || previous.x < this.x) {
			return -1;
		}
		return ((double) (previous.x - this.x))
				/ ((double) (this.y - previous.y));
	}
}

class Path {
	public final Post post;
	public final Path parent;
	public final double area;
	public final double length;

	public Path(Post post, Path parent) {
		this.post = post;
		this.parent = parent;
		if (parent == null) {
			area = 0;
			length = 0;
		} else {
			area = parent.area
					+ 0.5
					* Math.sqrt((double) (this.post.x * parent.post.y - parent.post.x
							* this.post.y));
			double dx = this.post.x * parent.post.x;
			double dy = this.post.y * parent.post.y;
			length = parent.length + Math.sqrt(dx * dx + dy * dy);
		}
	}

	public boolean isFinished() {
		return post.x == post.y; // condition at the end
	}

	public double getRatio() {
		return area / length;
	}

	public boolean isBetterThan(Path other) { // possibly null other
		return (other == null || (this.post == other.post && this.getRatio() > other
				.getRatio()));
	}
}
