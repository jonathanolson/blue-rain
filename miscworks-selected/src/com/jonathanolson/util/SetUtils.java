package com.jonathanolson.util;

import java.util.Set;

import com.jonathanolson.util.math.RandomUtils;

public class SetUtils {
    public static <T> T pickFromSet( Set<T> set ) {
        if ( set.isEmpty() ) {
            return null;
        }

        int idx = RandomUtils.nextInt( set.size() );
        for ( T element : set ) {
            if ( idx == 0 ) {
                return element;
            }

            idx--;
        }

        return null;
    }
}
