package com.jonathanolson.util;

import java.util.List;

import com.jonathanolson.util.math.RandomUtils;

public class ListUtils {
    public static <T> T pickFromList( List<T> list ) {
        if ( list.isEmpty() ) {
            return null;
        }

        int idx = RandomUtils.nextInt( list.size() );
        for ( T element : list ) {
            if ( idx == 0 ) {
                return element;
            }

            idx--;
        }

        return null;
    }
}
