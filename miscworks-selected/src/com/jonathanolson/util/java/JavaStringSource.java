package com.jonathanolson.util.java;

import java.io.File;
import java.io.IOException;

import com.jonathanolson.util.FileUtils;

public class JavaStringSource {

    private String fullClassName;
    private String source;

    public JavaStringSource( String fullClassName, String source ) {
        this.fullClassName = fullClassName;
        this.source = source;
    }

    public String getPackageName() {
        int idx = fullClassName.lastIndexOf( "." );
        if ( idx >= 0 ) {
            return fullClassName.substring( 0, idx );
        }
        else {
            return null;
        }
    }

    public String getClassName() {
        int idx = fullClassName.lastIndexOf( "." );
        if ( idx >= 0 ) {
            return fullClassName.substring( idx + 1 );
        }
        else {
            return fullClassName;
        }
    }

    public File writeToTemporaryDir( File temporaryDir ) throws IOException {
        File packageDir = getPackageDir( temporaryDir );
        File sourceFile = new File( packageDir, getClassName() + ".java" );
        FileUtils.overwriteStringToFile( source, sourceFile );
        return sourceFile;
    }

    public String getSource() {
        return source;
    }

    public String getFullClassName() {
        return fullClassName;
    }

    private File getPackageDir( File temporaryDir ) {
        String fullPack = getPackageName();

        if ( fullPack != null ) {
            String[] packs = fullPack.split( "\\." );
            File packageDir = temporaryDir;
            for ( int i = 0; i < packs.length; i++ ) {
                String pack = packs[i];
                packageDir = new File( packageDir, pack );
            }
            packageDir.mkdirs();
            return packageDir;
        }
        else {
            return temporaryDir;
        }
    }


    public static String getPackageName( String fullClassName ) {
        int idx = fullClassName.lastIndexOf( "." );
        if ( idx >= 0 ) {
            return fullClassName.substring( 0, idx );
        }
        else {
            return null;
        }
    }

    public static String getClassName( String fullClassName ) {
        int idx = fullClassName.lastIndexOf( "." );
        if ( idx >= 0 ) {
            return fullClassName.substring( idx + 1 );
        }
        else {
            return fullClassName;
        }
    }

    public String toString() {
        return source;
    }

}
