package com.jonathanolson.util.java;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import com.sun.tools.javac.Main;


public class CompilerUtils {

    private File temporaryDir;

    public CompilerUtils() {
        String dirName = "javatmp-" + String.valueOf( (int) Math.abs( Math.random() * 50000 + 10000 ) );
        temporaryDir = new File( System.getProperty( "java.io.tmpdir" ), dirName );
        temporaryDir.mkdirs();
        try {
            System.out.println( "Created dir: " + temporaryDir.getCanonicalPath() );
        }
        catch( IOException e ) {
            e.printStackTrace();
        }
    }

    public void clearDir() {
        recursiveClear( temporaryDir );
    }

    private void recursiveClear( File file ) {
        if ( file.isDirectory() ) {
            for ( File subfile : file.listFiles() ) {
                recursiveClear( subfile );
            }
            file.delete();
        }
        else {
            file.delete();
        }
    }

    public void compileSources( JavaStringSource[] sources ) throws IOException {
        List<JavaStringSource> lsources = new LinkedList<JavaStringSource>();
        Collections.addAll( lsources, sources );
        compileSources( lsources );
    }

    public void compileSources( List<JavaStringSource> sources ) throws IOException {
        List<String> sourceFiles = new LinkedList<String>();

        for ( JavaStringSource source : sources ) {
            File sourceFile = source.writeToTemporaryDir( temporaryDir );
            sourceFiles.add( sourceFile.getCanonicalPath() );
        }

        String[] fileArray = sourceFiles.toArray( new String[0] );

        int status = Main.compile( fileArray );

        if ( status != 0 ) {
            System.out.println( "WARNING: did not compile? status: " + status );
            throw new IOException();
        }
    }

    public Class getCompiledClass( String fullClassName ) throws MalformedURLException, ClassNotFoundException {
        String loaderPath = "file://" + temporaryDir + "/";
        URLClassLoader classLoader = new URLClassLoader( new URL[]{new URL( loaderPath )} );
        Class clazz = Class.forName( fullClassName, true, classLoader );
        return clazz;
    }

    public Object getNewDefaultInstance( Class clazz ) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException, InstantiationException {
        Constructor cons = clazz.getConstructor( new Class[]{} );
        Object ob = cons.newInstance();
        return ob;
    }

    public static class TestNew {
        public static void main( String[] args ) {
            CompilerUtils utils = new CompilerUtils();
            JavaStringSource sa = new JavaStringSource(
                    "com.test.Tester",
                    "package com.test; public class Tester { public boolean test() { System.out.println( \"TestNew\" ); return true; } }" );
            try {
                utils.compileSources( new JavaStringSource[]{sa} );
                Class clazz = utils.getCompiledClass( "com.test.Tester" );
                Constructor cons = clazz.getConstructor( new Class[]{} );
                Object ob = cons.newInstance();
                Method meth = clazz.getMethod( "test", new Class[]{} );
                meth.invoke( ob );
            }
            catch( IOException e ) {
                e.printStackTrace();
            }
            catch( ClassNotFoundException e ) {
                e.printStackTrace();
            }
            catch( NoSuchMethodException e ) {
                e.printStackTrace();
            }
            catch( InvocationTargetException e ) {
                e.printStackTrace();
            }
            catch( IllegalAccessException e ) {
                e.printStackTrace();
            }
            catch( InstantiationException e ) {
                e.printStackTrace();
            }

        }
    }

    public static class TestNewer {
        public static void main( String[] args ) {
            CompilerUtils utils = new CompilerUtils();
            JavaStringSource sa = new JavaStringSource(
                    "com.test.Tester",
                    "package com.test; " +
                    "public class Tester { " +
                    "public String test() { " +
                    "System.out.println( \"TestNew\" ); " +
                    "return new TesterUtils().toString();" +
                    "} " +
                    "}" );
            JavaStringSource sb = new JavaStringSource(
                    "com.test.TesterUtils",
                    "package com.test; " +
                    "public class TesterUtils { " +
                    "public String toString() { " +
                    "return \"This is a test string\";" +
                    "} " +
                    "}" );
            try {
                utils.compileSources( new JavaStringSource[]{sa, sb} );
                Class clazz = utils.getCompiledClass( "com.test.Tester" );
                Constructor cons = clazz.getConstructor( new Class[]{} );
                Object ob = cons.newInstance();
                Method meth = clazz.getMethod( "test", new Class[]{} );
                Object ret = meth.invoke( ob );
                if ( ret instanceof String ) {
                    String retString = (String) ret;
                    System.out.println( "String: " + retString );
                }
                else {
                    System.out.println( "Error?" );
                }
            }
            catch( IOException e ) {
                e.printStackTrace();
            }
            catch( ClassNotFoundException e ) {
                e.printStackTrace();
            }
            catch( NoSuchMethodException e ) {
                e.printStackTrace();
            }
            catch( InvocationTargetException e ) {
                e.printStackTrace();
            }
            catch( IllegalAccessException e ) {
                e.printStackTrace();
            }
            catch( InstantiationException e ) {
                e.printStackTrace();
            }

        }
    }

}
