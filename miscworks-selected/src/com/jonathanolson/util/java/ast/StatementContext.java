package com.jonathanolson.util.java.ast;

import java.util.HashSet;
import java.util.Set;

public class StatementContext {

    private Set<Variable> uninitializedVariables;
    private Set<Variable> initializedVariables;

    public StatementContext() {
        uninitializedVariables = new HashSet<Variable>();
        initializedVariables = new HashSet<Variable>();
    }

    public StatementContext( Set<Variable> uninitializedVariables, Set<Variable> initializedVariables ) {
        this.uninitializedVariables = new HashSet<Variable>();
        this.initializedVariables = new HashSet<Variable>();

        this.uninitializedVariables.addAll( uninitializedVariables );
        this.initializedVariables.addAll( initializedVariables );
    }

    public StatementContext( StatementContext context ) {
        this( context.getUninitializedVariables(), context.getInitializedVariables() );
    }

    public Set<Variable> getUninitializedVariables() {
        return uninitializedVariables;
    }

    public void setUninitializedVariables( Set<Variable> uninitializedVariables ) {
        this.uninitializedVariables = uninitializedVariables;
    }

    public Set<Variable> getInitializedVariables() {
        return initializedVariables;
    }

    public void setInitializedVariables( Set<Variable> initializedVariables ) {
        this.initializedVariables = initializedVariables;
    }

    public String getDebugString() {
        StringBuffer ret = new StringBuffer();

        ret.append( "// StatementContext\n" );

        for ( Variable uninitializedVariable : uninitializedVariables ) {
            ret.append( "// U " + uninitializedVariable.getDebugString() + "\n" );
        }

        for ( Variable initializedVariable : initializedVariables ) {
            ret.append( "// I " + initializedVariable.getDebugString() + "\n" );
        }

        return ret.toString();
    }

}
