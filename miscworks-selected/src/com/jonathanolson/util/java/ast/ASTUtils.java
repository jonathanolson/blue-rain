package com.jonathanolson.util.java.ast;

import java.util.HashSet;
import java.util.Set;

import com.jonathanolson.util.java.ast.statements.Block;
import com.jonathanolson.util.java.ast.statements.SimpleFor;

public class ASTUtils {

    public static Set<Variable> collectNodeVariables( Node node ) {
        Set<Variable> ret = new HashSet<Variable>();

        if ( node instanceof SimpleFor ) {
            ret.add( ( (SimpleFor) node ).getIterator() );
        }

        if ( node instanceof Block ) {
            ret.addAll( ( (Block) node ).getVariables() );
        }

        for ( Node child : node.getChildren() ) {
            ret.addAll( collectNodeVariables( child ) );
        }

        return ret;
    }

    public static void checkVariables( Node node ) {
        Set<Variable> vars = collectNodeVariables( node );

        for ( Variable a : vars ) {
            for ( Variable b : vars ) {
                if ( a != b && a.getName().equals( b.getName() ) ) {
                    throw new RuntimeException( "Duplicated variable name!!!" );
                }
            }
        }
    }

}
