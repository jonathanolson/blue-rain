package com.jonathanolson.util.java.ast;

public class Type {

    public static Type INTEGER = new Type( true );
    public static Type FLOAT = new Type( true );
    public static Type DOUBLE = new Type( true );
    public static Type STRING = new Type( true );
    public static Type BOOLEAN = new Type( true );
    public static Type VOID = new Type( true );

    private boolean raw;

    private Type( boolean raw ) {
        this.raw = raw;
    }

    public boolean isRaw() {
        return raw;
    }

    public String toString() {
        if ( isRaw() ) {
            if ( this == INTEGER ) {
                return "int";
            }
            else if ( this == FLOAT ) {
                return "float";
            }
            else if ( this == DOUBLE ) {
                return "double";
            }
            else if ( this == STRING ) {
                return "String";
            }
            else {
                throw new RuntimeException( "Unknown type encountered" );
            }
        }
        else {
            // TODO: for more classes etc...
            return "Object";
        }
    }

    public boolean isBoolean() {
        return this == BOOLEAN;
    }

}
