package com.jonathanolson.util.java.ast;

import java.util.LinkedList;
import java.util.List;

import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

public abstract class Node {

    private boolean mutatable;
    private Node parent;

    protected Node() {
        parent = null;
        mutatable = true;
    }

    protected Node( Node parent ) {
        this.parent = parent;
    }

    public abstract List<Node> getChildren();

    public abstract Class getPossibleClass( Node node ) throws ChildDoesNotExist;

    public abstract void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist;

    public abstract void replaceVariable( Variable src, Variable dest );

    protected void replaceNodeCheck( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        if ( !getPossibleClass( src ).isInstance( dest ) ) {
            throw new InvalidChildReplacement( "Invalid class for replacement" );
        }
    }

    protected List<Node> createEmptyNodeList() {
        return new LinkedList<Node>();
    }

    public boolean isMutatable() {
        return mutatable;
    }

    public void setMutatable( boolean mutatable ) {
        this.mutatable = mutatable;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent( Node parent ) {
        this.parent = parent;
    }

    public void propagateLinks() {
        for ( Node node : getChildren() ) {
            node.setParent( this );
            node.propagateLinks();
        }
    }
}
