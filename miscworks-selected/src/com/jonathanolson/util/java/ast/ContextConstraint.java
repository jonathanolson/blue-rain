package com.jonathanolson.util.java.ast;

import java.util.Set;

public class ContextConstraint {

    private Set<Variable> neededVariables;
    private Set<Variable> assignedVariables;

    public ContextConstraint( Set<Variable> neededVariables, Set<Variable> assignedVariables ) {
        this.neededVariables = neededVariables;
        this.assignedVariables = assignedVariables;
    }

    public Set<Variable> getNeededVariables() {
        return neededVariables;
    }

    public void setNeededVariables( Set<Variable> neededVariables ) {
        this.neededVariables = neededVariables;
    }

    public Set<Variable> getAssignedVariables() {
        return assignedVariables;
    }

    public void setAssignedVariables( Set<Variable> assignedVariables ) {
        this.assignedVariables = assignedVariables;
    }

    public String getDebugString() {
        StringBuffer ret = new StringBuffer();

        ret.append( "// ContextConstraint\n" );

        for ( Variable neededVariable : neededVariables ) {
            ret.append( "// N " + neededVariable.getDebugString() + "\n" );
        }

        for ( Variable assignedVariable : assignedVariables ) {
            ret.append( "// A " + assignedVariable.getDebugString() + "\n" );
        }

        return ret.toString();
    }

}
