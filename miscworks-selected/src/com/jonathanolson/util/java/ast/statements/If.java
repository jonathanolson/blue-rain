package com.jonathanolson.util.java.ast.statements;

import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.*;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;
import com.jonathanolson.util.java.ast.expressions.Expression;

public class If extends Statement implements CompoundStatement {

    private Expression test;
    private Block ifBody;
    private Block elseBody;

    public If( Expression test, Block ifBody, Block elseBody ) {
        this.test = test;
        this.ifBody = ifBody;
        this.elseBody = elseBody;
    }

    public Statement copy() {
        return new If( test.copy(), (Block) ifBody.copy(), ( hasElse() ? (Block) elseBody.copy() : null ) );
    }

    public boolean hasElse() {
        return elseBody != null;
    }

    public String toString() {
        String ret = "if(" + test + ") {\n" + ifBody + "}";
        if ( hasElse() ) {
            ret += " else {\n" + elseBody + "}";
        }
        return ret;
    }

    public ContextConstraint getContextConstraint() {
        Set<Variable> neededVars = createEmptyVariableSet();
        Set<Variable> assignedVars = createEmptyVariableSet();

        ContextConstraint ifConstraint = ifBody.getContextConstraint();

        neededVars.addAll( test.getReferencedVariables() );
        neededVars.addAll( ifConstraint.getNeededVariables() );

        assignedVars.addAll( ifConstraint.getAssignedVariables() );

        if ( hasElse() ) {
            ContextConstraint elseConstraint = elseBody.getContextConstraint();
            neededVars.addAll( elseConstraint.getNeededVariables() );

            // intersection
            assignedVars.retainAll( elseConstraint.getAssignedVariables() );
        }
        else {
            assignedVars.clear();
        }

        return new ContextConstraint( neededVars, assignedVars );
    }

    @Override
    public void updateContext( StatementContext context ) {
        super.updateContext( context );

        ifBody.updateContext( new StatementContext( context ) );

        if ( hasElse() ) {
            elseBody.updateContext( new StatementContext( context ) );
        }
    }

    @Override
    public boolean isValid() {
        Set<Variable> needed = createEmptyVariableSet();

        if ( !test.getType().isBoolean() ) {
            return false;
        }

        needed.addAll( test.getReferencedVariables() );
        needed.addAll( ifBody.getContextConstraint().getNeededVariables() );

        if ( hasElse() ) {
            needed.addAll( elseBody.getContextConstraint().getNeededVariables() );
        }

        Set<Variable> unvars = context.getUninitializedVariables();

        for ( Variable variable : needed ) {
            if ( unvars.contains( variable ) ) {
                return false;
            }
        }

        return super.isValid();
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();

        ret.add( test );
        ret.add( ifBody );
        if ( hasElse() ) {
            ret.add( elseBody );
        }

        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == test ) {
            return Expression.class;
        }
        else if ( node == ifBody ) {
            return Block.class;
        }
        else if ( hasElse() && node == elseBody ) {
            return Block.class;
        }
        else {
            throw new ChildDoesNotExist();
        }
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        if ( src == test ) {
            Expression destExpr = (Expression) dest;
            if ( destExpr.getType() == Type.BOOLEAN ) {
                test = destExpr;
            }
            else {
                throw new InvalidChildReplacement( "Attempted to replace If test with non-boolean expression" );
            }
        }
        else if ( src == ifBody ) {
            ifBody = (Block) dest;
        }
        else if ( hasElse() && src == elseBody ) {
            elseBody = (Block) dest;
        }
    }

    public void replaceVariable( Variable src, Variable dest ) {
        test.replaceVariable( src, dest );
        ifBody.replaceVariable( src, dest );

        if ( hasElse() ) {
            elseBody.replaceVariable( src, dest );
        }
    }

    public Expression getTest() {
        return test;
    }

    public Block getIfBody() {
        return ifBody;
    }

    public Block getElseBody() {
        return elseBody;
    }
}
