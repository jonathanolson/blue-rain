package com.jonathanolson.util.java.ast.statements;

import java.util.HashSet;
import java.util.Set;

import com.jonathanolson.util.java.ast.*;

public abstract class Statement extends Node {

    protected StatementContext context;

    public abstract Statement copy();

    public abstract String toString();

    public abstract ContextConstraint getContextConstraint();

    public boolean isValid() {
        Set<Variable> neededVariables = getContextConstraint().getNeededVariables();
        Set<Variable> assignedVariables = getContextConstraint().getAssignedVariables();
        Set<Variable> uninitializedVariables = context.getUninitializedVariables();
        Set<Variable> initializedVariables = context.getInitializedVariables();

        if ( ASTConstants.DEBUG ) {
            for ( Variable a : neededVariables ) {
                for ( Variable b : neededVariables ) {
                    if ( a != b && a.getName() == b.getName() ) {
                        throw new RuntimeException( "Duplicated variable in neededVariables for statement " + this );
                    }
                }
            }
            for ( Variable a : assignedVariables ) {
                for ( Variable b : assignedVariables ) {
                    if ( a != b && a.getName() == b.getName() ) {
                        throw new RuntimeException( "Duplicated variable in assignedVariables for statement " + this );
                    }
                }
            }
            for ( Variable a : uninitializedVariables ) {
                for ( Variable b : uninitializedVariables ) {
                    if ( a != b && a.getName() == b.getName() ) {
                        throw new RuntimeException( "Duplicated variable in uninitializedVariables for statement " + this );
                    }
                }
            }
            for ( Variable a : initializedVariables ) {
                for ( Variable b : initializedVariables ) {
                    if ( a != b && a.getName() == b.getName() ) {
                        throw new RuntimeException( "Duplicated variable in initializedVariables for statement " + this );
                    }
                }
            }
        }

        for ( Variable uninitializedVariable : uninitializedVariables ) {
            if ( neededVariables.contains( uninitializedVariable ) ) {
                if ( ASTConstants.DEBUG ) {
                    throw new RuntimeException( "Uninitialized variable " + uninitializedVariable.getDebugString() + " is needed for " + this );
                }
                return false;
            }
        }

        for ( Variable neededVariable : neededVariables ) {
            if ( !initializedVariables.contains( neededVariable ) ) {
                if ( ASTConstants.DEBUG ) {
                    throw new RuntimeException( "Needed variable " + neededVariable.getDebugString() + " is not initialized for statement " + this
                                                + "\nStatementContext\n" + getContext().getDebugString() + "ContextConstraint\n" + getContextConstraint().getDebugString() );
                }
                return false;
            }
        }

        return true;
    }

    public void updateContext( StatementContext context ) {
        this.context = context;
    }

    protected Set<Variable> createEmptyVariableSet() {
        return new HashSet<Variable>();
    }

    public StatementContext getContext() {
        return context;
    }

    public void setContext( StatementContext context ) {
        this.context = context;
    }
}
