package com.jonathanolson.util.java.ast.statements;

import java.util.*;

import com.jonathanolson.util.java.ast.*;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

public class Block extends Statement {

    // declared variables
    private Set<Variable> variables;

    private List<Statement> statements;

    private StatementContext endContext;

    public Block() {
        statements = new LinkedList<Statement>();
        variables = new HashSet<Variable>();
    }

    public void addStatement( Statement statement ) {
        statements.add( statement );
    }

    public void removeStatement( Statement statement ) {
        statements.remove( statement );
    }

    public void addVariable( Variable var ) {
        variables.add( var );
    }

    public void removeVariable( Variable var ) {
        variables.remove( var );
    }

    public Statement copy() {

        List<Statement> newStatements = new LinkedList<Statement>();

        for ( Statement statement : statements ) {
            Statement newStatement = statement.copy();
            newStatements.add( newStatement );
        }

        List<Variable> newVariables = new LinkedList<Variable>();
        Map<Variable, Variable> variableMap = new HashMap<Variable, Variable>();

        for ( Variable variable : variables ) {
            Variable newVariable = variable.copy();
            newVariables.add( newVariable );
            variableMap.put( newVariable, variable );
        }

        for ( Statement newStatement : newStatements ) {
            for ( Variable newVariable : newVariables ) {
                newStatement.replaceVariable( variableMap.get( newVariable ), newVariable );
            }
        }

        Block ret = new Block();

        for ( Variable newVariable : newVariables ) {
            ret.addVariable( newVariable );
        }

        for ( Statement newStatement : newStatements ) {
            ret.addStatement( newStatement );
        }

        return ret;
    }

    public String toString() {
        StringBuffer buf = new StringBuffer();

        if ( ASTConstants.DEBUG ) {
            buf.append( "// BLOCK CONTEXTS\n" );
            buf.append( getContext().getDebugString() );
            buf.append( getContextConstraint().getDebugString() );
            buf.append( "// END BLOCK CONTEXTS\n" );
        }

        for ( Variable variable : variables ) {
            buf.append( variable.getType() + " " + variable + ";\n" );
        }

        for ( Statement statement : statements ) {
            if ( ASTConstants.DEBUG ) {
                buf.append( statement.getContext().getDebugString() );
                buf.append( statement.getContextConstraint().getDebugString() );
            }
            buf.append( statement.toString() + "\n" );
        }

        if ( ASTConstants.DEBUG ) {
            buf.append( endContext.getDebugString() );
        }

        return buf.toString();
    }

    public ContextConstraint getContextConstraint() {
        Set<Variable> needed = createEmptyVariableSet();
        Set<Variable> assigned = createEmptyVariableSet();

        for ( Statement statement : statements ) {
            ContextConstraint subContext = statement.getContextConstraint();

            for ( Variable variable : subContext.getNeededVariables() ) {
                if ( !variables.contains( variable ) && !assigned.contains( variable ) ) {
                    needed.add( variable );
                }
            }

            for ( Variable variable : subContext.getAssignedVariables() ) {
                if ( !variables.contains( variable ) ) {
                    assigned.add( variable );
                }
            }
        }

        return new ContextConstraint( needed, assigned );
    }

    public void insertStatement( Statement statement, int index ) {
        statements.add( index, statement );
    }

    public StatementContext getContextAtIndex( int index ) {
        if ( index == statements.size() ) {
            return endContext;
        }
        else {
            return statements.get( index ).getContext();
        }
    }

    // only call on the root level block!
    public void cooldown() {
        updateContext( new StatementContext() );
        propagateLinks();
    }

    @Override
    public void updateContext( StatementContext context ) {
        super.updateContext( context );

        Set<Variable> unvars = createEmptyVariableSet();
        Set<Variable> invars = createEmptyVariableSet();

        for ( Variable variable : variables ) {
            unvars.add( variable );
        }

        unvars.addAll( context.getUninitializedVariables() );
        invars.addAll( context.getInitializedVariables() );

        for ( Statement statement : statements ) {
            statement.updateContext( new StatementContext( unvars, invars ) );
            ContextConstraint contextConstraint = statement.getContextConstraint();
            for ( Variable variable : contextConstraint.getAssignedVariables() ) {
                unvars.remove( variable );
                invars.add( variable );
            }
        }

        endContext = new StatementContext( unvars, invars );
    }

    @Override
    public boolean isValid() {
        for ( Statement statement : statements ) {
            if ( !statement.isValid() ) {
                return false;
            }
        }
        return super.isValid();
    }

    public int getSize() {
        return statements.size();
    }

    public boolean isEmpty() {
        return statements.isEmpty();
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();

        ret.addAll( statements );

        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node instanceof Statement ) {
            Statement statement = (Statement) node;

            if ( statements.contains( statement ) ) {
                return Statement.class;
            }
        }
        throw new ChildDoesNotExist();
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement {
        if ( dest instanceof Statement ) {
            if ( src instanceof Statement ) {
                Statement oldStatement = (Statement) src;
                Statement newStatement = (Statement) dest;

                int index = statements.indexOf( oldStatement );

                if ( index == -1 ) {
                    throw new InvalidChildReplacement( "Could not find statement to replace inside block" );
                }

                statements.set( index, newStatement );
            }
            else {
                throw new InvalidChildReplacement( "Cannot replace non-statement inside block" );
            }
        }
        else {
            throw new InvalidChildReplacement( "Attempted to replace statement with non-statement inside a block" );
        }
    }

    public void replaceVariable( Variable src, Variable dest ) {
        // NOTE: variables in the set should not be replaced! those are variables that are declared
        for ( Statement statement : statements ) {
            statement.replaceVariable( src, dest );
        }
    }

    public StatementContext getEndContext() {
        return endContext;
    }

    public Set<Variable> getVariables() {
        return variables;
    }

    public List<Statement> getStatements() {
        return statements;
    }


}
