package com.jonathanolson.util.java.ast.statements;

import java.util.List;

import com.jonathanolson.util.java.ast.ContextConstraint;
import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;
import com.jonathanolson.util.java.ast.expressions.Expression;

public class ExpressionStatement extends Statement {

    private Expression expr;

    public ExpressionStatement( Expression expr ) {
        this.expr = expr;
    }

    public Expression getExpr() {
        return expr;
    }

    public void setExpr( Expression expr ) {
        this.expr = expr;
    }

    public Statement copy() {
        return new ExpressionStatement( expr.copy() );
    }

    public String toString() {
        return expr.toString() + ";";
    }

    public ContextConstraint getContextConstraint() {
        return new ContextConstraint( expr.getReferencedVariables(), createEmptyVariableSet() );
    }

    public void replaceVariable( Variable src, Variable dest ) {
        expr.replaceVariable( src, dest );
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();

        ret.add( expr );

        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == expr ) {
            return Expression.class;
        }
        else {
            throw new ChildDoesNotExist();
        }
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        expr = (Expression) dest;
    }
}
