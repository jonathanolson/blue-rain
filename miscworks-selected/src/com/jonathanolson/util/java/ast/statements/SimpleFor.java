package com.jonathanolson.util.java.ast.statements;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.*;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;
import com.jonathanolson.util.java.ast.expressions.Expression;

public class SimpleFor extends Statement implements CompoundStatement {

    private Variable iterator;
    private Expression startVal;
    private Expression endVal;
    private Block body;

    private static final int CAP = 10000;

    private static int simpleForCount = 0;
    private int id;

    public SimpleFor( Expression startVal, Expression endVal, Block body ) {
        iterator = new Variable( Type.INTEGER );
        iterator.setMutationSettable( false );
        this.startVal = startVal;
        this.endVal = endVal;
        this.body = body;
        incrementCount();
    }

    // from another one!
    private SimpleFor( Expression startVal, Expression endVal, Block body, String iteratorName ) {
        iterator = new Variable( Type.INTEGER, iteratorName );
        iterator.setMutationSettable( false );
        this.startVal = startVal;
        this.endVal = endVal;
        this.body = body;
        incrementCount();
    }

    private void incrementCount() {
        id = simpleForCount++;
    }

    public Statement copy() {
        SimpleFor ret = new SimpleFor( startVal.copy(), endVal.copy(), (Block) body.copy(), iterator.getName() );
        ret.replaceVariable( iterator, ret.getIterator() );
        return ret;
    }

    public String toString() {
        String ret = "";

        String endName = "end_" + id;

        ret += "int " + endName + " = Math.min(" + endVal + ", " + CAP + ");\n";

        ret += "for(int " + iterator.toString() + " = " + startVal + "; ";
        ret += iterator.toString() + " < " + endName + "; ";
        ret += iterator.toString() + "++ ) {\n";
        //ret += "if ( Thread.currentThread().isInterrupted() ) { throw new InterruptedException(); }\n";
        ret += body;
        ret += "}\n";
        return ret;
    }

    public ContextConstraint getContextConstraint() {
        Set<Variable> neededVars = createEmptyVariableSet();
        Set<Variable> assignedVars = createEmptyVariableSet();

        ContextConstraint bodyConstraint = body.getContextConstraint();

        neededVars.addAll( startVal.getReferencedVariables() );
        neededVars.addAll( endVal.getReferencedVariables() );
        neededVars.addAll( bodyConstraint.getNeededVariables() );

        neededVars.remove( iterator );

        return new ContextConstraint( neededVars, assignedVars );
    }

    public List<Node> getChildren() {
        return Arrays.asList( startVal, endVal, body );
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == startVal ) {
            return Expression.class;
        }
        else if ( node == endVal ) {
            return Expression.class;
        }
        else if ( node == body ) {
            return Block.class;
        }
        else {
            throw new ChildDoesNotExist();
        }
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );
        if ( src == startVal ) {
            startVal = (Expression) dest;
        }
        else if ( src == endVal ) {
            endVal = (Expression) dest;
        }
        else if ( src == body ) {
            body = (Block) dest;
        }
    }

    public void replaceVariable( Variable src, Variable dest ) {
        if ( iterator == src ) {
            iterator = dest;
        }

        startVal.replaceVariable( src, dest );
        endVal.replaceVariable( src, dest );
        body.replaceVariable( src, dest );
    }

    @Override
    public void updateContext( StatementContext context ) {
        super.updateContext( context );

        Set<Variable> invars = createEmptyVariableSet();

        invars.add( iterator );
        invars.addAll( context.getInitializedVariables() );

        body.updateContext( new StatementContext( context.getUninitializedVariables(), invars ) );
    }

    public Expression getStartVal() {
        return startVal;
    }

    public Expression getEndVal() {
        return endVal;
    }

    public Block getBody() {
        return body;
    }

    public Variable getIterator() {
        return iterator;
    }
}
