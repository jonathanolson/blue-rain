package com.jonathanolson.util.java.ast.statements;

import java.util.List;

import com.jonathanolson.util.java.ast.ContextConstraint;
import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;
import com.jonathanolson.util.java.ast.expressions.Expression;
import com.jonathanolson.util.java.ast.expressions.VariableReference;

public class SimpleAssignment extends Statement {

    private VariableReference lhs;
    private Expression rhs;

    public SimpleAssignment( VariableReference lhs, Expression rhs ) {
        this.lhs = lhs;
        this.rhs = rhs;

        lhs.setMutatable( false );
    }

    public Statement copy() {
        return new SimpleAssignment( (VariableReference) lhs.copy(), rhs.copy() );
    }

    public String toString() {
        return lhs + " = " + rhs + ";";
    }

    public ContextConstraint getContextConstraint() {
        return new ContextConstraint( rhs.getReferencedVariables(), lhs.getReferencedVariables() );
    }

    public void replaceVariable( Variable src, Variable dest ) {
        lhs.replaceVariable( src, dest );
        rhs.replaceVariable( src, dest );
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();

        ret.add( lhs );
        ret.add( rhs );

        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == lhs ) {
            return VariableReference.class;
        }
        else if ( node == rhs ) {
            return Expression.class;
        }
        else {
            throw new ChildDoesNotExist();
        }
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        if ( src == lhs ) {
            lhs = (VariableReference) dest;
        }
        else if ( src == rhs ) {
            rhs = (Expression) dest;
        }
    }

    public VariableReference getLeftHandSide() {
        return lhs;
    }

    public Expression getRightHandSide() {
        return rhs;
    }
}
