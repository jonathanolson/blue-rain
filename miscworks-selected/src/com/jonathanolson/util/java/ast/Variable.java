package com.jonathanolson.util.java.ast;

public class Variable {

    private static int defaultNameCount = 0;

    private String name;
    private Type type;
    private boolean mutationSettable = true;

    private static int uniqueCount = 0;
    private int uniqueId;

    public Variable( Type type ) {
        this( type, "v" + String.valueOf( defaultNameCount ) );
        defaultNameCount++;
        uniqueId = uniqueCount++;
    }

    public Variable( Type type, String name ) {
        this.type = type;
        this.name = name;
        uniqueId = uniqueCount++;
    }

    public String toString() {
        // TODO: reference from context?       
        return name;
    }

    public Type getType() {
        return type;
    }

    public Variable copy() {
        return new Variable( type, name );
    }

    public boolean isMutationSettable() {
        return mutationSettable;
    }

    public void setMutationSettable( boolean mutationSettable ) {
        this.mutationSettable = mutationSettable;
    }

    public String getName() {
        return name;
    }

    public String getDebugString() {
        return "[var name:" + name + " type:" + type.toString() + " id:" + String.valueOf( uniqueId ) + "]";
    }
}
