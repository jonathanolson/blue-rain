package com.jonathanolson.util.java.ast.expressions;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.Function;
import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

// TODO: add validity check for arguments?
public class FunctionCall extends Expression {

    private Function function;
    private List<Expression> args;

    public FunctionCall( Function function, Expression[] args ) {
        this.function = function;
        this.args = new LinkedList<Expression>();
        for ( Expression arg : args ) {
            this.args.add( arg );
        }
    }

    public FunctionCall( Function function, List<Expression> args ) {
        this.function = function;
        this.args = args;
    }

    public Expression copy() {
        List<Expression> newArgs = new LinkedList<Expression>();
        for ( Expression arg : args ) {
            newArgs.add( arg.copy() );
        }
        //newArgs.addAll( args ); (DAMN YOU THIS LINE!)
        return new FunctionCall( function.copy(), newArgs );
    }

    public Type getType() {
        return function.getReturnType();
    }

    public Set<Variable> getReferencedVariables() {
        Set<Variable> ret = new HashSet<Variable>();
        for ( Expression arg : args ) {
            ret.addAll( arg.getReferencedVariables() );
        }
        return ret;
    }

    public String toString() {
        return function.callString( args );
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();
        ret.addAll( args );
        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node instanceof Expression ) {
            Expression expr = (Expression) node;
            int index = args.indexOf( expr );

            if ( index >= 0 ) {
                return Expression.class;
            }
        }
        throw new ChildDoesNotExist();
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        Expression srcExpr = (Expression) src;
        int index = args.indexOf( srcExpr );
        args.set( index, (Expression) dest );
    }

    public void replaceVariable( Variable src, Variable dest ) {
        for ( Expression arg : args ) {
            arg.replaceVariable( src, dest );
        }
    }


}
