package com.jonathanolson.util.java.ast.expressions;

import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

public abstract class TerminalExpression extends Expression {
    public Set<Variable> getReferencedVariables() {
        return getEmptyVariableSet();
    }

    public void replaceVariable( Variable src, Variable dest ) {

    }

    public List<Node> getChildren() {
        return createEmptyNodeList();
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        throw new ChildDoesNotExist();
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        throw new ChildDoesNotExist();
    }
}
