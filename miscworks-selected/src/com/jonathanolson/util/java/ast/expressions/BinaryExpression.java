package com.jonathanolson.util.java.ast.expressions;

import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

public abstract class BinaryExpression extends Expression {

    protected Expression left;
    protected Expression right;

    protected BinaryExpression( Expression left, Expression right ) {
        this.left = left;
        this.right = right;
    }

    public Set<Variable> getReferencedVariables() {
        Set<Variable> ret = left.getReferencedVariables();
        ret.addAll( right.getReferencedVariables() );
        return ret;
    }

    public void replaceVariable( Variable src, Variable dest ) {
        left.replaceVariable( src, dest );
        right.replaceVariable( src, dest );
    }

    //----------------------------------------------------------------------------
    // Getters and setters
    //----------------------------------------------------------------------------

    public Expression getLeft() {
        return left;
    }

    public void setLeft( Expression left ) {
        this.left = left;
    }

    public Expression getRight() {
        return right;
    }

    public void setRight( Expression right ) {
        this.right = right;
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();
        ret.add( left );
        ret.add( right );
        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == left || node == right ) {
            return Expression.class;
        }

        throw new ChildDoesNotExist();
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        if ( src == left ) {
            left = (Expression) dest;
        }
        else if ( src == right ) {
            right = (Expression) dest;
        }
    }
}
