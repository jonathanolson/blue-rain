package com.jonathanolson.util.java.ast.expressions;

import java.util.Set;

import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;

public class VariableReference extends TerminalExpression {

    private Variable variable;

    public VariableReference( Variable variable ) {
        this.variable = variable;
    }

    public String toString() {
        return variable.toString();
    }

    public Expression copy() {
        // variable should be changed later
        return new VariableReference( variable );
    }

    public Type getType() {
        return variable.getType();
    }

    public void replaceVariable( Variable src, Variable dest ) {
        if ( getVariable() == src ) {
            setVariable( dest );
        }
    }

    public Set<Variable> getReferencedVariables() {
        Set<Variable> ret = getEmptyVariableSet();
        ret.add( variable );
        return ret;
    }

    public Variable getVariable() {
        return variable;
    }

    public void setVariable( Variable variable ) {
        this.variable = variable;
    }

}
