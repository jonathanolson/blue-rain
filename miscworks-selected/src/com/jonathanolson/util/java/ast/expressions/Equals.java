package com.jonathanolson.util.java.ast.expressions;

import com.jonathanolson.util.java.ast.Type;

public class Equals extends BinaryExpression {

    public Equals( Expression left, Expression right ) {
        super( left, right );
    }

    public Expression copy() {
        return new Equals( left.copy(), right.copy() );
    }

    public Type getType() {
        return Type.BOOLEAN;
    }

    public String toString() {
        return "(" + left + ") == (" + right + ")";
    }


}
