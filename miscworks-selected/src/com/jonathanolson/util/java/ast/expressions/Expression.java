package com.jonathanolson.util.java.ast.expressions;

import java.util.HashSet;
import java.util.Set;

import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Type;
import com.jonathanolson.util.java.ast.Variable;

public abstract class Expression extends Node {

    public abstract Expression copy();

    public abstract Type getType();

    public abstract Set<Variable> getReferencedVariables();

    public abstract String toString();

    protected Set<Variable> getEmptyVariableSet() {
        return new HashSet<Variable>();
    }

}
