package com.jonathanolson.util.java.ast.expressions;

import java.util.List;
import java.util.Set;

import com.jonathanolson.util.java.ast.Node;
import com.jonathanolson.util.java.ast.Variable;
import com.jonathanolson.util.java.ast.exceptions.ChildDoesNotExist;
import com.jonathanolson.util.java.ast.exceptions.InvalidChildReplacement;

public abstract class UnaryExpression extends Expression {

    protected Expression expr;

    protected UnaryExpression( Expression expr ) {
        this.expr = expr;
    }

    public Set<Variable> getReferencedVariables() {
        return expr.getReferencedVariables();
    }

    public void replaceVariable( Variable src, Variable dest ) {
        expr.replaceVariable( src, dest );
    }

    //----------------------------------------------------------------------------
    // Getters and setters
    //----------------------------------------------------------------------------

    public Expression getExpr() {
        return expr;
    }

    public void setExpr( Expression expr ) {
        this.expr = expr;
    }

    public List<Node> getChildren() {
        List<Node> ret = createEmptyNodeList();
        ret.add( expr );
        return ret;
    }

    public Class getPossibleClass( Node node ) throws ChildDoesNotExist {
        if ( node == expr ) {
            return Expression.class;
        }

        throw new ChildDoesNotExist();
    }

    public void replaceChild( Node src, Node dest ) throws InvalidChildReplacement, ChildDoesNotExist {
        replaceNodeCheck( src, dest );

        expr = (Expression) dest;
    }
}
