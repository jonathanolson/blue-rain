package com.jonathanolson.util.java.ast.expressions;

import com.jonathanolson.util.java.ast.Type;

public class IntegerConstant extends TerminalExpression {

    private int value;

    public IntegerConstant( int value ) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public String toString() {
        return String.valueOf( value );
    }

    public Expression copy() {
        return new IntegerConstant( value );
    }

    public Type getType() {
        return Type.INTEGER;
    }
}
