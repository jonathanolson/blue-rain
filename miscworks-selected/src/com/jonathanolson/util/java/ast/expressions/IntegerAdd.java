package com.jonathanolson.util.java.ast.expressions;

import com.jonathanolson.util.java.ast.Type;

public class IntegerAdd extends BinaryExpression {

    public IntegerAdd( Expression left, Expression right ) {
        super( left, right );
    }

    public String toString() {
        return "(" + left + ") + (" + right + ")";
    }

    public Expression copy() {
        return new IntegerAdd( left.copy(), right.copy() );
    }

    public Type getType() {
        return Type.INTEGER;
    }
}
