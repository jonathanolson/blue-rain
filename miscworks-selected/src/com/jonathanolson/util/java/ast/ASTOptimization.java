package com.jonathanolson.util.java.ast;

import com.jonathanolson.util.java.ast.expressions.VariableReference;
import com.jonathanolson.util.java.ast.statements.If;
import com.jonathanolson.util.java.ast.statements.SimpleAssignment;
import com.jonathanolson.util.java.ast.statements.SimpleFor;
import com.jonathanolson.util.java.ast.statements.Statement;

public class ASTOptimization {

    public static boolean isStatementStandaloneUseless( Statement statement ) {
        if ( statement instanceof SimpleFor ) {
            SimpleFor stmt = (SimpleFor) statement;

            if ( stmt.getBody().isEmpty() ) {
                return true;
            }
        }

        if ( statement instanceof If ) {
            If stmt = (If) statement;

            if ( stmt.getIfBody().isEmpty() && ( !stmt.hasElse() || stmt.getElseBody().isEmpty() ) ) {
                return true;
            }
        }

        if ( statement instanceof SimpleAssignment ) {
            SimpleAssignment assignment = (SimpleAssignment) statement;

            if ( assignment.getRightHandSide() instanceof VariableReference && assignment.getLeftHandSide().getVariable() == ( (VariableReference) assignment.getRightHandSide() ).getVariable() ) {
                return true;
            }
        }

        return false;
    }

}
