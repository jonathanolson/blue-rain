package com.jonathanolson.util.java.ast.exceptions;

public class InvalidChildReplacement extends Exception {
    public InvalidChildReplacement() {
    }

    public InvalidChildReplacement( String s ) {
        super( s );
    }
}
