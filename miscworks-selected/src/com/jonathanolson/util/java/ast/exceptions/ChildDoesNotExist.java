package com.jonathanolson.util.java.ast.exceptions;

public class ChildDoesNotExist extends Exception {

    public ChildDoesNotExist() {
    }

    public ChildDoesNotExist( String s ) {
        super( s );
    }
}
