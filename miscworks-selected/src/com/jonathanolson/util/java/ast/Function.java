package com.jonathanolson.util.java.ast;

import java.util.List;

import com.jonathanolson.util.java.ast.expressions.Expression;

public class Function {

    private String name;
    private List<Type> specification;
    private Type returnType;

    public Function( String name, List<Type> specification, Type returnType ) {
        this.name = name;
        this.specification = specification;
        this.returnType = returnType;
    }

    public String callString( List<Expression> args ) {
        String ret = name;
        ret += "(";
        for ( int i = 0; i < args.size(); i++ ) {
            Expression expression = args.get( i );
            if ( i > 0 ) {
                ret += ", ";
            }

            ret += expression;
        }
        ret += ")";
        return ret;
    }

    public Function copy() {
        return new Function( name, specification, returnType );
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public List<Type> getSpecification() {
        return specification;
    }

    public Type getReturnType() {
        return returnType;
    }

    public void setReturnType( Type returnType ) {
        this.returnType = returnType;
    }
}
