package com.jonathanolson.util.math;

public class Interpolation {

    /**
     * Return x scaled from inbetween a and b => scaled inbetween c and d
     *
     * @param a Low domain
     * @param b High domain
     * @param c Low range
     * @param d High range
     * @param x Domain value
     * @return Range value
     */
    public static double fit( double a, double b, double c, double d, double x ) {
        return ( ( x - a ) / ( b - a ) ) * ( d - c ) + c;
    }

    /**
     * Returns a number between a and b corresponding to the fraction that b is involved.
     * IE approx( a, b, 0 ) == a, approx( a, b, 1 ) == b
     *
     * @param a        Low number
     * @param b        High number
     * @param fraction Fraction of the high number
     * @return Blended result
     */
    public static double approx( double a, double b, double fraction ) {
        return a + ( b - a ) * fraction;
    }

    /**
     * Same scaling for approx, but now with 3-length arrays of doubles
     *
     * @param a        A 3-length array (low)
     * @param b        A 3-length array (high)
     * @param fraction Fraction between the two
     * @return Blended result
     */
    public static double[] approx3( double[] a, double[] b, double fraction ) {
        double[] ret = new double[3];
        ret[0] = a[0] + ( b[0] - a[0] ) * fraction;
        ret[1] = a[1] + ( b[1] - a[1] ) * fraction;
        ret[2] = a[2] + ( b[2] - a[2] ) * fraction;
        return ret;
    }
}
