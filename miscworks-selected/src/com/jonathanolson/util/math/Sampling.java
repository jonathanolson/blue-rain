package com.jonathanolson.util.math;

public class Sampling {
    private static final double[] zero3 = new double[]{0, 0, 0};

    // TODO: add ability to specify defaults in fromSamples versions

    /**
     * Given an array of data, where the first element is represented by min, the last element is represented by max,
     * this will return an interpolated sample given a number between min and max corresponding to the closest data
     * in the data.
     *
     * @param data Samples
     * @param min  Min value
     * @param max  Max value
     * @param x    Value parameter
     * @return Interpolated value
     */
    public static double fromSamples( double[] data, double min, double max, double x ) {
        double rmax = data.length - 1;
        double y = ( ( x - min ) / ( max - min ) ) * rmax;
        if ( y > (double) 0 && y < rmax ) {
            int ip = (int) y;
            return Interpolation.approx( data[ip], data[ip + 1], y - ( (double) ip ) );
        }
        else {
            return 0;
        }
    }

    /**
     * Given an array of data, where the first element is represented by min, the last element is represented by max,
     * this will return an interpolated sample given a number between min and max corresponding to the closest data
     * in the data. (This is the 3-vector double version)
     *
     * @param data Samples
     * @param min  Min value
     * @param max  Max value
     * @param x    Value parameter
     * @return Interpolated value
     */
    public static double[] fromSamples3( double[][] data, double min, double max, double x ) {
        double rmax = data.length - 1;
        double y = ( ( x - min ) / ( max - min ) ) * rmax;
        if ( y > (double) 0 && y < rmax ) {
            int ip = (int) y;
            return Interpolation.approx3( data[ip], data[ip + 1], y - ( (double) ip ) );
        }
        else {
            return zero3;
        }
    }
}
