package com.jonathanolson.util.math;

import java.util.Random;

public class RandomUtils {
    private static Random random = null;

    public static Random getInstance() {
        if ( random == null ) {
            random = new Random( System.currentTimeMillis() );
        }

        return random;
    }

    public static int nextInt( int high ) {
        return getInstance().nextInt( high );
    }

    public static final ThreadLocal<Random> localRandom = new ThreadLocal<Random>() {
        @Override
        protected Random initialValue() {
            return new Random( (long) Math.random() ); // syncs, so we should get different seeds for different threads
        }
    };

    public static double random() {
        return localRandom.get().nextDouble();
    }

}
