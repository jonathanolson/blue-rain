package com.jonathanolson.util.math;

public class MathUtils {

    public static int integerModulus( int a, int b ) {
        return ( a % b + b ) % b;
    }

}
