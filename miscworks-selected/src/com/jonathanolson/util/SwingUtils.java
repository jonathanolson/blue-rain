package com.jonathanolson.util;

import java.awt.*;

import javax.swing.*;

public class SwingUtils {
    public static void popupFrame( final String title, final Component component ) {
        SwingUtilities.invokeLater( new Runnable() {
            public void run() {
                JFrame frame = new JFrame( title );
                frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

                JPanel panel = new JPanel( new GridLayout( 1, 1 ) );
                frame.setContentPane( panel );

                panel.add( component );

                frame.pack();
                frame.setVisible( true );
            }
        } );
    }
}
