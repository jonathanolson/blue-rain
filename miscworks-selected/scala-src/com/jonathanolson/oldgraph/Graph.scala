package com.jonathanolson.oldgraph

import collection.mutable.{PriorityQueue, ListBuffer, Set => MSet, Map => MMap}

// TODO: edges and vertices with info? possibly per-graph vertices and edges then?

trait Graph[V, E] {
    def vertices: collection.Set[V] // all vertices
    def edges: collection.Set[E] // all edges

    def edgesOf( v: V ): Iterable[E] // all edges touching vertex
    def edgesOf( a: V, b: V ): Iterable[E] // all edges from a to b

    def verticesOf( e: E ): (V, V) // vertices of an edge TODO: make set instead? better undirected support?

    def isEndOf( e: E, v: V ): Boolean = {
        require( edges.contains( e ) )
        val (a, b) = verticesOf( e )
        (v == a) || (v == b)
    }

    def otherEnd( e: E, v: V ): V = {
        require( edges.contains( e ) )
        require( vertices.contains( v ) )
        val (a, b) = verticesOf( e )
        require( (v == a) || (v == b) )
        if ( v == a ) b else a
    }

    def isIncidentTo( e: E, a: V, b: V ): Boolean // whether an edge goes from A to B
    def isHeadOf( e: E, v: V ): Boolean // whether v could be the head
    def isTailOf( e: E, v: V ): Boolean // whether v could be the tail

    def order: Int = vertices.size

    def size: Int = edges.size

    def degreeOf( v: V ): Int = { // vertex degree
        require( vertices.contains( v ) )
        var ret = 0
        for (e <- edgesOf( v )) {
            ret += 1
            val verts = verticesOf( e )
            if ( verts._1 == verts._2 ) {
                ret += 1 // must be a loop, double count it
            }
        }
        ret
    }

    // TODO: better name. + version that allows arbitrary metrics?
    def unweightedDijkstraIterator( startVertex: V ) = new Iterator[(Int, V)] { // distance, vertex in closest order
        require( vertices.contains( startVertex ) )

        val queue: PriorityQueue[Entry] = new PriorityQueue[Entry]
        val marked: MSet[V] = MSet.empty
        case class Entry( val d: Int, val v: V ) extends Ordered[Entry] {
            def compare( that: Entry ) = -this.d.compare( that.d )
        }

        marked += startVertex
        queue += Entry( 0, startVertex )

        def next(): (Int, V) = {
            var nextEntry: Entry = queue.dequeue
            val v: V = nextEntry.v
            val d: Int = nextEntry.d
            for (
                e <- edgesOf( v );
                if isTailOf( e, v );
                other = otherEnd( e, v );
                if !marked.contains( other )
            ) {
                marked += other
                queue += Entry( d + 1, other )
            }
            (d, v)
        }

        def hasNext = !queue.isEmpty
    }

}

trait SimpleGraph[V, E] { // no loops, no multi-edges
    def edgeOf( a: V, b: V ): Option[E] // edge from a to b (or if undirected b to a)
}

trait ImmutableGraph[G <: Graph[V, E], V, E] {
    def withVertex( v: V ): G

    def withEdge( a: V, b: V, e: E ): G
}

trait MutableGraph[V, E] {
    def addVertex( v: V ): Unit

    def addEdge( a: V, b: V, e: E ): Unit
}

trait UndirectedGraph[V, E] extends Graph[V, E] {
    def isIncidentTo( e: E, a: V, b: V ) = {
        val (x, y) = verticesOf( e )
        ((x == a) && (y == b)) || ((x == b) && (y == a))
    }

    def isHeadOf( e: E, v: V ) = isEndOf( e, v )

    def isTailOf( e: E, v: V ) = isEndOf( e, v )

}
trait DirectedGraph[V, E] extends Graph[V, E] {
    def headOf( e: E ): V = verticesOf( e )._1

    def tailOf( e: E ): V = verticesOf( e )._2

    def isIncidentTo( e: E, a: V, b: V ) = {
        val (x, y) = verticesOf( e )
        (x == a) && (y == b)
    }

    def isHeadOf( e: E, v: V ) = headOf( e ) == v

    def isTailOf( e: E, v: V ) = tailOf( e ) == v

}

trait WeightedGraph[V, E] {
    def weightedDijkstraIterator( startVertex: V ): Iterator[(Int, V)]
}

/*---------------------------------------------------------------------------*
* implementation
*----------------------------------------------------------------------------*/

trait ListSimpleGraph[V, E] extends SimpleGraph[V, E] {
    protected def vertexData: collection.Map[V, Seq[(V, E)]]

    protected def edgeData: collection.Map[E, (V, V)]

    def vertices: collection.Set[V]

    def edges: collection.Set[E]

    def edgeOf( a: V, b: V ): Option[E] = vertexData( a ).find( t => t._1 == b ) match {
        case Some( (_, e) ) => Some( e )
        case None => None
    }

    def verticesOf( e: E ) = edgeData( e )

    def edgesOf( a: V, b: V ) = for ((v, e) <- vertexData( a ); if v == b) yield e

    def edgesOf( v: V ) = for ((_, e) <- vertexData( v )) yield e

    override def toString = { // TODO: move to a trait?
        def edgeString( t: Tuple2[V, E] ): String = t._1.toString + "(" + t._2.toString + ")"
        def lineString( v: V ): String = v.toString + ": " + (for (t: Tuple2[V, E] <- vertexData( v )) yield edgeString( t )).mkString( ", " )
        val lines = for (a <- vertexData.keys) yield lineString( a )
        "vertices: " + (vertices mkString ",") + "\nedges: " + (edges mkString ",") + "\ndata:\n" + (lines mkString "\n")
    }
}

class DefaultMGraph[V, E] extends ListSimpleGraph[V, E] with UndirectedGraph[V, E] with MutableGraph[V, E] {
    protected val vertexData: MMap[V, ListBuffer[(V, E)]] = MMap.empty
    protected val edgeData: MMap[E, (V, V)] = MMap.empty
    val vertices: MSet[V] = MSet.empty
    val edges: MSet[E] = MSet.empty

    def addVertex( v: V ): Unit = {
        require( !vertices.contains( v ) )
        vertexData += (v -> new ListBuffer[(V, E)])
        vertices += v
    }

    def addEdge( a: V, b: V, e: E ): Unit = {
        require( !edges.contains( e ) )
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )
        vertexData( a ) += (b -> e)
        vertexData( b ) += (a -> e)
        edgeData += (e -> (a, b))
        edges += e
    }
}

object DefaultMGraph {
    def empty[V, E]: DefaultMGraph[V, E] = new DefaultMGraph[V, E]
}

class DefaultGraph[V, E](
        protected val vertexData: Map[V, List[(V, E)]],
        protected val edgeData: Map[E, (V, V)],
        val vertices: Set[V],
        val edges: Set[E]
        ) extends ListSimpleGraph[V, E] with UndirectedGraph[V, E] with ImmutableGraph[DefaultGraph[V, E], V, E] { // simple, undirected, immutable

    def withVertex( v: V ): DefaultGraph[V, E] = { // TODO: covariant?
        require( !vertices.contains( v ) )
        new DefaultGraph[V, E](
            vertexData + (v -> Nil),
            edgeData,
            vertices + v,
            edges )
    }

    def withEdge( a: V, b: V, e: E ): DefaultGraph[V, E] = {
        require( !edges.contains( e ) ) // TODO: add these elsewhere. maybe ImmutableSimpleGraph? or other
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )
        new DefaultGraph[V, E](
            vertexData.updated( a, (b, e) :: vertexData( a ) ).updated( b, (a, e) :: vertexData( b ) ),
            edgeData + (e -> (a, b)),
            vertices,
            edges + e )
    }

}

object DefaultGraph {
    def empty[V, E]: DefaultGraph[V, E] = new DefaultGraph[V, E]( Map[V, List[(V, E)]](), Map[E, (V, V)](), Set[V](), Set[E]() )
}

object GraphTest {
    def main( args: Array[String] ) {
        println( "Test" )

        var g = DefaultGraph.empty[Int, Int]
        println( g )
        g = g.withVertex( 1 )
        g = g.withVertex( 2 )
        g = g.withVertex( 3 )
        println( g )
        g = g.withEdge( 1, 2, 10 )
        g = g.withEdge( 1, 3, 20 )
        println( g )
        println( g.edgesOf( 1 ) )
        println( g.order )

        val m = DefaultMGraph.empty[Int, Int]
        println( m )
        List( 1, 2, 3, 4, 5, 6 ).foreach( m.addVertex _ )
        m.addEdge( 1, 2, 10 )
        m.addEdge( 1, 3, 20 )
        m.addEdge( 2, 4, 0 )
        m.addEdge( 3, 4, 1 ) // TODO: what if we don't care about the edge? edge factory? YES. also vertex factory
        m.addEdge( 5, 4, 5 )
        println( m )

        // TODO: add unit tests

        println( m.unweightedDijkstraIterator( 1 ) mkString ", " )

    }
}