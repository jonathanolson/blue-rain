package com.jonathanolson.math

trait Dat2[ T ] {
    val x: T
    val y: T
}

trait Matrix2[ T ] {
    val m00: T
    val m01: T
    val m10: T
    val m11: T
}

trait Tuple2d extends Dat2[ Double ] {
    val x: Double
    val y: Double

    def dot( other: Tuple2d ) = x * other.x + y * other.y

    def magnitudeSquared = dot( this )

    def magnitude = scala.math.sqrt( magnitudeSquared )

    def canEqual( other: Any ) = other.isInstanceOf[ Tuple2d ]

    override def equals( other: Any ) = other match {
        case that: Tuple2d => ( that canEqual this ) && ( this.x == that.x ) && ( this.y == that.y )
        case _ => false
    }

    override def hashCode = {
        var bits: Long = 1L
        bits = 31L * bits + longbits( x )
        bits = 31L * bits + longbits( y )
        ( bits ^ ( bits >> 32 ) ).asInstanceOf[ Int ]
    }

    override def toString = "(" + x + ", " + y + ")"

    private def longbits( d: Double ): Long = if ( d == 0 ) 0 else java.lang.Double.doubleToLongBits( d )
}

trait Tuple2i extends Dat2[ Int ] {
    val x: Int
    val y: Int

    def canEqual( other: Any ) = other.isInstanceOf[ Tuple2i ]

    override def equals( p1: Any ) = p1 match {
        case that: Tuple2i => ( that canEqual this ) && ( this.x == that.x ) && ( this.y == that.y )
        case _ => false
    }

    override def hashCode = {
        var bits: Int = x;
        bits = 31 * 256 * bits + y
        bits
    }

    override def toString = "(" + x + ", " + y + ")"
}

class Point2d(
        val x: Double,
        val y: Double
        ) extends Tuple2d {
    def this() = this ( 0, 0 )

    def map( f: ( Double ) => Double ) = new Point2d( f( x ), f( y ) )

    def +( other: Tuple2d ) = new Point2d( x + other.x, y + other.y )

    def -( other: Tuple2d ) = new Point2d( x - other.x, y - other.y )

    /**
     * Multiplication by a constant
     */
    def *( s: Double ) = scale( s ) // TODO: reverse

    /**
     * Component-wise multiplication
     */
    def multiplyComponents( t: Tuple2d ) = new Point2d( x * t.x, y * t.y )

    /**
     * Division by a constant
     */
    def /( s: Double ) = {
        require( s != 0 )
        new Point2d( x / s, y / s )
    }

    /**
     * Negation
     */
    def unary_-() = new Point2d( -x, -y )

    /**
     * Scaling
     */
    def scale( s: Double ) = new Point2d( x * s, y * s )

    /**
     * Manhattan distance
     */
    def distanceL1( other: Point2d ) = ( x - other.x ).abs + ( y - other.y ).abs
}

object Point2d {
    val ZERO = new Point2d( 0, 0 )
}

class Vector2d(
        val x: Double,
        val y: Double
        ) extends Tuple2d {
    def this() = this ( 0, 0 )

    def map( f: ( Double ) => Double ) = new Vector2d( f( x ), f( y ) )

    def +( other: Vector2d ) = new Vector2d( x + other.x, y + other.y )

    def +( point: Point2d ) = new Point2d( x + point.x, y + point.y )

    def -( other: Vector2d ) = new Vector2d( x - other.x, y - other.y )

    def -( point: Point2d ) = new Point2d( x - point.x, y - point.y )

    def *( s: Double ) = scale( s ) // TODO: reverse

    def /( s: Double ) = {
        require( s != 0 )
        new Vector2d( x / s, y / s )
    }

    def unary_-() = new Vector2d( -x, -y )

    def scale( s: Double ) = new Vector2d( x * s, y * s )

    def normalize = this / magnitude

    /**
     * Returns the angle in radians between this vector and the vector
     * parameter; the return value is constrained to the range [0,PI].
     *
     * @param v1 the other vector
     * @return the angle in radians in the range [0,PI]
     */
    final def angle( other: Vector2d ): Double = {
        var vDot: Double = ( this dot other ) / ( this.magnitude * other.magnitude )
        if ( vDot < -1.0 ) {vDot = -1.0}
        if ( vDot > 1.0 ) {vDot = 1.0}
        return ( ( scala.math.acos( vDot ) ).asInstanceOf[ Double ] )
    }

}

object Vector2d {
    val ZERO = new Vector2d( 0, 0 )
}

class Point2i(
        val x: Int,
        val y: Int
        ) extends Tuple2i {
    def this() = this ( 0, 0 )
}

class Matrix2d(
        val m00: Double,
        val m01: Double,
        val m10: Double,
        val m11: Double
        ) extends Matrix2[ Double ] {
    def this() = this ( 0, 0, 0, 0 )

    // TODO: implement equals / hashcode
    // TODO: implement epsilon equals
    // TODO: consolidate matrix calls into common place?

    private val EPS: Double = 1.110223024E-16
    private val ERR_EPS: Double = 1.0E-8

    def map( f: ( Double ) => Double ) = new Matrix2d(
        f( m00 ), f( m01 ),
        f( m10 ), f( m11 )
        )

    def map2( other: Matrix2d, f: (Double, Double) => Double ) = new Matrix2d(
        f( this.m00, other.m00 ), f( this.m01, other.m01 ),
        f( this.m10, other.m10 ), f( this.m11, other.m11 )
        )

    def +( other: Matrix2d ) = map2( other, (a: Double, b: Double) => a + b )

    def +( v: Double ) = map( (d: Double) => d + v )

    def -( other: Matrix2d ) = map2( other, (a: Double, b: Double) => a - b )

    def -( v: Double ) = map( (d: Double) => d - v )

    def unary_-() = map( (d: Double) => -d )

    def *( vec: Point2d ): Point2d = new Point2d(
        m00 * vec.x + m01 * vec.y,
        m10 * vec.x + m11 * vec.y
        )

    def *( vec: Vector2d ): Vector2d = new Vector2d(
        m00 * vec.x + m01 * vec.y,
        m10 * vec.x + m11 * vec.y
        )

    def *( other: Matrix2d ) = new Matrix2d(
        this.m00 * other.m00 + this.m01 * other.m10,
        this.m00 * other.m01 + this.m01 * other.m11,

        this.m10 * other.m00 + this.m11 * other.m10,
        this.m10 * other.m01 + this.m11 * other.m11
        )

    def *( v: Double ) = map( (d: Double) => d * v )

    def transpose = new Matrix2d(
        m00, m10,
        m01, m11
        )

    def inverse: Matrix2d = {
        new Matrix2d( m11, -m01, -m10, m00 ) * ( 1 / determinant )
    }

    def determinant: Double = {
        m00 * m11 - m01 * m10
    }

    // add rotation matrix

    override def toString = "{{" + m00 + ", " + m01 + "},{" + m10 + ", " + m11 + "}}"

}

object Matrix2d {
    val Identity = new Matrix2d( 1, 0, 0, 1 )
}
