package com.jonathanolson.math

object Numbers {
    def equalsEpsilon( x: Double, y: Double, epsilon: Double ): Boolean = {
        return scala.math.abs( x - y ) < epsilon
    }

    def equalsEpsilon( x: Double, y: Double ): Boolean = {
        return scala.math.abs( x - y ) < EPSILON
    }

    def zeroEpsilon( x: Double ): Boolean = {
        return scala.math.abs( x ) < EPSILON
    }

    val EPSILON: Double = 0.00000001
}
