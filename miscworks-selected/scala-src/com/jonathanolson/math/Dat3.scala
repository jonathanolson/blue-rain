package com.jonathanolson.math

import _root_.com.jonathanolson.util.math.vec.{Matrix3d => VMatrix3d}

/**
 * A point in 3 dimensions
 */
trait Dat3[ T ] {
    val x: T
    val y: T
    val z: T
}

trait Matrix3[ T ] {
    /**
     * The first matrix element in the first row.
     */
    val m00: T


    /**
     * The second matrix element in the first row.
     */
    val m01: T


    /**
     * The third matrix element in the first row.
     */
    val m02: T


    /**
     * The first matrix element in the second row.
     */
    val m10: T


    /**
     * The second matrix element in the second row.
     */
    val m11: T


    /**
     * The third matrix element in the second row.
     */
    val m12: T


    /**
     * The first matrix element in the third row.
     */
    val m20: T


    /**
     * The second matrix element in the third row.
     */
    val m21: T


    /**
     * The third matrix element in the third row.
     */
    val m22: T

}

trait Tuple3d extends Dat3[ Double ] {
    val x: Double

    val y: Double

    val z: Double

    def dot( other: Tuple3d ) = x * other.x + y * other.y + z * other.z

    def magnitudeSquared = dot( this )

    def magnitude = scala.math.sqrt( magnitudeSquared )

    def canEqual( other: Any ) = other.isInstanceOf[ Tuple3d ]

    override def equals( other: Any ) = other match {
        case that: Tuple3d => ( that canEqual this ) && ( this.x == that.x ) && ( this.y == that.y ) && ( this.z == that.z )
        case _ => false
    }

    override def hashCode = {
        var bits: Long = 1L
        bits = 31L * bits + longbits( x )
        bits = 31L * bits + longbits( y )
        bits = 31L * bits + longbits( z )
        ( bits ^ ( bits >> 32 ) ).asInstanceOf[ Int ]
    }

    override def toString = "(" + x + ", " + y + ", " + z + ")"

    private def longbits( d: Double ): Long = if ( d == 0 ) 0 else java.lang.Double.doubleToLongBits( d )
}

trait Tuple3i extends Dat3[ Int ] {
    val x: Int
    val y: Int
    val z: Int

    def canEqual( other: Any ) = other.isInstanceOf[ Tuple3i ]

    override def equals( p1: Any ) = p1 match {
        case that: Tuple3i => ( that canEqual this ) && ( this.x == that.x ) && ( this.y == that.y ) && ( this.z == that.z )
        case _ => false
    }

    override def hashCode = {
        var bits: Int = x;
        bits = 31 * 256 * bits + y
        bits = 31 * 256 * bits + z
        bits
    }

    override def toString = "(" + x + ", " + y + ", " + z + ")"
}

class Point3d(
        val x: Double,
        val y: Double,
        val z: Double
        ) extends Tuple3d {
    def this() = this ( 0, 0, 0 )

    def map( f: ( Double ) => Double ) = new Point3d( f( x ), f( y ), f( z ) )

    def mapComponent( f: ( Double ) => Double, component: Int ) = component match {
        case 0 => new Point3d( f( x ), y, z )
        case 1 => new Point3d( x, f( y ), z )
        case 2 => new Point3d( x, y, f( z ) )
    }

    def +( other: Tuple3d ) = new Point3d( x + other.x, y + other.y, z + other.z )

    def -( other: Tuple3d ) = new Point3d( x - other.x, y - other.y, z - other.z )

    def +( d: Double ) = new Point3d( x + d, y + d, z + d )

    def -( d: Double ) = new Point3d( x - d, y - d, z - d )

    /**
     * Multiplication by a constant
     */
    def *( s: Double ) = scale( s ) // TODO: reverse

    /**
     * Component-wise multiplication
     */
    def multiplyComponents( t: Tuple3d ) = new Point3d( x * t.x, y * t.y, z * t.z )

    /**
     * Division by a constant
     */
    def /( s: Double ) = {
        require( s != 0 )
        new Point3d( x / s, y / s, z / s )
    }

    /**
     * Negation
     */
    def unary_-() = new Point3d( -x, -y, -z )

    /**
     * Scaling
     */
    def scale( s: Double ) = new Point3d( x * s, y * s, z * s )

    /**
     * Manhattan distance
     */
    def distanceL1( other: Point3d ) = ( x - other.x ).abs + ( y - other.y ).abs + ( z - other.z ).abs

    def clampMin( d: Double ) = map( _ max d )

    def clampMax( d: Double ) = map( _ min d )

    def clamp( dMin: Double, dMax: Double ) = clampMin( dMin ).clampMax( dMax )

    def clampComponent( dMin: Double, dMax: Double, component: Int ) = mapComponent( _ max dMin, component ).mapComponent( _ min dMax, component )

    def fit( inLow: Double, inHigh: Double, outLow: Double, outHigh: Double ) = map( (d: Double) => ( ( d - inLow ) / ( inHigh - inLow ) ) * ( outHigh - outLow ) + outLow )

}

object Point3d {
    val ZERO = new Point3d( 0, 0, 0 )
}

class Vector3d(
        val x: Double,
        val y: Double,
        val z: Double
        ) extends Tuple3d {
    def this() = this ( 0, 0, 0 )

    def map( f: ( Double ) => Double ) = new Vector3d( f( x ), f( y ), f( z ) )

    def +( other: Vector3d ) = new Vector3d( x + other.x, y + other.y, z + other.z )

    def +( point: Point3d ) = new Point3d( x + point.x, y + point.y, z + point.z )

    def -( other: Vector3d ) = new Vector3d( x - other.x, y - other.y, z - other.z )

    def -( point: Point3d ) = new Point3d( x - point.x, y - point.y, z - point.z )

    def +( d: Double ) = new Vector3d( x + d, y + d, z + d )

    def -( d: Double ) = new Vector3d( x - d, y - d, z - d )

    def *( s: Double ) = scale( s ) // TODO: reverse

    def /( s: Double ) = {
        require( s != 0 )
        new Vector3d( x / s, y / s, z / s )
    }

    def unary_-() = new Vector3d( -x, -y, -z )

    def scale( s: Double ) = new Vector3d( x * s, y * s, z * s )

    def cross( other: Vector3d ) = new Vector3d(
        -z * other.y + y * other.z,
        z * other.x - x * other.z,
        -y * other.x + x * other.y
        )

    def normalize = this / magnitude

    /**
     * Returns the angle in radians between this vector and the vector
     * parameter; the return value is constrained to the range [0,PI].
     *
     * @param v1 the other vector
     * @return the angle in radians in the range [0,PI]
     */
    final def angle( other: Vector3d ): Double = {
        var vDot: Double = ( this dot other ) / ( this.magnitude * other.magnitude )
        if ( vDot < -1.0 ) {vDot = -1.0}
        if ( vDot > 1.0 ) {vDot = 1.0}
        return ( ( scala.math.acos( vDot ) ).asInstanceOf[ Double ] )
    }

}

object Vector3d {
    val ZERO = new Vector3d( 0, 0, 0 )

    //implicit def numToVec( d : Double ) = new Vector3d( d, d, d )

    /**
     * Returns a randomly sampled unit vector
     *
     * @return Random unit vector
     */
    def randomUnit: Vector3d = {
        // TODO: test
        var u: Double = 2 * scala.math.random - 1
        val v: Double = 2 * scala.math.Pi * scala.math.random
        val z: Double = u
        u = scala.math.sqrt( 1 - u * u )
        return new Vector3d( u * scala.math.cos( v ), u * scala.math.sin( v ), z )
    }

    /**
     * Returns a randomly sampled unit vector in the hemisphere centered in the direction of the normal vector
     *
     * @param normal The normal to the plane of the hemisphere
     * @return Unit vector
     */
    def randomHemisphere( normal: Vector3d ): Vector3d = {
        var ret: Vector3d = randomUnit
        if ( ret.dot( normal ) < 0 ) -ret else ret
    }

    /**
     * Cone in the z+ direction, with the specified minDot
     *
     * @param minDot Minimum dot-product with (0, 0, 1)
     * @return Random unit vector
     */
    def randomCone( minDot: Double ): Vector3d = {
        // TODO: test
        var u: Double = 2 * ( scala.math.random * ( 1 - minDot ) + minDot ) - 1
        val v: Double = 2 * scala.math.Pi * scala.math.random
        val z: Double = u
        u = scala.math.sqrt( 1 - u * u )
        return new Vector3d( u * scala.math.cos( v ), u * scala.math.sin( v ), z )
    }

    def randomProjectedHemisphere( normal: Vector3d ): Vector3d = {
        // TODO: test, possible rewrite?
        require( Numbers.equalsEpsilon( normal.magnitudeSquared, 1 ) )
        var ret: Vector3d = Vector3d.ZERO
        val preu: Double = scala.math.random
        val u: Double = scala.math.sqrt( preu )
        val v: Double = 2 * scala.math.Pi * scala.math.random
        val xf: Double = u * scala.math.cos( v )
        val yf: Double = u * scala.math.sin( v )
        val zf: Double = scala.math.sqrt( 1 - preu )
        assert( Numbers.equalsEpsilon( xf * xf + yf * yf + zf * zf, 1 ) )
        var perp: Vector3d = perpUnitOfUnit( normal )
        ret = ret + normal * zf + perp * xf
        perp = perp cross normal
        ret = ret + perp * yf
        assert( ret.dot( normal ) > Numbers.EPSILON )
        assert( Numbers.equalsEpsilon( ret.magnitudeSquared, 1 ) )
        ret
    }


    def perpUnitOfUnit( unit: Vector3d ): Vector3d = {
        // TODO: test, possible rewrite?
        var ax: Double = scala.math.abs( unit.x )
        var ay: Double = scala.math.abs( unit.y )
        var tmp: Vector3d = null
        if ( ax + ay > 0.1 ) {
            if ( ax > ay ) {
                tmp = new Vector3d( 0, 1, 0 )
            }
            else {
                tmp = new Vector3d( 1, 0, 0 )
            }
        }
        else {
            tmp = new Vector3d( 1, 0, 0 )
        }
        tmp = ( unit cross tmp ).normalize
        assert( scala.math.abs( tmp.dot( unit ) ) < Numbers.EPSILON )
        assert( Numbers.equalsEpsilon( tmp.magnitudeSquared, 1 ) )
        tmp
    }
}

class Point3i(
        val x: Int,
        val y: Int,
        val z: Int
        ) extends Tuple3i {
    def this() = this ( 0, 0, 0 )
}

object Point3i {
    implicit def toArr( p: Point3i ): Array[ Int ] = Array( p.x, p.y, p.z )
}

class Matrix3d(
        val m00: Double,
        val m01: Double,
        val m02: Double,
        val m10: Double,
        val m11: Double,
        val m12: Double,
        val m20: Double,
        val m21: Double,
        val m22: Double
        ) extends Matrix3[ Double ] {
    def this() = this ( 0, 0, 0, 0, 0, 0, 0, 0, 0 )

    // TODO: implement equals / hashcode
    // TODO: implement epsilon equals

    private val EPS: Double = 1.110223024E-16
    private val ERR_EPS: Double = 1.0E-8

    def map( f: ( Double ) => Double ) = new Matrix3d(
        f( m00 ), f( m01 ), f( m02 ),
        f( m10 ), f( m11 ), f( m12 ),
        f( m20 ), f( m21 ), f( m22 )
        )

    def map2( other: Matrix3d, f: (Double, Double) => Double ) = new Matrix3d(
        f( this.m00, other.m00 ), f( this.m01, other.m01 ), f( this.m02, other.m02 ),
        f( this.m10, other.m10 ), f( this.m11, other.m11 ), f( this.m12, other.m12 ),
        f( this.m20, other.m20 ), f( this.m21, other.m21 ), f( this.m22, other.m22 )
        )

    def +( other: Matrix3d ) = map2( other, (a: Double, b: Double) => a + b )

    def +( v: Double ) = map( (d: Double) => d + v )

    def -( other: Matrix3d ) = map2( other, (a: Double, b: Double) => a - b )

    def -( v: Double ) = map( (d: Double) => d - v )

    def unary_-() = map( (d: Double) => -d )

    def *( vec: Point3d ): Point3d = new Point3d(
        m00 * vec.x + m01 * vec.y + m02 * vec.z,
        m10 * vec.x + m11 * vec.y + m12 * vec.z,
        m20 * vec.x + m21 * vec.y + m22 * vec.z
        )

    def *( vec: Vector3d ): Vector3d = new Vector3d(
        m00 * vec.x + m01 * vec.y + m02 * vec.z,
        m10 * vec.x + m11 * vec.y + m12 * vec.z,
        m20 * vec.x + m21 * vec.y + m22 * vec.z
        )

    def *( other: Matrix3d ) = new Matrix3d(
        this.m00 * other.m00 + this.m01 * other.m10 + this.m02 * other.m20,
        this.m00 * other.m01 + this.m01 * other.m11 + this.m02 * other.m21,
        this.m00 * other.m02 + this.m01 * other.m12 + this.m02 * other.m22,

        this.m10 * other.m00 + this.m11 * other.m10 + this.m12 * other.m20,
        this.m10 * other.m01 + this.m11 * other.m11 + this.m12 * other.m21,
        this.m10 * other.m02 + this.m11 * other.m12 + this.m12 * other.m22,

        this.m20 * other.m00 + this.m21 * other.m10 + this.m22 * other.m20,
        this.m20 * other.m01 + this.m21 * other.m11 + this.m22 * other.m21,
        this.m20 * other.m02 + this.m21 * other.m12 + this.m22 * other.m22
        )

    def *( v: Double ) = map( (d: Double) => d * v )

    def transpose = new Matrix3d(
        m00, m10, m20,
        m01, m11, m21,
        m02, m12, m22
        )

    def inverse: Matrix3d = { //invertGeneral
        val mat = new VMatrix3d( m00, m01, m02, m10, m11, m12, m20, m21, m22 )
        mat.invert()
        return new Matrix3d( mat.m00, mat.m01, mat.m02, mat.m10, mat.m11, mat.m12, mat.m20, mat.m21, mat.m22 )
    }

    def determinant: Double = {
        this.m00 * ( this.m11 * this.m22 - this.m12 * this.m21 ) + this.m01 * ( this.m12 * this.m20 - this.m10 * this.m22 ) + this.m02 * ( this.m10 * this.m21 - this.m11 * this.m20 )
    }

    /**
     * Sets the value of this matrix to a counter clockwise rotation
     * about the x axis.
     *
     * @param angle the angle to rotate about the X axis in radians
     */
    final def rotX( angle: Double ): Matrix3d = {
        val sinAngle: Double = scala.math.sin( angle )
        val cosAngle: Double = scala.math.cos( angle )
        new Matrix3d( 1, 0, 0, 0, cosAngle, -sinAngle, 0, sinAngle, cosAngle )
    }


    /**
     * Sets the value of this matrix to a counter clockwise rotation
     * about the y axis.
     *
     * @param angle the angle to rotate about the Y axis in radians
     */
    final def rotY( angle: Double ): Matrix3d = {
        val sinAngle: Double = scala.math.sin( angle )
        val cosAngle: Double = scala.math.cos( angle )
        new Matrix3d( cosAngle, 0, sinAngle, 0, 1, 0, -sinAngle, 0, cosAngle )
    }


    /**
     * Sets the value of this matrix to a counter clockwise rotation
     * about the z axis.
     *
     * @param angle the angle to rotate about the Z axis in radians
     */
    final def rotZ( angle: Double ): Matrix3d = {
        val sinAngle: Double = scala.math.sin( angle )
        val cosAngle: Double = scala.math.cos( angle )
        new Matrix3d( cosAngle, -sinAngle, 0, sinAngle, cosAngle, 0, 0, 0, 1 )
    }

    override def toString = "{{" + m00 + ", " + m01 + ", " + m02 + "},{" + m10 + ", " + m11 + ", " + m12 + "},{" + m20 + ", " + m21 + ", " + m22 + "}}"

    private def invertGeneral: Matrix3d = {
        var result = new Array[ Double ]( 9 )
        var row_perm = new Array[ Int ]( 3 )
        var i: Int = 0
        var r: Int = 0
        var c: Int = 0
        var tmp: Array[ Double ] = new Array[ Double ]( 9 )
        tmp( 0 ) = m00
        tmp( 1 ) = m01
        tmp( 2 ) = m02
        tmp( 3 ) = m10
        tmp( 4 ) = m11
        tmp( 5 ) = m12
        tmp( 6 ) = m20
        tmp( 7 ) = m21
        tmp( 8 ) = m22
        if ( !Matrix3d.luDecomposition( tmp, row_perm ) ) {
            throw new RuntimeException( "Singular matrix" )
        }

        {
            i = 0
            while ( i < 9 ) {
                {
                    result( i ) = 0.0
                }
                ( {i += 1; i} )
            }
        }
        result( 0 ) = 1.0
        result( 4 ) = 1.0
        result( 8 ) = 1.0
        Matrix3d.luBacksubstitution( tmp, row_perm, result )
        new Matrix3d(
            result( 0 ), result( 1 ), result( 2 ),
            result( 3 ), result( 4 ), result( 5 ),
            result( 6 ), result( 7 ), result( 8 )
            )
    }

}

object Matrix3d {
    val Identity = new Matrix3d( 1, 0, 0, 0, 1, 0, 0, 0, 1 )

    // TODO: add general matrix trait with general matrix function implementations

    //implicit def numToMatrix( d : Double ) = new Matrix3d( d, d, d, d, d, d, d, d, d )

    /**
     * Given a 3x3 array "matrix0", this function replaces it with the
     * LU decomposition of a row-wise permutation of itself.  The input
     * parameters are "matrix0" and "dimen".  The array "matrix0" is also
     * an output parameter.  The vector "row_perm[3]" is an output
     * parameter that contains the row permutations resulting from partial
     * pivoting.  The output parameter "even_row_xchg" is 1 when the
     * number of row exchanges is even, or -1 otherwise.  Assumes data
     * type is always double.
     * <p/>
     * This function is similar to luDecomposition, except that it
     * is tuned specifically for 3x3 matrices.
     *
     * @return true if the matrix is nonsingular, or false otherwise.
     */
    def luDecomposition( matrix0: Array[ Double ], row_perm: Array[ Int ] ): Boolean = {
        var row_scale = new Array[ Double ]( 3 )

        var i: Int = 0
        var j: Int = 0
        var ptr: Int = 0
        var rs: Int = 0
        var big: Double = .0
        var temp: Double = .0
        ptr = 0
        rs = 0
        i = 3
        while ( ( {i -= 1; i} ) != 0 ) {
            big = 0.0
            j = 3
            while ( ( {j -= 1; j} ) != 0 ) {
                temp = matrix0( ( {ptr += 1; ptr} ) )
                temp = scala.math.abs( temp )
                if ( temp > big ) {
                    big = temp
                }
            }
            if ( big == 0.0 ) {
                return false
            }
            row_scale( rs ) = 1.0 / big
            rs += 1
        }


        j = 0;
        var mtx: Int = 0
        mtx = 0

        j = 0
        while ( j < 3 ) {
            {
                var i: Int = 0
                var imax: Int = 0
                var k: Int = 0
                var target: Int = 0
                var p1: Int = 0
                var p2: Int = 0
                var sum: Double = .0
                var big: Double = .0
                var temp: Double = .0

                {
                    i = 0
                    while ( i < j ) {
                        {
                            target = mtx + ( 3 * i ) + j
                            sum = matrix0( target )
                            k = i
                            p1 = mtx + ( 3 * i )
                            p2 = mtx + j
                            while ( ( {k -= 1; k} ) != 0 ) {
                                sum -= matrix0( p1 ) * matrix0( p2 )
                                ( {p1 += 1; p1} )
                                p2 += 3
                            }
                            matrix0( target ) = sum
                        }
                        ( {i += 1; i} )
                    }
                }
                big = 0.0
                imax = -1

                {
                    i = j
                    while ( i < 3 ) {
                        {
                            target = mtx + ( 3 * i ) + j
                            sum = matrix0( target )
                            k = j
                            p1 = mtx + ( 3 * i )
                            p2 = mtx + j
                            while ( ( {k -= 1; k} ) != 0 ) {
                                sum -= matrix0( p1 ) * matrix0( p2 )
                                ( {p1 += 1; p1} )
                                p2 += 3
                            }
                            matrix0( target ) = sum
                            if ( ( ( {temp = row_scale( i ) * scala.math.abs( sum ); temp} ) ) >= big ) {
                                big = temp
                                imax = i
                            }
                        }
                        ( {i += 1; i} )
                    }
                }
                if ( imax < 0 ) {
                    throw new RuntimeException( "Singular matrix" )
                }
                if ( j != imax ) {
                    k = 3
                    p1 = mtx + ( 3 * imax )
                    p2 = mtx + ( 3 * j )
                    while ( ( {k -= 1; k} ) != 0 ) {
                        temp = matrix0( p1 )
                        matrix0( ( {p1 += 1; p1} ) ) = matrix0( p2 )
                        matrix0( ( {p2 += 1; p2} ) ) = temp
                    }
                    row_scale( imax ) = row_scale( j )
                }
                row_perm( j ) = imax
                if ( matrix0( ( mtx + ( 3 * j ) + j ) ) == 0.0 ) {
                    return false
                }
                if ( j != ( 3 - 1 ) ) {
                    temp = 1.0 / ( matrix0( ( mtx + ( 3 * j ) + j ) ) )
                    target = mtx + ( 3 * ( j + 1 ) ) + j
                    i = 2 - j
                    while ( ( {i -= 1; i} ) != 0 ) {
                        matrix0( target ) *= temp
                        target += 3
                    }
                }
            }
            ( {j += 1; j} )
        }


        return true
    }


    /**
     * Solves a set of linear equations.  The input parameters "matrix1",
     * and "row_perm" come from luDecompostionD3x3 and do not change
     * here.  The parameter "matrix2" is a set of column vectors assembled
     * into a 3x3 matrix of floating-point values.  The procedure takes each
     * column of "matrix2" in turn and treats it as the right-hand side of the
     * matrix equation Ax = LUx = b.  The solution vector replaces the
     * original column of the matrix.
     * <p/>
     * If "matrix2" is the identity matrix, the procedure replaces its contents
     * with the inverse of the matrix from which "matrix1" was originally
     * derived.
     */
    def luBacksubstitution( matrix1: Array[ Double ], row_perm: Array[ Int ], matrix2: Array[ Double ] ): Unit = {
        var i: Int = 0
        var ii: Int = 0
        var ip: Int = 0
        var j: Int = 0
        var k: Int = 0
        var rp: Int = 0
        var cv: Int = 0
        var rv: Int = 0
        rp = 0


        k = 0
        while ( k < 3 ) {
            {
                cv = k
                ii = -1

                {
                    i = 0
                    while ( i < 3 ) {
                        {
                            var sum: Double = .0
                            ip = row_perm( rp + i )
                            sum = matrix2( cv + 3 * ip )
                            matrix2( cv + 3 * ip ) = matrix2( cv + 3 * i )
                            if ( ii >= 0 ) {
                                rv = i * 3

                                {
                                    j = ii
                                    while ( j <= i - 1 ) {
                                        {
                                            sum -= matrix1( rv + j ) * matrix2( cv + 3 * j )
                                        }
                                        ( {j += 1; j} )
                                    }
                                }
                            }
                            else if ( sum != 0.0 ) {
                                ii = i
                            }
                            matrix2( cv + 3 * i ) = sum
                        }
                        ( {i += 1; i} )
                    }
                }
                rv = 2 * 3
                matrix2( cv + 3 * 2 ) /= matrix1( rv + 2 )
                rv -= 3
                matrix2( cv + 3 * 1 ) = ( matrix2( cv + 3 * 1 ) - matrix1( rv + 2 ) * matrix2( cv + 3 * 2 ) ) / matrix1( rv + 1 )
                rv -= 3
                matrix2( cv + 4 * 0 ) = ( matrix2( cv + 3 * 0 ) - matrix1( rv + 1 ) * matrix2( cv + 3 * 1 ) - matrix1( rv + 2 ) * matrix2( cv + 3 * 2 ) ) / matrix1( rv + 0 )
            }
            ( {k += 1; k} )
        }

    }
}
