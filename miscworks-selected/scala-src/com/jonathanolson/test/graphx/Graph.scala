package com.jonathanolson.test.graphx

import collection.mutable.{Set => MSet, Map => MMap}

trait Graph[V <: Vertex, E <: Edge] {
    def vertices: Iterable[V]

    def edges: Iterable[E]

    def verticesOf( e: E ): Iterable[V]

    def edgesOf( v: V ): Iterable[E]
}

trait Vertex

trait VertexLike[This <: Vertex with VertexLike[This]] extends Vertex {
    self: This =>
    def edges[E <: Edge, G <: Graph[This, E]]( implicit g: G ): Iterable[E] = g.edgesOf( this )
}

trait Edge

trait EdgeLike[This <: Edge with EdgeLike[This]] extends Edge {
    self: This =>
    def vertices[V <: Vertex, G <: Graph[V, This]]( implicit g: G ): Iterable[V] = g.verticesOf( this )

    def toString[V <: Vertex, G <: Graph[V, This]]( implicit g: G ): String = g.verticesOf( this ).mkString( " => " )
}

trait MutableDirectedMapConnections[V, E] {
    // uses hashmap. not great performance
    private[this] val vertexData: MMap[V, MMap[V, E]] = MMap.empty
    private[this] val reversedVertexData: MMap[V, MMap[V, E]] = MMap.empty
    private[this] val edgeData: MMap[E, (V, V)] = MMap.empty

    val vertices: MSet[V] = MSet.empty
    val edges: MSet[E] = MSet.empty

    def addVertex( v: V ): Unit = {
        require( !vertices.contains( v ) )

        vertices += v
    }

    def addEdge( a: V, b: V, e: E ): Unit = {
        require( !edges.contains( e ) )
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )

        edges += e
        edgeData += (e -> (a, b))

        addToVertexMap( vertexData, a, b, e )
        addToVertexMap( reversedVertexData, b, a, e )

    }

    def verticesOf( e: E ) = {
        require( edges.contains( e ) )

        val (a, b) = edgeData( e )
        Set( a, b )
    }

    def edgesOf( v: V ) = {
        require( vertices.contains( v ) )

        // from and to
        vertexData( v ).map[E, Iterable[E]]( t => t._2 ) ++ reversedVertexData( v ).map[E, Iterable[E]]( t => t._2 )
    }

    private[this] def addToVertexMap( map: MMap[V, MMap[V, E]], a: V, b: V, e: E ): Unit = {
        if ( map.contains( a ) ) {
            map( a ) += (b -> e)
        } else {
            map += (a -> MMap( b -> e ))
        }
    }
}

class TestEdge extends Edge with EdgeLike[TestEdge] {
}

object TestEdge {
    implicit def eToString[V <: Vertex, G <: Graph[V, TestEdge]]( e: TestEdge )( implicit g: G ): String = e.vertices[V, G]( g ).mkString( " => " )
}

class TestVertex( val label: String ) extends Vertex with VertexLike[TestVertex] {
    override def toString = label

    override def equals( p1: Any ) = p1 match {
        case v: TestVertex => this.label.equals( v.label )
        case _ => false
    }

    override def hashCode = label.hashCode
}

object TestVertex {
}

class TestGraph[V <: Vertex, E <: Edge] extends Graph[V, E] with MutableDirectedMapConnections[V, E]

object NewGraphTester {
    def main( args: Array[String] ) {
        implicit val g: TestGraph[TestVertex, TestEdge] = new TestGraph
        val a = new TestVertex( "a" )
        val b = new TestVertex( "b" )
        val c = new TestVertex( "c" )
        val d = new TestVertex( "d" )
        g.addVertex( a )
        g.addVertex( b )
        g.addVertex( c )
        g.addVertex( d )
        g.vertices.foreach( println _ )
        val edge1: TestEdge = new TestEdge
        val edge2: TestEdge = new TestEdge
        val edge3: TestEdge = new TestEdge
        val edge4: TestEdge = new TestEdge
        g.addEdge( a, b, edge1 )
        g.addEdge( b, c, edge2 )
        g.addEdge( c, a, edge3 )
        g.addEdge( c, d, edge4 )
        a.edges[TestEdge, TestGraph[TestVertex, TestEdge]]( g ).foreach( (e: TestEdge) => println( e.toString[TestVertex, TestGraph[TestVertex, TestEdge]]( g ) ) )
        println()
        a.edges[TestEdge, TestGraph[TestVertex, TestEdge]].foreach( (e: TestEdge) => println( e.toString[TestVertex, TestGraph[TestVertex, TestEdge]] ) )
        println()
        a.edges[TestEdge, TestGraph[TestVertex, TestEdge]]( g ).foreach( (e: TestEdge) => println( e.toString[TestVertex, TestGraph[TestVertex, TestEdge]]( g ) ) )
        println()
        edge1.vertices[TestVertex, TestGraph[TestVertex, TestEdge]]( g )
        //edge1.vertices( g )
    }
}