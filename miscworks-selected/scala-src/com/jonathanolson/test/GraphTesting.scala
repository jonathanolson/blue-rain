package test

trait Vertex[V <: Vertex[V]] {
    this: V =>
    def degree( implicit g: Graph[V, X] forSome {type X <: Edge[X]} ) = g.degreeOf( this )

    def edges[E <: Edge[E]]( implicit g: Graph[V, E] ): Iterable[E] = g.edgesOf( this )
}

trait Edge[E <: Edge[E]]

trait Graph[V <: Vertex[V], E <: Edge[E]] {
    def degreeOf( v: V ): Int

    def edgesOf( v: V ): Iterable[E]
}

class TestVertex extends Vertex[TestVertex]

class TestEdge extends Edge[TestEdge]

class TestGraph extends Graph[TestVertex, TestEdge] {
    def degreeOf( v: TestVertex ) = 0

    def edgesOf( v: TestVertex ) = Set( new TestEdge, new TestEdge )
}

trait A extends ALike[A] {
    println( "init A" )

    def totallyAbstract(): Unit

    def aa(): Unit

    def ap(): Unit = println( "ap:A" )
}

trait ALike[+This <: A] {
    this: This =>
    println( "init ALike" )

    def aa(): Unit = println( "aa:ALike" )
}

trait B extends A with BLike[B] {
    println( "init B" )

    def bb(): Unit = println( "bb:B" )
}

trait BLike[+This <: B] extends ALike[This] {
    this: This =>
    println( "init BLike" )
}


class BConcrete extends B with BLike[BConcrete] {
    println( "init BConcrete" )

    def totallyAbstract() = println( "BConcrete" )

}

object GraphTesting {
    def main( args: Array[String] ) {
        //        implicit val g = new TestGraph
        //        val v = new TestVertex
        //        println( v.degree )
        //        println( v.edges )
        //        println( getDegree2( v ) )
        val a = new BConcrete
        println()
        a.aa()
        println()
        a.ap()
        println()
    }

    def getDegree2[V <: Vertex[V]]( v: V )( implicit g: Graph[V, E] forSome {type E <: Edge[E]} ) = v.degree( g )

}