package com.jonathanolson.test

import java.io.File
import com.jonathanolson.graphics.raster.{Raster, ImageRaster2, ImageRaster}
import com.jonathanolson.util.FileUtils
import collection.mutable.ListBuffer
import com.jonathanolson.color.{Color, RGBColorspace}
import util.Random

object ImageTest {
    private[ this ] val random = new Random

    def main( args: Array[ String ] ) {
        //        val original = ImageRaster.fromFile( new File( "/home/jon/data/captures/finished/photos1.png" ) )
        //        val edges = original.sobel
        //        edges.fit( 0, 1 ).writeToFile( new File( "/home/jon/tmp/sobel-test-1.png" ) )


        //        val files = new File( "/home/jon/data/captures/finished-bing" ).listFiles
        //
        //        var shuffledFiles: ListBuffer[ File ] = new ListBuffer
        //        for ( file <- files ) {
        //            shuffledFiles.insert( random.nextInt( shuffledFiles.length + 1 ), file )
        //        }


        //        var edges = new ImageRaster2( 1024, 1024, RGBColorspace.sRGB )
        //
        //        for ( i <- 0 to 10 ) {
        //            val file: File = files( i )
        //            println( "processing " + file )
        //            val original = ImageRaster.fromFile( file )
        //            edges = edges + original.sobel
        //        }
        //
        //        edges.fit( 0, 1 ).writeToFile( new File( "/home/jon/tmp/sobel-test-2.png" ) )



        //        val sob: ImageRaster2 = ImageRaster.fromFile( files( 0 ) ).sobel
        //        println( sob.minValue )
        //        println( sob.maxValue )

        val dir: File = new File( "/home/jon/data/captures/finished" )
        while ( !dir.listFiles.isEmpty ) {
            val files = dir.listFiles

            var shuffledFiles: ListBuffer[ File ] = new ListBuffer
            for ( file <- files ) {
                shuffledFiles.insert( random.nextInt( shuffledFiles.length + 1 ), file )
            }
            for ( file: File <- shuffledFiles.toList ) {
                println( "processing " + file.getName() )
                val original = ImageRaster.fromFile( file )
                ( ( original.sobel ) / 12.0 ).writeToFile( new File( "/home/jon/data/captures/edges", file.getName ) )
                FileUtils.copyTo( file, new File( "/home/jon/data/captures/1024top1000", file.getName ) )
                file.delete()
            }
        }

        //println( ( -3.5019730171281793E-19 ).toInt )


    }
}

object SumTest {
    def main( args: Array[ String ] ) {
        val files = new File( "/home/jon/data/captures/top-1000-sat" ).listFiles
        var edges = new ImageRaster2( 1024, 1024, RGBColorspace.Default )

        for ( file <- files ) {
            println( "processing " + file )
            edges = edges + ImageRaster.fromFile( file )
        }

        edges.fit( 0, 1 ).writeToFile( new File( "/home/jon/tmp/variation-sat.png" ) )
    }
}

object WeightedSumTest {
    def main( args: Array[ String ] ) {
        val files = new File( "/home/jon/data/captures/old-bing" ).listFiles
        var edges = new ImageRaster2( 1024, 1024, RGBColorspace.Default )

        for ( file <- files ) {
            println( "processing " + file )
            val sob: ImageRaster2 = ImageRaster.fromFile( file )
            val avg: Double = sob.foldLeft[ Double ]( 0 )( (total: Double, c: Color) => total + c.x + c.y + c.z )
            if ( avg > 1 ) {
                edges = edges + ( sob / avg )
            }
        }

        edges.fit( 0, 1 ).writeToFile( new File( "/home/jon/tmp/sobel-test-weighted-11.png" ) )
    }
}