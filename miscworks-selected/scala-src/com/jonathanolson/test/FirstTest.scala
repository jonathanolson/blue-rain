package com.jonathanolson.test

import java.io.File
import com.jonathanolson.jlink.JLink
import com.jonathanolson.math.{Matrix3d, Point3d}
import com.jonathanolson.color.{RGBColorspace, RGBColor, Color}
import com.jonathanolson.graphics.raster.SimpleRaster2

object FirstTest {
    def main( args: Array[ String ] ) {
        println( "Test" )
        for ( arg <- args.map( _.toUpperCase ) ) {println( arg )}

        val a = new Point3d( 0, 1, 2 )
        val mat = Matrix3d.Identity
        println( mat )
        println( a )
        println( mat + 4 )
        println( mat * 2 )
        println( mat * a )
        println( ( mat * 2 ) * a )
        println( "color tests: " )
        val color = new Color( 0.1, 0.2, 0.3 )
        println( color )
        println( color.toXYZ )
        //        val c : RGBColor = color.toColorspace(RGBColorspace.sRGB)
        //        println( c.getClass )

        val c: Color = RGBColorspace.sRGB.convert( color )
        println( c )

        println( "Quantization" )
        println( new Color( 0, 0, 0 ).quantized )
        println( new Color( 1, 1, 1 ).quantized )
        println( new Color( 0.99999999, 0.99999999, 0.99999999 ).quantized )

        val raster = new SimpleRaster2[ Double ]( 8, 8 )
        raster( 0, 0 ) = 5
        println( raster( 0, 0 ) )

        val raster2 = new SimpleRaster2[ Int ]( 8, 8, (a, b) => a + b )

        for ( index <- raster2.indices ) {
            println( index + " is " + raster2( index ) )
        }

        println( raster2.forall( _ > 0 ) )
        println( raster2.forall( _ >= 0 ) )

        println( raster2.dumpString )

        //        println( System.currentTimeMillis )
        //
        //        val image = new SimpleRaster2[ Color ]( 800, 600, (x: Int, y: Int) => new Color( x / 799.0, y / 599.0, 0 ) ) with ImageRaster
        //
        //        println( System.currentTimeMillis )
        //
        //        image.writeToFile( new File( "/home/jon/tmp/out-scala.png" ) )
        //
        //        println( System.currentTimeMillis )
        //
        //        val readImage = ImageRaster.fromFile( new File( "/home/jon/tmp/out.png" ) )
        //
        //        readImage.writeToFile( new File( "/home/jon/tmp/out-cpy.png" ) )

        for ( xval <- 0 to 50 ) {
            println( JLink.eval( "D[x^3,x] /. {x -> " + xval + "}" ) )
        }

    }
}



