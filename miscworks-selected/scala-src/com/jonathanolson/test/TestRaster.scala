package com.jonathanolson.test

import com.jonathanolson.math.Point3d

object TestRaster {
    def main( args: Array[ String ] ) {
        val ra: TestRasterTrait[ Point3d ] = new TestRasterImp
        val rb: TestRasterTrait[ AnyRef ] = ra

        println( "success" )
    }
}

trait TestRasterTrait[ +T ] {
    def apply( x: Int, y: Int ): T
}

trait MutableTestRasterTrait[ T ] extends TestRasterTrait[ T ] {
    def update( x: Int, y: Int, e: T ): Unit
}

class TestRasterImp extends TestRasterTrait[ Point3d ] with MutableTestRasterTrait[ Point3d ] {
    def apply( x: Int, y: Int ) = Point3d.ZERO

    def update( x: Int, y: Int, e: Point3d ) = ()
}

