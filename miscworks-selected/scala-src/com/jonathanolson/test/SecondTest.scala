package com.jonathanolson.test

object SecondTest {
    def named(x: Int = 1, y: Int = 0) {
        println(x)
        println(y)
    }

    def main(args: Array[String]) {
        named()
        named(y = 5)
    }
}