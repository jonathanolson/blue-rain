package graphy

trait Vertex[This <: Vertex[This]] {
    self: This =>
    def edges[E <: Edge, G <: Graph[This, E]]( implicit g: G ): Iterable[E] = g.edgesOf( this )
}

trait Edge

trait Graph[V <: Vertex[V], E <: Edge] {
    def edgesOf( v: V ): Iterable[E] // all edges that touch v
}

class TestEdge( val label: String ) extends Edge {
    override def toString = label
}

class TestVertex( val label: String ) extends Vertex[TestVertex] {
    override def toString = label
}

class TestGraph extends Graph[TestVertex, TestEdge] {
    import TestGraph._
    def edgesOf( v: TestVertex ): Iterable[TestEdge] = {
        if ( v == a ) return Set( e1, e2 )
        if ( v == b ) return Set( e1, e3 )
        if ( v == c ) return Set( e2, e3 )
        return Set.empty
    }
}

object TestGraph {
    val a = new TestVertex( "a" )
    val b = new TestVertex( "b" )
    val c = new TestVertex( "c" )

    val e1 = new TestEdge( "a => b" )
    val e2 = new TestEdge( "c => a" )
    val e3 = new TestEdge( "b => c" )
}

object GraphTestY {
    def main( args: Array[String] ) {
        import TestGraph._
        implicit val g = new TestGraph
        //println( a.edges ) // fails with error: could not find implicit value for parameter g: com.jonathanolson.test.graphy.Graph[com.jonathanolson.test.graphy.TestVertex,Nothing]
        //println( a.edges( g ) ) // fails with error: error: inferred type arguments [Nothing,com.jonathanolson.test.graphy.TestGraph] do not conform to method edges's type parameter bounds [E <: com.jonathanolson.test.graphy.Edge,G <: com.jonathanolson.test.graphy.Graph[com.jonathanolson.test.graphy.TestVertex,E]]
        println( a.edges[TestEdge, TestGraph] ) // works. result: Set(a => b, c => a)
        println( a.edges[TestEdge, TestGraph]( g ) ) // works. result: Set(a => b, c => a)
    }
}