package graphw

trait Vertex

trait Edge

trait Graph[V <: Vertex with VertexLike[V], E <: Edge] {
    def edgesOf( v: V ): Iterable[E] // all edges that touch v
}

// vertex convenience function implementation
trait VertexLike[This <: VertexLike[This] with Vertex] {
    self: This =>
    def edges[E <: Edge, G <: Graph[This, E]]( implicit g: G ): Iterable[E] = g.edgesOf( this )
}

class TestEdge( val label: String ) extends Edge {
    override def toString = label
}

class TestVertex( val label: String ) extends Vertex with VertexLike[TestVertex] {
    override def toString = label
}

class TestGraph extends Graph[TestVertex, TestEdge] {
    import TestGraph._
    def edgesOf( v: TestVertex ): Iterable[TestEdge] = {
        if ( v == a ) return Set( e1, e2 )
        if ( v == b ) return Set( e1, e3 )
        if ( v == c ) return Set( e2, e3 )
        return Set.empty
    }
}

object TestGraph {
    val a = new TestVertex( "a" )
    val b = new TestVertex( "b" )
    val c = new TestVertex( "c" )

    val e1 = new TestEdge( "a => b" )
    val e2 = new TestEdge( "c => a" )
    val e3 = new TestEdge( "b => c" )
}

object GraphTestW {
    def main( args: Array[String] ) {
        import TestGraph._
        implicit val g = new TestGraph
        //println( a.edges ) // fails with error: could not find implicit value for parameter g: graphz.Graph[graphz.TestVertex,Nothing]
        //println( a.edges( g ) ) // fails with error: inferred type arguments [Nothing,graphz.TestGraph] do not conform to method edges's type parameter bounds [E <: graphz.Edge,G <: graphz.Graph[graphz.TestVertex,E]]
        println( a.edges[TestEdge, TestGraph] ) // works. result: Set(a => b, c => a)
        println( a.edges[TestEdge, Graph[TestVertex,TestEdge]]( g ) ) // works. result: Set(a => b, c => a)
    }
}