package com.jonathanolson.test

object SpecTest {
    def main( args: Array[ String ] ) {
        println( "SpecTest" )

        val molly: Cow = new Cow( "Molly" )
        println( molly )
        val nellie: Dog = new Dog( "Nellie" )
        println( nellie )

        val dogFood = new Dogfood

        val mutant: Dog = molly.eat[Dog]( dogFood )
        mutant.bark()
        println( mutant )

    }
}

class Animal(
        val name: String
        ) {

    def eat[U <: Animal]( food : Food[U]) : U = food.afterEat( this )

    override def toString = this.getClass.getSimpleName + " \"" + name + "\""
}

class Cow( name: String ) extends Animal( name ) {
    def moo() = println( name + " moos" )
}
class Cheese extends Food[Cow] {
    type afterAnimal = Cow

    def afterEat( animal: Animal ) = new Cow( animal.name )
}

class Dog( name: String ) extends Animal( name ) {
    def bark() = println( name + " barks" )
}
class Dogfood extends Food[Dog] {
    type afterAnimal = Dog

    def afterEat( animal: Animal ) = new Dog( animal.name )
}

abstract class Food[AfterAnimal] {
    def afterEat( animal: Animal ): AfterAnimal
}


