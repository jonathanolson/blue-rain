package com.jonathanolson.slither.tests

import org.scalatest.junit.AssertionsForJUnit
import scala.collection.mutable.ListBuffer
import org.junit.Assert._
import org.junit.Test
import org.junit.Before
import java.lang.String
import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.Spot
import com.jonathanolson.slither.model.{ColoredGrid4, VertexStateGrid4, HorizontalEdge4, Grid4}
import com.jonathanolson.slither.patterns.{FacePattern, BasicRule}

class SlitherTests extends AssertionsForJUnit {
    //    var sb: StringBuilder = _
    //    var lb: ListBuffer[String] = _
    //
    //    @Before def initialize() {
    //        sb = new StringBuilder( "ScalaTest is " )
    //        lb = new ListBuffer[String]
    //    }
    //
    //    @Test def verifyEasy() { // Uses JUnit-style assertions
    //        sb.append( "easy!" )
    //        assertEquals( "ScalaTest is easy!", sb.toString )
    //        assertTrue( lb.isEmpty )
    //        lb += "sweet"
    //        try {
    //            "verbose".charAt( -1 )
    //            fail()
    //        }
    //        catch {
    //            case e: StringIndexOutOfBoundsException => // Expected
    //        }
    //    }
    //
    //    @Test def verifyFun() { // Uses ScalaTest assertions
    //        sb.append( "fun!" )
    //        assert( sb.toString === "ScalaTest is fun!" )
    //        assert( lb.isEmpty )
    //        lb += "sweeter"
    //        intercept[StringIndexOutOfBoundsException] {
    //            "concise".charAt( -1 )
    //        }
    //    }

    val grid20x20A: String = "20x20 .3.13.1.2333.1..31002..322..2..0.1.23.1133121..31....22...23..2......121.21.0.1.1.22..2..1.1..1.1..322.1.1111212.1...213.....2.2.31.1113231.1.21.312..01...2..02.01.2.22.....111.3.3.10.23.1.31..1....0....3..1.1..1..1.1.1.10...31.2311112312121.3.....3..1...1......3.2....112..211..11....3.22..3.1...1322022.2111...2.1.12..22..2.2.1.2221.1223.12..22.102.3.11.1.22...13.31..2.132.1.3..2.12221....22...110"

    @Test def constructEmptyGrid() {
        val grid = Grid4.empty( 4, 7 )
    }

    @Test def readSimpleGrid() {
        val gridString: String = "10x10 .1.3..0.3.3..3..3..33........3..0.33.2..0.2....2.22.0....3.2..2.03.3..3........23..0..3..3.3.1..3.2."
        val grid = Grid4.fromString( gridString )
        assert( grid.value( grid.faces( 0 )( 0 ) ) == None )
        assert( grid.value( grid.faces( 0 )( 1 ) ) == Some( 1 ) )
        assert( grid.state( grid.hedges( 1 )( 0 ) ) == White )
        assert( gridString == grid.simpleString )
    }

    @Test def readComplexGrid() {
        val gridString: String = "36x20 !187.87.87.1e269.2d.a52a5.b4.4b.963782c3.0f00f296.692a512d.3c2c311e3783d2378.5a26923c.c3187296.69.2d2a5.a5.3c.6912d3b4.5a2c3.1e.69.a5.2d.1e3e1.0f.b414b.1e.e1296.78.c3187.3c.5a.4b.a5.9625a.c3.872a5.2d.a5187.96269.b4.5a2c3.1e.e1187.b425a3d237814b.873b4.5a.e1187.0f.a52a5.87.a523c.5a3d2.5a.69.a500f.a5.2d3b4.5a.6912d.2d.1e26911e3b4.4b.2d12d.2d.87.a5.96.5a269.2d18712d.3c.5a.e112d.a5.3c.d214b.a511e3d2.783d2.78.d2269.1e.c300f.87.1e.4b.96269.0f.8700f.96.692a5.2d.1e14b.96.78.c311e14b3b4.5a37814b2a5.1e.78.c3.a5187.a518712d.8700f3b4.5a.783d2.5a3e1.c3.1e.7825a.e1.96378.4b.9625a.e1.0f3b4.5a.5a.69296.5a.d2.783d214b.a5.2d12d.a5.3c.5a378.d2.69.1e2c3.2d.87.3c3b425a.5a14b.a5.2d.1e.d2.6900f.b4.5a3e1.1e25a.5a3e1.0f.a5187.a5.96378.c3296.783d2.5a2c3.2d.0f00f.3c.5a3783d2.69.9625a.d23782c318712d.0f11e.e1.0f23c.5a.d2.4b23c.5a.e112d.2d.a5187.2d12d.87.a5.873b4.4b187.87.1e3d2.4b.a5.5a.e100f.2d.87.a5.b4.4b00f.873b414b.9614b2a500f.9614b3b4.4b11e269.3c2c3.1e2692a5.a52a5.1e.e1.3c.4b2a5.96.78.4b.b414b.96.69.a5.2d.8711e.e1.2d.1e3e1.1e.78.d2269.1e.e1187.96.4b.0f3b4.5a.5a.e112d.3c2c33b425a25a.6912d11e25a3e1.1e26911e3783d2.7814b.3c.4b.0f23c.d2.4b.a5187.0f.a52a5.2d.0f.1e269.1e.4b.3c2c3.0f.a5.a5.1e.5a14b.87.1e.4b.b425a.c3.1e.4b.2d11e.c311e14b.0f187.a511e269.3c3d2269.3c2c300f296.4b11e14b.8723c.d2.69.3c.5a.5a3d2.78.5a.5a.e100f.b4.5a14b.0f00f.3c.5a.c311e.6923c.d2.4b187.2d.87187.b4.5a.e1187.1e.d2378.c32a518729625a.c32a5.96.5a2c33b4.5a.6911e.4b.87187.1e2c3.3c3d2.4b00f.2d.96378.5a.6912d.a500f.2d23c.4b.2d00f.a5.a5.a5.2d.8712d.a512d296.a5.a5.9614b.96.5a.e123c.c3.b4.4b12d.87.0f296269.1e.d2.4b11e.78.c3.0f296.4b.0f11e.e1.2d23c.d2.783d2.78.5a.693e1.2d12d.1e.e100f3b4.4b2a52a5.0f296.78.d2.69.8700f2a500f.9614b23c3d2.69.87.87187.b4.4b00f.2d.872a5.96.5a14b.b4.4b.0f.0f3b4.5a.e1296.69.3c3d2.691872a5.96378.5a.78.5a3e1.0f.0f.a5296269.2d12d.a5.0f.0f.1e.6912d.a5.1e14b3e100f.87.1e269.87.2d12d.0f.872a5.1e.69.a52a5.1e3d2.5a.c33b4.4b.1e3e1.2d.87187.1e3782c3187296.4b11e.78.5a2c3.3c.d2378.5a3d2.78.c300f.1e.6923c.d2.5a.69.3c.4b12d.0f.a512d.0f.8723c.5a.e123c.d2.4b.a5.2d.2d00f.1e25a2c3.b4.0f.2d11e.c3.a51873b4.4b11e2c3.872a529614b11e2c3.87296378.c3296378.d214b23c.c3.a5.96378.c3.87187.96.4b.2d12d"
        val grid = Grid4.fromString( gridString )
        assert( grid.value( grid.faces( 0 )( 0 ) ) == Some( 1 ) )
        assert( grid.state( grid.hedges( 1 )( 0 ) ) == Black )
        assert( grid.state( grid.vedges( 0 )( 1 ) ) == Red )
        assert( gridString == grid.complexString )
    }

    @Test def equalities() {
        val grid = Grid4.empty( 4, 7 )
        assert( grid.vertices( 0 )( 0 ) == grid.vertices( 0 )( 0 ) )
        assert( grid.vertices( 0 )( 0 ) != grid.vertices( 1 )( 0 ) )
        assert( grid.faces( 0 )( 0 ) == grid.faces( 0 )( 0 ) )
        assert( grid.faces( 0 )( 0 ) != grid.faces( 1 )( 0 ) )
        assert( grid.hedges( 0 )( 0 ) == grid.hedges( 0 )( 0 ) )
        assert( grid.hedges( 0 )( 0 ) != grid.hedges( 1 )( 0 ) )
    }

    @Test def sizeIterators() {
        val rows = 4;
        val cols = 7;
        val grid = Grid4.empty( rows, cols )
        assert( grid.faceIterator.size == rows * cols )
        assert( grid.vertexIterator.size == (rows + 1) * (cols + 1) )
        assert( grid.edgeIterator.size == (rows + 1) * cols + rows * (cols + 1) )
    }

    @Test def otherVertexTests() {
        val grid = Grid4.empty( 4, 7 )
        val edge = grid.hedges( 0 )( 0 )
        assert( edge.va == grid.vertices( 0 )( 0 ) )
        assert( edge.vb == grid.vertices( 0 )( 1 ) )
        assert( edge.otherVertex( edge.va ) == edge.vb )
        assert( edge.otherVertex( edge.vb ) == edge.va )
    }

    @Test def otherFaceTests() {
        val grid = Grid4.empty( 4, 7 )
        val edge: HorizontalEdge4 = grid.hedges( 1 )( 0 )
        println( edge.fa )
        println( edge.fb )
        assert( edge.otherFace( grid.faces( 0 )( 0 ) ) == Some( grid.faces( 1 )( 0 ) ) )
    }

    @Test def spotTest1() {
        val grid = Grid4.empty( 4, 7 )
        val s1 = new Spot( grid, grid.vertices( 1 )( 1 ), North )
        println( s1 )
        assert( s1.northEdge == grid.vedges( 0 )( 1 ) )
        assert( s1.southEdge == grid.vedges( 1 )( 1 ) )
        assert( s1.westEdge == grid.hedges( 1 )( 0 ) )
        assert( s1.eastEdge == grid.hedges( 1 )( 1 ) )
        val s4 = s1.turnRight
        println( s4 )
        assert( s4.northEdge == grid.hedges( 1 )( 1 ) )
        assert( s4.southEdge == grid.hedges( 1 )( 0 ) )
        assert( s4.westEdge == grid.vedges( 0 )( 1 ) )
        assert( s4.eastEdge == grid.vedges( 1 )( 1 ) )
        val s5 = s4.forward
        println( s5 )
        assert( s5.northEdge == grid.hedges( 1 )( 2 ) )
        assert( s5.southEdge == grid.hedges( 1 )( 1 ) )
        assert( s5.westEdge == grid.vedges( 0 )( 2 ) )
        assert( s5.eastEdge == grid.vedges( 1 )( 2 ) )
        val s2 = s1.shiftRight
        println( s2 )
        assert( s2.northEdge == grid.vedges( 0 )( 2 ) )
        assert( s2.southEdge == grid.vedges( 1 )( 2 ) )
        assert( s2.westEdge == grid.hedges( 1 )( 1 ) )
        assert( s2.eastEdge == grid.hedges( 1 )( 2 ) )
        val s3 = s2.turnBack
        println( s3 )
        assert( s3.northEdge == grid.vedges( 1 )( 2 ) )
        assert( s3.southEdge == grid.vedges( 0 )( 2 ) )
        assert( s3.westEdge == grid.hedges( 1 )( 2 ) )
        assert( s3.eastEdge == grid.hedges( 1 )( 1 ) )
    }

    @Test def copyTestGrid4() {
        val grid1 = Grid4.fromString( grid20x20A )
        val grid2: Grid4 = grid1.newChild
        grid2.move( grid2.vedges( 0 )( 0 ) -> Red )
        grid2.move( grid2.hedges( 0 )( 0 ) -> Red )
        println( grid1.displayString )
        println()
        println( grid2.displayString )
        assert( grid2.state( grid2.vedges( 0 )( 0 ) ) == Red )
        assert( grid1.state( grid1.vedges( 0 )( 0 ) ) == White )
        assert( grid1.value( grid1.faces( 0 )( 1 ) ) == Some( 3 ) )
        assert( grid2.value( grid2.faces( 0 )( 1 ) ) == Some( 3 ) )
    }

    @Test def copyTestVertexStateGrid4() {
        val grid1 = VertexStateGrid4.fromString( grid20x20A )
        val grid2: VertexStateGrid4 = grid1.newChild
        grid2.move( grid2.vedges( 0 )( 0 ) -> Red )
        grid2.move( grid2.hedges( 0 )( 0 ) -> Red )
        println( grid1.displayString )
        println()
        println( grid2.displayString )
        assert( grid2.state( grid2.vedges( 0 )( 0 ) ) == Red )
        assert( grid1.state( grid1.vedges( 0 )( 0 ) ) == White )
        assert( grid1.value( grid1.faces( 0 )( 1 ) ) == Some( 3 ) )
        assert( grid2.value( grid2.faces( 0 )( 1 ) ) == Some( 3 ) )
    }

    @Test def verifyComplete() {
        var grid: Grid4 = Grid4.fromString( "3x2 !3e0.20.b0.3c.d0260" )
        assert( grid.isComplete )
        grid = Grid4.fromString( "3x2 !3e0.201b0.3c.d0260" )
        assert( !grid.isComplete )
        grid = Grid4.fromString( "3x2 !3e0.20.b0.3c.d0260", true )
        assert( !grid.isComplete )
        grid = Grid4.fromString( "3x2 !3e0.201b0.3c.d0260", true )
        assert( !grid.isComplete )
        grid = Grid4.fromString( "3x2 !370.50.60.90.40.10", true )
        assert( !grid.isComplete )
        grid = Grid4.fromString( "3x2 !378.5a.e1.96.4b23c", true )
        assert( grid.isComplete )
    }

    @Test def checkCounts() {
        val grid: Grid4 = Grid4.fromString( "4x4 !.00.00.80.00.10340120.00.00101.04.00.00.00.00.00" )
        println( grid.redCount )
        assert( grid.redCount == 1 )
        assert( grid.blackCount == 2 )
        val subGrid: Grid4 = grid.newChild
        assert( subGrid.redCount == 1 )
        assert( subGrid.blackCount == 2 )

        subGrid.move( subGrid.vedges( 0 )( 0 ) -> Red )

        assert( grid.redCount == 1 )
        assert( grid.blackCount == 2 )
        assert( subGrid.redCount == 2 )
        assert( subGrid.blackCount == 2 )
    }

    @Test def createColoredGrid() {
        val grid = ColoredGrid4.fromString( "4x4 !.00.80.10.c0.903702c0.20.20180230.40.00.30.40.00" )
    }

    @Test def colorTest1() {
        val grid = ColoredGrid4.empty( 2, 2 )
        val a = grid.colorOf( grid.faces( 0 )( 0 ) )
        val b = grid.colorOf( grid.faces( 0 )( 1 ) )
        val c = grid.colorOf( grid.faces( 1 )( 0 ) )
        val d = grid.colorOf( grid.faces( 1 )( 1 ) )
        grid.setColorsOpposite( a, b )
        grid.setColorsOpposite( a, c )
        grid.setColorsOpposite( c, d )
        assert( grid.isOppositeColor( a, b ) )
        assert( grid.isOppositeColor( c, d ) )
        assert( grid.isOppositeColor( a, c ) )
        assert( grid.isOppositeColor( b, d ) )
        assert( grid.isSameColor( a, d ) )
        assert( grid.isSameColor( b, c ) )
        for (color <- List( a, b, c, d )) {
            assert( grid.getLatestColor( color ) == grid.oppositeColor( grid.oppositeColor( color ) ) )
        }
    }

    @Test def colorTest2() {
        val grid = ColoredGrid4.empty( 2, 2 )
        val a = grid.colorOf( grid.faces( 0 )( 0 ) )
        val b = grid.colorOf( grid.faces( 0 )( 1 ) )
        val c = grid.colorOf( grid.faces( 1 )( 0 ) )
        val d = grid.colorOf( grid.faces( 1 )( 1 ) )
        grid.setColorsOpposite( a, b )
        grid.setColorsOpposite( c, d )
        grid.setColorsSame( a, c )
        assert( grid.isOppositeColor( a, b ) )
        assert( grid.isOppositeColor( a, d ) )
        assert( grid.isOppositeColor( b, c ) )
        assert( grid.isOppositeColor( c, d ) )
        assert( grid.isSameColor( a, c ) )
        assert( grid.isSameColor( b, d ) )
        for (color <- List( a, b, c, d )) {
            assert( grid.getLatestColor( color ) == grid.oppositeColor( grid.oppositeColor( color ) ) )
        }
    }

    @Test def colorTest3() {
        val grid = ColoredGrid4.empty( 2, 2 )
        val a = grid.colorOf( grid.faces( 0 )( 0 ) )
        val b = grid.colorOf( grid.faces( 0 )( 1 ) )
        val c = grid.colorOf( grid.faces( 1 )( 0 ) )
        val d = grid.colorOf( grid.faces( 1 )( 1 ) )
        grid.setColorsOpposite( a, c )
        grid.setColorsOpposite( a, b )
        assert( grid.isSameColor( b, c ) )
    }

    @Test def completeTest1() {
        val grid = ColoredGrid4.fromString( "10x10 !.96.69.a5.2c.00200.00300.003303e1.1e378.42.00200.00.00200.003b425a.4a390.48200.00200200.00.61.1e242.21.06300.00.00.00.00.00.02200390.48.00.00300.002003c0.002012a5.06300.00200200200.a4.00.00.20.00.80.00200.002003e1.0c.00.00200320.08200.002003b425a.40.08200200.02.00.00.003e1.86.10242380.00200380.00390" )
        assert( !grid.isComplete )
    }

    @Test def openBorderedTest() {
        val grid = ColoredGrid4.fromString( "4x3 .....33.....", true ) // open bordered
        assert( grid.openBorders )
        val child = grid.newChild
        assert( child.openBorders )
    }

    @Test def gridCompareToTest() {
        val a = ColoredGrid4.fromString( "3x3 !106.00.00.00200.00.00.00390" )
        val c = ColoredGrid4.fromString( "3x3 !106.00.00.00200.00.00.00390" )
        val b = ColoredGrid4.fromString( "3x3 !106.00.00.00200.00.00.103d0" )
        assert( a.compareTo( a ) == 0 )
        assert( a.compareTo( c ) == 0 )
        assert( a.compareTo( b ) == -1 )
        assert( b.compareTo( a ) == 1 )

        a.setColorsSame( a.colorOf( a.faces( 0 )( 0 ) ), a.colorOf( a.faces( 1 )( 0 ) ) )
        c.setColorsSame( c.colorOf( c.faces( 0 )( 0 ) ), c.colorOf( c.faces( 1 )( 0 ) ) )

        assert( a.compareTo( c ) == 0 )

        a.setColorsSame( a.colorOf( a.faces( 0 )( 0 ) ), a.colorOf( a.faces( 0 )( 1 ) ) )
        c.setColorsSame( c.colorOf( c.faces( 0 )( 0 ) ), c.colorOf( c.faces( 2 )( 0 ) ) )

        assert( a.compareTo( c ) != 0 )
    }

    @Test def ruleApplicationTesting() {
        println( "ruleApplicationTesting" )
        val rule = BasicRule.deserialize( "brule[s4:s1x1:open:f1:e....:v7f7f7f7f:c;s4:s1x1:open:f1:e....:v7d7e7b77:c;]" )
        val grid = ColoredGrid4.fromString( "s4:s1x1:open:f1:e....:v7f7f7f3f:c;" )
        val resultGrid = ColoredGrid4.fromString( "s4:s1x1:open:f1:e....:v7d7e7b37:c;" )
        rule.applyTo( grid )
        //        rule.apply( new Spot( grid, grid.vertices( 0 )( 0 ), East ), false )
        println( grid.serialString )
        println( resultGrid.serialString )
        assert( grid.compareTo( resultGrid ) == 0 )
    }

    @Test def ruleDominationTest1() {
        val rule = BasicRule.deserialize( "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx.........x..x....:v7f7f7f7f7f48447f7f41427f7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;]" )
        val grid = ColoredGrid4.fromString( "s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f767f7f7f7f7f:c;" )
        val resultGrid = ColoredGrid4.fromString( "s4:s3x3:open:f....0....:e.....xx.........x..x....:v7f7f7f7f7f48447f7f41427f7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;" )
        rule.applyTo( grid )
        println( grid.serialString )
        println( resultGrid.serialString )
        assert( grid.compareTo( resultGrid ) == 0 )
    }

    @Test def ruleDominationTest2() { // rule 1 dominates rule 2, so check that
        println( "ruleDominationTest2" )
        val rule1 = BasicRule.deserialize( "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx.........x..x....:v7f7f7f7f7f48447f7f41427f7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;]" )
        val rule2 = BasicRule.deserialize( "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f767f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx.........x..x....:v7f7f7f7f7f48447f7f41427f7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;]" )
        assert( rule1.patternConstraintCount < rule2.patternConstraintCount )
        val grid = rule2.beforeGrid
        BasicRule.applyRulesTo( grid, List( rule1 ) )
        println( grid.serialString )
        assert( grid.compareTo( rule2.afterGrid ) == 0 )
    }

    @Test def ruleDominationTest3() {
        println( "ruleDominationTest3" )
        val grid = ColoredGrid4.fromString( "s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f373b7f7f3e347f7f7f7f7f:c;" )
        val rule = BasicRule.deserialize( "brule[s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f7f7f7f7f7f777f7f7f7f7f:c;s4:s3x3:open:f....3....:e.x...-.........x-.......:v7f5f7f7f6f02197f7f2c357f7f7f7f7f:c/0,0-1,0-0,1\\1,1/1,2\\2,1;]" )
        println( rule )
        assert( !rule.matches( new Spot( grid, grid.vertices( 3 )( 3 ), North ), true ) )
        BasicRule.applyRulesTo( grid, List( rule ) ) // has been throwing exceptions!
    }

}
