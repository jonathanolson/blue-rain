package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import java.awt.{Dimension, GridBagConstraints, GridBagLayout}
import com.jonathanolson.slither.model.{Face4, FaceValueListener, EdgeStateListener, ColoredGrid4, Edge4, VertexStateGrid4, Grid4}
import scala.collection.mutable.ArrayStack
import java.awt.event.{KeyEvent, KeyListener, ActionListener, ActionEvent}
import javax.swing.{JCheckBox, JTextField, JButton, JPanel, JFrame}
import com.jonathanolson.slither.util.LogHelper
import com.jonathanolson.slither.view.{VisibilityControlsPanel, SlitherCanvas, SlitherNodeListener}
import com.jonathanolson.slither.solvers.{StaticRuleSolver, SolutionFinder, StaticEdgePatternSolver, BacktrackSolver, SolverStack, LoopInvalidator, MediumFaceColorSolver, FaceColorEdgeSolver, SimpleFaceColorSolver, SimpleVertexStateSolver, StaticFaceVertexStateSolver, StaticFaceSolver, FaceVertexStateSolver, VertexStateSolver, SimpleVertexSolver, SimpleFaceSolver}

object SlitherPlayer {
    def main( args: Array[String] ) {
        val player: SlitherPlayer = new SlitherPlayer
    }
}

class SlitherPlayer extends JFrame( "SlitherPlayer" ) with LogHelper {
    private val canvas = new SlitherCanvas
    private val mainPanel = new JPanel( new GridBagLayout )
    private val controlPanel = new JPanel( new GridBagLayout )
    private val visibilityControls = new VisibilityControlsPanel
    private val inputPanel = createInputPanel
    private val solveButton = new JButton( "Solve" )
    private val toggleAutoButton = new JButton( "Toggle Auto" )
    private val oneDeepButton = new JButton( "1-Deep" )
    private val twoDeepButton = new JButton( "2-Deep" )
    private val xDeepButton = new JButton( "x-Deep" )
    private val printSolutionsButton = new JButton( "Backtrack" )
    private val forkButton = new JButton( "Fork" )
    private val unforkButton = new JButton( "Unfork" )
    private val complexText = new JTextField( "" )

    private val fixedStack = new ArrayStack[(Grid4, SolverStack)]

    private var stringsValid = false

    var grid: Grid4 = null
    var stack: SolverStack = null

    complexText.setPreferredSize( new Dimension( 400, 20 ) )

    setContentPane( mainPanel )
    mainPanel.add( controlPanel, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c; } )
    mainPanel.add( visibilityControls, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 1; c; } )
    mainPanel.add( canvas, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 2; c; } )
    mainPanel.add( complexText, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 3; c; } )

    controlPanel.add( inputPanel, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c; } )
    controlPanel.add( solveButton, {val c = new GridBagConstraints(); c.gridx = 1; c.gridy = 0; c; } )
    controlPanel.add( toggleAutoButton, {val c = new GridBagConstraints(); c.gridx = 2; c.gridy = 0; c; } )
    controlPanel.add( oneDeepButton, {val c = new GridBagConstraints(); c.gridx = 3; c.gridy = 0; c; } )
    controlPanel.add( twoDeepButton, {val c = new GridBagConstraints(); c.gridx = 4; c.gridy = 0; c; } )
    controlPanel.add( xDeepButton, {val c = new GridBagConstraints(); c.gridx = 5; c.gridy = 0; c; } )
    controlPanel.add( printSolutionsButton, {val c = new GridBagConstraints(); c.gridx = 6; c.gridy = 0; c} )
    controlPanel.add( forkButton, {val c = new GridBagConstraints(); c.gridx = 7; c.gridy = 0; c; } )
    controlPanel.add( unforkButton, {val c = new GridBagConstraints(); c.gridx = 8; c.gridy = 0; c; } )

    canvas.addNodeListener( new SlitherNodeListener {
        override def onEdgeLeftClick( edge: Edge4 ) = {
            getGrid.state( edge ) = getGrid.state( edge ) match {
                case White => Black
                case Black => Red
                case Red => White
            }
        }

        override def onEdgeRightClick( edge: Edge4 ) = {
            getGrid.state( edge ) = getGrid.state( edge ) match {
                case White => Red
                case Black => White
                case Red => Black
            }
        }
    } )

    solveButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = (new Thread {
            override def run = stack.solve()
        }).start()
    } )

    toggleAutoButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = for (solver <- stack.solvers) {solver.setAuto( !solver.isAuto )}
    } )

    oneDeepButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = (new Thread {
            override def run = (new BacktrackSolver).breadthFirstToNSteps( grid, stack, 1 )
        }).start()
    } )

    twoDeepButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = (new Thread {
            override def run = (new BacktrackSolver).breadthFirstToNSteps( grid, stack, 2 )
        }).start()
    } )

    xDeepButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = (new Thread {
            override def run = (new BacktrackSolver( colorMatch = true )).adaptiveBreadthFirstToNSteps( grid, stack, grid.whiteCount )
        }).start()
    } )

    printSolutionsButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            (new Thread {
                override def run = {
                    val backtracker = SolutionFinder.solutionsOf( grid, random = false )
                    for (completeGrid <- backtracker) {
                        println( completeGrid.complexString )
                        println( completeGrid.displayString )
                    }
                }
            }).start()
        }
    } )

    forkButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            fixedStack.push( (grid, stack) )
            setGrid( grid.newChild )
            stack = stack.forChild( grid )
        }
    } )

    unforkButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            if ( !fixedStack.isEmpty ) {
                val (tGrid, tStack) = fixedStack.pop
                setGrid( tGrid )
                stack = tStack
            }
        }
    } )

    addKeyListener( new KeyListener {
        def keyReleased( p1: KeyEvent ) = {}

        def keyPressed( p1: KeyEvent ) = {
            p1.getKeyCode match {
                case KeyEvent.VK_R => canvas.slitherNode.setVertexStateRed( !canvas.slitherNode.getVertexStateRed )
                case KeyEvent.VK_B => canvas.slitherNode.setVertexStateBlue( !canvas.slitherNode.getVertexStateBlue )
                case _ => ()
            }
        }

        def keyTyped( p1: KeyEvent ) = {}
    } )

    val gridListener = new FaceValueListener with EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = invalidateStrings()

        override def onFaceValueChange( face: Face4, oldValue: Option[Int], newValue: Option[Int] ) = invalidateStrings()
    }

    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    setVisible( true );

    pack()

    def getGrid: Grid4 = grid

    def setGrid( grid: Grid4, keepSettings: Boolean = true ) {
        if ( getGrid != null ) {
            getGrid.removeEdgeStateListener( gridListener )
            getGrid.removeFaceValueListener( gridListener )
        }
        this.grid = grid
        canvas.setGrid( grid )
        visibilityControls.setSlitherNode( canvas.slitherNode, keepSettings )
        grid.addEdgeStateListener( gridListener )
        grid.addFaceValueListener( gridListener )
        pack()
    }

    def setRootGrid( grid: Grid4 ) {
        setGrid( grid, false )
        fixedStack.clear
    }

    def createInputPanel: JPanel = {
        val ret = new JPanel
        val inputText = new JTextField( "" )
        inputText.setPreferredSize( new Dimension( 400, 20 ) )
        val inputButton = new JButton( "input" )

        ret.add( inputText )
        ret.add( inputButton )

        inputButton.addActionListener( new ActionListener {
            def actionPerformed( p1: ActionEvent ) = {
                val (rows, cols) = Grid4.sizeFromString( inputText.getText )
                setRootGrid( ColoredGrid4.empty( rows, cols ) )
                //                setGrid( ColoredGrid4.fromString( inputText.getText ) )
                stack = new SolverStack( grid )

                stack.solvers += StaticFaceSolver.attach( grid )
                if ( grid.isInstanceOf[VertexStateGrid4] ) {
                    stack.solvers += StaticFaceVertexStateSolver.attach( grid )
                }

                stack.solvers += SimpleFaceSolver.attach( grid )
                stack.solvers += SimpleVertexSolver.attach( grid )
                stack.solvers += SimpleVertexStateSolver.attach( grid )
                stack.solvers += VertexStateSolver.attach( grid )
                stack.solvers += FaceVertexStateSolver.attach( grid )
                stack.solvers += SimpleFaceColorSolver.attach( grid )
                stack.solvers += FaceColorEdgeSolver.attach( grid )
                stack.solvers += MediumFaceColorSolver.attach( grid )
                if ( grid.openBorders ) {
                    stack.solvers += LoopInvalidator.attach( grid )
                }

//                stack.solvers += StaticRuleSolver.attach( grid )

                //                val staticPatternSolver: StaticEdgePatternSolver = StaticEdgePatternSolver.attach( grid )
                //                stack.solvers += staticPatternSolver
                //                staticPatternSolver.setCopy( false ) // don't use this for sub-procedures

                Grid4.setState( grid, inputText.getText )
            }
        } )

        ret
    }

    def invalidateStrings() = {
        stringsValid = false
    }

    (new Thread {
        override def run = {
            while (true) {
                if ( (complexText != null) && (grid != null) ) {
                    val str = getGrid.serialString
                    if ( str != complexText.getText ) {
                        complexText.setText( str )
                    }
                    stringsValid = true
                }
                Thread.sleep( 1000 )
            }
        }
    }).start()
}