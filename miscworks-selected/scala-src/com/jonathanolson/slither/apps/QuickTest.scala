package com.jonathanolson.slither.apps

import com.jonathanolson.slither.patterns.BasicRule
import com.jonathanolson.slither.solvers.SolutionFinder
import com.jonathanolson.slither.model.{GridConstraints, ColoredGrid4}

object QuickTest {
    def main( args: Array[String] ) {
        val g1 = ColoredGrid4.fromString( "s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f007f7f7f7f7f:c;" )
        val grid = BasicRule.canonicalPaddedGrid( g1 )
        val rule = BasicRule.ruleFrom( grid )
        println( rule.serialString )

        //        val constraints = new GridConstraints( grid.rows, grid.cols )
        //        constraints.init( grid )

//        for (completeGrid <- SolutionFinder.solutionsOf( grid )) {
//            //            constraints.allow( completeGrid )
//            println( completeGrid.serialString )
//        }
    }
}