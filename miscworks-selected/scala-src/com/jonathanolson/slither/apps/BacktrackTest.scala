package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.view.SimpleSlitherFrame
import com.jonathanolson.slither.exceptions.InvalidGridException
import scala.collection.mutable.{PriorityQueue, ArrayBuffer}
import scala.util.Random
import java.io.{BufferedReader, FileReader, File}
import com.jonathanolson.slither.model.{ColoredGrid4, GridConstraints, Edge4, Grid4, VertexStateGrid4}
import com.jonathanolson.slither.util.LogHelper
import com.jonathanolson.slither.solvers.{SolutionFinder, BacktrackSolver, MediumFaceColorSolver, SolverStack, SimpleFaceColorSolver, FaceColorEdgeSolver, SimpleVertexStateSolver, LoopInvalidator, SimpleFaceSolver, SimpleVertexSolver, VertexStateSolver, FaceVertexStateSolver, StaticFaceSolver, StaticFaceVertexStateSolver}

object BacktrackTest extends LogHelper {
    def main( args: Array[String] ) {

        val file = new File( "/home/jon/miscworks/trunk/data/slither/kwontomloop/all-puzzles" )

        val reader = new BufferedReader( new FileReader( file ) )

        var done = false

        while (!done) {
            val line = reader.readLine
            if ( (line == null) || (line.trim.length == 0) ) done = true else {
                val startGrid = ColoredGrid4.fromString( line )
                println( startGrid.simpleString )
                SolutionFinder.solutionsOf( startGrid ).foreach( (soln: Grid4) => println( soln.complexString ) )
            }
        }

    }

}
