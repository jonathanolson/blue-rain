package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.solvers.SolutionFinder
import com.jonathanolson.slither.patterns.BasicRule
import com.jonathanolson.slither.view.RulePanel
import javax.swing.{JScrollPane, BoxLayout, SwingConstants, JPanel, JFrame}
import scala.collection.mutable.ListBuffer
import java.awt.{Dimension, FlowLayout}
import java.io.{FileWriter, File}
import com.jonathanolson.slither.model.{Vertex4, Edge4, Face4, VertexState, ColoredGrid4, GridConstraints}
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.LogHelper

object RuleTest extends LogHelper {
    def main( args: Array[String] ) {
        val frame = new RuleTest

        var pattSet = Set.empty[String]

        val writer = new FileWriter( new File( "/home/jon/miscworks/trunk/data/slither/rules/raw-out3" ), true )

        def processGrid( grid: ColoredGrid4 ): Unit = {
            val paddedGrid = BasicRule.canonicalPaddedGrid( grid ) // also canonical!
            val pattString = paddedGrid.serialString
            info( "patt: " + pattString )
            if ( pattSet.contains( pattString ) ) {
                info( "duplicate" )
            } else {
                val rule: BasicRule = ruleFrom( paddedGrid )
                writer.write( rule.serialString + "\n" )
                writer.flush()
                frame.addRule( rule )
            }
            pattSet += pattString
        }

        val singleGrid = ColoredGrid4.empty( 1, 1, true )
        val doubleGrid = ColoredGrid4.empty( 2, 2, true )
        val tripleGrid = ColoredGrid4.empty( 3, 3, true )

        val gridStrings = List(
            "3x2 !.00.80.00.00.20.00",
            "3x2 !.00.08.00.00.02.00",
            "3x3 ....0....",
            "3x3 ....1....",
            "3x3 ....2....",
            "3x3 ....3....",
            "4x3 !.00.00.80.00.00300.b0.40.00.00.20.00"
            )

        for (gridString <- gridStrings) {
            processGrid( ColoredGrid4.fromString( gridString, true ) )
        }

        var count = 0

        println( "A: " )
        // single edges on single faces
        for (faceval <- 0 to 3; state <- List( Red, Black )) {
            val grid = ColoredGrid4.empty( 3, 3, true )
            grid.value( grid.faces( 1 )( 1 ) ) = Some( faceval )
            grid.state( grid.hedges( 1 )( 1 ) ) = state
            processGrid( grid )
        }

        println( "B: " )
        // single vertex state disallowed
        for (faceval <- 1 to 3; idx <- 0 to 6) {
            val grid = ColoredGrid4.empty( 3, 3, true )
            grid.value( grid.faces( 1 )( 1 ) ) = Some( faceval )
            grid.vertexState( grid.vertices( 1 )( 1 ) ) = idx match {
                case 0 => new VertexState( false, true, true, true, true, true, true )
                case 1 => new VertexState( true, false, true, true, true, true, true )
                case 2 => new VertexState( true, true, false, true, true, true, true )
                case 3 => new VertexState( true, true, true, false, true, true, true )
                case 4 => new VertexState( true, true, true, true, false, true, true )
                case 5 => new VertexState( true, true, true, true, true, false, true )
                case 6 => new VertexState( true, true, true, true, true, true, false )
            }
            processGrid( grid )
        }

        println( "C: " )
        // all combinations of 1 face 1 vertex with all vertex states
        for (
            faces <- combinations( singleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( singleGrid, faces );
            grid <- iterateGridWithFullVertexState( facedGrid, List( facedGrid.vertices( 0 )( 0 ) ) )
        ) {
            processGrid( grid )
        }

        println( "D: " )
        // all edges and faces on single
        for (
            faces <- combinations( singleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( singleGrid, faces );
            edges <- combinations( singleGrid.edgeIterator.toList );
            grid <- iterateGridWithEdges( facedGrid, edges )
        ) {
            processGrid( grid )
        }

        println( "E: " )
        // all combinations of 1 face 4 vertices with incident / blocked vertex states
        for (
            faces <- combinations( singleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( singleGrid, faces );
            vertices <- combinations( singleGrid.vertexIterator.toList );
            grid <- iterateGridWithIncidentBlockedVertexState( facedGrid, vertices )
        ) {
            processGrid( grid )
        }

        println( "F: " )
        // all quad faces
        for (faces <- combinations( doubleGrid.faceIterator.toList, 1 )) {
            for (grid <- iterateGridWithFaces( doubleGrid, faces )) {
                processGrid( grid )
            }
        }

        println( "G: " )
        // all combinations of 1 face 4 vertices with all vertex states
        for (
            faces <- combinations( singleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( singleGrid, faces );
            vertices <- combinations( singleGrid.vertexIterator.toList );
            grid <- iterateGridWithFullVertexState( facedGrid, vertices )
        ) {
            processGrid( grid )
        }

        println( "H: " )
        // all edges and faces on double
        for (
            faces <- combinations( doubleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( doubleGrid, faces );
            edges <- combinations( doubleGrid.edgeIterator.toList );
            grid <- iterateGridWithEdges( facedGrid, edges )
        ) {
            processGrid( grid )
        }

        println( "I: " )
        // all single pair colorings with one face on triple
        for (
            facedGrid <- iterateGridWithFaces( tripleGrid, List( tripleGrid.faces( 1 )( 1 ) ) );
            grid <- iterateGridWithSinglePairColoring( facedGrid, facedGrid.faceIterator.toList )
        ) {
            processGrid( grid )
        }

        println( "J: " )
        // all triple-triple faces
        for (faces <- combinations( tripleGrid.faceIterator.toList, 1 )) {
            for (grid <- iterateGridWithFaces( tripleGrid, faces )) {
                processGrid( grid )
            }
        }

        println( "K: " )
        // all edges and faces on triple
        for (
            faces <- combinations( tripleGrid.faceIterator.toList );
            facedGrid <- iterateGridWithFaces( tripleGrid, faces );
            edges <- combinations( tripleGrid.edgeIterator.toList );
            grid <- iterateGridWithEdges( facedGrid, edges )
        ) {
            processGrid( grid )
        }

        writer.close()
    }

    def ruleFrom( grid: ColoredGrid4 ): BasicRule = {
        val constraints = new GridConstraints( grid.rows, grid.cols )
        constraints.init( grid )

        val a = System.currentTimeMillis

        for (completeGrid <- SolutionFinder.solutionsOf( grid )) {
            constraints.allow( completeGrid )
        }

        val b = System.currentTimeMillis

        info( (b - a) + " ms" )

        val rule = constraints.constructRule.canonical

        info( rule.serialString )

        return rule
    }

    def iterateGridWithFaces[U]( grid: ColoredGrid4, faces: List[Face4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            faces.size match {
                case 0 => f( grid )
                case _ => {
                    val face = faces.head
                    for (faceval <- 0 to 3) {
                        val g = grid.newChild
                        g.value( face ) = Some( faceval )
                        iterateGridWithFaces( g, faces.tail ).foreach( f )
                    }
                }
            }
        }
    }

    def iterateGridWithEdges[U]( grid: ColoredGrid4, edges: List[Edge4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            edges.size match {
                case 0 => f( grid )
                case _ => {
                    val edge = edges.head
                    for (edgeState <- List( Red, Black )) {
                        val g = grid.newChild
                        g.state( edge ) = edgeState
                        iterateGridWithEdges( g, edges.tail ).foreach( f )
                    }
                }
            }
        }
    }

    def iterateGridWithSingleVertexState[U]( grid: ColoredGrid4, vertices: List[Vertex4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            vertices.size match {
                case 0 => f( grid )
                case _ => {
                    val vertex = vertices.head
                    for (vertexState <- List(
                        new VertexState( false, true, true, true, true, true, true ),
                        new VertexState( true, false, true, true, true, true, true ),
                        new VertexState( true, true, false, true, true, true, true ),
                        new VertexState( true, true, true, false, true, true, true ),
                        new VertexState( true, true, true, true, false, true, true ),
                        new VertexState( true, true, true, true, true, false, true ),
                        new VertexState( true, true, true, true, true, true, false )
                        )) {
                        val g = grid.newChild
                        g.vertexState( vertex ) = g.vertexState( vertex ) & vertexState
                        iterateGridWithSingleVertexState( g, vertices.tail ).foreach( f )
                    }
                }
            }
        }
    }

    def iterateGridWithIncidentBlockedVertexState[U]( grid: ColoredGrid4, vertices: List[Vertex4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            vertices.size match {
                case 0 => f( grid )
                case _ => {
                    val vertex = vertices.head
                    for (vertexState <- List(
                        VertexState.All.withIncident( Northwest ),
                        VertexState.All.withIncident( Northeast ),
                        VertexState.All.withBlocked( Northwest ),
                        VertexState.All.withBlocked( Northeast )
                        )) {
                        val g = grid.newChild
                        g.vertexState( vertex ) = g.vertexState( vertex ) & vertexState
                        iterateGridWithIncidentBlockedVertexState( g, vertices.tail ).foreach( f )
                    }
                }
            }
        }
    }

    def iterateGridWithFullVertexState[U]( grid: ColoredGrid4, vertices: List[Vertex4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            vertices.size match {
                case 0 => f( grid )
                case _ => {
                    val vertex = vertices.head
                    for (empty <- List( true, false );
                         vertical <- List( true, false );
                         horizontal <- List( true, false );
                         northwest <- List( true, false );
                         northeast <- List( true, false );
                         southeast <- List( true, false );
                         southwest <- List( true, false );
                         vertexState = new VertexState( empty, vertical, horizontal, northwest, northeast, southeast, southwest )) {
                        val g = grid.newChild
                        g.vertexState( vertex ) = g.vertexState( vertex ) & vertexState
                        iterateGridWithFullVertexState( g, vertices.tail ).foreach( f )
                    }
                }
            }
        }
    }

    def iterateGridWithSinglePairColoring[U]( grid: ColoredGrid4, faces: List[Face4] ): Traversable[ColoredGrid4] = new Traversable[ColoredGrid4] {
        def foreach[U]( f: (ColoredGrid4) => U ) = {
            for (facePair <- combinationsOfN( faces, 2 )) {
                for (same <- List( true, false )) {
                    try {
                        val g = grid.newChild
                        if ( same ) {
                            g.setColorsSame( g.colorOf( facePair( 0 ) ), g.colorOf( facePair( 1 ) ) )
                        } else {
                            g.setColorsOpposite( g.colorOf( facePair( 0 ) ), g.colorOf( facePair( 1 ) ) )
                        }
                        f( g )
                    } catch {
                        case e: InvalidGridException => () // just in case the setting was bad on something already there
                    }
                }
            }
        }
    }

    def combinations[T]( seq: collection.Seq[T], minSize: Int = 0 ): Traversable[List[T]] = {
        require( minSize <= seq.size )
        new Traversable[List[T]] {
            def foreach[U]( f: (List[T]) => U ) = {
                for (x <- minSize to seq.size) {
                    combinationsOfN( seq, x ).foreach( f )
                }
            }
        }
    }

    def combinationsOfN[T]( seq: collection.Seq[T], n: Int ): Traversable[List[T]] = { // iterate through each combination of n elements in seq
        require( n <= seq.size )
        new Traversable[List[T]] {
            def foreach[U]( f: (List[T]) => U ) = {
                def iter( soFar: List[T], idx: Int, m: Int ): Unit = {
                    m match {
                        case 0 => f( soFar.reverse ) // possibly switch to not reverse for performance?
                        case _ => {
                            for (x <- idx to (seq.size - m)) {
                                iter( seq( x ) :: soFar, x + 1, m - 1 )
                            }
                        }
                    }
                }
                iter( Nil, 0, n )
            }
        }
    }
}

class RuleTest extends JFrame {
    val panel: JPanel = new JPanel
    panel.setLayout( new BoxLayout( panel, BoxLayout.PAGE_AXIS ) )
    val pane: JScrollPane = new JScrollPane( panel )
    setContentPane( pane )

    setVisible( true );
    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE )

    def addRule( rule: BasicRule ): Unit = {
        panel.add( new RulePanel( rule ) )
        panel.invalidate()
        panel.revalidate()
    }

    setSize( 300, 400 )

}