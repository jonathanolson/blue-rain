package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import java.awt.{Dimension, FlowLayout, GridBagConstraints, GridBagLayout}
import java.awt.event.{ActionEvent, ActionListener}
import com.jonathanolson.slither.model.{ColoredGrid4, Grid4Hash, FaceValueListener, EdgeStateListener, Face4, Edge4, Grid4}
import com.jonathanolson.slither.patterns.BasicRule
import javax.swing.{SwingUtilities, JRadioButton, JTextArea, JTextField, JButton, JPanel, JFrame}
import com.jonathanolson.slither.view.{SimpleRuleFrame, VisibilityControlsPanel, SlitherCanvas, SlitherNodeListener}

object ExampleEditor {
    def main( args: Array[String] ) {
        val editor = new ExampleEditor

        val grid1 = ColoredGrid4.fromString( "20x20 .3.13.1.2333.1..31002..322..2..0.1.23.1133121..31....22...23..2......121.21.0.1.1.22..2..1.1..1.1..322.1.1111212.1...213.....2.2.31.1113231.1.21.312..01...2..02.01.2.22.....111.3.3.10.23.1.31..1....0....3..1.1..1..1.1.1.10...31.2311112312121.3.....3..1...1......3.2....112..211..11....3.22..3.1...1322022.2111...2.1.12..22..2.2.1.2221.1223.12..22.102.3.11.1.22...13.31..2.132.1.3..2.12221....22...110" )
        grid1.allowsInvalid = true
        editor.setGrid( grid1 )

        //        val grid2: Grid4Hash = Grid4.fromString( "10x10 !.e112d.2d3b4.4b.8700f.96378.c33b4.4b.1e3e1.96.783d2.69.0f3b43e1.0f.0f.2d.a5.0f.a5.0f.1e3e1.3c.c300f.963783d2.782c3.87.3c00f.b425a.e1.0f.a5.872a5.3c25a296.6900f.b4.5a.e1.2d3b4.5a25a.e1.0f296.6900f3b4.5a3e1.96.5a3b4.5a.69.0f.1e.69.87.a5.2d2963e1.1e.4b00f.96.5a3e1.3c.5a3e1.3c3d2.4b11e.e1.873b4.5a2c3.b4" )
        //        grid2.allowsInvalid = true
        //        editor.setGrid( grid2 )
    }
}

class ExampleEditor extends JFrame( "Slither ExampleEditor" ) {
    private val mainPanel = new JPanel( new GridBagLayout )
    private val controlPanel = new JPanel( new GridBagLayout )
    private val visibilityControls = new VisibilityControlsPanel
    private val slitherCanvas = new SlitherCanvas
    private val simpleText = new JTextField( "" )
    private val complexText = new JTextField( "" )
    private val inputPanel = new JPanel()
    private val inputText = new JTextField( "" )
    private val inputButton = new JButton( "input" )
    val cols = new JTextField( "" )
    val rows = new JTextField( "" )
    val create = new JButton( "Create" )
    val open = new JRadioButton( "Open" )
    val generateRule = new JButton( "To Rule" )

    var ruleFrame: Option[RuleFrame] = None

    simpleText.setPreferredSize( new Dimension( 400, 20 ) )
    complexText.setPreferredSize( new Dimension( 400, 20 ) )

    setContentPane( mainPanel )
    mainPanel.add( controlPanel, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c} )
    mainPanel.add( visibilityControls, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 1; c} )
    mainPanel.add( slitherCanvas, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 2; c} )
    mainPanel.add( simpleText, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 3; c} )
    mainPanel.add( complexText, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 4; c} )
    mainPanel.add( inputPanel, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 5; c} )

    inputPanel.add( inputText )
    inputText.setPreferredSize( new Dimension( 400, 20 ) )
    inputPanel.add( inputButton )

    controlPanel.add( cols, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c} )
    cols.setPreferredSize( new Dimension( 50, 20 ) )

    controlPanel.add( rows, {val c = new GridBagConstraints(); c.gridx = 1; c.gridy = 0; c} )
    rows.setPreferredSize( new Dimension( 50, 20 ) )

    controlPanel.add( create, {val c = new GridBagConstraints(); c.gridx = 2; c.gridy = 0; c} )
    controlPanel.add( open, {val c = new GridBagConstraints(); c.gridx = 3; c.gridy = 0; c} )
    controlPanel.add( generateRule, {val c = new GridBagConstraints(); c.gridx = 4; c.gridy = 0; c} )

    slitherCanvas.addNodeListener( new SlitherNodeListener {
        override def onEdgeLeftClick( edge: Edge4 ) = {
            getGrid.state( edge ) = getGrid.state( edge ) match {
                case White => Black
                case Black => Red
                case Red => White
            }
        }

        override def onEdgeRightClick( edge: Edge4 ) = {
            getGrid.state( edge ) = getGrid.state( edge ) match {
                case White => Red
                case Black => White
                case Red => Black
            }
        }

        override def onFaceLeftClick( face: Face4 ) = {
            getGrid.value( face ) = getGrid.value( face ) match {
                case None => Some( 0 )
                case Some( 3 ) => None
                case Some( x ) => Some( x + 1 )
            }
        }

        override def onFaceRightClick( face: Face4 ) = {
            getGrid.value( face ) = getGrid.value( face ) match {
                case None => Some( 3 )
                case Some( 0 ) => None
                case Some( x ) => Some( x - 1 )
            }
        }
    } )

    var grid: ColoredGrid4 = null

    def getGrid = grid

    def invalidateStrings() = {
        simpleText.setText( getGrid.simpleString )
        complexText.setText( getGrid.serialString )
    }

    val listener = new FaceValueListener with EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = invalidateStrings()

        override def onFaceValueChange( face: Face4, oldValue: Option[Int], newValue: Option[Int] ) = invalidateStrings()
    }

    create.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            val emptyGrid = ColoredGrid4.empty( rows.getText.toInt, cols.getText.toInt, open.isSelected )
            emptyGrid.allowsInvalid = true
            setGrid( emptyGrid )
        }
    } )

    generateRule.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            val g = BasicRule.canonicalPaddedGrid( grid )
//            val g = ColoredGrid4.empty( grid.rows, grid.cols, grid.openBorders )
            grid.copyStateTo( g )
            new Thread {
                override def run = {
                    val rule = BasicRule.ruleFrom( g )
                    SwingUtilities.invokeLater( new Runnable {
                        def run = {
                            addRule( rule )
                        }
                    } )
                }
            }.start()
        }
    } )

    inputButton.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            val newGrid = ColoredGrid4.fromString( inputText.getText )
            newGrid.allowsInvalid = true
            setGrid( newGrid )
        }
    } )

    def setGrid( grid: ColoredGrid4 ): Unit = {
        if ( getGrid != null ) {
            getGrid.removeEdgeStateListener( listener )
            getGrid.removeFaceValueListener( listener )
        }
        this.grid = grid
        slitherCanvas.setGrid( grid )
        visibilityControls.setSlitherNode( slitherCanvas.slitherNode, true )
        grid.addEdgeStateListener( listener )
        grid.addFaceValueListener( listener )
        invalidateStrings()
        pack()
    }

    def addRule( rule: BasicRule ): Unit = {
        this.synchronized {
            if ( ruleFrame.isEmpty ) {
                ruleFrame = Some( new RuleFrame() )
            }
            ruleFrame.get.addRule( rule )
        }
    }

    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    setVisible( true );

}