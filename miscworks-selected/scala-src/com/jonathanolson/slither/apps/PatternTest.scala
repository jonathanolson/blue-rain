package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.solvers.SolutionFinder
import com.jonathanolson.slither.model.{ColoredGrid4, GridConstraints, VertexStateGrid4}
import com.jonathanolson.slither.util.Spot
import com.jonathanolson.slither.patterns.{BasicRule, FacePattern}

object PatternTest {
    def main( args: Array[String] ) {
        val grid = ColoredGrid4.fromString( "3x4 !.00.00.00.00300.00.00208.00.00.02.00", true )

        println( "grid: " )
        println( grid.displayString )

        val constraints = new GridConstraints( grid.rows, grid.cols )
        constraints.init( grid )

        val a = System.currentTimeMillis

        for (completeGrid <- SolutionFinder.solutionsOf( grid )) {
            constraints.allow( completeGrid )
        }

        val b = System.currentTimeMillis

        println( (b - a) + " ms" )

        println( "constraints" )
        println( constraints )

        val rule = constraints.constructRule

        println( "rule" )
        println( rule )

        println( "before: " )
        println( rule.beforeGrid.displayString )

        println( "after: " )
        println( rule.afterGrid.displayString )

        val canonicalRule = rule.canonical

        println( "canonical before: " )
        println( canonicalRule.beforeGrid.displayString )
        println( canonicalRule.beforeGrid.serialString )

        println( "canonical after: " )
        println( canonicalRule.afterGrid.displayString )
        println( canonicalRule.afterGrid.serialString )

        println( canonicalRule.serialString )

        val copiedRule = BasicRule.deserialize( canonicalRule.serialString )

        println( copiedRule.serialString )

    }
}