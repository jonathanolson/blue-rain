package com.jonathanolson.slither.apps

import java.io.{BufferedReader, FileReader, File}
import com.jonathanolson.slither.patterns.BasicRule
import scala.collection.mutable.ListBuffer
import javax.swing.{JScrollPane, BoxLayout, JPanel, JFrame}
import com.jonathanolson.slither.view.RulePanel
import scala.util.control.Breaks._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.LogHelper

object RuleReader extends LogHelper {
    def main( args: Array[String] ) {
        val file = new File( "/home/jon/miscworks/trunk/data/slither/rules/rules-c" )
        val reader = new BufferedReader( new FileReader( file ) )
        var ready = false

        val ruleSet = new ListBuffer[BasicRule]

        var count = 0

        while (!ready) {
            val line = reader.readLine
            if ( line == null ) {
                ready = true
            } else {
                // process line
                val rule = BasicRule.deserialize( line )
                ruleSet.append( rule )
                count += 1
                if ( count % 1000 == 0 ) {
                    println( count )
                }
            }
        }


        val frame = new RuleFrame
        var displayCount = 0

        val sortedRules = ruleSet.sortWith( (a, b) => a.patternConstraintCount.compare( b.patternConstraintCount ) < 0 ).toList
        var addedRuleMap = Map.empty[Int, ListBuffer[BasicRule]]

        def addRule( rule: BasicRule ): Unit = {
            val constraints = rule.patternConstraintCount
            if ( !addedRuleMap.contains( constraints ) ) {
                addedRuleMap = addedRuleMap.updated( constraints, new ListBuffer[BasicRule] )
            }
            addedRuleMap( constraints ).append( rule )
            frame.addRule( rule )
            println( rule.serialString )
        }

        def rulesOfLevelsUnder( n: Int ): Traversable[BasicRule] = {
            var ret: Traversable[BasicRule] = Nil
            for (x <- 1 until n) {
                if ( addedRuleMap.contains( x ) ) {
                    ret = ret ++ addedRuleMap( x )
                }
            }
            return ret
        }

        def isRuleDisplayable( rule: BasicRule ): Boolean = {
            val beforeGrid = rule.beforeGrid
            val afterGrid = rule.afterGrid
            if ( beforeGrid.compareTo( afterGrid ) == 0 ) return false // nothing gained at all
            val solvedGrid = beforeGrid.newChild
            try {
                BasicRule.applyRulesTo( solvedGrid, rulesOfLevelsUnder( rule.patternConstraintCount ) )
            } catch {
                case e: InvalidGridException => {
                    if ( rule.isValid ) {
                        println( "egad!" )
                        println( "rule.before: " + beforeGrid.serialString )
                        println( "rule.after: " + afterGrid.serialString )
                        println( "solved: " + solvedGrid.serialString )
                        throw new RuntimeException( "egad! invalid grid exception on a valid rule?", e )
                    }
                    return false // rule was invalid, and lower level rules determined this!
                }
            }
            if ( solvedGrid.compareTo( afterGrid ) == 0 ) return false // we can derive this using lower rules
            debug( "rule: " + rule.serialString )
            debug( "             solved: " + solvedGrid.serialString )
            return true
        }

        breakable {
            for (rule <- sortedRules) {
                if ( isRuleDisplayable( rule ) ) {
                    displayCount += 1
                    addRule( rule )
                }
                if ( displayCount >= 1000 ) break
            }
        }
    }
}

class RuleFrame extends JFrame {
    val panel: JPanel = new JPanel
    panel.setLayout( new BoxLayout( panel, BoxLayout.PAGE_AXIS ) )
    val pane: JScrollPane = new JScrollPane( panel )
    setContentPane( pane )

    setVisible( true );
    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE )

    def addRule( rule: BasicRule ): Unit = {
        panel.add( new RulePanel( rule ) )
        panel.invalidate()
        panel.revalidate()
    }

    setSize( 300, 400 )

}