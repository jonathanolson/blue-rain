package com.jonathanolson.slither.apps

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.solvers.SolutionFinder
import com.jonathanolson.slither.patterns.BasicRule
import com.jonathanolson.slither.view.RulePanel
import javax.swing.{JScrollPane, BoxLayout, SwingConstants, JPanel, JFrame}
import scala.collection.mutable.ListBuffer
import java.awt.{Dimension, FlowLayout}
import java.io.{FileWriter, File}
import com.jonathanolson.slither.model.{Vertex4, Edge4, Face4, VertexState, ColoredGrid4, GridConstraints}
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.LogHelper

object SpeedTest extends LogHelper {
    def main( args: Array[String] ) {
        var pattSet = Set.empty[String]

        val a = System.currentTimeMillis

        def processGrid( grid: ColoredGrid4 ): Unit = {
            val paddedGrid = BasicRule.canonicalPaddedGrid( grid ) // also canonical!
            val pattString = paddedGrid.serialString
            info( "patt: " + pattString )
            if ( pattSet.contains( pattString ) ) {
                info( "duplicate" )
            } else {
                val rule: BasicRule = ruleFrom( paddedGrid )
            }
            pattSet += pattString
        }

        // single edges on single faces
        for (faceval <- 0 to 3; state <- List( Red, Black )) {
            val grid = ColoredGrid4.empty( 3, 3, true )
            grid.value( grid.faces( 1 )( 1 ) ) = Some( faceval )
            grid.state( grid.hedges( 1 )( 1 ) ) = state
            processGrid( grid )
        }

        val b = System.currentTimeMillis

        println( (b - a) + " ms" )

    }

    def ruleFrom( grid: ColoredGrid4 ): BasicRule = { // TODO: get rid of duplicates?
        val constraints = new GridConstraints( grid.rows, grid.cols )
        constraints.init( grid )

        val a = System.currentTimeMillis

        for (completeGrid <- SolutionFinder.solutionsOf( grid )) {
            constraints.allow( completeGrid )
        }

        val b = System.currentTimeMillis

        info( (b - a) + " ms" )

        val rule = constraints.constructRule.canonical

        info( rule.serialString )

        return rule
    }

}