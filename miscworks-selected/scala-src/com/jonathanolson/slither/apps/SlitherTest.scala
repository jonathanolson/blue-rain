package com.jonathanolson.slither.apps

import javax.swing.JFrame
import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{ColoredGrid4, Edge4, VertexStateGrid4, Grid4}
import com.jonathanolson.slither.solvers.{SolverStack, LoopInvalidator, MediumFaceColorSolver, FaceColorEdgeSolver, SimpleFaceColorSolver, SimpleVertexStateSolver, StaticFaceVertexStateSolver, StaticFaceSolver, FaceVertexStateSolver, VertexStateSolver, SimpleVertexSolver, SimpleFaceSolver}
import com.jonathanolson.slither.view.{SlitherCanvas, SlitherNodeListener}

object SlitherTest {
    def main( args: Array[String] ) {

        /* TODO add modifiable to grid view (and possibly grid?)
        TODO: face states (coloring)
         */

        //new SlitherTest()

        //val grid = Grid4.fromString( "10x10 .1.3..0.3.3..3..3..33........3..0.33.2..0.2....2.22.0....3.2..2.03.3..3........23..0..3..3.3.1..3.2." );
        //val grid = Grid4.fromString( "10x10 !.e112d.2d3b4.4b.8700f.96378.c33b4.4b.1e3e1.96.783d2.69.0f3b43e1.0f.0f.2d.a5.0f.a5.0f.1e3e1.3c.c300f.963783d2.782c3.87.3c00f.b425a.e1.0f.a5.872a5.3c25a296.6900f.b4.5a.e1.2d3b4.5a25a.e1.0f296.6900f3b4.5a3e1.96.5a3b4.5a.69.0f.1e.69.87.a5.2d2963e1.1e.4b00f.96.5a3e1.3c.5a3e1.3c3d2.4b11e.e1.873b4.5a2c3.b4" )

        //        val gridString: String = "20x20 .3.13.1.2333.1..31002..322..2..0.1.23.1133121..31....22...23..2......121.21.0.1.1.22..2..1.1..1.1..322.1.1111212.1...213.....2.2.31.1113231.1.21.312..01...2..02.01.2.22.....111.3.3.10.23.1.31..1....0....3..1.1..1..1.1.1.10...31.2311112312121.3.....3..1...1......3.2....112..211..11....3.22..3.1...1322022.2111...2.1.12..22..2.2.1.2221.1223.12..22.102.3.11.1.22...13.31..2.132.1.3..2.12221....22...110"
        val gridString = "40x30:311a1a222a3a2a1b2a21a2b2a0a2a110a1a123a33c0c2c02a3a11a1a32c1c2c32d31b02123a3b1b32b3b1a12231b31c33b2a2a3a2a231212d122222a2a3a3a3b03a1d3c3a213a10d22a211a3c3d3b3a2a22a2b1c1a22b13a1c2b3a22a2a1b1a0b3a1b2a22c0122c21a2b1a2b0a3a3a221a20113a1b10f12b2a12132a221a1b1a2a2d21d2d2d23d1a2a2e12b2a2b1212213b3221213b2a1b23c2a232d2e23b23b32e2d222a3a222b2212a223d2b1d323a3212b122c2b1b12e2123323101e22b2b2b2a2a1112a13223a3h2a22222a2132a2a33c1b3c1b2a2b32b1a2b1c3b1c32c2b1c2b3a1b20b2a3b2c2b1c33a1a2111a12221a3h1a11322a2311a2a2b2b3b32e2222201113e31b1b1c222b3122a323d2b2d113a2123b122a3a112d1e12b12b12e3d013a3c11b2a1b1223122b2331323b1a1b11e2a2a2d30d3d2d32d1a3a2b2a231a13231a2b02f22b2a22133a323a2a2a1b2a1b2a23c2222c22a1b1a1b2a2b2a3a13a3b1c2a22b32a2c0b3a12a3a1b1d3c1a221a12d21a112a2c3d2a32b2a2a2a2a032323d323112a1a1a2a3b32c10b32223a2b1b22b2b2a23322b21d23c2c2c12a2a11a1a33c1c3c21a323a3a222a3a3a1b2a01a1b2a2a2a211a3a113"
        val (rows, cols) = Grid4.sizeFromString( gridString )
        val grid = ColoredGrid4.empty( rows, cols, false );
        Grid4.setState( grid, gridString )
        //        val grid = VertexStateGrid4.fromString( gridString );
        println( grid.simpleString )

        //        for (edge <- grid.edgeIterator) {
        //            grid.state( edge ) = EdgeState.White
        //        }

        new SlitherTest( grid )

        val stack = new SolverStack( grid )

        stack.solvers += StaticFaceSolver.attach( grid )
        stack.solvers += StaticFaceVertexStateSolver.attach( grid )

        stack.solvers += SimpleFaceSolver.attach( grid )
        stack.solvers += SimpleVertexSolver.attach( grid )
        stack.solvers += SimpleVertexStateSolver.attach( grid )
        stack.solvers += VertexStateSolver.attach( grid )
        stack.solvers += FaceVertexStateSolver.attach( grid )
        stack.solvers += SimpleFaceColorSolver.attach( grid )
        stack.solvers += FaceColorEdgeSolver.attach( grid )
        stack.solvers += MediumFaceColorSolver.attach( grid )
        if ( grid.openBorders ) {
            stack.solvers += LoopInvalidator.attach( grid )
        }
        //        (new StaticFaceSolver( grid )).run()
        //        (new StaticFaceVertexStateSolver( grid )).run()

        stack.solve()

        println( grid.displayString )

        println( grid.colorOf( grid.faces( 0 )( 1 ) ) )

    }
}

class SlitherTest(
        val grid: Grid4
        ) extends JFrame {
    val canvas = new SlitherCanvas
    canvas.setGrid( grid )
    add( canvas )

    canvas.addNodeListener( new SlitherNodeListener {
        override def onEdgeLeftClick( edge: Edge4 ) = {
            grid.state( edge ) = grid.state( edge ) match {
                case White => Black
                case Black => Red
                case Red => White
            }
        }

        override def onEdgeRightClick( edge: Edge4 ) = {
            grid.state( edge ) = grid.state( edge ) match {
                case White => Red
                case Black => White
                case Red => Black
            }
        }
    } )

    setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
    setVisible( true );

    pack()
}

