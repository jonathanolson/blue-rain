package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import scala.collection.mutable.ListBuffer
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.{LogHelper, Direction, Spot}
import com.jonathanolson.slither.solvers.SolutionFinder
import com.jonathanolson.slither.model.{GridConstraints, VertexStateGrid4, Grid4, VertexState, ColoredGrid4}

object BasicRule extends LogHelper {
    def deserialize( str: String ): BasicRule = {
        require( str.startsWith( "brule" ) )
        require( str.endsWith( "]" ) )

        val valid = !str.startsWith( "brulei" )

        val startLength: Int = if ( valid ) "brule[".length else "brulei[".length

        val fittedSplit = str.substring( startLength, str.length - 1 ).split( ";" )
        val beforeGrid = ColoredGrid4.fromString( fittedSplit( 0 ) + ";" )
        val afterGrid = if ( valid ) ColoredGrid4.fromString( fittedSplit( 1 ) + ";" ) else beforeGrid

        val rule = new BasicRule

        if ( !valid ) {
            rule.results.append( new InvalidPattern )
        }

        // face values
        for (face <- beforeGrid.faceIterator) {
            val afterFace = face.inGrid( afterGrid )
            if ( beforeGrid.value( face ).isDefined ) {
                rule.patterns.append( new FacePattern( face.row, face.col, beforeGrid.value( face ).get ) )
            } else if ( valid && afterGrid.value( afterFace ).isDefined ) {
                rule.results.append( new FacePattern( face.row, face.col, afterGrid.value( afterFace ).get ) )
            }
        }

        // edge states
        for (edge <- beforeGrid.edgeIterator) {
            val afterEdge = edge.inGrid( afterGrid )
            if ( beforeGrid.state( edge ) != White ) {
                rule.patterns.append( new EdgePattern( edge.row, edge.col, edge.isVertical, beforeGrid.state( edge ) ) )
            } else if ( valid && afterGrid.state( afterEdge ) != White ) {
                rule.results.append( new EdgePattern( edge.row, edge.col, edge.isVertical, afterGrid.state( afterEdge ) ) )
            }
        }

        // vertex states
        for (vertex <- beforeGrid.vertexIterator) {
            val afterVertex = vertex.inGrid( afterGrid )
            val beforeState = beforeGrid.vertexState( vertex )
            val afterState = afterGrid.vertexState( afterVertex )
            if ( beforeState != VertexState.All ) {
                rule.patterns.append( new VertexStatePattern( vertex.row, vertex.col, beforeState ) )
            }
            if ( valid && afterState != VertexState.All ) {
                rule.results.append( new VertexStatePattern( vertex.row, vertex.col, beforeState.onlyNewConstraints( afterState ) ) )
            }
        }

        // colors
        for (face1 <- beforeGrid.faceIterator; face2 <- beforeGrid.faceIterator; if face1.compareTo( face2 ) < 0) {
            val afterFace1 = face1.inGrid( afterGrid )
            val afterFace2 = face2.inGrid( afterGrid )
            if ( beforeGrid.isSameColor( beforeGrid.colorOf( face1 ), beforeGrid.colorOf( face2 ) ) ) {
                rule.patterns.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, true ) )
            } else if ( beforeGrid.isOppositeColor( beforeGrid.colorOf( face1 ), beforeGrid.colorOf( face2 ) ) ) {
                rule.patterns.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, false ) )
            } else if ( valid && afterGrid.isSameColor( afterGrid.colorOf( afterFace1 ), afterGrid.colorOf( afterFace2 ) ) ) {
                rule.results.append( new ColorPattern( afterFace1.row, afterFace1.col, afterFace2.row, afterFace2.col, true ) )
            } else if ( valid && afterGrid.isOppositeColor( afterGrid.colorOf( afterFace1 ), afterGrid.colorOf( afterFace2 ) ) ) {
                rule.results.append( new ColorPattern( afterFace1.row, afterFace1.col, afterFace2.row, afterFace2.col, false ) )
            }
        }

        return rule
    }

    def canonicalPatternString( grid: Grid4 ): String = patternFromGrid( grid ).canonical.patternGrid.serialString

    def canonicalPaddedGrid( grid: Grid4 ): ColoredGrid4 = patternFromGrid( grid ).canonical.paddedPatternGrid

    def patternFromGrid( grid: Grid4 ): BasicRule = { // returns pattern only!
        val rule = new BasicRule

        // face values
        for (face <- grid.faceIterator) {
            if ( grid.value( face ).isDefined ) {
                rule.patterns.append( new FacePattern( face.row, face.col, grid.value( face ).get ) )
            }
        }

        // edge states
        for (edge <- grid.edgeIterator) {
            if ( grid.state( edge ) != White ) {
                rule.patterns.append( new EdgePattern( edge.row, edge.col, edge.isVertical, grid.state( edge ) ) )
            }
        }

        // vertex states
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            val g = grid.asInstanceOf[VertexStateGrid4]
            for (vertex <- g.vertexIterator) {
                if ( g.vertexState( vertex ) != VertexState.All ) {
                    rule.patterns.append( new VertexStatePattern( vertex.row, vertex.col, g.vertexState( vertex ) ) )
                }
            }
        }

        // colors
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            val g = grid.asInstanceOf[ColoredGrid4]
            for (face1 <- g.faceIterator; face2 <- g.faceIterator; if face1.compareTo( face2 ) < 0) {
                if ( g.isSameColor( g.colorOf( face1 ), g.colorOf( face2 ) ) ) {
                    rule.patterns.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, true ) )
                } else if ( g.isOppositeColor( g.colorOf( face1 ), g.colorOf( face2 ) ) ) {
                    rule.patterns.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, false ) )
                }
            }
        }

        return rule
    }

    def ruleFrom( grid: ColoredGrid4 ): BasicRule = {
        if ( !grid.openBorders ) {
            logger.warn( "ruleFrom with grid with openBorders = false" )
        }
        val constraints = new GridConstraints( grid.rows, grid.cols )
        constraints.init( grid )

        val a = System.currentTimeMillis
        for (completeGrid <- SolutionFinder.solutionsOf( grid )) {
            constraints.allow( completeGrid )
        }
        val b = System.currentTimeMillis

        info( (b - a) + " ms" )
        val rule = constraints.constructRule.canonical
        info( rule.serialString )

        return rule
    }

    def applyRulesTo( grid: Grid4, rules: Traversable[BasicRule] ): Unit = {
        debug( "applyRulesTo grid " + grid.serialString )
        var count = grid.moves.size
        var finished = false
        while (!finished) {
            for (rule <- rules) {
                val a = grid.moves.size
                var hadException: Option[InvalidGridException] = None
                try {
                    rule.applyTo( grid )
                } catch {
                    case e: InvalidGridException => hadException = Some( e )
                }
                if ( grid.moves.size > a ) {
                    debug( "used rule " + rule.serialString )
                    if ( hadException.isDefined ) {
                        debug( "     invalid grid exception" )
                    } else {
                        debug( "     grid of " + grid.serialString )
                    }
                }
                if ( hadException.isDefined ) throw hadException.get
            }
            if ( count == grid.moves.size ) {
                // if no moves were added
                finished = true
            }
            count = grid.moves.size
        }
        debug( "finished applyRulesTo" )
    }
}

class BasicRule extends LogHelper {
    // TODO: refactor out into other functions? then can be made more 'immutable-like'
    val patterns = new ListBuffer[GridPattern]
    val results = new ListBuffer[GridPattern]

    /*---------------------------------------------------------------------------*
    * construction
    *----------------------------------------------------------------------------*/

    if ( !isValid ) results.append( new InvalidPattern )


    def isEmpty: Boolean = patterns.isEmpty && results.isEmpty

    def matches( spot: Spot, reversed: Boolean ): Boolean = {
        patterns.forall( _.check( spot, reversed ) )
    }

    def apply( spot: Spot, reversed: Boolean ): Unit = {
        try {
            results.foreach( _.apply( spot, reversed ) )
        } catch {
            case e: InvalidGridException => {
                if ( isValid ) {
                    logger.warn( "problem applying rule " + this.serialString )
                }
                throw e
            }
        }
    }

    def applyTo( grid: Grid4 ): Unit = {
        for (
            vertex <- grid.vertexIterator;
            dir <- Direction.primaries;
            spot = new Spot( grid, vertex, dir );
            reversed <- List( true, false )
        ) {
            val isMatch: Boolean = matches( spot, reversed )
            val beforeCounts = spot.grid.moves.size
            if ( isMatch ) {
                debug( "matches " + spot + ", reversed: " + reversed + ", applying" )
                //                debug( "before apply: " + grid.serialString )
                apply( spot, reversed )
                //                debug( "after apply: " + grid.serialString )
            }
            if ( beforeCounts < spot.grid.moves.size ) {
                // we changed it, debug!
                debug( "changed with " + spot + ", reversed: " + reversed )
                debug( "        grid: " + grid.serialString )
            }
        }
    }

    def isValid: Boolean = results.forall( (gp) => !gp.isInstanceOf[InvalidPattern] )

    override def toString = "patterns:\n" + patterns.mkString( "\n" ) + "\nresults\n" + results.mkString( "\n" );

    def patternBounds: PatternBounds = {
        if ( patterns.isEmpty ) {
            new PatternBounds( 0, 0, 0, 0 )
        } else {
            patterns.foldLeft( patterns( 0 ).bounds )( (bounds, pattern) => bounds + pattern.bounds )
        }
    }

    def completeBounds: PatternBounds = {
        if ( results.isEmpty ) {
            patternBounds
        } else {
            patternBounds + results.foldLeft( results( 0 ).bounds )( (bounds, pattern) => bounds + pattern.bounds )
        }
    }

    def map( f: (GridPattern) => GridPattern ): BasicRule = {
        val newRule = new BasicRule
        for (pattern <- patterns) {
            newRule.patterns.append( f( pattern ) )
        }
        for (result <- results) {
            newRule.results.append( f( result ) )
        }
        return newRule
    }

    def shifted( rowShift: Int, colShift: Int ): BasicRule = map( (gridPattern) => gridPattern.shift( rowShift, colShift ) )

    def rotated( dir: Direction ): BasicRule = map( (gridPattern) => gridPattern.rotate( dir ) )

    def flipped: BasicRule = map( (gridPattern) => gridPattern.flip )

    def toCompleteOrigin: BasicRule = { // returns a copy shifted so that the rules and patterns have the smallest positive coordinates
        val bounds = completeBounds
        return shifted( -bounds.minRow, -bounds.minCol )
    }

    def toPatternOrigin: BasicRule = { // returns a copy shifted so that only the rules have the smallest positive coordinates
        val bounds = patternBounds
        return shifted( -bounds.minRow, -bounds.minCol )
    }

    def patternGrid: ColoredGrid4 = { // fitted to pattern, NOT to complete bounds
        val rule = toPatternOrigin
        var bounds = rule.patternBounds
        val grid = ColoredGrid4.empty( bounds.rows, bounds.cols, true )
        val spot = new Spot( grid, grid.vertices( 0 )( 0 ), East )
        rule.patterns.foreach( _.apply( spot, false ) )
        return grid
    }

    def paddedPatternGrid: ColoredGrid4 = { // fitted to 1 + pattern, NOT to complete bounds
        val rule = toPatternOrigin
        var bounds = rule.patternBounds
        val grid = ColoredGrid4.empty( bounds.rows + 2, bounds.cols + 2, true )
        val spot = new Spot( grid, grid.vertices( 1 )( 1 ), East )
        rule.patterns.foreach( _.apply( spot, false ) )
        return grid
    }

    def beforeGrid: ColoredGrid4 = { // fitted to complete, just pattern
        val rule = toCompleteOrigin
        val bounds = rule.completeBounds
        val grid = ColoredGrid4.empty( bounds.rows, bounds.cols, true )
        val spot = new Spot( grid, grid.vertices( 0 )( 0 ), East )
        rule.patterns.foreach( _.apply( spot, false ) )
        return grid
    }

    def afterGrid: ColoredGrid4 = { // fitted to complete, pattern and result
        val rule = toCompleteOrigin
        val bounds = rule.completeBounds
        val grid = ColoredGrid4.empty( bounds.rows, bounds.cols, true )
        val spot = new Spot( grid, grid.vertices( 0 )( 0 ), East )
        rule.patterns.foreach( _.apply( spot, false ) )
        try {
            if ( isValid ) {
                rule.results.foreach( _.apply( spot, false ) )
            }
        } catch {
            case e: InvalidGridException => () // ignore these
        }
        return grid
    }

    def canonical: BasicRule = {
        val rules = List(
            this.toCompleteOrigin,
            this.rotated( East ).toCompleteOrigin,
            this.rotated( South ).toCompleteOrigin,
            this.rotated( West ).toCompleteOrigin,
            this.flipped.toCompleteOrigin,
            this.flipped.rotated( East ).toCompleteOrigin,
            this.flipped.rotated( South ).toCompleteOrigin,
            this.flipped.rotated( West ).toCompleteOrigin
            )
        val withRules: List[(BasicRule, ColoredGrid4)] = rules.map( (rule) => (rule, rule.patternGrid) )
        def comparison( a: (BasicRule, ColoredGrid4), b: (BasicRule, ColoredGrid4) ): Boolean = {
            a._2.compareTo( b._2 ) < 0
        }
        val sorted = withRules.sortWith( comparison )
        return sorted.head._1
    }

    def serialString: String = {
        val str = new StringBuilder
        str.append( "brule" + (if ( isValid ) "" else "i") + "[" )

        val canon = canonical

        str.append( canon.beforeGrid.serialString )
        str.append( canon.afterGrid.serialString )

        str.append( "]" )
        return str.toString
    }

    def patternConstraintCount: Int = {
        var count = 0
        var faceSet = Set.empty[(Int, Int)]
        for (rule <- patterns) {
            rule match {
                case r: EdgePattern => count += 1
                case r: VertexStatePattern => count += r.state.constraintCount
                case r: FacePattern => count += 1
                case r: ColorPattern => {
                    faceSet += ((r.row1, r.col1))
                    faceSet += ((r.row2, r.col2))
                }
            }
        }
        count += faceSet.size
        return count
    }

    def patternCompareTo( other: BasicRule ): Int = {
        val ret = this.patternConstraintCount.compare( other.patternConstraintCount )
        if ( ret != 0 ) return ret
        val myGrid = this.canonical.paddedPatternGrid
        val otherGrid = other.canonical.paddedPatternGrid
        return myGrid.compareTo( otherGrid )
    }

}