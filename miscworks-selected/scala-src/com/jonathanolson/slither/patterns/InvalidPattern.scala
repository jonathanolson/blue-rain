package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.model.ColoredGrid4
import com.jonathanolson.slither.util.{Direction, Spot}
import com.jonathanolson.slither.exceptions.InvalidGridException

case class InvalidPattern extends GridPattern with GridPatternLike[InvalidPattern] {
    def bounds = new PatternBounds( 0, 0, 1, 1, false ) // not really defined! addition will ignore this

    def check( spot: Spot, reversed: Boolean ) = false

    def rotate( dir: Direction ) = this

    def flip = this

    def shift( rowShift: Int, colShift: Int ) = this

    // throw grid exception upon applying
    override def apply( spot: Spot, reversed: Boolean ) = throw new InvalidGridException
}