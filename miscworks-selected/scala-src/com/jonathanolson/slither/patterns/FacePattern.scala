package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.{LogHelper, Direction, Spot}

case class FacePattern( row: Int, col: Int, value: Int ) extends GridPattern with GridPatternLike[FacePattern] with LogHelper {
    def check( spot: Spot, reversed: Boolean ): Boolean = {
        debug( "check " + spot + ", " + reversed )
        val ret = findFace( spot, reversed ) match {
            case Some( face ) => {
                debug( "face: " + face + ", " + spot.grid.value( face ) )
                spot.grid.value( face ) == Some( value )
            }
            case None => false
        }
        debug( "ret: " + ret )
        return ret
    }

    def apply( spot: Spot, reversed: Boolean ): Unit = {
        debug( "before apply: " + spot.grid.serialString )
        findFace( spot, reversed ) match {
            case Some( face ) => spot.grid.value( face ) = Some( value )
            case None => ()
        }
        debug( "after apply: " + spot.grid.serialString )
    }

    def bounds = new PatternBounds( row, col, row + 1, col + 1 )

    def shift( rowShift: Int, colShift: Int ): FacePattern = new FacePattern( row + rowShift, col + colShift, value )

    def rotate( dir: Direction ): FacePattern = {
        val (newRow, newCol) = GridLocations.rotateFace( row, col, dir )
        new FacePattern( newRow, newCol, value )
    }

    def flip: FacePattern = {
        val (newRow, newCol) = GridLocations.flipFace( row, col )
        new FacePattern( newRow, newCol, value )
    }

    def findFace( spot: Spot, reversed: Boolean ) = GridLocations.findFace( row, col, spot, reversed )

    override def toString = "Face " + row + "," + col + " v:" + value
}











