package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{Vertex4, VertexStateGrid4, VertexState}
import com.jonathanolson.slither.util.{Direction, Spot}

case class VertexStatePattern( row: Int, col: Int, state: VertexState ) extends GridPattern with GridPatternLike[VertexStatePattern] {
    def check( spot: Spot, reversed: Boolean ): Boolean = {
        findVertex( spot, reversed ) match {
            case Some( vertex ) => {
                val gridState: VertexState = spot.grid.asInstanceOf[VertexStateGrid4].vertexState( vertex )

                val transformedState = transformState( state, spot.dir, reversed )

                // return "passing" the constraint if by &-ing out all values returns the same state
                (gridState & transformedState) == gridState
            }
            case None => false
        }
    }

    def apply( spot: Spot, reversed: Boolean ): Unit = {
        findVertex( spot, reversed ) match {
            case Some( vertex ) => {
                val grid: VertexStateGrid4 = spot.grid.asInstanceOf[VertexStateGrid4]
                val transformedState = transformState( state, spot.dir, reversed )
                grid.vertexState( vertex ) = grid.vertexState( vertex ) & transformedState
            }
            case None => ()
        }
    }

    def transformState( vertexState: VertexState, dir: Direction, reversed: Boolean ): VertexState = {
        var transformedState = vertexState
        if ( reversed ) {
            // if necessary, flip before doing any rotation
            transformedState = transformedState.flipped
        }
        // then rotate, where east is stable
        transformedState = transformedState.rotated( dir match {
            case East => North
            case South => East
            case West => South
            case North => West
        } )
        return transformedState
    }

    def bounds = new PatternBounds( row, col, row, col )

    def findVertex( spot: Spot, reversed: Boolean ): Option[Vertex4] = GridLocations.findVertex( row, col, spot, reversed )

    def shift( rowShift: Int, colShift: Int ): VertexStatePattern = new VertexStatePattern( row + rowShift, col + colShift, state )

    def rotate( dir: Direction ) = {
        val (newRow, newCol) = GridLocations.rotateVertex( row, col, dir )
        new VertexStatePattern( newRow, newCol, state.rotated( dir ) )
    }

    def flip = new VertexStatePattern( -row, col, state.flipped )

    override def toString = "Vertex " + row + "," + col + " s:" + state.doesNotAllowString
}