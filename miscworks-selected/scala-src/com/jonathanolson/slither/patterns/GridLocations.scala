package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{Vertex4, Edge4, Face4}
import com.jonathanolson.slither.util.{Direction, Spot}

object GridLocations {
    def rotateVertex( row: Int, col: Int, dir: Direction ): (Int, Int) = { // coordinates of a rotated vertex
        require( dir.isPrimary )
        dir match {
            case North => (row, col)
            case East => (col, -row)
            case South => (-row, -col)
            case West => (-col, row)
        }
    }

    def flipVertex( row: Int, col: Int ): (Int, Int) = (-row, col) // coordinates of a flipped vertex

    def rotateFace( row: Int, col: Int, dir: Direction ): (Int, Int) = { // coordinates of a rotated face
        require( dir.isPrimary )
        dir match {
            case North => (row, col)
            case East => rotateVertex( row + 1, col, dir )
            case South => rotateVertex( row + 1, col + 1, dir )
            case West => rotateVertex( row, col + 1, dir )
        }
    }

    def flipFace( row: Int, col: Int ): (Int, Int) = flipVertex( row + 1, col ) // coordinates of a flipped face

    def rotateEdge( row: Int, col: Int, vertical: Boolean, dir: Direction ): (Int, Int) = {
        // get the rotated locations of both of the edge vertices
        val (vaRow, vaCol) = GridLocations.rotateVertex( row, col, dir )
        val (vbRow, vbCol) = vertical match {
            case true => GridLocations.rotateVertex( row + 1, col, dir )
            case false => GridLocations.rotateVertex( row, col + 1, dir )
        }

        // find the minimums
        return (math.min( vaRow, vbRow ), math.min( vaCol, vbCol ))
    }

    def flipEdge( row: Int, col: Int, vertical: Boolean, dir: Direction ): (Int, Int) = {
        vertical match {
            case false => (-row, col)
            case true => (-row - 1, col)
        }
    }

    def findEdge( row: Int, col: Int, vertical: Boolean, spot: Spot, reversed: Boolean ): Option[Edge4] = {
        var cur = spot
        val rowNeg = row < 0
        val colNeg = col < 0
        val colDir = if ( colNeg ) South else North
        var rowDir = if ( reversed ) West else East
        if ( rowNeg ) rowDir = rowDir.opposite

        for (i <- 0 until math.abs( col )) {
            cur.getVertex( colDir ) match {
                case Some( vertex ) => cur = if ( colNeg ) cur.shiftBack else cur.forward
                case None => return None
            }
        }
        for (i <- 0 until math.abs( row )) {
            cur.getVertex( rowDir ) match {
            // xor on the dirs
                case Some( vertex ) => cur = if ( (reversed || rowNeg) && !(reversed && rowNeg) ) cur.shiftLeft else cur.shiftRight
                case None => return None
            }
        }
        if ( vertical ) {
            return cur.getEdge( if ( reversed ) West else East )
        } else {
            return cur.getEdge( North )
        }
    }

    def findFace( row: Int, col: Int, spot: Spot, reversed: Boolean ): Option[Face4] = {
        var cur = spot
        val rowNeg = row < 0
        val colNeg = col < 0
        val colDir = if ( colNeg ) South else North
        var rowDir = if ( reversed ) West else East
        if ( rowNeg ) rowDir = rowDir.opposite

        for (i <- 0 until math.abs( col )) {
            cur.getVertex( colDir ) match {
                case Some( vertex ) => cur = if ( colNeg ) cur.shiftBack else cur.forward
                case None => return None
            }
        }
        for (i <- 0 until math.abs( row )) {
            cur.getVertex( rowDir ) match {
                case Some( vertex ) => cur = if ( (reversed || rowNeg) && !(reversed && rowNeg) ) cur.shiftLeft else cur.shiftRight
                case None => return None
            }
        }
        cur.getFace( if ( reversed ) Northwest else Northeast ) match {
            case Some( face ) => return Some( face )
            case None => return None
        }
    }

    def findVertex( row: Int, col: Int, spot: Spot, reversed: Boolean ): Option[Vertex4] = {
        var cur = spot
        val rowNeg = row < 0
        val colNeg = col < 0
        val colDir = if ( colNeg ) South else North // direction to move horizontally
        var rowDir = if ( reversed ) West else East // direction to move vertically
        if ( rowNeg ) rowDir = rowDir.opposite

        for (i <- 0 until math.abs( col )) {
            cur.getVertex( colDir ) match {
                case Some( vertex ) => cur = if ( colNeg ) cur.shiftBack else cur.forward
                case None => return None
            }
        }
        for (i <- 0 until math.abs( row )) {
            cur.getVertex( rowDir ) match {
            // xor on the dirs
                case Some( vertex ) => cur = if ( (reversed || rowNeg) && !(reversed && rowNeg) ) cur.shiftLeft else cur.shiftRight
                case None => return None
            }
        }
        return Some( cur.vertex )
    }
}