package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.Edge4
import com.jonathanolson.slither.util.{Direction, Spot}

case class EdgePattern( row: Int, col: Int, vertical: Boolean, state: EdgeState ) extends GridPattern with GridPatternLike[EdgePattern] {
    def check( spot: Spot, reversed: Boolean ): Boolean = {
        findEdge( spot, reversed ) match {
            case Some( edge ) => spot.grid.state( edge ) == state
            case None => false
        }
    }

    def apply( spot: Spot, reversed: Boolean ): Unit = {
        spot.grid.move( findEdge( spot, reversed ) -> state )
    }

    def bounds = {
        vertical match {
            case true => new PatternBounds( row, col, row + 1, col ) // vertical
            case false => new PatternBounds( row, col, row, col + 1 ) // horizontal
        }
    }

    def shift( rowShift: Int, colShift: Int ): EdgePattern = new EdgePattern( row + rowShift, col + colShift, vertical, state )

    def rotate( dir: Direction ) = {
        require( dir.isPrimary )
        val (newRow, newCol) = GridLocations.rotateEdge( row, col, vertical, dir )

        // reverse vertical if needed
        dir match {
            case North => new EdgePattern( newRow, newCol, vertical, state )
            case East => new EdgePattern( newRow, newCol, !vertical, state )
            case South => new EdgePattern( newRow, newCol, vertical, state )
            case West => new EdgePattern( newRow, newCol, !vertical, state )
        }
    }

    def flip = vertical match {
        case false => new EdgePattern( -row, col, vertical, state )
        case true => new EdgePattern( -row - 1, col, vertical, state )
    }

    def findEdge( spot: Spot, reversed: Boolean ): Option[Edge4] = GridLocations.findEdge( row, col, vertical, spot, reversed )

    override def toString = "Edge " + row + "," + col + " v:" + vertical + " s:" + state
}









