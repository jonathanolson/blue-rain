package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.util.{Direction, Spot}

// TODO: test shifts, flips, rotations! (automated if possible)

trait GridPattern extends GridPatternLike[GridPattern] {
    def check( spot: Spot, reversed: Boolean ): Boolean // return true if it matches
    def apply( spot: Spot, reversed: Boolean ): Unit // apply it to the grid
    def bounds: PatternBounds // how far the pattern extends
}

trait GridPatternLike[+T] {
    def shift( rowShift: Int, colShift: Int ): T // translate essentially
    def flip: T // flip along row 0
    def rotate( dir: Direction ): T // rotate with 'north' being the identity
}









