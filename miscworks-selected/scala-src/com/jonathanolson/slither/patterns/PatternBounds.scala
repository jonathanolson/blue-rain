package com.jonathanolson.slither.patterns

case class PatternBounds(
        minRow: Int, // all of these are bounded by the outside 'vertices'
        minCol: Int,
        maxRow: Int,
        maxCol: Int,
        isDefined: Boolean = true
        ) {
    def +( other: PatternBounds ): PatternBounds = {
        if ( !isDefined ) return other
        if ( !other.isDefined ) return this
        new PatternBounds(
            math.min( minRow, other.minRow ),
            math.min( minCol, other.minCol ),
            math.max( maxRow, other.maxRow ),
            math.max( maxCol, other.maxCol )
            )
    }

    def rows = maxRow - minRow // number of rows

    def cols = maxCol - minCol // number of columns
}