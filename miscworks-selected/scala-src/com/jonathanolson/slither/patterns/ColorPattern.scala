package com.jonathanolson.slither.patterns

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.ColoredGrid4
import com.jonathanolson.slither.util.{Direction, Spot}

case class ColorPattern( row1: Int, col1: Int, row2: Int, col2: Int, same: Boolean ) extends GridPattern with GridPatternLike[ColorPattern] {
    def check( spot: Spot, reversed: Boolean ): Boolean = {
        val faceOpt1 = GridLocations.findFace( row1, col1, spot, reversed )
        val faceOpt2 = GridLocations.findFace( row2, col2, spot, reversed )
        val grid: ColoredGrid4 = spot.grid.asInstanceOf[ColoredGrid4];
        (faceOpt1, faceOpt2) match {
            case (Some( face1 ), Some( face2 )) => {
                if ( same ) {
                    grid.isSameColor( grid.colorOf( face1 ), grid.colorOf( face2 ) )
                } else {
                    grid.isOppositeColor( grid.colorOf( face1 ), grid.colorOf( face2 ) )
                }
            }
            case _ => false
        }
    }

    def apply( spot: Spot, reversed: Boolean ): Unit = {
        val faceOpt1 = GridLocations.findFace( row1, col1, spot, reversed )
        val faceOpt2 = GridLocations.findFace( row2, col2, spot, reversed )
        val grid: ColoredGrid4 = spot.grid.asInstanceOf[ColoredGrid4];
        (faceOpt1, faceOpt2) match {
            case (Some( face1 ), Some( face2 )) => {
                if ( same ) {
                    grid.setColorsSame( grid.colorOf( face1 ), grid.colorOf( face2 ) )
                } else {
                    grid.setColorsOpposite( grid.colorOf( face1 ), grid.colorOf( face2 ) )
                }
            }
            case _ => ()
        }
    }

    def bounds = new PatternBounds( row1, col1, row1 + 1, col1 + 1 ) + new PatternBounds( row2, col2, row2 + 1, col2 + 1 )

    def shift( rowShift: Int, colShift: Int ): ColorPattern = new ColorPattern( row1 + rowShift, col1 + colShift, row2 + rowShift, col2 + colShift, same )

    def rotate( dir: Direction ) = {
        val (nRow1, nCol1) = GridLocations.rotateFace( row1, col1, dir )
        val (nRow2, nCol2) = GridLocations.rotateFace( row2, col2, dir )
        new ColorPattern( nRow1, nCol1, nRow2, nCol2, same )
    }

    def flip = {
        val (nRow1, nCol1) = GridLocations.flipFace( row1, col1 )
        val (nRow2, nCol2) = GridLocations.flipFace( row2, col2 )
        new ColorPattern( nRow1, nCol1, nRow2, nCol2, same )
    }

    override def toString = "Color " + row1 + "," + col1 + " " + row2 + "," + col2 + ", same:" + same
}