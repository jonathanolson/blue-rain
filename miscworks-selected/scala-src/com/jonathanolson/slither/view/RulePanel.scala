package com.jonathanolson.slither.view

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.patterns.BasicRule
import com.jonathanolson.slither.model.{ColoredGrid4, VertexStateGrid4}
import javax.swing.{JTextField, SwingConstants, JSeparator, JPanel}
import java.awt.{Dimension, Color, GridBagConstraints, GridBagLayout}
import com.jonathanolson.slither.solvers.Solver

class RulePanel(
        val rule: BasicRule
        ) extends JPanel( new GridBagLayout ) {
    val beforeCanvas = new SlitherCanvas
    val afterCanvas = new SlitherCanvas
    val serialText: JTextField = new JTextField( rule.serialString )

    val beforeGrid: ColoredGrid4 = rule.beforeGrid
    val afterGrid: ColoredGrid4 = rule.afterGrid
    val solvedGrid = ColoredGrid4.emptyFrom( beforeGrid )

    val stack = Solver.standardSolverStack( solvedGrid )
    beforeGrid.copyStateTo( solvedGrid )
    stack.solve()

    beforeCanvas.setGrid( beforeGrid )
    afterCanvas.setGrid( afterGrid )

    beforeCanvas.slitherNode.setVertexStateRed( true )
    afterCanvas.slitherNode.setVertexStateRed( true )

    serialText.setPreferredSize( new Dimension( 200, 20 ) )

    add( new JSeparator( SwingConstants.VERTICAL ), {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c; } )
    add( beforeCanvas, {val c = new GridBagConstraints(); c.gridx = 1; c.gridy = 0; c; } )
    add( new JSeparator( SwingConstants.VERTICAL ), {val c = new GridBagConstraints(); c.gridx = 2; c.gridy = 0; c; } )
    add( new JSeparator( SwingConstants.HORIZONTAL ), {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 1; c.gridwidth = 4; c; } )
    add( serialText, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 2; c.gridwidth = 4; c; } )
    add( new JSeparator( SwingConstants.HORIZONTAL ), {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 3; c.gridwidth = 4; c; } )
    if ( rule.isValid ) {
        add( afterCanvas, {val c = new GridBagConstraints(); c.gridx = 3; c.gridy = 0; c; } )
        if ( afterGrid.compareTo( solvedGrid ) != 0 ) {
            val solvedCanvas = new SlitherCanvas
            solvedCanvas.setGrid( solvedGrid )
            solvedCanvas.slitherNode.setVertexStateRed( true )
            // not fully solved
            solvedCanvas.setBackground( new Color( 255, 255, 200 ) )
            add( new JSeparator( SwingConstants.VERTICAL ), {val c = new GridBagConstraints(); c.gridx = 4; c.gridy = 0; c; } )
            add( solvedCanvas, {val c = new GridBagConstraints(); c.gridx = 5; c.gridy = 0; c; } )
        } else {

        }
    } else {
        beforeCanvas.setBackground( new Color( 255, 200, 200 ) )
    }
}