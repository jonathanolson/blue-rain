package com.jonathanolson.slither.view

import edu.umd.cs.piccolo.PNode
import com.jonathanolson.slither.SlitherInclude._
import edu.umd.cs.piccolo.nodes.PText
import java.awt.Color
import edu.umd.cs.piccolo.event.{PBasicInputEventHandler, PInputEvent}
import com.jonathanolson.slither.model.{ColoredGrid4, Face4, Grid4}

class FaceNode(
        val grid: Grid4,
        val face: Face4
        ) extends PNode {
    private var canShowColors: Boolean = true

    private var highlighted: Boolean = false
    private var highlightedColor: Color = Color.BLACK

    val backgroundNode: PNode = new PNode();
    addChild( backgroundNode )

    val textNode: PText = new PText()
    addChild( textNode )

    val (x, y) = SlitherNode.mapFacePositionCenter( face )
    translate( x, y )

    update()

    def update() {
        backgroundNode.setBounds( -SlitherNode.Scale / 2, -SlitherNode.Scale / 2, SlitherNode.Scale, SlitherNode.Scale )

        var backgroundColor: Color = Color.WHITE

        if ( grid.isInstanceOf[ColoredGrid4] ) {
            val cgrid = grid.asInstanceOf[ColoredGrid4]
            val color = cgrid.colorOf( face )
            if ( color.isAbsolute ) {
                color match {
                    case Outside => backgroundColor = new Color( 240, 255, 240 )
                    case Inside => backgroundColor = new Color( 255, 240, 255 )
                }
            } else if ( highlighted ) {
                backgroundColor = highlightedColor
            } else {
                if ( cgrid.facesWithColor( color ).size > 1 || cgrid.getOppositeColor( color ).isDefined ) {
                    // a possibly important face with some outside connection
                    backgroundColor = new Color( 245, 245, 245 )
                }
            }
        }

        backgroundNode.setPaint( backgroundColor )

        textNode.setText( grid.value( face ) match {
            case None => " "
            case Some( x ) => x.toString
        } )
        textNode.setOffset( -textNode.getWidth / 2, -textNode.getHeight / 2 )
    }

    def setColorsEnabled( enabled: Boolean ): Unit = {
        if ( canShowColors == enabled ) return
        canShowColors = enabled
        backgroundNode.setVisible( canShowColors )
        update()
    }

    def highlight( opposite: Boolean ) {
        highlighted = true
        highlightedColor = if ( opposite ) new Color( 255, 255, 180 ) else new Color( 220, 220, 255 )
        update()
    }

    def unHighlight() {
        highlighted = false
        update()
    }

}