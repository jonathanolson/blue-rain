package com.jonathanolson.slither.view

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.Grid4
import javax.swing.JFrame

class SimpleSlitherFrame(
        val grid: Grid4
        ) extends JFrame {
    val canvas: SlitherCanvas = new SlitherCanvas
    canvas.setGrid( grid )
    add( canvas )

    setVisible( true );

    pack()

    def setGrid( grid: Grid4 ): Unit = {
        canvas.setGrid( grid )
        pack()
    }
}