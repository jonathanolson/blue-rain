package com.jonathanolson.slither.view

import com.jonathanolson.slither.model.Grid4
import javax.swing.JFrame
import com.jonathanolson.slither.patterns.BasicRule

class SimpleRuleFrame(
        val rule: BasicRule
        ) extends JFrame {
    add( new RulePanel( rule ) )

    setVisible( true );

    pack()
}