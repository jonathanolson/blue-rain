package com.jonathanolson.slither.view

import com.jonathanolson.slither.SlitherInclude._
import edu.umd.cs.piccolo.PNode
import java.awt.Color
import com.jonathanolson.slither.view.SlitherNode.{Scale, LineWidth}
import com.jonathanolson.slither.model.{VertexStateGrid4, Vertex4, Grid4}
import edu.umd.cs.piccolo.nodes.PPath

class VertexNode(
        val grid: Grid4,
        val vertex: Vertex4
        ) extends PNode {
    val (x, y) = SlitherNode.mapVertexPosition( vertex )

    var showRed = false
    var showBlue = false

    translate( x, y )

    private val vertexNode = new PNode()
    vertexNode.setBounds( -LineWidth / 2, -LineWidth / 2, LineWidth, LineWidth )
    vertexNode.setPaint( Color.BLACK )
    addChild( vertexNode )

    private val northwestRed = createBounce( Color.RED )
    private val northwestBlue = createBounce( Color.BLUE )
    private val northeastRed = createBounce( Color.RED )
    private val northeastBlue = createBounce( Color.BLUE )
    private val southeastRed = createBounce( Color.RED )
    private val southeastBlue = createBounce( Color.BLUE )
    private val southwestRed = createBounce( Color.RED )
    private val southwestBlue = createBounce( Color.BLUE )
    northwestRed.rotate( math.Pi )
    northwestBlue.rotate( math.Pi )
    southwestRed.rotate( math.Pi / 2 )
    southwestBlue.rotate( math.Pi / 2 )
    northeastRed.rotate( -math.Pi / 2 )
    northeastBlue.rotate( -math.Pi / 2 )
    addChild( northwestRed )
    addChild( northwestBlue )
    addChild( northeastRed )
    addChild( northeastBlue )
    addChild( southeastRed )
    addChild( southeastBlue )
    addChild( southwestRed )
    addChild( southwestBlue )
    private val verticalRed = createStraight( Color.RED )
    private val verticalBlue = createStraight( Color.BLUE )
    private val horizontalRed = createStraight( Color.RED )
    private val horizontalBlue = createStraight( Color.BLUE )
    horizontalRed.rotate( math.Pi / 2 )
    horizontalBlue.rotate( math.Pi / 2 )
    addChild( verticalRed )
    addChild( verticalBlue )
    addChild( horizontalRed )
    addChild( horizontalBlue )

    private val emptyRed = createEmpty( Color.RED )
    private val emptyBlue = createEmpty( Color.BLUE )
    addChild( emptyRed )
    addChild( emptyBlue )

    northwestRed.setVisible( false )
    northwestBlue.setVisible( false )
    northeastRed.setVisible( false )
    northeastBlue.setVisible( false )
    southeastRed.setVisible( false )
    southeastBlue.setVisible( false )
    southwestRed.setVisible( false )
    southwestBlue.setVisible( false )
    verticalRed.setVisible( false )
    verticalBlue.setVisible( false )
    horizontalRed.setVisible( false )
    horizontalBlue.setVisible( false )
    emptyRed.setVisible( false )
    emptyBlue.setVisible( false )


    update()

    def update(): Unit = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            val eastWhite = grid.getState( vertex.eastEdge ) == White
            val westWhite = grid.getState( vertex.westEdge ) == White
            val northWhite = grid.getState( vertex.northEdge ) == White
            val southWhite = grid.getState( vertex.southEdge ) == White
            val state = grid.asInstanceOf[VertexStateGrid4].vertexState( vertex )
            northwestRed.setVisible( showRed && !state.allowNorthwest )
            northwestBlue.setVisible( showBlue && state.allowNorthwest )
            northeastRed.setVisible( showRed && !state.allowNortheast )
            northeastBlue.setVisible( showBlue && state.allowNortheast )
            southeastRed.setVisible( showRed && !state.allowSoutheast )
            southeastBlue.setVisible( showBlue && state.allowSoutheast )
            southwestRed.setVisible( showRed && !state.allowSouthwest )
            southwestBlue.setVisible( showBlue && state.allowSouthwest )
            verticalRed.setVisible( showRed && !state.allowVertical )
            verticalBlue.setVisible( showBlue && state.allowVertical )
            horizontalRed.setVisible( showRed && !state.allowHorizontal )
            horizontalBlue.setVisible( showBlue && state.allowHorizontal )
            emptyRed.setVisible( showRed && !state.allowEmpty )
            emptyBlue.setVisible( showBlue && state.allowEmpty )
            //            northwestRed.setVisible( showRed && !state.allowNorthwest && (northWhite && westWhite) )
            //            northwestBlue.setVisible( showBlue && state.allowNorthwest && (northWhite && westWhite) )
            //            northeastRed.setVisible( showRed && !state.allowNortheast && (northWhite && eastWhite) )
            //            northeastBlue.setVisible( showBlue && state.allowNortheast && (northWhite && eastWhite) )
            //            southeastRed.setVisible( showRed && !state.allowSoutheast && (southWhite && eastWhite) )
            //            southeastBlue.setVisible( showBlue && state.allowSoutheast && (southWhite && eastWhite) )
            //            southwestRed.setVisible( showRed && !state.allowSouthwest && (southWhite && westWhite) )
            //            southwestBlue.setVisible( showBlue && state.allowSouthwest && (southWhite && westWhite) )
            //            verticalRed.setVisible( showRed && !state.allowVertical && (northWhite && southWhite) )
            //            verticalBlue.setVisible( showBlue && state.allowVertical && (northWhite && southWhite) )
            //            horizontalRed.setVisible( showRed && !state.allowHorizontal && (westWhite && eastWhite) )
            //            horizontalBlue.setVisible( showBlue && state.allowHorizontal && (westWhite && eastWhite) )
        }
    }

    def createBounce( color: Color ): PPath = {
        val path = new PPath
        val len = LineWidth * 2
        path.setStrokePaint( color )
        path.moveTo( LineWidth, LineWidth + len )
        path.lineTo( LineWidth, LineWidth )
        path.lineTo( LineWidth + len, LineWidth )
        path
    }

    def createStraight( color: Color ): PPath = {
        val path = new PPath
        val len = LineWidth * 3
        path.setStrokePaint( color )
        path.moveTo( 0, len )
        path.lineTo( 0, -len )
        path
    }

    def createEmpty( color: Color ): PPath = {
        val path = PPath.createEllipse( -LineWidth/2, -LineWidth/2, LineWidth, LineWidth )
        path.setStrokePaint( color )
        path
    }
}