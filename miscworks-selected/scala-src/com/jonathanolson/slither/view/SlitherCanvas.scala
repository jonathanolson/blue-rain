package com.jonathanolson.slither.view

import edu.umd.cs.piccolo.util.PPaintContext
import com.jonathanolson.slither.model.Grid4
import edu.umd.cs.piccolo.event.{PInputEvent, PBasicInputEventHandler, PInputEventListener}
import edu.umd.cs.piccolo.{PNode, PCanvas}
import javax.swing.{JComponent, JFrame}
import java.awt.{Container, Color, Dimension}

class SlitherCanvas extends PCanvas {
    var grid: Grid4 = null
    val slitherHolderNode: PNode = new PNode
    var slitherNode: SlitherNode = null

    slitherHolderNode.translate( 10, 10 )
    getLayer().addChild( slitherHolderNode )

    private var nodeListeners = Set.empty[SlitherNodeListener]
    private var canvasListeners = Set.empty[SlitherCanvasListener]

    setOpaque( true )
    setAnimatingRenderQuality( PPaintContext.HIGH_QUALITY_RENDERING )
    setInteractingRenderQuality( PPaintContext.HIGH_QUALITY_RENDERING )
    setOpaque( true )
    setPanEventHandler( null )
    setBackground( Color.WHITE )
    setZoomEventHandler( null )

    //    mouse - wheel resizing
    addInputEventListener( new PBasicInputEventHandler {
        override def mouseWheelRotated( event: PInputEvent ) = {
            val mouseWheelScale = 1.25
            if ( event.getWheelRotation > 0 ) {
                slitherNode.scale( mouseWheelScale )
            } else {
                slitherNode.scale( 1.0 / mouseWheelScale )
            }
            val slitherNodeCurrentScale: Double = slitherNode.getScale
            val totalWidth: Int = (grid.cols * SlitherNode.Scale * slitherNodeCurrentScale).toInt + 20
            val totalHeight: Int = (grid.rows * SlitherNode.Scale * slitherNodeCurrentScale).toInt + 20
            setPreferredSize( new Dimension( totalWidth, totalHeight ) )
            invalidate()
            revalidate()
            def findFrame( cmp: Container ): Option[JFrame] = {
                if ( cmp.isInstanceOf[JFrame] ) return Some( cmp.asInstanceOf[JFrame] )
                if ( cmp.getParent != null ) {
                    return findFrame( cmp.getParent )
                } else return None
            }
            findFrame( SlitherCanvas.this ) match {
                case Some( frame ) => frame.pack()
                case None => ()
            }
        }
    } )

    def setGrid( grid: Grid4 ): Unit = {
        val oldGrid = if ( this.grid == null ) None else Some( this.grid )
        this.grid = grid
        if ( slitherNode != null ) {
            slitherHolderNode.removeChild( slitherNode )
            nodeListeners.foreach( slitherNode.removeListener( _ ) )
            slitherNode.destroy()
        }
        slitherNode = new SlitherNode( grid )
        setPreferredSize( new Dimension( (grid.cols * SlitherNode.Scale).toInt + 20, (grid.rows * SlitherNode.Scale).toInt + 20 ) )
        invalidate()
        revalidate()
        slitherHolderNode.addChild( slitherNode )
        nodeListeners.foreach( slitherNode.addListener( _ ) )
        canvasListeners.foreach( _.onSetGrid( grid, oldGrid ) )
    }

    def destroy() {
        slitherNode.destroy()
    }

    def addNodeListener( listener: SlitherNodeListener ): Unit = {
        nodeListeners = nodeListeners + listener
        if ( slitherNode != null ) {
            slitherNode.addListener( listener )
        }
    }

    def removeNodeListener( listener: SlitherNodeListener ): Unit = {
        nodeListeners = nodeListeners - listener
        if ( slitherNode != null ) {
            slitherNode.removeListener( listener )
        }
    }
}

trait SlitherCanvasListener {
    def onSetGrid( grid: Grid4, oldGrid: Option[Grid4] ): Unit
}