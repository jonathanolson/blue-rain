package com.jonathanolson.slither.view

import edu.umd.cs.piccolo.PNode
import collection.mutable.{HashMap, Map => MMap}
import com.jonathanolson.slither.SlitherInclude._
import edu.umd.cs.piccolo.event.{PBasicInputEventHandler, PInputEvent}
import java.awt.event.InputEvent
import javax.swing.SwingUtilities
import com.jonathanolson.slither.model.{VertexStateGrid4, VertexState, VertexStateListener, ColoredGrid4, FaceColor, FaceColorListener, FaceValueListener, EdgeStateListener, VerticalEdge4, HorizontalEdge4, Edge4, Grid4, Face4, Vertex4}

object SlitherNode {
    var Scale: Float = 20
    var LineWidth: Float = Scale / 10

    def mapVertexPosition( v: Vertex4 ): (Float, Float) = (v.col * Scale, v.row * Scale)

    def mapFacePositionCenter( f: Face4 ): (Float, Float) = {
        val (ax, ay) = mapVertexPosition( f.northwestVertex )
        val (bx, by) = mapVertexPosition( f.southeastVertex )
        ((ax + bx) / 2, (ay + by) / 2)
    }
}

class SlitherNode(
        val grid: Grid4
        ) extends PNode {
    private val edgeMap = new HashMap[Edge4, EdgeNode]
    private val vertexMap = new HashMap[Vertex4, VertexNode]
    private val faceMap = new HashMap[Face4, FaceNode]

    private var vertexStateRed = false
    private var vertexStateBlue = false

    private var highlighted = false
    private var highlightedFace: Face4 = _

    private var listeners = Set.empty[SlitherNodeListener]

    for (row <- 0 until grid.rows; col <- 0 until grid.cols) {
        val face: Face4 = grid.faces( row )( col )
        val faceNode: FaceNode = new FaceNode( grid, face )
        addChild( faceNode )

        faceMap( face ) = faceNode

        faceNode.addInputEventListener( new PBasicInputEventHandler {
            override def mouseClicked( event: PInputEvent ) = {
                println( "clicked " + face )
                if ( (event.getModifiers & InputEvent.BUTTON3_MASK) == 0 ) {
                    listeners.foreach( _.onFaceLeftClick( face ) )
                } else {
                    // right click
                    listeners.foreach( _.onFaceRightClick( face ) )
                }
            }

            override def mouseEntered( event: PInputEvent ) = {
                if ( grid.isInstanceOf[ColoredGrid4] ) {
                    highlight( face )
                }
            }

            override def mouseExited( event: PInputEvent ) = {
                if ( grid.isInstanceOf[ColoredGrid4] ) {
                    unHighlight()
                }
            }
        } )
    }

    for (row <- 0 to grid.rows; col <- 0 until grid.cols) {
        val edge: HorizontalEdge4 = grid.hedges( row )( col )
        val edgeNode: EdgeNode = new EdgeNode( grid, edge )
        addChild( edgeNode )

        edgeMap( edge ) = edgeNode
    }

    for (row <- 0 to grid.rows; col <- 0 to grid.cols) {
        val vertex: Vertex4 = grid.vertices( row )( col )
        val vertexNode: VertexNode = new VertexNode( grid, vertex )
        addChild( vertexNode )

        vertexMap( vertex ) = vertexNode
    }

    for (row <- 0 until grid.rows; col <- 0 to grid.cols) {
        val edge: VerticalEdge4 = grid.vedges( row )( col )
        val edgeNode: EdgeNode = new EdgeNode( grid, edge )
        addChild( edgeNode )

        edgeMap( edge ) = edgeNode
    }

    val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            SwingUtilities.invokeLater( new Runnable {
                def run = edgeMap( edge ).update()
            } )
        }
    }
    val vertexStateListener: VertexStateListener = new VertexStateListener {
        override def onVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) = {
            SwingUtilities.invokeLater( new Runnable {
                def run = vertexMap( vertex ).update()
            } )
        }
    }
    val faceValueListener: FaceValueListener = new FaceValueListener {
        override def onFaceValueChange( face: Face4, oldValue: Option[Int], newValue: Option[Int] ) = {
            SwingUtilities.invokeLater( new Runnable {
                def run = faceMap( face ).update()
            } )
        }
    }
    val faceColorListener: FaceColorListener = new FaceColorListener {
        override def onColorChange( face: Face4, oldColor: FaceColor, newColor: FaceColor ) = {
            SwingUtilities.invokeLater( new Runnable {
                def run = {
                    //faceMap( face ).update()
                    val g = grid.asInstanceOf[ColoredGrid4]
                    val color = g.colorOf( face )
                    val facesToUpdate = g.facesWithColor( color ) ++ (if ( g.getOppositeColor( color ).isDefined ) g.facesWithColor( g.oppositeColor( color ) ) else Set.empty[Face4])
                    facesToUpdate.foreach( faceMap( _ ).update() )
                }
            } )
        }
    }

    for (edgeNode <- edgeMap.values) {
        edgeNode.addInputEventListener( new PBasicInputEventHandler {
            override def mouseClicked( event: PInputEvent ) = {
                val edge = edgeNode.edge
                println( "clicked " + edge )
                if ( (event.getModifiers & InputEvent.BUTTON3_MASK) == 0 ) {
                    listeners.foreach( _.onEdgeLeftClick( edge ) )
                } else {
                    // right click
                    listeners.foreach( _.onEdgeRightClick( edge ) )
                }
            }
        } )
    }

    grid.addEdgeStateListener( edgeStateListener )
    grid.addFaceValueListener( faceValueListener )
    if ( grid.isInstanceOf[VertexStateGrid4] ) {
        grid.asInstanceOf[VertexStateGrid4].addVertexStateListener( vertexStateListener )
    }
    if ( grid.isInstanceOf[ColoredGrid4] ) {
        grid.asInstanceOf[ColoredGrid4].addFaceColorListener( faceColorListener )
    }

    def removeListeners() {
        grid.removeEdgeStateListener( edgeStateListener )
        grid.removeFaceValueListener( faceValueListener )
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            grid.asInstanceOf[VertexStateGrid4].removeVertexStateListener( vertexStateListener )
        }
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            grid.asInstanceOf[ColoredGrid4].removeFaceColorListener( faceColorListener )
        }
    }

    def addListener( listener: SlitherNodeListener ): Unit = {
        listeners = listeners + listener
    }

    def removeListener( listener: SlitherNodeListener ): Unit = {
        listeners = listeners - listener
    }

    def destroy() {
        removeListeners()
    }

    def getVertexStateRed = vertexStateRed

    def setVertexStateRed( show: Boolean ) {
        vertexStateRed = show
        for ((vertex, node) <- vertexMap) {
            node.showRed = show
            node.update()
        }
    }

    def getVertexStateBlue = vertexStateBlue

    def setVertexStateBlue( show: Boolean ) {
        vertexStateBlue = show
        for ((vertex, node) <- vertexMap) {
            node.showBlue = show
            node.update()
        }
    }

    def highlight( face: Face4 ): Unit = {
        val g = grid.asInstanceOf[ColoredGrid4]
        val color = g.colorOf( face )
        if ( color.isAbsolute ) return
        if ( g.facesWithColor( color ).size < 2 && g.getOppositeColor( color ).isEmpty ) return
        if ( highlighted ) {
            unHighlight()
        }
        highlighted = true
        highlightedFace = face
        for (otherFace <- g.facesWithColor( color )) {
            faceMap( otherFace ).highlight( false )
        }
        if ( g.getOppositeColor( color ).isDefined ) {
            for (otherFace <- g.facesWithColor( g.oppositeColor( color ) )) {
                faceMap( otherFace ).highlight( true )
            }
        }
    }

    def unHighlight(): Unit = {
        if ( !highlighted ) return
        highlighted = false
        val g = grid.asInstanceOf[ColoredGrid4]
        val color = g.colorOf( highlightedFace )
        for (otherFace <- g.facesWithColor( color )) {
            faceMap( otherFace ).unHighlight()
        }
        if ( g.getOppositeColor( color ).isDefined ) {
            for (otherFace <- g.facesWithColor( g.oppositeColor( color ) )) {
                faceMap( otherFace ).unHighlight()
            }
        }
    }

    private var colorsEnabled = true

    def areColorsEnabled = colorsEnabled

    def setColorsEnabled( enabled: Boolean ): Unit = {
        if ( colorsEnabled != enabled ) {
            colorsEnabled = enabled
            for ((face, faceNode) <- faceMap) {
                faceNode.setColorsEnabled( enabled )
            }
            listeners.foreach( _.onColorsEnabledChange( enabled ) )
        }
    }

    def getFaceNode( face: Face4 ): FaceNode = faceMap( face )

    def getVertexNode( vertex: Vertex4 ): VertexNode = vertexMap( vertex )

    def getEdgeNode( edge: Edge4 ): EdgeNode = edgeMap( edge )
}

trait SlitherNodeListener {
    def onEdgeLeftClick( edge: Edge4 ) {

    }

    def onEdgeRightClick( edge: Edge4 ) {

    }

    def onFaceLeftClick( face: Face4 ) {

    }

    def onFaceRightClick( face: Face4 ) {

    }

    def onColorsEnabledChange( enabled: Boolean ) {

    }
}