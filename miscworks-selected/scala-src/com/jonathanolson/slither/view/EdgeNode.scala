package com.jonathanolson.slither.view

import edu.umd.cs.piccolo.PNode
import com.jonathanolson.slither.view.SlitherNode.{Scale, LineWidth}
import com.jonathanolson.slither.SlitherInclude._
import java.awt.{BasicStroke, Color}
import edu.umd.cs.piccolo.nodes.PPath
import com.jonathanolson.slither.model.{HorizontalEdge4, Edge4, Grid4}
import edu.umd.cs.piccolo.event.{PInputEvent, PBasicInputEventHandler}
import java.awt.event.InputEvent

class EdgeNode(
        val grid: Grid4,
        val edge: Edge4
        ) extends PNode {
    val crossSize = 1.75f
    val hitSize = 4f

    val (x, y) = SlitherNode.mapVertexPosition( grid.vertices( edge.row )( edge.col ) )

    val (midx: Float, midy: Float) = if ( edge.isInstanceOf[HorizontalEdge4] ) {
        (x + Scale / 2, y)
    } else {
        (x, y + Scale / 2)
    }

    val hitArea: PPath = new PPath()
    drawHitArea()
    hitArea.translate( x, y )
    addChild( hitArea )

    val cross: PNode = new PNode()
    cross.translate( midx, midy )
    drawCross()
    addChild( cross )

    val line: PNode = new PNode()
    line.translate( x, y )
    drawLine()
    addChild( line )

    update()

    def update() {
        grid.state( edge ) match {
            case White => cross.setVisible( false ); line.setVisible( false ); line.setPaint( Color.WHITE )
            case Black => cross.setVisible( false ); line.setVisible( true ); line.setPaint( Color.BLACK )
            case Red => cross.setVisible( true ); line.setVisible( false );
        }
    }

    def drawLine() {
        if ( edge.isInstanceOf[HorizontalEdge4] ) {
            line.setBounds( LineWidth * 1.0, -LineWidth / 2, LineWidth * 8, LineWidth );
        } else {
            line.setBounds( -LineWidth / 2, LineWidth * 1.0, LineWidth, LineWidth * 8 );
        }
    }

    def drawCross() {
        val a: PPath = PPath.createLine( -crossSize, -crossSize, crossSize, crossSize )
        val b: PPath = PPath.createLine( -crossSize, crossSize, crossSize, -crossSize )
        a.setStrokePaint( Color.RED )
        b.setStrokePaint( Color.RED )
        a.setStroke( new BasicStroke( 0.75f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER ) )
        b.setStroke( new BasicStroke( 0.75f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_MITER ) )
        cross.addChild( a )
        cross.addChild( b )
    }

    def drawHitArea() {
        hitArea.setPaint( new Color( 0, 0, 0, 0 ) )
        hitArea.setStrokePaint( new Color( 0, 0, 0, 0 ) )
        if ( edge.isInstanceOf[HorizontalEdge4] ) {
            hitArea.moveTo( 0, 0 )
            hitArea.lineTo( hitSize, hitSize )
            hitArea.lineTo( Scale - hitSize, hitSize )
            hitArea.lineTo( Scale, 0 )
            hitArea.lineTo( Scale - hitSize, -hitSize )
            hitArea.lineTo( hitSize, -hitSize )
            hitArea.lineTo( 0, 0 )
        } else {
            hitArea.moveTo( 0, 0 )
            hitArea.lineTo( hitSize, hitSize )
            hitArea.lineTo( hitSize, Scale - hitSize )
            hitArea.lineTo( 0, Scale )
            hitArea.lineTo( -hitSize, Scale - hitSize )
            hitArea.lineTo( -hitSize, hitSize )
            hitArea.lineTo( 0, 0 )
        }
    }

}