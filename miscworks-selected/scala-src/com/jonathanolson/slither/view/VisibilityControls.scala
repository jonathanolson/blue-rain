package com.jonathanolson.slither.view

import javax.swing.{JCheckBox, JPanel}
import java.awt.{GridBagConstraints, GridBagLayout}
import java.awt.event.{ActionListener, ActionEvent}

class VisibilityControlsPanel extends JPanel {
    // contain default values here
    private val vertexRedCheck = new JCheckBox( "Vertex red", false )
    private val vertexBlueCheck = new JCheckBox( "Vertex blue", false )
    private val showColors = new JCheckBox( "colors", true )

    var slitherNode: SlitherNode = _

    setLayout( new GridBagLayout )

    add( vertexRedCheck, {val c = new GridBagConstraints(); c.gridx = 0; c.gridy = 0; c; } )
    add( vertexBlueCheck, {val c = new GridBagConstraints(); c.gridx = 1; c.gridy = 0; c; } )
    add( showColors, {val c = new GridBagConstraints(); c.gridx = 2; c.gridy = 0; c; } )

    vertexRedCheck.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            if ( vertexRedCheck.isSelected != slitherNode.getVertexStateRed ) {
                slitherNode.setVertexStateRed( vertexRedCheck.isSelected )
            }
        }
    } )

    vertexBlueCheck.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            if ( vertexBlueCheck.isSelected != slitherNode.getVertexStateBlue ) {
                slitherNode.setVertexStateBlue( vertexBlueCheck.isSelected )
            }
        }
    } )

    showColors.addActionListener( new ActionListener {
        def actionPerformed( p1: ActionEvent ) = {
            if ( showColors.isSelected != slitherNode.areColorsEnabled ) {
                slitherNode.setColorsEnabled( showColors.isSelected )
            }
        }
    } )

    def setSlitherNode( slitherNode: SlitherNode, keepSettings: Boolean = true ): Unit = {
        this.slitherNode = slitherNode
        if ( keepSettings ) {
            slitherNode.setVertexStateRed( vertexRedCheck.isSelected )
            slitherNode.setVertexStateBlue( vertexBlueCheck.isSelected )
            slitherNode.setColorsEnabled( showColors.isSelected )
        } else {
            vertexRedCheck.setSelected( slitherNode.getVertexStateRed )
            vertexBlueCheck.setSelected( slitherNode.getVertexStateBlue )
            showColors.setSelected( slitherNode.areColorsEnabled )
        }
    }
}