package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.model.{EdgeStateListener, Vertex4, Edge4, Grid4}

object SimpleVertexSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): SimpleVertexSolver = {
        new SimpleVertexSolver( grid )
    }
}

class SimpleVertexSolver( val grid: Grid4 ) extends Solver {
    private var vertices = Set.empty[Vertex4]

    private val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            vertices = vertices + edge.va + edge.vb
            if ( isAuto ) solve()
        }
    }
    grid.addEdgeStateListener( edgeStateListener )

    def processVertex( vertex: Vertex4 ) {
        var redCount: Int = 0 // effective, not actual
        var blackCount: Int = 0

        for (edgeOpt <- vertex.edges) {
            grid.getState( edgeOpt ) match {
                case Red => redCount += 1
                case Black => blackCount += 1
                case _ => ()
            }
        }

        if ( blackCount > 2 ) throw new InvalidGridException
        if ( (blackCount == 1) && redCount == 3 ) throw new InvalidGridException
        if ( blackCount + redCount == 4 ) {
            return
        }

        // we have white adjacent

        def setWhiteEdges( s: EdgeState ) {
            for (
                edgeOpt <- vertex.edges;
                if edgeOpt.isDefined;
                edge = edgeOpt.get;
                if grid.state( edge ) == White
            ) {
                grid.move( edge -> s )
            }
        }

        if ( redCount == 3 ) {
            setWhiteEdges( Red )
        } else if ( redCount == 2 && blackCount == 1 ) {
            setWhiteEdges( Black )
        } else if ( blackCount == 2 ) {
            setWhiteEdges( Red )
        }
    }

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        SimpleVertexSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeEdgeStateListener( edgeStateListener )
    }

    def solve() = {
        for (vertex <- vertices) {
            vertices = vertices - vertex
            processVertex( vertex )
        }
    }

    def isDirty() = !vertices.isEmpty
}