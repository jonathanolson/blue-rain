package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{FaceColor, Face4, FaceColorListener, EdgeStateListener, ColoredGrid4, Grid4, VertexStateGrid4, Edge4}

object FaceColorEdgeSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): FaceColorEdgeSolver = {
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            new FaceColorEdgeSolver( grid.asInstanceOf[ColoredGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class FaceColorEdgeSolver( val grid: ColoredGrid4 ) extends Solver {
    private var faces = Set.empty[Face4]

    private val colorListener: FaceColorListener = new FaceColorListener {
        override def onColorChange( face: Face4, oldColor: FaceColor, newColor: FaceColor ) = {
            faces += face
            if ( isAuto ) solve()
        }
    }

    grid.addFaceColorListener( colorListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        FaceColorEdgeSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeFaceColorListener( colorListener )
    }

    def solve() = {
        for (face <- faces) {
            faces -= face
            val newColor = grid.colorOf( face )
            for (edge <- face.edges) {
                val otherColor: FaceColor = grid.getColorOf( edge.otherFace( face ) )
                if ( grid.isSameColor( newColor, otherColor ) ) {
                    grid.move( edge -> Red )
                }
                if ( grid.isOppositeColor( newColor, otherColor ) ) {
                    grid.move( edge -> Black )
                }
            }
        }
    }

    def isDirty() = !faces.isEmpty
}