package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{FaceColor, Face4, FaceColorListener, EdgeStateListener, ColoredGrid4, Grid4, VertexStateGrid4, Edge4}
import com.jonathanolson.slither.util.Direction

object MediumFaceColorSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): MediumFaceColorSolver = {
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            new MediumFaceColorSolver( grid.asInstanceOf[ColoredGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class MediumFaceColorSolver( val grid: ColoredGrid4 ) extends Solver { // handles the 4 colors around a non-empty face
    private var faces = Set.empty[Face4]

    private val colorListener: FaceColorListener = new FaceColorListener {
        override def onColorChange( face: Face4, oldColor: FaceColor, newColor: FaceColor ) = {
            // process this face
            faces = faces + face

            // process all existing adjacent faces
            // TODO: api simplification for this?
            for (edge <- face.edges; faceOpt = edge.otherFace( face ); if faceOpt.isDefined) faces = faces + faceOpt.get
            if ( isAuto ) solve()
        }
    }
    grid.addFaceColorListener( colorListener )

    def processFace( face: Face4 ) {
        // map of direction -> color
        val colorMap = Map( Direction.primaries.map( (d) => (d, grid.getColorOf( face.getEdge( d ).otherFace( face ) )) ): _* )
        val dirSet = Set.empty ++ Direction.primaries

        grid.value( face ) match {
        // TODO api improvement to refactor common code in cases
            case Some( 1 ) => {
                for (
                    i <- 0 to 3;
                    dir1 = Direction.primaries( i );
                    color1 = colorMap( dir1 );
                    j <- (i + 1) to 3;
                    dir2 = Direction.primaries( j );
                    color2 = colorMap( dir2 )
                ) {
                    val same = grid.isSameColor( color1, color2 )
                    val opp = grid.isOppositeColor( color1, color2 )
                    if ( same || opp ) {
                        val remainingDirs = ((dirSet - dir1) - dir2).toSeq
                        val color3 = grid.getColorOf( face.getEdge( remainingDirs( 0 ) ).otherFace( face ) )
                        val color4 = grid.getColorOf( face.getEdge( remainingDirs( 1 ) ).otherFace( face ) )
                        if ( same ) {
                            grid.setColorsSame( grid.colorOf( face ), color1 ) // have two equal neighbors, must have same color in center
                            grid.setColorsOpposite( color3, color4 )
                        } else if ( opp ) {
                            grid.setColorsSame( color3, color4 ) // two different, so other two (and our face) must be the same
                            grid.setColorsSame( grid.colorOf( face ), color3 )
                        }
                    }
                }
            }
            case Some( 2 ) => {
                // iterate through unique combinations of 2 edges
                for (
                    i <- 0 to 3;
                    dir1 = Direction.primaries( i );
                    color1 = colorMap( dir1 );
                    j <- (i + 1) to 3;
                    dir2 = Direction.primaries( j );
                    color2 = colorMap( dir2 )
                ) {
                    val same = grid.isSameColor( color1, color2 )
                    val opp = grid.isOppositeColor( color1, color2 )
                    if ( same || opp ) {
                        val remainingDirs = ((dirSet - dir1) - dir2).toSeq
                        val color3 = grid.getColorOf( face.getEdge( remainingDirs( 0 ) ).otherFace( face ) )
                        val color4 = grid.getColorOf( face.getEdge( remainingDirs( 1 ) ).otherFace( face ) )
                        if ( same ) {
                            grid.setColorsSame( color3, color4 ) // the other colors are the same too now
                            grid.setColorsOpposite( color1, color3 ) // and we make sure they are opposites
                        } else if ( opp ) {
                            grid.setColorsOpposite( color3, color4 )
                        }
                    }
                }
            }
            case Some( 3 ) => {
                for (
                    i <- 0 to 3;
                    dir1 = Direction.primaries( i );
                    color1 = colorMap( dir1 );
                    j <- (i + 1) to 3;
                    dir2 = Direction.primaries( j );
                    color2 = colorMap( dir2 )
                ) {
                    val same = grid.isSameColor( color1, color2 )
                    val opp = grid.isOppositeColor( color1, color2 )
                    if ( same || opp ) {
                        val remainingDirs = ((dirSet - dir1) - dir2).toSeq
                        val color3 = grid.getColorOf( face.getEdge( remainingDirs( 0 ) ).otherFace( face ) )
                        val color4 = grid.getColorOf( face.getEdge( remainingDirs( 1 ) ).otherFace( face ) )
                        if ( same ) {
                            grid.setColorsOpposite( grid.colorOf( face ), color1 ) // have two equal neighbors, must have different color in center
                            grid.setColorsOpposite( color3, color4 )
                        } else if ( opp ) {
                            grid.setColorsSame( color3, color4 ) // two different, so other two (and our face) must be the same
                            grid.setColorsOpposite( grid.colorOf( face ), color3 )
                        }
                    }
                }
            }
            case _ => ()
        }
    }

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        MediumFaceColorSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeFaceColorListener( colorListener )
    }

    def solve() = {
        for (face <- faces) {
            faces = faces - face
            processFace( face )
        }
    }

    def isDirty() = !faces.isEmpty
}