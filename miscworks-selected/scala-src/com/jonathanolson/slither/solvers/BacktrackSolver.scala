package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import scala.collection.mutable.ListBuffer
import com.jonathanolson.slither.model.{ColoredGrid4, VertexState, VertexStateGrid4, Move, FaceColorMove, FaceColor, VertexStateMove, EdgeStateMove, Vertex4, Face4, Edge4, Grid4}
import com.jonathanolson.slither.util.LogHelper

class BacktrackSolver( edgeMatch: Boolean = true, vertexMatch: Boolean = true, colorMatch: Boolean = false ) extends LogHelper {
    private def test( grid: Grid4, edge: Edge4, stack: SolverStack, f: (Grid4, SolverStack) => Unit ): Unit = {
        //        if ( logger.isDebugEnabled ) logger.debug( "testing edge " + edge )
        val redGrid = grid.newChild
        val blackGrid = grid.newChild
        try { // try red
            val childStack = stack.forChild( redGrid )
            //            childStack.solvers += LoopInvalidator.attach( redGrid, Some( grid ) )
            redGrid.move( edge -> Red )
            childStack.solve()
            f( redGrid, childStack )
        } catch {
            case e: InvalidGridException => {
                grid.move( edge -> Black )
                return
            }
        }
        try { // try black
            val childStack = stack.forChild( blackGrid )
            //            childStack.solvers += LoopInvalidator.attach( blackGrid, Some( grid ) )
            blackGrid.move( edge -> Black )
            childStack.solve()
            f( blackGrid, childStack )
        } catch {
            case e: InvalidGridException => {
                grid.move( edge -> Red )
                return
            }
        }

        if ( !edgeMatch && !vertexMatch && !colorMatch ) return

        // didn't detect a conflict. now we look at matching to find other uses
        var touchedEdges = Set.empty[Edge4]
        var touchedVertices = Set.empty[Vertex4]
        var touchedColors = Set.empty[FaceColor]

        // find everything we touched
        for (move <- (redGrid.moves ++ blackGrid.moves)) {
            move match {
                case m: EdgeStateMove => touchedEdges += m.edge
                case m: VertexStateMove => touchedVertices += m.vertex
                case m: FaceColorMove => {
                    touchedColors += m.color1
                    touchedColors += m.color2
                }
                case _ => ()
            }
        }

        val moves = new ListBuffer[Move]

        if ( edgeMatch ) {
            for (edge <- touchedEdges) {
                if ( (grid.state( edge ) == White) && (redGrid.state( edge ) == blackGrid.state( edge )) ) {
                    moves.append( new EdgeStateMove( edge, redGrid.state( edge ) ) )
                }
            }
        }

        if ( vertexMatch ) {
            if ( grid.isInstanceOf[VertexStateGrid4] ) {
                val g = grid.asInstanceOf[VertexStateGrid4]
                for (vertex <- touchedVertices) {
                    val oldState = g.vertexState( vertex )
                    val redState = redGrid.asInstanceOf[VertexStateGrid4].vertexState( vertex )
                    val blackState = blackGrid.asInstanceOf[VertexStateGrid4].vertexState( vertex )
                    val newState = redState | blackState
                    if ( newState != oldState ) {
                        moves.append( new VertexStateMove( vertex, newState ) )
                    }
                }
            }
        }

        if ( colorMatch ) {
            if ( grid.isInstanceOf[ColoredGrid4] ) {
                val g = grid.asInstanceOf[ColoredGrid4]
                val r = redGrid.asInstanceOf[ColoredGrid4]
                val b = blackGrid.asInstanceOf[ColoredGrid4]
                val colors = touchedColors.toSeq
                for (i <- 0 until colors.size; j <- (i + 1) until colors.size) {
                    val color1 = colors( i )
                    val color2 = colors( j )
                    val gs = g.isSameColor( color1, color2 )
                    val go = g.isOppositeColor( color1, color2 )
                    val rs = r.isSameColor( color1, color2 )
                    val ro = r.isOppositeColor( color1, color2 )
                    val bs = b.isSameColor( color1, color2 )
                    val bo = b.isOppositeColor( color1, color2 )
                    if ( !gs && rs && bs ) {
                        moves.append( new FaceColorMove( color1, color2, true ) )
                    }
                    if ( !go && ro && bo ) {
                        moves.append( new FaceColorMove( color1, color2, false ) )
                    }
                }
            }
        }

        moves.foreach( _.apply( grid ) )
    }

    private def testSolveAllEdgesUntilClean( grid: Grid4, stack: SolverStack, f: (Grid4, SolverStack) => Unit ): Unit = {
        // go through all of the edges repeatedly until we can't make any more deductions
        //        logger.debug( "testSolveAllEdgesUntilClean" )
        var ret = true
        while (ret) {
            val startMoves: Int = grid.moves.size
            ret = false
            for (edge <- grid.edgeIterator; if grid.state( edge ) == White) {
                test( grid, edge, stack, f )
                stack.solve()
                if ( grid.moves.size > startMoves ) {
                    ret = true
                }
            }
        }
    }

    private def testSolveAllEdgesUntilHit( grid: Grid4, stack: SolverStack, f: (Grid4, SolverStack) => Unit ): Unit = {
        // go through the edges at most once, stopping once we have made a deduction
        //        logger.debug( "testSolveAllEdgesUntilHit" )
        val startMoves: Int = grid.moves.size
        for (edge <- grid.edgeIterator; if grid.state( edge ) == White) {
            test( grid, edge, stack, f )
            stack.solve()
            if ( grid.moves.size > startMoves ) {
                return
            }
        }
    }

    private def nStepUntilClean( grid: Grid4, stack: SolverStack, depth: Int ): Unit = {
        // run the search at depth until no more deductions can be made
        //        logger.debug( "nStepUntilClean:" + depth )
        require( depth > 0 )
        testSolveAllEdgesUntilClean( grid, stack, (childGrid, childStack) => {
            if ( depth == 1 ) () else nStepUntilClean( childGrid, childStack, depth - 1 )
        } )
    }

    private def nStepUntilRootHit( grid: Grid4, stack: SolverStack, depth: Int ): Unit = {
        // run the search at depth, stop when a deduction has been made for the root grid
        //        logger.debug( "nStepUntilRootHit:" + depth )
        require( depth > 0 )

        // for the root testing (1st edge), we bail once we identify a concrete change
        testSolveAllEdgesUntilHit( grid, stack, (childGrid, childStack) => {
            // we continue to use nStepUntilClean for farther down operations
            if ( depth == 1 ) () else nStepUntilClean( childGrid, childStack, depth - 1 )
        } )
    }

    private def oneStep( grid: Grid4, stack: SolverStack ): Unit = {
        testSolveAllEdgesUntilClean( grid, stack, (grid, stack) => () )
    }

    def breadthFirstToNSteps( grid: Grid4, stack: SolverStack, depth: Int ): Unit = {
        require( depth > 0 )
        var curDepth = 1

        // attach solver if we need to
        if ( !stack.solvers.exists( _.isInstanceOf[LoopInvalidator] ) ) {
            stack.solvers += LoopInvalidator.attach( grid )
        }

        while ((curDepth <= depth) && (grid.whiteCount > 0)) {
            val a = System.currentTimeMillis
            nStepUntilClean( grid, stack, curDepth )
            val b = System.currentTimeMillis
            logger.debug( curDepth + "-step " + (b - a) )
            curDepth += 1
        }
    }

    def adaptiveBreadthFirstToNSteps( grid: Grid4, stack: SolverStack, depth: Int ): Unit = {
        // bail back to the 1-depths once we find something at higher depths
        require( depth > 0 )
        var curDepth = 1

        // attach solver if we need to
        if ( !stack.solvers.exists( _.isInstanceOf[LoopInvalidator] ) ) {
            stack.solvers += LoopInvalidator.attach( grid )
        }

        while ((curDepth <= depth) && (grid.whiteCount > 0)) {
            val startMoves: Int = grid.moves.size
            logger.debug( "starting depth " + curDepth )

            val a = System.currentTimeMillis
            if ( curDepth == 1 ) {
                nStepUntilClean( grid, stack, curDepth )
            } else {
                nStepUntilRootHit( grid, stack, curDepth )
            }
            val b = System.currentTimeMillis
            logger.debug( curDepth + "-step " + (b - a) )


            if ( (grid.moves.size > startMoves) && (curDepth != 1) ) {
                curDepth = 1 // we made a change, step back to depth 1
            } else {
                curDepth += 1 // no changes found, need to increase depth
            }
        }
    }
}