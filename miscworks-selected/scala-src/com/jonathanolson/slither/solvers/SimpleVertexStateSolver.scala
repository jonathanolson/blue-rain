package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{Grid4, VertexStateGrid4, EdgeStateListener, Edge4}

object SimpleVertexStateSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): SimpleVertexStateSolver = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            new SimpleVertexStateSolver( grid.asInstanceOf[VertexStateGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class SimpleVertexStateSolver( val grid: VertexStateGrid4 ) extends Solver {
    private var edges = Set.empty[Edge4]

    private val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            edges = edges + edge
            if ( isAuto ) {
                solve()
            }
        }
    }
    grid.addEdgeStateListener( edgeStateListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        SimpleVertexStateSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeEdgeStateListener( edgeStateListener )
    }

    def solve() = {
        for (edge <- edges; vertex <- edge.vertices) {
            edges = edges - edge
            val oldV = grid.vertexState( vertex )
            val newV = oldV.withEdge( vertex.directionOfEdge( edge ), grid.state( edge ) )
            grid.vertexState( vertex ) = newV
        }
    }

    def isDirty() = !edges.isEmpty
}