package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.Direction
import com.jonathanolson.slither.model.{Grid4, VertexState, Vertex4, VertexStateListener, VertexStateGrid4}

object VertexStateSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): VertexStateSolver = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            new VertexStateSolver( grid.asInstanceOf[VertexStateGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class VertexStateSolver( val grid: VertexStateGrid4 ) extends Solver {
    private var vertices = Set.empty[Vertex4]

    private val stateListener: VertexStateListener = new VertexStateListener {
        override def onVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) = {
            vertices = vertices + vertex
            if ( isAuto ) {
                solve()
            }
        }
    }
    grid.addVertexStateListener( stateListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        VertexStateSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeVertexStateListener( stateListener )
    }

    def solve() = {
        for (vertex <- vertices; newState = grid.vertexState( vertex )) {
            vertices = vertices - vertex
            for (dir <- Direction.primaries; edgeOpt = vertex.getEdge( dir ); if edgeOpt.isDefined) {
                val edge = edgeOpt.get
                val constrained = newState.constrainedValue( dir )
                (constrained, grid.state( edge )) match {
                    case (White, _) => () // not constrained
                    case (x, White) => grid.move( edge -> x ) // constrained and new move
                    case (x, y) if x == y => () // constraint matches current state
                    case _ => throw new InvalidGridException // constraint violates current state
                }
            }
        }
    }

    def isDirty() = !vertices.isEmpty
}