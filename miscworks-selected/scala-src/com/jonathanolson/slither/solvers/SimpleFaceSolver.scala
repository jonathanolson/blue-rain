package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.model.{EdgeStateListener, Face4, Edge4, Grid4}

object SimpleFaceSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): SimpleFaceSolver = {
        new SimpleFaceSolver( grid )
    }
}

class SimpleFaceSolver( val grid: Grid4 ) extends Solver {
    private var faces = Set.empty[Face4]

    private val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            if ( edge.fa.isDefined ) faces = faces + edge.fa.get
            if ( edge.fb.isDefined ) faces = faces + edge.fb.get
            if ( isAuto ) solve()
        }
    }
    grid.addEdgeStateListener( edgeStateListener )

    def processFace( face: Face4 ) {
        if ( grid.value( face ) == None ) {
            return
        }
        val faceCount: Int = grid.value( face ).get
        var redCount: Int = 0
        var blackCount: Int = 0
        for (edge <- face.edges) {
            if ( grid.state( edge ) == Black ) blackCount += 1
            if ( grid.state( edge ) == Red ) redCount += 1
        }
        if ( redCount + blackCount == 4 ) {
            // no white edges
            return
        }
        if ( (blackCount > faceCount) || (4 - redCount < faceCount) ) {
            throw new InvalidGridException
        }

        // now we know we have white edges

        if ( blackCount == faceCount ) {
            for (edge <- face.edges; if grid.state( edge ) == White) {
                grid.move( edge -> Red )
            }
        } else if ( redCount == 4 - faceCount ) {
            for (edge <- face.edges; if grid.state( edge ) == White) {
                grid.move( edge -> Black )
            }
        }
    }

    def attachToChild( child: Grid4, parent: Grid4 ) = SimpleFaceSolver.attach( child, Some( parent ) )

    def detach() = {
        grid.removeEdgeStateListener( edgeStateListener )
    }

    def solve() = {
        for (face <- faces) {
            faces = faces - face
            processFace( face )
        }
    }

    def isDirty() = !faces.isEmpty
}