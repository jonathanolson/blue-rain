package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.Spot
import com.jonathanolson.slither.model.Grid4

object StaticFaceSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): StaticFaceSolver = {
        new StaticFaceSolver( grid )
    }
}

class StaticFaceSolver( grid: Grid4 ) extends StaticSolver {
    def solve(): Unit = {
        if ( hasRun ) return
        zeros()
        doubleThrees()
        diagThrees()
        hasRun = true
    }

    def attachToChild( child: Grid4, parent: Grid4 ): StaticFaceSolver = {
        val solver = StaticFaceSolver.attach( child, Some( parent ) )
        solver.hasRun = this.hasRun
        solver
    }

    def zeros() {
        for (face <- grid.faceIterator; if grid.value( face ) == Some( 0 )) {
            for (edge <- face.edges) {
                grid.move( edge -> Red )
            }
        }
    }

    def doubleThrees() {
        for (face <- grid.faceIterator; if grid.value( face ) == Some( 3 )) {
            face.southEdge.otherFace( face ) match {
                case Some( lowerFace ) => if ( grid.value( lowerFace ) == Some( 3 ) ) {processFind( new Spot( grid, face.southwestVertex, East ) )}
                case None => ()
            }
            face.eastEdge.otherFace( face ) match {
                case Some( rightFace ) => if ( grid.value( rightFace ) == Some( 3 ) ) {processFind( new Spot( grid, face.southeastVertex, North ) )}
                case None => ()
            }
        }

        def processFind( spot: Spot ) {
            grid.move( spot.getEdge( North ) -> Black )
            grid.move( spot.shiftLeft.getEdge( North ) -> Black )
            grid.move( spot.shiftRight.getEdge( North ) -> Black )
            grid.move( spot.getEdge( South ) -> Red )
            grid.move( spot.forward.getEdge( North ) -> Red )
        }
    }

    def diagThrees() {
        for (face <- grid.faceIterator; if grid.value( face ) == Some( 3 )) {
            face.northeastVertex.northeastFace match {
                case Some( otherFace ) => if ( grid.value( otherFace ) == Some( 3 ) ) {processFind( new Spot( grid, face.northeastVertex, North ) )}
                case None => ()
            }
            face.southeastVertex.southeastFace match {
                case Some( otherFace ) => if ( grid.value( otherFace ) == Some( 3 ) ) {processFind( new Spot( grid, face.southeastVertex, East ) )}
                case None => ()
            }
        }

        def processFind( spot: Spot ) {
            val a = spot.forward.shiftRight.turnBack
            val b = spot.shiftBack.shiftLeft
            grid.move( a.getEdge( North ) -> Black )
            grid.move( a.getEdge( East ) -> Black )
            grid.move( b.getEdge( North ) -> Black )
            grid.move( b.getEdge( East ) -> Black )
        }
    }
}