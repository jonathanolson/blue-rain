package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.model.{Path4, Face4, Edge4, Grid4, EdgeStateListener}
import com.jonathanolson.slither.util.LogHelper

object LoopInvalidator {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): LoopInvalidator = {
        new LoopInvalidator( grid )
    }
}

class LoopInvalidator( val grid: Grid4 ) extends Solver with LogHelper {
    //    require( grid.openBorders == true )

    setAuto( true )

    private var edges = Set.empty[Edge4]

    private val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            edges = edges + edge
            if ( isAuto ) {
                solve()
            }
        }
    }
    grid.addEdgeStateListener( edgeStateListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        LoopInvalidator.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeEdgeStateListener( edgeStateListener )
    }

    def getStateAndRemove( edge: Edge4 ): EdgeState = {
        edges -= edge
        grid.state( edge )
    }

    def solve() = {
        for (edge <- edges) {
            edges -= edge
            val newState = getStateAndRemove( edge );
            if ( newState == Black ) {
                val optA = grid.otherBlackEdge( edge.va, edge );
                if ( !optA.isEmpty ) {
                    val optB = grid.otherBlackEdge( edge.vb, edge );
                    if ( !optB.isEmpty ) {
                        val path = Path4.createBlackLightPath( grid, edge.va );
                        if ( path.isLoop ) {
                            if ( grid.openBorders || (grid.blackCount > path.length) ) {
                                throw new InvalidGridException
                            }
                        }
                    }
                }
            }
        }
        //        for ( // TODO: optimize! (keep ends of paths around somewhere)
        //            edge <- edges;
        //            newState = getStateAndRemove( edge );
        //            if newState == Black;
        //            optA = grid.otherBlackEdge( edge.va, edge );
        //            if !optA.isEmpty;
        //            optB = grid.otherBlackEdge( edge.vb, edge );
        //            if !optB.isEmpty;
        //            path = Path4.createBlackLightPath( grid, edge.va );
        //            if path.isLoop
        //        ) {
        //            if ( grid.openBorders || (grid.blackCount > path.length) ) {
        //                throw new InvalidGridException
        //            }
        //        }
    }

    def isDirty() = !edges.isEmpty
}