package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.view.SimpleSlitherFrame
import com.jonathanolson.slither.exceptions.InvalidGridException
import scala.collection.mutable.{PriorityQueue, ArrayBuffer}
import scala.util.Random
import java.io.{BufferedReader, FileReader, File}
import com.jonathanolson.slither.model.{ColoredGrid4, GridConstraints, Edge4, Grid4, VertexStateGrid4}
import com.jonathanolson.slither.util.LogHelper

object SolutionFinder extends LogHelper {
    /**
     * Return a traversable entity over complete grids.
     * NOTE: if startGrid is not a VertexStateGrid4, its objects will not be found in the solutions
     */
    def solutionsOf( startGrid: Grid4, random: Boolean = false ): Traversable[Grid4] = {
        val midGrid = startGrid.newChild

        val stack = new SolverStack( midGrid )
        stack.solvers += StaticFaceSolver.attach( midGrid )
        stack.solvers += StaticFaceVertexStateSolver.attach( midGrid )
        stack.solvers += SimpleFaceSolver.attach( midGrid )
        stack.solvers += SimpleVertexSolver.attach( midGrid )
        stack.solvers += SimpleVertexStateSolver.attach( midGrid )
        stack.solvers += VertexStateSolver.attach( midGrid )
        stack.solvers += FaceVertexStateSolver.attach( midGrid )
        stack.solvers += SimpleFaceColorSolver.attach( midGrid )
        stack.solvers += FaceColorEdgeSolver.attach( midGrid )
        stack.solvers += MediumFaceColorSolver.attach( midGrid )

        try {
            stack.solve()

            if ( !midGrid.isComplete ) {
                //                (new BacktrackSolver).breadthFirstToNSteps( midGrid, stack, 1 )
                (new BacktrackSolver( colorMatch = true )).adaptiveBreadthFirstToNSteps( midGrid, stack, 1 )
            }

            if ( midGrid.isComplete ) {
                return List( midGrid )
            } else {
                val grid = if ( midGrid.isInstanceOf[VertexStateGrid4] ) {
                    VertexStateGrid4.child( midGrid.asInstanceOf[VertexStateGrid4] )
                } else {
                    val x = VertexStateGrid4.empty( midGrid.rows, midGrid.cols, midGrid.openBorders )
                    midGrid.copyStateTo( x )
                    x
                }

                return new SolutionFinder( grid, random )
            }
        } catch {
            case e: InvalidGridException => {
                return Nil
            }
        }

    }

}

class SolutionFinder( grid: Grid4, random: Boolean = false ) extends Traversable[Grid4] { // not thread-safe
    var curIdx: Int = 0
    val edgeOrder = new ArrayBuffer[Edge4]
    private var count: Int = 0
    private var nest: Int = 0

    for (sum <- 0 to (grid.rows + grid.cols); row <- 0 to sum; col = sum - row) {
        if ( (row <= grid.rows) && (col <= grid.cols) ) {
            if ( row < grid.rows ) {
                edgeOrder.append( grid.vedges( row )( col ) )
            }
            if ( col < grid.cols ) {
                edgeOrder.append( grid.hedges( row )( col ) )
            }
        }
    }

    //    for (row <- 0 to grid.rows; col <- 0 to grid.cols) {
    //        if ( (row <= grid.rows) && (col <= grid.cols) ) {
    //            if ( row < grid.rows ) {
    //                edgeOrder.append( grid.vedges( row )( col ) )
    //            }
    //            if ( col < grid.cols ) {
    //                edgeOrder.append( grid.hedges( row )( col ) )
    //            }
    //        }
    //    }

    //    println( "edge order: " )
    //    for (edge <- edgeOrder) {
    //        println( edge )
    //    }

    //    println( "# of edges: " + edgeOrder.size )

    def foreach[U]( f: (Grid4) => U ) = backtrackSolutions[U]( grid, f )

    def backtrackSolutions[U]( grid: Grid4, f: (Grid4) => U ): Unit = {
        if ( nest < 4 ) {
            //            println( (" " * nest) + nest )
            //            println( grid.displayString )
        }
        // grid shadows class parameter
        if ( grid.isComplete ) {
            f( grid )
            count += 1
            return
        }
        if ( curIdx == edgeOrder.size ) return

        var oldIdx = curIdx
        while ((grid.state( edgeOrder( curIdx ) ) != White)) {
            curIdx += 1
            if ( curIdx == edgeOrder.size ) {
                curIdx = oldIdx
                return // ran out of things
            }
        }

        val edge = edgeOrder( curIdx )

        curIdx += 1
        nest += 1

        def tryState( state: EdgeState ): Unit = {
            val subGrid = grid.newChild
            try {
                LoopInvalidator.attach( subGrid )
                SimpleFaceSolver.attach( subGrid ).setAuto( true )
                SimpleVertexSolver.attach( subGrid ).setAuto( true )
                SimpleVertexStateSolver.attach( subGrid ).setAuto( true )
                VertexStateSolver.attach( subGrid ).setAuto( true )
                VertexStateInvalidator.attach( subGrid ).setAuto( true )
                subGrid.move( edge -> state )
                backtrackSolutions[U]( subGrid, f )
            } catch {
                case e: InvalidGridException => ()
            }
        }

        if ( random && (Random.nextInt( 2 ) == 0) ) {
            tryState( Black )
            tryState( Red )
        } else {
            tryState( Red )
            tryState( Black )
        }

        nest -= 1

        curIdx = oldIdx
    }
}