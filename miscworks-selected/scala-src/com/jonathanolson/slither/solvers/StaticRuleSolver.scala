package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.patterns.BasicRule
import com.jonathanolson.slither.model.Grid4

object StaticRuleSolver {
    lazy val SimpleRules: List[BasicRule] = List(
        "brule[s4:s1x2:open:f..:e.....-.:v7f7f7f7f7f7f:c;s4:s1x2:open:f..:e.....-.:v7f7f16197f7f:c/0,0\\1,0;]",
        "brule[s4:s1x2:open:f..:e.....x.:v7f7f7f7f7f7f:c;s4:s1x2:open:f..:e.....x.:v7f7f69667f7f:c/0,0-1,0;]",
        "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx.........x..x....:v7f7f7f7f7f48447f7f41427f7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;]",
        "brule[s4:s1x1:open:f1:e....:v7f7f7f7f:c;s4:s1x1:open:f1:e....:v7d7e7b77:c;]",
        "brule[s4:s1x1:open:f3:e....:v7f7f7f7f:c;s4:s1x1:open:f3:e....:v373b3e3d:c;]",
        "brule[s4:s3x3:open:f....1....:e...................-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....1....:e.....xx.........x..-....:v7f7f7f7f7f48447f7f12117f7f7f7f7f:c/1,0-0,1-1,1-1,2\\2,1;]",
        "brule[s4:s1x2:open:f2.:e.....x.:v7f7f7f7f7f7f:c;s4:s1x2:open:f2.:e.....x.:v373b69667f7f:c/0,0-1,0;]",
        "brule[s4:s1x2:open:f2.:e.....-.:v7f7f7f7f7f7f:c;s4:s1x2:open:f2.:e.....-.:v7d7e16197f7f:c/0,0\\1,0;]",
        "brule[s4:s3x3:open:f....3....:e...................x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....3....:e.xx..--........x-x.x....:v7f5c5c7f690201667f28247f7f7f7f7f:c/0,0-1,0-0,1-0,2-1,2\\1,1-2,1;]",
        "brule[s4:s1x1:open:f2:e....:v7f7f7f77:c;s4:s1x1:open:f2:e....:v377f7f77:c;]",
        "brule[s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f7f7f7f7f7f777f7f7f7f7f:c;s4:s3x3:open:f....3....:e.x...-.........x-.......:v7f5c7f7f6902197f7f2c357f7f7f7f7f:c/0,0-1,0-0,1\\1,1/1,2\\2,1;]",
        "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f7d7f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx...x.....x..xx...:v7f7f7f7f7f48447f7f4140667f7f537f:c/1,0-0,1-1,1-2,1-1,2-2,2;]",
        "brule[s4:s3x3:open:f....0....:e........................:v7f7f7f7f7f7f7f7f7f7f3f7f7f7f7f7f:c;s4:s3x3:open:f....0....:e.....xx...-.....x..x-...:v7f7f7f7f7f48447f7f4102197f7f2c77:c/1,0-0,1-1,1-2,1-1,2\\2,2;]",
        "brule[s4:s2x2:open:f....:e.-......-...:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f....:e.-..x...-x..:v7d237f1608667f537f:c/0,0\\1,0-0,1-1,1;]",
        "brule[s4:s4x3:open:f.....00.....:e...............................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x3:open:f.....00.....:e..x...xxx...x.......xx..xx.....:v7f7f5c7f7f7f4840447f7f4140427f7f7f537f7f:c/1,0-0,1-1,1-2,1-0,2-1,2-2,2-1,3;]",
        "brule[s4:s3x3:open:f....02...:e........................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....02...:e.....xx.........x..x....:v7f7f7f7f7f48443b7f41423d7f7f7f7f:c/1,0-0,1-1,1-2,1-1,2;]",
        "brule[s4:s4x3:open:f.....03.....:e...............................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x3:open:f.....03.....:e..-x..xx-...-x......x-x.x-x....:v7f7f235c7f7f480401667f410208667f7f2c537f:c/1,0-0,1-1,1-2,1-1,2\\0,2-2,2-0,3-1,3-2,3;]",
        "brule[s4:s4x3:open:f.....33.....:e...............................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x3:open:f.....33.....:e..x...---...x..................:v7f7f5c7f7f7f2303237f7f2c0c2c7f7f7f537f7f:c/0,1-0,2\\2,1-2,2/1,0-1,2\\1,1-1,3;]",
        "brule[s4:s4x4:open:f......0..1......:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..1......:e.......xx...x.............x..xx.........:v7f7f7f7f7f7f7f48447f7f6940427f7f3a537f7f7f7f7f7f7f:c/1,1-2,1-0,2-1,2-2,2-1,3/2,0\\3,1;]",
        "brule[s4:s4x4:open:f......0..2......:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..2......:e.......xx.................x...x.........:v7f7f7f7f7f7f7f48447f7f3541427f7f45357f7f7f7f7f7f7f:c/1,1-0,2-1,2-2,2-1,3\\2,0-3,1;]",
        "brule[s4:s4x4:open:f......0..3......:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..3......:e.......xx...-.............x..-x.........:v7f7f7f7f7f7f7f48447f7f1601427f7f3a2c7f7f7f7f7f7f7f:c/1,1-0,2-1,2-2,2-1,3\\2,1/2,0\\3,1;]",
        "brule[s4:s2x2:open:f.23.:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f.23.:e............:v7f7f3b373b7f3e3d7f:c;]",
        "brule[s4:s4x4:open:f......3..3......:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......3..3......:e...x....-..-....x.........-x....x-......:v7f7f7f5c7f7f7d1601667f233a2c7f690419777f7f537f7f7f:c/0,2-0,3-1,3\\1,2/1,1\\2,2/2,0-3,0-3,1\\2,1;]",
        "brule[s4:s1x2:open:f..:e.......:v7f7f697f7f7f:c;s4:s1x2:open:f..:e.....x.:v7f7f69667f7f:c/0,0-1,0;]",
        "brule[s4:s2x2:open:f....:e............:v7f7f7f7f3a7f7f7f7f:c;s4:s2x2:open:f....:e............:v7f7f7f7f3a7f7f7f7f:c/0,0\\1,1;]",
        "brule[s4:s1x2:open:f1.:e.......:v7f7f7f6e7f7f:c;s4:s1x2:open:f1.:e.....x.:v7d7e69667f7f:c/0,0-1,0;]",
        "brule[s4:s3x3:open:f....1....:e........................:v7f7f7f7f7f7f7f7f7f7f3d7f7f7f7f7f:c;s4:s3x3:open:f....1....:e.....x..........x.......:v7f7f7f7f7f48667f7f53357f7f7f7f7f:c/1,0-0,1-1,1/1,2\\2,1;]",
        "brule[s4:s1x1:open:f2:e....:v7f7f7f6e:c;s4:s1x1:open:f2:e....:v7f3b7b6e:c;]",
        "brule[s4:s1x1:open:f2:e....:v7f7f7f3d:c;s4:s1x1:open:f2:e....:v7d7f7f3d:c;]",
        "brule[s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f7f7f7f7f7f767f7f7f7f7f:c;s4:s3x3:open:f....3....:e.x...-.........x-.......:v7f5c7f7f6902197e7f2c347f7f7f7f7f:c/0,0-1,0-0,1\\1,1/1,2\\2,1;]",
        "brule[s4:s2x2:open:f3...:e............:v7f7f7f7f7a7f7f7f7f:c;s4:s2x2:open:f3...:e............:v373b7f3e387f7f7f7f:c/0,0\\1,1;]",
        "brule[s4:s2x2:open:f3...:e............:v7f7f7f7f6b7f7f7f7f:c;s4:s2x2:open:f3...:e.........x..:v373b7f3e29667f7f7f:c/0,1-1,1;]",
        "brule[s4:s2x1:open:f3.:e.......:v7f7f7f7f6e7f:c;s4:s2x1:open:f3.:e.-.....:v37237f3e2c7f:c/0,0\\0,1;]",
        "brule[s4:s3x3:open:f.........:e......-.........-..-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f.........:e..x..x-...x.....-x.-x...:v7f7f5c7f7f1401667f1208667f7f537f:c/0,1-2,1-0,2-1,2-2,2\\1,0-1,1;]",
        "brule[s4:s3x3:open:f....1....:e................x..x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....1....:e................x..x....:v7f7f7f7f7f69667f7f69667f7f7f7f7f:c/0,1-1,1-2,1/1,0\\1,2;]",
        "brule[s4:s3x3:open:f....1....:e......x............x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....1....:e......x............x....:v7f7f7f7f7f355c7f7f69427f7f7f7f7f:c/0,1\\1,0/1,1-2,1-1,2;]",
        "brule[s4:s3x3:open:f....2....:e................x..x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e.....--.........x..x....:v7f7f7f7f7f21227f7f28247f7f7f7f7f:c/0,1-1,1-2,1\\1,0-1,2;]",
        "brule[s4:s3x3:open:f....2....:e................x..-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e................x..-....:v7f7f7f7f7f69667f7f16197f7f7f7f7f:c/0,1-1,1\\2,1/1,0\\1,2;]",
        "brule[s4:s3x3:open:f....2....:e................-..-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e.....xx.........-..-....:v7f7f7f7f7f14187f7f12117f7f7f7f7f:c/0,1-2,1\\1,0-1,1-1,2;]",
        "brule[s4:s3x3:open:f....2....:e......x............x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e.x...-x........x-..x....:v7f5c7f7f6902187f7f28427f7f7f7f7f:c/0,0-1,0-0,1\\1,1-2,1-1,2;]",
        "brule[s4:s3x3:open:f....2....:e......x............-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e......x............-....:v7f7f7f7f7f355c7f7f16117f7f7f7f7f:c/0,1\\1,0/1,1-1,2\\2,1;]",
        "brule[s4:s3x3:open:f....2....:e......-............-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....2....:e.....x-...x.....x..-x...:v7f7f7f7f7f48227f7f1208667f7f537f:c/1,0-0,1-1,1\\2,1-1,2-2,2;]",
        "brule[s4:s3x3:open:f....3....:e................-..-....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....3....:e................-..-....:v7f7f7f7f7f16197f7f16197f7f7f7f7f:c/0,1-2,1\\1,1/1,0\\1,2;]",
        "brule[s4:s4x4:open:f......0..20.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..20.....:e.......xx..-xx..x-........x..xxxx-x.....:v7f7f7f7f7f7f7f48447f7f21404066690401427f7f532c7f7f:c/1,1-2,1-0,2-1,2-2,2-3,2-1,3-2,3\\2,0-3,0-3,1;]",
        "brule[s4:s4x4:open:f......2..30.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......2..30.....:e......x-...-xx..x-..........x-x.x-x.....:v7f7f7f7f7f7f5c233a7f690208447f690401427f7f532c7f7f:c/0,2\\1,3/1,0-2,0-3,0-1,1-3,1\\2,1-1,2-2,2-3,2-2,3;]",
        "brule[s4:s4x4:open:f......0..01.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..01.....:e.......xx..xx.............x..xx..x......:v7f7f7f7f7f7f7f48447f7f4840427f7f4142357f7f7f7f7f7f:c/2,0-1,1-2,1-3,1-0,2-1,2-2,2-1,3/2,3\\3,2;]",
        "brule[s4:s4x4:open:f......0..11.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..11.....:e.......xx...x.............x..xx.........:v7f7f7f7f7f7f7f48447f7f6940427f7f3a53357f7f7f7f7f7f:c/1,1-2,1-0,2-1,2-2,2-1,3/2,0\\3,1/2,3\\3,2;]",
        "brule[s4:s2x2:open:f.131:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f.131:e............:v7f7d7e3739563e3977:c;]",
        "brule[s4:s4x4:open:f......0..12.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..12.....:e.......xx...x-....x.......x..xx-..-x....:v7f7f7f7f7f7f7f48447f7f694002197f3a1208667f7f7f537f:c/1,1-2,1-0,2-1,2-2,2-1,3\\3,2-2,3-3,3/2,0\\3,1;]",
        "brule[s4:s4x4:open:f......0..32.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..32.....:e.......xx...-.............x..-x.........:v7f7f7f7f7f7f7f48447f7f1601427f7f3a2c357f7f7f7f7f7f:c/1,1-0,2-1,2-2,2-1,3\\2,1/2,0\\3,1/2,3\\3,2;]",
        "brule[s4:s2x2:open:f.112:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f.112:e............:v7f7d7e7d7a737b767f:c;]",
        "brule[s4:s2x2:open:f.232:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f.232:e............:v7f7f3b373b3f3e3d7f:c;]",
        "brule[s4:s4x4:open:f......3..32.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......3..32.....:e...x....-..-....xx........-x...xx-......:v7f7f7f5c7f7f7d1601667f23322866690418777f7f53537f7f:c/0,2-0,3-1,3-2,3\\1,2/1,1\\2,2/2,0-3,0-3,1-3,2\\2,1;]",
        "brule[s4:s4x4:open:f......0..23.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......0..23.....:e.......xx..x--...xx.......x..-x-.x-x....:v7f7f7f7f7f7f7f48447f7f140102197f410408667f7f53537f:c/1,1-0,2-1,2-2,2-1,3\\2,0-2,1-3,1-3,2-2,3-3,3;]",
        "brule[s4:s4x4:open:f......1..13.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......1..13.....:e........x..x..............x......x......:v7f7f7f7f7f7f7f69447f7f5c32137f7f41263d7f7f7f7f7f7f:c/0,2-1,2-1,3/1,1\\2,2/2,0-2,1-3,1;]",
        "brule[s4:s2x2:open:f.123:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f.123:e............:v7f7d7e7f33337b3e3d:c;]",
        "brule[s4:s4x4:open:f......2..33.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f......2..33.....:e.......x...---...x........-....x........:v7f7f7f7f7f7f7f14197f7f230321667f2c0c2c7f7f7f537f7f:c/3,1-0,2-3,2\\1,1-1,2/2,1-1,3-2,3\\2,0-2,2;]",
        "brule[s4:s2x2:open:f....:e............:v7f7f7f7f4a7f7f7f7f:c;s4:s2x2:open:f....:e............:v7f7f7f7f4a7f7f7f7f:c/1,0-0,1;]",
        "brule[s4:s2x1:open:f..:e.......:v7f7f7f7f2c7f:c;s4:s2x1:open:f..:e.-.....:v7f237f7f2c7f:c/0,0\\0,1;]",
        "brule[s4:s2x2:open:f1...:e............:v7f7f7f7f4d7f7f7f7f:c;s4:s2x2:open:f1...:e............:v7d7e7f7b457f7f7f7f:c/0,0-1,1;]",
        "brule[s4:s2x2:open:f1...:e............:v7f7f7f7f2b7f7f7f7f:c;s4:s2x2:open:f1...:e....-.......:v7d7e7f7b237f7f2c7f:c/1,0\\1,1;]",
        "brule[s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f7f7f7f7f7f637f7f7f7f7f:c;s4:s3x3:open:f....3....:e.x...-....-....x-...x...:v7f5c7f7f6902197f7f2c21667f7b2c7f:c/0,0-1,0-0,1\\1,1/1,2-2,2\\2,1;]",
        "brule[s4:s3x3:open:f....3....:e........................:v7f7f7f7f7f7f7f7f7f7f477f7f7f7f7f:c;s4:s3x3:open:f....3....:e.x...-.........x-.......:v7f5c7f7f6902197f7f2c057f7f7f7f7f:c/0,0-1,0-0,1\\1,1-2,2/1,2\\2,1;]",
        "brule[s4:s3x3:open:f....1....:e......x.........x..x....:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s3x3:open:f....1....:e.....-x.........x..x....:v7f7f7f7f7f21447f7f28427f7f7f7f7f:c/0,1-1,1-2,1-1,2\\1,0;]",
        "brule[s4:s4x4:open:f.....01..32.....:e........................................:v7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f7f:c;s4:s4x4:open:f.....01..32.....:e..x...xxx..--x..xx.......xx.-x--x-x.....:v7f7f5c7f7f7f4840447f1601021019690408427f7f53537f7f:c/1,0-0,1-1,1-2,1-0,2-1,2-1,3\\2,0-3,0-3,1-2,2-3,2-2,3;]",
        "brule[s4:s2x2:open:f2222:e............:v7f7f7f7f7f7f7f7f7f:c;s4:s2x2:open:f2222:e............:v7f7f7f7f3f7f7f7f7f:c;]",
        "brule[s4:s1x1:open:f.:e....:v7f7f6d5b:c;s4:s1x1:open:f.:e....:v7f7e6d5b:c;]",
        "brule[s4:s1x1:open:f.:e....:v7f67577f:c;s4:s1x1:open:f.:e....:v7d67577f:c;]",
        "brule[s4:s3x3:open:f....2....:e........................:v7f7f7f7f7f7f7f7f7f7f4a7f7f7f7f7f:c;s4:s3x3:open:f....2....:e........................:v7f7f7f7f7f4a3a7f7f3a4a7f7f7f7f7f:c/1,0-0,1\\2,1-1,2;]",
        "brule[s4:s1x1:open:f.:e....:v7f673e77:c;s4:s1x1:open:f.:e....:v6b673e77:c;]",
        "brule[s4:s3x2:open:f.3....:e.................:v7f7f7f7f7f353a7f7f7f7f7f:c;s4:s3x2:open:f.3....:e.................:v7f373b7f7f34387f7f7b777f:c/0,1\\1,0-1,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f3a7f7f3a357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f353a7f7f3a357f7f7f7f7f:c/0,1-2,1\\1,0-1,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f397f7f047f7f7f7f7f7f:c;s4:s3x3:open:f.........:e.....-...x........x-....:v7f7d7f7f7f23387f6904197f7f537f7f:c/0,1\\1,2/1,0-2,0-2,1\\1,1;]",
        "brule[s4:s3x2:open:f......:e.................:v7f7f797f7f047e7f7f7f7f7f:c;s4:s3x2:open:f......:e.-...xx....x-....:v7f23787e6904187f7f53537f:c/0,0-1,0-1,1-1,2\\0,1;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f4a7f7f3a357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7d4a7f7f3a357f7f7f7f7f:c/1,1-0,2/1,0-1,2\\2,1;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f457f7f3a357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f4a457f7f3a357f7f7f7f7f:c/1,0-0,1-1,2\\2,1;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f3a7f7f3a4a7f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f4a3a7f7f3a4a7f7f7f7f7f:c/1,0-0,1\\2,1-1,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f3a7f7f3a457f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f373a7f7f3a457f7f7f7f7f:c/0,1\\1,2/1,0\\2,1/1,1-2,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f4a7f7f4a357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7d4a7f7f4a357f7f7f7f7f:c/2,0-1,1-0,2/1,2\\2,1;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f4a7f7f45357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f6b4a7f7f45357f7f7f7f7f:c/1,1-0,2/1,0-2,1\\1,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f457f7f45357f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f35457f7f45357f7f7f7f7f:c/0,1-1,2\\1,0-2,1;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f4a7f7f3a4a7f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f6b4a7f7f3a4a7f7f7f7f7f:c/1,1-0,2/1,0\\2,1-1,2;]",
        "brule[s4:s3x3:open:f.........:e........................:v7f7f7f7f7f7f457f7f3a4a7f7f7f7f7f:c;s4:s3x3:open:f.........:e........................:v7f7f7f7f7f35457f7f3a4a7f7f7f7f7f:c/0,1-2,1-1,2\\1,0;]"
        ).map( (str) => BasicRule.deserialize( str ) )

    def attach( grid: Grid4, parent: Option[Grid4] = None ): StaticRuleSolver = {
        new StaticRuleSolver( grid )
    }
}

class StaticRuleSolver( grid: Grid4 ) extends StaticSolver {
    def solve(): Unit = {
        if ( hasRun ) return
        BasicRule.applyRulesTo( grid, StaticRuleSolver.SimpleRules )
        hasRun = true
    }

    def attachToChild( child: Grid4, parent: Grid4 ): StaticRuleSolver = {
        val solver = StaticRuleSolver.attach( child, Some( parent ) )
        solver.hasRun = this.hasRun
        solver
    }
}