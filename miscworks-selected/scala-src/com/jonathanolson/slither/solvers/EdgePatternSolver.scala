package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import java.io.{BufferedReader, InputStreamReader}
import scala.collection.mutable.ListBuffer
import com.jonathanolson.slither.util.{Direction, LogHelper, Spot}
import com.jonathanolson.slither.model.{EdgeStateListener, Grid4, Edge4}
import com.jonathanolson.slither.patterns.{BasicRule, EdgePattern, GridPattern, FacePattern}

object EdgePatternSolver extends LogHelper {
    private lazy val patterns: ListBuffer[BasicRule] = {
        val instream = this.getClass.getResourceAsStream( "/solving_rules_4.txt" )
        //        println( instream )
        val reader = new BufferedReader( new InputStreamReader( instream ) )
        var done = false
        val patterns = new ListBuffer[BasicRule]
        var pattern = new BasicRule
        var ruleMode = true
        val LocationRegex = """([^,]+),([^,]+),([^,]+)""".r
        while (!done) {
            val line = reader.readLine
            if ( line == null ) done = true else {
                //                println( line )
                if ( !line.startsWith( "#" ) && (line.trim.length > 0) ) {
                    if ( line.startsWith( "required:" ) ) {
                        ruleMode = true
                        if ( !pattern.isEmpty ) {
                            patterns.append( pattern )
                            pattern = new BasicRule
                        }
                    } else if ( line.startsWith( "implied:" ) ) {
                        ruleMode = false
                    } else {
                        var LocationRegex( colString, rowString, valueString ) = line
                        var col: Int = colString.toInt
                        var row: Int = rowString.toInt
                        var value: Int = valueString.toInt
                        if ( (col % 2 == 0) && (row % 2 == 0) ) {
                            // face
                            row /= 2
                            col /= 2
                            //                            logger.debug( "found face rule: row " + row + ", col " + col + ", " + value )
                            assert( ruleMode )
                            pattern.patterns.append( new FacePattern( row, col, value ) )
                        } else {
                            // edge
                            val state: EdgeState = if ( value == 1 ) Black else Red
                            if ( col % 2 == 0 ) {
                                // horizontal edge
                                col /= 2
                                row = (row + 1) / 2
                                //                                logger.debug( "found horizontal edge: upper is row " + row + ", col " + col + " " + state )

                                (if ( ruleMode ) pattern.patterns else pattern.results).append(
                                    new EdgePattern( row, col, false, state )
                                    )
                            } else {
                                // vertical edge
                                col = (col + 1) / 2
                                row /= 2
                                //                                logger.debug( "found vertical edge: upper is row " + row + ", col " + col + " " + state )
                                (if ( ruleMode ) pattern.patterns else pattern.results).append(
                                    new EdgePattern( row, col, true, state )
                                    )
                            }
                        }
                    }
                }
            }
        }
        if ( !pattern.isEmpty ) {
            patterns.append( pattern )
            pattern = new BasicRule
        }
        logger.info( "found " + patterns.size + " patterns" )
        patterns
    }

    def applyPatternsEverywhere( grid: Grid4 ) {
        logger.debug( "applying patterns" )
        for (
            vertex <- grid.vertexIterator; // (rows + 1) * (cols + 1)
            dir <- Direction.primaries; // * 4
            spot = new Spot( grid, vertex, dir );
            pattern <- patterns; // * 129
            reversed <- Set( true, false ) // * 2
        ) {
            try {
                if ( pattern.matches( spot, reversed ) ) {
                    pattern.apply( spot, reversed )
                }
            } catch {
                case e: Exception => {
                    logger.error( patterns.findIndexOf( _ == pattern ), e )
                }
            }
        }
        logger.debug( "finished patterns" )
    }
}

object StaticEdgePatternSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): StaticEdgePatternSolver = {
        new StaticEdgePatternSolver( grid )
    }
}

class StaticEdgePatternSolver( grid: Grid4 ) extends StaticSolver {
    grid.addEdgeStateListener( new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = hasRun = false
    } )
    def solve(): Unit = {
        if ( hasRun ) return
        EdgePatternSolver.applyPatternsEverywhere( grid )
        hasRun = true
    }

    def attachToChild( child: Grid4, parent: Grid4 ): StaticEdgePatternSolver = {
        val solver = StaticEdgePatternSolver.attach( child, Some( parent ) )
        solver.hasRun = this.hasRun
        solver
    }
}
