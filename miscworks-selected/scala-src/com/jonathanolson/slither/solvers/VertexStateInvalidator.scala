package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.Direction
import com.jonathanolson.slither.model.{Edge4, EdgeStateListener, Grid4, VertexState, Vertex4, VertexStateListener, VertexStateGrid4}

object VertexStateInvalidator {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): VertexStateInvalidator = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            new VertexStateInvalidator( grid.asInstanceOf[VertexStateGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class VertexStateInvalidator( val grid: VertexStateGrid4 ) extends Solver {
    private var vertices = Set.empty[Vertex4]

    private val stateListener = new VertexStateListener with EdgeStateListener {
        override def onVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) = {
            vertices = vertices + vertex
            if ( isAuto ) {
                solve()
            }
        }

        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            vertices = vertices + edge.va
            vertices = vertices + edge.vb
            if ( isAuto ) {
                solve()
            }
        }
    }
    grid.addVertexStateListener( stateListener )
    grid.addEdgeStateListener( stateListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        VertexStateInvalidator.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeVertexStateListener( stateListener )
    }

    def solve() = {
        for (vertex <- vertices; state = grid.vertexState( vertex )) {
            vertices = vertices - vertex
            for (dir <- Direction.primaries; edgeState = grid.getState( vertex.getEdge( dir ) )) {
                edgeState match {
                    case Black => {
                        if ( !state.allowsBlack( dir ) ) throw new InvalidGridException
                    }
                    case Red => {
                        if ( !state.allowsRed( dir ) ) throw new InvalidGridException
                    }
                    case White => ()
                }
            }
        }
    }

    def isDirty() = !vertices.isEmpty
}