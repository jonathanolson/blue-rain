package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{ColoredGrid4, EdgeStateListener, Grid4, VertexStateGrid4, Edge4}

object SimpleFaceColorSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): SimpleFaceColorSolver = {
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            new SimpleFaceColorSolver( grid.asInstanceOf[ColoredGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class SimpleFaceColorSolver( val grid: ColoredGrid4 ) extends Solver {
    private var edges = Set.empty[Edge4]

    private val edgeStateListener: EdgeStateListener = new EdgeStateListener {
        override def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) = {
            edges = edges + edge
            if ( isAuto ) {
                solve()
            }
        }
    }
    grid.addEdgeStateListener( edgeStateListener )

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        SimpleFaceColorSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeEdgeStateListener( edgeStateListener )
    }

    def solve() = {
        for (edge <- edges) {
            edges = edges - edge
            grid.state( edge ) match {
                case Red => {
                    grid.setColorsSame( grid.getColorOf( edge.fa ), grid.getColorOf( edge.fb ) )
                }
                case Black => {
                    grid.setColorsOpposite( grid.getColorOf( edge.fa ), grid.getColorOf( edge.fb ) )
                }
                case _ => ()
            }
        }
    }

    def isDirty() = !edges.isEmpty
}