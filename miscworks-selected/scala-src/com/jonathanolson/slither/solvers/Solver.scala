package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.model.Grid4

object Solver {
    // patterns
    // TODO: add in "fundamental" rules
    // TODO: actual pattern database (like postgresql?)
    // TODO: GUI: zones for highlights instead of direct hits?

    // solvers
    // TODO: multi-step backtracker repeats a lot of tests. need ordering of edges?
    // TODO: backtracker needs edge support in general. edge order is very important? (check) (YES) - when going back to 2-depth, choose edges carefully
    // TODO: backtracker 2-depth or more should try edges near each other first, then farther away edges
    // TODO: backtrack by extending paths
    // TODO: colors => vertex state
    // TODO: vertex state => colors
    // TODO: create pattern DB
    // TODO: breadth-first testing and comparison
    // TODO: highlander
    // TODO: find out how to detect highlander patterns

    // TODO: what about picking a bunch of different initial edges multiple times, and record which edges are most likely to appear

    // GUI
    // TODO: editor needs to edit face colors and vertex states

    // other
    // TODO: MED-HIGH copying of face colors in copyStateTo
    // TODO: make solving multi-threaded?
    // TODO: optimizations to maybe use specific mutable sets instead of reassigning immutable ones?
    // TODO: make a check of collections performance

    // threads
    // solving techniques http://www.kwontomloop.com/forum.php?a=topic&topic_id=304&pg=1
    // v_e_e_n_c_a's solver http://www.kwontomloop.com/forum.php?a=topic&topic_id=357

    // puzzles

    // 1s and 2s very hard: (missing 2-depth patterns for this?)
    // 20x14 222.222...2.22.2..2...1...2.222.22.2121.22..221.12.212.22.22.121.2.2.1..2222.122.2.1.112..22122...2.21212.2.22.2.2.2.211...2..212..1.2.1.2.2111..2..2.22......2.2.21.2.21.2.2211212....122..2..2..2.1..2..22.1.222222.1222.2222.122.11..21...2..2.....21.2.122222.2.22.2222.222.2.2.1.1.
    // progress so far: 20x14 !269224200.80200220200.00.00.20200.0a200221.a4200.00.80200.00.42.00109.25.04.00200.00208200218.d2248200.20280109224100.00200200.02.00200200100.00103214.432a511e240.00228202.08200200.00109284100.00208.00200.00100.00.30242281214242.00102200200.84212.60108.0010310c200.00.00200200101224200.00.00.00200.1026910521414b284.00202.00200200.00200.00200.00200.0020010110c.42.18.c0212.61.04200108200.00.00100.00200.00100.00200.0020210918712d.0c.81284.00.92248.00200200.00.00.00.00.00.00200.00212.782c3106.a12a5.0c221106.00200.00200200100100200100200.00.40.03.241092a523c.d2.48200.00.00200.00.00218.40108.00.00200.00.00201296.7814b.25296248200200200200.00102200202200.00210200200200.2111e242200.29106100.00.08200108.10.40.00200.00.00240.00.00.00.02.00281106.00200.0010321425a241214248.00210.40200280.00200200200220.00200280200.80200.02218.c0103.04101.04

    // really big
    // 64x50:b2a23b3c3a32b33a3d2a3a3a33a23b22a332a3b323c2212a1a313c3c1a3d0e1a1c23121e3b2b12b2a21a2c1e1b1d13a32c311b1a0a0c12a21a2b2b11a3b3a2c2g2a231a2b3a2a02b2d2a2d3b2c111a2c323f1c3b2b33a11b220a1a3a2c333a322b21211g2a2a0e11a22a31b31e2b3a3f1a2c2a22a22c10c22f23d220b2a2b2a1c2a32a3a2a3a2b2c21a21b202a12a1a1b3a011a1b2d10a1211d2a21b1b3e1312d23c2d1a3a2211a1a3e3b2b2b13a131a12a3b20a021d2b221a1b211b21d2f2a0c1b22a2e2j3a2d212c32a2b202i2a13b33a2d1a222a12d2a3a3d1a2b33d3b1b32a11a2a22b3a2a0a2b31a2a2312211321a123a22c331222b2c11a2a22b33e30c2a3b2b2c2a12b1f1b221g3c31f2b2222a1c23c2a2b33d2b1a2a312a3c12a212a213f2c11a2b32b12c1a2b1222a22a1c1201d31a3a223a11a2b20a2a03a3a3b2c1c3c11a1c1a21a3a32c1a02a2g31a1a322a1a3a2h2a2a2b02212223a3b32c22a1c23a22a2a12c3e1b2a32213b23a2a3b333h31a1a231b13b1a321b3a2a11b1c131a12a1e3222d1c01a111d3b32b2a1a2a22b223c32a2d2c2c13b1e3b13e3b01b31a1a1a01b3232b22e1f2a2a322b2c1313b1d3a223e3c13c2212a3a3a12b33a22a231a122c21c1d2b210b0d1b22a1e232b2d231b230a1a33b1a12a23a10d2b312b2c33c2b1a2a122a22c3b1c0d2b1d2a13c2b320b22c12a10a2b2a32a3d0a3c13a3b2a231a1b3i23c32b1c2c2232a2b12a212c2b2b1d3d2212b13b21d2a3a2g221e32g3a2c2b2a121a222e3a3j11c2a331a1e1d32b2d203c322b2f22c2a33a2a3a1a32b2d3a2a1a1b3a1c221a02a2b112e3b12a2a2a2d1211a2d3a213c23a1f321b33a2b21121a13a1b1a3b2b21a3e2b2a11232b22a3b2a20a1a2a31d0a2a21a1c21201312d22a2c1a22c23d1c23b12a2a2b23e2a22a2b32c221b202a1a1311321331b22e2a21b1a22b2e33a12d31a2b2d3a0a2a2c1g3b22312c23a3c2d22e3a1a2a1c2b1b2b3122c21a2a2c1b1a3a32c1c0a2b23213a102b2a2222h1b23a3223b1a2b3a2b2e2c3a1a0a02d1b1b32b0a13a2a2b122g0b2d2a2a2a3a3b2a1a111c2a12a3a2a2a1b3a2b10a1c1a1a2e02a32a31a1d1c2b3a2a11a12b1a232b3a3b0a2a2a222323a321b23d21a121a3a33a01f3a31f3a1a2b2c1a2a1b232b2b2d2a23a213f3b1b2c2h23a3a2b321a223a3b1d21223b2a1a3d223b1a3a1111a2a2a232a3c1a13a1c1212b21c2a1a2a3b2b1a1a1c2d2b1a1122313a1b2c32c3d2a2d23f21d12b22312b323a3b2a2e321b322b2b1111b2222c121222b2b3b1e322c1d3b1b022a21b2a2b3a3a3a12a1212a1a2b122b3f1e2a3b3f13c1e12a2f32a1a2213c222a2222a2a3b3a3d0b3a3a3a221b113a30c31b20a1a21b3e2e1221c02a0a2a12b2f22a1f23b1222a3a322a3c3a221g1g3e1a2a21021b2a2b3a11a222d2a22b1312a3a3a321a2c2b2a11g3h23a1a2a2i12a12a1d1b1c322a1b3a3a2b3b2a3a2c1a1c22b331c1a2a2b1a22c2a3a3a3c2a21d21b2a2a3d2b23a3a3a3a2c3a3a3a33b3b3b33b3a22b3a233233d31b3b333a23222a2b

    def standardSolverStack( grid: Grid4 ): SolverStack = {
        val stack = new SolverStack( grid )
        stack.solvers += StaticFaceSolver.attach( grid )
        stack.solvers += StaticFaceVertexStateSolver.attach( grid )
        stack.solvers += SimpleFaceSolver.attach( grid )
        stack.solvers += SimpleVertexSolver.attach( grid )
        stack.solvers += SimpleVertexStateSolver.attach( grid )
        stack.solvers += VertexStateSolver.attach( grid )
        stack.solvers += FaceVertexStateSolver.attach( grid )
        stack.solvers += SimpleFaceColorSolver.attach( grid )
        stack.solvers += FaceColorEdgeSolver.attach( grid )
        stack.solvers += MediumFaceColorSolver.attach( grid )
        if ( grid.openBorders ) {
            stack.solvers += LoopInvalidator.attach( grid )
        }
        return stack
    }
}

trait Solver {
    protected var auto: Boolean = false
    protected var cpy: Boolean = true

    def attachToChild( child: Grid4, parent: Grid4 ): Solver // TODO: make more generic? evaluate later

    def detach(): Unit

    def isDirty(): Boolean // does this need to be processed again?

    def solve(): Unit // actually do the solving

    def isAuto: Boolean = auto

    def setAuto( auto: Boolean ) {
        this.auto = auto
    }

    def canCopy: Boolean = cpy

    def setCopy( copy: Boolean ) {
        this.cpy = copy
    }

}