package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.Direction
import com.jonathanolson.slither.model.{Grid4, Vertex4, VertexStateGrid4}

object StaticFaceVertexStateSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): StaticFaceVertexStateSolver = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            new StaticFaceVertexStateSolver( grid.asInstanceOf[VertexStateGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class StaticFaceVertexStateSolver( grid: VertexStateGrid4 ) extends StaticSolver {
    def attachToChild( child: Grid4, parent: Grid4 ): StaticFaceVertexStateSolver = {
        if ( child.isInstanceOf[VertexStateGrid4] ) {
            val solver = StaticFaceVertexStateSolver.attach( child.asInstanceOf[VertexStateGrid4], Some( parent ) )
            solver.hasRun = this.hasRun
            solver
        } else throw new RuntimeException( "bad grid type for solver" )
    }

    def solve(): Unit = {
        if ( hasRun ) return
        for (face <- grid.faceIterator) {
            grid.value( face ) match {
                case None => ()
                case Some( 0 ) => {
                    for (dir <- Direction.secondaries) {
                        val vertex: Vertex4 = face.getVertex( dir )
                        grid.vertexState( vertex ) = grid.vertexState( vertex ).withoutLines.withOnlyWrap( dir )
                    }
                }
                case Some( 1 ) => {
                    for (dir <- Direction.secondaries) {
                        val vertex: Vertex4 = face.getVertex( dir )
                        grid.vertexState( vertex ) = grid.vertexState( vertex ).withoutWrap( dir.opposite )
                    }
                }
                case Some( 2 ) => ()
                case Some( 3 ) => {
                    for (dir <- Direction.secondaries) {
                        val vertex: Vertex4 = face.getVertex( dir )
                        grid.vertexState( vertex ) = grid.vertexState( vertex ).withoutWrap( dir ).withoutEmpty
                    }
                }
            }
        }
        hasRun = true
    }
}