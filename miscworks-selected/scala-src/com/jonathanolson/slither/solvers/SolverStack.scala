package com.jonathanolson.slither.solvers

import scala.collection.mutable.ListBuffer
import com.jonathanolson.slither.model.Grid4

class SolverStack( val grid: Grid4 ) {
    val solvers = new ListBuffer[Solver]

    def solve(): Unit = {
        var dirty: Option[Solver] = null
        while ({dirty = solvers.find( _.isDirty ); dirty.isDefined}) {
            dirty.get.solve()
        }
    }

    def forChild( child: Grid4 ): SolverStack = {
        val stack = new SolverStack( child )
        for (solver <- solvers; if solver.canCopy) {
            stack.solvers += solver.attachToChild( child, grid )
        }
        stack
    }
}