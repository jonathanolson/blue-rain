package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.Direction
import com.jonathanolson.slither.model.{Grid4, Face4, VertexState, Vertex4, VertexStateListener, VertexStateGrid4}

object FaceVertexStateSolver {
    def attach( grid: Grid4, parent: Option[Grid4] = None ): FaceVertexStateSolver = {
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            new FaceVertexStateSolver( grid.asInstanceOf[VertexStateGrid4] )
        } else throw new RuntimeException( "bad grid type for solver" )
    }
}

class FaceVertexStateSolver( val grid: VertexStateGrid4 ) extends Solver {
    private var vertices = Set.empty[Vertex4]

    // TODO: more involved patterns?
    private val stateListener: VertexStateListener = new VertexStateListener {
        override def onVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) = {
            vertices += vertex
            if ( isAuto ) solve()
        }
    }
    grid.addVertexStateListener( stateListener )

    def processTwoFace( face: Face4 ) {
        // TODO: optimize?
        for (dir <- Direction.secondaries) {
            val vertex = face.getVertex( dir )
            val state = grid.vertexState( vertex )
            if ( state.isBlocked( dir ) ) {
                grid.vertexState( face.getVertex( dir.opposite ) ) = grid.vertexState( face.getVertex( dir.opposite ) ).withBlocked( dir.opposite )
                grid.vertexState( face.getVertex( dir.right ) ) = grid.vertexState( face.getVertex( dir.right ) ).withIncident( dir.right )
                grid.vertexState( face.getVertex( dir.left ) ) = grid.vertexState( face.getVertex( dir.left ) ).withIncident( dir.left )
            } else if ( state.isIncident( dir ) ) {
                grid.vertexState( face.getVertex( dir.opposite ) ) = grid.vertexState( face.getVertex( dir.opposite ) ).withIncident( dir.opposite )
            }
        }
    }

    def attachToChild( child: Grid4, parent: Grid4 ) = {
        FaceVertexStateSolver.attach( child, Some( parent ) )
    }

    def detach() = {
        grid.removeVertexStateListener( stateListener )
    }

    def solve() = {
        for (vertex <- vertices) {
            vertices -= vertex

            val newState = grid.vertexState( vertex )

            for (dir <- Direction.secondaries; faceOpt = vertex.getFace( dir ); if faceOpt.isDefined) {
                val face = faceOpt.get
                grid.value( face ) match {
                    case None => () // TODO: possibly handle cases which would cause a single square
                    case Some( 0 ) => ()
                    case Some( 1 ) => {
                        if ( newState.isIncident( dir ) ) {
                            grid.move( face.getEdge( dir.halfLeft ) -> Red )
                            grid.move( face.getEdge( dir.halfRight ) -> Red )
                        } else if ( newState.isBlocked( dir ) ) {
                            grid.move( face.getEdge( dir.opposite.halfLeft ) -> Red )
                            grid.move( face.getEdge( dir.opposite.halfRight ) -> Red )
                        }
                    }
                    case Some( 2 ) => {
                        processTwoFace( face )
                    }
                    case Some( 3 ) => {
                        if ( newState.isIncident( dir ) ) {
                            grid.move( face.getEdge( dir.halfLeft ) -> Black )
                            grid.move( face.getEdge( dir.halfRight ) -> Black )
                        } else if ( newState.isBlocked( dir ) ) {
                            grid.move( face.getEdge( dir.opposite.halfLeft ) -> Black )
                            grid.move( face.getEdge( dir.opposite.halfRight ) -> Black )
                        }
                    }
                }
            }
        }
    }

    def isDirty() = !vertices.isEmpty
}