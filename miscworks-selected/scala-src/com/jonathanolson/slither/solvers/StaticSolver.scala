package com.jonathanolson.slither.solvers

import com.jonathanolson.slither.model.Grid4

trait StaticSolver extends Solver {
    var hasRun = false

    def isDirty() = !hasRun

    def detach() = {}
}