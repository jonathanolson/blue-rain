package com.jonathanolson.slither.exceptions

class SpotException( msg: String ) extends Exception( msg )