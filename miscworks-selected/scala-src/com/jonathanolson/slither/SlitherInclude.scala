package com.jonathanolson.slither

import com.jonathanolson.slither.util.Direction
import com.jonathanolson.slither.model.{FaceColor, Edge4, EdgeState}

object SlitherInclude {
    val North = Direction.North
    val East = Direction.East
    val South = Direction.South
    val West = Direction.West
    val Northwest = Direction.Northwest
    val Northeast = Direction.Northeast
    val Southeast = Direction.Southeast
    val Southwest = Direction.Southwest

    type EdgeState = EdgeState.EdgeState

    val White = EdgeState.White
    val Black = EdgeState.Black
    val Red = EdgeState.Red

    val Inside = FaceColor.Inside
    val Outside = FaceColor.Outside

    implicit def toOptionMoveTuple( kv: (Edge4, EdgeState) ): (Option[Edge4], EdgeState) = (Some( kv._1 ), kv._2)
}