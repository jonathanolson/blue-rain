package com.jonathanolson.slither.util

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.model.{Face4, Edge4, Vertex4, Grid4}
import com.jonathanolson.slither.exceptions.SpotException

class Spot(
        val grid: Grid4,
        val vertex: Vertex4,
        val dir: Direction
        ) {
    override def toString = vertex + " -> " + dir

    def getEdge( relativeDir: Direction ): Option[Edge4] = {
        require( relativeDir.isPrimary )
        (dir + relativeDir) match {
            case North => vertex.northEdge
            case East => vertex.eastEdge
            case South => vertex.southEdge
            case West => vertex.westEdge
        }
    }

    def getVertex( relativeDir: Direction ): Option[Vertex4] = {
        require( relativeDir.isPrimary )
        getEdge( relativeDir ) match {
            case Some( edge ) => Some( edge.otherVertex( vertex ) )
            case None => None
        }
    }

    def getFace( relativeDir: Direction ): Option[Face4] = {
        require( !relativeDir.isPrimary )
        vertex.getFace( relativeDir + dir )
    }

    def hasDirection( relativeDir: Direction ): Boolean = {
        val absoluteDir = (relativeDir + dir)
        def testAbsolute( d: Direction ) = vertex.getEdge( d ).isDefined
        if ( absoluteDir.isPrimary ) {
            return testAbsolute( absoluteDir )
        } else {
            val (a, b) = absoluteDir.components
            return testAbsolute( a ) && testAbsolute( b )
        }
    }

    def northEdge = getEdge( North ).get

    def eastEdge = getEdge( East ).get

    def southEdge = getEdge( South ).get

    def westEdge = getEdge( West ).get

    def northVertex = getVertex( North ).get

    def eastVertex = getVertex( East ).get

    def southVertex = getVertex( South ).get

    def westVertex = getVertex( West ).get

    def northwestFace = getFace( Northwest ).get

    def northeastFace = getFace( Northeast ).get

    def southeastFace = getFace( Southeast ).get

    def southwestFace = getFace( Southwest ).get

    def turnLeft = new Spot( grid, vertex, dir.left )

    def turnRight = new Spot( grid, vertex, dir.right )

    def turnBack = new Spot( grid, vertex, dir.back )

    def forward = {
        getVertex( North ) match {
            case Some( newVertex ) => new Spot( grid, newVertex, dir )
            case None => throw new SpotException( "Could not move forward (" + dir + ")" )
        }
    }

    def shiftLeft = turnLeft.forward.turnRight

    def shiftRight = turnRight.forward.turnLeft

    def shiftBack = turnBack.forward.turnBack
}