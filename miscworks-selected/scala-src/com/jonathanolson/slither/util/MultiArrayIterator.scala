package com.jonathanolson.slither.util

class MultiArrayIterator[+T]( data: Array[Array[T]], rows: Int, cols: Int ) extends Iterator[T] {
    var row = 0
    var col = 0

    override def foreach[U]( f: (T) => U ) = {
        for (arr <- data) arr.foreach[U]( f )
    }

    def next() = {
        assert( row < rows )
        assert( col < cols )
        val ret = data( row )( col )
        if ( col == cols - 1 ) {
            col = 0
            row += 1
        } else {
            col += 1
        }
        ret
    }

    def hasNext = row < rows && col < cols

    override def size = rows * cols
}









