package com.jonathanolson.slither.util

object Direction {
    val North = new Direction( 0, true, "North" )
    val East = new Direction( 1, true, "East" )
    val South = new Direction( 2, true, "South" )
    val West = new Direction( 3, true, "West" )
    val Northwest = new Direction( 4, false, "Northwest" )
    val Northeast = new Direction( 5, false, "Northeast" )
    val Southeast = new Direction( 6, false, "Southeast" )
    val Southwest = new Direction( 7, false, "Southwest" )

    val primaries = List( North, East, South, West )
    val secondaries = List( Northwest, Northeast, Southeast, Southwest )
}

class Direction( val id: Int, val primary: Boolean, val name: String ) {
    import Direction._
    override def equals( p1: Any ) = {
        if ( p1.isInstanceOf[Direction] ) {
            id == p1.asInstanceOf[Direction].id
        } else {
            false
        }
    }

    override def hashCode = id

    override def toString = name

    val isPrimary = primary

    def right = this match {
        case North => East
        case East => South
        case South => West
        case West => North
        case Northeast => Southeast
        case Southeast => Southwest
        case Southwest => Northwest
        case Northwest => Northeast
    }

    def left = this match {
        case North => West
        case East => North
        case South => East
        case West => South
        case Northeast => Northwest
        case Southeast => Northeast
        case Southwest => Southeast
        case Northwest => Southwest
    }

    def back = this match {
        case North => South
        case East => West
        case South => North
        case West => East
        case Northeast => Southwest
        case Southeast => Northwest
        case Southwest => Northeast
        case Northwest => Southeast
    }

    def opposite = back

    def components: (Direction, Direction) = {
        require( !isPrimary )
        this match {
            case Northeast => (North, East)
            case Southeast => (South, East)
            case Southwest => (South, West)
            case Northwest => (North, West)
        }
    }

    def halfRight = this match {
        case North => Northeast
        case East => Southeast
        case South => Southwest
        case West => Northwest
        case Northeast => East
        case Southeast => South
        case Southwest => West
        case Northwest => North
    }

    def halfLeft = this match {
        case North => Northwest
        case East => Northeast
        case South => Southeast
        case West => Southwest
        case Northeast => North
        case Southeast => East
        case Southwest => South
        case Northwest => West
    }

    def +( other: Direction ): Direction = (this, other) match {
        case (x, North) => x
        case (x, East) => x.right
        case (x, South) => x.back
        case (x, West) => x.left
        case (North, x) => x
        case (East, x) => x.right
        case (South, x) => x.back
        case (West, x) => x.left
        case (x, y) => x.halfLeft + y.halfRight // diagonal, split them
    }
}
