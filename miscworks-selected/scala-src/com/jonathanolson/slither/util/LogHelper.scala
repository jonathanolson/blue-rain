package com.jonathanolson.slither.util

import org.apache.log4j.Logger;

trait LogHelper {
    val loggerName = this.getClass.getName
    lazy val logger = Logger.getLogger( loggerName )

    def debug( message: => Object ): Unit = {
        if ( logger.isDebugEnabled ) {
            logger.debug( message )
        }
    }

    def info( message: => Object ): Unit = {
        if ( logger.isInfoEnabled ) {
            logger.info( message )
        }
    }
}
