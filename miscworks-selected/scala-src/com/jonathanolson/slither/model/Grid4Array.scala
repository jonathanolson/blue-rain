package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.{ListBuffer, HashMap => MHashMap, Map => MMap, Set => MSet}
import com.jonathanolson.slither.exceptions.InvalidGridException

trait Grid4Array extends Grid4 {
    private val gridlock = new Object // yes, the pun!

    private val states: Array[EdgeState] = if ( hasParent ) {
        parent.asInstanceOf[Grid4Array].states.clone()
    } else {
        new Array[EdgeState]( (rows * (cols + 1) + (rows + 1) * cols) ) // states
    }
    private val values: Array[Option[Int]] = if ( hasParent ) {
        parent.asInstanceOf[Grid4Array].values.clone()
    } else {
        new Array[Option[Int]]( rows * cols ) // values
    }

    if ( !hasParent ) {
        for (face <- faceIterator) {
            values( faceIdx( face ) ) = None
        }
        for (edge <- edgeIterator) {
            states( edgeIdx( edge ) ) = White
        }
        for (state <- states) assert( state == White )
        for (intOpt <- values) assert( intOpt == None )
    }

    def state: EdgeStateCheckable = new EdgeStateCheckable {
        def apply( edge: Edge4 ): EdgeState = gridlock.synchronized {states( edgeIdx( edge ) )}

        def update( edge: Edge4, state: EdgeState ) {
            // TODO: refactor into different place
            gridlock.synchronized {
                var oldState = apply( edge )
                if ( state != oldState ) {
                    if ( !allowsInvalid && (oldState != White) && (oldState != state) ) throw new InvalidGridException
                    oldState match {
                        case White => whiteCount -= 1
                        case Red => redCount -= 1
                        case Black => blackCount -= 1
                    }
                    state match {
                        case White => whiteCount += 1
                        case Red => redCount += 1
                        case Black => blackCount += 1
                    }
                    states( edgeIdx( edge ) ) = state
                    moves.append( new EdgeStateMove( edge, state ) )
                    notifyEdgeStateChange( edge, oldState, state )
                }
            }
        }

        def noneState = if ( openBorders ) White else Red
    }

    def value: FaceValueCheckable = new FaceValueCheckable {
        def apply( face: Face4 ): Option[Int] = values( faceIdx( face ) )

        def update( face: Face4, value: Option[Int] ) {
            value match {
                case Some( i ) => require( (i >= 0) && (i <= 4) )
                case None => ()
            }
            var oldValue = apply( face )
            values( faceIdx( face ) ) = value
            if ( value != oldValue ) {
                notifyFaceValueChange( face, oldValue, value )
            }
        }
    }
}