package com.jonathanolson.slither.model

class FaceColorMove(
        val color1: FaceColor,
        val color2: FaceColor,
        val same: Boolean
        ) extends Move {
    override def toString = color1 + "+" + color2 + ":" + same

    def apply( grid: Grid4 ) = if ( grid.isInstanceOf[ColoredGrid4] ) {
        if ( same ) {
            grid.asInstanceOf[ColoredGrid4].setColorsSame( color1, color2 )
        } else {
            grid.asInstanceOf[ColoredGrid4].setColorsOpposite( color1, color2 )
        }
    }
}