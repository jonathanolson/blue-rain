package com.jonathanolson.slither.model

object EdgeState extends Enumeration {
    type EdgeState = Value
    val White, Red, Black = Value
}