package com.jonathanolson.slither.model

import collection.mutable.ListBuffer
import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.{LogHelper, Direction}

class Vertex4(
        val row: Int,
        val col: Int
        ) extends LogHelper {
    var northEdge: Option[Edge4] = None
    var westEdge: Option[Edge4] = None
    var eastEdge: Option[Edge4] = None
    var southEdge: Option[Edge4] = None

    var northwestFace: Option[Face4] = None
    var northeastFace: Option[Face4] = None
    var southwestFace: Option[Face4] = None
    var southeastFace: Option[Face4] = None

    def edges: Set[Option[Edge4]] = Set( westEdge, northEdge, eastEdge, southEdge )

    def realEdges: Set[Edge4] = for (edgeOpt <- edges; if edgeOpt.isDefined) yield edgeOpt.get

    def faces: Set[Option[Face4]] = Set( northwestFace, northeastFace, southeastFace, southwestFace )

    def realFaces: Set[Face4] = for (faceOpt <- faces; if faceOpt.isDefined) yield faceOpt.get

    def getEdge( dir: Direction ): Option[Edge4] = dir match {
        case North => northEdge
        case West => westEdge
        case South => southEdge
        case East => eastEdge
        case _ => throw new RuntimeException( "bad dir " + dir + " for getEdge" )
    }

    def edge( dir: Direction ): Edge4 = getEdge( dir ).get

    def directionOfEdge( edge: Edge4 ): Direction = {
        require( edges.contains( Some( edge ) ) )
        if ( northEdge.isDefined && edge == northEdge.get ) return North
        if ( westEdge.isDefined && edge == westEdge.get ) return West
        if ( southEdge.isDefined && edge == southEdge.get ) return South
        if ( eastEdge.isDefined && edge == eastEdge.get ) return East
        throw new RuntimeException( "should never happen" )
    }

    def getFace( dir: Direction ): Option[Face4] = dir match {
        case Northwest => northwestFace
        case Northeast => northeastFace
        case Southeast => southeastFace
        case Southwest => southwestFace
        case _ => throw new RuntimeException( "bad dir " + dir + " for getFace" )
    }

    def face( dir: Direction ): Face4 = getFace( dir ).get

    def directionOfFace( face: Face4 ): Direction = {
        require( faces.contains( Some( face ) ) )
        if ( northwestFace.isDefined && face == northwestFace.get ) return Northwest
        if ( northeastFace.isDefined && face == northeastFace.get ) return Northeast
        if ( southeastFace.isDefined && face == southeastFace.get ) return Southeast
        if ( southwestFace.isDefined && face == southwestFace.get ) return Southwest
        throw new RuntimeException( "should never happen" )
    }

    def emptyDirections: List[Direction] = {
        val buf = new ListBuffer[Direction]
        if ( northEdge.isEmpty ) buf.append( North )
        if ( eastEdge.isEmpty ) buf.append( East )
        if ( southEdge.isEmpty ) buf.append( South )
        if ( westEdge.isEmpty ) buf.append( West )
        buf.toList
    }

    def getEdgeTo( other: Vertex4 ): Option[Edge4] = {
        realEdges.find( (e) => e.otherVertex( this ) == other )
    }

    def edgeTo( other: Vertex4 ): Edge4 = getEdgeTo( other ).get

    override def toString = "(" + row + "," + col + ")";

    def compareTo( other: Vertex4 ): Int = {
        val ret = new Integer( col ).compareTo( other.col )
        if ( ret != 0 ) return ret
        return new Integer( row ).compareTo( other.row )
    }

    def inGrid( grid: Grid4 ): Vertex4 = {
        grid.vertices( row )( col )
    }
}