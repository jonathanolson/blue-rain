package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._

class EdgeStateMove(
        val edge: Edge4,
        val state: EdgeState
        ) extends Move {
    override def toString = edge + ":" + state

    def apply( grid: Grid4 ) = {
        grid.state( edge ) = state
    }
}