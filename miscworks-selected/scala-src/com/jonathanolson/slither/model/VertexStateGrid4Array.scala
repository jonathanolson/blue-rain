package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._

trait VertexStateGrid4Array extends VertexStateGrid4 {
    private val vertexStates: Array[VertexState] = if ( hasParent && parent.isInstanceOf[VertexStateGrid4Array] ) {
        parent.asInstanceOf[VertexStateGrid4Array].vertexStates.clone()
    } else new Array[VertexState]( (rows + 1) * (cols + 1) )

    def vertexState: VertexStateCheckable = new VertexStateCheckable {
        def apply( vertex: Vertex4 ) = vertexStates( vertexIdx( vertex ) )

        def update( vertex: Vertex4, state: VertexState ) = {
            var oldState = apply( vertex )
            if ( state != oldState ) {
                vertexStates( vertexIdx( vertex ) ) = state
                moves.append( new VertexStateMove( vertex, state ) )
                notifyVertexStateChange( vertex, oldState, state )
            }
        }
    }

    if ( !hasParent ) {
        // TODO: optimize
        for (vertex <- vertexIterator) {
            vertexState( vertex ) = VertexState.All
        }
        if ( !openBorders ) {
            for (vertex <- vertexIterator; dir <- vertex.emptyDirections) {
                vertexState( vertex ) = vertexState( vertex ).withEdge( dir, Red )
            }
        }
    }
}