package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.HashMap

object VertexStateGrid4 {
    def empty( gridRows: Int, gridColumns: Int, gridOpenBorders: Boolean = false ) = new {
    val openBorders = gridOpenBorders
    val parentGrid = None
    val cols = gridColumns
    val rows = gridRows
    } with Grid4Array with VertexStateGrid4Array {
        def newChild: VertexStateGrid4 = VertexStateGrid4.child( this )
    }

    def child( grid: VertexStateGrid4 ): VertexStateGrid4 = {
        val c = new {
        val openBorders = grid.openBorders
        val parentGrid = Some( grid )
        val cols = grid.cols
        val rows = grid.rows
        } with Grid4Array with VertexStateGrid4Array {
            def newChild: VertexStateGrid4 = VertexStateGrid4.child( this )

            whiteCount = grid.whiteCount
            redCount = grid.redCount
            blackCount = grid.blackCount
        }
        c
    }

    def fromString( str: String, gridOpenBordersOverride: Boolean = false ) = {
        val (rows, cols) = Grid4.sizeFromString( str )
        val open = gridOpenBordersOverride || str.contains( ":open" )
        var grid = empty( rows, cols, open );
        Grid4.setState( grid, str )
        grid;
    }
}

trait VertexStateGrid4 extends Grid4 with VertexStateGrid4Like[VertexStateGrid4] {
    val parentGrid: Option[VertexStateGrid4]

    def newChild: VertexStateGrid4

    def vertexState: VertexStateCheckable

    override def parent: VertexStateGrid4 = parentGrid.get

    override def root: VertexStateGrid4 = if ( hasParent ) parent.root else this
}

trait VertexStateGrid4Like[+This <: VertexStateGrid4] extends Grid4Like[This] {
    self: This =>

    private var vertexStateListeners = Set.empty[VertexStateListener]

    override def copyStateTo( emptyGrid: Grid4 ): Unit = {
        super.copyStateTo( emptyGrid )
        if ( emptyGrid.isInstanceOf[VertexStateGrid4] ) {
            val g: VertexStateGrid4 = emptyGrid.asInstanceOf[VertexStateGrid4]
            for (vertex <- vertexIterator) {
                g.vertexState( g.vertices( vertex.row )( vertex.col ) ) = vertexState( vertex )
            }
        }
    }

    def addVertexStateListener( listener: VertexStateListener ) {
        vertexStateListeners = vertexStateListeners + listener;
    }

    def removeVertexStateListener( listener: VertexStateListener ) {
        vertexStateListeners = vertexStateListeners - listener;
    }

    def notifyVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) {
        vertexStateListeners.foreach( _.onVertexStateChange( vertex, oldState, newState ) )
    }

    override def compareTo( grid: Grid4 ): Int = {
        val superRet = super.compareTo( grid )
        if ( superRet != 0 ) return superRet
        if ( grid.isInstanceOf[VertexStateGrid4] ) {
            val g = grid.asInstanceOf[VertexStateGrid4]
            for (row <- 0 to rows; col <- 0 to cols) {
                val ret = vertexState( vertices( row )( col ) ).compareTo( g.vertexState( grid.vertices( row )( col ) ) )
                if ( ret != 0 ) return ret
            }
        }
        return 0 // defer to subclass
    }

    override protected def partSerialString: String = {
        val str = new StringBuilder( super.partSerialString )
        str.append( ":v" )
        for (vertex <- vertexIterator) {
            str.append( vertexState( vertex ).serialString )
        }
        return str.toString
    }

    def deserializeVertexPart( str: String ) {
        var idx = 0
        for (vertex <- vertexIterator) {
            vertexState( vertex ) = VertexState.deserialize( str.substring( idx, idx + 2 ) )
            idx += 2
        }
    }
}

trait VertexStateCheckable {
    def apply( vertex: Vertex4 ): VertexState

    def update( vertex: Vertex4, state: VertexState )
}

trait VertexStateListener {
    def onVertexStateChange( vertex: Vertex4, oldState: VertexState, newState: VertexState ) {

    }
}
