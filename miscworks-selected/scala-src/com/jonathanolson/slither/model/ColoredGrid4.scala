package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.exceptions.InvalidGridException
import scala.collection.Set
import scala.collection.mutable.{ArraySeq, ListBuffer, HashMap}

object ColoredGrid4 {
    def empty( gridRows: Int, gridColumns: Int, gridOpenBorders: Boolean = false ) = new {
    val openBorders = gridOpenBorders
    val parentGrid = None
    val cols = gridColumns
    val rows = gridRows
    } with Grid4Array with VertexStateGrid4Array with ColoredGrid4 {
        def newChild: ColoredGrid4 = ColoredGrid4.child( this )
    }

    def emptyFrom( grid: Grid4 ): ColoredGrid4 = {
        empty( grid.rows, grid.cols, grid.openBorders )
    }

    def child( grid: ColoredGrid4 ): ColoredGrid4 = {
        val c = new {
        val openBorders = grid.openBorders
        val parentGrid = Some( grid )
        val cols = grid.cols
        val rows = grid.rows
        } with Grid4Array with VertexStateGrid4Array with ColoredGrid4 {
            def newChild: ColoredGrid4 = ColoredGrid4.child( this )

            whiteCount = grid.whiteCount
            redCount = grid.redCount
            blackCount = grid.blackCount
        }
        c
    }

    def fromString( str: String, gridOpenBordersOverride: Boolean = false ) = {
        val (rows, cols) = Grid4.sizeFromString( str )
        val open = gridOpenBordersOverride || str.contains( ":open" )
        val grid = empty( rows, cols, open );
        Grid4.setState( grid, str )
        grid;
    }
}

trait ColoredGrid4 extends VertexStateGrid4 with ColoredGrid4Like[ColoredGrid4] {
    val parentGrid: Option[ColoredGrid4]

    def newChild: ColoredGrid4

    def colorOf( face: Face4 ): FaceColor // no option
    def getColorOf( faceOpt: Option[Face4] ): FaceColor // option
    def facesWithColor( color: FaceColor ): collection.Set[Face4] // faces with the specified color
    def colors: collection.Set[FaceColor] // all active colors
    def getOppositeColor( color: FaceColor ): Option[FaceColor] // get opposite if it exists
    def getLatestColor( color: FaceColor ): FaceColor // if old color (removed by combination) get latest

    def setColorsSame( a: FaceColor, b: FaceColor ): Unit // set two colors to be the same
    def setColorsOpposite( a: FaceColor, b: FaceColor ): Unit // set two colors to be the opposite

    override def parent: ColoredGrid4 = parentGrid.get

    override def root: ColoredGrid4 = if ( hasParent ) parent.root else this
}

trait ColoredGrid4Like[+This <: ColoredGrid4] extends VertexStateGrid4Like[This] {
    self: This =>

    private val faceLock = new Object

    private var colorSet = Set.empty[FaceColor]
    private val faceToColorMap = new HashMap[Face4, FaceColor]
    private val colorToFacesMap = new HashMap[FaceColor, Set[Face4]]
    private val colorOppositeMap = new HashMap[FaceColor, Option[FaceColor]]
    private val oldColorMap = new HashMap[FaceColor, FaceColor] // just one step!!

    //    private val vertexStates = new HashMap[Vertex4, VertexState]
    private var faceColorListeners = Set.empty[FaceColorListener]

    def colorOf( face: Face4 ): FaceColor = {
        faceLock.synchronized {
            faceToColorMap.get( face ) match {
                case Some( color ) => color
                case None => if ( hasParent ) {
                    val ret = parent.colorOf( face )
                    faceToColorMap( face ) = ret
                    ret
                } else throw new RuntimeException( "didn't have a color for " + face )
            }
        }
    }

    def getColorOf( faceOpt: Option[Face4] ): FaceColor = faceOpt match {
        case Some( face ) => colorOf( face )
        case None => if ( !openBorders ) FaceColor.Outside else FaceColor.Unknown
    }

    def getColorOpt( faceOpt: Option[Face4] ): Option[FaceColor] = faceOpt match {
        case Some( face ) => Some( colorOf( face ) )
        case None => None
    }

    def facesWithColor( color: FaceColor ): collection.Set[Face4] = {
        faceLock.synchronized {
            if ( color == FaceColor.Unknown ) return Set.empty[Face4]
            val latest = getLatestColor( color )
            colorToFacesMap.get( latest ) match {
                case Some( faceSet ) => faceSet
                case None => if ( hasParent ) {
                    val ret = parent.facesWithColor( latest )
                    colorToFacesMap( latest ) = ret
                    ret
                } else throw new RuntimeException( "didn't have a face set for " + latest )
            }
        }
    }

    def colors: collection.Set[FaceColor] = colorSet

    def getOppositeColor( color: FaceColor ): Option[FaceColor] = {
        faceLock.synchronized {
            if ( color == FaceColor.Unknown ) return None
            val latest = getLatestColor( color )
            if ( latest == Inside ) return Some( Outside )
            if ( latest == Outside ) return Some( Inside )
            colorOppositeMap.get( latest ) match {
                case Some( colorOpt ) => colorOpt
                case None => if ( hasParent ) {
                    val ret = parent.getOppositeColor( latest )
                    colorOppositeMap( latest ) = ret
                    ret
                } else None // allow not loading the base instance!
            }
        }
    }

    def hasOppositeColor( color: FaceColor ): Boolean = getOppositeColor( color ).isDefined // not computationally friendly

    def oppositeColor( color: FaceColor ): FaceColor = getOppositeColor( color ).get

    def getLatestColor( color: FaceColor ): FaceColor = {
        faceLock.synchronized {
            if ( color == FaceColor.Unknown ) return color
            if ( colors.contains( color ) ) {
                color
            } else {
                oldColorMap.get( color ) match {
                    case Some( newColor ) => {
                        val ret = getLatestColor( newColor )
                        oldColorMap( color ) = ret
                        ret
                    }
                    case None => {
                        val ret = getLatestColor( parent.getLatestColor( color ) )
                        oldColorMap( color ) = ret
                        ret
                    }
                }
            }
        }
    }

    def isOppositeColor( a: FaceColor, b: FaceColor ): Boolean = {
        faceLock.synchronized {
            if ( a == FaceColor.Unknown || b == FaceColor.Unknown ) return false
            val ax = getLatestColor( a )
            val bx = getLatestColor( b )
            return getOppositeColor( ax ).isDefined && (getOppositeColor( ax ).get == bx)
        }
    }

    def isSameColor( a: FaceColor, b: FaceColor ): Boolean = {
        faceLock.synchronized {
            if ( a == FaceColor.Unknown || b == FaceColor.Unknown ) return false
            val ax = getLatestColor( a )
            val bx = getLatestColor( b )
            return ax == bx
        }
    }

    private var hitFaces = new ListBuffer[(Face4, FaceColor, FaceColor)]

    private def resetHitFaces(): Unit = {
        hitFaces = new ListBuffer[(Face4, FaceColor, FaceColor)]
    }

    private def notifyHitFaces(): Unit = {
        for ((face, from, to) <- hitFaces) {
            notifyColorChange( face, from, to )
        }
        resetHitFaces()
    }

    private def getAffectedCount( color: FaceColor ): Int = {
        val oppositeCount: Int = if ( getOppositeColor( color ).isDefined ) facesWithColor( getOppositeColor( color ).get ).size else 0
        facesWithColor( color ).size + oppositeCount
    }

    private def changeColor( a: FaceColor, b: FaceColor ): Unit = { // change a -> b
        val ax = getLatestColor( a )
        val bx = getLatestColor( b )
        require( !ax.isAbsolute )
        if ( ax == bx ) return
        if ( getOppositeColor( ax ).isDefined && (getOppositeColor( ax ).get == bx) ) {
            if ( allowsInvalid ) {
                return
            } else {
                throw new InvalidGridException
            }
        }

        //        for (face <- facesWithColor( b )) hitFaces.append( (face, b, b) ) // TODO: need to send events on faces that were in B?
        for (face <- facesWithColor( a )) {
            faceToColorMap( face ) = b
            hitFaces.append( (face, a, b) )
        }
        colorToFacesMap( b ) = facesWithColor( b ) ++ facesWithColor( a )
        colorToFacesMap -= a
        if ( getOppositeColor( a ).isDefined ) {
            colorOppositeMap( getOppositeColor( a ).get ) = Some( b )
        }
        oldColorMap( a ) = b
        colorSet = colorSet - a
    }

    private def combineColors( a: FaceColor, b: FaceColor ): Unit = { // combine a and b in the best order.
        val ax = getLatestColor( a )
        val bx = getLatestColor( b )
        if ( ax == bx ) return
        if ( ax.isAbsolute && bx.isAbsolute ) throw new InvalidGridException
        if ( ax.isAbsolute ) {
            changeColor( bx, ax )
        } else if ( bx.isAbsolute ) {
            changeColor( ax, bx )
        } else if ( getAffectedCount( ax ) > getAffectedCount( bx ) ) {
            changeColor( bx, ax )
        } else {
            changeColor( ax, bx )
        }
    }

    def setColorsSame( a: FaceColor, b: FaceColor ): Unit = { // note: make sure notifyHitFaces() is called
        faceLock.synchronized {
            if ( a == FaceColor.Unknown || b == FaceColor.Unknown ) return // don't worry about setting unknown colors
            resetHitFaces()
            val ax = getLatestColor( a )
            val bx = getLatestColor( b )
            if ( ax == bx ) return
            if ( getOppositeColor( ax ).isDefined && (getOppositeColor( ax ).get == bx) ) {
                if ( allowsInvalid ) {
                    return
                } else {
                    throw new InvalidGridException
                }
            }

            if ( !hasOppositeColor( ax ) || !hasOppositeColor( bx ) ) {
                combineColors( ax, bx )
            } else {
                combineColors( oppositeColor( ax ), oppositeColor( bx ) )
                combineColors( ax, bx )
            }
            moves.append( new FaceColorMove( ax, bx, true ) )
            notifyHitFaces()
        }
    }

    def setColorsOpposite( a: FaceColor, b: FaceColor ): Unit = { // note: make sure notifyHitFaces() is called
        faceLock.synchronized {
            if ( a == FaceColor.Unknown || b == FaceColor.Unknown ) return // don't worry about setting unknown colors
            resetHitFaces()
            val ax = getLatestColor( a )
            val bx = getLatestColor( b )
            if ( ax == bx ) {
                if ( allowsInvalid ) {
                    return
                } else {
                    throw new InvalidGridException
                }
            }
            if ( getOppositeColor( ax ).isDefined && (getOppositeColor( ax ).get == bx) ) return

            val aHas = hasOppositeColor( ax )
            val bHas = hasOppositeColor( bx )

            if ( !aHas && !bHas ) {
                colorOppositeMap( ax ) = Some( bx )
                colorOppositeMap( bx ) = Some( ax )

                // send notification for essential color change? // TODO: redo events to specify color differences between colors
                // TODO: then remove this after
                for (face <- facesWithColor( ax )) {
                    hitFaces.append( (face, ax, ax) )
                }
            } else if ( aHas && !bHas ) {
                colorOppositeMap( bx ) = Some( ax )
                combineColors( oppositeColor( ax ), bx )
            } else if ( !aHas && bHas ) {
                colorOppositeMap( ax ) = Some( bx )
                combineColors( oppositeColor( bx ), ax )
            } else if ( aHas && bHas ) {
                val bOppy: FaceColor = oppositeColor( bx )
                combineColors( oppositeColor( ax ), bx )
                combineColors( bOppy, ax )
            }
            moves.append( new FaceColorMove( ax, bx, false ) )
            notifyHitFaces()
        }
    }

    if ( hasParent ) {
        colorSet = parent.colorSet
    } else {
        colorSet = colorSet + FaceColor.Outside
        colorSet = colorSet + FaceColor.Inside
        for (face <- faceIterator) {
            val color: FaceColor = new FaceColor
            faceToColorMap( face ) = color
            colorSet = colorSet + color
            colorToFacesMap( color ) = Set( face )
            // don't load colorOppositeMap for now
        }
        colorToFacesMap( Inside ) = Set.empty[Face4]
        colorToFacesMap( Outside ) = Set.empty[Face4]
    }

    def addFaceColorListener( listener: FaceColorListener ) {
        faceColorListeners = faceColorListeners + listener;
    }

    def removeFaceColorListener( listener: FaceColorListener ) {
        faceColorListeners = faceColorListeners - listener;
    }

    def notifyColorChange( face: Face4, oldColor: FaceColor, newColor: FaceColor ) {
        faceColorListeners.foreach( _.onColorChange( face, oldColor, newColor ) )
    }

    override def compareTo( grid: Grid4 ): Int = {
        val superRet = super.compareTo( grid )
        if ( superRet != 0 ) return superRet
        if ( grid.isInstanceOf[ColoredGrid4] ) {
            val other = grid.asInstanceOf[ColoredGrid4]

            for (face <- faceIterator) {
                val otherFace = face.inGrid( other )

                val sameRet = compareFaceSets( facesWithColor( colorOf( face ) ), other.facesWithColor( other.colorOf( otherFace ) ) )
                if ( sameRet != 0 ) return sameRet

                val oppRet = (getOppositeColor( colorOf( face ) ), other.getOppositeColor( other.colorOf( otherFace ) )) match {
                    case (None, None) => 0
                    case (Some( color ), None) => 1
                    case (None, Some( otherColor )) => -1
                    case (Some( color ), Some( otherColor )) => compareFaceSets( facesWithColor( color ), other.facesWithColor( otherColor ) )
                }
                if ( oppRet != 0 ) return oppRet
            }
        }
        return 0 // defer to subclass
    }

    private def compareFaceSets( a: Set[Face4], b: Set[Face4] ): Int = {
        if ( a.size < b.size ) return -1
        if ( a.size > b.size ) return 1
        val aFaces = (Nil ++ a).sortWith( _.compareTo( _ ) < 0 )
        val bFaces = (Nil ++ b).sortWith( _.compareTo( _ ) < 0 )
        val aIter = aFaces.iterator
        val bIter = bFaces.iterator
        while (aIter.hasNext) {
            val ret = aIter.next.compareTo( bIter.next )
            if ( ret != 0 ) return -ret
        }
        return 0
    }

    override protected def partSerialString: String = {
        val str = new StringBuilder( super.partSerialString )

        str.append( ":c" )

        var faceSet = Set.empty[Face4] ++ faceIterator

        def faceString( face: Face4 ): String = face.row + "," + face.col

        for (face <- faceIterator; if faceSet.contains( face )) {
            val color = colorOf( face )
            val sameList: List[Face4] = (Nil ++ facesWithColor( color )).sortWith( _.compareTo( _ ) < 0 )
            val otherList: List[Face4] = getOppositeColor( color ) match {
                case None => Nil
                case Some( otherColor ) => (Nil ++ facesWithColor( otherColor )).sortWith( _.compareTo( _ ) < 0 )
            }
            if ( sameList.size + otherList.size > 1 ) {
                str.append( "/" )
                if ( color.isAbsolute ) {
                    color match {
                        case Inside => str.append( "i" )
                        case Outside => str.append( "o" )
                    }
                }
                str.append( sameList.map( faceString( _ ) ).mkString( "-" ) )
                if ( otherList.size > 0 ) {
                    str.append( "\\" + otherList.map( faceString( _ ) ).mkString( "-" ) )
                }
            }

            // don't repeat over faces
            for (face <- sameList ++ otherList) {
                faceSet -= face
            }
        }

        return str.toString
    }

    def deserializeColorPart( str: String ): Unit = {
        if ( str.length == 0 ) return
        require( str.startsWith( "/" ) )
        val pairs = str.substring( 1 ).split( "/" )
        for (pair <- pairs) {
            var pairString = pair
            val isInside = pair.startsWith( "i" )
            val isOutside = pair.startsWith( "o" )
            if ( isInside || isOutside ) {
                pairString = pairString.substring( 1 )
            }
            val sets: List[String] = if ( pairString.contains( "\\" ) ) {
                pairString.split( "\\\\" ).toList
            } else {
                List( pairString )
            }
            var firsts = new ListBuffer[Face4]
            for (set <- sets) {
                // faces of all same color
                val faceStrings = set.split( "-" ).toList
                val faceSet = faceStrings.map( (fs) => {
                    val ns: Array[String] = fs.split( "," )
                    val row = ns( 0 ).toInt
                    val col = ns( 1 ).toInt
                    faces( row )( col )
                } )
                firsts.append( faceSet.head )
                for (face <- faceSet; if face != faceSet.head) {
                    setColorsSame( colorOf( face ), colorOf( faceSet.head ) )
                }
            }
            if ( firsts.size > 1 ) {
                // there were two sets. set them to opposite colors
                setColorsOpposite( colorOf( firsts( 0 ) ), colorOf( firsts( 1 ) ) )
            }
            if ( isInside ) {setColorsSame( colorOf( firsts( 0 ) ), Inside )}
            if ( isOutside ) {setColorsSame( colorOf( firsts( 0 ) ), Outside )}
        }
    }
}

trait FaceColorListener {
    def onColorChange( face: Face4, oldColor: FaceColor, newColor: FaceColor ) {

    }
}

object FaceColor {
    val Inside = new FaceColor( true )
    val Outside = new FaceColor( true )
    val Unknown = new FaceColor( true ) // used for open grids. always not same or opposite to other colors
}

class FaceColor( val isAbsolute: Boolean = false ) {
    def absoluteOpposite: FaceColor = {
        require( isAbsolute )
        this match {
            case Inside => Outside
            case Outside => Inside
        }
    }

    def isKnown: Boolean = this == FaceColor.Unknown

    override def toString = this match {
        case Inside => "Inside"
        case Outside => "Outside"
        case _ => "Other"
    }
}