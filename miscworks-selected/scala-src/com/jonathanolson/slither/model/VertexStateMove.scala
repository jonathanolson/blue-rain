package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._

class VertexStateMove(
        val vertex: Vertex4,
        val state: VertexState
        ) extends Move {
    override def toString = vertex + ":" + state

    def apply( grid: Grid4 ) = if ( grid.isInstanceOf[VertexStateGrid4] ) { // TODO: get rid of cast here? not likely
        grid.asInstanceOf[VertexStateGrid4].vertexState( vertex ) = state
    }
}