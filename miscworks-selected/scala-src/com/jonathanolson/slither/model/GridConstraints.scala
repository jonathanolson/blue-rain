package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import scala.collection.mutable.ListBuffer
import scala.collection.Set
import com.jonathanolson.slither.patterns.{InvalidPattern, ColorPattern, VertexStatePattern, FacePattern, EdgePattern, BasicRule}
import com.jonathanolson.slither.util.LogHelper

class GridConstraints( val rows: Int, val cols: Int ) extends LogHelper {
    var redEdges = Set.empty[Edge4]
    var blackEdges = Set.empty[Edge4]
    var vertices = Map.empty[Vertex4, VertexState]
    var sameColors = Set.empty[(Face4, Face4)]
    var oppositeColors = Set.empty[(Face4, Face4)]
    var initialGrid: Grid4 = null
    var allowedCount: Int = 0

    def constructRule: BasicRule = {
        debug( "constructing rule from constraints:\n" + toString )
        val rule = new BasicRule
        def addPatterns: Unit = {
            // initial face constraints
            for (face <- initialGrid.faceIterator) {
                if ( initialGrid.value( face ).isDefined ) {
                    rule.patterns.append( new FacePattern( face.row, face.col, initialGrid.value( face ).get ) )
                }
            }
            // initial edge constraints
            for (edge <- initialGrid.edgeIterator) {
                if ( initialGrid.state( edge ) != White ) {
                    rule.patterns.append( new EdgePattern( edge.row, edge.col, edge.isVertical, initialGrid.state( edge ) ) )
                }
            }
            // initial vertex state constraints
            if ( initialGrid.isInstanceOf[VertexStateGrid4] ) {
                val grid = initialGrid.asInstanceOf[VertexStateGrid4]
                for (vertex <- grid.vertexIterator) {
                    if ( grid.vertexState( vertex ) != VertexState.All ) {
                        rule.patterns.append( new VertexStatePattern( vertex.row, vertex.col, grid.vertexState( vertex ) ) )
                    }
                }
            }
            // initial coloring constraints
            if ( initialGrid.isInstanceOf[ColoredGrid4] ) {
                val grid = initialGrid.asInstanceOf[ColoredGrid4]
                var colorSet = Set.empty[FaceColor] ++ grid.colors
                def setSameColor( faces: Set[Face4] ) {
                    if ( faces.size >= 2 ) {
                        val mainFace = faces.head
                        for (otherFace <- faces; if mainFace != otherFace) {
                            rule.patterns.append( new ColorPattern( mainFace.row, mainFace.col, otherFace.row, otherFace.col, true ) )
                        }
                    }
                }
                for (color <- grid.colors; if colorSet.contains( color )) {
                    colorSet -= color // don't do this color again

                    // handle same colored faces
                    val sameColoredFaces: Set[Face4] = grid.facesWithColor( color )
                    setSameColor( sameColoredFaces )

                    // check for opposite color
                    grid.getOppositeColor( color ) match {
                        case Some( oppositeColor ) => {
                            colorSet -= oppositeColor // don't do this color again
                            val oppositeColoredFaces: Set[Face4] = grid.facesWithColor( oppositeColor )
                            setSameColor( oppositeColoredFaces )
                            if ( (oppositeColoredFaces.size > 0) && (sameColoredFaces.size > 0) ) {
                                val face1 = sameColoredFaces.head
                                val face2 = oppositeColoredFaces.head
                                rule.patterns.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, false ) )
                            }
                        }
                        case None => ()
                    }
                }
            }
        }
        def addResults: Unit = {
            for (edge <- redEdges) {
                rule.results.append( new EdgePattern( edge.row, edge.col, edge.isVertical, Red ) )
            }
            for (edge <- blackEdges) {
                rule.results.append( new EdgePattern( edge.row, edge.col, edge.isVertical, Black ) )
            }
            for ((vertex, resultState) <- vertices; if resultState != VertexState.All) {
                if ( initialGrid.isInstanceOf[VertexStateGrid4] ) {
                    // has false only for new constraints not in original grid
                    val newState = initialGrid.asInstanceOf[VertexStateGrid4].vertexState( vertex ).onlyNewConstraints( resultState )
                    if ( newState != VertexState.All ) {
                        rule.results.append( new VertexStatePattern( vertex.row, vertex.col, newState ) )
                    }
                } else {
                    // initial grid (or pattern) did not have vertex state. just add the whole state in
                    rule.results.append( new VertexStatePattern( vertex.row, vertex.col, resultState ) )
                }
            }
            // TODO: possibly optimize so we don't add more color patterns than necessary
            if ( initialGrid.isInstanceOf[ColoredGrid4] ) {
                val grid = initialGrid.asInstanceOf[ColoredGrid4]
                for ((face1, face2) <- sameColors; if !grid.isSameColor( grid.colorOf( face1 ), grid.colorOf( face2 ) )) {
                    rule.results.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, true ) )
                }
                for ((face1, face2) <- oppositeColors; if !grid.isOppositeColor( grid.colorOf( face1 ), grid.colorOf( face2 ) )) {
                    rule.results.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, false ) )
                }
            } else {
                for ((face1, face2) <- sameColors) {
                    rule.results.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, true ) )
                }
                for ((face1, face2) <- oppositeColors) {
                    rule.results.append( new ColorPattern( face1.row, face1.col, face2.row, face2.col, false ) )
                }
            }
        }
        addPatterns
        if ( allowedCount > 0 ) {
            addResults
        } else {
            rule.results.append( new InvalidPattern )
        }
        return rule
    }

    def init( grid: Grid4 ): Unit = {
        initialGrid = grid

        for (edge <- grid.edgeIterator) {
            redEdges += edge
            blackEdges += edge
        }
        for (vertex <- grid.vertexIterator) {
            vertices += (vertex -> VertexState.None)
        }
        vertices = vertices.updated( grid.vertices( 0 )( 0 ), VertexState.None.copy( allowNorthwest = true ) )
        vertices = vertices.updated( grid.vertices( grid.rows )( grid.cols ), VertexState.None.copy( allowSoutheast = true ) )
        vertices = vertices.updated( grid.vertices( grid.rows )( 0 ), VertexState.None.copy( allowSouthwest = true ) )
        vertices = vertices.updated( grid.vertices( 0 )( grid.cols ), VertexState.None.copy( allowNortheast = true ) )
        for (face1 <- grid.faceIterator; face2 <- grid.faceIterator; if face1 != face2) {
            if ( (face1.row < face2.row) || ((face1.row == face2.row) && face1.col < face2.col) ) {
                sameColors += ((face1, face2))
                oppositeColors += ((face1, face2))
            }
        }
        debug( "initialized constraints:\n" + toString )
    }

    override def toString = {
        "constraints\nred edges\n  " +
                redEdges.mkString( "\n  " ) +
                "\nblack edges\n  " +
                blackEdges.mkString( "\n  " ) +
                "\nvertex states\n  " +
                vertices.keys.filter( (vertex) => vertices( vertex ) != VertexState.All ).map( (vertex) => vertices( vertex ).doesNotAllowString + " " + vertex ).mkString( "\n  " ) +
                "\nsame faces\n  " +
                sameColors.mkString( "\n  " ) +
                "\nopposite faces\n  " +
                oppositeColors.mkString( "\n  " )
    }

    def allow( grid: Grid4 ): Unit = {
        allowedCount += 1
        for (edge <- grid.edgeIterator) {
            grid.state( edge ) match {
                case Red => blackEdges -= edge
                case Black => redEdges -= edge
            }
        }
        for (vertex <- grid.vertexIterator) {
            val oldState: VertexState = vertices( vertex )
            vertices = vertices.updated( vertex, oldState | VertexState.fromVertex( grid, vertex ) )
        }

        // use true/false as "colors" for coloring the filled in open-bounded grid.
        // use pairs as a list of already-scanned faces with their color

        val pairs = new ListBuffer[(Face4, Boolean)]
        var lastToLeft = true // irrelevant at the start
        var lastLeftRow = true // default value for upper-left ish

        for (face <- grid.faceIterator) {
            var color: Boolean = true
            (face.westEdge.otherFace( face ) match {
                case Some( westFace ) => {
                    color = (grid.state( face.westEdge ) match {
                        case Red => lastToLeft
                        case Black => !lastToLeft
                    })
                    lastToLeft = color
                }
                case None => {
                    color = (grid.state( face.northEdge ) match {
                        case Red => lastLeftRow
                        case Black => !lastLeftRow
                    })
                    lastLeftRow = color
                    lastToLeft = color
                }
            })
            // color is now assigned
            for ((otherFace, otherColor) <- pairs) {
                if ( color == otherColor ) {
                    // colors are the same, so rule out the opposite constraint
                    oppositeColors -= ((otherFace, face)) // in this order
                } else {
                    // colors different, rule out the same constraint
                    sameColors -= ((otherFace, face))
                }
            }
            pairs.append( (face, color) )
        }
    }
}