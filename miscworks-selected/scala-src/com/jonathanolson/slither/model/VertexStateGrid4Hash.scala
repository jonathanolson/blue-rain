package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.HashMap

trait VertexStateGrid4Hash extends VertexStateGrid4 {
    private val vertexStates = new HashMap[Vertex4, VertexState]

    def vertexState: VertexStateCheckable = new VertexStateCheckable {
        def apply( vertex: Vertex4 ) = {
            vertexStates.get( vertex ) match {
                case Some( state ) => state
                case None => if ( hasParent ) {
                    val ret = parent.vertexState( vertex )
                    vertexStates( vertex ) = ret
                    ret
                } else VertexState.All
            }
        }

        def update( vertex: Vertex4, state: VertexState ) = {
            var oldState = apply( vertex )
            if ( state != oldState ) {
                vertexStates( vertex ) = state
                moves.append( new VertexStateMove( vertex, state ) )
                notifyVertexStateChange( vertex, oldState, state )
            }
        }
    }

    if ( !hasParent ) {
        // TODO: optimize
        if ( !openBorders ) {
            for (vertex <- vertexIterator; dir <- vertex.emptyDirections) {
                vertexState( vertex ) = vertexState( vertex ).withEdge( dir, Red )
            }
        }
    }
}