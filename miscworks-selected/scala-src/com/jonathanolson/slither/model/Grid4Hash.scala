package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.{ListBuffer, HashMap => MHashMap, Map => MMap, Set => MSet}
import com.jonathanolson.slither.exceptions.InvalidGridException

trait Grid4Hash extends Grid4 {
    private val states = new MHashMap[Edge4, EdgeState] // states
    private val values = new MHashMap[Face4, Option[Int]] // values

    // TODO TODO: allow indexing by row/col of edge instead of hash!
    def state: EdgeStateCheckable = new EdgeStateCheckable {
        def apply( edge: Edge4 ): EdgeState = {
            states.get( edge ) match {
                case Some( state ) => state
                case None => if ( hasParent ) {
                    // update it in our state
                    val ret = parent.state( edge )
                    states( edge ) = ret
                    ret
                } else EdgeState.White
            }
        }

        def update( edge: Edge4, state: EdgeState ) {
            var oldState = apply( edge )
            if ( state != oldState ) {
                if ( !allowsInvalid && (oldState != White) && (oldState != state) ) throw new InvalidGridException
                oldState match {
                    case White => whiteCount -= 1
                    case Red => redCount -= 1
                    case Black => blackCount -= 1
                }
                state match {
                    case White => whiteCount += 1
                    case Red => redCount += 1
                    case Black => blackCount += 1
                }
                states( edge ) = state
                moves.append( new EdgeStateMove( edge, state ) )
                notifyEdgeStateChange( edge, oldState, state )
            }
        }

        def noneState = if ( openBorders ) White else Red
    }

    def value: FaceValueCheckable = new FaceValueCheckable {
        def apply( face: Face4 ): Option[Int] = {
            values.get( face ) match {
                case Some( value ) => value
                case None => if ( hasParent ) {
                    // update it in our state
                    val ret = parent.value( face )
                    values( face ) = ret
                    ret
                } else None
            }
        }

        def update( face: Face4, value: Option[Int] ) {
            value match {
                case Some( i ) => require( (i >= 0) && (i <= 4) )
                case None => ()
            }
            var oldValue = apply( face )
            values( face ) = value
            if ( value != oldValue ) {
                notifyFaceValueChange( face, oldValue, value )
            }
        }
    }
}