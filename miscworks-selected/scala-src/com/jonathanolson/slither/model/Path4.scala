package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import scala.collection.mutable.ListBuffer
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.util.LogHelper

object Path4 extends LogHelper {
    def createBlackLightPath( grid: Grid4, startVertex: Vertex4 ): LightPath4 = {
        val path = new LightPath4( startVertex )
        fillEmptyBlackPath( path, grid )
        path
    }

    def createBlackFullPath( grid: Grid4, startVertex: Vertex4 ): FullPath4 = {
        val path = new FullPath4( startVertex )
        fillEmptyBlackPath( path, grid )
        path
    }

    def fillEmptyBlackPath( path: Path4, grid: Grid4 ): Unit = {
        var blackEdgeOpt = path.headVertex.realEdges.find( (e) => grid.state( e ) == Black )
        if ( blackEdgeOpt.isDefined ) {

            path.addEdge( blackEdgeOpt.get )

            // add on to the head and see if we loop

            var other: Option[Edge4] = grid.otherBlackEdge( path.headVertex, path.headEdge )
            while (other.isDefined && (path.headVertex != path.tailVertex)) {
                path.addEdge( other.get )
                other = grid.otherBlackEdge( path.headVertex, path.headEdge )
            }

            // didn't loop, so let's add on to the tail

            if ( path.headVertex != path.tailVertex ) {
                other = grid.otherBlackEdge( path.tailVertex, path.tailEdge )
                while (other.isDefined) {
                    path.addEdge( other.get )
                    other = grid.otherBlackEdge( path.tailVertex, path.tailEdge )
                }
            }
        }
    }
}

trait Path4 {
    def length: Int // number of edges
    def headVertex: Vertex4 // head vertex
    def tailVertex: Vertex4 // tail vertex
    def headEdge: Edge4 // head edge
    def tailEdge: Edge4 // tail edge

    def addEdge( edge: Edge4 ): Unit

    def isLoop: Boolean = (length > 0) && (headVertex == tailVertex)
}

class LightPath4( startVertex: Vertex4 ) extends Path4 {
    // TODO: redo to encapsulate?

    var length = 0
    var headVertex = startVertex
    var tailVertex = startVertex
    var headEdge: Edge4 = null
    var tailEdge: Edge4 = null

    override def toString = "L" + length + " head: " + headVertex + ", tail: " + tailVertex + ", edges " + headEdge + ", " + tailEdge

    def addEdge( edge: Edge4 ): Unit = {
        if ( !((edge != headEdge) && (edge != tailEdge)) ) throw new InvalidGridException
        require( (edge != headEdge) && (edge != tailEdge) )
        if ( length == 0 ) {
            headVertex = edge.vb
            tailVertex = edge.va
            headEdge = edge
            tailEdge = edge
        } else {
            if ( edge.va == headVertex ) {
                headEdge = edge
                headVertex = edge.vb
            } else if ( edge.vb == headVertex ) {
                headEdge = edge
                headVertex = edge.va
            } else if ( edge.va == tailVertex ) {
                tailEdge = edge
                tailVertex = edge.vb
            } else if ( edge.vb == tailVertex ) {
                tailEdge = edge
                tailVertex = edge.va
            } else {
                throw new RuntimeException( "bad add to path" )
            }
        }
        length = length + 1
    }
}

class FullPath4( startVertex: Vertex4 ) extends Path4 {
    // TODO: refactor with lightpath
    var length = 0
    var headVertex = startVertex
    var tailVertex = startVertex
    var headEdge: Edge4 = null
    var tailEdge: Edge4 = null
    var edges = new ListBuffer[Edge4]

    def addEdge( edge: Edge4 ): Unit = {
        require( (edge != headEdge) && (edge != tailEdge) )
        if ( length == 0 ) {
            headVertex = edge.vb
            tailVertex = edge.va
            headEdge = edge
            tailEdge = edge
            edges.append( edge )
        } else {
            if ( edge.va == headVertex ) {
                headEdge = edge
                headVertex = edge.vb
                edges.append( edge )
            } else if ( edge.vb == headVertex ) {
                headEdge = edge
                headVertex = edge.va
                edges.append( edge )
            } else if ( edge.va == tailVertex ) {
                tailEdge = edge
                tailVertex = edge.vb
                edges.prepend( edge )
            } else if ( edge.vb == tailVertex ) {
                tailEdge = edge
                tailVertex = edge.va
                edges.prepend( edge )
            } else {
                throw new RuntimeException( "bad add to path" )
            }
        }
        length = length + 1
    }
}