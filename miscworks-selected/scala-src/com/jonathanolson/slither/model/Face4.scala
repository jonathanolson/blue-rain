package com.jonathanolson.slither.model

import collection.mutable.{ListBuffer}
import com.jonathanolson.slither.SlitherInclude._
import com.jonathanolson.slither.util.Direction

class Face4(
        val row: Int,
        val col: Int
        ) {
    var northEdge: Edge4 = null
    var westEdge: Edge4 = null
    var eastEdge: Edge4 = null
    var southEdge: Edge4 = null

    var northwestVertex: Vertex4 = null
    var northeastVertex: Vertex4 = null
    var southwestVertex: Vertex4 = null
    var southeastVertex: Vertex4 = null

    def edges = Set( westEdge, northEdge, eastEdge, southEdge )

    def vertices = Set( northwestVertex, northeastVertex, southeastVertex, southwestVertex )

    def getEdge( dir: Direction ): Edge4 = dir match {
        case North => northEdge
        case West => westEdge
        case South => southEdge
        case East => eastEdge
        case _ => throw new RuntimeException( "bad dir " + dir + " for getEdge" )
    }

    def directionOfEdge( edge: Edge4 ): Direction = {
        require( edges.contains( edge ) )
        if ( edge == northEdge ) return North
        if ( edge == westEdge ) return West
        if ( edge == southEdge ) return South
        if ( edge == eastEdge ) return East
        throw new RuntimeException( "should never happen" )
    }

    def getVertex( dir: Direction ): Vertex4 = dir match {
        case Northwest => northwestVertex
        case Northeast => northeastVertex
        case Southeast => southeastVertex
        case Southwest => southwestVertex
        case _ => throw new RuntimeException( "bad dir " + dir + " for getVertex" )
    }

    def directionOfVertex( vertex: Vertex4 ): Direction = {
        require( vertices.contains( vertex ) )
        if ( vertex == northwestVertex ) return Northwest
        if ( vertex == northeastVertex ) return Northeast
        if ( vertex == southeastVertex ) return Southeast
        if ( vertex == southwestVertex ) return Southwest
        throw new RuntimeException( "should never happen" )
    }

    //override def toString = "F[" + row + "," + col + " v: " + vertices.mkString( "," ) + " e: " + edges.mkString( "," ) + "]"
    override def toString = "F[" + row + "," + col + "]"

    def compareTo( other: Face4 ): Int = {
        val ret = col.compare( other.col )
        if ( ret != 0 ) return ret
        return row.compare( other.row )
    }

    def inGrid( grid: Grid4 ): Face4 = {
        grid.faces( row )( col )
    }
}