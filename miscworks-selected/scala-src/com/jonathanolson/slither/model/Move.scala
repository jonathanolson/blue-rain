package com.jonathanolson.slither.model

trait Move {
    def apply( grid: Grid4 ): Unit // use the move on the grid
}