package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.HashMap
import com.jonathanolson.slither.exceptions.InvalidVertexStateException
import com.jonathanolson.slither.util.Direction

object VertexState {
    val All = new VertexState( true, true, true, true, true, true, true )
    val None = new VertexState( false, false, false, false, false, false, false )

    def fromVertex( grid: Grid4, vertex: Vertex4 ): VertexState = {
        val north = grid.getState( vertex.northEdge ) == Black
        val east = grid.getState( vertex.eastEdge ) == Black
        val south = grid.getState( vertex.southEdge ) == Black
        val west = grid.getState( vertex.westEdge ) == Black
        (north, east, south, west) match {
            case (false, false, false, false) => VertexState.None.copy( allowEmpty = true )
            case (true, false, true, false) => VertexState.None.copy( allowVertical = true )
            case (false, true, false, true) => VertexState.None.copy( allowHorizontal = true )
            case (true, true, false, false) => VertexState.None.copy( allowNortheast = true )
            case (false, true, true, false) => VertexState.None.copy( allowSoutheast = true )
            case (false, false, true, true) => VertexState.None.copy( allowSouthwest = true )
            case (true, false, false, true) => VertexState.None.copy( allowNorthwest = true )
            case (true, false, false, false) => { // north
                var state = VertexState.None
                if ( grid.openBorders && vertex.eastEdge.isEmpty ) state = state.copy( allowNortheast = true )
                if ( grid.openBorders && vertex.southEdge.isEmpty ) state = state.copy( allowVertical = true )
                if ( grid.openBorders && vertex.westEdge.isEmpty ) state = state.copy( allowNorthwest = true )
                state
            }
            case (false, true, false, false) => { // east
                var state = VertexState.None
                if ( grid.openBorders && vertex.southEdge.isEmpty ) state = state.copy( allowSoutheast = true )
                if ( grid.openBorders && vertex.westEdge.isEmpty ) state = state.copy( allowHorizontal = true )
                if ( grid.openBorders && vertex.northEdge.isEmpty ) state = state.copy( allowNortheast = true )
                state
            }
            case (false, false, true, false) => { // south
                var state = VertexState.None
                if ( grid.openBorders && vertex.westEdge.isEmpty ) state = state.copy( allowSouthwest = true )
                if ( grid.openBorders && vertex.northEdge.isEmpty ) state = state.copy( allowVertical = true )
                if ( grid.openBorders && vertex.eastEdge.isEmpty ) state = state.copy( allowSoutheast = true )
                state
            }
            case (false, false, false, true) => { // west
                var state = VertexState.None
                if ( grid.openBorders && vertex.northEdge.isEmpty ) state = state.copy( allowNorthwest = true )
                if ( grid.openBorders && vertex.eastEdge.isEmpty ) state = state.copy( allowHorizontal = true )
                if ( grid.openBorders && vertex.southEdge.isEmpty ) state = state.copy( allowSouthwest = true )
                state
            }
            case _ => throw new RuntimeException( "tried to match vertex in fromVertex badly: " + north + ", " + east + ", " + south + ", " + west )
        }
    }

    def deserialize( str: String ): VertexState = {
        assert( str.length == 2 )
        val m = Grid4.fromHex( str( 0 ) )
        val n = Grid4.fromHex( str( 1 ) )

        return new VertexState(
            ((m / 4) % 2) == 1,
            ((m / 2) % 2) == 1,
            ((m) % 2) == 1,
            ((n / 8) % 2) == 1,
            ((n / 4) % 2) == 1,
            ((n / 2) % 2) == 1,
            ((n) % 2) == 1
            )
    }

    //    def serialString: String = {
    //        var m = 0
    //        var n = 0
    //        if ( allowEmpty ) m += 4
    //        if ( allowVertical ) m += 2
    //        if ( allowHorizontal ) m += 1
    //        if ( allowNorthwest ) n += 8
    //        if ( allowNortheast ) n += 4
    //        if ( allowSoutheast ) n += 2
    //        if ( allowSouthwest ) n += 1
    //        return m.toString + (n match {
    //            case x if x < 10 => x.toString
    //            case 10 => "a"
    //            case 11 => "b"
    //            case 12 => "c"
    //            case 13 => "d"
    //            case 14 => "e"
    //            case 15 => "f"
    //        })
    //    }
}

case class VertexState(
        allowEmpty: Boolean,
        allowVertical: Boolean,
        allowHorizontal: Boolean,
        allowNorthwest: Boolean,
        allowNortheast: Boolean,
        allowSoutheast: Boolean,
        allowSouthwest: Boolean
        ) {

    // TODO: add iteration or list/set of defined faces and edges

    // TODO: add function to determine whether colorings of opposite squares are constrained

    def allowsBlack( dir: Direction ): Boolean = dir match {
        case North => allowVertical || allowNortheast || allowNorthwest
        case South => allowVertical || allowSoutheast || allowSouthwest
        case West => allowHorizontal || allowNorthwest || allowSouthwest
        case East => allowHorizontal || allowNortheast || allowSoutheast
    }

    def allowsRed( dir: Direction ): Boolean = dir match {
        case North => allowEmpty || allowHorizontal || allowSoutheast || allowSouthwest
        case East => allowEmpty || allowVertical || allowNorthwest || allowSouthwest
        case South => allowEmpty || allowHorizontal || allowNorthwest || allowNortheast
        case West => allowEmpty || allowVertical || allowSoutheast || allowNortheast
    }

    def isIncident( dir: Direction ): Boolean = dir match {
        case Northwest => (!allowEmpty) && (!allowNorthwest) && (!allowSoutheast)
        case Southeast => (!allowEmpty) && (!allowNorthwest) && (!allowSoutheast)
        case Northeast => (!allowEmpty) && (!allowNortheast) && (!allowSouthwest)
        case Southwest => (!allowEmpty) && (!allowNortheast) && (!allowSouthwest)
    }

    def isBlocked( dir: Direction ): Boolean = dir match {
        case Northwest => (!allowVertical) && (!allowHorizontal) && (!allowNortheast) && (!allowSouthwest)
        case Southeast => (!allowVertical) && (!allowHorizontal) && (!allowNortheast) && (!allowSouthwest)
        case Northeast => (!allowVertical) && (!allowHorizontal) && (!allowNorthwest) && (!allowSoutheast)
        case Southwest => (!allowVertical) && (!allowHorizontal) && (!allowNorthwest) && (!allowSoutheast)
    }

    def isAll = this == VertexState.All

    def constrainedValue( dir: Direction ): EdgeState = {
        (allowsBlack( dir ), allowsRed( dir )) match {
            case (true, true) => White
            case (true, false) => Black
            case (false, true) => Red
            case _ => throw new InvalidVertexStateException
        }
    }

    def withIncident( dir: Direction ): VertexState = {
        require( !dir.isPrimary )
        dir match {
            case Northwest => copy( allowEmpty = false, allowNorthwest = false, allowSoutheast = false )
            case Southeast => copy( allowEmpty = false, allowNorthwest = false, allowSoutheast = false )
            case Northeast => copy( allowEmpty = false, allowNortheast = false, allowSouthwest = false )
            case Southwest => copy( allowEmpty = false, allowNortheast = false, allowSouthwest = false )
        }
    }

    def withBlocked( dir: Direction ): VertexState = {
        require( !dir.isPrimary )
        dir match {
            case Northwest => copy( allowVertical = false, allowHorizontal = false, allowNortheast = false, allowSouthwest = false )
            case Southeast => copy( allowVertical = false, allowHorizontal = false, allowNortheast = false, allowSouthwest = false )
            case Northeast => copy( allowVertical = false, allowHorizontal = false, allowNorthwest = false, allowSoutheast = false )
            case Southwest => copy( allowVertical = false, allowHorizontal = false, allowNorthwest = false, allowSoutheast = false )
        }
    }

    def withoutWrap( dir: Direction ): VertexState = { // get rid of this particular wrap
        require( !dir.isPrimary )
        dir match {
            case Northwest => copy( allowNorthwest = false )
            case Northeast => copy( allowNortheast = false )
            case Southeast => copy( allowSoutheast = false )
            case Southwest => copy( allowSouthwest = false )
        }
    }

    def withOnlyWrap( dir: Direction ): VertexState = { // get rid of all other wraps
        require( !dir.isPrimary )
        dir match {
            case Northwest => copy( allowSouthwest = false, allowSoutheast = false, allowNortheast = false )
            case Northeast => copy( allowNorthwest = false, allowSoutheast = false, allowSouthwest = false )
            case Southeast => copy( allowNorthwest = false, allowNortheast = false, allowSouthwest = false )
            case Southwest => copy( allowNorthwest = false, allowNortheast = false, allowSoutheast = false )
        }
    }

    def withoutLine( dir: Direction ): VertexState = { // without straight in one of these directions
        require( dir.isPrimary )
        dir match {
            case North => copy( allowVertical = false )
            case South => copy( allowVertical = false )
            case East => copy( allowHorizontal = false )
            case West => copy( allowHorizontal = false )
        }
    }

    def withoutLines: VertexState = copy( allowHorizontal = false, allowVertical = false )

    def withoutWraps: VertexState = copy( allowSouthwest = false, allowSoutheast = false, allowNortheast = false, allowNorthwest = false )

    def withoutEmpty: VertexState = copy( allowEmpty = false )

    def withEdge( dir: Direction, edgeState: EdgeState ): VertexState = {
        edgeState match {
            case White => if ( isAll ) this else throw new Exception( "not handled yet" )
            case Black => {
                if ( allowsRed( dir ) ) {
                    new VertexState(
                        false, // empty
                        if ( (dir == North) || (dir == South) ) allowVertical else false, // vertical
                        if ( (dir == West) || (dir == East) ) allowHorizontal else false, // horizontal
                        if ( (dir == North) || (dir == West) ) allowNorthwest else false, // nw
                        if ( (dir == North) || (dir == East) ) allowNortheast else false, // ne
                        if ( (dir == South) || (dir == East) ) allowSoutheast else false, // se
                        if ( (dir == South) || (dir == West) ) allowSouthwest else false // sw
                        )
                } else this
            }
            case Red => {
                if ( allowsBlack( dir ) ) {
                    new VertexState(
                        allowEmpty,
                        if ( (dir == North) || (dir == South) ) false else allowVertical, // vertical
                        if ( (dir == West) || (dir == East) ) false else allowHorizontal, // horizontal
                        if ( (dir == North) || (dir == West) ) false else allowNorthwest, // nw
                        if ( (dir == North) || (dir == East) ) false else allowNortheast, // ne
                        if ( (dir == South) || (dir == East) ) false else allowSoutheast, // se
                        if ( (dir == South) || (dir == West) ) false else allowSouthwest // sw
                        )
                } else this
            }
        }
    }

    def |( other: VertexState ): VertexState = new VertexState(
        allowEmpty || other.allowEmpty,
        allowVertical || other.allowVertical,
        allowHorizontal || other.allowHorizontal,
        allowNorthwest || other.allowNorthwest,
        allowNortheast || other.allowNortheast,
        allowSoutheast || other.allowSoutheast,
        allowSouthwest || other.allowSouthwest
        )

    def &( other: VertexState ): VertexState = new VertexState(
        allowEmpty && other.allowEmpty,
        allowVertical && other.allowVertical,
        allowHorizontal && other.allowHorizontal,
        allowNorthwest && other.allowNorthwest,
        allowNortheast && other.allowNortheast,
        allowSoutheast && other.allowSoutheast,
        allowSouthwest && other.allowSouthwest
        )

    def onlyNewConstraints( result: VertexState ): VertexState = new VertexState( // only false if true in this instance, false in result
        result.allowEmpty || (!allowEmpty && !result.allowEmpty),
        result.allowVertical || (!allowVertical && !result.allowVertical),
        result.allowHorizontal || (!allowHorizontal && !result.allowHorizontal),
        result.allowNorthwest || (!allowNorthwest && !result.allowNorthwest),
        result.allowNortheast || (!allowNortheast && !result.allowNortheast),
        result.allowSoutheast || (!allowSoutheast && !result.allowSoutheast),
        result.allowSouthwest || (!allowSouthwest && !result.allowSouthwest)
        )

    def flipped: VertexState = { // flip about row (flip north and south)
        new VertexState( allowEmpty, allowVertical, allowHorizontal, allowSouthwest, allowSoutheast, allowNortheast, allowNorthwest )
    }

    def rotated( dir: Direction ): VertexState = { // rotate, with north being the identity, east is 1 step clockwise
        require( dir.isPrimary )
        dir match {
            case North => this
            case East => new VertexState( allowEmpty, allowHorizontal, allowVertical, allowSouthwest, allowNorthwest, allowNortheast, allowSoutheast )
            case South => new VertexState( allowEmpty, allowVertical, allowHorizontal, allowSoutheast, allowSouthwest, allowNorthwest, allowNortheast )
            case West => new VertexState( allowEmpty, allowHorizontal, allowVertical, allowNortheast, allowSoutheast, allowSouthwest, allowNorthwest )
        }
    }

    def doesNotAllowString = {
        val builder = new StringBuilder
        if ( allowEmpty ) builder.append( "  " ) else builder.append( "E " )
        if ( allowVertical ) builder.append( "  " ) else builder.append( "V " )
        if ( allowHorizontal ) builder.append( "  " ) else builder.append( "H " )
        if ( allowNorthwest ) builder.append( "   " ) else builder.append( "NW " )
        if ( allowNortheast ) builder.append( "   " ) else builder.append( "NE " )
        if ( allowSoutheast ) builder.append( "   " ) else builder.append( "SE " )
        if ( allowSouthwest ) builder.append( "  " ) else builder.append( "SW" )
        builder.toString
    }

    def compareTo( other: VertexState ): Int = {
        if ( allowEmpty != other.allowEmpty ) return if ( allowEmpty ) -1 else 1
        if ( allowVertical != other.allowVertical ) return if ( allowVertical ) -1 else 1
        if ( allowHorizontal != other.allowHorizontal ) return if ( allowHorizontal ) -1 else 1
        if ( allowNorthwest != other.allowNorthwest ) return if ( allowNorthwest ) -1 else 1
        if ( allowNortheast != other.allowNortheast ) return if ( allowNortheast ) -1 else 1
        if ( allowSoutheast != other.allowSoutheast ) return if ( allowSoutheast ) -1 else 1
        if ( allowSouthwest != other.allowSouthwest ) return if ( allowSouthwest ) -1 else 1
        return 0
    }

    def serialString: String = {
        var m = 0
        var n = 0
        if ( allowEmpty ) m += 4
        if ( allowVertical ) m += 2
        if ( allowHorizontal ) m += 1
        if ( allowNorthwest ) n += 8
        if ( allowNortheast ) n += 4
        if ( allowSoutheast ) n += 2
        if ( allowSouthwest ) n += 1
        return m.toString + (n match {
            case x if x < 10 => x.toString
            case 10 => "a"
            case 11 => "b"
            case 12 => "c"
            case 13 => "d"
            case 14 => "e"
            case 15 => "f"
        })
    }

    def booleans: List[Boolean] = List( allowEmpty, allowVertical, allowHorizontal, allowNorthwest, allowNortheast, allowSoutheast, allowSouthwest )

    def constraintCount: Int = booleans.count( !_ )

}







