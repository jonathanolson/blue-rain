package com.jonathanolson.slither.model

object Edge4 {
    implicit def toOption( edge: Edge4 ): Option[Edge4] = Some( edge )
}

abstract class Edge4(
        val row: Int,
        val col: Int,
        val isVertical: Boolean
        ) {
    def va: Vertex4

    def vb: Vertex4

    def fa: Option[Face4]

    def fb: Option[Face4]

    def vertices = List( va, vb )

    def faces = List( fa, fb )

    override def toString = va + " => " + vb

    def otherVertex( v: Vertex4 ): Vertex4 = {
        if ( v == va ) vb else {
            if ( v == vb ) va else {
                throw new RuntimeException( "vertex v:" + v + " not part of edge " + this )
            }
        }
    }

    def otherFace( face: Face4 ): Option[Face4] = {
        if ( fa.isDefined && fa.get == face ) {
            return fb
        } else if ( fb.isDefined && fb.get == face ) {
            return fa
        } else {
            throw new RuntimeException( "otherFace for edge " + this + " didn't have face " + face )
        }
    }

    def compareTo( other: Edge4 ): Int = {
        var ret = col.compare( other.col )
        if ( ret != 0 ) return ret
        ret = row.compare( other.row )
        if ( ret != 0 ) return ret
        if ( isVertical == other.isVertical ) return 0
        if ( isVertical ) return -1 else 1
    }

    def inGrid( grid: Grid4 ): Edge4
}

class VerticalEdge4( row: Int, col: Int ) extends Edge4( row, col, true ) {
    var northVertex: Vertex4 = null
    var southVertex: Vertex4 = null

    var eastFace: Option[Face4] = None
    var westFace: Option[Face4] = None

    def fb = eastFace

    def fa = westFace

    def vb = southVertex

    def va = northVertex

    def inGrid( grid: Grid4 ) = grid.vedges( row )( col )
}

class HorizontalEdge4( row: Int, col: Int ) extends Edge4( row, col, false ) {
    var westVertex: Vertex4 = null
    var eastVertex: Vertex4 = null

    var northFace: Option[Face4] = None
    var southFace: Option[Face4] = None


    def fb = southFace

    def fa = northFace

    def vb = eastVertex

    def va = westVertex

    def inGrid( grid: Grid4 ) = grid.hedges( row )( col )
}