package com.jonathanolson.slither.model

import com.jonathanolson.slither.SlitherInclude._
import collection.mutable.{ListBuffer, HashMap => MHashMap, Map => MMap, Set => MSet}
import com.jonathanolson.slither.exceptions.InvalidGridException
import com.jonathanolson.slither.solvers.Solver
import com.jonathanolson.slither.util.{LogHelper, MultiArrayIterator}

object Grid4 {
    def empty( gridRows: Int, gridColumns: Int, gridOpenBorders: Boolean = false ) = new {
    val openBorders = gridOpenBorders
    val parentGrid = None
    val cols = gridColumns
    val rows = gridRows
    } with Grid4Hash {
        def newChild: Grid4 = Grid4.child( this )
    }

    def child( grid: Grid4 ): Grid4 = {
        val c = new {
        val openBorders = grid.openBorders
        val parentGrid = Some( grid )
        val cols = grid.cols
        val rows = grid.rows
        } with Grid4Hash {
            def newChild: Grid4 = Grid4.child( this )

            whiteCount = grid.whiteCount
            redCount = grid.redCount
            blackCount = grid.blackCount
        }
        c
    }

    def deserialize( grid: Grid4, str: String ): Unit = {
        require( str.startsWith( "s4:" ) )
        require( str.endsWith( ";" ) )

        val sub = str.substring( "s4:".length, str.length - 1 )
        val parts = sub.split( ":" )
        for (part <- parts) {
            part( 0 ) match {
                case 'f' => grid.deserializeFacePart( part.substring( 1 ) )
                case 'e' => grid.deserializeEdgePart( part.substring( 1 ) )
                case 'v' => grid.asInstanceOf[VertexStateGrid4].deserializeVertexPart( part.substring( 1 ) )
                case 'c' => grid.asInstanceOf[ColoredGrid4].deserializeColorPart( part.substring( 1 ) )
                case 's' => () // size, already handled
                case 'o' => () // open, already handled
            }
        }
    }

    def fromHex( c: Char ): Int = {
        c match {
            case '0' => return 0;
            case '1' => return 1;
            case '2' => return 2;
            case '3' => return 3;
            case '4' => return 4;
            case '5' => return 5;
            case '6' => return 6;
            case '7' => return 7;
            case '8' => return 8;
            case '9' => return 9;
            case 'a' => return 10;
            case 'A' => return 10;
            case 'b' => return 11;
            case 'B' => return 11;
            case 'c' => return 12;
            case 'C' => return 12;
            case 'd' => return 13;
            case 'D' => return 13;
            case 'e' => return 14;
            case 'E' => return 14;
            case 'f' => return 15;
            case 'F' => return 15;
            case _ => throw new RuntimeException( "not hex" );
        }
    }

    def setState( grid: Grid4, str: String ): Unit = {
        if ( str.startsWith( "s4:" ) ) {
            deserialize( grid, str )
            return
        }
        var complex: Boolean = false
        var loopy: Boolean = false

        var ex: Int = str.indexOf( "x" );
        var sp: Int = str.indexOf( " " );
        if ( sp == -1 ) {
            loopy = true
            sp = str.indexOf( ":" )
        }
        var idx: Int = sp + 1;
        if ( str.charAt( sp + 1 ) == '!' ) {
            idx += 1;
            complex = true;
        }

        var a: Int = 0;
        var b: Int = 0;

        if ( loopy ) {
            var row: Int = 0
            var col: Int = 0

            def addNumber( i: Int ) {
                grid.value( grid.faces( row )( col ) ) = Some( i )
                advance()
            }

            def addSpace( c: Char ) {
                var ch: Char = c
                grid.value( grid.faces( row )( col ) ) = None
                advance()
                while (ch != 'a') {
                    ch = (ch - 1).toChar
                    grid.value( grid.faces( row )( col ) ) = None
                    advance()
                }
            }

            def advance() {
                col += 1
                if ( col == grid.cols ) {
                    col = 0
                    row += 1
                }
            }

            while (row < grid.rows) {
                str.charAt( idx ) match {
                    case '0' => addNumber( 0 )
                    case '1' => addNumber( 1 )
                    case '2' => addNumber( 2 )
                    case '3' => addNumber( 3 )
                    case '4' => addNumber( 4 )
                    case c => addSpace( c )
                }
                idx += 1
            }
        } else {

            for (i <- 0 until grid.rows) {
                for (j <- 0 until grid.cols) {
                    var value = str.charAt( idx );
                    if ( value == '.' ) {
                        grid.value( grid.faces( i )( j ) ) = None;
                    } else {
                        grid.value( grid.faces( i )( j ) ) = Some( value.toString.toInt );
                    }
                    idx += 1;
                    if ( complex ) {
                        a = fromHex( str.charAt( idx ) );
                        idx += 1
                        b = fromHex( str.charAt( idx ) );
                        idx += 1
                        // bit-order E N W S
                        if ( a % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).eastEdge ) = Black;
                        }
                        a /= 2;
                        if ( a % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).northEdge ) = Black;
                        }
                        a /= 2;
                        if ( a % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).westEdge ) = Black;
                        }
                        a /= 2;
                        if ( a % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).southEdge ) = Black;
                        }

                        if ( b % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).eastEdge ) = Red;
                        }
                        b /= 2;
                        if ( b % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).northEdge ) = Red;
                        }
                        b /= 2;
                        if ( b % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).westEdge ) = Red;
                        }
                        b /= 2;
                        if ( b % 2 == 1 ) {
                            grid.state( grid.faces( i )( j ).southEdge ) = Red;
                        }
                    }
                }
            }

        }
    }

    def fromString( str: String, gridOpenBordersOverride: Boolean = false ) = {
        val (rows, cols) = sizeFromString( str )
        val open = gridOpenBordersOverride || str.contains( ":open" )
        val grid = empty( rows, cols, open );
        setState( grid, str )
        grid;
    }

    def sizeFromString( str: String ): (Int, Int) = { // returns rows, cols
        val ex: Int = str.indexOf( "x" );
        var sp: Int = str.indexOf( " " );
        if ( sp == -1 ) sp = str.indexOf( ":", ex )
        val startIndex: Int = if ( str.startsWith( "s4:s" ) ) 4 else 0
        (str.substring( ex + 1, sp ).toInt, str.substring( startIndex, ex ).toInt)
    }

}

trait EdgeStateCheckable {
    def apply( edge: Edge4 ): EdgeState

    def update( edge: Edge4, state: EdgeState )

    def noneState: EdgeState
}

trait FaceValueCheckable {
    def apply( face: Face4 ): Option[Int]

    def update( face: Face4, value: Option[Int] )
}

trait Grid4 extends Grid4Like[Grid4] {

    /*---------------------------------------------------------------------------*
    * define these top four as pre-initialized fields, and implement newChild
    *----------------------------------------------------------------------------*/

    val rows: Int // number of rows of faces
    val cols: Int // number of columns of faces
    val parentGrid: Option[Grid4] // a parent grid if it exists
    val openBorders: Boolean // whether grid borders are open (white) or closed (red)
    def newChild: Grid4 // get a new "child" grid

    val faces: Array[Array[Face4]] // faces
    val vertices: Array[Array[Vertex4]] // vertices
    val vedges: Array[Array[VerticalEdge4]] // vertical edges
    val hedges: Array[Array[HorizontalEdge4]] // horizontal edges

    def state: EdgeStateCheckable // set and get edge states
    def value: FaceValueCheckable // set and get face values

    def allowsInvalid: Boolean // whether or not invalid grid configurations will work or throw InvalidGridExceptions

    def getState( edgeOpt: Option[Edge4] ): EdgeState // get an edge's state, but handle non-grid edges (border)

    def moves: Seq[Move] // moves from either the start or parent grid (depending on whether parent exists)

    def move( kv: (Option[Edge4], EdgeState) ): Unit // move, but more optional

    def displayString: String // display string
    def simpleString: String // simple string
    def complexString: String // complex string
    def serialString: String // serial state string

    def faceIterator: Iterator[Face4] // iterate through faces, row-major
    def vertexIterator: Iterator[Vertex4] // iterate through vertices, row-major
    def edgeIterator: Iterator[Edge4] // iterate through edges, vertical first row-major, then horizontal row-major

    def hasParent: Boolean // whether the parent exists
    def parent: Grid4 // the parent (only if it exists)
    def root: Grid4 // root grid

    var whiteCount: Int
    var redCount: Int
    var blackCount: Int

    def isComplete: Boolean // if open-bounded, then no loops and all edges filled consistently. otherwise, a puzzle soln
}

trait Grid4Like[+This <: Grid4] extends LogHelper {
    self: This =>

    var allowsInvalid: Boolean = if ( hasParent ) parent.allowsInvalid else false

    var whiteCount: Int = rows * (cols + 1) + (rows + 1) * cols
    var redCount: Int = 0
    var blackCount: Int = 0

    val faces: Array[Array[Face4]] = if ( hasParent ) parent.faces else Array.ofDim[Face4]( rows, cols ) // faces
    val vertices: Array[Array[Vertex4]] = if ( hasParent ) parent.vertices else Array.ofDim[Vertex4]( rows + 1, cols + 1 ) // vertices
    val vedges: Array[Array[VerticalEdge4]] = if ( hasParent ) parent.vedges else Array.ofDim[VerticalEdge4]( rows, cols + 1 ) // vedges
    val hedges: Array[Array[HorizontalEdge4]] = if ( hasParent ) parent.hedges else Array.ofDim[HorizontalEdge4]( rows + 1, cols ) // hedges

    val moves = new ListBuffer[Move]

    def move( kv: (Option[Edge4], EdgeState) ): Unit = {
        if ( kv._1.isDefined ) {
            state( kv._1.get ) = kv._2
        }
    }

    def copyStateTo( emptyGrid: Grid4 ): Unit = {
        for (row <- 0 to rows; col <- 0 to cols) {
            if ( (row < rows) && (col < cols) ) {
                emptyGrid.value( emptyGrid.faces( row )( col ) ) = value( faces( row )( col ) )
            }
            if ( row < rows ) {
                emptyGrid.state( emptyGrid.vedges( row )( col ) ) = state( vedges( row )( col ) )
            }
            if ( col < cols ) {
                emptyGrid.state( emptyGrid.hedges( row )( col ) ) = state( hedges( row )( col ) )
            }
        }
    }

    def displayString: String = { // adapted from AS version
        var i: Int = 0
        var j: Int = 0
        var str = new StringBuffer
        for (i <- 0 until rows) {
            for (j <- 0 until cols) {
                str append "o";
                state( hedges( i )( j ) ) match {
                    case White => str append "   ";
                    case Red => str append " x ";
                    case Black => str append "---";
                }
            }
            str append "o\n";
            for (j <- 0 until cols) {
                state( vedges( i )( j ) ) match {
                    case White => str append " ";
                    case Red => str append "x";
                    case Black => str append "|";
                }
                str append " ";
                value( faces( i )( j ) ) match {
                    case Some( 0 ) => str append "0";
                    case Some( 1 ) => str append "1";
                    case Some( 2 ) => str append "2";
                    case Some( 3 ) => str append "3";
                    case Some( 4 ) => str append "4";
                    case None => str append " ";
                }
                str append " ";
            }
            state( vedges( i )( cols ) ) match {
                case White => str append " ";
                case Red => str append "x";
                case Black => str append "|";
            }
            str append "\n";
        }
        for (j <- 0 until cols) {
            str append "o";
            state( hedges( rows )( j ) ) match {
                case White => str append "   ";
                case Red => str append " x ";
                case Black => str append "---";
            }
        }
        str append "o\n\n";
        return str.toString;
    }

    def simpleString: String = {
        val str = new StringBuilder
        str.append( cols + "x" + rows + " " )
        for (face <- faceIterator) {
            str.append( value( face ) match {
                case Some( x ) => x.toString
                case None => "."
            } )
        }
        return str.toString
    }

    def complexString: String = {
        val str = new StringBuilder
        str.append( cols + "x" + rows + " !" )
        for (face <- faceIterator) {
            str.append( value( face ) match {
                case Some( x ) => x.toString
                case None => "."
            } )
            var a: Int = 0
            var b: Int = 0
            a += (if ( state( face.eastEdge ) == Black ) 1 else 0)
            a += (if ( state( face.northEdge ) == Black ) 2 else 0)
            a += (if ( state( face.westEdge ) == Black ) 4 else 0)
            a += (if ( state( face.southEdge ) == Black ) 8 else 0)
            b += (if ( state( face.eastEdge ) == Red ) 1 else 0)
            b += (if ( state( face.northEdge ) == Red ) 2 else 0)
            b += (if ( state( face.westEdge ) == Red ) 4 else 0)
            b += (if ( state( face.southEdge ) == Red ) 8 else 0)
            str.append( Integer.toHexString( a ).toLowerCase )
            str.append( Integer.toHexString( b ).toLowerCase )

        }
        return str.toString
    }

    override def toString = serialString

    def serialString: String = partSerialString + ";"

    protected def partSerialString: String = {
        val str = new StringBuilder
        str.append( "s4" )
        str.append( ":s" + cols + "x" + rows )
        if ( openBorders ) {
            str.append( ":open" )
        }
        str.append( ":f" )
        for (face <- faceIterator) {
            str.append( value( face ) match {
                case Some( x ) => x.toString
                case None => "."
            } )
        }
        //        if ( redCount > 0 || blackCount > 0 ) {
        str.append( ":e" )
        for (edge <- edgeIterator) {
            str.append( state( edge ) match {
                case White => "."
                case Red => "x"
                case Black => "-"
            } )
        }
        //        }
        return str.toString
    }

    def deserializeFacePart( str: String ) {
        var idx = 0
        for (face <- faceIterator) {
            str( idx ) match {
                case '.' => value( face ) = None
                case '0' => value( face ) = Some( 0 )
                case '1' => value( face ) = Some( 1 )
                case '2' => value( face ) = Some( 2 )
                case '3' => value( face ) = Some( 3 )
                case '4' => value( face ) = Some( 4 )
            }
            idx += 1
        }
    }

    def deserializeEdgePart( str: String ) {
        var idx = 0
        for (edge <- edgeIterator) {
            str( idx ) match {
                case '.' => state( edge ) = White
                case 'x' => state( edge ) = Red
                case '-' => state( edge ) = Black
            }
            idx += 1
        }
    }

    def getState( edgeOpt: Option[Edge4] ): EdgeState = {
        if ( edgeOpt.isDefined ) state( edgeOpt.get ) else state.noneState
    }

    def faceIterator: Iterator[Face4] = new MultiArrayIterator[Face4]( faces, rows, cols ) // faces, row-major. DO NOT CHANGE ORDER
    def vertexIterator: Iterator[Vertex4] = new MultiArrayIterator[Vertex4]( vertices, rows + 1, cols + 1 ) // vertices, same order
    def edgeIterator: Iterator[Edge4] = (new MultiArrayIterator[VerticalEdge4]( vedges, rows, cols + 1 )) ++ (new MultiArrayIterator[HorizontalEdge4]( hedges, rows + 1, cols ))

    private var edgeStateListeners = Set.empty[EdgeStateListener]

    def addEdgeStateListener( listener: EdgeStateListener ): Unit = {
        edgeStateListeners = edgeStateListeners + listener
    }

    def removeEdgeStateListener( listener: EdgeStateListener ): Unit = {
        edgeStateListeners = edgeStateListeners - listener
    }

    def notifyEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) {
        edgeStateListeners.foreach( _.onEdgeStateChange( edge, oldState, newState ) )
    }

    private var faceValueListeners = Set.empty[FaceValueListener]

    def addFaceValueListener( listener: FaceValueListener ): Unit = {
        faceValueListeners = faceValueListeners + listener
    }

    def removeFaceValueListener( listener: FaceValueListener ): Unit = {
        faceValueListeners = faceValueListeners - listener
    }

    def notifyFaceValueChange( face: Face4, oldValue: Option[Int], newValue: Option[Int] ) {
        faceValueListeners.foreach( _.onFaceValueChange( face, oldValue, newValue ) )
    }

    def hasParent: Boolean = parentGrid.isDefined

    def parent: Grid4 = parentGrid.get

    def root: Grid4 = if ( hasParent ) parent.root else this

    if ( !hasParent ) { // initialize
        for ( // create vertices
            row <- 0 until rows + 1;
            col <- 0 until cols + 1
        ) {
            vertices( row )( col ) = new Vertex4( row, col )
        }

        for ( // create edges, hook
            row <- 0 until rows + 1;
            col <- 0 until cols + 1
        ) {
            if ( row < rows ) {
                val edge: VerticalEdge4 = new VerticalEdge4( row, col )
                vedges( row )( col ) = edge

                edge.northVertex = vertices( row )( col )
                edge.southVertex = vertices( row + 1 )( col )

                vertices( row )( col ).southEdge = Some( edge )
                vertices( row + 1 )( col ).northEdge = Some( edge )
            }
            if ( col < cols ) {
                val edge: HorizontalEdge4 = new HorizontalEdge4( row, col )
                hedges( row )( col ) = edge

                edge.westVertex = vertices( row )( col )
                edge.eastVertex = vertices( row )( col + 1 )

                vertices( row )( col ).eastEdge = Some( edge )
                vertices( row )( col + 1 ).westEdge = Some( edge )
            }
        }

        for ( // create faces, hook to vertices
            row <- 0 until rows;
            col <- 0 until cols
        ) {
            val face: Face4 = new Face4( row, col )
            faces( row )( col ) = face

            face.northwestVertex = vertices( row )( col )
            face.northeastVertex = vertices( row )( col + 1 )
            face.southeastVertex = vertices( row + 1 )( col + 1 )
            face.southwestVertex = vertices( row + 1 )( col )

            vertices( row )( col ).southeastFace = Some( face )
            vertices( row )( col + 1 ).southwestFace = Some( face )
            vertices( row + 1 )( col + 1 ).northwestFace = Some( face )
            vertices( row + 1 )( col ).northeastFace = Some( face )

            face.westEdge = vedges( row )( col )
            face.eastEdge = vedges( row )( col + 1 )
            face.northEdge = hedges( row )( col )
            face.southEdge = hedges( row + 1 )( col )

            vedges( row )( col ).eastFace = Some( face )
            vedges( row )( col + 1 ).westFace = Some( face )
            hedges( row )( col ).southFace = Some( face )
            hedges( row + 1 )( col ).northFace = Some( face )
        }
    }

    def otherBlackEdge( vertex: Vertex4, edge: Edge4 ): Option[Edge4] = {
        require( state( edge ) == Black )
        val others = vertex.realEdges.filter( (e) => (this.state( e ) == Black) && (e != edge) )
        others.size match {
            case 0 => None
            case 1 => Some( others.head )
            case _ => throw new InvalidGridException // too many black edges inbound
        }
    }

    def isComplete: Boolean = {
        if ( openBorders && (whiteCount > 0) ) return false

        if ( !areFacesComplete ) return false

        if ( openBorders ) {
            // TODO: add mixin that keeps paths and full paths up-to-date for faster completeness checking
            return innerVerticesOK && !hasLoops
        } else {
            // find a black edge
            edgeIterator.find( state( _ ) == Black ) match {
                case None => return true // empty grid OK
                case Some( startEdge ) => {
                    // get total number of black edges, compare with tracing one path
                    val totalBlackCount: Int = edgeIterator.count( this.state( _ ) == Black )
                    val pathBlackCount: Int = Path4.createBlackLightPath( this, startEdge.va ).length
                    return pathBlackCount == totalBlackCount
                }
            }
        }
    }

    def innerVerticesOK: Boolean = {
        for (vertex <- vertexIterator) {
            if ( (vertex.row > 0) && (vertex.col > 0) && (vertex.row < rows) && (vertex.col < cols) ) {
                val count = vertex.realEdges.count( state( _ ) == Black )
                if ( (count != 0) && (count != 2) ) {
                    return false
                }
            }
        }
        true
    }

    def areFacesComplete: Boolean = {
        for (face <- faceIterator; if this.value( face ).isDefined) {
            val blackCount = face.edges.count( this.getState( _ ) == Black )
            if ( blackCount != value( face ).get ) {
                return false
            }
        }
        return true;
    }

    def hasLoops: Boolean = {
        // TODO: optimize!
        var edgeSet: Set[Edge4] = edgeIterator.toSet.filter( this.state( _ ) == Black )
        while (!edgeSet.isEmpty) {
            val path = Path4.createBlackFullPath( this, edgeSet.head.va )
            if ( path.isLoop ) return true

            for (edge <- path.edges) {
                edgeSet = edgeSet - edge
            }
        }
        return false
    }

    protected def edgeIdx( edge: Edge4 ): Int = {
        if ( edge.isVertical ) {
            edge.row * (cols + 1) + edge.col
        } else {
            (rows * (cols + 1)) + edge.row * cols + edge.col
        }
    }

    protected def faceIdx( face: Face4 ): Int = face.row * cols + face.col

    protected def vertexIdx( vertex: Vertex4 ): Int = vertex.row * (cols + 1) + vertex.col

    def compareTo( grid: Grid4 ): Int = {
        // compare dimensions
        if ( new Integer( rows ).compareTo( grid.rows ) != 0 ) return new Integer( rows ).compareTo( grid.rows )
        if ( new Integer( cols ).compareTo( grid.cols ) != 0 ) return new Integer( cols ).compareTo( grid.cols )
        // compare face values
        for (row <- 0 until rows; col <- 0 until cols) {
            val ret = (value( faces( row )( col ) ), grid.value( grid.faces( row )( col ) )) match {
                case (None, None) => 0
                case (None, Some( x )) => -1
                case (Some( x ), None) => 1
                case (Some( state1: Int ), Some( state2: Int )) => new Integer( state1 ).compareTo( state2 )
            }
            if ( ret != 0 ) return ret
        }
        // compare edge values
        for (row <- 0 to rows; col <- 0 to cols) {
            def getRet( a: Edge4, b: Edge4 ): Int = (state( a ), grid.state( b )) match {
                case (x, y) if x == y => 0
                case (White, _) => -1
                case (_, White) => 1
                case (Red, _) => -1
                case (_, Red) => 1
            }
            if ( row < rows ) {
                // vertical
                val ret = getRet( vedges( row )( col ), grid.vedges( row )( col ) )
                if ( ret != 0 ) return ret
            }
            if ( col < cols ) {
                val ret = getRet( hedges( row )( col ), grid.hedges( row )( col ) )
                if ( ret != 0 ) return ret
            }
        }
        return 0 // rely on subclasses to decide. otherwise equal
    }
}

trait EdgeStateListener {
    def onEdgeStateChange( edge: Edge4, oldState: EdgeState, newState: EdgeState ) {

    }
}

trait FaceValueListener {
    def onFaceValueChange( face: Face4, oldValue: Option[Int], newValue: Option[Int] ) {

    }
}
