package com.jonathanolson.graphics.raster

import com.jonathanolson.color.{Color, Colorspace}
import com.jonathanolson.math.{Point2i, Point3d}

class ImageRaster2(
        val width: Int,
        val height: Int,
        val space: Colorspace
        ) extends ColorRaster2 with RectangleRaster[Color] with MutableRaster2[Color] with ImageRaster {
    def this(width: Int, height: Int, space: Colorspace, f: (Int, Int) => Color) = {
        this (width, height, space)
        initialize(f)
    }

    /**
     * We wrap this Point3d model
     * TODO: wrap arbitrary?
     */
    private[this] val pointRaster = new DefaultPoint3dRaster2(width, height)

    def apply(x: Int, y: Int): Color = new Color(pointRaster.apply(x, y), space)

    def update(x: Int, y: Int, e: Color) = pointRaster.update(x, y, space.convert(e))

    def map(f: (Color) => Color) = new ImageRaster2(width, height, space,
        (x: Int, y: Int) => f(this(x, y))
        )

    def map2(raster: DoubleRaster2, f: (Color, Double) => Color) = new ImageRaster2(width, height, space,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def map2[A <: Point3d](raster: Point3dRaster2[A], f: (Color, A) => Color) = new ImageRaster2(width, height, space,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def map2(raster: ImageRaster2, f: (Color, Color) => Color) = new ImageRaster2(width, height, space,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def +[A <: Point3d](raster: Point3dRaster2[A]): ImageRaster2 = map2(raster, (a: Color, b: A) => a + b)

    def +(raster: ColorRaster2): ImageRaster2 = map2(raster, (a: Color, b: Color) => a + b)

    def -[A <: Point3d](raster: Point3dRaster2[A]): ImageRaster2 = map2(raster, (a: Color, b: A) => a - b)

    def -(raster: ColorRaster2): ImageRaster2 = map2(raster, (a: Color, b: Color) => a - b)

    def +(raster: DoubleRaster2): ImageRaster2 = map2(raster, (a: Color, b: Double) => a + b)

    def -(raster: DoubleRaster2): ImageRaster2 = map2(raster, (a: Color, b: Double) => a - b)

    def *(raster: DoubleRaster2): ImageRaster2 = map2(raster, (a: Color, b: Double) => a * b)

    def /(raster: DoubleRaster2): ImageRaster2 = map2(raster, (a: Color, b: Double) => a / b)

    def +(d: Double) = map((x: Color) => x + d)

    def -(d: Double) = map((x: Color) => x - d)

    def *(d: Double) = map((x: Color) => x * d)

    def /(d: Double) = map((x: Color) => x / d)

    def fit(inLow: Double, inHigh: Double, outLow: Double, outHigh: Double): ImageRaster2 = map((color: Color) => color.fit(inLow, inHigh, outLow, outHigh))

    def fit(low: Double, high: Double): ImageRaster2 = {
        val oldLow = foldLeft(java.lang.Double.POSITIVE_INFINITY)((top: Double, c: Color) => top.min(c.x).min(c.y).min(c.z))
        val oldHigh = foldLeft(java.lang.Double.NEGATIVE_INFINITY)((top: Double, c: Color) => top.max(c.x).max(c.y).max(c.z))
        fit(oldLow, oldHigh, low, high)
    }

    def minValue = foldLeft(java.lang.Double.POSITIVE_INFINITY)((top: Double, c: Color) => top.min(c.x).min(c.y).min(c.z))

    def maxValue = foldLeft(java.lang.Double.NEGATIVE_INFINITY)((top: Double, c: Color) => top.max(c.x).max(c.y).max(c.z))

    def convolve(kernel: DoubleRaster2 with RectangleRaster[Double]): ImageRaster2 = {
        require(kernel.width % 2 == 1)
        require(kernel.height % 2 == 1)
        convolve(kernel, new Point2i((kernel.width - 1) / 2, (kernel.height - 1) / 2))
    }

    def convolve(kernel: DoubleRaster2 with RectangleRaster[Double], kernelOffset: Point2i): ImageRaster2 = {
        var ret: ImageRaster2 = new ImageRaster2(width, height, space)
        for (index <- kernel.indices) {
            val tmpRaster = new ImageRaster2Wrapper(this) with OffsetRaster2[Color] {
                // offset the copy
                val xOffset = kernelOffset.x - index._1
                val yOffset = kernelOffset.y - index._2

                // scale it by
                override def apply(x: Int, y: Int) = super.apply(x, y) * kernel(index)
            }
            ret = ret + tmpRaster
        }
        ret
    }

    def sobel: ImageRaster2 = {
        // use separable filters
        val h1 = Raster.createDouble2(List(List(1), List(2), List(1)))
        val h2 = Raster.createDouble2(List(List(1, 0, -1)))
        val v1 = Raster.createDouble2(List(List(1), List(0), List(-1)))
        val v2 = Raster.createDouble2(List(List(1, 2, 1)))

        val hRaster: ImageRaster2 = convolve(h1).convolve(h2)
        val vRaster: ImageRaster2 = convolve(v1).convolve(v2)

        // compute the L2 norm between the two rasters. Per channel
        hRaster.map2(vRaster, (a: Color, b: Color) => new Color(
            scala.math.sqrt(a.x * a.x + b.x * b.x),
            scala.math.sqrt(a.y * a.y + b.y * b.y),
            scala.math.sqrt(a.z * a.z + b.z * b.z),
            space
            ))
    }
}
