package com.jonathanolson.graphics.raster

trait DoubleRaster2 extends Raster2[ Double ] {
    def +( raster: DoubleRaster2 ): DoubleRaster2

    def -( raster: DoubleRaster2 ): DoubleRaster2

    def *( raster: DoubleRaster2 ): DoubleRaster2

    def /( raster: DoubleRaster2 ): DoubleRaster2

    def +( d: Double ): DoubleRaster2

    def -( d: Double ): DoubleRaster2

    def *( d: Double ): DoubleRaster2

    def /( d: Double ): DoubleRaster2
}
