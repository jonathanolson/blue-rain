package com.jonathanolson.graphics.raster

import com.jonathanolson.math.Point3d

class ImageRaster2Wrapper( val raster: ImageRaster2 ) extends ColorRaster2 {
    val space = raster.space

    def overflowIndex( x: Int, y: Int ) = raster.overflowIndex( x, y )

    def contains( x: Int, y: Int ) = raster.contains( x, y )

    def indices = raster.indices

    def apply( x: Int, y: Int ) = raster.apply( x, y )

    def +( d: Double ) = raster + d

    def -( d: Double ) = raster - d

    def *( d: Double ) = raster * d

    def /( d: Double ) = raster / d

    def +( other: DoubleRaster2 ) = raster + other

    def -( other: DoubleRaster2 ) = raster - other

    def *( other: DoubleRaster2 ) = raster * other

    def /( other: DoubleRaster2 ) = raster / other

    def +[ A <: Point3d ]( other: Point3dRaster2[ A ] ) = raster + other

    def -[ A <: Point3d ]( other: Point3dRaster2[ A ] ) = raster - other
}
