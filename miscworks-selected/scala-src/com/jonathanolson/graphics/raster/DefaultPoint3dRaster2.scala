package com.jonathanolson.graphics.raster

import com.jonathanolson.math.Point3d

class DefaultPoint3dRaster2(
        val width: Int,
        val height: Int
        ) extends Point3dRaster2[Point3d] with RectangleRaster[Point3d] with MutableRaster2[Point3d] {
    def this(width: Int, height: Int, f: (Int, Int) => Point3d) = {
        this (width, height)
        initialize(f)
        for (index <- indices) {
            update(index, f(index._1, index._2))
        }
    }

    private[this] val data: Array[Double] = new Array[Double](width * height * 3)

    def apply(x: Int, y: Int): Point3d = {
        require(x >= 0)
        require(y >= 0)
        require(x < width)
        require(y < height)
        val idx = index(x, y)
        new Point3d(data(idx), data(idx + 1), data(idx + 2))
    }

    def update(x: Int, y: Int, e: Point3d) = {
        val idx = index(x, y)

        data(idx) = e.x
        data(idx + 1) = e.y
        data(idx + 2) = e.z
    }

    def map(f: (Point3d) => Point3d): DefaultPoint3dRaster2 = new DefaultPoint3dRaster2(width, height,
        (x: Int, y: Int) => f(this(x, y))
        )

    def map2(raster: DoubleRaster2, f: (Point3d, Double) => Point3d) = new DefaultPoint3dRaster2(width, height,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def map2[A <: Point3d](raster: Point3dRaster2[A], f: (Point3d, A) => Point3d) = new DefaultPoint3dRaster2(width, height,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def +[A <: Point3d](raster: Point3dRaster2[A]) = map2(raster, (a: Point3d, b: A) => a + b)

    def +(raster: DoubleRaster2) = map2(raster, (a: Point3d, b: Double) => a + b)

    def -[A <: Point3d](raster: Point3dRaster2[A]) = map2(raster, (a: Point3d, b: A) => a - b)

    def -(raster: DoubleRaster2) = map2(raster, (a: Point3d, b: Double) => a - b)

    def *(raster: DoubleRaster2) = map2(raster, (a: Point3d, b: Double) => a * b)

    def /(raster: DoubleRaster2) = map2(raster, (a: Point3d, b: Double) => a / b)

    def +(d: Double) = map((x: Point3d) => x + d)

    def -(d: Double) = map((x: Point3d) => x - d)

    def *(d: Double) = map((x: Point3d) => x * d)

    def /(d: Double) = map((x: Point3d) => x / d)

    /**
     * For indexing into the data array
     */
    private[this] def index(x: Int, y: Int) = (x + y * width) * 3
}
