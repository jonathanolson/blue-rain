package com.jonathanolson.graphics.raster

/**
 * A 2-d raster. Doesn't have to be rectangular
 */
trait Raster2[+T] extends Raster[T] with Traversable[T] {
    // TODO: redo better for 2.8
    def apply(x: Int, y: Int): T

    final def apply(t: Tuple2[Int, Int]): T = apply(t._1, t._2)

    def indices: Iterator[Tuple2[Int, Int]]

    def contains(x: Int, y: Int): Boolean

    final def contains(t: Tuple2[Int, Int]): Boolean = contains(t._1, t._2)

    def elements: Iterator[T] = new Iterator[T] {
        val indexIterator = indices

        def next() = apply(indexIterator.next)

        def hasNext = indexIterator.hasNext
    }

    def foreach[U](f: (T) => U) = elements.foreach(f)

    /**
     * The index that is "closest" to the specified index
     */
    def overflowIndex(x: Int, y: Int): Tuple2[Int, Int]

}
