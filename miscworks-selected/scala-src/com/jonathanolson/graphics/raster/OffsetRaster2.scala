package com.jonathanolson.graphics.raster

trait OffsetRaster2[ +T ] extends Raster2[ T ] {
    val xOffset: Int
    val yOffset: Int

    abstract override def apply( x: Int, y: Int ) = {
        if ( contains( x + xOffset, y + yOffset ) ) {
            super.apply( x + xOffset, y + yOffset )
        } else {
            val mapped: Tuple2[ Int, Int ] = overflowIndex( x, y )
            super.apply( mapped._1, mapped._2 )
        }
    }
}
