package com.jonathanolson.graphics.raster

import com.jonathanolson.math.Point3d
import com.jonathanolson.color.{Color, Colorspace}

trait Raster[ +T ]

object Raster {
    def createDouble2( width: Int, height: Int ) = new DefaultDoubleRaster2( width, height )

    def createDouble2( width: Int, height: Int, f: (Int, Int) => Double ) = new DefaultDoubleRaster2( width, height, f )

    /**
     * Create a raster of doubles from the row-major nested list format
     */
    def createDouble2( dat: List[ List[ Double ] ] ) = new DefaultDoubleRaster2( dat )

    def createPoint3d2( width: Int, height: Int ) = new DefaultPoint3dRaster2( width, height )

    def createPoint3d2( width: Int, height: Int, f: (Int, Int) => Point3d ) = new DefaultPoint3dRaster2( width, height, f )

    def createColor2( width: Int, height: Int, space: Colorspace ) = new ImageRaster2( width, height, space )

    def createColor2( width: Int, height: Int, space: Colorspace, f: (Int, Int) => Color ) = new ImageRaster2( width, height, space, f )
}
