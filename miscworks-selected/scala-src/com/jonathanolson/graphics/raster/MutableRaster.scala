package com.jonathanolson.graphics.raster

trait MutableRaster[ T ] extends Raster[ T ] {
    def update( x: Int, y: Int, e: T ): Unit

    final def update( t: Tuple2[ Int, Int ], e: T ): Unit = update( t._1, t._2, e )

}

















