package com.jonathanolson.graphics.raster

import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File
import com.jonathanolson.color.{Colorspace, RGBColorspace, Color}
import com.jonathanolson.math.{Point2i, Point3d, Point3i}

trait MutableRaster2[ T ] extends Raster2[ T ] with MutableRaster[ T ] {
    def initialize( f: (Int, Int) => T ) = {
        for ( index <- indices ) {
            update( index, f( index._1, index._2 ) )
        }
    }
}

















