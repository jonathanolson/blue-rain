package com.jonathanolson.graphics.raster

trait RectangleRaster[ +T ] extends Raster2[ T ] {
    val width: Int
    val height: Int

    // TODO: add builder factories, update collections

    def contains( x: Int, y: Int ) = ( x >= 0 ) && ( y >= 0 ) && ( x < width ) && ( y < height )

    def overflowIndex( x: Int, y: Int ) = (x.max( 0 ).min( width ), y.max( 0 ).min( height ))

    def rowIndices( y: Int ): Iterator[ Tuple2[ Int, Int ] ] = new Iterator[ Tuple2[ Int, Int ] ] {
        var x = 0;

        def next() = {
            val idx = (x, y)
            x += 1
            idx
        }

        def hasNext = x < width
    }

    def columnIndices( x: Int ): Iterator[ Tuple2[ Int, Int ] ] = new Iterator[ Tuple2[ Int, Int ] ] {
        var y = 0;

        def next() = {
            val idx = (x, y)
            y += 1
            idx
        }

        def hasNext = y < height
    }

    def rowIterator: Iterator[ Iterator[ Tuple2[ Int, Int ] ] ] = new Iterator[ Iterator[ Tuple2[ Int, Int ] ] ] {
        var y = 0;

        def next() = {
            val iter = rowIndices( y )
            y += 1
            iter
        }

        def hasNext = y < height
    }

    def columnIterator: Iterator[ Iterator[ Tuple2[ Int, Int ] ] ] = new Iterator[ Iterator[ Tuple2[ Int, Int ] ] ] {
        var x = 0;

        def next() = {
            val iter = columnIndices( x )
            x += 1
            iter
        }

        def hasNext = x < width
    }

    def indices: Iterator[ Tuple2[ Int, Int ] ] = new Iterator[ Tuple2[ Int, Int ] ] {
        var x = 0;
        var y = 0;

        def next() = {
            val idx = (x, y)
            if ( x < width - 1 ) x += 1 else {x = 0; y += 1}
            idx
        }

        def hasNext = y < height
    }

    def dumpString = {
        val columns = 0 to ( width - 1 )
        val rows = 0 to ( height - 1 )

        val colWidths = columns.map( (col: Int) => {
            ( 0 /: rows )( (total: Int, row: Int) => total max ( apply( col, row ).toString.length ) )
        } )

        rows.map( (row: Int) => {
            columns.map( (col: Int) => {
                val str: String = apply( col, row ).toString
                str + " " * ( colWidths( col ) - str.length )
            } ).mkString( " " )
        } ).mkString( "\n" )
    }
}
