package com.jonathanolson.graphics.raster

import com.jonathanolson.math.Point3i
import com.jonathanolson.color.{Color, RGBColorspace}
import java.io.File
import javax.imageio.ImageIO
import java.awt.image.BufferedImage

trait ImageRaster extends RectangleRaster[ Color ] {
    def writeToFile( file: File ): Unit = writeToFile( file, "png" )

    def writeToFile( file: File, filetype: String ): Unit = {
        ImageIO.write( toBufferedImage, filetype, file )
    }

    def toBufferedImage: BufferedImage = {
        val image: BufferedImage = new BufferedImage( width, height, BufferedImage.TYPE_INT_RGB )
        for ( index <- indices ) {
            image.getRaster.setPixel( index._1, index._2, apply( index ).toIntRGB )
        }
        image
    }
}

object ImageRaster {
    def fromFile( file: File ) = fromBufferedImage( ImageIO.read( file ) )

    def fromBufferedImage( image: BufferedImage ): ImageRaster2 = {
        val space = RGBColorspace.Default
        new ImageRaster2( image.getWidth, image.getHeight, space,
            (x: Int, y: Int) => {
                val arr: Array[ Int ] = new Array[ Int ]( 4 )
                image.getRaster.getPixel( x, y, arr )
                space.fromQuantizedInt( new Point3i( arr( 0 ), arr( 1 ), arr( 2 ) ) )
            }
            )
    }
}
