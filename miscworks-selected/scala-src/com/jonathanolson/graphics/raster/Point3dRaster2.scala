package com.jonathanolson.graphics.raster

import com.jonathanolson.math.Point3d

trait Point3dRaster2[ +T <: Point3d ] extends Raster2[ T ] {
    def +[ A <: Point3d ]( raster: Point3dRaster2[ A ] ): Point3dRaster2[ Point3d ]

    def -[ A <: Point3d ]( raster: Point3dRaster2[ A ] ): Point3dRaster2[ Point3d ]

    def +( raster: DoubleRaster2 ): Point3dRaster2[ Point3d ]

    def -( raster: DoubleRaster2 ): Point3dRaster2[ Point3d ]

    def *( raster: DoubleRaster2 ): Point3dRaster2[ Point3d ]

    def /( raster: DoubleRaster2 ): Point3dRaster2[ Point3d ]

    def +( d: Double ): Point3dRaster2[ Point3d ]

    def -( d: Double ): Point3dRaster2[ Point3d ]

    def *( d: Double ): Point3dRaster2[ Point3d ]

    def /( d: Double ): Point3dRaster2[ Point3d ]

}
