package com.jonathanolson.graphics.raster

class DefaultDoubleRaster2(
        width: Int,
        height: Int
        ) extends SimpleRaster2[Double](width, height) with DoubleRaster2 {
    def this(width: Int, height: Int, f: (Int, Int) => Double) = {
        this (width, height)
        initialize(f)
    }

    def this(dat: List[List[Double]]) = this (dat(0).length, dat.length, (x: Int, y: Int) => dat(y)(x))

    def map(f: (Double) => Double) = new DefaultDoubleRaster2(width, height,
        (x: Int, y: Int) => f(this(x, y))
        )

    def map2(raster: DoubleRaster2, f: (Double, Double) => Double) = new DefaultDoubleRaster2(width, height,
        (x: Int, y: Int) => if (raster.contains(x, y)) f(this(x, y), raster(x, y)) else this(x, y)
        )

    def +(raster: DoubleRaster2) = map2(raster, _ + _)

    def -(raster: DoubleRaster2) = map2(raster, _ - _)

    def *(raster: DoubleRaster2) = map2(raster, _ * _)

    def /(raster: DoubleRaster2) = map2(raster, _ / _)

    def +(d: Double) = map((x: Double) => x + d)

    def -(d: Double) = map((x: Double) => x - d)

    def *(d: Double) = map((x: Double) => x * d)

    def /(d: Double) = map((x: Double) => x / d)
}
