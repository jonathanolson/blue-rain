package com.jonathanolson.graphics.raster

import collection.mutable.ArrayBuffer

/**
 * Simple rectangle raster implementation
 */
class SimpleRaster2[ T ](
        val width: Int,
        val height: Int
        ) extends RectangleRaster[ T ] with MutableRaster2[ T ] {
    def this( width: Int, height: Int, f: (Int, Int) => T ) = {
        this ( width, height )
        initialize( f )
    }

    private[ this ] val data: ArrayBuffer[ T ] = new ArrayBuffer[ T ]( width * height )

    def apply( x: Int, y: Int ): T = data( index( x, y ) )

    def update( x: Int, y: Int, e: T ): Unit = data( index( x, y ) ) = e

    /**
     * For indexing into the data array
     */
    private[ this ] def index( x: Int, y: Int ) = x + y * width
}
