package com.jonathanolson.graphics.raster

import com.jonathanolson.color.{Colorspace, Color}

trait ColorRaster2 extends Point3dRaster2[ Color ] {
    val space: Colorspace
}
