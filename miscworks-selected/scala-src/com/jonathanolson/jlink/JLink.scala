package com.jonathanolson.jlink

import com.wolfram.jlink._
import javax.swing.{JFrame, JPanel}
import java.awt.image.BufferedImage
import java.awt.{Image, GridLayout, Dimension}
import com.jonathanolson.graphics.raster.ImageRaster

/*---------------------------------------------------------------------------*
* Used for running Mathematica code from Java
*----------------------------------------------------------------------------*/

trait Expression {
    def expr: Expr

    def +(e: Expression): Expression = Fun('Plus, this, e)

    def -(e: Expression): Expression = this + Fun('Minus, e)

    def *(e: Expression): Expression = Fun('Times, this, e)

    def /(e: Expression): Expression = Fun('Divide, this, e) //this * Fun( 'Power, e, -1 )

    def ^(e: Expression): Expression = Fun('Power, this, e)

    def cross(e: Expression): Expression = Fun('Cross, this, e)

    def unary_-(): Expression = Fun('Minus, this)

    def unary_!(): Expression = Fun('Factorial, this)

    /*---------------------------------------------------------------------------*
    * symbol forms
    *----------------------------------------------------------------------------*/

    def +(s: Symbol): Expression = this + Sym(s)

    def -(s: Symbol): Expression = this - Sym(s)

    def *(s: Symbol): Expression = this * Sym(s)

    def /(s: Symbol): Expression = this / Sym(s)

    def ^(s: Symbol): Expression = this ^ Sym(s)
}

case class MInt(i: Int) extends Expression {
    def expr = new Expr(i)
}

case class MDouble(v: Double) extends Expression {
    def expr = new Expr(v)
}

case class Sym(s: Symbol) extends Expression {
    def expr = new Expr(Expr.SYMBOL, s.name)
}

class FunBase(s: Symbol, exprs: Expression*) extends Expression {
    def expr = new Expr(new Expr(Expr.SYMBOL, s.name), exprs.map(_ expr).toArray)
}

case class Fun(s: Symbol, exprs: Expression*) extends FunBase(s, exprs: _*)

case class Li(listExpressions: Expression*) extends FunBase('List, listExpressions: _*)

case class D(a: Expression, b: Expression) extends FunBase('D, a, b)

object JLinkHelper {
    implicit def expressionToExpr(e: Expression): Expr = e.expr

    implicit def doubleToExpression(v: Double): MDouble = new MDouble(v)

    implicit def intToExpression(v: Int): MInt = new MInt(v)

    implicit def symbolToSymbol(s: Symbol): Sym = new Sym(s)

    implicit def exprToRichExpr(e: Expr): RichExpr = new RichExpr(e)
}

class JLink {
    val ml: KernelLink = try {
        MathLinkFactory.createKernelLink("-linkmode launch -linkname 'math -mathlink'")
    } catch {
        case e: MathLinkException => {
            e.printStackTrace
            null
        }
    }
    ml.discardAnswer

    def eval(e: Expression): Expr = {
        ml.evaluate(e.expr)
        ml.waitForAnswer
        ml.getExpr
    }

    def eval(str: String): Expr = {
        ml.evaluate(str)
        ml.waitForAnswer
        ml.getExpr
    }

    def close() = ml.close

    /**
     * Display a frame with the graphic
     */
    def show(str: String): Unit = {
        var frame: JFrame = new JFrame(if (str.length < 50) str else str.substring(0, 50))
        val pane: JPanel = new JPanel(new GridLayout(1, 1))
        frame.setContentPane(pane)
        pane.add(commandPanel(str))

        frame.setMinimumSize(new Dimension(300, 200))

        frame.pack();
        frame.setVisible(true)
    }

    /**
     * Get a component with the graphic
     */
    def commandPanel(str: String): MathGraphicsJPanel = {
        val graphicsPanel: MathGraphicsJPanel = new MathGraphicsJPanel(ml)
        graphicsPanel.setImageType(MathGraphicsJPanel.TYPESET)
        graphicsPanel.setMathCommand(str)
        graphicsPanel.setPreferredSize(new Dimension(graphicsPanel.getImage.getWidth(graphicsPanel), graphicsPanel.getImage.getHeight(graphicsPanel)))
        graphicsPanel
    }

    /**
     * Get a raster of the graphic
     */
    def commandRaster(str: String): ImageRaster = {
        val graphicsPanel: MathGraphicsJPanel = new MathGraphicsJPanel(ml)
        graphicsPanel.setImageType(MathGraphicsJPanel.TYPESET)
        graphicsPanel.setMathCommand(str)
        val image: Image = graphicsPanel.getImage
        val tmpImage: BufferedImage = new BufferedImage(image.getWidth(graphicsPanel), image.getHeight(graphicsPanel), BufferedImage.TYPE_INT_RGB)
        tmpImage.getGraphics.drawImage(image, 0, 0, graphicsPanel)
        ImageRaster.fromBufferedImage(tmpImage)
    }

}

/**
 * Singleton instance
 */
object JLink extends JLink {
    Runtime.getRuntime.addShutdownHook(new Thread() {
        override def run = {
            realclose()
        }
    })

    private def realclose() = super.close

    override def close() = ()
}

object MathTest {
    def debug(result: Expr): String = {
        var str: String = ""

        if (result.numberQ) str += ("numberQ ")
        if (result.integerQ) str += ("integerQ ")
        if (result.bigDecimalQ) str += ("bigDecimalQ ")
        if (result.bigIntegerQ) str += ("bigIntegerQ ")
        if (result.realQ) str += ("realQ ")
        if (result.rationalQ) str += ("rationalQ ")
        if (result.listQ) str += ("listQ ")
        if (result.matrixQ) str += ("matrixQ ")
        if (result.stringQ) str += ("stringQ ")
        if (result.symbolQ) str += ("symbolQ ")
        if (result.trueQ) str += ("trueQ ")
        if (result.vectorQ) str += ("vectorQ ")

        str
    }

    def dump(expr: Expr, level: Int): Unit = {
        val str: String = if (expr.length == 0) {
            expr.toString
        } else {
            expr.part(0).toString
        }
        println(("   " * level) + str + "   " + debug(expr))
        if (expr.length > 0) {
            for (i <- 1 to expr.length) {
                dump(expr.part(i), level + 1)
            }
        }
    }

    def testdump(ml: KernelLink, cmd: String): Unit = {
        ml.evaluate(cmd)
        ml.waitForAnswer
        val result = ml.getExpr
        println(cmd + ": " + result)
        dump(result, 0)
        println()
    }

    def main(args: Array[String]) {

        import JLinkHelper._

        val m = new JLink

        println(m.eval(Fun('Integrate, 'x, Li('x, 0, 1))))
        println(m.eval(Fun('Inverse, Li(Li(1, 2), Li(3, 4)))))
        println(m.eval("x - y"))
        println(m.eval("x / y"))
        println(m.eval("x ^ y"))
        println(m.eval(!MInt(5)))

        println(m.eval(5 + 'x + 2 * 'y))
        println(m.eval('x * 'y ^ 'z))

        println(m.eval("Set[x,5]"))
        println(m.eval("x"))

        //        while ( true ) {
        //            val s: String = Console.in.readLine
        //            m.show( s );
        //        }

        m.close()

    }
}

class RichExpr(val raw: Expr) extends Seq[RichExpr] {

    import JLinkHelper._

    def isAtomic = raw.length == 0

    def iterator = new Iterator[RichExpr] {
        var idx = 1

        def next() = {
            val ret = new RichExpr(raw.part(idx))
            idx += 1
            ret
        }

        def hasNext = idx <= raw.length
    }

    def length = raw.length

    def apply(idx: Int): RichExpr = new RichExpr(raw.part(idx + 1))

    def asDouble = raw.asDouble

    def asList: List[RichExpr] = (Nil ++ this)

}
