package com.jonathanolson.games.endice

import java.util.Date
import scala.collection.immutable.Set
import scala.collection.mutable.{Set => MutableSet, Map => MutableMap, HashMap => MutableHashMap, HashSet => MutableHashSet}

case class Spot(val row: Byte, val col: Byte) {
  def this(a: Int, b: Int) = this (a.toByte, b.toByte)

  // TODO: not necessary?
  override def equals(ob: Any) = {
    ob match {
      case Spot(r, c) => (r == row) && (c == col)
      case _ => false
    }
  }

  def offset(rowOffset: Int, colOffset: Int): Spot = {
    Spot((row + rowOffset).toByte, (col + colOffset).toByte)
  }

  def distanceTo(other: Spot): Int = {
    // manhattan distance
    ((row - other.row) abs) + ((col - other.col) abs)
  }
}

case class Die(val spot: Spot, val number: Byte, val id: String) {
  def this(a: Spot, b: Int, c: String) = this (a, b.toByte, c)

  require(number >= 0)

  // TODO: not necessary?
  override def equals(ob: Any) = {
    ob match {
      case Die(s, n, i) => (s == spot) && (n == number) && (i == id)
      case _ => false
    }
  }
}

case class Move(val id: String, val offset: Tuple2[Int, Int])

class Board(val endSpots: Set[Spot], val dice: Set[Die], val moves: List[Move]) {
  def isGoalState: Boolean = dice.forall((die: Die) => endSpots.contains(die.spot))

  def hasOutsideDeadSpot: Boolean = dice.exists((die: Die) => {
    if (die.number == 0 && !endSpots.contains(die.spot)) {
      // test whether no other dice can reach to push back
      if (die.spot.row < minRowEndSpot) {
        // returns true if all dice are either our die, or could not reach past the die to push it
        dice.forall((pusher: Die) => pusher == die || pusher.spot.row - (pusher.number - 1) >= die.spot.row)
      } else if (die.spot.row > maxRowEndSpot) {
        dice.forall((pusher: Die) => pusher == die || pusher.spot.row + (pusher.number - 1) <= die.spot.row)
      } else if (die.spot.col < minColEndSpot) {
        dice.forall((pusher: Die) => pusher == die || pusher.spot.col - (pusher.number - 1) >= die.spot.col)
      } else if (die.spot.col > maxColEndSpot) {
        dice.forall((pusher: Die) => pusher == die || pusher.spot.col + (pusher.number - 1) <= die.spot.col)
      } else {
        false
      }
    } else {
      false
    }
  })

  def isDoomed: Boolean = {
    if (numOpenEndSpots > numMovesLeft) {
      return true
    }
    if (hasOutsideDeadSpot) {
      return true
    }
    false
  }

  def isFinished: Boolean = dice.forall((die: Die) => die.number == 0)

  def numOpenEndSpots = (0 /: endSpots)((v: Int, spot: Spot) => v + (if (dice.exists(_.spot == spot)) 0 else 1))

  def numMovesLeft = (0 /: dice)((v: Int, die: Die) => v + die.number)

  def minRowEndSpot = (500 /: endSpots)(_ min _.row)

  def maxRowEndSpot = (-500 /: endSpots)(_ max _.row)

  def minColEndSpot = (500 /: endSpots)(_ min _.col)

  def maxColEndSpot = (-500 /: endSpots)(_ max _.col)

  def minRowDie = (500 /: dice)(_ min _.spot.row)

  def maxRowDie = (-500 /: dice)(_ max _.spot.row)

  def minColDie = (500 /: dice)(_ min _.spot.col)

  def maxColDie = (-500 /: dice)(_ max _.spot.col)

  override def equals(ob: Any) = ob match {
  // assume for now endSpots are the same, and moves don't matter
    case b: Board => {
      val ax = sortedDiceList
      val bx = b.sortedDiceList
      ax.length == bx.length && List.forall2(ax, bx)((a: Die, b: Die) => a == b)
    }
    case _ => false
  }

  def getDieById(id: String): Die = {
    dice.find(_.id == id) match {
      case Some(x) => x
      case None => throw new RuntimeException("Unknown die ID")
    }
  }

  def moveDie(die: Die, rowOffset: Int, colOffset: Int): Board = {
    def diceMove(oldDie: Die, diceSet: Set[Die]): Set[Die] = {
      val toSpot = oldDie.spot.offset(rowOffset, colOffset)
      val newDie = Die(toSpot, (oldDie.number - (if (oldDie == die) 1 else 0)).toByte, oldDie.id)
      dice.find(_.spot == toSpot) match {
        case Some(pushDie: Die) => (diceMove(pushDie, diceSet) - oldDie) + newDie
        case None => (diceSet - oldDie) + newDie
      }
    }
    new Board(endSpots, diceMove(die, dice), Move(die.id, (rowOffset, colOffset)) :: moves)
  }

  def neighbors: Set[Board] = {
    // flatmap and turn into a set, so each function returns a list of boards
    Set[Board]() ++ dice.flatMap {
      (die: Die) => {
        if (die.number == 0) {
          // can't move die
          Nil
        } else {
          def dieMove(rowOffset: Int, colOffset: Int) = moveDie(die, rowOffset, colOffset)
          List(dieMove(1, 0), dieMove(-1, 0), dieMove(0, 1), dieMove(0, -1))
        }
      }
    }
  }


  override def toString = {
    "Board:\n" + endSpots.toString + "\n" + dice.toString + "\n" + moves.toString + "\n--------"
  }

  lazy val sortedDiceList = dice.toList.sort((a, b) => a.spot.row < b.spot.row || (a.spot.row == b.spot.row && a.spot.col < b.spot.col))


  //override def hashCode = dice.hashCode
  override def hashCode = {
    var ret = 0
    var base = 1
    val diceList = sortedDiceList
    for (die: Die <- sortedDiceList) {
      base *= 7
      ret += base * (die.number + 3 * die.spot.row + 5 * die.spot.col) + 1
    }
    ret
  }
}

class BoardCache {
  val cache: MutableHashSet[Board] = new MutableHashSet[Board]()
  private[this] val counts: MutableMap[Int, Int] = MutableMap[Int, Int]()

  var accessCount: Long = 0
  var boardCount: Long = 0

  def check(board: Board): Boolean = {
    accessCount += 1
    if (cache.contains(board)) {
      true
    } else {
      if (board.numMovesLeft != 0) {
        cache += board
        val depth = board.numMovesLeft
        counts.put(depth, (counts.get(depth) match {case Some(x) => x; case None => 0}) + 1)
        boardCount += 1
      }
      false
    }
  }

  def report(top: Int) = {
    var str = "Cache:"

    for (idx <- 1 to top) {
      str += " " + idx + "(" + counts(idx) + ")"
    }

    str
  }
}

class Solver(val startBoard: Board) {
  var startDate: Date = _

  def solve() {
    startDate = new Date()
    testBoard(startBoard, 0)
  }

  private[this] var totalCount: Int = 0;
  private[this] val counts: MutableMap[Int, Int] = MutableMap[Int, Int]()

  private[this] def incrementCount(depth: Int) {
    counts.put(depth, (counts.get(depth) match {case Some(x) => x; case None => 0}) + 1)
    totalCount += 1
    if (totalCount % 100000 == 0) {
      var str = "counts: "
      var idx = 0
      var continue = true
      while (continue) {
        counts.get(idx) match {
          case Some(v) => {
            str += idx.toString + "(" + v.toString + ") "
            idx += 1
          }
          case None => continue = false
        }
      }
      val seconds: Long = (new Date().getTime() - startDate.getTime()) / 1000
      str += "unique-boards/total: " + cache.boardCount + "/" + totalCount
      str += " total: " + totalCount
      str += " seconds: " + seconds
      if (seconds != 0) {
        str += " boards/s: " + totalCount / seconds
        str += " unique-boards/s: " + cache.boardCount / seconds
      }
      println(str)
      println(cache.report(idx - 1))
      println()
    }
  }

  val cache = new BoardCache()

  def testBoard(board: Board, depth: Int) {
    incrementCount(depth)
    if (cache.check(board)) {
      return
    }
    if (board.isFinished) {
      if (board.isGoalState) {
        println("Solution: " + board.moves.toString)
        System.exit(0)
      }
    } else if (!board.isDoomed) {
      board.neighbors.foreach(testBoard(_, depth + 1))
    }
  }
}

object TestEndice {
  def main(args: Array[String]) {
    println("testing")
    val startBoard = Boards.level13
    new Solver(startBoard).solve()
  }


}

object Boards {
  def level1 = new Board(Set(Spot(2, 2), Spot(3, 1), Spot(3, 3)), Set(Die(Spot(0, 3), 3, "A"), Die(Spot(3, 0), 1, "B"), Die(Spot(2, 4), 2, "C")), Nil)

  def level2 = new Board(
    Set(
      Spot(0, 0), Spot(0, 2), Spot(2, 1)
      ),
    Set(
      Die(Spot(0, 1), 1, "A"),
      Die(Spot(2, 0), 2, "B"),
      Die(Spot(2, 2), 3, "C")
      ),
    Nil
    )

  def level13 = {
    val endSpots = Set(
      Spot(0, 1),
      Spot(0, 2),
      Spot(1, 1),
      Spot(1, 2),
      Spot(4, 1),
      Spot(4, 2)
      )
    val startDice = Set(
      Die(Spot(1, 0), 1, "A"),
      Die(Spot(2, 0), 2, "B"),
      Die(Spot(3, 0), 3, "C"),
      Die(Spot(1, 3), 3, "D"),
      Die(Spot(2, 3), 2, "E"),
      Die(Spot(3, 3), 1, "F")
      )
    new Board(endSpots, startDice, Nil)
  }
}