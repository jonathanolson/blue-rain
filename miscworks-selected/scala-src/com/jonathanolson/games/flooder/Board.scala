package org.jonathanolson.games.flooder

import collection.mutable.{PriorityQueue, LinkedList, ListBuffer}
import scala.util.Random
import scala.collection.mutable.{Set => MutableSet}

object Board {
  def freshBoard(size: Int): Board = freshBoard(size, size)

  def freshBoard(width: Int, height: Int): Board = freshBoard(width, height, 6)

  def freshBoard(width: Int, height: Int, numColors: Int): Board = {
    val board = new Board(width, height, numColors)
    board.fill()
    board
  }

  def inputBoard(width: Int, height: Int, numColors: Int, data: List[Int]) = {
    // TODO: refactor some of this into Board?
    val board = new Board(width, height, numColors)
    for (index <- 0 until data.length) {
      board.squares(index) = new Square(data(index), board.rowFromIndex(index), board.colFromIndex(index), false)
    }
    board.setActive();
    board
  }

  def colorChange(board: Board, value: Int): Board = {
    val newBoard = new Board(board.width, board.height, board.numColors)
    newBoard.colorChangeFrom(board, value)
    newBoard
  }
}

class Board(val width: Int, val height: Int, val numColors: Int) {
  def numSquares = width * height

  val squares: Array[Square] = new Array[Square](numSquares)

  def getSquare(index: Int): Square = squares(index)

  def getSquare(row: Int, col: Int): Square = squares(indexFromRC(row, col))

  def getOptionalSquare(row: Int, col: Int): Option[Square] = {
    if (row >= 0 && row < height && col >= 0 && col < width) {
      Some(getSquare(row, col))
    } else {
      None
    }
  }

  def adjacentSquares(center: Square): List[Square] = {
    def neighbors(coords: List[Tuple2[Int, Int]]): List[Square] = {
      coords match {
        case head :: tail => {
          val opt = getOptionalSquare(center.row + head._1, center.col + head._2)
          opt match {
            case Some(sq: Square) => sq :: neighbors(tail)
            case None => neighbors(tail)
          }
        }
        case _ => Nil
      }
    }
    neighbors(List((-1, 0), (1, 0), (0, -1), (0, 1)))
  }

  def fill() {
    for (index <- squares.indices) {
      squares(index) = new Square(Square.randomValue(numColors), rowFromIndex(index), colFromIndex(index), false)
    }
    setActive()
  }

  def setActive() {
    val startSquare = getSquare(0, 0)
    val startValue = startSquare.value
    def switchActive(sq: Square) {
      if (!sq.active && sq.value == startValue) {
        sq.active = true
        for (adjSquare <- adjacentSquares(sq)) {
          switchActive(adjSquare)
        }
      }
    }
    switchActive(startSquare)
  }

  def colorChangeFrom(oldBoard: Board, newValue: Int) {
    val checkList = new ListBuffer[Square]
    for (index <- squares.indices) {
      val oldSquare = oldBoard.getSquare(index)
      squares(index) = if (oldSquare.active) {
        new Square(oldSquare, newValue)
      } else {
        val nextToActive = oldBoard.adjacentSquares(oldSquare).exists(_.active)
        if (nextToActive && oldSquare.value == newValue) {
          val newSquare = new Square(oldSquare, newValue, true)
          checkList += newSquare
          newSquare
        } else {
          new Square(oldSquare)
        }
      }
    }
    while (!checkList.isEmpty) {
      val square = checkList.remove(0)
      for (neighbor <- adjacentSquares(square)) {
        if (!neighbor.active && neighbor.value == newValue) {
          neighbor.active = true
          checkList += neighbor
        }
      }
    }
  }

  def isComplete: Boolean = {
    val value = squares(0).value
    var complete = true
    for (square <- squares) {
      // TODO: bail out early!
      complete = complete && (value == square.value)
    }
    complete
  }

  override def toString = {
    val rows = for (
      row <- 0 to width - 1;
      rowSquares = squares.slice(row * width, (row + 1) * width)
    ) yield " " + rowSquares.mkString("")
    "-" * rows(0).length + "\n" + rows.mkString("\n") + "\n" + "-" * rows(0).length + "\n"
  }

  def indexFromRC(row: Int, col: Int) = row * width + col

  def rowFromIndex(index: Int) = index / width

  def colFromIndex(index: Int) = index % width

}

class Square(val value: Int, val row: Int, val col: Int, var active: Boolean) {
  def this(sq: Square) = this (sq.value, sq.row, sq.col, sq.active)

  def this(sq: Square, value: Int) = this (value, sq.row, sq.col, sq.active)

  def this(sq: Square, value: Int, active: Boolean) = this (value, sq.row, sq.col, active)

  override def toString = value.toString + (if (active) "*" else " ")
}

object Square {
  private val random = new Random()

  def randomValue(numColors: Int) = random.nextInt(numColors)
}

object Node {
  def retrace(nodeOp: Option[Node]): List[Int] = {
    nodeOp match {
      case Some(node: Node) => {
        node.move match {
          case Some(move: Int) => move :: retrace(node.parent)
          case None => retrace(node.parent)
        }
      }
      case None => Nil
    }
  }
}

class Node(val parent: Option[Node], val board: Board, val score: Double, val move: Option[Int], val traveled: Double) extends Ordered[Node] {
  def compare(that: Node): Int = -score.compare(that.score)

  override def toString = Node.retrace(Some(this)).reverse.mkString(",") + "(f=" + score + ",g=" + traveled + ",h=" + (score - traveled) + "):\n" + board
}

object Solver {
  def heuristicActiveCount(board: Board): Double = {
    (board.numSquares - board.squares.foldLeft[Double](0)((count: Double, square: Square) => count + (if (square.active) 1 else 0))) / board.numColors
  }

  def nonActiveColorCount(board: Board): Double = {

    val colors = MutableSet[Int]()
    for (square <- board.squares if !square.active) {
      colors += square.value
    }
    colors.size
  }

  def anyColorFloodCount(baseBoard: Board): Double = {
    val board: Board = new Board(baseBoard.width, baseBoard.height, baseBoard.numColors)
    val edgeSpots = MutableSet[Square]()
    for (oldSquare <- baseBoard.squares) {
      val newSquare: Square = new Square(oldSquare)
      board.squares(board.indexFromRC(oldSquare.row, oldSquare.col)) = newSquare
      if (board.adjacentSquares(oldSquare).exists((s: Square) => !s.active)) {
        edgeSpots += newSquare
      }
    }
    // TODO: more!
    0
  }

}

class Solver(val startBoard: Board, val heuristic: Board => Double) {
  var count: Int = 0
  val openNodes: PriorityQueue[Node] = new PriorityQueue()

  def solve(): List[Int] = {
    val startNode = new Node(None, startBoard, heuristic(startBoard), None, 0)
    openNodes += startNode;

    val endNode = aStar()
    val moves = Node.retrace(Some(endNode)).reverse
    moves
  }

  def aStar(): Node = {
    while (!openNodes.isEmpty) {
      val x = openNodes.dequeue
      //println("looking at node: " + x)
      if (x.board.isComplete) {
        return x
      } else {
        for (value <- 0 until x.board.numColors) {
          val newBoard = Board.colorChange(x.board, value)
          val traveled = x.traveled + 1
          val move = value
          val score = traveled + heuristic(newBoard)
          val node = new Node(Some(x), newBoard, score, Some(move), traveled)
          openNodes += node
        }
      }
      count = count + 1
      if (count > 100000) {
        throw new RuntimeException("Too many iterations")
      }
    }
    throw new RuntimeException("Could not reach end (should never happen)")
  }

}

object BoardTest {
  def main(args: Array[String]) {

    // sizes: 12, 17, 22
    val startBoard = Board.freshBoard(22)
    println(startBoard)
    val solver: Solver = new Solver(startBoard, Solver.heuristicActiveCount)
    val moveList: List[Int] = solver.solve
    println("Best solution in " + moveList.length + " moves is: " + moveList.mkString(", "))
    println("Solver checked " + solver.count + " nodes.")

    var board = startBoard
    for (move <- moveList) {
      println("move: " + move)
      board = Board.colorChange(board, move)
      println(board)
    }
  }
}

object BoardSolve {
  def main(args: Array[String]) {
    val startBoard = Board.inputBoard(12, 12, 6, List(
      1, 5, 4, 1, 3, 6, 2, 1, 3, 5, 5, 1,
      2, 2, 2, 5, 4, 2, 4, 3, 2, 2, 1, 5,
      3, 5, 6, 1, 2, 1, 3, 4, 2, 6, 2, 6,
      3, 4, 2, 4, 1, 5, 1, 3, 2, 5, 3, 5,
      5, 3, 5, 3, 4, 6, 1, 5, 3, 2, 1, 2,
      2, 3, 3, 3, 4, 1, 2, 6, 3, 3, 2, 1,
      5, 2, 1, 2, 4, 4, 4, 1, 2, 3, 3, 1,
      5, 4, 1, 6, 6, 3, 4, 6, 4, 6, 1, 1,
      5, 4, 6, 4, 2, 3, 5, 2, 4, 6, 5, 4,
      3, 2, 2, 4, 3, 3, 5, 2, 1, 1, 5, 1,
      1, 5, 2, 2, 3, 6, 6, 5, 5, 5, 2, 4,
      1, 2, 5, 2, 3, 1, 5, 6, 5, 3, 3, 1
      ).map(_ - 1))
    println(startBoard)
    val solver: Solver = new Solver(startBoard, Solver.heuristicActiveCount)
    val moveList: List[Int] = solver.solve
    println("Best solution in " + moveList.length + " moves is: " + moveList.map(_ + 1).mkString(", "))
    println("Solver checked " + solver.count + " nodes.")

    var board = startBoard
    for (move <- moveList) {
      println("move: " + move)
      board = Board.colorChange(board, move)
      println(board)
    }
  }
}

