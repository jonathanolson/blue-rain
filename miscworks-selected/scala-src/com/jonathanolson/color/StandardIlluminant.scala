package com.jonathanolson.color

import com.jonathanolson.math.Point3d

/**
 * A double-backed color with a colorspace
 */
object StandardIlluminant {
    val A_XYZ: Point3d = new Point3d( 1.09850, 1.00000, 0.35585 )
    val B_XYZ: Point3d = new Point3d( 0.99072, 1.00000, 0.85223 )
    val C_XYZ: Point3d = new Point3d( 0.98074, 1.00000, 1.18232 )
    val D50_XYZ: Point3d = new Point3d( 0.96422, 1.00000, 0.82521 )
    val D55_XYZ: Point3d = new Point3d( 0.95682, 1.00000, 0.92149 )
    val D65_XYZ: Point3d = new Point3d( 0.95047, 1.00000, 1.08883 )
    val D75_XYZ: Point3d = new Point3d( 0.94972, 1.00000, 1.22638 )
    val E_XYZ: Point3d = new Point3d( 1.00000, 1.00000, 1.00000 )
    val F2_XYZ: Point3d = new Point3d( 0.99186, 1.00000, 0.67393 )
    val F7_XYZ: Point3d = new Point3d( 0.95041, 1.00000, 1.08747 )
    val F11_XYZ: Point3d = new Point3d( 1.00962, 1.00000, 0.64350 )
}
