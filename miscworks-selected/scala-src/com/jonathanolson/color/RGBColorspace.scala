package com.jonathanolson.color

import com.jonathanolson.math.{Point3i, Matrix3d, Point3d}

/**
 * A double-backed color with a colorspace
 */
class RGBColorspace(
        val gamma: Double,
        val referenceWhiteXYZ: Point3d,
        val redxyY: Point3d,
        val greenxyY: Point3d,
        val bluexyY: Point3d
        ) extends Colorspace {
    protected lazy val RGBToXYZ: Matrix3d = {
        val r = new Point3d( redxyY.x / redxyY.y, 1, ( 1 - redxyY.x - redxyY.y ) / redxyY.y )
        val g = new Point3d( greenxyY.x / greenxyY.y, 1, ( 1 - greenxyY.x - greenxyY.y ) / greenxyY.y )
        val b = new Point3d( bluexyY.x / bluexyY.y, 1, ( 1 - bluexyY.x - bluexyY.y ) / bluexyY.y )
        val tmp: Matrix3d = new Matrix3d( r.x, g.x, b.x, r.y, g.y, b.y, r.z, g.z, b.z ).inverse
        val s: Point3d = tmp * referenceWhiteXYZ
        new Matrix3d( s.x * r.x, s.y * g.x, s.z * b.x, s.x * r.y, s.y * g.y, s.z * b.y, s.x * r.z, s.y * g.z, s.z * b.z )
    }
    protected lazy val XYZToRGB: Matrix3d = RGBToXYZ.inverse

    def fromQuantizedInt( point: Point3i ): RGBColor = {
        def unquantize( i: Int ) = i.toDouble / 255
        new RGBColor( unquantize( point.x ), unquantize( point.y ), unquantize( point.z ), this )
    }

    def fromXYZ( color: Point3d ): Point3d = ( XYZToRGB * color ).map( applyInverseGamma )

    def toXYZ( color: Point3d ): Point3d = RGBToXYZ * ( color.map( applyGamma ) )

    def applyGamma( d: Double ) = scala.math.pow( d, gamma )

    def applyInverseGamma( d: Double ) = scala.math.pow( d, 1 / gamma )

    def convert( color: RGBColor ): RGBColor = {
        if ( color.space eq this ) color else {
            new RGBColor( fromXYZ( color.toXYZ ), this )
        }
    }
}

object RGBColorspace {
    val sRGB = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.212656 ), new Point3d( 0.3000, 0.6000, 0.715158 ), new Point3d( 0.1500, 0.0600, 0.072186 ) ) {
        override def applyInverseGamma( d: Double ) = if ( d <= 0.0031308 ) d * 12.92 else 1.055 * scala.math.pow( d, 1.0 / 2.4 ) - 0.055

        override def applyGamma( d: Double ) = if ( d <= 0.04045 ) d / 12.92 else scala.math.pow( ( d + 0.055 ) / 1.055, 2.4 )
    }

    val LinearSRGB = new RGBColorspace( 1, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.212656 ), new Point3d( 0.3000, 0.6000, 0.715158 ), new Point3d( 0.1500, 0.0600, 0.072186 ) )

    val Default = sRGB

    val Adobe = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.297361 ), new Point3d( 0.2100, 0.7100, 0.627355 ), new Point3d( 0.1500, 0.0600, 0.075285 ) )
    val Apple = new RGBColorspace( 1.8, StandardIlluminant.D65_XYZ, new Point3d( 0.6250, 0.3400, 0.244634 ), new Point3d( 0.2800, 0.5950, 0.672034 ), new Point3d( 0.1550, 0.0700, 0.083332 ) )
    val Best = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.7347, 0.2653, 0.228457 ), new Point3d( 0.2150, 0.7750, 0.737352 ), new Point3d( 0.1300, 0.0350, 0.034191 ) )
    val Beta = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.6888, 0.3112, 0.303273 ), new Point3d( 0.1986, 0.7551, 0.663786 ), new Point3d( 0.1265, 0.0352, 0.032941 ) )
    val Bruce = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.240995 ), new Point3d( 0.2800, 0.6500, 0.683554 ), new Point3d( 0.1500, 0.0600, 0.075452 ) )
    val CIE = new RGBColorspace( 2.2, StandardIlluminant.E_XYZ, new Point3d( 0.7350, 0.2650, 0.176204 ), new Point3d( 0.2740, 0.7170, 0.812985 ), new Point3d( 0.1670, 0.0090, 0.010811 ) )
    val ColorMatch = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.6300, 0.3400, 0.274884 ), new Point3d( 0.2950, 0.6050, 0.658132 ), new Point3d( 0.1500, 0.0750, 0.066985 ) )
    val ECI = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.6700, 0.3300, 0.320250 ), new Point3d( 0.2100, 0.7100, 0.602071 ), new Point3d( 0.1400, 0.0800, 0.077679 ) )
    val Ekta_Space_PS5 = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.6950, 0.3050, 0.260629 ), new Point3d( 0.2600, 0.7000, 0.734946 ), new Point3d( 0.1100, 0.0050, 0.004425 ) )
    val NTSC = new RGBColorspace( 2.2, StandardIlluminant.C_XYZ, new Point3d( 0.6700, 0.3300, 0.298839 ), new Point3d( 0.2100, 0.7100, 0.586811 ), new Point3d( 0.1400, 0.0800, 0.114350 ) )
    val PAL_SECAM = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6400, 0.3300, 0.222021 ), new Point3d( 0.2900, 0.6000, 0.706645 ), new Point3d( 0.1500, 0.0600, 0.071334 ) )
    val ProPhoto = new RGBColorspace( 1.8, StandardIlluminant.D50_XYZ, new Point3d( 0.7347, 0.2653, 0.288040 ), new Point3d( 0.1596, 0.8404, 0.711874 ), new Point3d( 0.0366, 0.0001, 0.000086 ) )
    val SMPTE_C = new RGBColorspace( 2.2, StandardIlluminant.D65_XYZ, new Point3d( 0.6300, 0.3400, 0.212395 ), new Point3d( 0.3100, 0.5950, 0.701049 ), new Point3d( 0.1550, 0.0700, 0.086556 ) )
    val Wide_Gamut = new RGBColorspace( 2.2, StandardIlluminant.D50_XYZ, new Point3d( 0.7350, 0.2650, 0.258187 ), new Point3d( 0.1150, 0.8260, 0.724938 ), new Point3d( 0.1570, 0.0180, 0.016875 ) )
}
