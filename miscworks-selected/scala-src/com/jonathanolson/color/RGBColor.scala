package com.jonathanolson.color

import com.jonathanolson.math.Point3d

/**
 * A double-backed color with a colorspace
 */
class RGBColor(
        val red: Double,
        val green: Double,
        val blue: Double,
        space: RGBColorspace
        ) extends Color( red, green, blue, space ) {
    def this( point: Point3d, space: RGBColorspace ) = this ( point.x, point.y, point.z, space )

    def this( point: Point3d ) = this ( point.x, point.y, point.z, RGBColorspace.Default )

    def this( red: Double, green: Double, blue: Double ) = this ( red, green, blue, RGBColorspace.Default )

    require( space.isInstanceOf[ RGBColorspace ] )
}
