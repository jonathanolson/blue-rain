package com.jonathanolson.color

import com.jonathanolson.math.Point3d

/**
 * A double-backed color with a colorspace
 */
trait Colorspace {
    def fromXYZ( color: Point3d ): Point3d

    def toXYZ( color: Point3d ): Point3d

    def convert( color: Color ) = {
        if ( color.space eq this ) color else {
            new Color( fromXYZ( color.toXYZ ), this )
        }
    }

    val colorspaceId = Colorspace.nextCount()

    override def hashCode = colorspaceId

    override def equals( p1: Any ) = p1 match {
        case c: Colorspace => c.colorspaceId == colorspaceId
        case _ => false
    }


    override def toString = "#" + colorspaceId
}

object Colorspace {
    var count: Int = 0

    def nextCount(): Int = {
        count += 1
        count
    }
}
