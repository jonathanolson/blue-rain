package com.jonathanolson.color

import com.jonathanolson.math.{Tuple3d, Point3i, Point3d}

/**
 * A double-backed color with a colorspace
 */
class Color(
        x: Double,
        y: Double,
        z: Double,
        val space: Colorspace
        ) extends Point3d( x, y, z ) {
    def this( point: Point3d, space: Colorspace ) = this ( point.x, point.y, point.z, space )

    def this( x: Double, y: Double, z: Double ) = this ( x, y, z, RGBColorspace.Default )

    def toIntRGB = RGBColorspace.Default.convert( this ).quantized

    def quantized: Point3i = quantized( 256 )

    /**
     * Convert into an integer point
     */
    def quantized( numBins: Int ): Point3i = {
        def convertChannel( d: Double ): Int = {
            // quantize it simply for now
            val tmp: Int = ( d * ( numBins.toDouble ) ).toInt
            if ( tmp == numBins ) numBins - 1 else tmp
        }
        new Point3i( convertChannel( x ), convertChannel( y ), convertChannel( z ) )
    }

    /**
     * Convert to XYZ colorspace
     */
    def toXYZ = new Color( space.toXYZ( this ), XYZColorspace.XYZ )

    def getChannel( ch: Int ): Double = {
        ch match {
            case 0 => x
            case 1 => y
            case 2 => z
            case _ => throw new RuntimeException( "bad channel: " + ch )
        }
    }

    /*---------------------------------------------------------------------------*
    * equals / hashcode
    *----------------------------------------------------------------------------*/

    override def equals( other: Any ) = other match {
        case that: Color => ( this.x == that.x ) && ( this.y == that.y ) && ( this.z == that.z ) && ( this.space == that.space )
    }

    override def hashCode = {
        ( x.hashCode + y.hashCode * 31 ) * 3 + z.hashCode * 31 + space.hashCode
    }

    /*---------------------------------------------------------------------------*
    * override math for with points
    *----------------------------------------------------------------------------*/

    override def +( other: Tuple3d ) = new Color( super.+( other ), space )

    override def -( other: Tuple3d ) = new Color( super.-( other ), space )

    override def +( d: Double ) = new Color( super.+( d ), space )

    override def -( d: Double ) = new Color( super.-( d ), space )

    override def *( s: Double ) = new Color( super.*( s ), space )

    override def /( s: Double ) = new Color( super./( s ), space )

    def +( other: Color ) = new Color( super.+( space.convert( other ) ), space )

    def -( other: Color ) = new Color( super.-( space.convert( other ) ), space )

    override def fit( inLow: Double, inHigh: Double, outLow: Double, outHigh: Double ) = new Color( super.fit( inLow, inHigh, outLow, outHigh ), space )
}
