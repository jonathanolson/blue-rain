package com.jonathanolson.color

import com.jonathanolson.math.Point3d

/**
 * A double-backed color with a colorspace
 */
class XYZColorspace extends Colorspace {
    def fromXYZ( color: Point3d ) = color

    def toXYZ( color: Point3d ) = color

}

object XYZColorspace {
    val XYZ = new XYZColorspace
}

