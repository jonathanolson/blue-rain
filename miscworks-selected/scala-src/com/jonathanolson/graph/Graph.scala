package com.jonathanolson.graph

import collection.mutable.{Queue => MQueue, Set => MSet, Map => MMap}
import collection.immutable.SortedSet

// commit around last major change: 123

/*---------------------------------------------------------------------------*
* interface traits
*----------------------------------------------------------------------------*/

trait Graph[V, E] extends AnyRef with GraphLike[V, E, Graph[V, E]] {
    def vertices: collection.Set[V] // all vertices
    def edges: collection.Set[E] // all edges
    def order: Int // number of vertices
    def size: Int // number of edges

    def verticesOf( e: E ): (V, V) // both vertices for an edge. if directed, in their specific order

    def edgesOf( v: V ): Iterable[E] // all edges touching
    def edgesOf( a: V, b: V ): Iterable[E] // all edges from a to b
    def edgesFrom( v: V ): Iterable[E] // all edges from v
    def edgesTo( v: V ): Iterable[E] // all edges from v
    def neighbors( v: V ): Iterable[V] // all vertices with an edge to or from v
    def successors( v: V ): Iterable[V] // all vertices s.t. (v,X) is an edge
    def predecessors( v: V ): Iterable[V] // all vertices s.t. (X,v) is an edge
    def isAdjacent( a: V, b: V ): Boolean // whether an edge goes from a to b
    def degreeOf( v: V ): Int // vertex degree

    def isEndOf( e: E, v: V ): Boolean // whether v is an edge of e
    def otherEnd( e: E, v: V ): V // the other end of an edge

    def sourceOf( e: E ): V // source
    def targetOf( e: E ): V // target
}

trait MutableGraph[V, E] extends Graph[V, E] {
    def addVertex( v: V ): Unit // add vertex
    def removeVertex( v: V ): Unit // remove vertex
    def addEdge( a: V, b: V, e: E ): Unit // add edge
    def removeEdge( e: E ): Unit // remove edge
    def replaceVertex( a: V, b: V ): Unit // replace a vertex with another
    def replaceEdge( a: E, b: E ): Unit // replace an edge with another
}

trait ImmutableGraph[V, E] extends Graph[V, E] with ImmutableGraphLike[V, E, ImmutableGraph[V, E]]

trait DirectedGraph[V, E] extends Graph[V, E]

trait UndirectedGraph[V, E] extends Graph[V, E] {
    def predecessors( v: V ) = neighbors( v )

    def successors( v: V ) = neighbors( v )

    def edgesTo( v: V ) = edgesOf( v )

    def edgesFrom( v: V ) = edgesOf( v )
}

trait SimpleGraph[V, E] extends Graph[V, E]

trait MultiGraph[V, E] extends Graph[V, E]

trait PseudoGraph[V, E] extends Graph[V, E]


class Vertex[V, E, +G <: Graph[V, E]]( val graph: G, val vertex: V ) {
    def degree = graph.degreeOf( vertex )

    def edges = for (e <- graph.edgesOf( vertex )) yield graph.bindEdge( e )

    // TODO: fill in for more methods

    override def toString = vertex.toString
}

object Vertex {
    implicit def toVertex[V, E, G <: Graph[V, E]]( v: Vertex[V, E, G] ) = v.vertex
}

class Edge[V, E, +G <: Graph[V, E]]( val graph: G, val edge: E ) {
    def source = graph.bindVertex( graph.sourceOf( edge ) )

    def target = graph.bindVertex( graph.targetOf( edge ) )

    // TODO: fill in for more methods

    override def toString = source + " => " + target
}

object Edge {
    implicit def toEdge[V, E, G <: Graph[V, E]]( e: Edge[V, E, G] ) = e.edge
}

/*---------------------------------------------------------------------------*
* implementation interface traits?
*----------------------------------------------------------------------------*/

trait GraphLike[V, E, +G <: Graph[V, E]] {
    self: G =>
    def order: Int = vertices.size // number of vertices
    def size: Int = edges.size // number of edges

    def degreeOf( v: V ): Int = { // vertex degree
        require( vertices.contains( v ) )
        var ret = 0
        for (e <- edgesOf( v )) {
            ret += 1
            val verts = verticesOf( e )
            if ( verts._1 == verts._2 ) {
                ret += 1 // must be a loop, double count it
            }
        }
        ret
    }

    def isEndOf( e: E, v: V ): Boolean = {
        require( edges.contains( e ) )
        val (a, b) = verticesOf( e )
        (v == a) || (v == b)
    }

    def otherEnd( e: E, v: V ): V = {
        require( edges.contains( e ) )
        require( vertices.contains( v ) )
        val (a, b) = verticesOf( e )
        require( (v == a) || (v == b) )
        if ( v == a ) b else a
    }

    def isAdjacent( a: V, b: V ): Boolean = !edgesOf( a, b ).isEmpty

    def breadthFirstIterator( startVertex: V ) = new Iterator[(Int, V)] { // distance, vertex in closest order
        require( vertices.contains( startVertex ) )

        val queue = new MQueue[(Int, V)]
        val marked: MSet[V] = MSet.empty

        marked += startVertex
        queue += Tuple2( 0, startVertex )

        def next(): (Int, V) = {
            var (d, v) = queue.dequeue
            for (
                other <- successors( v );
                if !marked.contains( other )
            ) {
                marked += other
                queue += Tuple2( d + 1, other )
            }
            (d, v)
        }

        def hasNext = !queue.isEmpty
    }

    def bindVertex( v: V ): Vertex[V, E, G] = new Vertex[V, E, G]( this, v )

    def bindEdge( e: E ): Edge[V, E, G] = new Edge[V, E, G]( this, e )

    def sourceOf( e: E ): V = verticesOf( e )._1

    def targetOf( e: E ): V = verticesOf( e )._2
}

trait ImmutableGraphLike[V, E, +This <: ImmutableGraph[V, E]] extends GraphLike[V, E, This] {
    self: This =>
    def withVertex( v: V ): This // add vertex
    def withoutVertex( v: V ): This // remove vertex
    def withEdge( a: V, b: V, e: E ): This // add edge
    def withoutEdge( e: E ): This // remove edge
    def withReplacedVertex( a: V, b: V ): This // replace a vertex with another
    def withReplacedEdge( a: E, b: E ): This // replace an edge with another
}

/*---------------------------------------------------------------------------*
* concrete classes
*----------------------------------------------------------------------------*/

class TestVertex( val label: String ) {
    override def toString = label
}
class TestEdge

class TesterGraph[V, E] extends Graph[V, E] with MutableGraph[V, E] with DirectedSimpleMutableMapConnections[V, E]

class TestUEdge

object TestUniGraph {
    def empty[V, E] = new TestUniGraph[V, E]( Map.empty[V, Map[V, E]], Map.empty[E, (V, V)] )
}

class TestUniGraph[V, E](
        val vertexData: Map[V, Map[V, E]],
        val edgeData: Map[E, (V, V)]
        ) extends Graph[V, E] with ImmutableGraph[V, E] with SimpleGraph[V, E] with UndirectedGraph[V, E] {
    def withReplacedEdge( a: E, b: E ): TestUniGraph[V, E] = {
        require( edges.contains( a ) )
        require( !edges.contains( b ) )

        val (x, y) = verticesOf( a )
        this.withoutEdge( a ).withEdge( x, y, b )
    }

    def withReplacedVertex( a: V, b: V ): TestUniGraph[V, E] = {
        require( vertices.contains( a ) )
        require( !vertices.contains( b ) )

        val oldEdges = edgesOf( a, b )
        val withoutEdges = (this /: oldEdges)( (graph, edge) => graph.withoutEdge( edge ) )
        val withNewVertex = withoutEdges.withoutVertex( a ).withVertex( b )
        (withNewVertex /: oldEdges)( (graph, edge) => graph.withEdge( a, b, edge ) )
    }

    def withoutEdge( e: E ): TestUniGraph[V, E] = {
        require( edges.contains( e ) )

        val (a, b) = verticesOf( e )

        new TestUniGraph[V, E](
            vertexData.updated( a, vertexData( a ) - b ).updated( b, vertexData( b ) - a ),
            edgeData - e
            )
    }

    def withEdge( a: V, b: V, e: E ): TestUniGraph[V, E] = {
        require( a != b )
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )
        require( !edges.contains( e ) )
        require( edgesOf( a, b ).isEmpty )

        new TestUniGraph[V, E](
            vertexData.updated( a, vertexData( a ) + (b -> e) ).updated( b, vertexData( b ) + (a -> e) ),
            edgeData + (e -> (a, b))
            )
    }

    def withoutVertex( v: V ): TestUniGraph[V, E] = {
        require( vertices.contains( v ) )

        val g = (this /: edgesOf( v ))( (graph, edge) => graph.withoutEdge( edge ) )
        new TestUniGraph[V, E](
            g.vertexData - v,
            g.edgeData
            )
    }

    def withVertex( v: V ): TestUniGraph[V, E] = {
        require( !vertices.contains( v ) )

        new TestUniGraph[V, E](
            vertexData + (v -> Map.empty),
            edgeData
            )
    }

    def neighbors( v: V ) = {
        require( vertices.contains( v ) )

        edgesOf( v ).map( otherEnd( _, v ) )
    }

    def edgesOf( a: V, b: V ) = {
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )

        for (
            (v, e) <- vertexData( a );
            if v == b
        ) yield e
    }

    def edgesOf( v: V ) = {
        require( vertices.contains( v ) )

        vertexData( v ).values
    }

    def verticesOf( e: E ) = edgeData( e )

    def edges = edgeData.keySet

    def vertices = vertexData.keySet
}

/*---------------------------------------------------------------------------*
* tests
*----------------------------------------------------------------------------*/

object NewGraphTest {
    def main( args: Array[String] ) {
        val a = new TestVertex( "a" )
        val b = new TestVertex( "b" )
        val c = new TestVertex( "c" )
        val d = new TestVertex( "d" )

        {
            implicit val g = new TesterGraph[TestVertex, TestEdge]
            implicit def toV( v: TestVertex )( implicit graph: TesterGraph[TestVertex, TestEdge] ) = graph.bindVertex( v )
            implicit def toE( e: TestEdge )( implicit graph: TesterGraph[TestVertex, TestEdge] ) = graph.bindEdge( e )

            g.addVertex( a )
            g.addVertex( b )
            g.addVertex( c )
            g.addVertex( d ) // TODO: auto-add vertices?
            g.addEdge( a, b, new TestEdge )
            g.addEdge( a, c, new TestEdge )
            g.addEdge( c, b, new TestEdge )
            g.addEdge( c, d, new TestEdge )
            println( g.vertices )
            println( g.edges.map( _.toString ).mkString( ", " ) )
            g.removeVertex( b )
            g.replaceVertex( c, b )

            g.breadthFirstIterator( a ).foreach( println _ )

            println()

            println( a.degree )
            println( a.edges )

            println()

            for (
                e <- a.edges
            ) {
                println( g.verticesOf( e ) )
            }
        }

        {
            val g = TestUniGraph.empty[TestVertex, TestUEdge]
            var h = g.withVertex( a ).withVertex( b ).withVertex( c ).withVertex( d )
            h = h.withEdge( a, b, new TestUEdge ).withEdge( a, c, new TestUEdge ).withEdge( b, c, new TestUEdge ).withEdge( c, d, new TestUEdge )
            //            h.breadthFirstIterator( a ).foreach( println _ )
        }


    }
}



trait DirectedSimpleMutableMapConnections[V, E] { // does not allow multiple edges
    private[this] val vertexData: MMap[V, MMap[V, E]] = MMap.empty
    private[this] val reversedVertexData: MMap[V, MMap[V, E]] = MMap.empty
    private[this] val edgeData: MMap[E, (V, V)] = MMap.empty

    val vertices: MSet[V] = MSet.empty
    val edges: MSet[E] = MSet.empty

    def addVertex( v: V ): Unit = {
        require( !vertices.contains( v ) )

        vertices += v
        vertexData += (v -> MMap.empty)
        reversedVertexData += (v -> MMap.empty)
    }

    def addEdge( a: V, b: V, e: E ): Unit = {
        require( !edges.contains( e ) )
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )

        edges += e
        edgeData += (e -> (a, b))

        addToVertexMap( vertexData, a, b, e )
        addToVertexMap( reversedVertexData, b, a, e )
    }

    def removeEdge( e: E ): Unit = {
        require( edges.contains( e ) )

        val (a, b) = verticesOf( e )

        edges -= e
        edgeData -= e

        removeFromVertexMap( vertexData, a, b )
        removeFromVertexMap( reversedVertexData, b, a )
    }

    def removeVertex( v: V ): Unit = {
        require( vertices.contains( v ) )

        edgesOf( v ).foreach( e => removeEdge( e ) )
        vertices -= v
        vertexData -= v
        reversedVertexData -= v
    }

    def replaceVertex( a: V, b: V ): Unit = {
        require( vertices.contains( a ) )
        require( !vertices.contains( b ) )

        vertices -= a
        vertices += b
        def clean( data: MMap[V, MMap[V, E]] ): Unit = {
            for (
                outer <- data.keys;
                inmap = data( outer );
                if inmap.contains( a )
            ) {
                inmap += (b -> inmap( a ))
                inmap -= a
            }
            if ( data.contains( a ) ) {
                data += (b -> data( a ))
                data -= a
            }
        }
        clean( vertexData )
        clean( reversedVertexData )
    }

    def replaceEdge( a: E, b: E ): Unit = {
        require( edges.contains( a ) )
        require( !edges.contains( b ) )

        edges -= a
        edges += b
        edgeData += (b -> edgeData( a ))
        edgeData -= a
    }

    def verticesOf( e: E ) = {
        require( edges.contains( e ) )

        edgeData( e )
    }

    def edgesOf( v: V ) = {
        require( vertices.contains( v ) )

        // from and to
        vertexData( v ).values ++ reversedVertexData( v ).values
    }

    def edgesOf( a: V, b: V ) = {
        require( vertices.contains( a ) )
        require( vertices.contains( b ) )

        for (
            (v, e) <- vertexData( a );
            if v == b
        ) yield e
    }

    def edgesFrom( v: V ): Iterable[E] = {
        require( vertices.contains( v ) )

        vertexData( v ).values
    }

    def edgesTo( v: V ): Iterable[E] = {
        require( vertices.contains( v ) )

        reversedVertexData( v ).values
    }

    def neighbors( v: V ): Iterable[V] = {
        require( vertices.contains( v ) )

        Set.empty ++ vertexData.keySet ++ reversedVertexData.keySet
    }

    def successors( v: V ): Iterable[V] = {
        require( vertices.contains( v ) )

        Set.empty ++ vertexData.keySet
    }

    def predecessors( v: V ): Iterable[V] = {
        require( vertices.contains( v ) )

        Set.empty ++ reversedVertexData.keySet
    }

    private[this] def addToVertexMap( map: MMap[V, MMap[V, E]], a: V, b: V, e: E ): Unit = {
        if ( map.contains( a ) ) {
            map( a ) += (b -> e)
        } else {
            map += (a -> MMap( b -> e ))
        }
    }

    private[this] def removeFromVertexMap( map: MMap[V, MMap[V, E]], a: V, b: V ): Unit = {
        if ( map.contains( a ) ) {
            map( a ) -= b
        }
    }

}
