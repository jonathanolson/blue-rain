
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns the original value if it is inclusively within the [max,min] range. If it's below the range, min is
 * returned, and if it's above the range, max is returned.
 *
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function clamp( value, min, max ) {
  assert( typeof value === 'number' );
  assert( typeof min === 'number' );
  assert( typeof max === 'number' );

  if ( value < min ) {
    return min;
  }
  else if ( value > max ) {
    return max;
  }
  else {
    return value;
  }
}

namespace.clamp = clamp;

export default clamp;
