
import assert from 'core/assert';
import fixedRay from 'math/fixedRay';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Vector2 from 'math/Vector2';

class Ray2 {
  constructor( position, direction ) {
    assert( Math.abs( direction.magnitude - 1 ) < 1e-4 , 'the direction must be a unit vector' );

    // @public {Vector2}
    this.position = position;
    this.direction = direction;
  }
}

fixedRay( Ray2, 'Ray2', 2, Vector2 );

namespace.Ray2 = Ray2;

Poolable.mixInto( Ray2, {
  defaultArguments: [ Vector2.ZERO, Vector2.X_UNIT ],
  initialize: Ray2.prototype.setPosDir
} );

export default Ray2;
