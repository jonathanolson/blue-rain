
import namespace from 'namespace';
import Vector3 from 'math/Vector3';

function v3( x, y, z ) {
  return Vector3.create( x, y, z );
}

namespace.v3 = v3;

export default v3;
