
import namespace from 'namespace';

const coordinateNames = [ 'x', 'y', 'z', 'w', 't' ];

namespace.coordinateNames = coordinateNames;

export default coordinateNames;
