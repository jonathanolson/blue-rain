
import assert from 'core/assert';
import combinations from 'core/combinations';
import coordinateNames from 'math/coordinateNames';
import createFunctionWrap from 'core/createFunctionWrap';
import createForwardGetter from 'core/createForwardGetter';
import flatten from 'core/flatten';
import Poolable from 'core/Poolable';
import rangeInclusive from 'math/rangeInclusive';

/**
 * Sets up general methods for fixed-size square matrix types (e.g. Matrix3).
 *
 * @param {function} type - The constructor/class, e.g. Matrix3 itself
 * @param {string} name - The name of the type, e.g. 'Matrix3'
 * @param {number} size - e.g. 3
 * @param {function} vectorType - The type of the matching vector, e.g. Vector3
 * @param {function|null} affineVectorType - The type of the vector that is 1 dimension smaller, e.g. Vector2
 */
function fixedMatrix( type, name, size, vectorType, affineVectorType ) {
  assert( typeof type === 'function' );
  assert( typeof name === 'string' );
  assert( typeof size === 'number' && size < 10 );

  // Grabs the actual prototype
  const proto = type.prototype;

  const indices = rangeInclusive( 0, size - 1 );

  const vectorVariable = `v${size}`;
  const vectorName = `Vector${size}`;
  const affineVectorVariable = `v${size - 1}`;
  const affineVectorName = `Vector${size - 1}`;

  function entry( row, col, name = 'this' ) {
    // stored column-major (because WebGL) for now
    return `${name}.d[${row + col * size}]`;
  }
  function entryCode( rowName, colName, name = 'this' ) {
    // stored column-major (because WebGL) for now
    return `${name}.d[ ${rowName}+${colName}*${size}]`;
  }
  function vari( row, col ) {
    return `v${row}${col}`;
  }
  function columnMajorLoop( f ) {
    const arr = [];
    for ( let col = 0; col < size; col++ ) {
      for ( let row = 0; row < size; row++ ) {
        arr.push( f( row, col ) );
      }
    }
    return arr;
  }
  function rowMajorLoop( f ) {
    return columnMajorLoop( ( row, col ) => f( col, row ) );
  }

  const getAssertions = params => {
    let assertions = '';
    assert( ( params.forEach( param => {
      if ( param === 'm' ) {
        assertions += `assert( ${param} instanceof ${name}, '${param} should be an instance of ${name}' );`;
      }
      else if ( param === 's' ||
                param === 'row' ||
                param === 'col' ||
                coordinateNames.includes( param ) ||
                rowMajorLoop( ( row, col ) => vari( row, col ) ).includes( param ) ) {
        assertions += `assert( typeof ${param} === 'number' && isFinite( ${param} ), '${param} should be a number' );`;
      }
      else if ( param === vectorVariable ) {
        assertions += `assert( ${param} instanceof ${vectorName}, '${param} should be an instance of ${vectorName}' );`;
      }
      else if ( param === affineVectorVariable ) {
        assertions += `assert( ${param} instanceof ${affineVectorName}, '${param} should be an instance of ${affineVectorName}' );`;
      }
    } ), true ) );
    return assertions;
  };

  const wrap = createFunctionWrap( {
    [ name ]: type,
    assert: assert,
    [ vectorName ]: vectorType,
    [ affineVectorName ]: affineVectorType
  }, getAssertions );
  const forwardGetter = createForwardGetter( proto, wrap );
  function mutateConsistent( f ) {
    return `return this.rowMajor(${rowMajorLoop( ( row, col ) => f( row, col, entry( row, col ), entry( row, col, 'm' ) ) ).join( ',' )});`;
  }

  rowMajorLoop( ( row, col ) => {
    const getter = wrap( [], `return ${entry( row, col )};` );
    const setter = wrap( [ 's' ], `${entry( row, col )}=s;` );
    proto[ `get${row}${col}` ] = getter;
    proto[ `set${row}${col}` ] = setter;
    Object.defineProperty( proto, `m${row}${col}`, {
      get: getter,
      set: setter
    } );
  } );

  const vectorComponents = coordinateNames.slice( 0, size );
  proto[ `multiply${vectorName}` ] = wrap( [ vectorVariable ], `return ${vectorVariable}.set${vectorComponents.join( '' ).toUpperCase()}(${vectorComponents.map( ( _, row ) => {
    return vectorComponents.map( ( c, col ) => `${entry( row, col )}*${vectorVariable}.${c}` ).join( '+' );
  } ).join( ',' )});` );
  proto[ `times${vectorName}` ] = wrap( [ vectorVariable ], `return this.multiply${vectorName}(${vectorVariable}.copy());` );

  const affineVectorComponents = coordinateNames.slice( 0, size - 1 );
  if ( affineVectorType ) {
    const affineUpperCase = affineVectorComponents.join( '' ).toUpperCase();
    proto[ `multiply${affineVectorName}` ] = wrap( [ affineVectorVariable ], `return ${affineVectorVariable}.set${affineUpperCase}( ${affineVectorComponents.map( ( _, row ) => {
      return affineVectorComponents.map( ( c, col ) => `${entry( row, col )} * ${affineVectorVariable}.${c}` ).join( '+' ) + `+${entry( row, size - 1 )}`;
    } ).join( ',' )} );` );
    proto[ `times${affineVectorName}` ] = wrap( [ affineVectorVariable ], `return this.multiply${affineVectorName}( ${affineVectorVariable}.copy() );` );

    proto[ `multiply${affineVectorName}Delta` ] = wrap( [ affineVectorVariable ], `return ${affineVectorVariable}.set${affineUpperCase}(${affineVectorComponents.map( ( _, row ) => {
      return affineVectorComponents.map( ( c, col ) => `${entry( row, col )} * ${affineVectorVariable}.${c}` ).join( '+' );
    } ).join( ', ' )});` );
    proto[ `times${affineVectorName}Delta` ] = wrap( [ affineVectorVariable ], `return this.multiply${affineVectorName}Delta(${affineVectorVariable}.copy());` );

    proto.prependTranslation = wrap( affineVectorComponents, `${affineVectorComponents.map( ( c, i ) => `${entry( i, size - 1 )}+=${c};`).join( '' )}return this;` );
  }

  function immutableFromCopy( name, params ) {
    return wrap( params, `return this.copy().${name}(${params.join( ',' )});` );
  }

  proto.set = wrap( [ 'm' ], `${rowMajorLoop( ( row, col ) => `${entry( row, col )}=${entry( row, col, 'm' )};` ).join( '' )}return this;` );
  proto.copy = wrap( [], `return ${name}.dirty().set(this);` );

  proto.getEntry = wrap( [ 'row', 'col' ], `return ${entryCode( 'row', 'col' )};` );
  proto.setEntry = wrap( [ 'row', 'col', 's' ], `return ${entryCode( 'row', 'col' )}=s;` );

  proto.transpose = wrap( [], `return this.rowMajor( ${columnMajorLoop( ( row, col ) => entry( row, col ) ).join( ',' )} );` );
  proto.transposed = immutableFromCopy( 'transpose', [] );

  proto.multiply = wrap( [ 'm' ], `${mutateConsistent( ( row, col ) => {
    return indices.map( index => `${entry( row, index )}*${entry( index, col, 'm' )}` ).join( '+' );
  } )}` );
  proto.times = immutableFromCopy( 'multiply', [ 'm' ] );

  function removeColumn( rows, colIndex ) {
    return rows.map( row => [
      ...row.slice( 0, colIndex ),
      ...row.slice( colIndex + 1, row.length )
    ] );
  }
  function removeRow( rows, rowIndex ) {
    return rows.filter( ( row, index ) => index !== rowIndex );
  }
  function detRecursive( rows ) {
    if ( rows.length === 1 ) {
      assert( rows[ 0 ].length === 1 );

      return [ {
        sign: 1,
        value: rows[ 0 ][ 0 ]
      } ];
    }
    else {
      return flatten( rows[ 0 ].map( ( firstRowItem, index ) => {
        return detRecursive( removeColumn( rows.slice( 1 ), index ) ).map( item => {
          return {
            value: `${firstRowItem} * ${item.value}`,
            sign: item.sign * ( index % 2 === 0 ? 1 : -1 )
          };
        } );
      } ) );
    }
  }
  function detString( rows ) {
    return detRecursive( rows ).map( item => `${item.sign < 0 ? '-' : ''}( ${item.value} )` ).join( ' + ' );
  }
  const thisDetRows = indices.map( row => indices.map( col => entry( row, col ) ) );
  proto.getDeterminant = wrap( [], `return ${detString( thisDetRows )};` );
  forwardGetter( 'getDeterminant', 'determinant' );

  proto.invert = wrap( [], `const det=this.getDeterminant();${mutateConsistent( ( row, col ) => {
    const negative = ( row + col ) % 2 === 1;
    // Yes, these look reversed on purpose
    return `${negative ? '-' : ''}(${detString( removeColumn( removeRow( thisDetRows, col ), row ) )})/det`;
  } )}` );
  proto.inverted = immutableFromCopy( 'invert', [] );

  proto.add = wrap( [ 'm' ], mutateConsistent( ( row, col, ours, theirs ) => `${ours}+${theirs}` ) );
  proto.plus = immutableFromCopy( 'add', [ 'm' ] );

  proto.subtract = wrap( [ 'm' ], mutateConsistent( ( row, col, ours, theirs ) => `${ours}-${theirs}` ) );
  proto.minus = immutableFromCopy( 'subtract', [ 'm' ] );

  proto.negate = wrap( [], mutateConsistent( ( row, col, ours ) => `-${ours}` ) );
  proto.negated = immutableFromCopy( 'negate', [] );

  proto.addScalar = wrap( [ 's' ], mutateConsistent( ( row, col, ours ) => `${ours}+s` ) );
  proto.plusScalar = immutableFromCopy( 'addScalar', [ 's' ] );

  proto.subtractScalar = wrap( [ 's' ], mutateConsistent( ( row, col, ours ) => `${ours}-s` ) );
  proto.minusScalar = immutableFromCopy( 'subtractScalar', [ 's' ] );

  proto.multiplyScalar = wrap( [ 's' ], mutateConsistent( ( row, col, ours ) => `${ours}*s` ) );
  proto.timesScalar = immutableFromCopy( 'multiplyScalar', [ 's' ] );

  proto.divideScalar = wrap( [ 's' ], mutateConsistent( ( row, col, ours ) => `${ours}/s` ) );
  proto.dividedScalar = immutableFromCopy( 'divideScalar', [ 's' ] );

  proto.columnMajor = wrap( columnMajorLoop( ( row, col ) => vari( row, col ) ), `${columnMajorLoop( ( row, col ) => `${entry( row, col )}=${vari( row, col )};` ).join( '' ) } return this;` );
  proto.rowMajor = wrap( rowMajorLoop( ( row, col ) => vari( row, col ) ), `${rowMajorLoop( ( row, col ) => `${entry( row, col )}=${vari( row, col )};` ).join( '' ) } return this;` );
  proto.toString = wrap( [], `return ${indices.map( row => indices.map( col => entry( row, col ) ).join( '+\' \'+' ) ).join( '+\'\\n\'+' )};` );

  proto.setToIdentity = wrap( [], mutateConsistent( ( row, col ) => row === col ? 1 : 0 ) );
  proto.setToTranslation = wrap( affineVectorComponents, mutateConsistent( ( row, col ) => {
    return row === col ? 1 : ( ( col === size - 1 ) ? affineVectorComponents[ row ] : 0 );
  } ) );
  proto.setToScale = wrap( vectorComponents.map( c => `${c}=1` ), mutateConsistent( ( row, col ) => {
    return row === col ? vectorComponents[ row ] : 0;
  } ) );

  proto.equals = wrap( [ 'm' ], `return ${rowMajorLoop( ( row, col ) => `${entry( row, col )}===${entry( row, col, 'm' )}` ).join( '&&' )};` );
  proto.equalsEpsilon = wrap( [ 'm', 'epsilon=1e-8' ], `return ${rowMajorLoop( ( row, col ) => `Math.abs(${entry( row, col )}-${entry( row, col, 'm' )})<epsilon` ).join( '&&' )};` );

  proto.isIdentity = wrap( [], `return this.equals( ${name}.IDENTITY );` );
  proto.isAffine = wrap( [], `return ${indices.map( col => `${entry( size - 1, col )}===${( col === size - 1 ) ? 1 : 0}` ).join( '&&' )};` );
  proto.isFinite = wrap( [], `return ${rowMajorLoop( ( row, col ) => `isFinite(${entry( row, col )})` ).join( '&&' )}` );

  // Rotations
  combinations( 2, indices ).forEach( indexPair => {
    proto[ `setToRotation${vectorComponents[ indexPair[ 0 ] ].toUpperCase()}${vectorComponents[ indexPair[ 1 ] ].toUpperCase()}` ] = wrap( [ 'theta' ],
      `const c=Math.cos(theta);const s=Math.sin(theta);${mutateConsistent( ( row, col ) => {
        if ( ( row === indexPair[ 0 ] && col === indexPair[ 0 ] ) ||
             ( row === indexPair[ 1 ] && col === indexPair[ 1 ] ) ) {
          return 'c';
        }
        else if ( row === indexPair[ 0 ] && col === indexPair[ 1 ] ) {
          return '-s';
        }
        else if ( row === indexPair[ 1 ] && col === indexPair[ 0 ] ) {
          return 's';
        }
        else if ( row === col ) {
          return '1';
        }
        else {
          return '0';
        }
      } )}`
    );
  } );

  const affineMatrixComponents = indices.slice( 0, size - 1 ).map( row => indices.map( col => vari( row, col ) ) );

  // Row major order, all but bottom row
  proto.setToAffine = wrap( affineMatrixComponents, mutateConsistent( ( row, col ) => {
    return row < size - 1 ? vari( row, col ) : ( row === col ? 1 : 0 );
  } ) );

  type.identity = () => type.dirty().setToIdentity();
  type.translation = wrap( affineVectorComponents, `return ${name}.dirty().setToTranslation(${affineVectorComponents.join( ',' )});` );
  type.translationFromVector = wrap( [ affineVectorVariable ], `return ${name}.dirty().setToTranslation(${affineVectorComponents.map( c => `${affineVectorVariable}.${c}` ).join( ',' )});` );
  type.scale = wrap( affineVectorComponents, `return ${name}.dirty().setToScale(${affineVectorComponents.join( ',' )});` );

  // Row major order, all but bottom row
  type.affine = wrap( affineMatrixComponents, `return ${name}.dirty().setToAffine(${affineMatrixComponents.join( ',' )});` );

  Poolable.mixInto( type, {
    initialize: proto.rowMajor,
    useDefaultConstruction: true,
    maxSize: 300
  } );

  type.IDENTITY = type.identity();
}

export default fixedMatrix;
