
import moduloBetweenDown from 'math/moduloBetweenDown';
import namespace from 'namespace';

/**
 * Returns a number in the range $n\in(\mathrm{min},\mathrm{max}]$ with the same equivalence class as the input
 * value mod (max-min), i.e. for a value $m$, $m\equiv n\ (\mathrm{mod}\ \mathrm{max}-\mathrm{min})$.
 *
 * The 'up' indicates that if the value is equal to min or max, the min is returned.
 *
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function moduloBetweenUp( value, min, max ) {
  return -moduloBetweenDown( -value, -max, -min );
}

namespace.moduloBetweenUp = moduloBetweenUp;

export default moduloBetweenUp;
