
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Converts radians to degrees.
 *
 * @param {number} radians
 * @returns {number}
 */
function toDegrees( radians ) {
  assert( typeof radians === 'number' );

  return 180 * radians / Math.PI;
}

namespace.toDegrees = toDegrees;

export default toDegrees;
