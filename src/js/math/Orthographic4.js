
import assert from 'core/assert';
import namespace from 'namespace';
import Ray4 from 'math/Ray4';
import v3 from 'math/v3';
import v4 from 'math/v4';
import Vector3 from 'math/Vector3';
import Vector4 from 'math/Vector4';

const Orthographic4 = {
  /**
   * Returns a 4d ray pointing to all points that would project to the 3d given point.
   * @public
   *
   * @param {Vector3} point3
   * @returns {Ray4}
   */
  projectedPointToRay( point3 ) {
    assert( point3 instanceof Vector3 );

    return Ray4.create( v4( point3.x, point3.y, point3.z, 0 ), Vector4.W_UNIT );
  },

  /**
   * Returns the projection of a 4d point.
   * @public
   *
   * @param {Vector4} point4
   * @returns {Vector3}
   */
  pointToProjection( point4 ) {
    assert( point4 instanceof Vector4 );

    return v3( point4.x, point4.y, point4.z );
  },

  /**
   * Returns the minimum radius of a circle in the projected hyperplane that will fully contain the projected image of
   * a 3-sphere in the full space with the given radius and distance from the camera.
   * @public
   *
   * @param {number} radius
   * @param {number} distance
   * @returns {number}
   */
  minimumRadiusToContainSphere( radius, distance ) {
    assert( typeof radius === 'number' );
    assert( typeof distance === 'number' );
    assert( radius >= 0 );
    assert( distance >= 0 );

    return radius;
  }
};

namespace.Orthographic4 = Orthographic4;

export default Orthographic4;
