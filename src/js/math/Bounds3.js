
import assert from 'core/assert';
import fixedBounds from 'math/fixedBounds';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray3 from 'math/Ray3';
import Vector3 from 'math/Vector3';

class Bounds3 {
  /**
   * @param {number} minX
   * @param {number} minY
   * @param {number} minZ
   * @param {number} maxX
   * @param {number} maxY
   * @param {number} maxZ
   */
  constructor( minX, minY, minZ, maxX, maxY, maxZ ) {
    assert( typeof minX === 'number' );
    assert( typeof minY === 'number' );
    assert( typeof minZ === 'number' );
    assert( typeof maxX === 'number' );
    assert( typeof maxY === 'number' );
    assert( typeof maxZ === 'number' );

    // @public {number}
    this.minX = minX;
    this.minY = minY;
    this.minZ = minZ;
    this.maxX = maxX;
    this.maxY = maxY;
    this.maxZ = maxZ;
  }
}

fixedBounds( Bounds3, 'Bounds3', 3, Vector3, Ray3 );

namespace.Bounds3 = Bounds3;

Poolable.mixInto( Bounds3, {
  initialize: Bounds3.prototype.setMinMax,
  defaultArguments: [ 0, 0, 0, 0, 0, 0 ]
} );

export default Bounds3;
