
import assert from 'core/assert';
import combinations from 'core/combinations';
import coordinateNames from 'math/coordinateNames';
import createForwardGetter from 'core/createForwardGetter';
import createFunctionWrap from 'core/createFunctionWrap';
import rangeInclusive from 'math/rangeInclusive';

/**
 * Sets up general methods for fixed-dimension hyperplanes (e.g. Plane3)
 *
 * @param {function} type - The constructor/class, e.g. Plane3 itself
 * @param {string} name
 * @param {number} size - The dimension of the space (not the dimension of the plane)
 * @param {function} vectorType - Type used for vectors in the same dimension (e.g. Vector3)
 * @param {function} rayType - Type used for rays in the same dimension (e.g. Ray3)
 */
function fixedPlane( type, name, size, vectorType, rayType ) {
  assert( typeof type === 'function' );
  assert( typeof name === 'string' );
  assert( typeof size === 'number' );
  assert( typeof vectorType === 'function' );
  assert( typeof rayType === 'function' );

  // Grabs the actual prototype
  const proto = type.prototype;

  const vectorVariable = `v${size}`;
  const vectorName = `Vector${size}`;
  const rayVariable = `r${size}`;
  const rayName = `Ray${size}`;

  const getAssertions = params => {
    let assertions = '';
    assert( ( params.forEach( param => {
      if ( param === vectorVariable ) {
        assertions += `assert( ${param} instanceof ${vectorName}, '${param} should be an instance of ${vectorName}' );`;
      }
      else if ( param === rayVariable ) {
        assertions += `assert( ${param} instanceof ${rayName}, '${param} should be an instance of ${rayName}' );`;
      }
      else if ( param === 'd' ) {
        assertions += `assert( typeof ${param} === 'number', '${param} should be a number' );`;
      }
    } ), true ) );
    return assertions;
  };

  const wrap = createFunctionWrap( {
    [ name ]: type,
    assert: assert,
    [ vectorName ]: vectorType,
    [ rayName ]: rayType
  }, getAssertions );
  const forwardGetter = createForwardGetter( proto, wrap );

  proto.setPlane = wrap( [ vectorVariable, 'd' ], `this.normal=${vectorVariable};this.distance=d;return this;` );

  // TODO: epilson check for distance
  proto.intersectWithRay = wrap( [ rayVariable, 'epsilon=1e-6' ], `const d=${rayVariable}.distanceToHyperplane(this.distance,this.normal);return d>epsilon?${rayVariable}.pointAtDistance(d):null;` );
  proto.distanceToPoint = wrap( [ vectorVariable ], `return Math.abs(this.distance-${vectorVariable}.dot(this.normal));` );

  const indices = rangeInclusive( 0, size - 1 );
  combinations( size - 1, indices ).forEach( usedIndices => {
    type[ `${usedIndices.map( i => coordinateNames[ i ].toUpperCase() ).join( '' )}` ] = new type( new vectorType( ...indices.map( index => usedIndices.includes( index ) ? 0 : 1 ) ), 0 );
  } );

  proto.flip = wrap( [], 'this.distance=-this.distance;this.normal=this.normal.negated();return this;' );
  proto.flipped = wrap( [], 'return this.copy().flip();' );

  // getCenterPoint() / centerPoint - The point on the plane that is closest to the origin.
  proto.getCenterPoint = wrap( [], 'return this.normal.timesScalar(this.distance);' );
  forwardGetter( 'getCenterPoint', 'centerPoint' );

/* INTERSECTION TODO: need to handle intersections. think CAG/CSG/etc.

   * @param {number} distance - The distance between the ray's position and the point of intersection
   * @param {VectorX} point - The location of the intersection
   * @param {VectorX} normal - The normal (unit vector perpendicular to the segment at the location) at the
   *                           intersection, such that the dot product between the normal and ray direction is <= 0.
   * @param {number} wind - The winding number for the intersection. Either 1 or -1, depending on the direction the
   *                        segment goes relative to the ray (to the left or right). Used for computing Shape
   *                        intersection via the non-zero fill rule.
   * @param {VectorX-1} t - Parametric value (for the segment) of the intersection -- if parametric? maybe custom obj?
*/
}

export default fixedPlane;
