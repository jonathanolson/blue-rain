
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns a number in the range $n\in[\mathrm{min},\mathrm{max})$ with the same equivalence class as the input
 * value mod (max-min), i.e. for a value $m$, $m\equiv n\ (\mathrm{mod}\ \mathrm{max}-\mathrm{min})$.
 *
 * The 'down' indicates that if the value is equal to min or max, the max is returned.
 *
 * @param {number} value
 * @param {number} min
 * @param {number} max
 * @returns {number}
 */
function moduloBetweenDown( value, min, max ) {
  assert( typeof value === 'number' );
  assert( typeof min === 'number' );
  assert( typeof max === 'number' );
  assert( max > min, 'max > min required for moduloBetween' );

  const divisor = max - min;

  // get a partial result of value-min between [0,divisor)
  let partial = ( value - min ) % divisor;
  if ( partial < 0 ) {
    // since if value-min < 0, the remainder will give us a negative number
    partial += divisor;
  }

  return partial + min; // add back in the minimum value
}

namespace.moduloBetweenDown = moduloBetweenDown;

export default moduloBetweenDown;
