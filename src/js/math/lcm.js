
import assert from 'core/assert';
import gcd from 'math/gcd';
import namespace from 'namespace';
import roundSymmetric from 'math/roundSymmetric';

/**
 * Least Common Multiple, https://en.wikipedia.org/wiki/Least_common_multiple
 *
 * @param {number} a
 * @param {number} b
 * @returns {number} lcm, an integer
 */
function lcm( a, b ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );

  return roundSymmetric( Math.abs( a * b ) / gcd( a, b ) );
}

namespace.lcm = lcm;

export default lcm;
