
import fixedMatrix from 'math/fixedMatrix';
import namespace from 'namespace';
import Vector2 from 'math/Vector2';

class Matrix2 {
  constructor() {
    // @private {Array.<number>} - TODO: Performance test with Float64Array?
    this.d = [ 1, 0, 0, 1 ];
  }
}

namespace.Matrix2 = Matrix2;

fixedMatrix( Matrix2, 'Matrix2', 2, Vector2, null );

export default Matrix2;
