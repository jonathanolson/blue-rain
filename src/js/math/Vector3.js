
import assert from 'core/assert';
import coordinateNames from 'math/coordinateNames';
import fixedVector from 'math/fixedVector';
import namespace from 'namespace';

class Vector3 {
  constructor( x, y, z ) {
    assert( x === undefined || typeof x === 'number' );
    assert( y === undefined || typeof y === 'number' );
    assert( z === undefined || typeof z === 'number' );

    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
  }

  /**
   * Sets our value to the Euclidean 3-dimensional cross-product of this vector by the passed-in vector.
   * @public
   *
   * @param {Vector3} v
   * @returns {Vector3}
   */
  setCross( v ) {
    return this.setXYZ(
      this.y * v.z - this.z * v.y,
      this.z * v.x - this.x * v.z,
      this.x * v.y - this.y * v.x
    );
  }

  /**
   * The Euclidean 3-dimensional cross-product of this vector by the passed-in vector.
   * @public
   *
   * @param {Vector3} v
   * @returns {Vector3}
   */
  cross( v ) {
    return Vector3.create(
      this.y * v.z - this.z * v.y,
      this.z * v.x - this.x * v.z,
      this.x * v.y - this.y * v.x
    );
  }
}

fixedVector( Vector3, 'Vector3', coordinateNames.slice( 0, 3 ) );

namespace.Vector3 = Vector3;

export default Vector3;
