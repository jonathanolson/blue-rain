
import assert from 'core/assert';
import namespace from 'namespace';
import Ray3 from 'math/Ray3';
import Vector3 from 'math/Vector3';

/**
 * Ray-sphere intersection, returning information about the closest intersection. Assumes the sphere is centered
 * at the origin (for ease of computation), transform the ray to compensate if needed.
 *
 * If there is no intersection, null is returned. Otherwise an object will be returned like:
 * <pre class="brush: js">
 * {
 *   distance: {number}, // distance from the ray position to the intersection
 *   hitPoint: {Vector3}, // location of the intersection
 *   normal: {Vector3}, // the normal of the sphere's surface at the intersection
 *   fromOutside: {boolean}, // whether the ray intersected the sphere from outside the sphere first
 * }
 * </pre>
 *
 * Assumes a sphere with the specified radius, centered at the origin
 *
 * @param {number} radius
 * @param {Ray3} ray
 * @param {number} [epsilon]
 * @returns {Object}
 */
function sphereRayIntersection( radius, ray, epsilon = 1e-5 ) {
  assert( typeof radius === 'number' );
  assert( ray instanceof Ray3 );
  assert( typeof epsilon === 'number' );

  // center is the origin for now, but leaving in computations so that we can change that in the future. optimize away if needed
  var center = Vector3.ZERO;

  var rayDir = ray.direction;
  var pos = ray.position;
  var centerToRay = pos.minus( center );

  // basically, we can use the quadratic equation to solve for both possible hit points (both +- roots are the hit points)
  var tmp = rayDir.dot( centerToRay );
  var centerToRayDistSq = centerToRay.magnitudeSquared;
  var det = 4 * tmp * tmp - 4 * ( centerToRayDistSq - radius * radius );
  if ( det < epsilon ) {
    // ray misses sphere entirely
    return null;
  }

  var base = rayDir.dot( center ) - rayDir.dot( pos );
  var sqt = Math.sqrt( det ) / 2;

  // the "first" entry point distance into the sphere. if we are inside the sphere, it is behind us
  var ta = base - sqt;

  // the "second" entry point distance
  var tb = base + sqt;

  if ( tb < epsilon ) {
    // sphere is behind ray, so don't return an intersection
    return null;
  }

  var hitPositionB = ray.pointAtDistance( tb );
  var normalB = hitPositionB.minus( center ).normalized();

  if ( ta < epsilon ) {
    // we are inside the sphere
    // in => out
    return {
      distance: tb,
      hitPoint: hitPositionB,
      normal: normalB.negated(),
      fromOutside: false
    };
  }
  else {
    // two possible hits
    var hitPositionA = ray.pointAtDistance( ta );
    var normalA = hitPositionA.minus( center ).normalized();

    // close hit, we have out => in
    return {
      distance: ta,
      hitPoint: hitPositionA,
      normal: normalA,
      fromOutside: true
    };
  }
}

namespace.sphereRayIntersection = sphereRayIntersection;

export default sphereRayIntersection;
