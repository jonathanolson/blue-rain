
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns whether the input is a number that is an integer (no fractional part).
 *
 * @param {number} n
 * @returns {boolean}
 */
function isInteger( n ) {
  assert( typeof n === 'number' );

  return n % 1 === 0;
}

namespace.isInteger = isInteger;

export default isInteger;
