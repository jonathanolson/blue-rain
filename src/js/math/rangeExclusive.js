
import assert from 'core/assert';
import namespace from 'namespace';
import rangeInclusive from 'math/rangeInclusive';

/**
 * Returns an array of integers from A to B (exclusive), e.g. rangeExclusive( 4, 7 ) maps to [ 5, 6 ].
 *
 * @param {number} a
 * @param {number} b
 * @returns {Array.<number>}
 */
function rangeExclusive( a, b ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );

  return rangeInclusive( a + 1, b - 1 );
}

namespace.rangeExclusive = rangeExclusive;

export default rangeExclusive;
