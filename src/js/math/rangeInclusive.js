
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns an array of integers from A to B (inclusive), e.g. rangeInclusive( 4, 7 ) maps to [ 4, 5, 6, 7 ].
 *
 * @param {number} a
 * @param {number} b
 * @returns {Array.<number>}
 */
function rangeInclusive( a, b ) {
  assert( typeof a === 'number' && a % 1 === 0 );
  assert( typeof b === 'number' && b % 1 === 0 );

  if ( b < a ) {
    return [];
  }
  const result = new Array( b - a + 1 );
  for ( let i = a; i <= b; i++ ) {
    result[ i - a ] = i;
  }
  return result;
}

namespace.rangeInclusive = rangeInclusive;

export default rangeInclusive;
