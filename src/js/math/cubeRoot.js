
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns the unique real cube root of x, such that $y^3=x$.
 *
 * @param {number} x
 * @returns {number}
 */
function cubeRoot( x ) {
  assert( typeof x === 'number' );

  return x >= 0 ? Math.pow( x, 1 / 3 ) : -Math.pow( -x, 1 / 3 );
}

namespace.cubeRoot = cubeRoot;

export default cubeRoot;
