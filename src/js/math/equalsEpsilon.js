
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns true if two numbers are within epsilon of each other.
 *
 * @param {number} a
 * @param {number} b
 * @param {number} [epsilon]
 * @return {boolean}
 */
function equalsEpsilon( a, b, epsilon = 1e-8 ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );
  assert( typeof epsilon === 'number' );

  return Math.abs( a - b ) <= epsilon;
}

namespace.equalsEpsilon = equalsEpsilon;

export default equalsEpsilon;
