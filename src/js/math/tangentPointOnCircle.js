
import assert from 'core/assert';
import namespace from 'namespace';
import v2 from 'math/v2';

/**
 * Returns a point on a circle (at the origin) with the given radius, at the position where a tangent line (which
 * would go through the point (px,0)) would touch the circle. Returns with a non-negative y value.
 *
 * @param {number} radius
 * @param {number} px
 * @returns {Vector2}
 */
function tangentPointOnCircle( radius, px ) {
  assert( typeof radius === 'number' );
  assert( typeof px === 'number' );
  assert( px >= radius );

  const magSq = px * px;
  const discriminant = magSq - radius * radius;
  const sqrt = Math.sqrt( discriminant );
  return v2(
    ( radius / magSq ) * ( radius * px ),
    ( radius / magSq ) * ( px * sqrt )
  );
}

namespace.tangentPointOnCircle = tangentPointOnCircle;

export default tangentPointOnCircle;
