
import assert from 'core/assert';
import namespace from 'namespace';
import rangeInclusive from 'math/rangeInclusive';

class Permutation {
  /**
   * Creates a permutation that will rearrange a list so that newList[i] = oldList[permutation[i]]
   *
   * @param {Array.<number>} indices
   */
  constructor( indices ) {
    // @private {Array.<number>}
    this.indices = indices;
  }

  get length() {
    return this.indices.length;
  }

  /**
   * Applies this permutation to an array, returning the result.
   * @public
   *
   * @param {Array.<*>} array
   * @returns {Array.<*>}
   */
  apply( array ) {
    assert( Array.isArray( array ) );
    assert( array.length === this.length, 'Permutation length ' + this.length + ' not equal to list length ' + array.length );

    // permute it as an array
    const result = new Array( array.length );
    for ( let i = 0; i < array.length; i++ ) {
      result[ i ] = array[ this.indices[ i ] ];
    }
    return result;
  }

  /**
   * Permute a single index.
   * @public
   *
   * @param {number} n
   * @returns {number}
   */
  applyToIndex( n ) {
    return this.indices[ n ];
  }

  /**
   * Returns the inverse of this permutation.
   * @public
   *
   * @returns {Permutation}
   */
  inverted() {
    const newPermutation = new Array( this.length );
    for ( let i = 0; i < this.length; i++ ) {
      newPermutation[ this.indices[ i ] ] = i;
    }
    return new Permutation( newPermutation );
  }

  // TODO: doc?
  withIndicesPermuted( indices ) {
    const result = [];
    Permutation.forEachPermutation( indices, integers => {
      const oldIndices = this.indices;
      const newPermutation = oldIndices.slice( 0 );

      for ( let i = 0; i < indices.length; i++ ) {
        newPermutation[ indices[ i ] ] = oldIndices[ integers[ i ] ];
      }
      result.push( new Permutation( newPermutation ) );
    } );
    return result;
  }

  toString() {
    return `Permutation[${this.indices.join( ',' )}]`;
  }

  /**
   * Returns all permutations of a given size.
   * @public
   *
   * @returns {Array.<Permutation>}
   */
  static permutations( size ) {
    assert( typeof size === 'number' && size >= 0 );

    const result = [];
    Permutation.forEachPermutation( rangeInclusive( 0, size - 1 ), integers => {
      result.push( new Permutation( integers ) );
    } );
    return result;
  }

  /**
   * Creates an identity permutation.
   * @public
   *
   * @returns {Permutation}
   */
  static identity( size ) {
    assert( typeof size === 'number' && size >= 0 );

    const indices = new Array( size );
    for ( let i = 0; i < size; i++ ) {
      indices[ i ] = i;
    }
    return new Permutation( indices );
  }

  /**
   * Call our function with each permutation of the provided list PREFIXED by prefix, in lexicographic order
   * @private
   *
   * @param {Array.<*>} array
   * @param {Array.<*>} prefix - Elements that should be inserted at the front of each list before each call
   * @param {function} callback - Called with function( {Array.<*>} ) for each permutation
   */
  static recursiveForEachPermutation( array, prefix, callback ) {
    if ( array.length === 0 ) {
      callback( prefix );
    }
    else {
      for ( let i = 0; i < array.length; i++ ) {
        const element = array[ i ];

        // remove the element from the array
        const nextArray = array.slice( 0 );
        nextArray.splice( i, 1 );

        // add it into the prefix
        const nextPrefix = prefix.slice( 0 );
        nextPrefix.push( element );

        Permutation.recursiveForEachPermutation( nextArray, nextPrefix, callback );
      }
    }
  }

  /**
   * Calls the callback for each permuted copy of the given array.
   * @public
   *
   * @param {Array.<*>} array
   * @param {function} callback - Called with function( {Array.<*>} ) for each permutation
   */
  static forEachPermutation( array, callback ) {
    Permutation.recursiveForEachPermutation( array, [], callback );
  }
}

namespace.Permutation = Permutation;

export default Permutation;
