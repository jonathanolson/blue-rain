
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Defines and evaluates a linear mapping. The mapping is defined so that $f(a_1)=b_1$ and $f(a_2)=b_2$, and other
 * values are interpolated along the linear equation. The returned value is $f(a_3)$.
 *
 * @param {number} a1
 * @param {number} a2
 * @param {number} b1
 * @param {number} b2
 * @param {number} a3
 * @returns {number}
 */
function linear( a1, a2, b1, b2, a3 ) {
  assert( typeof a1 === 'number' );
  assert( typeof a2 === 'number' );
  assert( typeof b1 === 'number' );
  assert( typeof b2 === 'number' );
  assert( typeof a3 === 'number' );

  return ( b2 - b1 ) / ( a2 - a1 ) * ( a3 - a1 ) + b1;
}

namespace.linear = linear;

export default linear;
