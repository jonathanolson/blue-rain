
import assert from 'core/assert';
import namespace from 'namespace';
import v2 from 'math/v2';

/**
 * Computes the intersection of the two line segments $(x_1,y_1)(x_2,y_2)$ and $(x_3,y_3)(x_4,y_4)$. If there is no
 * intersection, null is returned.
 *
 * @param {number} x1
 * @param {number} y1
 * @param {number} x2
 * @param {number} y2
 * @param {number} x3
 * @param {number} y3
 * @param {number} x4
 * @param {number} y4
 * @param {number} [epsilon]
 * @returns {Vector2|null}
 */
function lineSegmentIntersection( x1, y1, x2, y2, x3, y3, x4, y4, epsilon = 1e-10 ) {
  assert( typeof x1 === 'number' );
  assert( typeof y1 === 'number' );
  assert( typeof x2 === 'number' );
  assert( typeof y2 === 'number' );
  assert( typeof x3 === 'number' );
  assert( typeof y3 === 'number' );
  assert( typeof x4 === 'number' );
  assert( typeof y4 === 'number' );
  assert( typeof epsilon === 'number' );

  // Determines counterclockwiseness. Positive if counterclockwise, negative if clockwise, zero if straight line
  // Point1(a,b), Point2(c,d), Point3(e,f)
  // See http://jeffe.cs.illinois.edu/teaching/373/notes/x05-convexhull.pdf
  // @returns {number}
  const ccw = ( a, b, c, d, e, f ) => ( f - b ) * ( c - a ) - ( d - b ) * ( e - a );

  // Check if intersection doesn't exist. See http://jeffe.cs.illinois.edu/teaching/373/notes/x06-sweepline.pdf
  // If point1 and point2 are on opposite sides of line 3 4, exactly one of the two triples 1, 3, 4 and 2, 3, 4
  // is in counterclockwise order.
  if ( ccw( x1, y1, x3, y3, x4, y4 ) * ccw( x2, y2, x3, y3, x4, y4 ) > 0 ||
       ccw( x3, y3, x1, y1, x2, y2 ) * ccw( x4, y4, x1, y1, x2, y2 ) > 0
  ) {
    return null;
  }

  const denom = ( x1 - x2 ) * ( y3 - y4 ) - ( y1 - y2 ) * ( x3 - x4 );
  // If denominator is 0, the lines are parallel or coincident
  if ( Math.abs( denom ) < epsilon ) {
    return null;
  }

  // Check if there is an exact endpoint overlap (and then return an exact answer).
  if ( ( x1 === x3 && y1 === y3 ) || ( x1 === x4 && y1 === y4 ) ) {
    return v2( x1, y1 );
  }
  else if ( ( x2 === x3 && y2 === y3 ) || ( x2 === x4 && y2 === y4 ) ) {
    return v2( x2, y2 );
  }

  // Use determinants to calculate intersection, see https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
  const intersectionX = ( ( x1 * y2 - y1 * x2 ) * ( x3 - x4 ) - ( x1 - x2 ) * ( x3 * y4 - y3 * x4 ) ) / denom;
  const intersectionY = ( ( x1 * y2 - y1 * x2 ) * ( y3 - y4 ) - ( y1 - y2 ) * ( x3 * y4 - y3 * x4 ) ) / denom;
  return v2( intersectionX, intersectionY );
}

namespace.lineSegmentIntersection = lineSegmentIntersection;

export default lineSegmentIntersection;
