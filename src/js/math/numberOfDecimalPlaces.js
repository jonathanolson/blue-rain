
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Determines the number of decimal places in a value.
 *
 * @param {number} value
 * @returns {number}
 */
function numberOfDecimalPlaces( value ) {
  assert( typeof value === 'number' );

  let count = 0;
  let multiplier = 1;
  while ( ( value * multiplier ) % 1 !== 0 ) {
    count++;
    multiplier *= 10;
  }
  return count;
}

namespace.numberOfDecimalPlaces = numberOfDecimalPlaces;

export default numberOfDecimalPlaces;
