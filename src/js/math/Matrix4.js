
import fixedMatrix from 'math/fixedMatrix';
import namespace from 'namespace';
import Vector3 from 'math/Vector3';
import Vector4 from 'math/Vector4';

class Matrix4 {
  constructor() {
    // @private {Array.<number>} - TODO: Performance test with Float64Array?
    this.d = [ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ];
  }
}

namespace.Matrix4 = Matrix4;

fixedMatrix( Matrix4, 'Matrix4', 4, Vector4, Vector3 );

export default Matrix4;
