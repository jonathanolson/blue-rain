
import distToSegmentSquared from 'math/distToSegmentSquared';
import namespace from 'namespace';

/**
 * distance from a point to a line segment squared.
 *
 * @param {Vector2} point - The point
 * @param {Vector2} a - Starting point of the line segment
 * @param {Vector2} b - Ending point of the line segment
 * @returns {number}
 */
function distToSegment( point, a, b ) {
  return Math.sqrt( distToSegmentSquared( point, a, b ) );
}

namespace.distToSegment = distToSegment;

export default distToSegment;
