
import assert from 'core/assert';
import coordinateNames from 'math/coordinateNames';
import fixedVector from 'math/fixedVector';
import namespace from 'namespace';

class Vector4 {
  constructor( x, y, z, w ) {
    assert( x === undefined || typeof x === 'number' );
    assert( y === undefined || typeof y === 'number' );
    assert( z === undefined || typeof z === 'number' );
    assert( w === undefined || typeof w === 'number' );

    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
    this.w = w || 0;
  }
}

fixedVector( Vector4, 'Vector4', coordinateNames.slice( 0, 4 ) );

namespace.Vector4 = Vector4;

export default Vector4;
