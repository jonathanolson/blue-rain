
import fixedMatrix from 'math/fixedMatrix';
import namespace from 'namespace';
import Vector4 from 'math/Vector4';
import Vector5 from 'math/Vector5';

class Matrix5 {
  constructor() {
    // @private {Array.<number>}
    this.d = [ 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1 ];
  }
}

namespace.Matrix5 = Matrix5;

fixedMatrix( Matrix5, 'Matrix5', 5, Vector5, Vector4 );

export default Matrix5;
