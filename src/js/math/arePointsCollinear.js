
import assert from 'core/assert';
import namespace from 'namespace';
import triangleArea from 'math/triangleArea';
import Vector2 from 'math/Vector2';

/**
 * Determines whether the three points are approximately collinear.
 *
 * @param {Vector2} a
 * @param {Vector2} b
 * @param {Vector2} c
 * @param {number} epsilon
 * @returns {boolean}
 */
function arePointsCollinear( a, b, c, epsilon = 1e-10 ) {
  assert( a instanceof Vector2 );
  assert( b instanceof Vector2 );
  assert( c instanceof Vector2 );
  assert( typeof epsilon === 'number' );

  return triangleArea( a, b, c ) <= epsilon;
}

namespace.arePointsCollinear = arePointsCollinear;

export default arePointsCollinear;
