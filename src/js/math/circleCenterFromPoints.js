
import assert from 'core/assert';
import lineLineIntersection from 'math/lineLineIntersection';
import namespace from 'namespace';
import v2 from 'math/v2';
import Vector2 from 'math/Vector2';

/**
 * Returns the center of a circle that will lie on 3 points (if it exists), otherwise null (if collinear).
 *
 * @param {Vector2} p1
 * @param {Vector2} p2
 * @param {Vector2} p3
 * @returns {Vector2|null}
 */
function circleCenterFromPoints( p1, p2, p3 ) {
  assert( p1 instanceof Vector2 );
  assert( p2 instanceof Vector2 );
  assert( p3 instanceof Vector2 );

  // TODO: Can we make scratch vectors here, avoiding the circular reference?

  // midpoints between p1-p2 and p2-p3
  const p12 = v2( ( p1.x + p2.x ) / 2, ( p1.y + p2.y ) / 2 );
  const p23 = v2( ( p2.x + p3.x ) / 2, ( p2.y + p3.y ) / 2 );

  // perpendicular points from the minpoints
  const p12x = v2( p12.x + ( p2.y - p1.y ), p12.y - ( p2.x - p1.x ) );
  const p23x = v2( p23.x + ( p3.y - p2.y ), p23.y - ( p3.x - p2.x ) );

  return lineLineIntersection( p12, p12x, p23, p23x );
}

namespace.circleCenterFromPoints = circleCenterFromPoints;

export default circleCenterFromPoints;
