
import assert from 'core/assert';
import createForwardGetter from 'core/createForwardGetter';
import createFunctionWrap from 'core/createFunctionWrap';
import mapJoin from 'core/mapJoin';
import Poolable from 'core/Poolable';

/**
 * Sets up general methods for fixed-length vector types (e.g. Vector2).
 *
 * @param {function} type - The constructor/class, e.g. Vector2 itself
 * @param {string} name - The name of the type, e.g. 'Vector2'
 * @param {Array.<string>} components - The names of the scalar components, e.g. [ 'x', 'y' ]
 */
function fixedVector( type, name, components ) {
  assert( typeof type === 'function' );
  assert( typeof name === 'string' );
  assert( Array.isArray( components ) );

  // Grabs the actual prototype
  const proto = type.prototype;
  const allUnitsCap = components.join( '' ).toUpperCase();

  // {Array.<Array.<string>>}, e.g. [ [ 'x', 'y' ], [ 'x' ], [ 'y' ] ]
  const allSubcomponents = [ components, ...components.map( cs => [ cs ] ) ];

  const capComponents = subcomponents => subcomponents.join( '' ).toUpperCase();
  const getAssertions = params => {
    let assertions = '';
    assert( ( params.forEach( param => {
      if ( param === 'v' ) {
        assertions += `assert( ${param} instanceof ${name}, '${param} should be an instance of ${name}' );`;
      }
      else {
        assertions += `assert( typeof ${param} === 'number' && isFinite( ${param} ), '${param} should be a number' );`;
      }
    } ), true ) );
    return assertions;
  };
  const wrap = createFunctionWrap( {
    [ name ]: type,
    assert: assert
  }, getAssertions );
  const forwardGetter = createForwardGetter( proto, wrap );
  function immutable( params, componentMap, preamble ) {
    return wrap( params, `${preamble || ''}return ${name}.create(${mapJoin( components, componentMap, ',' )});` );
  }
  function mutable( params, statements ) {
    return wrap( params, `${statements.join( '' )}return this;` );
  }
  function inputVectorPair( immutableName, generator ) {
    // e.g. distance( v )
    proto[ immutableName ] = wrap( [ 'v' ], generator( c => `v.${c}` ) );

    // e.g. distanceXY( x, y )
    proto[ `${immutableName}${allUnitsCap}` ] = wrap( components, generator( c => c ) );
  }
  function binaryPair( immutableName, mutableName, immutableScalarName, mutableScalarName, op, mutateOp ) {
    const mutableDirectOp = c => `${mutateOp( `this.${c}`, c )};`;
    const immutableDirectOp = c => op( `this.${c}`, c );

    // e.g. plus( v )
    proto[ immutableName ] = immutable( [ 'v' ], c => op( `this.${c}`, `v.${c}` ), '' );

    // e.g. add( v )
    proto[ mutableName ] = mutable( [ 'v' ], components.map( c => `${mutateOp( `this.${c}`, `v.${c}` )};` ) );

    // e.g. plusScalar( n )
    proto[ immutableScalarName ] = immutable( [ 'n' ], c => op( `this.${c}`, 'n' ), '' );

    // e.g. addScalar( n )
    proto[ mutableScalarName ] = mutable( [ 'n' ], components.map( c => `${mutateOp( `this.${c}`, 'n' )};` ) );

    allSubcomponents.forEach( subcomponents => {
      // e.g. plusXY( x, y ), plusX( x )
      proto[ `${immutableName}${capComponents( subcomponents )}` ] = immutable( subcomponents, c => subcomponents.includes( c ) ? immutableDirectOp( c ) : `this.${c}` );

      // e.g. addXY( x, y ), addX( x )
      proto[ `${mutableName}${capComponents( subcomponents )}` ] = mutable( subcomponents, subcomponents.map( mutableDirectOp ) );
    } );
  }
  binaryPair( 'plus', 'add', 'plusScalar', 'addScalar', ( a, b ) => `${a}+${b}`, ( a, b ) => `${a}+=${b}` );
  binaryPair( 'minus', 'subtract', 'minusScalar', 'subtractScalar', ( a, b ) => `${a}-${b}`, ( a, b ) => `${a}-=${b}` );
  binaryPair( 'componentTimes', 'componentMultiply', 'timesScalar', 'multiplyScalar', ( a, b ) => `${a}*${b}`, ( a, b ) => `${a}*=${b}` );
  binaryPair( 'componentDivided', 'componentDivide', 'dividedScalar', 'divideScalar', ( a, b ) => `${a}/${b}`, ( a, b ) => `${a}/=${b}` );
  binaryPair( 'componentExponentiated', 'componentExponentiate', 'exponentiatedScalar', 'exponentiateScalar', ( a, b ) => `${a}**${b}`, ( a, b ) => `${a}**=${b}` );

  // distanceSquared( v ), e.g. distanceSquaredXY( x, y )
  inputVectorPair( 'distanceSquared', map => {
    return `${mapJoin( components, c => `const d${c}=this.${c}-${map( c )};` )}return ${mapJoin( components, c => `d${c}*d${c}`, '+' )};`;
  } );

  // distance( v ), e.g. distanceXY( x, y )
  inputVectorPair( 'distance', map => {
    return `${mapJoin( components, c => `const d${c}=this.${c}-${map( c )};` )}return Math.sqrt( ${mapJoin( components, c => `d${c}*d${c}`, '+' )} );`;
  } );

  // dot( v ), e.g. dotXY( x, y )
  inputVectorPair( 'dot', map => {
    return `return ${mapJoin( components, c => `this.${c}*${map( c )}`, '+' )};`;
  } );

  // equals( v ), e.g. equalsXY( x, y )
  inputVectorPair( 'equals', map => {
    return `return ${mapJoin( components, c => `this.${c}===${map( c )}`, '&&' )};`;
  } );

  proto.isFinite = wrap( [], `return ${mapJoin( components, c => `isFinite(this.${c})`, '&&' )};` );

  // TODO: equalsEpsilon
  // TODO: isFinite
  // TODO: average
  // TODO: conversion to other types
  // TODO: rounding
  // TODO: withMagnitude
  // TODO: 2d crossScalar, perpendicular, rotateAboutXY, rotateAboutPoint (and immutable) rotate() setPolar createPolar
  // TODO: 3d things

  // magnitudeSquared
  proto.getMagnitudeSquared = wrap( [], `return ${mapJoin( components, c => `this.${c}*this.${c}`, '+' ) };` );
  forwardGetter( 'getMagnitudeSquared', 'magnitudeSquared' );

  // magnitude
  proto.getMagnitude = wrap( [], `return Math.sqrt( ${mapJoin( components, c => `this.${c}*this.${c}`, '+' ) } );` );
  forwardGetter( 'getMagnitude', 'magnitude' );

  // copy
  proto.copy = wrap( [], `return ${name}.create( ${mapJoin( components, c => `this.${c}`, ',' )} );` );

  // normalize / normalized
  proto.normalize = wrap( [], 'const mag=this.getMagnitude();if(mag===0){throw new Error (\'Cannot normalize a 0-magnitude vector\');}else{return this.divideScalar(mag);}' );
  proto.normalized = wrap( [], 'return this.copy().normalize();' );

  // negate / negated
  proto.negate = wrap( [], `${mapJoin( components, c => `this.${c}=-this.${c};` )}return this;` );
  proto.negated = wrap( [], 'return this.copy().negate();' );

  // toString
  proto.toString = wrap( [], `return '${name}(' + ${mapJoin( components, c => `this.${c}`, '+\', \'+' )} + ')';` );

  // blended( v, ratio ) / blend( v, ratio )
  proto.blended = immutable( [ 'v', 'r' ], c => `this.${c}+(v.${c}-this.${c})*r` );
  proto.blend = mutable( [ 'v', 'r' ], components.map( c => `this.${c}+=(v.${c}-this.${c})*r;` ) );

  proto[ `toVec${components.length}` ] = wrap( [], `return \`vec${components.length}(${mapJoin( components, c => `this.${c}`, ',' )})\`;` );

  allSubcomponents.forEach( subcomponents => {
    // e.g. withXY( x, y ), withX( x ), etc.
    proto[ `with${capComponents( subcomponents )}` ] = immutable( subcomponents, cc => subcomponents.includes( cc ) ? cc : `this.${cc}` );

    // e.g. setXY( x, y ), setX( x ), etc.
    proto[ `set${capComponents( subcomponents )}` ] = mutable( subcomponents, subcomponents.map( c => `this.${c}=${c};` ) );
  } );

  proto.set = wrap( [ 'v' ], `return this.set${allUnitsCap}(${mapJoin( components, c => `v.${c}`, ',' )});` );

  Poolable.mixInto( type, {
    initialize: proto[ `set${allUnitsCap}` ]
  } );

  // vector constants, e.g. ONE, ZERO, X_UNIT, etc.
  type.ZERO = new type( ...( components.map( c => 0 ) ) );
  type.ONE = new type( ...( components.map( c => 1 ) ) );
  components.forEach( c => {
    type[ `${c.toUpperCase()}_UNIT` ] = new type( ...( components.map( cc => cc === c ? 1 : 0 ) ) );
  } );
}

export default fixedVector;
