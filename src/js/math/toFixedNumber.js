
import assert from 'core/assert';
import namespace from 'namespace';
import toFixed from 'math/toFixed';

/**
 * A predictable implementation of toFixed, where the result is returned as a number instead of a string.
 *
 * JavaScript's toFixed is notoriously buggy, behavior differs depending on browser,
 * because the spec doesn't specify whether to round or floor.
 * Rounding is symmetric for positive and negative values, see roundSymmetric.
 *
 * @param {number} value
 * @param {number} decimalPlaces
 * @returns {number}
 */
function toFixedNumber( value, decimalPlaces ) {
  assert( typeof value === 'number' );
  assert( typeof decimalPlaces === 'number' );

  return parseFloat( toFixed( value, decimalPlaces ) );
}

namespace.toFixedNumber = toFixedNumber;

export default toFixedNumber;
