
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Get the median from an unsorted array of numbers
 *
 * @param {Array.<number>} numbers - (un)sorted array
 * @returns {number|null} - null if array is empty
 */
function median( numbers ) {
  assert( Array.isArray( numbers ) );

  numbers.sort( ( a, b ) => a - b );

  if ( numbers.length === 0 ) {
    return null;
  }

  const half = Math.floor( numbers.length / 2 );

  if ( numbers.length % 2 ) {
    return numbers[ half ];
  }
  else {
    return ( numbers[ half - 1 ] + numbers[ half ] ) / 2;
  }
}

namespace.median = median;

export default median;
