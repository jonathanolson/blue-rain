
import assert from 'core/assert';
import fixedRay from 'math/fixedRay';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Vector3 from 'math/Vector3';

class Ray3 {
  constructor( position, direction ) {
    assert( Math.abs( direction.magnitude - 1 ) < 1e-4 , 'the direction must be a unit vector' );

    // @public {Vector3}
    this.position = position;
    this.direction = direction;
  }
}

fixedRay( Ray3, 'Ray3', 3, Vector3 );

namespace.Ray3 = Ray3;

Poolable.mixInto( Ray3, {
  defaultArguments: [ Vector3.ZERO, Vector3.X_UNIT ],
  initialize: Ray3.prototype.setPosDir
} );

export default Ray3;
