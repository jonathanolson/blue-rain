
import assert from 'core/assert';
import namespace from 'namespace';
import roundSymmetric from 'math/roundSymmetric';

/**
 * A predictable implementation of toFixed.
 *
 * JavaScript's toFixed is notoriously buggy, behavior differs depending on browser,
 * because the spec doesn't specify whether to round or floor.
 * Rounding is symmetric for positive and negative values, see roundSymmetric.
 *
 * @param {number} value
 * @param {number} decimalPlaces
 * @returns {string}
 */
function toFixed( value, decimalPlaces ) {
  assert( typeof value === 'number' );
  assert( typeof decimalPlaces === 'number' );

  const multiplier = Math.pow( 10, decimalPlaces );
  const newValue = roundSymmetric( value * multiplier ) / multiplier;
  return newValue.toFixed( decimalPlaces );
}

namespace.toFixed = toFixed;

export default toFixed;
