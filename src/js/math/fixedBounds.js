
import assert from 'core/assert';
import coordinateNames from 'math/coordinateNames';
import createForwardGetter from 'core/createForwardGetter';
import createFunctionWrap from 'core/createFunctionWrap';
import mapJoin from 'core/mapJoin';

/**
 * Sets up general methods for fixed-dimension hypercube (e.g. Bounds3)
 *
 * @param {function} type - The constructor/class, e.g. Bounds3 itself
 * @param {string} name
 * @param {number} size - The dimension of the space (not the dimension of the plane)
 * @param {function} vectorType - Type used for vectors in the same dimension (e.g. Vector3)
 * @param {function} rayType - Type used for rays in the same dimension (e.g. Ray3)
 */
function fixedBounds( type, name, size, vectorType, rayType ) {
  assert( typeof type === 'function' );
  assert( typeof name === 'string' );
  assert( typeof size === 'number' );
  assert( typeof vectorType === 'function' );
  assert( typeof rayType === 'function' );

  // Grabs the actual prototype
  const proto = type.prototype;

  const vectorVariable = `v${size}`;
  const vectorName = `Vector${size}`;
  const rayVariable = `r${size}`;
  const rayName = `Ray${size}`;

  const coordinates = coordinateNames.slice( 0, size );
  const capCoordinates = coordinates.map( c => c.toUpperCase() );
  const minCoordinates = capCoordinates.map( c => `min${c}` );
  const maxCoordinates = capCoordinates.map( c => `max${c}` );
  const allCoordinates = [ ...minCoordinates, ...maxCoordinates ];

  const getAssertions = params => {
    let assertions = '';
    assert( ( params.forEach( param => {
      if ( param === vectorVariable ) {
        assertions += `assert( ${param} instanceof ${vectorName}, '${param} should be an instance of ${vectorName}' );`;
      }
      else if ( param === rayVariable ) {
        assertions += `assert( ${param} instanceof ${rayName}, '${param} should be an instance of ${rayName}' );`;
      }
      else if ( param === 'b' ) {
        assertions += `assert( ${param} instanceof ${name}, '${param} should be an instance of ${name}' );`;
      }
      else if ( param === 'd' || coordinates.includes( param ) ) {
        assertions += `assert( typeof ${param} === 'number', '${param} should be a number' );`;
      }
    } ), true ) );
    return assertions;
  };

  const wrap = createFunctionWrap( {
    [ name ]: type,
    assert: assert,
    [ vectorName ]: vectorType,
    [ rayName ]: rayType
  }, getAssertions );
  const forwardGetter = createForwardGetter( proto, wrap );

  proto.setMinMax = wrap( allCoordinates, `${mapJoin( allCoordinates, c => `this.${c}=${c};` )};return this;` );

  capCoordinates.forEach( ( capCoordinate, index ) => {
    proto[ `getSize${capCoordinate}` ] = wrap( [], `return this.${maxCoordinates[ index ]}-this.${minCoordinates[ index ]};` );
    forwardGetter( `getSize${capCoordinate}`, `size${capCoordinate}` );
  } );

  proto.getArea = wrap( [], `return ${mapJoin( capCoordinates, c => `this.size${c}`, '*' )};` );
  forwardGetter( 'getArea', 'area' );

  proto.isEmpty = wrap( [], `return ${mapJoin( capCoordinates, c => `this.size${c}<0`, '||' )}` );
  proto.isFinite = wrap( [], `return ${mapJoin( allCoordinates, c => `isFinite(this.${c})`, '&&' )}` );
  proto.isValid = wrap( [], 'return !this.isEmpty()&&this.isFinite();' );
  proto.containsCoordinates = wrap( coordinates, `return ${mapJoin( coordinates, ( c, index ) => `this.${minCoordinates[ index ]}<=${c}&&${c}<=this.${maxCoordinates[ index ]}`, '&&' )};` );
  proto.containsBounds = wrap( [ 'b' ], `return ${mapJoin( coordinates, ( c, index ) => `this.${minCoordinates[ index ]}<=b.${minCoordinates[ index ]}&&b.${maxCoordinates[ index ]}<=this.${maxCoordinates[ index ]}`, '&&' )};` );
  proto.containsPoint = wrap( [ vectorVariable ], `return this.containsCoordinates(${mapJoin( coordinates, c => `${vectorVariable}.${c}`, ',' )});` );
  proto.intersectsBounds = wrap( [ 'b' ], `return ${mapJoin( capCoordinates, c => `Math.min(this.max${c},b.max${c})-Math.max(this.min${c},b.min${c})>=0`, '&&' )}` );
  proto.closestPointTo = wrap( [ vectorVariable ], `if(this.containsPoint(${vectorVariable})){return ${vectorVariable}}else{return ${vectorName}.create(${mapJoin( coordinates, ( c, i ) => `Math.max(Math.min(${vectorVariable}.${c},this.${maxCoordinates[ i ]}),this.${minCoordinates[ i ]})`, ',' )});}` );
  proto.equals = wrap( [ 'b' ], `return ${mapJoin( allCoordinates, c => `this.${c}===b.${c}`, '&&' )};` );
  proto.equalsEpsilon = wrap( [ 'b', 'epsilon=1e-8' ], `return ${mapJoin( allCoordinates, c => `(isFinite(this.${c})===isFinite(b.${c})&&(isFinite(this.${c})?Math.abs(this.${c}-b.${c})<epsilon:this.${c}===b.${c}))`, '&&' )};` );
  proto.copy = wrap( [], `return ${name}.dirty().setMinMax(${mapJoin( allCoordinates, c => `this.${c}`, ',' )});` );
  proto.set = wrap( [ 'b' ], `${mapJoin( allCoordinates, c => `this.${c}=b.${c};` )}return this;` );

  proto.includeBounds = wrap( [ 'b' ], `return this.setMinMax(${mapJoin( capCoordinates, c => `Math.min(this.min${c},b.min${c})`, ',' )},${mapJoin( capCoordinates, c => `Math.max(this.max${c},b.max${c})`, ',' )});` );
  proto.constrainBounds = wrap( [ 'b' ], `return this.setMinMax(${mapJoin( capCoordinates, c => `Math.max(this.min${c},b.min${c})`, ',' )},${mapJoin( capCoordinates, c => `Math.min(this.max${c},b.max${c})`, ',' )});` );
  proto.union = wrap( [ 'b' ], 'return this.copy().includeBounds(b);' );
  proto.intersection = wrap( [ 'b' ], 'return this.copy().constrainBounds(b);' );

  // TODO: consider separating out intersection methods?
  // NOTE: assumes maxX > minX
  /* e.g.
      let tNear = Number.NEGATIVE_INFINITY;
      let tFar = Number.POSITIVE_INFINITY;

      if ( ray.direction.x > 0 ) {
        tNear = Math.max( tNear, ( this.minX - ray.position.x ) / ray.direction.x );
        tFar = Math.min( tFar, ( this.maxX - ray.position.x ) / ray.direction.x );
      }
      else if ( ray.direction.x < 0 ) {
        tNear = Math.max( tNear, ( this.maxX - ray.position.x ) / ray.direction.x );
        tFar = Math.min( tFar, ( this.minX - ray.position.x ) / ray.direction.x );
      }
      ...
   */
  proto.rayIntersectionRange = wrap( [ 'ray' ], `let tNear=Number.NEGATIVE_INFINITY;let tFar=Number.POSITIVE_INFINITY;${mapJoin( coordinates, c => {
    function clause( tName, mathName, selfName ) {
      return `${tName}=Math.${mathName}(${tName},(this.${selfName}${c.toUpperCase()}-ray.position.${c})/ray.direction.${c});`;
    }
    return `if(ray.direction.${c}>0){${clause( 'tNear', 'max', 'min' )}${clause( 'tFar', 'min', 'max' )}}else if(ray.direction.${c}<0){${clause( 'tNear', 'max', 'max' )}${clause( 'tFar', 'min', 'min' )}}`;
  } )}return {tNear,tFar}` );
  proto.rayIntersectionRangeAndNormal = wrap( [ 'ray' ], `let tNear=Number.NEGATIVE_INFINITY;let tFar=Number.POSITIVE_INFINITY;let tNearNormal=${vectorName}.ZERO;let tFarNormal=${vectorName}.ZERO;${mapJoin( coordinates, c => {
    function clause( tName, mathName, selfName ) {
      const condition = { min: '<', max: '>' }[ mathName ];
      const normalExpr = `${vectorName}.${c.toUpperCase()}_UNIT${selfName === 'min' ? '.negated()' : ''}`;
      return `const new${tName}=(this.${selfName}${c.toUpperCase()}-ray.position.${c})/ray.direction.${c};if(new${tName}${condition}${tName}){${tName}=new${tName};${tName}Normal=${normalExpr};}`;
    }
    return `if(ray.direction.x>0){${clause( 'tNear', 'max', 'min' )}${clause( 'tFar', 'min', 'max' )}}else if(ray.direction.x<0){${clause( 'tNear', 'max', 'max' )}${clause( 'tFar', 'min', 'min' )}}`;
  } )}return {tNear,tFar,tNearNormal,tFarNormal}` );

  proto.NOTHING = new type( ...coordinates.map( () => Number.POSITIVE_INFINITY ), ...coordinates.map( () => Number.NEGATIVE_INFINITY ) );
  proto.EVERYTHING = new type( ...coordinates.map( () => Number.NEGATIVE_INFINITY ), ...coordinates.map( () => Number.POSITIVE_INFINITY ) );
}

export default fixedBounds;
