
import namespace from 'namespace';
import Vector2 from 'math/Vector2';

function v2( x, y ) {
  return Vector2.create( x, y );
}

namespace.v2 = v2;

export default v2;
