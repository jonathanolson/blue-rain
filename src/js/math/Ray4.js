
import assert from 'core/assert';
import fixedRay from 'math/fixedRay';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Vector4 from 'math/Vector4';

class Ray4 {
  constructor( position, direction ) {
    assert( Math.abs( direction.magnitude - 1 ) < 1e-4 , 'the direction must be a unit vector' );

    // @public {Vector4}
    this.position = position;
    this.direction = direction;
  }
}

fixedRay( Ray4, 'Ray4', 4, Vector4 );

namespace.Ray4 = Ray4;

Poolable.mixInto( Ray4, {
  defaultArguments: [ Vector4.ZERO, Vector4.X_UNIT ],
  initialize: Ray4.prototype.setPosDir
} );

export default Ray4;
