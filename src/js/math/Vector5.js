
import assert from 'core/assert';
import coordinateNames from 'math/coordinateNames';
import fixedVector from 'math/fixedVector';
import namespace from 'namespace';

class Vector5 {
  constructor( x, y, z, w, t ) {
    assert( x === undefined || typeof x === 'number' );
    assert( y === undefined || typeof y === 'number' );
    assert( z === undefined || typeof z === 'number' );
    assert( w === undefined || typeof w === 'number' );
    assert( t === undefined || typeof t === 'number' );

    this.x = x || 0;
    this.y = y || 0;
    this.z = z || 0;
    this.w = w || 0;
    this.t = t || 0;
  }
}

fixedVector( Vector5, 'Vector5', coordinateNames.slice( 0, 5 ) );

namespace.Vector5 = Vector5;

export default Vector5;
