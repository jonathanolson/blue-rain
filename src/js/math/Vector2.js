
import assert from 'core/assert';
import clamp from 'math/clamp';
import coordinateNames from 'math/coordinateNames';
import fixedVector from 'math/fixedVector';
import namespace from 'namespace';

class Vector2 {
  constructor( x, y ) {
    assert( x === undefined || typeof x === 'number' );
    assert( y === undefined || typeof y === 'number' );

    this.x = x || 0;
    this.y = y || 0;
  }

  /**
   * The angle $\theta$ of this vector, such that this vector is equal to
   * $$ u = \begin{bmatrix} r\cos\theta \\ r\sin\theta \end{bmatrix} $$
   * for the magnitude $r \ge 0$ of the vector, with $\theta\in(-\pi,\pi]$
   * @public
   *
   * @returns {number}
   */
  getAngle() {
    return Math.atan2( this.y, this.x );
  }
  get angle() { return this.getAngle(); }

  /**
   * The angle between this vector and another vector, in the range $\theta\in[0, \pi]$.
   * @public
   *
   * Equal to $\theta = \cos^{-1}( \hat{u} \cdot \hat{v} )$ where $\hat{u}$ is this vector (normalized) and $\hat{v}$
   * is the input vector (normalized).
   *
   * @param {Vector2} v
   * @returns {number}
   */
  angleBetween( v ) {
    return Math.acos( clamp( ( this.x * v.x + this.y * v.y ) / ( this.magnitude * v.magnitude ), -1, 1 ) );
  }
}

fixedVector( Vector2, 'Vector2', coordinateNames.slice( 0, 2 ) );

namespace.Vector2 = Vector2;

export default Vector2;
