
import assert from 'core/assert';
import namespace from 'namespace';
import numberOfDecimalPlaces from 'math/numberOfDecimalPlaces';
import roundSymmetric from 'math/roundSymmetric';
import toFixedNumber from 'math/toFixedNumber';

/**
 * Rounds a value to a multiple of a specified interval.
 * Examples:
 * roundToInterval( 0.567, 0.01 ) -> 0.57
 * roundToInterval( 0.567, 0.02 ) -> 0.56
 * roundToInterval( 5.67, 0.5 ) -> 5.5
 *
 * @param {number} value
 * @param {number} interval
 * @returns {number}
 */
function roundToInterval( value, interval ) {
  assert( typeof value === 'number' );
  assert( typeof interval === 'number' );

  return toFixedNumber( roundSymmetric( value / interval ) * interval, numberOfDecimalPlaces( interval ) );
}

namespace.roundToInterval = roundToInterval;

export default roundToInterval;
