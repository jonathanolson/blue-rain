
import assert from 'core/assert';
import fixedPlane from 'math/fixedPlane';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray2 from 'math/Ray2';
import Vector2 from 'math/Vector2';

class Plane2 {
  /**
   * @param {Vector2} normal - A normal vector (perpendicular) to the plane
   * @param {number} distance - The signed distance to the plane from the origin, so that normal.times( distance )
   *                            will be a point on the plane.
   */
  constructor( normal, distance ) {
    // @public {Vector2}
    this.normal = normal;

    // @public {number}
    this.distance = distance;

    assert( Math.abs( normal.magnitude - 1 ) < 0.01, 'the normal vector must be a unit vector' );
  }
}

fixedPlane( Plane2, 'Plane2', 2, Vector2, Ray2 );

namespace.Plane2 = Plane2;

Poolable.mixInto( Plane2, {
  initialize: Plane2.prototype.setPlane
} );

export default Plane2;
