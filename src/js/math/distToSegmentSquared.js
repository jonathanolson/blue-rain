
import assert from 'core/assert';
import namespace from 'namespace';
import Vector2 from 'math/Vector2';

/**
 * Squared distance from a point to a line segment squared.
 * See http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment
 *
 * @param {Vector2} point - The point
 * @param {Vector2} a - Starting point of the line segment
 * @param {Vector2} b - Ending point of the line segment
 * @returns {number}
 */
function distToSegmentSquared( point, a, b ) {
  assert( point instanceof Vector2 );
  assert( a instanceof Vector2 );
  assert( b instanceof Vector2 );

  // the square of the distance between a and b,
  const segmentSquaredLength = a.distanceSquared( b );

  // if the segment length is zero, the a and b point are coincident. return the squared distance between a and point
  if ( segmentSquaredLength === 0 ) { return point.distanceSquared( a ); }

  // the t value parametrize the projection of the point onto the a b line
  const t = ( ( point.x - a.x ) * ( b.x - a.x ) + ( point.y - a.y ) * ( b.y - a.y ) ) / segmentSquaredLength;

  let distanceSquared;

  if ( t < 0 ) {
    // if t<0, the projection point is outside the ab line, beyond a
    distanceSquared = point.distanceSquared( a );
  }
  else if ( t > 1 ) {
    // if t>1, the projection past is outside the ab segment, beyond b,
    distanceSquared = point.distanceSquared( b );
  }
  else {
    // if 0<t<1, the projection point lies along the line joining a and b.
    distanceSquared = point.distanceSquaredXY( a.x + t * ( b.x - a.x ), a.y + t * ( b.y - a.y ) );
  }

  return distanceSquared;
}

namespace.distToSegmentSquared = distToSegmentSquared;

export default distToSegmentSquared;
