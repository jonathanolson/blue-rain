
import namespace from 'namespace';
import Vector4 from 'math/Vector4';

function v4( x, y, z, w ) {
  return Vector4.create( x, y, z, w );
}

namespace.v4 = v4;

export default v4;
