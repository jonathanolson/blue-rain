
import assert from 'core/assert';
import namespace from 'namespace';
import Ray3 from 'math/Ray3';
import v2 from 'math/v2';
import v3 from 'math/v3';
import Vector2 from 'math/Vector2';
import Vector3 from 'math/Vector3';

const Orthographic3 = {
  /**
   * Returns a 3d ray pointing to all points that would project to the 2d given point.
   * @public
   *
   * @param {Vector2} point2
   * @returns {Ray3}
   */
  projectedPointToRay( point2 ) {
    assert( point2 instanceof Vector2 );

    return Ray3.create( v3( point2.x, point2.y, 0 ), Vector3.Z_UNIT );
  },

  /**
   * Returns the projection of a 3d point.
   * @public
   *
   * @param {Vector3} point3
   * @returns {Vector2}
   */
  pointToProjection( point3 ) {
    assert( point3 instanceof Vector3 );

    return v2( point3.x, point3.y );
  },

  /**
   * Returns the minimum radius of a circle in the projected hyperplane that will fully contain the projected image of
   * a sphere in the full space with the given radius and distance from the camera.
   * @public
   *
   * @param {number} radius
   * @param {number} distance
   * @returns {number}
   */
  minimumRadiusToContainSphere( radius, distance ) {
    assert( typeof radius === 'number' );
    assert( typeof distance === 'number' );
    assert( radius >= 0 );
    assert( distance >= 0 );

    return radius;
  }
};

namespace.Orthographic3 = Orthographic3;

export default Orthographic3;
