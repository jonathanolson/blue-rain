
import assert from 'core/assert';
import namespace from 'namespace';
import solveLinearRootsReal from 'math/solveLinearRootsReal';

/**
 * Returns an array of the real roots of the quadratic equation $ax^2 + bx + c=0$, or null if every value is a
 * solution. If a is nonzero, there should be between 0 and 2 (inclusive) values returned.
 *
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @returns {Array.<number>|null} - The real roots of the equation, or null if all values are roots. If the root has
 *                                  a multiplicity larger than 1, it will be repeated that many times.
 */
function solveQuadraticRootsReal( a, b, c, epsilon = 1e-7 ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );
  assert( typeof c === 'number' );
  assert( typeof epsilon === 'number' );

  // Check for a degenerate case where we don't have a quadratic, or if the order of magnitude is such where the
  // linear solution would be expected
  if ( a === 0 || Math.abs( b / a ) > 1 / epsilon || Math.abs( c / a ) > 1 / epsilon ) {
    return solveLinearRootsReal( b, c );
  }

  var discriminant = b * b - 4 * a * c;
  if ( discriminant < 0 ) {
    return [];
  }
  var sqrt = Math.sqrt( discriminant );
  // TODO: how to handle if discriminant is 0? give unique root or double it?
  // TODO: probably just use Complex for the future
  return [
    ( -b - sqrt ) / ( 2 * a ),
    ( -b + sqrt ) / ( 2 * a )
  ];
}

namespace.solveQuadraticRootsReal = solveQuadraticRootsReal;

export default solveQuadraticRootsReal;
