
import assert from 'core/assert';
import namespace from 'namespace';
import Vector2 from 'math/Vector2';

/**
 * The area inside the triangle defined by the three vertices, but with the sign determined by whether the vertices
 * provided are clockwise or counter-clockwise.
 *
 * If the vertices are counterclockwise (in a right-handed coordinate system), then the signed area will be
 * positive.
 *
 * @param {Vector2} a
 * @param {Vector2} b
 * @param {Vector2} c
 * @returns {number}
 */
function triangleAreaSigned( a, b, c ) {
  assert( a instanceof Vector2 );
  assert( b instanceof Vector2 );
  assert( c instanceof Vector2 );

  return a.x * ( b.y - c.y ) + b.x * ( c.y - a.y ) + c.x * ( a.y - b.y );
}

namespace.triangleAreaSigned = triangleAreaSigned;

export default triangleAreaSigned;
