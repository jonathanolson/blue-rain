
import assert from 'core/assert';
import namespace from 'namespace';
import Vector2 from 'math/Vector2';

/**
 * Intersection point between the lines defined by the line segments p1-2 and p3-p4. If the
 * lines are not properly defined, null is returned. If there are no intersections or infinitely many,
 * e.g. parallel lines, null is returned.
 *
 * @param {Vector2} p1
 * @param {Vector2} p2
 * @param {Vector2} p3
 * @param {Vector2} p4
 * @param {number} [epsilon]
 * @returns {Vector2|null}
 */
function lineLineIntersection( p1, p2, p3, p4, epsilon = 1e-10 ) {
  assert( p1 instanceof Vector2 );
  assert( p2 instanceof Vector2 );
  assert( p3 instanceof Vector2 );
  assert( p4 instanceof Vector2 );
  assert( typeof epsilon === 'number' );

  // If the endpoints are the same, they don't properly define a line
  if ( p1.equals( p2 ) || p3.equals( p4 ) ) {
    return null;
  }

  // Taken from an answer in
  // http://stackoverflow.com/questions/385305/efficient-maths-algorithm-to-calculate-intersections
  const x12 = p1.x - p2.x;
  const x34 = p3.x - p4.x;
  const y12 = p1.y - p2.y;
  const y34 = p3.y - p4.y;

  const denom = x12 * y34 - y12 * x34;

  // If the denominator is 0, lines are parallel or coincident
  if ( Math.abs( denom ) < epsilon ) {
    return null;
  }

  // define intersection using determinants, see https://en.wikipedia.org/wiki/Line%E2%80%93line_intersection
  const a = p1.x * p2.y - p1.y * p2.x;
  const b = p3.x * p4.y - p3.y * p4.x;

  return new Vector2(
    ( a * x34 - x12 * b ) / denom,
    ( a * y34 - y12 * b ) / denom
  );
}

namespace.lineLineIntersection = lineLineIntersection;

export default lineLineIntersection;
