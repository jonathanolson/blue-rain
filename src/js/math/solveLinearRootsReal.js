
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Returns an array of the real roots of the quadratic equation $ax + b=0$, or null if every value is a solution.
 *
 * @param {number} a
 * @param {number} b
 * @returns {Array.<number>|null} - The real roots of the equation, or null if all values are roots. If the root has
 *                                  a multiplicity larger than 1, it will be repeated that many times.
 */
function solveLinearRootsReal( a, b ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );

  if ( a === 0 ) {
    if ( b === 0 ) {
      return null;
    }
    else {
      return [];
    }
  }
  else {
    return [ -b / a ];
  }
}

namespace.solveLinearRootsReal = solveLinearRootsReal;

export default solveLinearRootsReal;
