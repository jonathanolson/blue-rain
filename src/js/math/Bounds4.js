
import assert from 'core/assert';
import fixedBounds from 'math/fixedBounds';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray4 from 'math/Ray4';
import Vector4 from 'math/Vector4';

class Bounds4 {
  /**
   * @param {number} minX
   * @param {number} minY
   * @param {number} minZ
   * @param {number} minW
   * @param {number} maxX
   * @param {number} maxY
   * @param {number} maxZ
   * @param {number} maxW
   */
  constructor( minX, minY, minZ, minW, maxX, maxY, maxZ, maxW ) {
    assert( typeof minX === 'number' );
    assert( typeof minY === 'number' );
    assert( typeof minZ === 'number' );
    assert( typeof minW === 'number' );
    assert( typeof maxX === 'number' );
    assert( typeof maxY === 'number' );
    assert( typeof maxZ === 'number' );
    assert( typeof maxW === 'number' );

    // @public {number}
    this.minX = minX;
    this.minY = minY;
    this.minZ = minZ;
    this.minW = minW;
    this.maxX = maxX;
    this.maxY = maxY;
    this.maxZ = maxZ;
    this.maxW = maxW;
  }
}

fixedBounds( Bounds4, 'Bounds4', 4, Vector4, Ray4 );

namespace.Bounds4 = Bounds4;

Poolable.mixInto( Bounds4, {
  initialize: Bounds4.prototype.setMinMax,
  defaultArguments: [ 0, 0, 0, 0, 0, 0, 0, 0 ]
} );

export default Bounds4;
