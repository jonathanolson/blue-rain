
import assert from 'core/assert';
import fixedBounds from 'math/fixedBounds';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray2 from 'math/Ray2';
import Vector2 from 'math/Vector2';

class Bounds2 {
  /**
   * @param {number} minX
   * @param {number} minY
   * @param {number} maxX
   * @param {number} maxY
   */
  constructor( minX, minY, maxX, maxY ) {
    assert( typeof minX === 'number' );
    assert( typeof minY === 'number' );
    assert( typeof maxX === 'number' );
    assert( typeof maxY === 'number' );

    // @public {number}
    this.minX = minX;
    this.minY = minY;
    this.maxX = maxX;
    this.maxY = maxY;
  }
}

fixedBounds( Bounds2, 'Bounds2', 2, Vector2, Ray2 );

namespace.Bounds2 = Bounds2;

Poolable.mixInto( Bounds2, {
  initialize: Bounds2.prototype.setMinMax,
  defaultArguments: [ 0, 0, 0, 0 ]
} );

export default Bounds2;
