
import assert from 'core/assert';
import mod from 'math/mod';
import namespace from 'namespace';

/**
 * Greatest Common Divisor, using https://en.wikipedia.org/wiki/Euclidean_algorithm. See
 * https://en.wikipedia.org/wiki/Greatest_common_divisor
 *
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
function gcd( a, b ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );

  return Math.abs( b === 0 ? a : gcd( b, mod( a, b ) ) );
}

namespace.gcd = gcd;

export default gcd;
