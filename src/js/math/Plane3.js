
import assert from 'core/assert';
import fixedPlane from 'math/fixedPlane';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray3 from 'math/Ray3';
import Vector3 from 'math/Vector3';

class Plane3 {
  /**
   * @param {Vector3} normal - A normal vector (perpendicular) to the plane
   * @param {number} distance - The signed distance to the plane from the origin, so that normal.times( distance )
   *                            will be a point on the plane.
   */
  constructor( normal, distance ) {
    // @public {Vector3}
    this.normal = normal;

    // @public {number}
    this.distance = distance;

    assert( Math.abs( normal.magnitude - 1 ) < 0.01, 'the normal vector must be a unit vector' );
  }

  /**
   * Returns a new plane that passes through three points $(\vec{a},\vec{b},\vec{c})$
   * The normal of the plane points along $\vec{c-a} \times \vec{b-a}$
   * Passing three collinear points will return null
   * @public
   *
   * @param {Vector3} a - first point
   * @param {Vector3} b - second point
   * @param {Vector3} c - third point
   * @returns {Plane3|null}
   */
  static fromTriangle( a, b, c ) {
    // TODO: reduce allocations
    const normal = ( c.minus( a ) ).cross( b.minus( a ) );
    if ( normal.magnitude === 0 ) {
      return null;
    }
    normal.normalize();

    return Plane3.create( normal, normal.dot( a ) );
  }
}

fixedPlane( Plane3, 'Plane3', 3, Vector3, Ray3 );

namespace.Plane3 = Plane3;

Poolable.mixInto( Plane3, {
  initialize: Plane3.prototype.setPlane
} );

export default Plane3;
