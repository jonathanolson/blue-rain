
// NOTE: incomplete!!! Do not use yet

import assert from 'core/assert';
import namespace from 'namespace';

const pcgStreamHigh = 0xda3e39cb;
const pcgStreamLow = 0x94b95bdb;
const pcgMultHigh = 0x5851f42d;
const pcgMultLow = 0x4c957f2d;

function add( arr32, aHigh, aLow ) {

  let low = aLow + arr32[ 1 ];
  let high = aHigh + arr32[ 0 ];
  assert( low >= 0 );
  assert( high >= 0 );
  assert( low < 0xF00000000 );
  assert( high < 0xF00000000 );

  // TODO: factor out carry
  if ( low > 0xFFFFFFFF ) {
    high += ( ( low / 0x100000000 ) >> 0 );
    low &= 0xFFFFFFFF;
  }
  high &= 0xFFFFFFFF;
  if ( low < 0 ) {
    low += 0x100000000;
  }
  if ( high < 0 ) {
    high += 0x100000000;
  }

  arr32[ 0 ] = high;
  arr32[ 1 ] = low;
}

function multiply( arr32, aHigh, aLow ) {

  // aHigh top, aHigh bot, aLow top, aLow bot
  //
  // bHigh top [_] [_] [_] [ ]
  // bHigh bot [_] [_] [ ] [x]
  // bLow top  [_] [ ] [x] [x]
  // bLow bot  [ ] [x] [x] [x]

  const a0 = aLow & 0xFFFF;
  const b0 = arr32[ 1 ] & 0xFFFF;
  const a1 = ( aLow / 0x10000 ) >> 0;
  const b1 = ( arr32[ 1 ] / 0x10000 ) >> 0;
  const a2 = aHigh & 0xFFFF;
  const b2 = arr32[ 0 ] & 0xFFFF;
  const a3 = ( aHigh / 0x10000 ) >> 0;
  const b3 = ( arr32[ 0 ] / 0x10000 ) >> 0;

  const a = a0 * b0;
  const b = a1 * b0 + a0 * b1;
  const c = a2 * b0 + a0 * b2 + a1 * b1;
  const d = a3 * b0 + a0 * b3 + a2 * b1 + a1 * b2;

  let low = a + ( ( b & 0xFFFF ) * 0x10000 );
  let high = ( ( b / 0x10000 ) >> 0 ) + c + ( ( d & 0xFFFF ) * 0x10000 );
  assert( low >= 0 );
  assert( high >= 0 );
  assert( low < 0xF00000000 );
  assert( high < 0xF00000000 );

  if ( low > 0xFFFFFFFF ) {
    high += ( ( low / 0x100000000 ) >> 0 );
    low &= 0xFFFFFFFF;
  }
  high &= 0xFFFFFFFF;
  if ( low < 0 ) {
    low += 0x100000000;
  }
  if ( high < 0 ) {
    high += 0x100000000;
  }

  arr32[ 0 ] = high;
  arr32[ 1 ] = low;
}

// function xor( arr32, aHigh, aLow ) {
//   arr32[ 0 ] = arr32[ 0 ] ^ aHigh;
//   arr32[ 1 ] = arr32[ 1 ] ^ aLow;
// }

// function rightShift( arr32, n ) {
//   const shift = 2 ** n;
//   const antishift = 2 ** ( 32 - n );
//   // TODO: how to handle the "large" shifts
//   const low = ( ( arr32[ 1 ] / shift ) >> 0 ) + ( arr32[ 0 ] & shift ) * antishift;
//   const high = ( ( arr32[ 0 ] / shift ) >> 0 );
//   arr32[ 0 ] = high;
//   arr32[ 1 ] = low;
// }

class PCG32 {
  constructor( highState = 0x853c49e6, lowState = 0x748fea9b ) {
    // @private {Uint32Array}
    this.state = new Uint32Array( 2 );

    this.generate();
    add( this.state, highState, lowState );
    this.generate();
  }

  generate() {
    // const oldHigh = this.state[ 0 ];
    // const oldLow = this.state[ 1 ];

    multiply( this.state, pcgMultHigh, pcgMultLow );
    add( this.state, pcgStreamHigh, pcgStreamLow );

    // const xor1Low = ( ( oldLow / 0x40000 ) >> 0 ) + ( oldHigh & 0x40000 ) * 0x4000;
    // const xor1High = ( ( oldHigh / 0x40000 ) >> 0 );

    // const xor2Low = xor1Low ^ oldLow;
    // const xor2High = xor1High ^ oldHigh;

    // const xor3Low = ( ( xor2Low / 0x8000000 ) >> 0 ) + ( xor2High & 0x8000000 ) * 0x20;
    // const xor3High = ( ( xor2High / 0x8000000 ) >> 0 );

    // const rot = ( oldHigh / 0x8000000 ) >> 0;



    //uint64_t oldstate = rng->state;
    // rng->state = oldstate * 6364136223846793005 + rng->inc;
    // uint32_t xorshifted = ((oldstate >> 18u) ^ oldstate) >> 27u;
    // uint32_t rot = oldstate >> 59u;
    // return (xorshifted >> rot) | (xorshifted << ((-rot) & 31));
  }

  toString() {
    return this.state[ 0 ].toString( 16 ) + this.state[ 1 ].toString( 16 );
  }
}

/*
// pcg32_boundedrand(bound):
// pcg32_boundedrand_r(rng, bound):
//     Generate a uniformly distributed number, r, where 0 <= r < bound

uint32_t pcg32_boundedrand_r(pcg32_random_t* rng, uint32_t bound)
{
    // To avoid bias, we need to make the range of the RNG a multiple of
    // bound, which we do by dropping output less than a threshold.
    // A naive scheme to calculate the threshold would be to do
    //
    //     uint32_t threshold = 0x100000000ull % bound;
    //
    // but 64-bit div/mod is slower than 32-bit div/mod (especially on
    // 32-bit platforms).  In essence, we do
    //
    //     uint32_t threshold = (0x100000000ull-bound) % bound;
    //
    // because this version will calculate the same modulus, but the LHS
    // value is less than 2^32.

    uint32_t threshold = -bound % bound;

    // Uniformity guarantees that this loop will terminate.  In practice, it
    // should usually terminate quickly; on average (assuming all bounds are
    // equally likely), 82.25% of the time, we can expect it to require just
    // one iteration.  In the worst case, someone passes a bound of 2^31 + 1
    // (i.e., 2147483649), which invalidates almost 50% of the range.  In
    // practice, bounds are typically small and only a tiny amount of the range
    // is eliminated.
    for (;;) {
        uint32_t r = pcg32_random_r(rng);
        if (r >= threshold)
            return r % bound;
    }
}
*/

namespace.PCG32 = PCG32;

export default PCG32;
