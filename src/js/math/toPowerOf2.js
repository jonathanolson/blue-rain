
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Finds the smallest power of 2 that is at least as large as n.
 *
 * @param {number} n
 * @returns {number} The smallest power of 2 that is greater than or equal n
 */
function toPowerOf2( n ) {
  assert( typeof n === 'number' && n > 0 );

  var result = 1;
  while ( result < n ) {
    result *= 2;
  }
  return result;
}

namespace.toPowerOf2 = toPowerOf2;

export default toPowerOf2;
