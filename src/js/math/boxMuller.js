
import assert from 'core/assert';
import namespace from 'namespace';
import v2 from 'math/v2';

/**
 * @param {number} r1 - First random number
 * @param {number} r2 - Second random number
 * @returns {Vector2}
 */
function boxMuller( r1, r2 ) {
  assert( typeof r1 === 'number' && r1 >= 0 && r1 < 1 );
  assert( typeof r2 === 'number' && r2 >= 0 && r2 < 1 );

  const angle = 2 * Math.PI * r2;
  const multiplier = Math.sqrt( -2 * Math.log( r1 ) );
  return v2( multiplier * Math.cos( angle ), multiplier * Math.sin( angle ) );
}

namespace.boxMuller = boxMuller;

export default boxMuller;
