
import assert from 'core/assert';
import cubeRoot from 'math/cubeRoot';
import namespace from 'namespace';
import solveQuadraticRootsReal from 'math/solveQuadraticRootsReal';

/**
 * Returns an array of the real roots of the quadratic equation $ax^3 + bx^2 + cx + d=0$, or null if every value is a
 * solution. If a is nonzero, there should be between 0 and 3 (inclusive) values returned.
 *
 * @param {number} a
 * @param {number} b
 * @param {number} c
 * @param {number} d
 * @returns {Array.<number>|null} - The real roots of the equation, or null if all values are roots. If the root has
 *                                  a multiplicity larger than 1, it will be repeated that many times.
 */
function solveCubicRootsReal( a, b, c, d, epsilon = 1e-7 ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );
  assert( typeof c === 'number' );
  assert( typeof d === 'number' );
  assert( typeof epsilon === 'number' );
  // TODO: a Complex type!

  // Check for a degenerate case where we don't have a cubic
  if ( a === 0 ) {
    return solveQuadraticRootsReal( b, c, d );
  }

  //We need to test whether a is several orders of magnitude less than b, c, d
  if ( a === 0 || Math.abs( b / a ) > 1 / epsilon || Math.abs( c / a ) > 1 / epsilon || Math.abs( d / a ) > 1 / epsilon ) {
    return solveQuadraticRootsReal( b, c, d );
  }
  if ( d === 0 || Math.abs( a / d ) > 1 / epsilon || Math.abs( b / d ) > 1 / epsilon || Math.abs( c / d ) > 1 / epsilon ) {
    return [ 0 ].concat( solveQuadraticRootsReal( a, b, c ) );
  }

  b /= a;
  c /= a;
  d /= a;

  var q = ( 3.0 * c - ( b * b ) ) / 9;
  var r = ( -( 27 * d ) + b * ( 9 * c - 2 * ( b * b ) ) ) / 54;
  var discriminant = q * q * q + r * r;
  var b3 = b / 3;

  if ( discriminant > epsilon ) {
    // a single real root
    var dsqrt = Math.sqrt( discriminant );
    return [ cubeRoot( r + dsqrt ) + cubeRoot( r - dsqrt ) - b3 ];
  }

  // three real roots
  if ( discriminant === 0 ) {
    // contains a double root
    var rsqrt = cubeRoot( r );
    var doubleRoot = -b3 - rsqrt;
    return [ -b3 + 2 * rsqrt, doubleRoot, doubleRoot ];
  }
  else {
    // all unique
    var qX = -q * q * q;
    qX = Math.acos( r / Math.sqrt( qX ) );
    var rr = 2 * Math.sqrt( -q );
    return [
      -b3 + rr * Math.cos( qX / 3 ),
      -b3 + rr * Math.cos( ( qX + 2 * Math.PI ) / 3 ),
      -b3 + rr * Math.cos( ( qX + 4 * Math.PI ) / 3 )
    ];
  }
}

namespace.solveCubicRootsReal = solveCubicRootsReal;

export default solveCubicRootsReal;
