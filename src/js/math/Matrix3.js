
import fixedMatrix from 'math/fixedMatrix';
import namespace from 'namespace';
import Vector2 from 'math/Vector2';
import Vector3 from 'math/Vector3';

class Matrix3 {
  constructor() {
    // @private {Array.<number>} - TODO: Performance test with Float64Array?
    this.d = [ 1, 0, 0, 0, 1, 0, 0, 0, 1 ];
  }

  /**
   * Sets the transform of a Canvas 2D rendering context to the affine part of this matrix
   * @public
   *
   * @param {CanvasRenderingContext2D} context
   */
  canvasSetTransform( context ) {
    context.setTransform( this.m00, this.m10, this.m01, this.m11, this.m02, this.m12 );
  }

  /**
   * Appends the affine part of this matrix to the Canvas 2D rendering context
   * @public
   *
   * @param {CanvasRenderingContext2D} context
   */
  canvasAppendTransform( context ) {
    context.transform( this.m00, this.m10, this.m01, this.m11, this.m02, this.m12 );
  }

  /**
   * Sets to a rotation around an axis by a certain angle.
   * @public
   *
   * @param {Vector3} axis
   * @param {number} angle
   */
  setToRotationAxisAngle( axis, angle ) {
    var c = Math.cos( angle );
    var s = Math.sin( angle );
    var C = 1 - c;

    return this.rowMajor(
      axis.x * axis.x * C + c, axis.x * axis.y * C - axis.z * s, axis.x * axis.z * C + axis.y * s,
      axis.y * axis.x * C + axis.z * s, axis.y * axis.y * C + c, axis.y * axis.z * C - axis.x * s,
      axis.z * axis.x * C - axis.y * s, axis.z * axis.y * C + axis.x * s, axis.z * axis.z * C + c );
  }

  /**
   * Returns a rotation matrix that rotates A to B (Vector3 instances), by rotating about the axis A.cross( B ) --
   * Shortest path. ideally should be unit vectors.
   * @public
   *
   * @param {Vector3} a
   * @param {Vector3} b
   */
  setRotationAToB( a, b ) {
    // see http://graphics.cs.brown.edu/~jfh/papers/Moller-EBA-1999/paper.pdf for information on this implementation
    let start = a;
    let end = b;

    let epsilon = 0.0001;

    let e;
    let h;
    let f;

    let v = start.cross( end );
    e = start.dot( end );
    f = ( e < 0 ) ? -e : e;

    // if "from" and "to" vectors are nearly parallel
    if ( f > 1 - epsilon ) {
      let c1;
      let c2;
      let c3;

      let x = new Vector3(
        ( start.x > 0 ) ? start.x : -start.x,
        ( start.y > 0 ) ? start.y : -start.y,
        ( start.z > 0 ) ? start.z : -start.z
      );

      if ( x.x < x.y ) {
        if ( x.x < x.z ) {
          x = Vector3.X_UNIT;
        }
        else {
          x = Vector3.Z_UNIT;
        }
      }
      else {
        if ( x.y < x.z ) {
          x = Vector3.Y_UNIT;
        }
        else {
          x = Vector3.Z_UNIT;
        }
      }

      let u = x.minus( start );
      v = x.minus( end );

      c1 = 2 / u.dot( u );
      c2 = 2 / v.dot( v );
      c3 = c1 * c2 * u.dot( v );

      return this.rowMajor(
        -c1 * u.x * u.x - c2 * v.x * v.x + c3 * v.x * u.x + 1,
        -c1 * u.x * u.y - c2 * v.x * v.y + c3 * v.x * u.y,
        -c1 * u.x * u.z - c2 * v.x * v.z + c3 * v.x * u.z,
        -c1 * u.y * u.x - c2 * v.y * v.x + c3 * v.y * u.x,
        -c1 * u.y * u.y - c2 * v.y * v.y + c3 * v.y * u.y + 1,
        -c1 * u.y * u.z - c2 * v.y * v.z + c3 * v.y * u.z,
        -c1 * u.z * u.x - c2 * v.z * v.x + c3 * v.z * u.x,
        -c1 * u.z * u.y - c2 * v.z * v.y + c3 * v.z * u.y,
        -c1 * u.z * u.z - c2 * v.z * v.z + c3 * v.z * u.z + 1
      );
    }
    else {
      // the most common case, unless "start"="end", or "start"=-"end"
      let hvx;
      let hvz;
      let hvxy;
      let hvxz;
      let hvyz;
      h = 1 / ( 1 + e );
      hvx = h * v.x;
      hvz = h * v.z;
      hvxy = hvx * v.y;
      hvxz = hvx * v.z;
      hvyz = hvz * v.y;

      return this.rowMajor(
        e + hvx * v.x, hvxy - v.z, hvxz + v.y,
        hvxy + v.z, e + h * v.y * v.y, hvyz - v.x,
        hvxz - v.y, hvyz + v.x, e + hvz * v.z
      );
    }
  }

  /**
   * Returns a 3x3 2d rotation matrix.
   * @public
   *
   * @param {number} theta - The angle, in radians
   */
  static rotation2( theta ) {
    return Matrix3.dirty().setToRotationXY( theta );
  }

  static rotationAround( angle, x, y ) {
    return Matrix3.translation( x, y ).times( Matrix3.rotation2( angle ) ).times( Matrix3.translation( -x, -y ) );
  }
}

namespace.Matrix3 = Matrix3;

fixedMatrix( Matrix3, 'Matrix3', 3, Vector3, Vector2 );

export default Matrix3;
