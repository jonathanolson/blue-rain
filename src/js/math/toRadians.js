
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Converts degrees to radians.
 *
 * @param {number} degrees
 * @returns {number}
 */
function toRadians( degrees ) {
  assert( typeof degrees === 'number' );

  return Math.PI * degrees / 180;
}

namespace.toRadians = toRadians;

export default toRadians;
