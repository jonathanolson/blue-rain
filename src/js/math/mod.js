
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Workaround for broken modulo operator.
 * E.g. on iOS9, 1e10 % 1e10 -> 2.65249474e-315
 * See https://github.com/phetsims/dot/issues/75
 *
 * @param {number} a
 * @param {number} b
 * @returns {number}
 */
function mod( a, b ) {
  assert( typeof a === 'number' );
  assert( typeof b === 'number' );

  if ( a / b % 1 === 0 ) {
    return 0; // a is a multiple of b
  }
  else {
    return a % b;
  }
}

namespace.mod = mod;

export default mod;
