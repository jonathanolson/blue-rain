
import assert from 'core/assert';
import namespace from 'namespace';
import Ray3 from 'math/Ray3';
import tangentPointOnCircle from 'math/tangentPointOnCircle';
import v2 from 'math/v2';
import v3 from 'math/v3';
import Vector2 from 'math/Vector2';
import Vector3 from 'math/Vector3';

/*

TODO: projection unit tests, e.g.:

yrio.Stereographic3.pointToProjection( yrio.Stereographic3.projectedPointToRay( yrio.v2( 20, -7 ) ).direction )
Vector2 {x: 20.000000000000025, y: -7.000000000000009}
yrio.Perspective3.pointToProjection( yrio.Perspective3.projectedPointToRay( yrio.v2( 20, -7 ) ).direction )
Vector2 {x: 20, y: -7}
yrio.Orthographic3.pointToProjection( yrio.Orthographic3.projectedPointToRay( yrio.v2( 20, -7 ) ).position )
Vector2 {x: 20, y: -7}
*/

const Perspective3 = {
  /**
   * Returns a 3d ray pointing to all points that would project to the 2d given point.
   * @public
   *
   * @param {Vector2} point2
   * @returns {Ray3}
   */
  projectedPointToRay( point2 ) {
    assert( point2 instanceof Vector2 );

    return Ray3.create( Vector3.ZERO, v3( point2.x, point2.y, 1 ).normalize() );
  },

  /**
   * Returns the projection of a 3d point.
   * @public
   *
   * @param {Vector3} point3
   * @returns {Vector2}
   */
  pointToProjection( point3 ) {
    assert( point3 instanceof Vector3 );

    return v2(
      point3.x / point3.z,
      point3.y / point3.z
    );
  },

  /**
   * Returns the minimum radius of a circle in the projected hyperplane that will fully contain the projected image of
   * a sphere in the full space with the given radius and distance from the camera.
   * @public
   *
   * @param {number} radius
   * @param {number} distance
   * @returns {number}
   */
  minimumRadiusToContainSphere( radius, distance ) {
    assert( typeof radius === 'number' );
    assert( typeof distance === 'number' );
    assert( radius >= 0 );
    assert( distance >= 0 );

    const point = tangentPointOnCircle( radius, distance );
    const value = point.subtract( v2( distance, 0 ) ).normalize().y;
    return value / Math.sqrt( 1 - value * value );
  }
};

namespace.Perspective3 = Perspective3;

export default Perspective3;
