
import assert from 'core/assert';
import createFunctionWrap from 'core/createFunctionWrap';

/**
 * Sets up general methods for fixed-length ray types (e.g. Ray2).
 *
 * @param {function} type - The constructor/class, e.g. Ray2 itself
 * @param {string} name
 * @param {number} size
 * @param {function} vectorType
 */
function fixedRay( type, name, size, vectorType ) {
  assert( typeof type === 'function' );
  assert( typeof name === 'string' );

  // Grabs the actual prototype
  const proto = type.prototype;

  const vectorName = `Vector${size}`;

  const getAssertions = params => {
    let assertions = '';
    assert( ( params.forEach( param => {
      if ( param === 'pos' ) {
        assertions += `assert( ${param} instanceof ${vectorName}, '${param} should be a vector' );`;
      }
      else if ( param === 'dir' || param === 'normal' ) {
        assertions += `assert( ${param} instanceof ${vectorName} && Math.abs( ${param}.magnitude - 1 ) < 1e-4, '${param} should be a unit vector' );`;
      }
      else if ( param === 'dist' ) {
        assertions += `assert( typeof ${param} === 'number', '${param} should be a number' );`;
      }
      else if ( param === 'ray' ) {
        assertions += `assert( ${param} instanceof ${name}, '${param} should be a ${name}' );`;
      }
    } ), true ) );
    return assertions;
  };

  const wrap = createFunctionWrap( {
    [ name ]: type,
    assert: assert,
    [ vectorName ]: vectorType
  }, getAssertions );

  proto.setPosDir = wrap( [ 'pos', 'dir' ], 'this.position=pos;this.direction=dir;return this;' );
  proto.set = wrap( [ 'ray' ], 'this.position=ray.position;this.direction=ray.direction;return this;' );
  proto.copy = wrap( [], `return ${name}.dirty().set(this);` );

  proto.shift = wrap( [ 'dist' ], 'this.position=this.pointAtDistance(dist);return this;' );
  proto.shifted = wrap( [ 'dist' ], 'return this.copy().shift(dist);' );

  proto.pointAtDistance = wrap( [ 'dist' ], 'return this.position.plus(this.direction.timesScalar(dist));' );
  proto.distanceToHyperplane = wrap( [ 'dist', 'normal' ], 'return (dist-this.position.dot(normal))/this.direction.dot(normal);' );
  proto.toString = wrap( [], 'return this.position.toString()+\'=>\'+this.direction.toString();' );

  // I - 2.0 * dot(N, I) * N.
  proto.reflect = wrap( [ 'normal' ], `this.direction=${vectorName}.dirty().set(normal).multiplyScalar(-2*this.direction.dot(normal)).add(this.direction);return this;` );
  proto.reflected = wrap( [ 'normal' ], 'return this.copy().reflect(normal);' );
}

export default fixedRay;
