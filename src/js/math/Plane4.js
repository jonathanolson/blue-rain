
import assert from 'core/assert';
import fixedPlane from 'math/fixedPlane';
import namespace from 'namespace';
import Poolable from 'core/Poolable';
import Ray4 from 'math/Ray4';
import Vector4 from 'math/Vector4';

class Plane4 {
  /**
   * @param {Vector4} normal - A normal vector (perpendicular) to the plane
   * @param {number} distance - The signed distance to the plane from the origin, so that normal.times( distance )
   *                            will be a point on the plane.
   */
  constructor( normal, distance ) {
    // @public {Vector4}
    this.normal = normal;

    // @public {number}
    this.distance = distance;

    assert( Math.abs( normal.magnitude - 1 ) < 0.01, 'the normal vector must be a unit vector' );
  }
}

fixedPlane( Plane4, 'Plane4', 4, Vector4, Ray4 );

namespace.Plane4 = Plane4;

Poolable.mixInto( Plane4, {
  initialize: Plane4.prototype.setPlane
} );

export default Plane4;
