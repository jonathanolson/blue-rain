
import assert from 'core/assert';
import namespace from 'namespace';
import Ray4 from 'math/Ray4';
import tangentPointOnCircle from 'math/tangentPointOnCircle';
import v2 from 'math/v2';
import v3 from 'math/v3';
import v4 from 'math/v4';
import Vector3 from 'math/Vector3';
import Vector4 from 'math/Vector4';

const Stereographic4 = {
  /**
   * Returns a 4d ray pointing to all points that would project to the 3d given point.
   * @public
   *
   * @param {Vector3} point3
   * @returns {Ray4}
   */
  projectedPointToRay( point3 ) {
    assert( point3 instanceof Vector3 );

    return Ray4.create( Vector4.ZERO, v4( 4 * point3.x, 4 * point3.y, 4 * point3.z, 4 - point3.magnitudeSquared ).normalize() );
  },

  /**
   * Returns the projection of a 4d point.
   * @public
   *
   * @param {Vector4} point4
   * @returns {Vector3}
   */
  pointToProjection( point4 ) {
    // TODO: could inline operations so we don't do as much work here
    assert( point4 instanceof Vector4 );

    const origin = v4( 0, 0, 0, -2 );
    const direction = v4( point4.x, point4.y, point4.z, point4.w + 1 ).normalize();

    const ray = Ray4.create( origin, direction );

    const t = ray.distanceToHyperplane( 0, Vector4.W_UNIT );
    const p = ray.pointAtDistance( t );

    origin.freeToPool();
    direction.freeToPool();
    ray.freeToPool();

    return v3( p.x, p.y, p.z );
  },

  /**
   * Returns the minimum radius of a circle in the projected hyperplane that will fully contain the projected image of
   * a 3-sphere in the full space with the given radius and distance from the camera.
   * @public
   *
   * @param {number} radius
   * @param {number} distance
   * @returns {number}
   */
  minimumRadiusToContainSphere( radius, distance ) {
    assert( typeof radius === 'number' );
    assert( typeof distance === 'number' );
    assert( radius >= 0 );
    assert( distance >= 0 );

    const point = tangentPointOnCircle( radius, distance );
    const value = point.subtract( v2( distance, 0 ) ).normalize().y;
    return ( 2 - 2 * Math.sqrt( 1 - value * value ) ) / value;
  }
};

namespace.Stereographic4 = Stereographic4;

export default Stereographic4;
