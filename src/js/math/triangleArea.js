
import namespace from 'namespace';
import triangleAreaSigned from 'math/triangleAreaSigned';

/**
 * The area inside the triangle defined by the three vertices.
 *
 * @param {Vector2} a
 * @param {Vector2} b
 * @param {Vector2} c
 * @returns {number}
 */
function triangleArea( a, b, c ) {
  return Math.abs( triangleAreaSigned( a, b, c ) );
}

namespace.triangleArea = triangleArea;

export default triangleArea;
