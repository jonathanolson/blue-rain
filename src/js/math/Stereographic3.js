
import assert from 'core/assert';
import namespace from 'namespace';
import Ray3 from 'math/Ray3';
import tangentPointOnCircle from 'math/tangentPointOnCircle';
import v2 from 'math/v2';
import v3 from 'math/v3';
import Vector2 from 'math/Vector2';
import Vector3 from 'math/Vector3';

const Stereographic3 = {
  /**
   * Returns a 3d ray pointing to all points that would project to the 2d given point.
   * @public
   *
   * @param {Vector2} point2
   * @returns {Ray3}
   */
  projectedPointToRay( point2 ) {
    assert( point2 instanceof Vector2 );

    return Ray3.create( Vector3.ZERO, v3( 4 * point2.x, 4 * point2.y, 4 - point2.magnitudeSquared ).normalize() );
  },

  /**
   * Returns the projection of a 3d point.
   * @public
   *
   * @param {Vector3} point3
   * @returns {Vector2}
   */
  pointToProjection( point3 ) {
    // TODO: could inline operations so we don't do as much work here
    assert( point3 instanceof Vector3 );

    const origin = v3( 0, 0, -2 );
    const direction = v3( point3.x, point3.y, point3.z + 1 ).normalize();

    const ray = Ray3.create( origin, direction );

    const t = ray.distanceToHyperplane( 0, Vector3.Z_UNIT );
    const p = ray.pointAtDistance( t );

    origin.freeToPool();
    direction.freeToPool();
    ray.freeToPool();

    return v2( p.x, p.y );
  },

  /**
   * Returns the minimum radius of a circle in the projected hyperplane that will fully contain the projected image of
   * a sphere in the full space with the given radius and distance from the camera.
   * @public
   *
   * @param {number} radius
   * @param {number} distance
   * @returns {number}
   */
  minimumRadiusToContainSphere( radius, distance ) {
    assert( typeof radius === 'number' );
    assert( typeof distance === 'number' );
    assert( radius >= 0 );
    assert( distance >= 0 );

    const point = tangentPointOnCircle( radius, distance );
    const value = point.subtract( v2( distance, 0 ) ).normalize().y;
    return ( 2 - 2 * Math.sqrt( 1 - value * value ) ) / value;
  }
};

namespace.Stereographic3 = Stereographic3;

export default Stereographic3;
