
import assert from 'core/assert';
import extend from 'core/extend';
import namespace from 'namespace';

class ShaderProgram {
  /**
   * @param {WebGL2RenderingContext} gl
   * @param {string} vertexSource
   * @param {string} fragmentSource
   * @param {Object} [options]
   */
  constructor( gl, vertexSource, fragmentSource, options ) {
    assert( typeof vertexSource === 'string' );
    assert( typeof fragmentSource === 'string' );

    options = extend( {
      attributes: [], // {Array.<string>} - vertex attribute names
      uniforms: [] // {Array.<string>} - uniform names
    }, options );

    assert( Array.isArray( options.attributes ) );
    assert( Array.isArray( options.uniforms ) );

    // @private {string}
    this.vertexSource = vertexSource;
    this.fragmentSource = fragmentSource;

    // @private {Array.<string>}
    this.attributeNames = options.attributes;
    this.uniformNames = options.uniforms;

    // @private {WebGL2RenderingContext|null}
    this.gl = null;

    // @private {boolean}
    this.used = false;

    // @private {WebGLProgram|null}
    this.program = null;

    // @public {Object} - Maps name => location
    this.uniformLocations = {}; // map name => uniform location for program
    this.attributeLocations = {}; // map name => attribute location for program

    // @public {Object} - Maps name => boolean (enabled)
    this.activeAttributes = {};

    this.initialize( gl );
  }

  /**
   * @param {WebGL2RenderingContext} gl
   */
  initialize( gl ) {
    assert( gl instanceof WebGL2RenderingContext );

    this.gl = gl;
    this.used = false;
    this.program = this.gl.createProgram();

    const vertexShader = this.createShader( this.vertexSource, gl.VERTEX_SHADER );
    const fragmentShader = this.createShader( this.fragmentSource, gl.FRAGMENT_SHADER );

    gl.attachShader( this.program, vertexShader );
    gl.attachShader( this.program, fragmentShader );
    gl.linkProgram( this.program );

    if ( !gl.getProgramParameter( this.program, gl.LINK_STATUS ) ) {
      console.log( 'GLSL link error:' );
      console.log( gl.getProgramInfoLog( this.program ) );
      console.log( 'for vertex shader' );
      console.log( this.vertexSource );
      console.log( 'for fragment shader' );
      console.log( this.fragmentSource );

      // Normally it would be best to throw an exception here, but a context loss could cause the shader parameter check
      // to fail, and we must handle context loss gracefully between any adjacent pair of gl calls.
      // Therefore, we simply report the errors to the console.  See #279
    }

    // clean these up, they aren't needed after the link
    gl.deleteShader( vertexShader );
    gl.deleteShader( fragmentShader );

    this.uniformLocations = {}; // map name => uniform location for program
    this.attributeLocations = {}; // map name => attribute location for program
    this.activeAttributes = {}; // map name => boolean (enabled)

    this.attributeNames.forEach( attributeName => {
      this.attributeLocations[ attributeName ] = gl.getAttribLocation( this.program, attributeName );
      this.activeAttributes[ attributeName ] = true; // default to enabled
    } );
    this.uniformNames.forEach( uniformName => {
      this.uniformLocations[ uniformName ] = gl.getUniformLocation( this.program, uniformName );
    } );
  }

  /*
   * Creates and compiles a GLSL Shader object in WebGL.
   * @public
   *
   * @param {number} type - Should be: gl.VERTEX_SHADER or gl.FRAGMENT_SHADER
   * @param {tring} source - The shader source code.
   */
  createShader( source, type ) {
    var shader = this.gl.createShader( type );
    this.gl.shaderSource( shader, source );
    this.gl.compileShader( shader );

    if ( !this.gl.getShaderParameter( shader, this.gl.COMPILE_STATUS ) ) {
      console.log( 'GLSL compile error:' );
      console.log( this.gl.getShaderInfoLog( shader ) );
      console.log( source );

      // Normally it would be best to throw an exception here, but a context loss could cause the shader parameter check
      // to fail, and we must handle context loss gracefully between any adjacent pair of gl calls.
      // Therefore, we simply report the errors to the console.  See #279
    }

    return shader;
  }

  use() {
    if ( this.used ) { return; }

    this.used = true;
    this.gl.useProgram( this.program );

    // enable the active attributes
    this.attributeNames.forEach( attributeName => {
      if ( this.activeAttributes[ attributeName ] ) {
        this.enableVertexAttribArray( attributeName );
      }
    } );
  }

  // TODO: Is this really the right abstraction?
  activateAttribute( attributeName ) {
    // guarded so we don't enable twice
    if ( !this.activeAttributes[ attributeName ] ) {
      this.activeAttributes[ attributeName ] = true;

      if ( this.used ) {
        this.enableVertexAttribArray( attributeName );
      }
    }
  }

  enableVertexAttribArray( attributeName ) {
    this.gl.enableVertexAttribArray( this.attributeLocations[ attributeName ] );
  }

  unuse() {
    if ( !this.used ) { return; }

    this.used = false;

    this.attributeNames.forEach( attributeName => {
      if ( this.activeAttributes[ attributeName ] ) {
        this.disableVertexAttribArray( attributeName );
      }
    } );
  }

  disableVertexAttribArray( attributeName ) {
    this.gl.disableVertexAttribArray( this.attributeLocations[ attributeName ] );
  }

  deactivateAttribute( attributeName ) {
    // guarded so we don't disable twice
    if ( this.activeAttributes[ attributeName ] ) {
      this.activeAttributes[ attributeName ] = false;

      if ( this.used ) {
        this.disableVertexAttribArray( attributeName );
      }
    }
  }

  dispose() {
    this.gl.deleteProgram( this.program );
  }

  static toFloat( n ) {
    var s = n.toString();
    return ( s.indexOf( '.' ) < 0 && s.indexOf( 'e' ) < 0 && s.indexOf( 'E' ) < 0 ) ? ( s + '.0' ) : s;
  }

  /**
   * TODO: More general?
   * @param {WebGL2RenderingContext} gl
   * @param {number} width
   * @param {number} height
   * @param {TexType} texType
   * @returns {WebGLTexture}
   */
  static makeNearestTexture( gl, width, height, texType ) {
    var texture = gl.createTexture();
    gl.bindTexture( gl.TEXTURE_2D, texture );
    gl.pixelStorei( gl.UNPACK_FLIP_Y_WEBGL, false );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE );
    gl.texParameteri( gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE );
    gl.texImage2D( gl.TEXTURE_2D, 0, texType.internal, width, height, 0, texType.format, texType.type, null );
    gl.bindTexture( gl.TEXTURE_2D, null );
    return texture;
  }

  static updateNearestTexture( gl, texture, width, height, texType ) {
    gl.bindTexture( gl.TEXTURE_2D, texture );
    gl.texImage2D( gl.TEXTURE_2D, 0, texType.internal, width, height, 0, texType.format, texType.type, null );
    gl.bindTexture( gl.TEXTURE_2D, null );
    return texture;
  }
}

ShaderProgram.TexType = {
  RGB: {
    internal: 0x1907, // RGB
    format: 0x1907, // RGB
    type: 0x1401 // UNSIGNED_BYTE
  },
  RGBA: {
    internal: 0x1908, // RGBA
    format: 0x1908, // RGBA
    type: 0x1401 // UNSIGNED_BYTE
  },
  RGBA16F: {
    internal: 0x881A, // RGBA16F,
    format: 0x1908, // RGBA
    type: 0x1406 // FLOAT    TODO could use HALF_FLOAT
  }
};

namespace.ShaderProgram = ShaderProgram;

export default ShaderProgram;
