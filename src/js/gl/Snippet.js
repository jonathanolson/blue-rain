
import assert from 'core/assert';
import constantLength from 'gl/constantLength';
import namespace from 'namespace';

let globalSnippetIdCounter = 0;

class Snippet {
  /**
   * Represents a piece of GLSL shader code with dependencies on other snippets. Supports serialization of only the
   * code that is needed.
   *
   * const a = new Snippet( 'A' )
   * const b = new Snippet( 'B', [a] )
   * const c = new Snippet( 'C', [a] )
   * const d = new Snippet( 'D', [b,c] )
   * d.toString() => "ABCD"
   * b.toString() => "AB"
   * c.toString() => "AC"
   *
   * @param {string} source
   * @param {Array.<Snippet>} dependencies
   */
  constructor( source, dependencies = [] ) {
    assert( typeof source === 'string' );
    assert( Array.isArray( dependencies ) );

    // @private
    this.id = globalSnippetIdCounter++;
    this.source = source;
    this.dependencies = dependencies;
  }

  /**
   * Assuming no circular dependencies, this returns the entire required subprogram as a string.
   * usedSnippets is used for internal use, just call toString().
   *
   * @param {Object} [usedSnippets] - Optional map from snippet ID => whether it was used.
   * @returns {boolean}
   */
  toString( usedSnippets = {} ) {
    let result = '';

    // if we have already been included, all of our dependencies have been included
    if ( usedSnippets[ this.id ] ) {
      return result;
    }

    if ( this.dependencies ) {
      for ( let i = 0; i < this.dependencies.length; i++ ) {
        result += this.dependencies[i].toString( usedSnippets );
      }
    }

    result += this.source;

    usedSnippets[ this.id ] = true;

    return result;
  }

  /**
   * Creates a snippet for a numeric constant from a given large-precision string.
   * @public
   *
   * @param {string} name
   * @param {string} value
   * @returns {Snippet}
   */
  static numericConstant( name, value ) {
    return new Snippet( `#define ${name} ${value.substring( 0, constantLength )}\n` );
  }
}

namespace.Snippet = Snippet;

export default Snippet;

/*

var epsilon = '0.0001';
var smallEpsilon = '0.0000001';

Snippet.rand = new Snippet(
  'highp float rand(vec2 co) {\n' +
  '    highp float a = 12.9898;\n' +
  '    highp float b = 78.233;\n' +
  '    highp float c = 43758.5453;\n' +
  '    highp float dt= dot(co.xy ,vec2(a,b));\n' +
  '    highp float sn= mod(dt,3.1415926);\n' +
  '    return fract(sin(sn) * c);\n' +
  '}\n' );

Snippet.pseudorandom = new Snippet(
  'float pseudorandom(float u) {\n' +
    '  float a = fract(sin(gl_FragCoord.x*12.9898*3758.5453));\n' +
    '  float b = fract(sin(gl_FragCoord.x*63.7264*3758.5453));\n' +
    '  return rand(gl_FragCoord.xy * mod(u * 4.5453,3.1415926));\n' +
  '}\n', [ Snippet.rand ] );

// for a plane determined by "normal . p = d" for points "p", returns ray t to intersection
Snippet.rayIntersectPlane4 = new Snippet(
  'float rayIntersectPlane4( vec4 normal, float d, vec4 rayPos, vec4 rayDir ) {\n' +
  '  return ( d - dot( normal, rayPos ) ) / dot( normal, rayDir );\n' +
  '}\n' );

// returns vec2( tNear, tFar). intersection based on the slab method.
// Only assume an intersection if tNear < tFear
Snippet.rayIntersectAABB4 = new Snippet(
  'float rayIntersectAABB4( vec4 boxMinCorner, vec4 boxMaxCorner, vec4 rayPos, vec4 rayDir ) {\n' +
  // t values for the negative plane sides
  '  vec4 tBack = ( boxMinCorner - rayPos ) / rayDir;\n' +
  '  vec4 tFront = ( boxMaxCorner - rayPos ) / rayDir;\n' +

  // sort t values based on closeness
  '  vec4 tMin = min( tBack, tFront );\n' +
  '  vec4 tMax = max( tBack, tFront );\n' +

  // farthest "near" is when the ray as passed all three planes
  '  float tNear = max( max( max( tMin.x, tMin.y ), tMin.z ), tMin.w );\n' +

  // closest "far" is when the ray will exit
  '  float tFar = min( min( min( tMax.x, tMax.y ), tMax.z ), tMax.w );\n' +

  // if tNear >= tFar, there is no intersection
  '  return vec2( tNear, tFar );\n' +
  '}\n' );

// boxCenter = (maxCorner + minCorner)/2, boxHalfSize = (maxCorner - minCorner)/2
Snippet.normalOnAABB4 = new Snippet(
  'vec3 normalOnAABB4( vec4 boxCenter, vec4 boxHalfSize, vec4 point ) {\n' +
  '  vec4 delta = ( point - boxCenter ) / boxHalfSize;\n' +
  '  vec4 ab = abs( delta );\n' +
  '  if ( ab.x > ab.y ) {\n' +
  '    if ( ab.x > ab.z ) {\n' +
  '      if ( ab.x > ab.w ) {\n' +
  '        return vec4( sign( delta.x ), 0, 0, 0 );\n' +
  '      }\n' +
  '    } else {\n' +
  '      if ( ab.z > ab.w ) {\n' +
  '        return vec4( 0, 0, sign( delta.z ), 0 );\n' +
  '      }\n' +
  '    }\n' +
  '  } else {\n' +
  '    if ( ab.y > ab.z ) {\n' +
  '      if ( ab.y > ab.w ) {\n' +
  '        return vec4( 0, sign( delta.y ), 0, 0 );\n' +
  '      }\n' +
  '    } else {\n' +
  '      if ( ab.z > ab.w ) {\n' +
  '        return vec4( 0, 0, sign( delta.z ), 0 );\n' +
  '      }\n' +
  '    }\n' +
  '  }\n' +
  '  return vec4( 0, 0, 0, sign( delta.w ) );\n' +
  '}\n' );

// boxCenter = (maxCorner + minCorner)/2, boxHalfSize = (maxCorner - minCorner)/2
// NOTE: for intersections very close to corners and edges, we may return a normal blended between the faces
Snippet.normalFastOnAABB4 = new Snippet(
  'vec4 normalFastOnAABB4( vec4 boxCenter, vec4 boxHalfSize, vec4 point ) {\n' +
  '  vec4 unitDelta = ( point - boxCenter ) / boxHalfSize;\n' +
  '  return normalize( step( 1.0 - ' + epsilon + ', unitDelta ) - step( 1.0 - ' + epsilon + ', -1.0 * unitDelta ) );\n' +
  '}\n' );

// returns vec2( tNear, tFar ), only assume intersection if tNear < tFar
Snippet.rayIntersect3Sphere = new Snippet(
  'vec2 rayIntersect3Sphere( vec4 center, float radius, vec4 rayPos, vec4 rayDir ) {\n' +
  '  vec4 toSphere = rayPos - center;\n' +
  '  float a = dot( rayDir, rayDir );\n' +
  '  float b = 2.0 * dot( toSphere, rayDir );\n' +
  '  float c = dot( toSphere, toSphere ) - radius * radius;\n' +
  '  float discriminant = b * b - 4.0 * a * c;\n' +
  '  if( discriminant > ' + smallEpsilon + ' ) {\n' +
  '    float sqt = sqrt( discriminant );\n' +
  '    return ( vec2( -sqt, sqt ) - b ) / ( 2.0 * a );\n' +
  '  } else {\n' +
  '    return vec2( 1.0, -1.0 );\n' +
  '  }\n' +
  '}\n' );

Snippet.boxMuller = new Snippet(
  'vec2 boxMuller( float xi1, float xi2 ) {\n' +
  '  float angle = TWO_PI * xi2;\n' +
  '  return vec2( cos( angle ), sin( angle ) ) * sqrt( -2 * log( xi1 ) );\n' +
  '}\n', [ Snippet.TWO_PI ] );

Snippet.sampleUniformOn3Sphere = new Snippet(
  'vec4 sampleUniformOn3Sphere( float xi1, float xi2, float xi3, float xi4 ) {\n' +
  '  return normalize( vec4( boxMuller( xi1, xi2 ).xy, boxMuller( xi3, xi4 ).xy ) );\n' +
  '}\n', [ Snippet.boxMuller ] );

// w >= 0
Snippet.sampleUniformOn3Hemisphere = new Snippet(
  'vec4 sampleUniformOn3Hemisphere( float xi1, float xi2, float xi3, float xi4 ) {\n' +
  '  vec2 boxy = boxMuller( xi3, xi4 );\n' +
  '  return normalize( vec4( boxMuller( xi1, xi2 ).xy, boxy.x, abs( boxy.y ) ) );\n' +
  '}\n', [ Snippet.boxMuller ] );

// dot-weighted by (0,0,0,1)
Snippet.sampleDotWeightOn3Hemiphere = new Snippet(
  'vec4 sampleDotWeightOnHemiphere( float xi1, float xi2, float xi3 ) {\n' +
  '  float tr = pow( abs( xi1 ), 1.0/3.0 );\n' +
  '  float mag = tr * sqrt( xi2 * ( 1 - xi2 ) );\n' +
  '  float angle = TWO_PI * xi3;\n' +
  '  return vec4( mag * cos( angle ), mag * sin( angle ), tr * ( 1 - 2 * xi2 ), sqrt( 1 - tr * tr ) );\n' +
  '}\n', [ Snippet.TWO_PI ] );

Snippet.constructBasis4 = new Snippet(
  // assumes n.y and n.w are not zero
  'mat4 constructBasis4Ordered( vec4 n ){\n' +
  '  float n14 = n.x / n.w;\n' +
  '  float n32 = n.z / n.y;\n' +
  '  vec4 x = vec4( 1, 0, 0, -n14 ) * inversesqrt( n14 * n14 + 1.0 );\n' +
  '  vec4 y = vec4( 0, -n32, 1, 0 ) * inversesqrt( n32 * n32 + 1.0 );\n' +
  '  vec4 z = vec4( n.y * x.w / y.z, n.w * y.z / x.x, -n.w * y.y / x.x, -n.y * x.x / y.z );\n' +
  '  return mat4( x, y, z, n );\n' +
  '}\n' +

  'mat4 constructBasis4( vec4 normal ) {\n' +
  '  bvec4 sig = greaterThan( abs( normal ), vec4( ' + smallEpsilon + ' ) );\n' +
  '  mat4 basis;\n' +
  '  if ( sig.x ) {\n' +
  '    if ( sig.y ) {\n' +
  '      basis = constructBasis4Ordered( normal.wyzx );\n' +
  '      return mat4( basis[0].wyzx, basis[1].wyzx, basis[2].wyzx, basis[3].wyzx );\n' +
  '    } else if ( sig.z ) {\n' +
  '      basis = constructBasis4Ordered( normal.yxwz );\n' +
  '      return mat4( basis[0].yxwz, basis[1].yxwz, basis[2].yxwz, basis[3].yxwz );\n' +
  '    } else if ( sig.w ) {\n' +
  '      basis = constructBasis4Ordered( normal.yxzw );\n' +
  '      return mat4( basis[0].yxzw, basis[1].yxzw, basis[2].yxzw, basis[3].yxzw );\n' +
  '    } else {\n' +
  '      return mat4( vec4(0,1,0,0), vec4(0,0,1,0), vec4(0,0,0,1), normal );\n' +
  '    }\n' +
  '  } else if ( sig.y ) {\n' +
  '    if ( sig.z ) {\n' +
  '      basis = constructBasis4Ordered( normal.xywz );\n' +
  '      return mat4( basis[0].xywz, basis[1].xywz, basis[2].xywz, basis[3].xywz );\n' +
  '    } else if ( sig.w ) {\n' +
  '      basis = constructBasis4Ordered( normal.xyzw );\n' +
  '      return mat4( basis[0].xyzw, basis[1].xyzw, basis[2].xyzw, basis[3].xyzw );\n' +
  '    } else {\n' +
  '      return mat4( vec4(1,0,0,0), vec4(0,0,1,0), vec4(0,0,0,1), normal );\n' +
  '    }\n' +
  '  } else if ( sig.z ) {\n' +
  '    if ( sig.w ) {\n' +
  '      basis = constructBasis4Ordered( normal.xzyw );\n' +
  '      return mat4( basis[0].xzyw, basis[1].xzyw, basis[2].xzyw, basis[3].xzyw );\n' +
  '    } else {\n' +
  '      return mat4( vec4(1,0,0,0), vec4(0,1,0,0), vec4(0,0,0,1), normal );\n' +
  '    }\n' +
  '  } else if ( sig.w ) {\n' +
  '    return mat4( vec4(1,0,0,0), vec4(0,1,0,0), vec4(0,0,1,0), normal );\n' +
  '  } else {\n' +
  // no significant vectors? should be impossible, but just bail with default case
  '    return constructBasis4Ordered( normal );\n' +
  '  }\n' +
  '}\n' );

Snippet.sampleBasis4 = new Snippet(
  'vec4 sampleBasis4( mat3 basis, vec4 sampleDir ) {\n' +
  '  return basis[0] * sampleDir.x + basis[1] * sampleDir.y + basis[2] * sampleDir.z + basis[3] * sampleDir.w;\n' +
  '}\n', [ Snippet.constructBasis4 ] );

Snippet.projectPerspective3 = new Snippet(
  'void projectPerspective3( in vec2 p2, out vec3 p3, out vec3 d3 ) { p3 = vec3( 0, 0, 0 ); d3 = normalize( vec3( p2.xy, 1 ) ); }\n', [] );

Snippet.projectPerspective4 = new Snippet(
  'void projectPerspective4( in vec3 p3, out vec4 p4, out vec4 p4 ) { p4 = vec4( 0, 0, 0, 0 ); d4 = normalize( vec4( p3.xyz, 1 ) ); }\n', [] );

Snippet.projectStereographic3 = new Snippet(
  'void projectStereographic3( in vec2 p2, out vec3 p3, out vec3 d3 ) { p3 = vec3( 0, 0, 0 ); d3 = normalize( vec3( 4.0 * p2.xy, 4.0 - p2.x * p2.x - p2.y * p2.y ) ); }\n', [] );

Snippet.projectStereographic4 = new Snippet(
  'void projectStereographic4( in vec3 p3, out vec4 p4, out vec4 p4 ) { p4 = vec4( 0, 0, 0, 0 ); d4 = normalize( vec4( 4.0 * p3.xyz, 4.0 - p3.x * p3.x - p3.y * p3.y - p3.z * p3.z ) ); }\n', [] );

Snippet.projectOrthographic3 = new Snippet(
  'void projectOrthographic3( in vec2 p2, out vec3 p3, out vec3 d3 ) { p3 = vec3( p2.xy, 0 ); d3 = vec3( 0, 0, 1 ); }\n', [] );

Snippet.projectStereographic4 = new Snippet(
  'void projectStereographic4( in vec3 p3, out vec4 p4, out vec4 p4 ) { p4 = vec4( p3.xyz, 0 ); d4 = vec4( 0, 0, 0, 1 ); }\n', [] );
*/
