
import namespace from 'namespace';

const constantLength = 33;

namespace.constantLength = constantLength;

export default constantLength;
