
import Snippet from 'gl/Snippet';

Snippet.SQRT_5 = Snippet.numericConstant( 'SQRT_5', '2.236067977499789696409173668731276235440618359611526' );

export default Snippet.SQRT_5;
