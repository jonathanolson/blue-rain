
import Snippet from 'gl/Snippet';

Snippet.SQRT_2 = Snippet.numericConstant( 'SQRT_2', '1.414213562373095048801688724209698078569671875376948' );

export default Snippet.SQRT_2;
