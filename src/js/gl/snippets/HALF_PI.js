
import Snippet from 'gl/Snippet';

Snippet.HALF_PI = Snippet.numericConstant( 'HALF_PI', '1.5707963267948966192313216916397514420985846996875529' );

export default Snippet.HALF_PI;
