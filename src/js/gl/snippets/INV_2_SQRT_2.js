
import Snippet from 'gl/Snippet';

Snippet.INV_2_SQRT_2 = Snippet.numericConstant( 'INV_2_SQRT_2', '0.3535533905932737622004221810524245196424179688442370' );

export default Snippet.INV_2_SQRT_2;
