
import Snippet from 'gl/Snippet';

Snippet.TWO_PI = Snippet.numericConstant( 'TWO_PI', '6.2831853071795864769252867665590057683943387987502116' );

export default Snippet.TWO_PI;
