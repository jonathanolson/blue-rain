
import Snippet from 'gl/Snippet';

Snippet.PI = Snippet.numericConstant( 'PI', '3.1415926535897932384626433832795028841971693993751058' );

export default Snippet.PI;
