
import Snippet from 'gl/Snippet';

Snippet.PHI = Snippet.numericConstant( 'PHI', '1.618033988749894848204586834365638117720309179805763' );

export default Snippet.PHI;
