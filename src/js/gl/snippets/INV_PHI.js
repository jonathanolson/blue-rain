
import Snippet from 'gl/Snippet';

Snippet.INV_PHI = Snippet.numericConstant( 'INV_PHI', '0.6180339887498948482045868343656381177203091798057629' );

export default Snippet.INV_PHI;
