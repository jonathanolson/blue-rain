
/* eslint no-unused-vars: 0 */  // --> OFF

// TODO: Can we generate this file?

// TODO: Approximate branch date is 2018-11-26 (for dot)

import namespace from 'namespace';

import assert from 'core/assert';
import extend from 'core/extend';
import Poolable from 'core/Poolable';

import arePointsCollinear from 'math/arePointsCollinear';
import circleCenterFromPoints from 'math/circleCenterFromPoints';
import clamp from 'math/clamp';
import distToSegmentSquared from 'math/distToSegmentSquared';
import gcd from 'math/gcd';
import lcm from 'math/lcm';
import lineLineIntersection from 'math/lineLineIntersection';
import mod from 'math/mod';
import moduloBetweenDown from 'math/moduloBetweenDown';
import moduloBetweenUp from 'math/moduloBetweenUp';
import rangeExclusive from 'math/rangeExclusive';
import rangeInclusive from 'math/rangeInclusive';
import roundSymmetric from 'math/roundSymmetric';
import toDegrees from 'math/toDegrees';
import toRadians from 'math/toRadians';
import triangleArea from 'math/triangleArea';
import triangleAreaSigned from 'math/triangleAreaSigned';
import v2 from 'math/v2';
import v3 from 'math/v3';
import v4 from 'math/v4';
import Vector2 from 'math/Vector2';
import Vector3 from 'math/Vector3';
import Vector4 from 'math/Vector4';

export default namespace;
