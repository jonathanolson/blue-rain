
import namespace from 'namespace';

/**
 * Iterates through the 2nd and further objects passed in, respectively assigning their properties to the 1st object
 * in order.
 *
 * @param {...Object}
 */
function extend( obj ) {
  Array.prototype.slice.call( arguments, 1 ).forEach( source => {
    if ( source ) {
      for ( var prop in source ) {
        Object.defineProperty( obj, prop, Object.getOwnPropertyDescriptor( source, prop ) );
      }
    }
  } );
  return obj;
}

namespace.extend = extend;

export default extend;
