
import forEachCombination from 'core/forEachCombination';
import namespace from 'namespace';

/**
 * Returns all combinations of length `quantity` of the given array.
 *
 * @param {number} quantity
 * @param {Array.<*>} array
 * @returns {Array.<Array.<*>>}
 */
function combinations( quantity, array ) {
  const result = [];
  forEachCombination( quantity, array, combination => result.push( combination.slice() ) );
  return result;
}

namespace.combinations = combinations;

export default combinations;
