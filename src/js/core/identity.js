
import namespace from 'namespace';

function identity( x ) {
  return x;
}

namespace.identity = identity;

export default identity;
