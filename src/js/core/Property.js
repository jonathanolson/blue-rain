
import assert from 'core/assert';
import Emitter from 'core/Emitter';
import extend from 'core/extend';
import namespace from 'namespace';

// variables
let globalId = 0; // autoincremented for unique IDs

class Property {
  /**
   * @param {*} value - The initial value
   * @param {Object} [options]
   */
  constructor( value, options ) {
    options = extend( {
      // {boolean} - Whether the `equals` method should be used (otherwise uses `===`)
      useDeepEquality: false,

      // {boolean} - Whether the initial value should be stored
      storeInitialValue: true
    }, options );

    // @public {number} - Unique identifier for this Property.
    this.id = globalId++;

    // @private {*} - Store the internal value and the initial value
    this._value = value;

    // @protected {*} - Initial value
    this._initialValue = options.storeInitialValue ? value : null;

    // @private {boolean} whether to use the values' equals method or === equality
    this.useDeepEquality = options.useDeepEquality;

    // @private {Emitter}
    this.changedEmitter = new Emitter();

    // @public {boolean}
    this.isDisposed = false;
  }

  /**
   * Gets the value.
   * @public
   *
   * @returns {*}
   */
  get() {
    return this._value;
  }

  /**
   * Sets the value and notifies listeners.
   * @public
   *
   * @param {*} value
   * @returns {Property.<*>} this instance, for chaining.
   */
  set( value ) {
    if ( !this.equalsValue( value ) ) {
      this.setValueAndNotifyListeners( value );
    }
    return this;
  }

  /**
   * Returns true if and only if the specified value equals the value of this property
   * @protected
   *
   * @param {*} value
   * @returns {boolean}
   */
  equalsValue( value ) {
    return this.areValuesEqual( value, this._value );
  }

  /**
   * Determines equality semantics for the wrapped type, including whether notifications are sent out when the
   * wrapped value changes, and whether onValue is triggered.
   * @private
   *
   * useDeepEquality: true => Use the `equals` method on the values
   * useDeepEquality: false => Use === for equality test
   *
   * Alternatively different implementation can be provided by subclasses or instances to change the equals
   * definition. See #10 and #73 and #115
   *
   * @param {*} a - should have the same type as Property element type
   * @param {*} b - should have the same type as Property element type
   * @returns {boolean}
   */
  areValuesEqual( a, b ) {
    if ( this.useDeepEquality && a && b && a.constructor === b.constructor ) {

      assert( !!a.equals, 'no equals function for 1st arg' );
      assert( !!b.equals, 'no equals function for 2nd arg' );
      assert( a.equals( b ) === b.equals( a ), 'incompatible equality checks' );

      return a.equals( b );
    }
    else {
      // Reference equality for objects, value equality for primitives
      return a === b;
    }
  }

  /**
   * Returns the initial value of this Property.
   * @public
   *
   * @returns {*}
   */
  getInitialValue() {
    return this._initialValue;
  }

  // @public
  get initialValue() {
    return this.getInitialValue();
  }

  /**
   * Updates the value of this node
   * @protected - can be overridden.
   *
   * @param {*} value - the new value this Property will take, which is different than the previous value.
   */
  setValueAndNotifyListeners( value ) {
    const oldValue = this.get();
    this._value = value;
    this.changedEmitter.emit3( this.get(), oldValue, this );
  }

  /**
   * Use this method when mutating a value (not replacing with a new instance) and you want to send notifications about the change.
   * This is different from the normal axon strategy, but may be necessary to prevent memory allocations.
   * This method is unsafe for removing listeners because it assumes the listener list not modified, to save another allocation
   * Only provides the new reference as a callback (no oldvalue)
   * See https://github.com/phetsims/axon/issues/6
   * @public
   */
  notifyListenersStatic() {
    this.changedEmitter.emit3( this.get(), undefined, this );
  }

  /**
   * Resets the value to the initial value.
   * @public
   */
  reset() {
    this.set( this._initialValue );
  }

  // @public
  get value() { return this.get(); }

  // @public
  set value( newValue ) { this.set( newValue ); }

  /**
   * Adds listener and calls it immediately. If listener is already registered, this is a no-op. The initial
   * notification provides the current value for newValue and null for oldValue.
   * @public
   *
   * @param {function} listener a function of the form listener(newValue,oldValue)
   */
  link( listener ) {
    this.changedEmitter.addListener( listener );
    listener( this.get(), null, this ); // null should be used when an object is expected but unavailable
  }

  /**
   * Add an listener to the Property, without calling it back right away.
   * This is used when you need to register a listener without an immediate callback.
   * @public
   *
   * @param {function} listener - a function with a single argument, which is the value of the property at the time the function is called.
   */
  lazyLink( listener ) {
    this.changedEmitter.addListener( listener );
  }

  /**
   * Removes a listener. If listener is not registered, this is a no-op.
   * @public
   *
   * @param {function} listener
   */
  unlink( listener ) {
    this.changedEmitter.removeListener( listener );
  }

  /**
   * Removes all listeners. If no listeners are registered, this is a no-op.
   * @public
   */
  unlinkAll() {
    this.changedEmitter.removeAllListeners();
  }

  /**
   * Links an object's named attribute to this property.  Returns a handle so it can be removed using Property.unlink();
   * Example: modelVisibleProperty.linkAttribute(view,'visible');
   *
   * @param object
   * @param attributeName
   * @public
   */
  linkAttribute( object, attributeName ) {
    const handle = value => { object[ attributeName ] = value; };
    this.link( handle );
    return handle;
  }

  /**
   * Unlink an listener added with linkAttribute.  Note: the args of linkAttribute do not match the args of
   * unlinkAttribute: here, you must pass the listener handle returned by linkAttribute rather than object and attributeName
   * @public
   *
   * @param {function} listener
   */
  unlinkAttribute( listener ) {
    this.unlink( listener );
  }

  // @public Provide toString for console debugging, see http://stackoverflow.com/questions/2485632/valueof-vs-tostring-in-javascript
  toString() {
    return `Property#${this.id}{${this.get()}}`;
  }

  // @public
  valueOf() { return this.toString(); }

  /**
   * Modifies the value of this Property with the ! operator.  Works for booleans and non-booleans.
   * @public
   */
  toggle() {
    this.value = !this.value;
  }

  // @public Ensures that the Property is eligible for GC
  dispose() {
    // remove any listeners that are still attached to this property
    this.unlinkAll();

    this.isDisposed = true;
    this.changedEmitter.dispose();
  }

  /**
   * Checks whether a listener is registered with this Property
   * @public
   *
   * @param {function} listener
   * @returns {boolean}
   */
  hasListener( listener ) {
    return this.changedEmitter.hasListener( listener );
  }

  /**
   * Returns true if there are any listeners.
   * @public
   *
   * @returns {boolean}
   */
  hasListeners() {
    assert( arguments.length === 0, 'Property.hasListeners should be called without arguments' );

    return this.changedEmitter.hasListeners();
  }
}

namespace.Property = Property;

export default Property;
