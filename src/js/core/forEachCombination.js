
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * Calls the given callback for every combination of length `quantity` of the given array items.
 *
 * NOTE: The array will be mutated while processing, so create a copy if needed.
 *
 * @param {number} quantity
 * @param {Array.<*>} array
 * @param {function} callback - function( {Array.<*>} combination )
 */
function forEachCombination( quantity, array, callback ) {
  assert( typeof quantity === 'number' );
  assert( Array.isArray( array ) );
  assert( typeof callback === 'function' );

  const combination = [];
  function recur( index ) {
    if ( combination.length === quantity ) {
      callback( combination );
    }
    else if ( index < array.length ) {
      combination.push( array[ index ] );
      recur( index + 1 );
      combination.pop();
      recur( index + 1 );
    }
  }
  recur( 0 );
}

namespace.forEachCombination = forEachCombination;

export default forEachCombination;
