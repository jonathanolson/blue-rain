
import namespace from 'namespace';

function assert( predicate, message ) {
  if ( !predicate ) {
    throw new Error( message );
  }
  return predicate;
}

// Inform when assertions are enabled?
// let enabled = false;
// assert( ( () => { enabled = true; return true; } )() );
// if ( enabled ) {
//   console.log( 'assertions enabled' );
// }

namespace.assert = assert;

export default assert;
