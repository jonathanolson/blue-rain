
import namespace from 'namespace';

/**
 * Returns a fresh array with only unique elements.
 * @public
 *
 * @param {Array.<*>} array
 * @returns {Array.<*>}
 */
function unique( array ) {
  const result = [];

  for ( let i = 0; i < array.length; i++ ) {
    const element = array[ i ];
    if ( !result.includes( element ) ) {
      result.push( element );
    }
  }

  return result;
}

namespace.unique = unique;

export default unique;
