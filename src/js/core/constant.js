
import namespace from 'namespace';

function constant( x ) {
  return () => x;
}

namespace.constant = constant;

export default constant;
