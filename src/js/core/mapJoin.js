
import namespace from 'namespace';

/**
 * Returns the result of mapping the array with the function (presumably returning strings) and then joining them with
 * the given string
 *
 * @param {Array.<*>} array
 * @param {function} mapFunction
 * @param {string} [joinString]
 * @returns {string}
 */
function mapJoin( array, mapFunction, joinString = '' ) {
  return array.map( mapFunction ).join( joinString );
}

namespace.mapJoin = mapJoin;

export default mapJoin;
