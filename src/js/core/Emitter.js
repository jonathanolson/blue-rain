
import assert from 'core/assert';
import namespace from 'namespace';

class Emitter {
  /**
   * @param {function} initialListener
   */
  constructor( initialListener = null ) {
    // @private {function[]} - the listeners that will be called on emit
    this.listeners = [];

    // @private {function[][]} - during emit() keep track of which listeners should receive events in order to manage
    //                         - removal of listeners during emit()
    this.activeListenersStack = [];

    // @public {boolean}
    this.isDisposed = false;

    initialListener && this.addListener( initialListener );
  }

  dispose() {
    this.listeners.length = 0;
    this.isDisposed = true;
  }

  /**
   * Adds a listener.
   * @public
   *
   * @param {function} listener
   */
  addListener( listener ) {
    assert( this.listeners.indexOf( listener ) === -1, 'Cannot add the same listener twice' );

    if ( this.isDisposed ) { return; }

    // If a listener is added during an emit(), we must make a copy of the current list of listeners--the newly added
    // listener will be available for the next emit() but not the one in progress.  This is to match behavior with
    // removeListener.
    this.defendListeners();

    this.listeners.push( listener );
  }

  /**
   * Removes a listener
   * @public
   *
   * @param {function} listener
   */
  removeListener( listener ) {
    if ( this.isDisposed ) { return; }

    const index = this.listeners.indexOf( listener );

    // Throw an error when removing a non-listener (except when the Emitter has already been disposed, see
    // https://github.com/phetsims/sun/issues/394#issuecomment-419998231
    assert( index >= 0, 'tried to removeListener on something that wasn\'t a listener' );

    // If an emit is in progress, make a copy of the current list of listeners--the removed listener will remain in
    // the list and be called for this emit call, see #72
    this.defendListeners();

    this.listeners.splice( index, 1 );
  }

  /**
   * Removes all the listeners
   * @public
   */
  removeAllListeners() {
    if ( this.isDisposed ) { return; }

    while ( this.listeners.length > 0 ) {
      this.removeListener( this.listeners[ 0 ] );
    }
  }

  /**
   * If addListener/removeListener is called while emit() is in progress, we must make a defensive copy of the array
   * of listeners before changing the array, and use it for the rest of the notifications until the emit call has
   * completed.
   * @private
   */
  defendListeners() {
    for ( let i = this.activeListenersStack.length - 1; i >= 0; i-- ) {

      // Once we meet a level that was already defended, we can stop, since all previous levels are also defended
      if ( this.activeListenersStack[ i ].defended ) {
        break;
      }
      else {
        const defendedListeners = this.listeners.slice();

        // Mark copies as 'defended' so that it will use the original listeners when emit started and not the modified
        // list.
        defendedListeners.defended = true;
        this.activeListenersStack[ i ] = defendedListeners;
      }
    }
  }

  /**
   * Emits a single event.
   * @public
   *
   * @params - expected parameters are based on options.argumentTypes, see constructor
   */
  emit() {
    // Notify wired-up listeners, if any
    if ( this.listeners.length > 0 ) {

      this.activeListenersStack.push( this.listeners );
      const lastEntry = this.activeListenersStack.length - 1;

      // Notify listeners
      for ( let i = 0; i < this.activeListenersStack[ lastEntry ].length; i++ ) {
        this.activeListenersStack[ lastEntry ][ i ].apply( this, arguments );
      }

      this.activeListenersStack.pop();
    }
  }

  /**
   * Checks whether a listener is registered with this Emitter
   * @public
   *
   * @param {function} listener
   * @returns {boolean}
   */
  hasListener( listener ) {
    assert( arguments.length === 1, 'Emitter.hasListener should be called with 1 argument' );

    return this.listeners.indexOf( listener ) >= 0;
  }

  /**
   * Returns true if there are any listeners.
   * @public
   *
   * @returns {boolean}
   */
  hasListeners() {
    assert( arguments.length === 0, 'Emitter.hasListeners should be called without arguments' );

    return this.listeners.length > 0;
  }

  /**
   * Returns the number of listeners.
   * @public
   *
   * @returns {number}
   */
  getListenerCount() {
    return this.listeners.length;
  }
}

namespace.Emitter = Emitter;

export default Emitter;
