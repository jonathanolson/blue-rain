
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * @param {Array.<Array.<...*>>}
 * @returns {Array.<*>}
 */
function flatten( arr, maxCount = 1 ) {
  // TODO: tests
  assert( typeof maxCount === 'number' && maxCount >= 0 );

  if ( maxCount === 0 ) {
    return arr;
  }

  const result = [];

  if ( Array.isArray( arr ) ) {
    arr.forEach( item => {
      const flattened = flatten( item, maxCount - 1 );
      Array.prototype.push.apply( result, flattened );
    } );
  }
  else {
    result.push( arr );
  }

  return result;
}

namespace.flatten = flatten;

export default flatten;
