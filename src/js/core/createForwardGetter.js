
import assert from 'core/assert';
import namespace from 'namespace';

function createForwardGetter( proto, wrap ) {
  assert( typeof wrap === 'function' );
  
  return ( fullName, esName ) => Object.defineProperty( proto, esName, {
    get: wrap( [], `return this.${fullName}();` )
  } );
}

namespace.createForwardGetter = createForwardGetter;

export default createForwardGetter;
