
import assert from 'core/assert';
import namespace from 'namespace';

/**
 * @param {Object} mapping - string keys map to values provided to the closure
 * @param {function} assertionFunction - When passed a parameter name, it should return a string for an assertion
 * @returns {function} - function( {Array.<string>} params, {string} body ) => {function}
 */
function createFunctionWrap( mapping, assertionFunction ) {
  assert( typeof mapping === 'object' );
  assert( typeof assertionFunction === 'function' );

  return ( params, body, includeAssertions = true ) => {
    const js = `function(${params.join( ',' )}){${includeAssertions ? assertionFunction( params ) : ''}${body}}`;
    const keys = Object.keys( mapping );
    return ( new Function( ...keys, `return ${js};` ) )( ...keys.map( key => mapping[ key ] ) );
  };
}

namespace.createFunctionWrap = createFunctionWrap;

export default createFunctionWrap;
