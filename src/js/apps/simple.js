
// import Bounds3 from 'math/Bounds3';
import namespace from 'namespace';
import Plane3 from 'math/Plane3';
import Ray3 from 'math/Ray3';
import sphereRayIntersection from 'math/sphereRayIntersection';
import v3 from 'math/v3';
import Vector3 from 'math/Vector3';

const ground = new Plane3( Vector3.Y_UNIT, 0 );
// const box = new Bounds3( -1, 0, -1, 1, 2, 1 );
// TODO: box intersection seems fucked

const face = new Plane3( Vector3.Z_UNIT, 5.5 );

const size = 1024;

/*
  // dot-weighted by (0,0,1)
  snippets.sampleDotWeightOnHemiphere = new tess.Snippet(
    'vec3 sampleDotWeightOnHemiphere( float xi1, float xi2 ) {\n' +
    '  float angle = TWO_PI * xi1;\n' +
    '  float mag = sqrt( xi2 );\n' +
    '  return vec3( mag * cos( angle ), mag * sin( angle ), sqrt( 1.0 - xi2 ) );\n' +
    '}\n', [snippets.TWO_PI] );
    */

/*
  snippets.sampleTowardsNormal3 = new tess.Snippet(
    'vec3 sampleTowardsNormal3( vec3 normal, vec3 sampleDir ) {\n' +
    '  vec3 a, b;\n' +
    '  if ( abs( normal.x ) < 0.5 ) {\n' +
    '    a = normalize( cross( normal, vec3( 1, 0, 0 ) ) );\n' +
    '  } else {\n' +
    '    a = normalize( cross( normal, vec3( 0, 1, 0 ) ) );\n' +
    '  }\n' +
    '  b = normalize( cross( normal, a ) );\n' +
    '  return a * sampleDir.x + b * sampleDir.y + normal * sampleDir.z;\n' +
    '}\n' );
    */

function sampleDotWeightOnHemisphere() {
  const xi1 = Math.random();
  const xi2 = Math.random();
  const angle = 2 * Math.PI * xi1;
  const mag = Math.sqrt( xi2 );
  return v3( mag * Math.cos( angle ), mag * Math.sin( angle ), Math.sqrt( 1 - xi2 ) );
}

const scratchVector3A = Vector3.ZERO.copy();
const scratchVector3B = Vector3.ZERO.copy();

function sampleTowards3( normal, sampleDir ) {
  const a = scratchVector3A.set( normal );
  if ( Math.abs( normal.x ) < 0.5 ) {
    a.setCross( Vector3.X_UNIT ).normalize();
  } else {
    a.setCross( Vector3.Y_UNIT ).normalize();
  }
  const b = scratchVector3B.set( normal ).setCross( a ).normalize();

  a.multiplyScalar( sampleDir.x );
  b.multiplyScalar( sampleDir.y );
  return Vector3.dirty().set( normal ).multiplyScalar( sampleDir.z ).add( a ).add( b );
  // return a.timesScalar( sampleDir.x ).plus( b.timesScalar( sampleDir.y ) ).plus( normal.timesScalar( sampleDir.z ) );
}

function sample( ray3 ) {
  // TODO: update
  let iteration = 0;

  const attenuation = v3( 1, 1, 1 );

  const ray = ray3.copy();
  while ( iteration++ < 15 ) {
    const groundT = ray.distanceToHyperplane( ground.distance, ground.normal );
    const faceT = ray.distanceToHyperplane( face.distance, face.normal );
    const sphereInfo = sphereRayIntersection( 1, ray, 1e-9 );
    // const boxInfo = box.rayIntersectionRangeAndNormal( ray );

    let t = Number.POSITIVE_INFINITY;
    let normal = Vector3.ZERO;
    let op = () => {};

    if ( groundT > 1e-8 && groundT < t ) {
      t = groundT;
      normal = Vector3.Y_UNIT;
      op = () => {};
    }
    if ( faceT > 1e-8 && faceT < t ) {
      const p = ray.pointAtDistance( faceT );
      if ( Math.sqrt( p.x * p.x + p.y * p.y ) < 5 ) {
        t = faceT;
        normal = v3( 0, 0, -Math.sign( ray.direction.z ) );
        op = () => {
          attenuation.y *= 0.5;
          attenuation.z *= 0.5;
        };
      }
    }
    if ( sphereInfo && sphereInfo.distance > 1e-8 && sphereInfo.distance < t ) {
      t = sphereInfo.distance;
      normal = sphereInfo.normal;
      op = () => {};
    }

    op();
    // else if ( boxInfo.tNear < boxInfo.tFar ) {
    //   if ( boxInfo.tNear > 1e-8 && boxInfo.tNear < t ) {
    //     t = boxInfo.tNear;
    //     normal = boxInfo.tNearNormal;
    //     attenuation.set( Vector3.ZERO );
    //     break;
    //   }
    //   else if ( boxInfo.tFar > 1e-8 && boxInfo.tFar < t ) {
    //     t = boxInfo.tFar;
    //     normal = boxInfo.tFarNormal;
    //   }
    // }

    if ( t !== Number.POSITIVE_INFINITY ) {
      // sampleTowardsNormal3( normal, sampleDotWeightOnHemiphere( pseudorandom(float(bounce) + seed*164.32+2.5), pseudorandom(float(bounce) + 7.233 * seed + 1.3) ) )
      if ( Math.random() < 0.85 ) {
        ray.shift( t );
        ray.direction = sampleTowards3( normal, sampleDotWeightOnHemisphere() );
        // ray.reflect( normal );
        
        if ( Math.abs( ray.position.y ) < 1e-6 && Math.abs( ray.position.x - Math.round( ray.position.x ) ) < 0.003 || Math.abs( ray.position.z - Math.round( ray.position.z ) ) < 0.003 ) {
          attenuation.set( Vector3.ZERO );
          break;
        }

        ray.shift( 1e-7 ); // go forward just a bit
      }
      else {
        attenuation.set( Vector3.ZERO );
        break;
      }
    }
    else {
      break;
    }
  }

  return attenuation;
}

namespace.sample = sample;


const canvas = document.createElement( 'canvas' );
namespace.canvas = canvas;
canvas.width = size;
canvas.height = size;
const context = canvas.getContext( '2d' );

const imageData = context.createImageData( size, size );

const rgbData = new Float64Array( size * size * 3 );

for ( let i = 0; i < size * size; i++ ) {
  imageData.data[ i * 4 ] = 255;
  imageData.data[ i * 4 + 1 ] = 128;
  imageData.data[ i * 4 + 2 ] = 255;
  imageData.data[ i * 4 + 3 ] = 255;
}

context.putImageData( imageData, 0, 0 );

document.body.appendChild( canvas );


let count = 0;

namespace.iterate = () => {
  count++;

  for ( let row = 0; row < size; row++ ) {
    for ( let col = 0; col < size; col++ ) {
      const rgbDataIndex = row * size * 3 + col * 3;
      const imageDataIndex = row * size * 4 + col * 4;

      const x = col - size / 2 + Math.random();
      const y = -( row - size / 2 + Math.random() );

      const ray = Ray3.create( v3( 0, 1, -4 ), v3( x, y, size / 2 ).normalized() );
      const attenuation = sample( ray );
      ray.freeToPool();

      rgbData[ rgbDataIndex ] += attenuation.x;
      rgbData[ rgbDataIndex + 1 ] += attenuation.y;
      rgbData[ rgbDataIndex + 2 ] += attenuation.z;

      imageData.data[ imageDataIndex ] = Math.floor( rgbData[ rgbDataIndex ] / count * 255 );
      imageData.data[ imageDataIndex + 1 ] = Math.floor( rgbData[ rgbDataIndex + 1 ] / count * 255 );
      imageData.data[ imageDataIndex + 2 ] = Math.floor( rgbData[ rgbDataIndex + 2 ] / count * 255 );
    }
  }

  context.putImageData( imageData, 0, 0 );
};

(function step() {
  requestAnimationFrame( step );

  namespace.iterate();
})();

export default namespace;
