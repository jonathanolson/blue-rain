
import namespace from 'namespace';
import ShaderProgram from 'gl/ShaderProgram';

const vertexSource = `#version 300 es
in vec4 a_position;

void main() {
  gl_Position = a_position;
}`;

const fragmentSource = `#version 300 es
precision mediump float;
out vec4 outColor;

void main() {
  highp ivec2 index = ivec2( gl_FragCoord.xy );
  outColor = vec4(index.x % 2 == 0 ? 1.0 : 0.0, index.y % 2 == 0 ? 1.0 : 0.0, 0.5, 1);
}`;

const canvas = document.createElement( 'canvas' );
const gl = canvas.getContext( 'webgl2' );

if ( !gl ) {
  throw new Error( 'Could not get a context' );
}

const shaderProgram = new ShaderProgram( gl, vertexSource, fragmentSource, {
  attributes: [ 'a_position' ],
  uniforms: []
} );

const positionBuffer = gl.createBuffer();
gl.bindBuffer( gl.ARRAY_BUFFER, positionBuffer );

gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
  -1, -1,
  -1, 1,
  1, -1,
  1, 1
] ), gl.STATIC_DRAW );

const vao = gl.createVertexArray();
gl.bindVertexArray( vao );
gl.enableVertexAttribArray( shaderProgram.attributeLocations.a_position );
gl.vertexAttribPointer( shaderProgram.attributeLocations.a_position, 2, gl.FLOAT, false, 0, 0 );


canvas.width = 256;
canvas.height = 256;
gl.viewport( 0, 0, 256, 256 );

gl.clearColor( 0, 0, 0, 0 );
gl.clear( gl.COLOR_BUFFER_BIT );

shaderProgram.use();
gl.bindVertexArray( vao );

gl.drawArrays( gl.TRIANGLE_STRIP, 0, 4 );

document.body.appendChild( canvas );

export default namespace;
