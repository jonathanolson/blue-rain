
const yrio = require( '../../../build/yrio.js' );
const qunit = require( 'qunit' );

qunit.module( 'matrixTests' );

qunit.test( 'Matrix3 individual getters/setters', assert => {
  const m = yrio.Matrix3.identity();

  assert.equal( m.m00, 1, 'Part of identity' );
  assert.equal( m.m01, 0, 'Part of identity' );

  m.m00 = 4;

  assert.equal( m.determinant, 4, 'det check' );
} );

qunit.test( 'Matrix2 determinant', assert => {
  const a = 1.15;
  const b = -0.27;
  const c = 0.526;
  const d = 4.21;
  const m = yrio.Matrix2.create( a, b, c, d );

  assert.equal( m.determinant, a * d - b * c, '2d determinant' );
} );
