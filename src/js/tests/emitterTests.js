const yrio = require( '../../../build/yrio.js' );
const qunit = require( 'qunit' );

qunit.module( 'emitterTests' );

qunit.test( 'Emitter Basics', assert => {
  const stack = [];
  const emitter = new yrio.Emitter();
  const a = () => {
    stack.push( 'a' );
    emitter.removeListener( b );
  };
  const b = () => {
    stack.push( 'b' );
  };
  emitter.addListener( a );
  emitter.addListener( b );
  emitter.emit();

  assert.equal( stack.length, 2, 'Should have received 2 callbacks' );
  assert.equal( stack[ 0 ], 'a', 'true' );
  assert.equal( stack[ 1 ], 'b', 'true' );

  assert.equal( emitter.hasListener( b ), false, 'b should have been removed' );
} );

qunit.test( 'Emitter Tricks', assert => {
  const entries = [];

  const emitter = new yrio.Emitter();

  const a = arg => {
    entries.push( { listener: 'a', arg: arg } );

    if ( arg === 'first' ) {
      emitter.emit( 'second' );
    }
  };
  const b = arg => {
    entries.push( { listener: 'b', arg: arg } );

    if ( arg === 'second' ) {
      emitter.addListener( c );
      emitter.emit( 'third' );
    }
  };
  const c = arg => {
    entries.push( { listener: 'c', arg: arg } );
  };

  emitter.addListener( a );
  emitter.addListener( b );
  emitter.emit( 'first' );

  /*
   * Expected order:
   *   a first
   *     a second
   *     b second
   *       a third
   *       b third
   *       c third
   *   b first
   *
   * It looks like "c first" is (currently?) being triggered since defendCallbacks only defends the top of the stack.
   * If the stack is [ undefended, undefended ], changing listeners copies only the top, leaving
   * [ undefended, defended ], and our first event triggers a listener that wasn't listening when it was called.
   */
  entries.forEach( entry => {
    assert.ok( !( entry.listener === 'c' && entry.arg === 'first' ), 'not C,first' );
  } );

  assert.equal( entries.length, 7, 'Should have 7 callbacks' );

  assert.equal( entries[ 0 ].listener, 'a' );
  assert.equal( entries[ 0 ].arg, 'first' );

  assert.equal( entries[ 1 ].listener, 'a' );
  assert.equal( entries[ 1 ].arg, 'second' );

  assert.equal( entries[ 2 ].listener, 'b' );
  assert.equal( entries[ 2 ].arg, 'second' );

  assert.equal( entries[ 3 ].listener, 'a' );
  assert.equal( entries[ 3 ].arg, 'third' );

  assert.equal( entries[ 4 ].listener, 'b' );
  assert.equal( entries[ 4 ].arg, 'third' );

  assert.equal( entries[ 5 ].listener, 'c' );
  assert.equal( entries[ 5 ].arg, 'third' );

  assert.equal( entries[ 6 ].listener, 'b' );
  assert.equal( entries[ 6 ].arg, 'first' );
} );
