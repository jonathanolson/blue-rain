
const yrio = require( '../../../build/yrio.js' );
const qunit = require( 'qunit' );

qunit.module( 'vectorTests' );

qunit.test( 'Vector2 plus', assert => {
  const a = new yrio.Vector2( 1, 2 );
  const b = new yrio.Vector2( 3, 4 );
  const c = a.plus( b );
  assert.equal( c.x, 4, 'plus x' );
  assert.equal( c.y, 6, 'plus y' );
} );

qunit.test( 'Vector ONE and ZERO magnitudes', assert => {
  assert.ok( yrio.equalsEpsilon( Math.sqrt( 2 ), yrio.Vector2.ONE.magnitude ), 'Vector2.ONE' );
  assert.ok( yrio.equalsEpsilon( Math.sqrt( 3 ), yrio.Vector3.ONE.magnitude ), 'Vector3.ONE' );
  assert.ok( yrio.equalsEpsilon( Math.sqrt( 4 ), yrio.Vector4.ONE.magnitude ), 'Vector4.ONE' );
  assert.ok( yrio.equalsEpsilon( 0, yrio.Vector2.ZERO.magnitude ), 'Vector2.ZERO' );
  assert.ok( yrio.equalsEpsilon( 0, yrio.Vector3.ZERO.magnitude ), 'Vector3.ZERO' );
  assert.ok( yrio.equalsEpsilon( 0, yrio.Vector4.ZERO.magnitude ), 'Vector4.ZERO' );
} );

qunit.test( 'Vector shortcuts', assert => {
  assert.ok( yrio.v2( 2, 3 ).equals( new yrio.Vector2( 2, 3 ) ), 'Vector2 v2 equals' );
  assert.ok( yrio.v3( 2, 3, 4 ).equals( new yrio.Vector3( 2, 3, 4 ) ), 'Vector3 v3 equals' );
  assert.ok( yrio.v4( 2, 3, 4, 5 ).equals( new yrio.Vector4( 2, 3, 4, 5 ) ), 'Vector4 v4 equals' );
} );
