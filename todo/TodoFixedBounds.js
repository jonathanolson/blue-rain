// Copyright 2013-2015, University of Colorado Boulder

/**
 * A 2D rectangle-shaped bounded area (bounding box).
 *
 * There are a number of convenience functions to get locations and points on the Bounds. Currently we do not
 * store these with the Bounds2 instance, since we want to lower the memory footprint.
 *
 * minX, minY, maxX, and maxY are actually stored. We don't do x,y,width,height because this can't properly express
 * semi-infinite bounds (like a half-plane), or easily handle what Bounds2.NOTHING and Bounds2.EVERYTHING do with
 * the constructive solid areas.
 *
 * @author Jonathan Olson <jonathan.olson@colorado.edu>
 */

define( function( require ) {
  'use strict';

  var dot = require( 'DOT/dot' );
  var inherit = require( 'PHET_CORE/inherit' );
  var Poolable = require( 'PHET_CORE/Poolable' );
  var Vector2 = require( 'DOT/Vector2' );

  // Temporary instances to be used in the transform method.
  var scratchVector2 = new dot.Vector2();

  /**
   * Creates a 2-dimensional bounds (bounding box).
   * @constructor
   * @public
   *
   * @param {number} minX - The initial minimum X coordinate of the bounds.
   * @param {number} minY - The initial minimum Y coordinate of the bounds.
   * @param {number} maxX - The initial maximum X coordinate of the bounds.
   * @param {number} maxY - The initial maximum Y coordinate of the bounds.
   */
  function Bounds2( minX, minY, maxX, maxY ) {
    assert && assert( maxY !== undefined, 'Bounds2 requires 4 parameters' );

    // @public {number} - The minimum X coordinate of the bounds.
    this.minX = minX;

    // @public {number} - The minimum Y coordinate of the bounds.
    this.minY = minY;

    // @public {number} - The maximum X coordinate of the bounds.
    this.maxX = maxX;

    // @public {number} - The maximum Y coordinate of the bounds.
    this.maxY = maxY;
  }

  dot.register( 'Bounds2', Bounds2 );

  inherit( Object, Bounds2, {
    // @public (read-only) - Helps to identify the dimension of the bounds
    isBounds: true,
    dimension: 2,

    /*---------------------------------------------------------------------------*
     * Properties
     *---------------------------------------------------------------------------*/

    /**
     * The width of the bounds, defined as maxX - minX.
     * @public
     *
     * @returns {number}
     */
    getWidth: function() { return this.maxX - this.minX; },
    get width() { return this.getWidth(); },

    /**
     * The height of the bounds, defined as maxY - minY.
     * @public
     *
     * @returns {number}
     */
    getHeight: function() { return this.maxY - this.minY; },
    get height() { return this.getHeight(); },

    /*
     * Convenience locations
     * upper is in terms of the visual layout in Scenery and other programs, so the minY is the "upper", and minY is the "lower"
     *
     *             minX (x)     centerX        maxX
     *          ---------------------------------------
     * minY (y) | leftTop     centerTop     rightTop
     * centerY  | leftCenter  center        rightCenter
     * maxY     | leftBottom  centerBottom  rightBottom
     */

    /**
     * Alias for minX, when thinking of the bounds as an (x,y,width,height) rectangle.
     * @public
     *
     * @returns {number}
     */
    getX: function() { return this.minX; },
    get x() { return this.getX(); },

    /**
     * Alias for minY, when thinking of the bounds as an (x,y,width,height) rectangle.
     * @public
     *
     * @returns {number}
     */
    getY: function() { return this.minY; },
    get y() { return this.getY(); },

    /**
     * Alias for minX, when thinking in the UI-layout manner.
     * @public
     *
     * @returns {number}
     */
    getLeft: function() { return this.minX; },
    get left() { return this.minX; },

    /**
     * Alias for minY, when thinking in the UI-layout manner.
     * @public
     *
     * @returns {number}
     */
    getTop: function() { return this.minY; },
    get top() { return this.minY; },

    /**
     * Alias for maxX, when thinking in the UI-layout manner.
     * @public
     *
     * @returns {number}
     */
    getRight: function() { return this.maxX; },
    get right() { return this.maxX; },

    /**
     * Alias for maxY, when thinking in the UI-layout manner.
     * @public
     *
     * @returns {number}
     */
    getBottom: function() { return this.maxY; },
    get bottom() { return this.maxY; },

    /**
     * The horizontal (X-coordinate) center of the bounds, averaging the minX and maxX.
     * @public
     *
     * @returns {number}
     */
    getCenterX: function() { return ( this.maxX + this.minX ) / 2; },
    get centerX() { return this.getCenterX(); },

    /**
     * The vertical (Y-coordinate) center of the bounds, averaging the minY and maxY.
     * @public
     *
     * @returns {number}
     */
    getCenterY: function() { return ( this.maxY + this.minY ) / 2; },
    get centerY() { return this.getCenterY(); },

    /**
     * The point (minX, minY), in the UI-coordinate upper-left.
     * @public
     *
     * @returns {Vector2}
     */
    getLeftTop: function() { return new dot.Vector2( this.minX, this.minY ); },
    get leftTop() { return this.getLeftTop(); },

    /**
     * The point (centerX, minY), in the UI-coordinate upper-center.
     * @public
     *
     * @returns {Vector2}
     */
    getCenterTop: function() { return new dot.Vector2( this.getCenterX(), this.minY ); },
    get centerTop() { return this.getCenterTop(); },

    /**
     * The point (right, minY), in the UI-coordinate upper-right.
     * @public
     *
     * @returns {Vector2}
     */
    getRightTop: function() { return new dot.Vector2( this.maxX, this.minY ); },
    get rightTop() { return this.getRightTop(); },

    /**
     * The point (left, centerY), in the UI-coordinate center-left.
     * @public
     *
     * @returns {Vector2}
     */
    getLeftCenter: function() { return new dot.Vector2( this.minX, this.getCenterY() ); },
    get leftCenter() { return this.getLeftCenter(); },

    /**
     * The point (centerX, centerY), in the center of the bounds.
     * @public
     *
     * @returns {Vector2}
     */
    getCenter: function() { return new dot.Vector2( this.getCenterX(), this.getCenterY() ); },
    get center() { return this.getCenter(); },

    /**
     * The point (maxX, centerY), in the UI-coordinate center-right
     * @public
     *
     * @returns {Vector2}
     */
    getRightCenter: function() { return new dot.Vector2( this.maxX, this.getCenterY() ); },
    get rightCenter() { return this.getRightCenter(); },

    /**
     * The point (minX, maxY), in the UI-coordinate lower-left
     * @public
     *
     * @returns {Vector2}
     */
    getLeftBottom: function() { return new dot.Vector2( this.minX, this.maxY ); },
    get leftBottom() { return this.getLeftBottom(); },

    /**
     * The point (centerX, maxY), in the UI-coordinate lower-center
     * @public
     *
     * @returns {Vector2}
     */
    getCenterBottom: function() { return new dot.Vector2( this.getCenterX(), this.maxY ); },
    get centerBottom() { return this.getCenterBottom(); },

    /**
     * The point (maxX, maxY), in the UI-coordinate lower-right
     * @public
     *
     * @returns {Vector2}
     */
    getRightBottom: function() { return new dot.Vector2( this.maxX, this.maxY ); },
    get rightBottom() { return this.getRightBottom(); },

    /**
     * The squared distance from the input point to the point closest to it inside the bounding box.
     * @public
     *
     * @param {Vector2} point
     * @returns {number}
     */
    minimumDistanceToPointSquared: function( point ) {
      var closeX = point.x < this.minX ? this.minX : ( point.x > this.maxX ? this.maxX : null );
      var closeY = point.y < this.minY ? this.minY : ( point.y > this.maxY ? this.maxY : null );
      var d;
      if ( closeX === null && closeY === null ) {
        // inside, or on the boundary
        return 0;
      }
      else if ( closeX === null ) {
        // vertically directly above/below
        d = closeY - point.y;
        return d * d;
      }
      else if ( closeY === null ) {
        // horizontally directly to the left/right
        d = closeX - point.x;
        return d * d;
      }
      else {
        // corner case
        var dx = closeX - point.x;
        var dy = closeY - point.y;
        return dx * dx + dy * dy;
      }
    },

    /**
     * The squared distance from the input point to the point furthest from it inside the bounding box.
     * @public
     *
     * @param {Vector2} point
     * @returns {number}
     */
    maximumDistanceToPointSquared: function( point ) {
      var x = point.x > this.getCenterX() ? this.minX : this.maxX;
      var y = point.y > this.getCenterY() ? this.minY : this.maxY;
      x -= point.x;
      y -= point.y;
      return x * x + y * y;
    },

    /**
     * Debugging string for the bounds.
     * @public
     *
     * @returns {string}
     */
    toString: function() {
      return '[x:(' + this.minX + ',' + this.maxX + '),y:(' + this.minY + ',' + this.maxY + ')]';
    },

    /*---------------------------------------------------------------------------*
     * Immutable operations
     *---------------------------------------------------------------------------*/

    /**
     * The smallest bounds that contains both this bounds and the input bounds, returned as a copy.
     * @public
     *
     * This is the immutable form of the function includeBounds(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {Bounds2} bounds
     * @returns {Bounds2}
     */
    union: function( bounds ) {
      return new Bounds2(
        Math.min( this.minX, bounds.minX ),
        Math.min( this.minY, bounds.minY ),
        Math.max( this.maxX, bounds.maxX ),
        Math.max( this.maxY, bounds.maxY )
      );
    },

    /**
     * The smallest bounds that is contained by both this bounds and the input bounds, returned as a copy.
     * @public
     *
     * This is the immutable form of the function constrainBounds(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {Bounds2} bounds
     * @returns {Bounds2}
     */
    intersection: function( bounds ) {
      return new Bounds2(
        Math.max( this.minX, bounds.minX ),
        Math.max( this.minY, bounds.minY ),
        Math.min( this.maxX, bounds.maxX ),
        Math.min( this.maxY, bounds.maxY )
      );
    },
    // TODO: difference should be well-defined, but more logic is needed to compute

    /**
     * The smallest bounds that contains this bounds and the point (x,y), returned as a copy.
     * @public
     *
     * This is the immutable form of the function addCoordinates(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    withCoordinates: function( x, y ) {
      return new Bounds2(
        Math.min( this.minX, x ),
        Math.min( this.minY, y ),
        Math.max( this.maxX, x ),
        Math.max( this.maxY, y )
      );
    },

    /**
     * The smallest bounds that contains this bounds and the input point, returned as a copy.
     * @public
     *
     * This is the immutable form of the function addPoint(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {Vector2} point
     * @returns {Bounds2}
     */
    withPoint: function( point ) {
      return this.withCoordinates( point.x, point.y );
    },

    /**
     * Returns the smallest bounds that contains both this bounds and the x value provided.
     * @public
     *
     * This is the immutable form of the function addX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    withX: function( x ) {
      return this.copy().addX( x );
    },

    /**
     * Returns the smallest bounds that contains both this bounds and the y value provided.
     * @public
     *
     * This is the immutable form of the function addY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    withY: function( y ) {
      return this.copy().addY( y );
    },

    /**
     * A copy of this bounds, with minX replaced with the input.
     * @public
     *
     * This is the immutable form of the function setMinX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} minX
     * @returns {Bounds2}
     */
    withMinX: function( minX ) {
      return new Bounds2( minX, this.minY, this.maxX, this.maxY );
    },

    /**
     * A copy of this bounds, with minY replaced with the input.
     * @public
     *
     * This is the immutable form of the function setMinY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} minY
     * @returns {Bounds2}
     */
    withMinY: function( minY ) {
      return new Bounds2( this.minX, minY, this.maxX, this.maxY );
    },

    /**
     * A copy of this bounds, with maxX replaced with the input.
     * @public
     *
     * This is the immutable form of the function setMaxX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} maxX
     * @returns {Bounds2}
     */
    withMaxX: function( maxX ) {
      return new Bounds2( this.minX, this.minY, maxX, this.maxY );
    },

    /**
     * A copy of this bounds, with maxY replaced with the input.
     * @public
     *
     * This is the immutable form of the function setMaxY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} maxY
     * @returns {Bounds2}
     */
    withMaxY: function( maxY ) {
      return new Bounds2( this.minX, this.minY, this.maxX, maxY );
    },

    /**
     * A copy of this bounds, with the minimum values rounded down to the nearest integer, and the maximum values
     * rounded up to the nearest integer. This causes the bounds to expand as necessary so that its boundaries
     * are integer-aligned.
     * @public
     *
     * This is the immutable form of the function roundOut(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @returns {Bounds2}
     */
    roundedOut: function() {
      return new Bounds2(
        Math.floor( this.minX ),
        Math.floor( this.minY ),
        Math.ceil( this.maxX ),
        Math.ceil( this.maxY )
      );
    },

    /**
     * A copy of this bounds, with the minimum values rounded up to the nearest integer, and the maximum values
     * rounded down to the nearest integer. This causes the bounds to contract as necessary so that its boundaries
     * are integer-aligned.
     * @public
     *
     * This is the immutable form of the function roundIn(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @returns {Bounds2}
     */
    roundedIn: function() {
      return new Bounds2(
        Math.ceil( this.minX ),
        Math.ceil( this.minY ),
        Math.floor( this.maxX ),
        Math.floor( this.maxY )
      );
    },

    /**
     * A bounding box (still axis-aligned) that contains the transformed shape of this bounds, applying the matrix as
     * an affine transformation.
     * @public
     *
     * NOTE: bounds.transformed( matrix ).transformed( inverse ) may be larger than the original box, if it includes
     * a rotation that isn't a multiple of $\pi/2$. This is because the returned bounds may expand in area to cover
     * ALL of the corners of the transformed bounding box.
     *
     * This is the immutable form of the function transform(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {Matrix3} matrix
     * @returns {Bounds2}
     */
    transformed: function( matrix ) {
      return this.copy().transform( matrix );
    },

    /**
     * A bounding box that is expanded on all sides by the specified amount.)
     * @public
     *
     * This is the immutable form of the function dilate(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} d
     * @returns {Bounds2}
     */
    dilated: function( d ) {
      return new Bounds2( this.minX - d, this.minY - d, this.maxX + d, this.maxY + d );
    },

    /**
     * A bounding box that is expanded horizontally (on the left and right) by the specified amount.
     * @public
     *
     * This is the immutable form of the function dilateX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    dilatedX: function( x ) {
      return new Bounds2( this.minX - x, this.minY, this.maxX + x, this.maxY );
    },

    /**
     * A bounding box that is expanded vertically (on the top and bottom) by the specified amount.
     * @public
     *
     * This is the immutable form of the function dilateY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    dilatedY: function( y ) {
      return new Bounds2( this.minX, this.minY - y, this.maxX, this.maxY + y );
    },

    /**
     * A bounding box that is expanded on all sides, with different amounts of expansion horizontally and vertically.
     * Will be identical to the bounds returned by calling bounds.dilatedX( x ).dilatedY( y ).
     * @public
     *
     * This is the immutable form of the function dilateXY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x - Amount to dilate horizontally (for each side)
     * @param {number} y - Amount to dilate vertically (for each side)
     * @returns {Bounds2}
     */
    dilatedXY: function( x, y ) {
      return new Bounds2( this.minX - x, this.minY - y, this.maxX + x, this.maxY + y );
    },

    /**
     * A bounding box that is contracted on all sides by the specified amount.
     * @public
     *
     * This is the immutable form of the function erode(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} amount
     * @returns {Bounds2}
     */
    eroded: function( amount ) { return this.dilated( -amount ); },

    /**
     * A bounding box that is contracted horizontally (on the left and right) by the specified amount.
     * @public
     *
     * This is the immutable form of the function erodeX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    erodedX: function( x ) { return this.dilatedX( -x ); },

    /**
     * A bounding box that is contracted vertically (on the top and bottom) by the specified amount.
     * @public
     *
     * This is the immutable form of the function erodeY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    erodedY: function( y ) { return this.dilatedY( -y ); },

    /**
     * A bounding box that is contracted on all sides, with different amounts of contraction horizontally and vertically.
     * @public
     *
     * This is the immutable form of the function erodeXY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x - Amount to erode horizontally (for each side)
     * @param {number} y - Amount to erode vertically (for each side)
     * @returns {Bounds2}
     */
    erodedXY: function( x, y ) { return this.dilatedXY( -x, -y ); },

    /**
     * A bounding box that is expanded by a specific amount on all sides (or if some offsets are negative, will contract
     * those sides).
     * @public
     *
     * This is the immutable form of the function offset(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} left - Amount to expand to the left (subtracts from minX)
     * @param {number} top - Amount to expand to the top (subtracts from minY)
     * @param {number} right - Amount to expand to the right (adds to maxX)
     * @param {number} bottom - Amount to expand to the bottom (adds to maxY)
     * @returns {Bounds2}
     */
    withOffsets: function( left, top, right, bottom ) {
      return new Bounds2( this.minX - left, this.minY - top, this.maxX + right, this.maxY + bottom );
    },

    /**
     * Our bounds, translated horizontally by x, returned as a copy.
     * @public
     *
     * This is the immutable form of the function shiftX(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    shiftedX: function( x ) {
      return new Bounds2( this.minX + x, this.minY, this.maxX + x, this.maxY );
    },

    /**
     * Our bounds, translated vertically by y, returned as a copy.
     * @public
     *
     * This is the immutable form of the function shiftY(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    shiftedY: function( y ) {
      return new Bounds2( this.minX, this.minY + y, this.maxX, this.maxY + y );
    },

    /**
     * Our bounds, translated by (x,y), returned as a copy.
     * @public
     *
     * This is the immutable form of the function shift(). This will return a new bounds, and will not modify
     * this bounds.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    shifted: function( x, y ) {
      return new Bounds2( this.minX + x, this.minY + y, this.maxX + x, this.maxY + y );
    },

    /**
     * Returns an interpolated value of this bounds and the argument.
     * @public
     *
     * @param {Bounds2} bounds
     * @param {number} ratio - 0 will result in a copy of `this`, 1 will result in bounds, and in-between controls the
     *                         amount of each.
     */
    blend: function( bounds, ratio ) {
      var t = 1 - ratio;
      return new Bounds2(
        t * this.minX + ratio * bounds.minX,
        t * this.minY + ratio * bounds.minY,
        t * this.maxX + ratio * bounds.maxX,
        t * this.maxY + ratio * bounds.maxY
      );
    },

    /*---------------------------------------------------------------------------*
     * Mutable operations
     *
     * All mutable operations should call one of the following:
     *   setMinMax, setMinX, setMinY, setMaxX, setMaxY
     *---------------------------------------------------------------------------*/

    /**
     * Modifies this bounds so that it contains both its original bounds and the input point (x,y).
     * @public
     *
     * This is the mutable form of the function withCoordinates(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    addCoordinates: function( x, y ) {
      return this.setMinMax(
        Math.min( this.minX, x ),
        Math.min( this.minY, y ),
        Math.max( this.maxX, x ),
        Math.max( this.maxY, y )
      );
    },

    /**
     * Modifies this bounds so that it contains both its original bounds and the input point.
     * @public
     *
     * This is the mutable form of the function withPoint(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {Vector2} point
     * @returns {Bounds2}
     */
    addPoint: function( point ) {
      return this.addCoordinates( point.x, point.y );
    },

    /**
     * Modifies this bounds so that it is guaranteed to include the given x value (if it didn't already). If the x value
     * was already contained, nothing will be done.
     * @public
     *
     * This is the mutable form of the function withX(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    addX: function( x ) {
      this.minX = Math.min( x, this.minX );
      this.maxX = Math.max( x, this.maxX );
      return this;
    },

    /**
     * Modifies this bounds so that it is guaranteed to include the given y value (if it didn't already). If the y value
     * was already contained, nothing will be done.
     * @public
     *
     * This is the mutable form of the function withY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    addY: function( y ) {
      this.minY = Math.min( y, this.minY );
      this.maxY = Math.max( y, this.maxY );
      return this;
    },

    /**
     * Modifies this bounds so that its boundaries are integer-aligned, rounding the minimum boundaries down and the
     * maximum boundaries up (expanding as necessary).
     * @public
     *
     * This is the mutable form of the function roundedOut(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @returns {Bounds2}
     */
    roundOut: function() {
      return this.setMinMax(
        Math.floor( this.minX ),
        Math.floor( this.minY ),
        Math.ceil( this.maxX ),
        Math.ceil( this.maxY )
      );
    },

    /**
     * Modifies this bounds so that its boundaries are integer-aligned, rounding the minimum boundaries up and the
     * maximum boundaries down (contracting as necessary).
     * @public
     *
     * This is the mutable form of the function roundedIn(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @returns {Bounds2}
     */
    roundIn: function() {
      return this.setMinMax(
        Math.ceil( this.minX ),
        Math.ceil( this.minY ),
        Math.floor( this.maxX ),
        Math.floor( this.maxY )
      );
    },

    /**
     * Modifies this bounds so that it would fully contain a transformed version if its previous value, applying the
     * matrix as an affine transformation.
     * @public
     *
     * NOTE: bounds.transform( matrix ).transform( inverse ) may be larger than the original box, if it includes
     * a rotation that isn't a multiple of $\pi/2$. This is because the bounds may expand in area to cover
     * ALL of the corners of the transformed bounding box.
     *
     * This is the mutable form of the function transformed(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {Matrix3} matrix
     * @returns {Bounds2}
     */
    transform: function( matrix ) {
      // if we contain no area, no change is needed
      if ( this.isEmpty() ) {
        return this;
      }

      // optimization to bail for identity matrices
      if ( matrix.isIdentity() ) {
        return this;
      }

      var minX = this.minX;
      var minY = this.minY;
      var maxX = this.maxX;
      var maxY = this.maxY;
      this.set( dot.Bounds2.NOTHING );

      // using mutable vector so we don't create excessive instances of Vector2 during this
      // make sure all 4 corners are inside this transformed bounding box

      this.addPoint( matrix.multiplyVector2( scratchVector2.setXY( minX, minY ) ) );
      this.addPoint( matrix.multiplyVector2( scratchVector2.setXY( minX, maxY ) ) );
      this.addPoint( matrix.multiplyVector2( scratchVector2.setXY( maxX, minY ) ) );
      this.addPoint( matrix.multiplyVector2( scratchVector2.setXY( maxX, maxY ) ) );
      return this;
    },

    /**
     * Expands this bounds on all sides by the specified amount.
     * @public
     *
     * This is the mutable form of the function dilated(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} d
     * @returns {Bounds2}
     */
    dilate: function( d ) {
      return this.setMinMax( this.minX - d, this.minY - d, this.maxX + d, this.maxY + d );
    },

    /**
     * Expands this bounds horizontally (left and right) by the specified amount.
     * @public
     *
     * This is the mutable form of the function dilatedX(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    dilateX: function( x ) {
      return this.setMinMax( this.minX - x, this.minY, this.maxX + x, this.maxY );
    },

    /**
     * Expands this bounds vertically (top and bottom) by the specified amount.
     * @public
     *
     * This is the mutable form of the function dilatedY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    dilateY: function( y ) {
      return this.setMinMax( this.minX, this.minY - y, this.maxX, this.maxY + y );
    },

    /**
     * Expands this bounds independently in the horizontal and vertical directions. Will be equal to calling
     * bounds.dilateX( x ).dilateY( y ).
     * @public
     *
     * This is the mutable form of the function dilatedXY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    dilateXY: function( x, y ) {
      return this.setMinMax( this.minX - x, this.minY - y, this.maxX + x, this.maxY + y );
    },

    /**
     * Contracts this bounds on all sides by the specified amount.
     * @public
     *
     * This is the mutable form of the function eroded(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} d
     * @returns {Bounds2}
     */
    erode: function( d ) { return this.dilate( -d ); },

    /**
     * Contracts this bounds horizontally (left and right) by the specified amount.
     * @public
     *
     * This is the mutable form of the function erodedX(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    erodeX: function( x ) { return this.dilateX( -x ); },

    /**
     * Contracts this bounds vertically (top and bottom) by the specified amount.
     * @public
     *
     * This is the mutable form of the function erodedY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    erodeY: function( y ) { return this.dilateY( -y ); },

    /**
     * Contracts this bounds independently in the horizontal and vertical directions. Will be equal to calling
     * bounds.erodeX( x ).erodeY( y ).
     * @public
     *
     * This is the mutable form of the function erodedXY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    erodeXY: function( x, y ) { return this.dilateXY( -x, -y ); },

    /**
     * Expands this bounds independently for each side (or if some offsets are negative, will contract those sides).
     * @public
     *
     * This is the mutable form of the function withOffsets(). This will mutate (change) this bounds, in addition to
     * returning this bounds itself.
     *
     * @param {number} left - Amount to expand to the left (subtracts from minX)
     * @param {number} top - Amount to expand to the top (subtracts from minY)
     * @param {number} right - Amount to expand to the right (adds to maxX)
     * @param {number} bottom - Amount to expand to the bottom (adds to maxY)
     * @returns {Bounds2}
     */
    offset: function( left, top, right, bottom ) {
      return new Bounds2( this.minX - left, this.minY - top, this.maxX + right, this.maxY + bottom );
    },

    /**
     * Translates our bounds horizontally by x.
     * @public
     *
     * This is the mutable form of the function shiftedX(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @returns {Bounds2}
     */
    shiftX: function( x ) {
      return this.setMinMax( this.minX + x, this.minY, this.maxX + x, this.maxY );
    },

    /**
     * Translates our bounds vertically by y.
     * @public
     *
     * This is the mutable form of the function shiftedY(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} y
     * @returns {Bounds2}
     */
    shiftY: function( y ) {
      return this.setMinMax( this.minX, this.minY + y, this.maxX, this.maxY + y );
    },

    /**
     * Translates our bounds by (x,y).
     * @public
     *
     * This is the mutable form of the function shifted(). This will mutate (change) this bounds, in addition to returning
     * this bounds itself.
     *
     * @param {number} x
     * @param {number} y
     * @returns {Bounds2}
     */
    shift: function( x, y ) {
      return this.setMinMax( this.minX + x, this.minY + y, this.maxX + x, this.maxY + y );
    },

    /**
     * Find a point in the bounds closest to the specified point.
     * @public
     *
     * @param {number} x - X coordinate of the point to test.
     * @param {number} y - Y coordinate of the point to test.
     * @param {Vector2} [result] - Vector2 that can store the return value to avoid allocations.
     * @returns {Vector2}
     */
    getClosestPoint: function( x, y, result ) {
      if ( result ) {
        result.setXY( x, y );
      }
      else {
        result = new dot.Vector2( x, y );
      }
      if ( result.x < this.minX ) { result.x = this.minX; }
      if ( result.x > this.maxX ) { result.x = this.maxX; }
      if ( result.y < this.minY ) { result.y = this.minY; }
      if ( result.y > this.maxY ) { result.y = this.maxY; }
      return result;
    }
  }, {
    /**
     * Returns a new Bounds2 object, with the familiar rectangle construction with x, y, width, and height.
     * @public
     *
     * @param {number} x - The minimum value of X for the bounds.
     * @param {number} y - The minimum value of Y for the bounds.
     * @param {number} width - The width (maxX - minX) of the bounds.
     * @param {number} height - The height (maxY - minY) of the bounds.
     * @returns {Bounds2}
     */
    rect: function( x, y, width, height ) {
      return new Bounds2( x, y, x + width, y + height );
    },

    /**
     * Returns a new Bounds2 object that only contains the specified point (x,y). Useful for being dilated to form a
     * bounding box around a point. Note that the bounds will not be "empty" as it contains (x,y), but it will have
     * zero area. The x and y coordinates can be specified by numbers or with at Vector2
     * @public
     *
     * @param {number|Vector2} x
     * @param [number] y
     * @returns {Bounds2}
     */
    point: function( x, y ) {
      if ( x instanceof dot.Vector2 ) {
        var p = x;
        return new Bounds2( p.x, p.y, p.x, p.y );
      }
      else {
        return new Bounds2( x, y, x, y );
      }
    }
  } );

  return Bounds2;
} );
