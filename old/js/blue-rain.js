import Camera3 from './Camera3.js';
import Camera4 from './Camera4.js';
import Compositor from './Compositor.js';
import Projection from './Projection.js';
import ProjectionSlicer from './ProjectionSlicer.js';
import ShaderProgram from './ShaderProgram.js';
import SingleSliceCompositor from './SingleSliceCompositor.js';
import Slicer from './Slicer.js';
import Snippet from './Snippet.js';
import WeightedCompositor from './WeightedCompositor.js';

var layoutBounds = new dot.Bounds2( 0, 0, 1024, 768 );
var viewGlobalMatrix = new dot.Matrix3().setTo32Bit();
var globalViewMatrix = new dot.Matrix3().setTo32Bit();
var viewClipMatrix = new dot.Matrix3().setTo32Bit();
var scratchMatrix1 = new dot.Matrix3().setTo32Bit();

//TODO: don't do this
window.blueRain = {
  Camera3,
  Camera4,
  Compositor,
  Projection,
  ProjectionSlicer,
  ShaderProgram,
  SingleSliceCompositor,
  Slicer,
  Snippet,
  WeightedCompositor
};

window.assertions.enableAssert();

var canvas = document.createElement( 'canvas' );
function resizeCanvas() {
  // TODO: high DPI
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}
window.addEventListener( 'resize', resizeCanvas );
resizeCanvas();
document.body.appendChild( canvas );
canvas.style.position = 'absolute';
canvas.style.left = '0';
canvas.style.top = '0';

// initialize our scene
var scene = new scenery.Node();
var display = new scenery.Display( scene );
display.initializeEvents(); // sets up listeners on the document with preventDefault(), and forwards those events to our scene
display.resizeOnWindowResize(); // the scene gets resized to the full screen size
document.body.appendChild( display.domElement );

var gl = canvas.getContext( 'webgl2' );
if ( !gl ) {
  throw new Error( 'Could not get a WebGL2 context.' );
}
if ( gl.getExtension( 'EXT_color_buffer_float' ) === null ) {
  throw new Error( 'Required WebGL2 EXT_color_buffer_float extension could not be loaded' );
}
blueRain.gl = gl;

var camera3 = new Camera3();

var projectionSlicer = new ProjectionSlicer( gl, camera3, `
vec4 sample3( vec3 worldPoint ) {
  float outer = 2.0;
  float inner = 1.0;
  vec4 color;
  if ( abs( worldPoint.x ) < outer && abs( worldPoint.y ) < outer && abs( worldPoint.z ) < outer && ( abs( worldPoint.x ) > inner || abs( worldPoint.y ) > inner || abs( worldPoint.z ) > inner ) ) {
    if ( abs( worldPoint.x ) > inner && abs( worldPoint.y ) > inner && abs( worldPoint.z ) > inner ) {
      color = vec4( worldPoint.x > 0.0 ? 1 : 0, worldPoint.y > 0.0 ? 1 : 0, worldPoint.z > 0.0 ? 1 : 0, 1 );
    }
    else {
      float ratio = ( worldPoint.z / 1.9 ) * 0.5 + 0.5;
      ratio = ratio > 0.5 ? 1.0 : 0.0;
      color = 2.0 * vec4( ratio, ratio * 0.5 + 0.5, 1.0 - ratio, 1 );
    }
  }
  else {
    color = vec4( 0, 0, 0, 1 );
  }
  float fuzz = 0.05;
  bool closeX = abs( mod( worldPoint.x, 1.0 ) + 0.5 ) - 0.5 < fuzz;
  bool closeY = abs( mod( worldPoint.y, 1.0 ) + 0.5 ) - 0.5 < fuzz;
  bool closeZ = abs( mod( worldPoint.z, 1.0 ) + 0.5 ) - 0.5 < fuzz;
  float multiplier = 3.0;
  float noMultiplier = 0.5;
  if ( closeX && closeY ) {
    color.xyz *= multiplier;
  }
  else if ( closeX && closeZ ) {
    color.xyz *= multiplier;
  }
  else if ( closeY && closeZ ) {
    color.xyz *= multiplier;
  }
  else {
    color.xyz *= noMultiplier;
  }
  if ( closeX ) {
    color.xyz *= multiplier;
  }
  else if ( closeY ) {
    color.xyz *= multiplier;
  }
  else if ( closeZ ) {
    color.xyz *= multiplier;
  }
  else {
    color.xyz *= noMultiplier;
  }
  color.a = 1.0;
  return color;
}
`, [] );
var texture = ShaderProgram.makeNearestTexture( gl, 512, 512, ShaderProgram.TexType.RGBA16F );

var compositor = new WeightedCompositor( gl );

var vertexShaderSource = `#version 300 es
in vec2 a_position;
in vec2 a_texcoord;
out vec2 v_texcoord;
uniform mat3 u_viewClipMatrix;

void main() {
  vec3 ndc = u_viewClipMatrix * vec3( a_position, 1.0 );
  gl_Position = vec4( ndc.xy, 0.0, 1.0 );
  v_texcoord = a_texcoord;
}
`;

var fragmentShaderSource = `#version 300 es
precision mediump float;
in vec2 v_texcoord;
uniform sampler2D u_texture;
out vec4 outColor;

void main() {
  outColor = texture( u_texture, v_texcoord );
  float brightness = 1.0;
  outColor.rgb = brightness * pow( abs( outColor.rgb ), vec3( 1.0 / 2.2 ) );
}
`;

var shaderProgram = new ShaderProgram( gl, vertexShaderSource, fragmentShaderSource, {
  attributes: [ 'a_position', 'a_texcoord' ],
  uniforms: [ 'u_viewClipMatrix', 'u_texture' ]
} );

var vao = gl.createVertexArray();
gl.bindVertexArray( vao );

var positionBuffer = gl.createBuffer();
gl.bindBuffer( gl.ARRAY_BUFFER, positionBuffer );
gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
  0, 0,
  0, 768,
  1024, 0,
  0, 768,
  1024, 768,
  1024, 0
] ), gl.DYNAMIC_DRAW );
gl.vertexAttribPointer( shaderProgram.attributeLocations.a_position, 2, gl.FLOAT, false, 0, 0 );

var texcoordBuffer = gl.createBuffer();
gl.bindBuffer( gl.ARRAY_BUFFER, texcoordBuffer );
gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
  0, 0,
  0, 1,
  1, 0,
  0, 1,
  1, 1,
  1, 0
] ), gl.STATIC_DRAW );
gl.vertexAttribPointer( shaderProgram.attributeLocations.a_texcoord, 2, gl.FLOAT, false, 0, 0 );
gl.bindVertexArray( null );

var lastGlobalPoint;
var scratchMatrix2 = new dot.Matrix3().setTo32Bit();
display.addInputListener( new scenery.PressListener( {
  press: function( event ) {
    lastGlobalPoint = event.pointer.point.copy();
  },
  drag: function( event ) {
    var newGlobalPoint = event.pointer.point.copy();
    var delta = newGlobalPoint.minus( lastGlobalPoint );
    lastGlobalPoint = newGlobalPoint;

    // TODO: Scale according to the display scale size?
    if ( delta.x !== 0 ) {
      scratchMatrix2.setToRotationY( delta.x * 0.003 );
      camera3.rotationMatrix.multiplyMatrix( scratchMatrix2 );
    }
    if ( delta.y !== 0 ) {
      scratchMatrix2.setToRotationX( -delta.y * 0.003 );
      camera3.rotationMatrix.multiplyMatrix( scratchMatrix2 );
    }
  }
} ) );

// var angle = 0;
var rotationTransform = new dot.Transform3( new dot.Matrix3() );
var lastTime = -1;
( function step() {
  window.requestAnimationFrame( step );

  var currentTime = Date.now();
  var elapsedTime = ( lastTime > 0 ) ? ( currentTime - lastTime ) / 1000 : 0;
  lastTime = currentTime;

  var scale = Math.min( gl.drawingBufferWidth / layoutBounds.width, gl.drawingBufferHeight / layoutBounds.height );

  var dx = 0;
  var dy = 0;

  //center vertically
  if ( scale === gl.drawingBufferWidth / layoutBounds.width ) {
    dy = ( gl.drawingBufferHeight / scale - layoutBounds.height ) / 2;
  }

  //center horizontally
  else if ( scale === gl.drawingBufferHeight / layoutBounds.height ) {
    dx = ( gl.drawingBufferWidth / scale - layoutBounds.width ) / 2;
  }

  viewGlobalMatrix.setToScale( scale ).multiplyMatrix( scratchMatrix1.setToTranslation( dx, dy ) );
  globalViewMatrix.set( viewGlobalMatrix ).invert();
  viewClipMatrix.setToScale( 1, -1 ).multiplyMatrix( scratchMatrix1.setToTranslation( -1, -1 ) ).multiplyMatrix( scratchMatrix1.setToScale( 2 / gl.drawingBufferWidth, 2 / gl.drawingBufferHeight ) ).multiplyMatrix( viewGlobalMatrix );

  // TODO: reduce GC
  var spotSize = 400;
  var spacing = 0;
  var leftRight = layoutBounds.centerX - spacing / 2;
  var rightLeft = layoutBounds.centerX + spacing / 2;
  var spotTop = layoutBounds.centerY - spotSize / 2;
  var spotBottom = layoutBounds.centerY + spotSize / 2;
  var leftGlobalBounds = new dot.Bounds2( leftRight - spotSize, spotTop, leftRight, spotBottom ).transform( viewGlobalMatrix ).roundOut();
  var rightGlobalBounds = new dot.Bounds2( rightLeft, spotTop, rightLeft + spotSize, spotBottom ).transform( viewGlobalMatrix ).roundOut();
  var leftViewBounds = leftGlobalBounds.copy().transform( globalViewMatrix );
  var rightViewBounds = rightGlobalBounds.copy().transform( globalViewMatrix );

  gl.viewport( 0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight );

  gl.clearColor( 0, 0, 0, 1 );
  gl.clear( gl.COLOR_BUFFER_BIT );

  function sliceBounds( bounds ) {
    ShaderProgram.updateNearestTexture( gl, texture, bounds.width, bounds.height, ShaderProgram.TexType.RGBA16F );

    var distanceFromCamera = camera3.position.magnitude();
    var paddedBoundingRadius = 3.6;
    var numSlices = 300;

    for ( var i = 0; i < numSlices; i++ ) {
      var ratio = i / ( numSlices - 1 );
      var depth = distanceFromCamera + paddedBoundingRadius * ( 1 - 2 * ratio );
      projectionSlicer.drawProjectionSlice( texture, bounds.width, bounds.height, depth, paddedBoundingRadius, distanceFromCamera );
      compositor.compositeSlice( texture, 1 / numSlices );
    }
  }

  function drawBounds( bounds ) {
    gl.bindBuffer( gl.ARRAY_BUFFER, positionBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
      bounds.minX, bounds.minY,
      bounds.minX, bounds.maxY,
      bounds.maxX, bounds.minY,
      bounds.minX, bounds.maxY,
      bounds.maxX, bounds.maxY,
      bounds.maxX, bounds.minY
    ] ), gl.DYNAMIC_DRAW );
    gl.bindBuffer( gl.ARRAY_BUFFER, null );

    gl.viewport( 0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight );

    gl.bindVertexArray( vao );
    shaderProgram.use();
    gl.activeTexture( gl.TEXTURE0 );
    gl.bindTexture( gl.TEXTURE_2D, compositor.currentTexture );
    gl.uniform1i( shaderProgram.uniformLocations.u_texture, 0 );
    gl.uniformMatrix3fv( shaderProgram.uniformLocations.u_viewClipMatrix, false, viewClipMatrix.entries ); // TODO: don't use entries in the future
    gl.bindVertexArray( vao );

    gl.drawArrays( gl.TRIANGLES, 0, 6 );

    gl.bindVertexArray( null );
    gl.bindTexture( gl.TEXTURE_2D, null );
    shaderProgram.unuse();
    gl.bindVertexArray( null );
  }

  var cameraDistance = 6;
  var mainCameraPosition = camera3.rotationMatrix.timesVector3( dot.v3( 0, 0, -1 ) ).timesScalar( cameraDistance );

  var halfEyeDistance = 0.3;

  camera3.position = mainCameraPosition.plus( camera3.rotationMatrix.timesVector3( dot.v3( halfEyeDistance, 0, 0 ) ) );

  compositor.prepare( leftGlobalBounds.width, leftGlobalBounds.height );
  sliceBounds( leftGlobalBounds );
  drawBounds( leftViewBounds );

  camera3.position = mainCameraPosition.plus( camera3.rotationMatrix.timesVector3( dot.v3( -halfEyeDistance, 0, 0 ) ) );

  compositor.prepare( leftGlobalBounds.width, leftGlobalBounds.height );
  sliceBounds( rightGlobalBounds );
  drawBounds( rightViewBounds );

  // angle += elapsedTime;

  display.updateDisplay();
} )();
