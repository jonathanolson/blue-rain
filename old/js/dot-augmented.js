'use strict';

dot.Vector2.prototype.toVec2 = function() {
  return `vec2(${this.x},${this.y})`;
}

dot.Vector3.prototype.toVec3 = function() {
  return `vec3(${this.x},${this.y},${this.z})`;
}

dot.Vector4.prototype.toVec4 = function() {
  return `vec4(${this.x},${this.y},${this.z},${this.w})`;
}

dot.Vector4.prototype.componentDivide = function( v ) {
  return this.setXYZW( this.x / v.x, this.y / v.y, this.z / v.z, this.w / v.w );
}

dot.Vector4.prototype.componentTimes = function( v ) {
  return new Vector4( this.x / v.x, this.y / v.y, this.z / v.z, this.w / v.w );
}

/**
 * @param {Vector4} position - the ray's point of origin
 * @param {Vector4} direction - the ray's unit direction vector
 * @constructor
 */
function Ray4( position, direction ) {

  this.position = position;  // @public (read-only)
  this.direction = direction; // @public (read-only)

  assert && assert( Math.abs( direction.magnitude() - 1 ) < 0.01 , 'the direction must be a unit vector');
}

dot.register( 'Ray4', Ray4 );

Ray4.prototype = {
  constructor: Ray4,

  /**
   * Returns a new Ray that has it origin shifted to a position given by an amount distance*this.direction.
   * @public
   * @param {number} distance
   * @returns {Ray4}
   */
  shifted: function( distance ) {
    return new Ray4( this.pointAtDistance( distance ), this.direction );
  },

  /**
   * Returns a position that is a distance 'distance' along the ray.
   * @public
   * @param {number} distance
   * @returns {Vector4}
   */
  pointAtDistance: function( distance ) {
    return this.position.plus( this.direction.timesScalar( distance ) );
  },

  /**
   * Returns the distance of this ray to a plane
   * @public
   * @param {Plane3} plane
   * @returns {number}
   */
  distanceToPlane: function( plane ) {
    return ( plane.distance - this.position.dot( plane.normal ) ) / this.direction.dot( plane.normal );
  },

  /**
   * Returns the attributes of this ray into a string
   * @public
   * @returns {string}
   */
  toString: function() {
    return this.position.toString() + ' => ' + this.direction.toString();
  }
};
