
import ShaderProgram from './ShaderProgram.js';

class Slicer {
  /**
   * @param {string} sliceSample TODO: doc
   * @param {Array.<string>} uniforms
   */
  constructor( gl, sliceSample, uniforms ) {
    // @private
    this.sliceSample = sliceSample;
    this.uniforms = uniforms;
    
    this.initialize( gl );
  }

  /** 
   * @param {WebGL2RenderingContext} gl
   */
  initialize( gl ) {
    // @private
    this.gl = gl;

    // @protected {ShaderProgram}
    this.shaderProgram = new ShaderProgram( gl, `#version 300 es
in vec2 a_position;
out vec2 v_position;

void main() {
  v_position = a_position;
  gl_Position = vec4( a_position, 0, 1 );
}
`, `#version 300 es
precision mediump float;
in vec2 v_position;

${this.sliceSample}

void main() {
  sliceSample( v_position );
}
`, {
  attributes: [ 'a_position' ],
  uniforms: this.uniforms
} );

    // @private
    this.vao = gl.createVertexArray();
    gl.bindVertexArray( this.vao );

    // @private
    this.positionBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.positionBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
      -1, -1,
      -1, 1,
      1, -1,
      1, 1
    ] ), gl.STATIC_DRAW );
    gl.vertexAttribPointer( this.shaderProgram.attributeLocations.a_position, 2, gl.FLOAT, false, 0, 0 );

    gl.bindVertexArray( null );

    // @private
    this.framebuffer = gl.createFramebuffer();
  }

  draw( outputTextures, width, height ) {
    const gl = this.gl;

    gl.bindVertexArray( this.vao );
    this.shaderProgram.use();

    this.beforeDraw( gl );

    gl.bindFramebuffer( gl.FRAMEBUFFER, this.framebuffer );
    for ( var i = 0; i < outputTextures.length; i++ ) {
      gl.framebufferTexture2D( gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0 + i, gl.TEXTURE_2D, outputTextures[ i ], 0 );
    }
    gl.drawBuffers( [
      // TODO
      gl.COLOR_ATTACHMENT0
    ] );
    gl.viewport( 0, 0, width, height );

    // TODO: not needed, we overwrite everything?
    gl.clearColor( 0, 0, 1, 1 );
    gl.clear( gl.COLOR_BUFFER_BIT );

    gl.drawArrays( gl.TRIANGLE_STRIP, 0, 4 );
    gl.bindFramebuffer( gl.FRAMEBUFFER, null );
    this.shaderProgram.unuse();
    gl.bindVertexArray( null );
  }

  beforeDraw( gl ) {

  }

  dispose() {
    this.shaderProgram.dispose();
    this.gl.deleteVertexArray( this.vao );
    this.gl.deleteBuffer( this.positionBuffer );
    this.gl.deleteFramebuffer( this.framebuffer );
  }
}

export default Slicer;
