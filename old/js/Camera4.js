
import Projection from './Projection.js';

class Camera4 {
  /**
   *
   */
  constructor() {
    // @public {Vector4}
    this.position = new dot.Vector4( 0, 0, 0 );

    // @public {Matrix4}
    this.rotationMatrix = new dot.Matrix4();

    // @public {Property.<Projection>}
    this.projectionProperty = new axon.Property( Projection.PERSPECTIVE_4D );
  }
}

export default Camera4;
