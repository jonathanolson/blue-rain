
import SingleSliceCompositor from './SingleSliceCompositor.js';

class WeightedCompositor extends SingleSliceCompositor {
  /**
   */
  constructor( gl ) {
    super( gl, `
uniform float u_slice_weight;
vec4 compositeColors( vec4 sliceColor, vec4 previousColor ) {
  return previousColor + sliceColor * u_slice_weight;
}
`, [ 'u_slice_weight' ] );
  }

  compositeSlice( texture, sliceWeight ) {
    // @private
    this.sliceWeight = sliceWeight;

    super.compositeSlice( texture );
  }

  beforeDraw( gl ) {
    super.beforeDraw( gl );

    gl.uniform1f( this.shaderProgram.uniformLocations.u_slice_weight, this.sliceWeight );
  }
}

export default WeightedCompositor;
