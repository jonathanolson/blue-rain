
import Compositor from './Compositor.js';

class SingleSliceCompositor extends Compositor {
  /**
   * @param {string} compositeColors
   * @param {Array.<string>} uniforms
   */
  constructor( gl, compositeColors, uniforms ) {
    super( gl, `
uniform sampler2D u_slice;
${compositeColors}
vec4 composite( vec2 position, vec4 oldColor ) {
  return compositeColors( texture( u_slice, position * 0.5 + 0.5 ), oldColor );
}
`, uniforms.concat( 'u_slice' ) );
  }

  compositeSlice( texture ) {
    // @private
    this.sliceTexture = texture;

    super.composite();
  }

  beforeDraw( gl ) {
    super.beforeDraw( gl );

    gl.activeTexture( gl.TEXTURE1 );
    gl.bindTexture( gl.TEXTURE_2D, this.sliceTexture );
    gl.uniform1i( this.shaderProgram.uniformLocations.u_slice, 1 );
  }
}

export default SingleSliceCompositor;
