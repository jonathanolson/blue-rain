
import Projection from './Projection.js';

class Camera3 {
  /**
   *
   */
  constructor() {
    // @public {Vector3}
    this.position = new dot.Vector3( 0, 0, 0 );

    // @public {Matrix3}
    this.rotationMatrix = new dot.Matrix3().setTo32Bit();

    // @public {Property.<Projection>}
    this.projectionProperty = new axon.Property( Projection.PERSPECTIVE_3D );
  }
}

export default Camera3;
