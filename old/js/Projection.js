
import Snippet from './Snippet.js';

class Projection {
  /**
   * @param {Object} options
   */
  constructor( options ) {
    _.extend( this, options );
  }
}

function tangentPointOnCircle( radius, px ) {
  var magSq = px * px;
  var discriminant = magSq - radius * radius;
  var sqrt = Math.sqrt( discriminant );
  return new dot.Vector2(
    ( radius / magSq ) * ( radius * px ),
    ( radius / magSq ) * ( px * sqrt )
  );
}

Projection.PERSPECTIVE_3D = new Projection( {
  project( pos2 ) {
    return new dot.Ray3( dot.Vector3.ZERO, new dot.Vector3( pos2.x, pos2.y, 1 ).normalize() );
  },
  snippet: Snippet.projectPerspective3,
  functionName: 'projectPerspective3',
  minToContainSphere( radius, distance ) {
    var point = tangentPointOnCircle( radius, distance );
    var value = point.minus( new dot.Vector2( distance, 0 ) ).normalize().y;
    return value / Math.sqrt( 1 - value * value );
  },
  type: 0
} );

Projection.PERSPECTIVE_4D = new Projection( {
  project( pos3 ) {
    return new dot.Ray4( dot.Vector3.ZERO, new dot.Vector4( pos3.x, pos3.y, pos3.z, 1 ).normalize() );
  },
  snippet: Snippet.projectPerspective4,
  functionName: 'projectPerspective4',
  minToContain3Sphere( radius, distance ) {
    var point = tangentPointOnCircle( radius, distance );
    var value = point.minus( new dot.Vector2( distance, 0 ) ).normalize().y;
    return value / Math.sqrt( 1 - value * value );
  },
  type: 0
} );

Projection.STEREOGRAPHIC_3D = new Projection( {
  project( pos2 ) {
    return new dot.Ray3( dot.Vector3.ZERO, new dot.Vector3( 4 * pos2.x, 4 * pos2.y, 4 - pos2.magnitudeSquared() ).normalize() );
  },
  snippet: Snippet.projectStereographic3,
  functionName: 'projectStereographic3',
  minToContainSphere( radius, distance ) {
    var point = tangentPointOnCircle( radius, distance );
    var value = point.minus( new dot.Vector2( distance, 0 ) ).normalize().y;
    return ( 2 - 2 * Math.sqrt( 1 - value * value ) ) / value;
  },
  type: 1
} );

Projection.STEREOGRAPHIC_4D = new Projection( {
  project( pos3 ) {
    return new dot.Ray4( dot.Vector3.ZERO, new dot.Vector4( 4 * pos3.x, 4 * pos3.y, 4 * pos3.z, 4 - pos3.magnitudeSquared() ).normalize() );
  },
  snippet: Snippet.projectStereographic4,
  functionName: 'projectStereographic4',
  minToContain3Sphere( radius, distance ) {
    var point = tangentPointOnCircle( radius, distance );
    var value = point.minus( new dot.Vector2( distance, 0 ) ).normalize().y;
    return ( 2 - 2 * Math.sqrt( 1 - value * value ) ) / value;
  },
  type: 1
} );

Projection.ORTHOGRAPHIC_3D = new Projection( {
  project( pos2 ) {
    return new dot.Ray3( new dot.Vector3( pos2.x, pos2.y, 0 ), dot.Vector3.Z_UNIT );
  },
  snippet: Snippet.projectOrthographic3,
  functionName: 'projectOrthographic3',
  minToContainSphere( radius, distance ) {
    return radius;
  },
  type: 2
} );

Projection.ORTHOGRAPHIC_4D = new Projection( {
  project( pos3 ) {
    return new dot.Ray4( new dot.Vector4( pos3.x, pos3.y, 0 ), dot.Vector4.W_UNIT );
  },
  snippet: Snippet.projectOrthographic4,
  functionName: 'projectOrthographic4',
  minToContain3Sphere( radius, distance ) {
    return radius;
  },
  type: 2
} );

export default Projection;
