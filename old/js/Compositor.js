
import ShaderProgram from './ShaderProgram.js';

class Compositor {
  /**
   * @param {string} composite
   * @param {Array.<string>} uniforms
   */
  constructor( gl, composite, uniforms ) {
    // @private
    this.composite = composite;
    this.uniforms = uniforms;

    this.initialize( gl );
  }

  /**
   * @param {WebGL2RenderingContext} gl
   */
  initialize( gl ) {
    // @private
    this.gl = gl;

    // @private {ShaderProgram}
    this.shaderProgram = new ShaderProgram( gl, `#version 300 es
in vec2 a_position;
out vec2 v_position;

void main() {
  v_position = a_position;
  gl_Position = vec4( a_position, 0, 1 );
}
`, `#version 300 es
precision mediump float;
in vec2 v_position;
uniform sampler2D u_texture;
layout(location=0) out vec4 outColor;

${this.composite}

void main() {
  outColor = composite( v_position, texture( u_texture, v_position * 0.5 + 0.5 ) );
}
`, {
  attributes: [ 'a_position' ],
  uniforms: this.uniforms.concat( 'u_texture' )
} );

    // @private
    this.vao = gl.createVertexArray();
    gl.bindVertexArray( this.vao );

    // @private
    this.positionBuffer = gl.createBuffer();
    gl.bindBuffer( gl.ARRAY_BUFFER, this.positionBuffer );
    gl.bufferData( gl.ARRAY_BUFFER, new Float32Array( [
      -1, -1,
      -1, 1,
      1, -1,
      1, 1
    ] ), gl.STATIC_DRAW );
    gl.vertexAttribPointer( this.shaderProgram.attributeLocations.a_position, 2, gl.FLOAT, false, 0, 0 );

    gl.bindVertexArray( null );

    // @private
    this.framebuffer = gl.createFramebuffer();

    this.previousTexture = ShaderProgram.makeNearestTexture( gl, 256, 256, ShaderProgram.TexType.RGBA16F );
    this.currentTexture = ShaderProgram.makeNearestTexture( gl, 256, 256, ShaderProgram.TexType.RGBA16F );
  }

  prepare( width, height ) {
    const gl = this.gl;

    // @private
    this.width = width;
    this.height = height;

    ShaderProgram.updateNearestTexture( gl, this.previousTexture, width, height, ShaderProgram.TexType.RGBA16F );
    ShaderProgram.updateNearestTexture( gl, this.currentTexture, width, height, ShaderProgram.TexType.RGBA16F );

    gl.bindFramebuffer( gl.FRAMEBUFFER, this.framebuffer );
    gl.framebufferTexture2D( gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.currentTexture, 0 );
    gl.drawBuffers( [ gl.COLOR_ATTACHMENT0 ] );
    gl.viewport( 0, 0, width, height );

    gl.clearColor( 0, 0, 0, 1 );
    gl.clear( gl.COLOR_BUFFER_BIT );

    gl.bindFramebuffer( gl.FRAMEBUFFER, null );
  }

  composite() {
    const gl = this.gl;

    // Swap
    var tmpTexture = this.currentTexture;
    this.currentTexture = this.previousTexture;
    this.previousTexture = tmpTexture;

    gl.bindVertexArray( this.vao );
    this.shaderProgram.use();
    gl.activeTexture( gl.TEXTURE0 );
    gl.bindTexture( gl.TEXTURE_2D, this.previousTexture );
    gl.uniform1i( this.shaderProgram.uniformLocations.u_texture, 0 );

    this.beforeDraw( gl );

    gl.bindFramebuffer( gl.FRAMEBUFFER, this.framebuffer );
    gl.framebufferTexture2D( gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, this.currentTexture, 0 );
    gl.drawBuffers( [ gl.COLOR_ATTACHMENT0 ] );
    gl.viewport( 0, 0, this.width, this.height );

    // TODO: not needed, we overwrite everything?
    gl.clearColor( 0, 0, 1, 1 );
    gl.clear( gl.COLOR_BUFFER_BIT );

    gl.drawArrays( gl.TRIANGLE_STRIP, 0, 4 );
    gl.bindFramebuffer( gl.FRAMEBUFFER, null );
    this.shaderProgram.unuse();
    gl.bindVertexArray( null );
  }

  beforeDraw( gl ) {

  }

  dispose() {
    const gl = this.gl;

    this.shaderProgram.dispose();
    gl.deleteVertexArray( this.vao );
    gl.deleteBuffer( this.positionBuffer );
    gl.deleteFramebuffer( this.framebuffer );
    gl.deleteTexture( this.previousTexture );
    gl.deleteTexture( this.currentTexture );
  }
}

export default Compositor;
