
import Projection from './Projection.js';
import Slicer from './Slicer.js';
import Snippet from './Snippet.js';

class ProjectionSlicer extends Slicer {
  constructor( gl, camera3, sample3, uniforms ) {
    const projections = [ Projection.PERSPECTIVE_3D, Projection.STEREOGRAPHIC_3D, Projection.ORTHOGRAPHIC_3D ];

    super( gl, `
layout(location=0) out vec4 outColor;
uniform int u_projectionType;
uniform float u_depth;
uniform float u_image_scale;
uniform mat3 u_rotation;
uniform vec3 u_translation;
${new Snippet( '', projections.map( projection => projection.snippet ) ).toString()}
${sample3}
void sliceSample( vec2 p ) {
  vec3 pos;
  vec3 dir;
${projections.map( projection => `
  if ( u_projectionType == ${projection.type} ) {
    ${projection.functionName}( p * u_image_scale, pos, dir );
  }
` ).join( '' )}
  vec3 cameraPoint = pos + dir * u_depth;
  vec3 worldPoint = u_rotation * cameraPoint + u_translation;
  outColor = sample3( worldPoint );
}
`, uniforms.concat( [ 'u_projectionType', 'u_depth', 'u_image_scale', 'u_rotation', 'u_translation' ] ) );

    // @private {Camera3}
    this.camera3 = camera3;
  }

  beforeDraw( gl ) {
    super.beforeDraw( gl );

    gl.uniform1i( this.shaderProgram.uniformLocations.u_projectionType, this.camera3.projectionProperty.value.type );
    gl.uniform1f( this.shaderProgram.uniformLocations.u_depth, this.depth );
    gl.uniform1f( this.shaderProgram.uniformLocations.u_image_scale, this.imageScale );
    gl.uniformMatrix3fv( this.shaderProgram.uniformLocations.u_rotation, false, this.camera3.rotationMatrix.entries ); // TODO: don't use entries in the future
    gl.uniform3f( this.shaderProgram.uniformLocations.u_translation, this.camera3.position.x, this.camera3.position.y, this.camera3.position.z );
  }

  drawProjectionSlice( outputTexture, width, height, depth, boundingRadius, distance ) {
    // @private
    this.depth = depth;

    // @private
    this.imageScale = this.camera3.projectionProperty.value.minToContainSphere( boundingRadius, distance );

    this.draw( [ outputTexture ], width, height );
  }
}

export default ProjectionSlicer;
