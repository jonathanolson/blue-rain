
const acorn = require( 'acorn' );
const escodegen = require( 'escodegen' );
const eslint = require( 'eslint' );
const execute = require( './execute' );
const fs = require( 'fs' );
const babelMinify = require( 'babel-minify' );
const path = require( 'path' );
const rollup = require( 'rollup' );
const rimraf = require( 'rimraf' );
const unassert = require( 'unassert' );

const rootDir = path.resolve( __dirname, '..' );
const cmd = process.argv[ 2 ];

async function clean() {
  return new Promise( ( resolve, reject ) => {
    rimraf( `${rootDir}/build`, err => {
      if ( err ) {
        reject( err );
      }
      else {
        fs.mkdirSync( `${rootDir}/build` );
        resolve();
      }
    } );
  } );
}

async function getJSFiles() {
  return ( await execute( 'find', [ 'src/js', '-type', 'f', '-name', '*.js' ], rootDir ) ).trim().replace( /\r/g, '' ).split( '\n' );
}

function jsFilesToYrioModules( files ) {
  return files.map( file => file.replace( 'src/js/', '' ).replace( '.js', '' ) ).filter( file => {
    return file === 'namespace' ||
           file.startsWith( 'core/' ) ||
           file.startsWith( 'gl/' ) ||
           file.startsWith( 'math/' );
  } );
}

function everythingJS( files ) {
  return jsFilesToYrioModules( files ).map( name => `import ${name.replace( /\//g, '_' )} from '${name}';\n` ).join( '' ) + 'export default namespace;\n';
}

async function lint( files ) {
  console.log( 'linting' );

  const cli = new eslint.CLIEngine( {
    cwd: rootDir,
    configFile: 'config/eslint.config.json',
    cache: true,
    cacheFile: `.eslintcache`
  } );

  const report = cli.executeOnFiles( files );

  const hasError = report.warningCount + report.errorCount > 0;

  if ( hasError ) {
    console.log( cli.getFormatter()( report.results ) );
  }
  else {
    console.log( 'success' );
  }

  return !hasError;
}

async function qunit() {
  let output;
  try {
    output = await execute( 'node', [ 'node_modules/qunit/bin/qunit', 'src/js/tests/*.js' ], rootDir );
    console.log( output );
    console.log( 'tests pass' );
  }
  catch ( e ) {
    console.log( e.stdout );
  }
}

async function pack( file ) {
  const bundle = await rollup.rollup( {
    input: file,
    plugins: [ {
      resolveId( importee, importer ) {
        const rootPath = `../src/js/${importee}.js`;
        const absPath = path.resolve( __dirname, rootPath );
        return fs.existsSync( absPath ) ? absPath : null;
      }
    } ]
  } );
  // has imports, exports, modules

  const { code, map } = await bundle.generate( {
    format: 'umd',
    name: 'yrio'
  } );

  return code;
}

async function stripAssrtions( code ) {
  var ast = acorn.parse( code );
  var modifiedAst = unassert( ast );

  return escodegen.generate( unassert( ast ) );
}

function minify( code ) {
  return babelMinify( code, {
    keepClassName: true
  } ).code;
}

async function build( file, name ) {
  const template = fs.readFileSync( 'src/html/template.html', 'utf8' );

  const js = await pack( file );
  const minJS = minify( await stripAssrtions( js ) );
  const html = template.replace( '{{SRC}}', `${name}.js` ).replace( '{{TITLE}}', name );
  const minHTML = template.replace( '{{SRC}}', `${name}.min.js` ).replace( '{{TITLE}}', name );

  fs.writeFileSync( `${rootDir}/build/${name}.js`, js );
  fs.writeFileSync( `${rootDir}/build/${name}.min.js`, minJS );
  fs.writeFileSync( `${rootDir}/build/${name}.html`, html );
  fs.writeFileSync( `${rootDir}/build/${name}.min.html`, minHTML );

  return name;
}

async function buildYrio( jsFiles ) {
  fs.writeFileSync( 'build/everything.js', everythingJS( jsFiles ) );
  await build( 'build/everything.js', 'yrio' );
}

async function buildSimple( jsFiles ) {
  await build( 'src/js/apps/simple.js', 'simple' );
}

async function buildWebglTest( jsFiles ) {
  await build( 'src/js/apps/webgltest.js', 'webgltest' );
}

( async () => {
  const jsFiles = await getJSFiles();

  // TODO: this is not good for maintenance
  if ( cmd === 'clean' ) {
    await clean();
  }
  else if ( cmd === 'lint' ) {
    await lint( jsFiles );
  }
  else if ( cmd === 'qunit' ) {
    await qunit();
  }
  else if ( cmd === 'yrio' ) {
    await clean();
    await buildYrio( jsFiles );
  }
  else if ( cmd === 'simple' ) {
    await clean();
    await buildSimple( jsFiles );
  }
  else if ( cmd === 'webgltest' ) {
    await clean();
    await buildWebglTest( jsFiles );
  }
  else if ( cmd === 'all' ) {
    await clean();
    await buildYrio( jsFiles );
    await buildSimple( jsFiles );
  }
  else if ( cmd === 'build' ) {
    await clean();
    await buildYrio( jsFiles );
    await buildSimple( jsFiles );
    await qunit();
    await lint( jsFiles );
  }
  else if ( cmd === 'test' ) {
    await qunit();
    await lint( jsFiles );
  }
  else {
    throw new Error( 'unknown command' );
  }
} )();
